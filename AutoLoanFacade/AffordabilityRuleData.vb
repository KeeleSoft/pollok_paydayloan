﻿Public Class AffordabilityRuleData

    Public Property AffordabilityPassed As Boolean
    Public Property Comment As String
    Public Property LoanFound As Boolean
    Public Property RepayAmount As Double
    Public Property CUCAAvgIncome As Double
    Public Property Share1AvgIncome As Double
    Public Property Share4AvgIncome As Double
    Public Property TotalIncome As Double
    Public Property TotalLoan As Double 'Current loan balance + Newly requested amount
    Public Property NewRepaymentAmount As Double
    Public Property NewMonthlyIncrease As Double
    Public Property RepaymentReduced As Boolean
    Public Property ProposedAffordableLoanAmount As Double
    Public Property ProposedAffordableRepaymentAmount As Double

    Public Overrides Function ToString() As String
        Dim value As String = "Affordability Rule Passed: " & If(_AffordabilityPassed, "Yes", "No")
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", Current Repayment Amount: " & _RepayAmount.ToString
        value = value & ", CUCA Income: " & _CUCAAvgIncome.ToString
        value = value & ", Share1 Income: " & _Share1AvgIncome.ToString
        value = value & ", Share4 Income: " & _Share4AvgIncome.ToString
        value = value & ", Total Income: " & _TotalIncome.ToString
        value = value & ", Total Loan: " & _TotalLoan.ToString
        value = value & ", New Repayment Amount: " & _NewRepaymentAmount.ToString
        value = value & ", New Monthly Increase: " & _NewMonthlyIncrease.ToString
        value = value & ", Monthly Repayment Reduced: " & If(_RepaymentReduced, "Yes", "No")
        If ProposedAffordableLoanAmount > 0 Then
            value = value & ", Proposed affordable loan amount: " & _ProposedAffordableLoanAmount.ToString
        End If
        If ProposedAffordableRepaymentAmount > 0 Then
            value = value & ", Proposed affordable repayment amount: " & _ProposedAffordableRepaymentAmount.ToString
        End If
        Return value
    End Function
End Class
