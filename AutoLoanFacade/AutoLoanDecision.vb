﻿''' <summary>
''' This class may hold auto loan decision related values after doing all the calculations
''' </summary>
Public Class AutoLoanDecision
    Public Property TotalMaxLoanLimitGivenRepayments As Double
    Public Property TotalMaxLoanLimitGivenRepaymentsSTL As Double
    Public Property CurrentRisk As Double
    Public Property MaxLoan As Double
    Public Property MaxRepaymentForLoanFixed As Double
    Public Property MaxRepaymentForLoanFixedSTL As Double
    Public Property TotalIncomeAfterRatio As Double
    Public Property HighestIncomeSelected As Double
    Public Property HighestIncomeSelectedSTL As Double
    Public Property ShareBalanceAfterSTL As Double
    Public Property CurrentLoanBalanceAfterSTL As Double
    Public Property STLReductionAmount As Double

    'No Share To Loan Result
    Public Property NoSTLLoanEntitlement As Double
    Public Property NoSTLTotalLoanBalance As Double
    Public Property NoSTLRepaymentAmount As Double
    Public Property NoSTLNewLoanTerm As Integer
    Public Property NoSTLLoanEntitlementPreRisk As Double
    Public Property NoSTLPreRiskLoanBalance As Double
    Public Property NoSTLHigherThanMaxReduceFigure As Double

    Public Property NoSTLRepaymentAmount_New As Double
    Public Property NoSTLNewLoanTerm_New As Integer

    'Yes Share To Loan Result
    Public Property YesSTLLoanEntitlement As Double
    Public Property YesSTLTotalLoanBalance As Double
    Public Property YesSTLRepaymentAmount As Double
    Public Property YesSTLNewLoanTerm As Integer
    Public Property YesSTLLoanEntitlementPreRisk As Double
    Public Property YesSTLPreRiskLoanBalance As Double
    Public Property YesSTLHigherThanMaxReduceFigure As Double

    Public Property YesSTLRepaymentAmount_New As Double
    Public Property YesSTLNewLoanTerm_New As Integer

    'Assuming requested amount can be granted No STL
    Public Property NoSTLRequestedAmountTotalLoanBalance As Double
    Public Property NoSTLRequestedAmountRepaymentForMaxPeriod As Double
    Public Property NoSTLRequestedAmountTermForCurrentRepayment As Integer
    Public Property NoSTLRequestedAmountFinalRepayment As Double
    Public Property NoSTLRequestedAmountFinalTerm As Integer

    Public Property NoSTLRequestedAmountFinalRepayment_New As Double
    Public Property NoSTLRequestedAmountFinalTerm_New As Integer

    'Assuming requested amount can be granted Yes STL
    Public Property YesSTLRequestedAmountTotalLoanBalance As Double
    Public Property YesSTLRequestedAmountRepaymentForMaxPeriod As Double
    Public Property YesSTLRequestedAmountTermForCurrentRepayment As Integer
    Public Property YesSTLRequestedAmountFinalRepayment As Double
    Public Property YesSTLRequestedAmountFinalTerm As Integer

    Public Property YesSTLRequestedAmountFinalRepayment_New As Double
    Public Property YesSTLRequestedAmountFinalTerm_New As Integer


    Public Overrides Function ToString() As String
        Dim value As String = "Calculations= Total Max Loan Limit (given repayments):" & _TotalMaxLoanLimitGivenRepayments.ToString
        value = value & ", Total Max Loan Limit (given repayments) STL: " & _TotalMaxLoanLimitGivenRepaymentsSTL.ToString
        value = value & ", Current Risk:: " & _CurrentRisk.ToString
        value = value & ", Max Loan:: " & _MaxLoan.ToString
        value = value & ", Max Repayment for Max Loan, Fixed 18mths: " & _MaxRepaymentForLoanFixed.ToString
        value = value & ", Max Repayment for Max Loan, Fixed 18mths (STL): " & _MaxRepaymentForLoanFixedSTL.ToString
        value = value & ", 15% of Total Income: " & _TotalIncomeAfterRatio.ToString
        value = value & ", Highest Income Selected:: " & _HighestIncomeSelected.ToString
        value = value & ", Highest Income Selected (STL): " & _HighestIncomeSelectedSTL.ToString
        value = value & ", Share Balance after STL: " & _ShareBalanceAfterSTL.ToString
        value = value & ", Current Loan balance after STL: " & _CurrentLoanBalanceAfterSTL.ToString
        value = value & ", STL Reduction Amount: " & _STLReductionAmount.ToString
        value = value & ", Highest Income Selected (STL): " & _HighestIncomeSelectedSTL.ToString & vbCrLf
        value = value & "------No STL Chosen-------" & vbCrLf
        value = value & ", New Loan Entitlement: " & _NoSTLLoanEntitlement.ToString
        value = value & ", Total Loan Balance: " & _NoSTLTotalLoanBalance.ToString
        value = value & ", Repayment Amount: " & _NoSTLRepaymentAmount.ToString
        value = value & ", New Loan Term: " & _NoSTLNewLoanTerm.ToString
        value = value & ", Loan Entitlement (pre-risk): " & _NoSTLLoanEntitlementPreRisk.ToString
        value = value & ", Pre-risk Loan Balance: " & _NoSTLPreRiskLoanBalance.ToString
        value = value & ", If higher than Max, reduce by this: " & _NoSTLHigherThanMaxReduceFigure.ToString & vbCrLf

        value = value & "------Yes STL Chosen-------" & vbCrLf
        value = value & ", New Loan Entitlement: " & _YesSTLLoanEntitlement.ToString
        value = value & ", Total Loan Balance: " & _YesSTLTotalLoanBalance.ToString
        value = value & ", Repayment Amount: " & _YesSTLRepaymentAmount.ToString
        value = value & ", New Loan Term: " & _YesSTLNewLoanTerm.ToString
        value = value & ", Loan Entitlement (pre-risk): " & _YesSTLLoanEntitlementPreRisk.ToString
        value = value & ", Pre-risk Loan Balance: " & _YesSTLPreRiskLoanBalance.ToString
        value = value & ", If higher than Max, reduce by this: " & _YesSTLHigherThanMaxReduceFigure.ToString & vbCrLf

        Return value
    End Function
End Class
