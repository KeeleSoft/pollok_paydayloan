﻿Public Class AutoLoanEngine

    Dim autoLoanData As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim oDLoanData As PollokCU.DataAccess.Layer.clsODLoan = New PollokCU.DataAccess.Layer.clsODLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    ''' <summary>
    ''' Get member details using member id
    ''' </summary>
    Public Sub GetMemberDetails(ByRef model As AutoLoanModel)
        Try
            Dim memberData As Member = Nothing
            If model IsNot Nothing Then
                Dim memDataAccess As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember

                Dim dt As DataTable = memDataAccess.SelectMemberDetail(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    memberData = New Member
                    With dt.Rows(0)
                        memberData.FirstName = CStr(.Item("FirstNameDec"))
                        memberData.Surname = CStr(.Item("SurnameDec"))
                        memberData.DateOfBirth = CDate(.Item("DateOfBirth"))
                        memberData.MemberID = CInt(.Item("MemberID"))
                        If .Item("MobileNumber") IsNot System.DBNull.Value Then
                            memberData.MobileNumber = CStr(.Item("MObileNumber"))
                        End If
                        memberData.Accountstatus = CStr(.Item("AccountStatus"))
                        If .Item("AddressLine1") IsNot System.DBNull.Value Then
                            memberData.AddressLine1 = CStr(.Item("AddressLine1"))
                        End If
                        If .Item("City") IsNot System.DBNull.Value Then
                            memberData.City = CStr(.Item("City"))
                        End If
                        If .Item("County") IsNot System.DBNull.Value Then
                            memberData.County = CStr(.Item("County"))
                        End If
                        If .Item("PostCode") IsNot System.DBNull.Value Then
                            memberData.PostCode = CStr(.Item("PostCode"))
                        End If
                    End With
                End If
            End If

            model.MemberDetails = memberData
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Get member details using member mobile number
    ''' </summary>
    Public Sub GetMemberDetailsByMobile(ByRef model As AutoLoanModel)
        Try
            Dim memberData As Member = Nothing
            If model IsNot Nothing Then
                Dim memDataAccess As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember

                Dim dt As DataTable = memDataAccess.SelectMemberDetailByMobile(model.MemberMobileNumber)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    memberData = New Member
                    With dt.Rows(0)
                        memberData.FirstName = CStr(.Item("FirstNameDec"))
                        memberData.Surname = CStr(.Item("SurnameDec"))
                        memberData.DateOfBirth = CDate(.Item("DateOfBirth"))
                        memberData.MemberID = CInt(.Item("MemberID"))
                        model.MemberID = memberData.MemberID
                        If .Item("MobileNumber") IsNot System.DBNull.Value Then
                            memberData.MobileNumber = CStr(.Item("MObileNumber"))
                        End If
                        memberData.Accountstatus = CStr(.Item("AccountStatus"))
                        memberData.PhoneNumberCount = CInt(.Item("PhoneNumberCount"))
                    End With
                End If
            End If

            model.MemberDetails = memberData
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function AutoLoanDuplicated(ByVal MemberID As Integer, ByVal AppType As String) As Boolean
        Return autoLoanData.GeAutoLoanDuplicateCheck(MemberID, AppType)
    End Function

    ''' <summary>
    ''' Get Auto Loan Parameters
    ''' </summary>
    Public Sub GetAutoLoanParameters(ByRef model As AutoLoanModel)
        Try
            Dim params As AutoLoanParameters = Nothing
            If model IsNot Nothing Then
                Dim dt As DataTable = autoLoanData.GetAutoLoanParameters()
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    params = New AutoLoanParameters
                    With dt.Rows(0)
                        params.IncomeToLoanRatio = CInt(.Item("IncomeToLoanRatio"))
                        params.InterestRate = CDbl(.Item("InterestRate"))
                        params.InterestRateOld = CDbl(.Item("OldInterestRate"))
                        params.DefaultLoanTermMonths = CInt(.Item("DefaultLoanTermMonths"))
                        params.ShareToLoanMinAmount = CDbl(.Item("ShareToLoanMinAmount"))
                        params.MaxAllowedPDLAmount = CDbl(.Item("MaxAllowedPDLAmount"))
                        params.MaxAllowedRevLoanAmount = CDbl(.Item("MaxAllowedRevLoanAmount"))
                        params.CUCADefaultSaving = CDbl(.Item("CUCADefaultSaving"))
                        params.FasterPaymentFee = CDbl(.Item("FasterPaymentFee"))
                        params.MaxAllowedODLoanAmount = CDbl(.Item("MaxAllowedODLoanAmount"))
                    End With
                End If
            End If
            model.AutoLoanParams = params
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Perform the one third rule and get all other details required to do the calculations
    ''' </summary>
    Public Sub GetInitialMemberDetails(ByRef model As AutoLoanModel)
        Try
            Dim ruleData As OneThirdRuleData = Nothing
            If model IsNot Nothing AndAlso model.MemberID > 0 Then
                Dim dt As DataTable = autoLoanData.GetInitialMemberDetails(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    ruleData = New OneThirdRuleData
                    With dt.Rows(0)
                        ruleData.MemberID = CInt(.Item("MemberID"))
                        ruleData.OneThirdRulePassed = CBool(.Item("OneThirdRulePassed"))
                        ruleData.Comment = CStr(.Item("Comment"))
                        If Not IsDBNull(.Item("LastLoanDate")) Then
                            ruleData.LastLoanDate = CDate(.Item("LastLoanDate"))
                        End If

                        If Not IsDBNull(.Item("LastLoanAmount")) Then
                            ruleData.LastLoanAmount = CDbl(.Item("LastLoanAmount"))
                        End If

                        If Not IsDBNull(.Item("LastLoanBalance")) Then
                            ruleData.LastLoanBalance = CDbl(.Item("LastLoanBalance"))
                        End If

                        If Not IsDBNull(.Item("CurrentBalance")) Then
                            ruleData.CurrentBalance = CDbl(.Item("CurrentBalance"))
                        End If

                        If Not IsDBNull(.Item("LastLoanProductCode")) Then
                            ruleData.LastLoanProductCode = CStr(.Item("LastLoanProductCode"))
                        End If

                        If Not IsDBNull(.Item("CurrentRepayAmount")) Then
                            ruleData.CurrentRepayAmount = CDbl(.Item("CurrentRepayAmount"))
                        End If

                        If Not IsDBNull(.Item("OneThirdAmount")) Then
                            ruleData.OneThirdAmount = CDbl(.Item("OneThirdAmount"))
                        End If

                        If Not IsDBNull(.Item("RiskLimit")) Then
                            model.RiskLimit = CDbl(.Item("RiskLimit"))
                        End If

                        If Not IsDBNull(.Item("CUCAAvgIncome")) Then
                            model.CUCAAvgIncome = CDbl(.Item("CUCAAvgIncome"))
                        End If

                        If Not IsDBNull(.Item("Share1AvgIncome")) Then
                            model.Share1AvgIncome = CDbl(.Item("Share1AvgIncome"))
                        End If

                        If Not IsDBNull(.Item("Share4AvgIncome")) Then
                            model.Share4AvgIncome = CDbl(.Item("Share4AvgIncome"))
                        End If

                        If Not IsDBNull(.Item("TotalIncome")) Then
                            model.TotalIncome = CDbl(.Item("TotalIncome"))
                        End If

                        If Not IsDBNull(.Item("TotalLoansOutstanding")) Then
                            model.TotalLoansOutstanding = CDbl(.Item("TotalLoansOutstanding"))
                            If ruleData.LastLoanProductCode <> "PLUSLO" Then
                                model.TotalLoansOutstanding = model.TotalLoansOutstanding + ruleData.CurrentBalance
                            End If
                            If model.TotalLoansOutstanding <= 0 AndAlso ruleData.CurrentRepayAmount > 50 Then ruleData.CurrentRepayAmount = 50 'WORKAROUND, if there is no current loan outstanding set the repayment value to 0 so it will pickup the maximum affordability when calculating
                        End If

                        If Not IsDBNull(.Item("TotalLoansRepayment")) Then
                            model.TotalLoansRepayment = CDbl(.Item("TotalLoansRepayment"))
                        End If

                        If Not IsDBNull(.Item("CurrentAccountNumber")) Then
                            model.AccountNumber = .Item("CurrentAccountNumber").ToString.PadLeft(8, "0"c)
                            model.SortCode = "089401"   'LMCU sort code
                        End If

                        If Not IsDBNull(.Item("MonthsLeftToSettle")) Then
                            ruleData.MonthsLeftToSettle = CInt(Math.Round(CDbl(.Item("MonthsLeftToSettle"))))
                        End If

                        'If Not ruleData.OneThirdRulePassed AndAlso ruleData.OneThirdAmount > 0 Then 'If 1/3rd rule failed calculate in how many months eligible to apply again
                        '    ruleData.MonthsLeftToSettle = CInt(Math.Round(((ruleData.CurrentBalance + model.TotalLoansOutstanding) - ruleData.OneThirdAmount) / (ruleData.CurrentRepayAmount + model.TotalLoansRepayment)))
                        '    'ruleData.MonthsLeftToSettle = CInt(Math.Round(((ruleData.CurrentBalance + model.TotalLoansOutstanding) / 3) / (ruleData.CurrentRepayAmount + model.TotalLoansRepayment)))
                        'End If
                    End With
                End If
            End If
            model.OneThirdRule = ruleData
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Perform the share to loan rule and populate the passed in model with processed data
    ''' </summary>
    Public Sub PerformShareToLoanRule(ByRef model As AutoLoanModel)
        Try
            Dim ruleData As ShareToLoanRuleData = Nothing
            If model IsNot Nothing AndAlso model.MemberID > 0 Then
                Dim dt As DataTable = autoLoanData.GetShareToLoanRuleData(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    ruleData = New ShareToLoanRuleData
                    With dt.Rows(0)
                        ruleData.MemberID = CInt(.Item("MemberID"))
                        ruleData.Comment = CStr(.Item("Comment"))
                        ruleData.ShareToLoanRulePassed = CBool(.Item("ShareToLoanRulePassed"))

                        If Not IsDBNull(.Item("Share1Balance")) Then
                            ruleData.Share1Balance = CDbl(.Item("Share1Balance"))
                        End If
                    End With

                End If
            End If
            model.ShareToLoanRule = ruleData
        Catch ex As Exception
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' Perform all the calculations and decide options available
    ''' </summary>
    Public Sub PerformDecisionCalculations(ByRef model As AutoLoanModel)
        Dim calcsData As AutoLoanDecision = New AutoLoanDecision
        Dim totalOutstanding As Double = model.TotalLoansOutstanding
        If model.OneThirdRule.LastLoanProductCode = "PLUSLO" Then
            totalOutstanding = totalOutstanding + model.OneThirdRule.CurrentBalance
        End If

        'Calculate income ration
        If model.TotalIncome > 0 Then
            calcsData.TotalIncomeAfterRatio = model.TotalIncome * (model.AutoLoanParams.IncomeToLoanRatio / 100)
        End If
        calcsData.CurrentRisk = totalOutstanding - model.ShareToLoanRule.Share1Balance
        calcsData.MaxLoan = model.RiskLimit - calcsData.CurrentRisk

        'Calculate Max Repayment for Max Loan, Fixed 18mths:
        calcsData.MaxRepaymentForLoanFixed = ((calcsData.MaxLoan + totalOutstanding) * (model.AutoLoanParams.InterestRateOld + (model.AutoLoanParams.InterestRateOld / (((1 + model.AutoLoanParams.InterestRateOld) ^ model.AutoLoanParams.DefaultLoanTermMonths) - 1)))) + 1
        calcsData.HighestIncomeSelected = Math.Max(Math.Min(Math.Max(model.OneThirdRule.CurrentRepayAmount, calcsData.TotalIncomeAfterRatio), calcsData.MaxRepaymentForLoanFixed), model.OneThirdRule.CurrentRepayAmount)
        calcsData.TotalMaxLoanLimitGivenRepayments = calcsData.HighestIncomeSelected / (model.AutoLoanParams.InterestRateOld + model.AutoLoanParams.InterestRateOld / (((1 + model.AutoLoanParams.InterestRateOld) ^ model.AutoLoanParams.DefaultLoanTermMonths) - 1))

        If model.ShareToLoanRule.ShareToLoanRulePassed Then
            calcsData.STLReductionAmount = Math.Ceiling(model.ShareToLoanRule.Share1Balance - model.AutoLoanParams.ShareToLoanMinAmount)
            calcsData.CurrentLoanBalanceAfterSTL = totalOutstanding - calcsData.STLReductionAmount
            calcsData.ShareBalanceAfterSTL = model.ShareToLoanRule.Share1Balance - calcsData.STLReductionAmount
            calcsData.MaxRepaymentForLoanFixedSTL = ((calcsData.MaxLoan + calcsData.CurrentLoanBalanceAfterSTL) * (model.AutoLoanParams.InterestRateOld + (model.AutoLoanParams.InterestRateOld / (((1 + model.AutoLoanParams.InterestRateOld) ^ model.AutoLoanParams.DefaultLoanTermMonths) - 1)))) + 1
            calcsData.HighestIncomeSelectedSTL = Math.Max(Math.Min(Math.Max(model.OneThirdRule.CurrentRepayAmount, calcsData.TotalIncomeAfterRatio), calcsData.MaxRepaymentForLoanFixedSTL), model.OneThirdRule.CurrentRepayAmount)
            calcsData.TotalMaxLoanLimitGivenRepaymentsSTL = calcsData.HighestIncomeSelectedSTL / (model.AutoLoanParams.InterestRateOld + model.AutoLoanParams.InterestRateOld / (((1 + model.AutoLoanParams.InterestRateOld) ^ model.AutoLoanParams.DefaultLoanTermMonths) - 1))
        End If

        'No Share To Loan Selected Options
        calcsData.NoSTLLoanEntitlementPreRisk = model.RiskLimit - calcsData.CurrentRisk + calcsData.HighestIncomeSelected
        calcsData.NoSTLPreRiskLoanBalance = totalOutstanding + calcsData.NoSTLLoanEntitlementPreRisk
        calcsData.NoSTLHigherThanMaxReduceFigure = Math.Max(calcsData.NoSTLPreRiskLoanBalance - calcsData.TotalMaxLoanLimitGivenRepayments, 0)
        calcsData.NoSTLLoanEntitlement = Math.Floor(Math.Max(Math.Min(calcsData.NoSTLLoanEntitlementPreRisk - calcsData.NoSTLHigherThanMaxReduceFigure, model.RiskLimit - calcsData.CurrentRisk), 0))
        calcsData.NoSTLTotalLoanBalance = totalOutstanding 'calcsData.NoSTLLoanEntitlement + model.TotalLoansOutstanding
        calcsData.NoSTLRepaymentAmount = Math.Ceiling(Math.Min(Math.Max(model.OneThirdRule.CurrentRepayAmount, calcsData.TotalIncomeAfterRatio), calcsData.MaxRepaymentForLoanFixed))
        'calcsData.NoSTLNewLoanTerm = CInt(Math.Ceiling((Math.Log((1 + (model.AutoLoanParams.InterestRateOld / ((calcsData.HighestIncomeSelected / calcsData.NoSTLTotalLoanBalance) - model.AutoLoanParams.InterestRateOld))))) / (Math.Log((1 + model.AutoLoanParams.InterestRateOld)))))

        Dim NoSTLLoanEntitlementloanAmountToCalc As Double = calcsData.NoSTLLoanEntitlement
        If model.OneThirdRule.LastLoanProductCode = "PLUSLO" Then
            NoSTLLoanEntitlementloanAmountToCalc = NoSTLLoanEntitlementloanAmountToCalc + model.OneThirdRule.CurrentBalance
        End If
        calcsData.NoSTLRepaymentAmount_New = Math.Ceiling(Pmt(model.AutoLoanParams.InterestRate, model.AutoLoanParams.DefaultLoanTermMonths, -1 * NoSTLLoanEntitlementloanAmountToCalc))
        calcsData.NoSTLNewLoanTerm_New = model.AutoLoanParams.DefaultLoanTermMonths

        calcsData.NoSTLRepaymentAmount = model.OneThirdRule.CurrentRepayAmount - calcsData.NoSTLRepaymentAmount_New
        Try
            calcsData.NoSTLNewLoanTerm = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, calcsData.NoSTLRepaymentAmount, -1 * calcsData.NoSTLTotalLoanBalance)))
        Catch ex As Exception
            calcsData.NoSTLRepaymentAmount = 0
            calcsData.NoSTLNewLoanTerm = 0
            objSystemData.InsertErrorLog(ex, "AutoLoanModel")
        End Try

        Dim yesSTLLoanEntitlementloanAmountToCalc As Double = 0
        'Yes Share To Loan selected options
        If model.ShareToLoanRule.ShareToLoanRulePassed Then
            calcsData.YesSTLLoanEntitlementPreRisk = calcsData.TotalMaxLoanLimitGivenRepayments - calcsData.CurrentLoanBalanceAfterSTL + calcsData.HighestIncomeSelectedSTL
            calcsData.YesSTLPreRiskLoanBalance = calcsData.CurrentLoanBalanceAfterSTL + calcsData.YesSTLLoanEntitlementPreRisk
            calcsData.YesSTLHigherThanMaxReduceFigure = Math.Max(calcsData.YesSTLPreRiskLoanBalance - calcsData.TotalMaxLoanLimitGivenRepaymentsSTL, 0)
            calcsData.YesSTLLoanEntitlement = Math.Floor(Math.Max(Math.Min(calcsData.YesSTLLoanEntitlementPreRisk - calcsData.YesSTLHigherThanMaxReduceFigure, model.RiskLimit - calcsData.CurrentRisk), 0))
            calcsData.YesSTLTotalLoanBalance = calcsData.CurrentLoanBalanceAfterSTL 'calcsData.YesSTLLoanEntitlement + calcsData.CurrentLoanBalanceAfterSTL
            calcsData.YesSTLRepaymentAmount = Math.Ceiling(Math.Max(Math.Min(Math.Max(model.OneThirdRule.CurrentRepayAmount, calcsData.TotalIncomeAfterRatio), calcsData.MaxRepaymentForLoanFixedSTL), model.OneThirdRule.CurrentRepayAmount))
            'calcsData.YesSTLNewLoanTerm = CInt(Math.Ceiling((Math.Log((1 + (model.AutoLoanParams.InterestRateOld / ((calcsData.HighestIncomeSelectedSTL / calcsData.YesSTLTotalLoanBalance) - model.AutoLoanParams.InterestRateOld))))) / (Math.Log((1 + model.AutoLoanParams.InterestRateOld)))))

            yesSTLLoanEntitlementloanAmountToCalc = calcsData.YesSTLLoanEntitlement
            If model.OneThirdRule.LastLoanProductCode = "PLUSLO" Then
                yesSTLLoanEntitlementloanAmountToCalc = yesSTLLoanEntitlementloanAmountToCalc + model.OneThirdRule.CurrentBalance
            End If
            calcsData.YesSTLRepaymentAmount_New = Math.Ceiling(Pmt(model.AutoLoanParams.InterestRate, model.AutoLoanParams.DefaultLoanTermMonths, -1 * yesSTLLoanEntitlementloanAmountToCalc))
            calcsData.YesSTLNewLoanTerm_New = model.AutoLoanParams.DefaultLoanTermMonths

            calcsData.YesSTLRepaymentAmount = calcsData.YesSTLRepaymentAmount - calcsData.YesSTLRepaymentAmount_New
            Try
                calcsData.YesSTLNewLoanTerm = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, calcsData.YesSTLRepaymentAmount, -1 * calcsData.YesSTLTotalLoanBalance)))
            Catch ex As Exception
                calcsData.YesSTLLoanEntitlement = 0
                calcsData.YesSTLTotalLoanBalance = 0
                Dim calcData As String = model.AutoLoanParams.InterestRateOld & "-" & calcsData.YesSTLRepaymentAmount & "-" & calcsData.YesSTLTotalLoanBalance
                objSystemData.InsertErrorLog(ex, "AutoLoanModel", calcData)
            End Try
        End If

        'Calculate assuming requested amount can be granted without STL
        calcsData.NoSTLRequestedAmountTotalLoanBalance = totalOutstanding '+ model.LoanAmount
        calcsData.NoSTLRequestedAmountRepaymentForMaxPeriod = Pmt(model.AutoLoanParams.InterestRateOld, model.AutoLoanParams.DefaultLoanTermMonths, -1 * calcsData.NoSTLRequestedAmountTotalLoanBalance)
        calcsData.NoSTLRequestedAmountTermForCurrentRepayment = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, model.OneThirdRule.CurrentRepayAmount, -1 * calcsData.NoSTLRequestedAmountTotalLoanBalance)))
        If model.OneThirdRule.CurrentRepayAmount > calcsData.NoSTLRequestedAmountRepaymentForMaxPeriod Then 'If the current repayment already higher than new calculated repayment use current repayment and reduce the term
            calcsData.NoSTLRequestedAmountFinalRepayment = model.OneThirdRule.CurrentRepayAmount
            'calcsData.NoSTLRequestedAmountFinalTerm = calcsData.NoSTLRequestedAmountTermForCurrentRepayment
        Else
            calcsData.NoSTLRequestedAmountFinalRepayment = calcsData.NoSTLRequestedAmountRepaymentForMaxPeriod
            'calcsData.NoSTLRequestedAmountFinalTerm = model.AutoLoanParams.DefaultLoanTermMonths
        End If
        Dim loanAmountToCalc As Double = model.LoanAmount
        If model.OneThirdRule.LastLoanProductCode = "PLUSLO" Then
            loanAmountToCalc = loanAmountToCalc + model.OneThirdRule.CurrentBalance
        End If
        calcsData.NoSTLRequestedAmountFinalRepayment_New = Math.Ceiling(Pmt(model.AutoLoanParams.InterestRate, model.AutoLoanParams.DefaultLoanTermMonths, -1 * loanAmountToCalc))  'calcsData.NoSTLRequestedAmountTotalLoanBalance
        calcsData.NoSTLRequestedAmountFinalTerm_New = model.AutoLoanParams.DefaultLoanTermMonths

        Try
            calcsData.NoSTLRequestedAmountFinalRepayment = calcsData.NoSTLRequestedAmountFinalRepayment - calcsData.NoSTLRequestedAmountFinalRepayment_New
            calcsData.NoSTLRequestedAmountFinalTerm = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, calcsData.NoSTLRequestedAmountFinalRepayment, -1 * calcsData.NoSTLRequestedAmountTotalLoanBalance)))
        Catch ex As Exception
            calcsData.NoSTLLoanEntitlement = 0  'Do allow to get the requested amount
            Dim calcData As String = model.AutoLoanParams.InterestRateOld & "-" & calcsData.NoSTLRequestedAmountFinalRepayment & "-" & calcsData.NoSTLRequestedAmountTotalLoanBalance
            objSystemData.InsertErrorLog(ex, "AutoLoanModel", calcData)
        End Try

        'Calculate assuming requested amount can be granted with STL
        calcsData.YesSTLRequestedAmountTotalLoanBalance = calcsData.CurrentLoanBalanceAfterSTL '+ model.LoanAmount
        calcsData.YesSTLRequestedAmountRepaymentForMaxPeriod = Pmt(model.AutoLoanParams.InterestRateOld, model.AutoLoanParams.DefaultLoanTermMonths, -1 * calcsData.YesSTLRequestedAmountTotalLoanBalance)
        calcsData.YesSTLRequestedAmountTermForCurrentRepayment = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, model.OneThirdRule.CurrentRepayAmount, -1 * calcsData.YesSTLRequestedAmountTotalLoanBalance)))
        If model.OneThirdRule.CurrentRepayAmount > calcsData.YesSTLRequestedAmountRepaymentForMaxPeriod Then 'If the current repayment already higher than new calculated repayment use current repayment and reduce the term
            calcsData.YesSTLRequestedAmountFinalRepayment = model.OneThirdRule.CurrentRepayAmount
            calcsData.YesSTLRequestedAmountFinalTerm = calcsData.YesSTLRequestedAmountTermForCurrentRepayment
        Else
            calcsData.YesSTLRequestedAmountFinalRepayment = calcsData.YesSTLRequestedAmountRepaymentForMaxPeriod
            calcsData.YesSTLRequestedAmountFinalTerm = model.AutoLoanParams.DefaultLoanTermMonths
        End If

        yesSTLLoanEntitlementloanAmountToCalc = calcsData.YesSTLLoanEntitlement
        If model.OneThirdRule.LastLoanProductCode = "PLUSLO" Then
            yesSTLLoanEntitlementloanAmountToCalc = yesSTLLoanEntitlementloanAmountToCalc + model.OneThirdRule.CurrentBalance
        End If
        calcsData.YesSTLRequestedAmountFinalRepayment_New = Math.Ceiling(Pmt(model.AutoLoanParams.InterestRate, model.AutoLoanParams.DefaultLoanTermMonths, -1 * yesSTLLoanEntitlementloanAmountToCalc))
        calcsData.YesSTLRequestedAmountFinalTerm_New = model.AutoLoanParams.DefaultLoanTermMonths

        calcsData.YesSTLRequestedAmountFinalRepayment = calcsData.YesSTLRequestedAmountFinalRepayment - calcsData.YesSTLRequestedAmountFinalRepayment_New
        Try
            calcsData.YesSTLRequestedAmountFinalTerm = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRateOld, calcsData.YesSTLRequestedAmountFinalRepayment, -1 * calcsData.YesSTLRequestedAmountTotalLoanBalance)))
        Catch ex As Exception
            calcsData.YesSTLRequestedAmountFinalRepayment = 0
            calcsData.YesSTLRequestedAmountTotalLoanBalance = 0
            Dim calcData As String = model.AutoLoanParams.InterestRateOld & "-" & calcsData.YesSTLRequestedAmountFinalRepayment & "-" & calcsData.YesSTLRequestedAmountTotalLoanBalance
            objSystemData.InsertErrorLog(ex, "AutoLoanModel", calcData)
        End Try

        model.DecisionCalcs = calcsData
        'finally round up all calculated loan figures to the nearest 5
        model.DecisionCalcs.NoSTLLoanEntitlement = Math.Ceiling(model.DecisionCalcs.NoSTLLoanEntitlement / 5) * 5
        model.DecisionCalcs.YesSTLLoanEntitlement = Math.Ceiling(model.DecisionCalcs.YesSTLLoanEntitlement / 5) * 5

    End Sub
#Region "Obsolete"
    ''' <summary>
    ''' Perform the 1/3rd rule and populate the passed in model with processed data
    ''' </summary>
    Public Sub PerformOneThirdRule(ByRef model As AutoLoanModel)
        Try
            Dim ruleData As OneThirdRuleData = Nothing
            If model IsNot Nothing AndAlso model.MemberID > 0 Then
                Dim dt As DataTable = autoLoanData.GetOneThirdRuleData(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    ruleData = New OneThirdRuleData
                    With dt.Rows(0)
                        ruleData.MemberID = CInt(.Item("MemberID"))
                        ruleData.OneThirdRulePassed = CBool(.Item("OneThirdRulePassed"))
                        ruleData.Comment = CStr(.Item("Comment"))
                        If Not IsDBNull(.Item("LastLoanDate")) Then
                            ruleData.LastLoanDate = CDate(.Item("LastLoanDate"))
                        End If

                        If Not IsDBNull(.Item("LastLoanAmount")) Then
                            ruleData.LastLoanAmount = CDbl(.Item("LastLoanAmount"))
                        End If

                        If Not IsDBNull(.Item("LastLoanBalance")) Then
                            ruleData.LastLoanBalance = CDbl(.Item("LastLoanBalance"))
                        End If

                        If Not IsDBNull(.Item("CurrentBalance")) Then
                            ruleData.CurrentBalance = CDbl(.Item("CurrentBalance"))
                        End If

                        If Not IsDBNull(.Item("LastLoanProductCode")) Then
                            ruleData.LastLoanProductCode = CStr(.Item("LastLoanProductCode"))
                        End If

                        If Not IsDBNull(.Item("LastRepayAmount")) Then
                            ruleData.CurrentRepayAmount = CDbl(.Item("LastRepayAmount"))
                        End If

                        If Not IsDBNull(.Item("OneThirdAmount")) Then
                            ruleData.OneThirdAmount = CDbl(.Item("OneThirdAmount"))
                        End If
                    End With

                End If
            End If
            model.OneThirdRule = ruleData
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Perform the how much can borrow rule and populate the passed in model with processed data
    ''' </summary>
    Public Sub PerformLoanEntitlementRule(ByRef model As AutoLoanModel)
        'Try
        '    Dim ruleData As LoanEntitlementRuleData = Nothing
        '    If model IsNot Nothing AndAlso model.MemberID > 0 Then
        '        Dim dt As DataTable = autoLoanData.GetLoanEntitlementRuleData(model.MemberID)
        '        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
        '            ruleData = New LoanEntitlementRuleData
        '            With dt.Rows(0)
        '                ruleData.MemberID = CInt(.Item("MemberID"))
        '                ruleData.LoanEntitlement = CDbl(.Item("LoanEntitlement"))
        '                If ruleData.LoanEntitlement > 0 Then
        '                    Dim multiplier As Integer = CInt(Math.Floor(ruleData.LoanEntitlement / model.AutoLoanParams.LoanEntitlementRoundUpValue))
        '                    If ruleData.LoanEntitlement Mod model.AutoLoanParams.LoanEntitlementRoundUpValue > 0 Then
        '                        multiplier += 1
        '                    End If
        '                    ruleData.LoanEntitlementRounded = multiplier * model.AutoLoanParams.LoanEntitlementRoundUpValue
        '                Else
        '                    ruleData.LoanEntitlementRounded = 0
        '                End If
        '                ruleData.Comment = CStr(.Item("Comment"))
        '                If ruleData.LoanEntitlement >= model.LoanAmount Then
        '                    ruleData.LoanEntitlementRulePassed = True
        '                Else
        '                    ruleData.LoanEntitlementRulePassed = False
        '                End If

        '                If Not IsDBNull(.Item("LoanLimit")) Then
        '                    ruleData.LoanLimit = CDbl(.Item("LoanLimit"))
        '                End If

        '                If Not IsDBNull(.Item("TotalLoansOutstanding")) Then
        '                    ruleData.TotalLoansOutstanding = CDbl(.Item("TotalLoansOutstanding"))
        '                End If

        '                If Not IsDBNull(.Item("ShareBalance")) Then
        '                    ruleData.ShareBalance = CDbl(.Item("ShareBalance"))
        '                End If

        '                If Not IsDBNull(.Item("TotalRisk")) Then
        '                    ruleData.TotalRisk = CDbl(.Item("TotalRisk"))
        '                End If

        '                If Not IsDBNull(.Item("FutureRepaymentsToAdd")) Then
        '                    ruleData.FutureRepaymentsToAdd = CInt(.Item("FutureRepaymentsToAdd"))
        '                End If

        '                If Not IsDBNull(.Item("FutureRepaymentsAmount")) Then
        '                    ruleData.FutureRepaymentsAmount = CDbl(.Item("FutureRepaymentsAmount"))
        '                End If

        '                If Not IsDBNull(.Item("LoanRepayAmount")) Then
        '                    ruleData.LoanRepayAmount = CDbl(.Item("LoanRepayAmount"))
        '                End If

        '                If Not IsDBNull(.Item("NextRepayDate")) Then
        '                    ruleData.NextRepayDate = CDate(.Item("NextRepayDate"))
        '                End If
        '            End With

        '        End If
        '    End If
        '    model.LoanEntitlementRule = ruleData
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    ''' <summary>
    ''' Perform the Affordability rule
    ''' </summary>
    Public Sub PerformAffordabilityRule(ByRef model As AutoLoanModel)
        'Try
        '    If model.AutoLoanParams Is Nothing Then
        '        Throw New NotImplementedException("No auto loan parameters loaded")
        '    End If

        '    If model.LoanEntitlementRule Is Nothing Then
        '        Throw New NotImplementedException("No auto loan entitlement data loaded")
        '    End If

        '    Dim affordRuleData As AffordabilityRuleData = Nothing
        '    If model IsNot Nothing AndAlso model.MemberID > 0 Then
        '        Dim dt As DataTable = autoLoanData.GetAffordabilityRuleData(model.MemberID)
        '        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
        '            affordRuleData = New AffordabilityRuleData
        '            With dt.Rows(0)
        '                affordRuleData.Comment = CStr(.Item("Comment"))
        '                affordRuleData.LoanFound = CBool(.Item("LoanFound"))
        '                affordRuleData.RepayAmount = CDbl(.Item("RepayAmount"))
        '                affordRuleData.CUCAAvgIncome = CDbl(.Item("CUCAAvgIncome"))
        '                affordRuleData.Share1AvgIncome = CDbl(.Item("Share1AvgIncome"))
        '                affordRuleData.Share4AvgIncome = CDbl(.Item("Share4AvgIncome"))
        '                affordRuleData.TotalIncome = CDbl(.Item("TotalIncome"))
        '            End With
        '        End If
        '    End If

        '    'Calculate total loan (Loan entitlement + new requested amount
        '    affordRuleData.TotalLoan = model.LoanEntitlementRule.TotalLoansOutstanding + model.LoanAmount
        '    affordRuleData.NewRepaymentAmount = Math.Ceiling(Math.Round(Pmt((model.AutoLoanParams.InterestRate / 100) / 12, model.AutoLoanParams.DefaultLoanTermMonths, -1 * affordRuleData.TotalLoan), 2))
        '    affordRuleData.NewMonthlyIncrease = affordRuleData.NewRepaymentAmount - affordRuleData.RepayAmount
        '    If affordRuleData.NewMonthlyIncrease <= 0 Then 'If the monthly payment is decreased can offer the member to keep upto same payments, So set the property inorder to handle by the UI
        '        affordRuleData.AffordabilityPassed = True   'Can definitely afford if the monthly payment is reduced
        '        affordRuleData.RepaymentReduced = True
        '    Else
        '        Dim MaxCanAfford As Double = affordRuleData.TotalIncome * (model.AutoLoanParams.IncomeToLoanRatio / 100)
        '        If affordRuleData.NewRepaymentAmount <= MaxCanAfford + model.AutoLoanParams.AffordabilityTolerance Then
        '            affordRuleData.AffordabilityPassed = True
        '        Else
        '            affordRuleData.AffordabilityPassed = False
        '        End If
        '    End If
        '    model.AffordabilityRule = affordRuleData
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    ''' <summary>
    ''' Calculate the loan amount based on the current loan repayment
    ''' </summary>
    Public Sub FindLoanAmountBasedOnCurrentRepayment(ByRef model As AutoLoanModel)
        'Try
        '    Dim ProposedLoanAmount As Double = model.LoanAmount
        '    Dim CalculatedLoanRepayment As Double = model.AffordabilityRule.NewRepaymentAmount
        '    Dim LoanReduceValue As Double = 50


        '    While ProposedLoanAmount > 100 AndAlso CalculatedLoanRepayment > model.LoanEntitlementRule.LoanRepayAmount + model.AutoLoanParams.LoanRepaymentTolerance
        '        ProposedLoanAmount = ProposedLoanAmount - LoanReduceValue
        '        CalculatedLoanRepayment = Math.Ceiling(Math.Round(Pmt((model.AutoLoanParams.InterestRate / 100) / 12, model.AutoLoanParams.DefaultLoanTermMonths, -1 * (model.LoanEntitlementRule.TotalLoansOutstanding + ProposedLoanAmount)), 2))

        '    End While
        '    model.AffordabilityRule.ProposedAffordableLoanAmount = ProposedLoanAmount
        '    model.AffordabilityRule.ProposedAffordableRepaymentAmount = CalculatedLoanRepayment
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    ''' <summary>
    ''' Determine available options when not passed one or more checks
    ''' </summary>
    Public Sub GetAvailableOptions(ByRef model As AutoLoanModel)
        If model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.OneThirdRulePassed Then  'If the 1/3 rule passed only we can proceed
            If model.ShareToLoanRule IsNot Nothing AndAlso model.ShareToLoanRule.ShareToLoanRulePassed Then 'Give options with share to loan

            End If
        End If
    End Sub
#End Region

#Region "Pay Day Loan"
    ''' <summary>
    ''' Perform the pay day loan rule and populate the passed in model with processed data
    ''' </summary>
    Public Sub PerformPayDayLoanRule(ByRef model As AutoLoanModel)
        Try
            Dim ruleData As PayDayLoanRuleData = Nothing
            If model IsNot Nothing AndAlso model.MemberID > 0 Then
                Dim dt As DataTable = autoLoanData.GetPayDayLoanRuleData(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    ruleData = New PayDayLoanRuleData
                    With dt.Rows(0)
                        ruleData.MemberID = CInt(.Item("MemberID"))
                        ruleData.Comment = CStr(.Item("Comment"))
                        ruleData.PayDayLoanRulePassed = CBool(.Item("PayDayLoanRulePassed"))
                        ruleData.PDLRiskLimit = CInt(.Item("PDLRiskLimit"))
                        'model.SortCode = CStr(.Item("LastPaidSortCode"))
                        'model.AccountNumber = CStr(.Item("LastPaidAccNumber"))

                        If Not IsDBNull(.Item("LastPaidSortCode")) Then
                            ruleData.LastPaidSortCode = CStr(.Item("LastPaidSortCode"))
                        End If

                        If Not IsDBNull(.Item("LastPaidAccNumber")) Then
                            ruleData.LastPaidAccNumber = CStr(.Item("LastPaidAccNumber"))
                        End If

                        If Not IsDBNull(.Item("LastPDLLoanDate")) Then
                            ruleData.LastPDLLoanDate = CDate(.Item("LastPDLLoanDate"))
                        End If

                        If Not IsDBNull(.Item("LastPDLLoanAmount")) Then
                            ruleData.LastPDLLoanAmount = CDbl(.Item("LastPDLLoanAmount"))
                        End If

                        If Not IsDBNull(.Item("CurrentPDLLoanBalance")) Then
                            ruleData.CurrentPDLLoanBalance = CDbl(.Item("CurrentPDLLoanBalance"))
                        End If
                        ruleData.LastPDLApplicationID = CInt(.Item("LastPDLApplicationID"))
                        ruleData.LastSagePayResponseID = CInt(.Item("LastSagePayResponseID"))
                        ruleData.Last4Digits = CInt(.Item("Last4Digits"))
                        ruleData.IsLastPDLBACS = CBool(.Item("IsLastPDLBACS"))

                        ruleData.PDLShareBalance = CDbl(.Item("PDLShareBalance"))
                        ruleData.PDLShareToLoanRequired = CBool(.Item("PDLShareToLoanRequired"))
                        ruleData.SettleFromLastSagePayment = CBool(.Item("SettleFromLastSagePayment"))
                        ruleData.LastSagePayment = CDbl(.Item("LastSagePayment"))
                        ruleData.LoanFound = CBool(.Item("LoanFound"))
                    End With

                    If ruleData.PayDayLoanRulePassed AndAlso ruleData.PDLRiskLimit >= model.LoanAmount Then   'Pay day rule passed, so do the calculations
                        ruleData.MonthlyRepayment = Math.Round(Pmt(0.02, model.PDLLoanTerm, -1 * model.LoanAmount), 2)
                        model.FinalLoanAmount = model.LoanAmount
                        model.FinalLoanTerm = model.PDLLoanTerm
                        If model.PaymentMethod = "FP" Then
                            model.FinalMonthlyPayment = ruleData.MonthlyRepayment + (model.AutoLoanParams.FasterPaymentFee / model.PDLLoanTerm)
                        Else
                            model.FinalMonthlyPayment = ruleData.MonthlyRepayment
                        End If
                    End If
                End If
            End If
            model.PayDayLoanRule = ruleData
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "OD Loan"
    ''' <summary>
    ''' Perform the pay day loan rule and populate the passed in model with processed data
    ''' </summary>
    Public Sub PerformODLoanRule(ByRef model As AutoLoanModel)
        Try
            Dim ruleData As ODLoanRuleData = Nothing
            If model IsNot Nothing AndAlso model.MemberID > 0 Then
                Dim dt As DataTable = oDLoanData.GetODLoanRuleData(model.MemberID)
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    ruleData = New ODLoanRuleData
                    With dt.Rows(0)
                        ruleData.MemberID = CInt(.Item("MemberID"))
                        ruleData.Comment = CStr(.Item("Comment"))
                        ruleData.ODLoanRulePassed = CBool(.Item("ODLoanRulePassed"))
                        ruleData.ODLoanRiskLimit = CInt(.Item("ODLoanRiskLimit"))

                        If Not IsDBNull(.Item("LastPaidSortCode")) Then
                            ruleData.LastPaidSortCode = CStr(.Item("LastPaidSortCode"))
                        End If

                        If Not IsDBNull(.Item("LastPaidAccNumber")) Then
                            ruleData.LastPaidAccNumber = CStr(.Item("LastPaidAccNumber"))
                        End If

                        If Not IsDBNull(.Item("LastODLoanDate")) Then
                            ruleData.LastODLoanDate = CDate(.Item("LastODLoanDate"))
                        End If

                        If Not IsDBNull(.Item("LastODLoanAmount")) Then
                            ruleData.LastODLoanAmount = CDbl(.Item("LastODLoanAmount"))
                        End If

                        If Not IsDBNull(.Item("CurrentODLoanBalance")) Then
                            ruleData.CurrentODLoanBalance = CDbl(.Item("CurrentODLoanBalance"))
                        End If
                        ruleData.LastODLoanApplicationID = CInt(.Item("LastODLoanApplicationID"))
                        ruleData.LastSagePayResponseID = CInt(.Item("LastSagePayResponseID"))
                        ruleData.Last4Digits = CInt(.Item("Last4Digits"))
                        ruleData.IsLastODLoanBACS = CBool(.Item("IsLastODLoanBACS"))
                    End With

                    If ruleData.ODLoanRulePassed AndAlso ruleData.ODLoanRiskLimit >= model.LoanAmount Then   'Od loan rule passed, so do the calculations
                        ruleData.MonthlyRepayment = Math.Round(Pmt(model.AutoLoanParams.InterestRate, model.PDLLoanTerm, -1 * model.LoanAmount), 2)
                        model.FinalLoanAmount = model.LoanAmount
                        model.FinalLoanTerm = model.PDLLoanTerm
                        If model.PaymentMethod = "FP" Then
                            model.FinalMonthlyPayment = ruleData.MonthlyRepayment + (model.AutoLoanParams.FasterPaymentFee / model.PDLLoanTerm)
                        Else
                            model.FinalMonthlyPayment = ruleData.MonthlyRepayment
                        End If
                    End If
                End If
            End If
            model.ODLoanRule = ruleData
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Helpers"
    Shared Function GenerateSecurityCode(DOB As Date) As String
        Dim code As String = ""
        Dim DOBDay As Integer = DOB.Day
        Dim DOBMonth As Integer = DOB.Month
        Dim DOBYear As Integer = DOB.Year

        Dim NowDay As Integer = Now.Day
        Dim NowMonth As Integer = Now.Month
        Dim NowYear As Integer = Now.Year

        Dim NowCode As Integer = NowYear * NowMonth * NowDay
        Dim DOBCode As Integer = DOBYear + DOBMonth + DOBDay
        code = (NowCode + DOBCode).ToString
        Return code
    End Function
#End Region
End Class
