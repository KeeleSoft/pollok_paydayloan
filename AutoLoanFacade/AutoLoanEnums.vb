﻿Public Class AutoLoanEnums
    Public Enum AutoLoanType
        None = 0
        PayDay = 1
        RevLoan = 2
        ODLoan = 3
    End Enum

    Public Enum ApplyMethod
        None = 0
        MemberSite = 1
        StaffLogin = 2
        TextSystem = 3
        MobileApp = 4
        PDLMainApplication = 5
    End Enum

    Public Enum ApplicationStatus
        None = 0
        Applied = 1
        Appealed = 2
        Approved_Automatically = 3
        Approved_By_Staff = 4
        Refused_Automatically = 5
        Refused_By_Staff = 6
        Security_Failed = 7
        UnderInvestigation = 8
        OneThirdRuleFailed = 9
    End Enum

    Public Enum LoanOutcome
        NotCompleted = 0
        LoanAgreedNoConditions = 1
        NoSTLOptionSelected = 2
        YesSTLOptionSelected = 3
        Appealed = 4
        Refused = 5
    End Enum
End Class
