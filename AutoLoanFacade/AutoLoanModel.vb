﻿Public Class AutoLoanModel

    Public Property UserID As Integer
    Public Property MemberID As Integer
    Public Property MemberMobileNumber As String
    Public Property LoanAmount As Double
    Public Property PDLLoanTerm As Integer
    Public Property LoanType As AutoLoanEnums.AutoLoanType
    Public Property ApplyMethod As AutoLoanEnums.ApplyMethod
    Public Property RiskLimit As Double
    Public Property TotalIncome As Double
    Public Property CUCAAvgIncome As Double
    Public Property Share1AvgIncome As Double
    Public Property Share4AvgIncome As Double
    Public Property TotalLoansOutstanding As Double
    Public Property TotalLoansRepayment As Double

    Public Property MemberDetails As Member = Nothing
    Public Property AutoLoanParams As AutoLoanParameters = Nothing
    Public Property OneThirdRule As OneThirdRuleData = Nothing
    Public Property DecisionCalcs As AutoLoanDecision = Nothing
    Public Property LoanEntitlementRule As LoanEntitlementRuleData = Nothing
    Public Property AffordabilityRule As AffordabilityRuleData = Nothing
    Public Property ShareToLoanRule As ShareToLoanRuleData = Nothing
    Public Property PayDayLoanRule As PayDayLoanRuleData = Nothing
    Public Property ODLoanRule As ODLoanRuleData = Nothing

    Public Property AutoLoanID As Integer = 0
    Public Property AppealReason As String = ""
    Public Property ApplicationStatus As AutoLoanEnums.ApplicationStatus = AutoLoanEnums.ApplicationStatus.Applied

    Public Property PaymentMethod As String = ""
    Public Property AccountNumber As String = ""
    Public Property SortCode As String = ""
    Public Property AccountOpenYear As Integer = 0
    Public Property AccountOpenMonth As Integer = 0

    Public Property ShareToLoanAccepted As Boolean
    Public Property LoanOutcomeFlag As Integer


    Public Property FinalLoanAmount As Double
    Public Property FinalLoanTerm As Integer
    Public Property FinalMonthlyPayment As Double
    Public Property FinalMonthlyPaymentForCUCA As Double
    Public Property FinalOldLoanAmount As Double
    Public Property FinalOldLoanTerm As Integer
    Public Property FinalOldMonthlyPayment As Double

    Public Property SMSSecurityCode As Integer

    Public Property NextPayDay As Date

    Public Property Flag As Integer = 0

    ''' <summary>
    ''' This is just a flag to avoid duplicating the faster payment fee to be added to the final amount
    ''' </summary>
    Public Property FasterPayFeeAddedToFinalAmount As Boolean

    ''' <summary>
    ''' Staff should be able to ignore one third failures and proceed to insert the loan so it can be processed via staff admin
    ''' </summary>
    Public Property IgnoreOneThirdFailure As Boolean

    Public Sub SaveAutoLoanModelDetails(ByVal Level As Integer)
        Try
            Dim da As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

            Dim oneThirdRuleStatus As Integer = 0
            Dim oneThirdRuleStr As String = ""
            Dim currentRepayment As Double = 0
            Dim productCode As String = ""
            Dim currentBalance As Double = 0
            If OneThirdRule IsNot Nothing Then
                If OneThirdRule.OneThirdRulePassed Then
                    oneThirdRuleStatus = 1
                Else
                    oneThirdRuleStatus = 2
                End If
                oneThirdRuleStr = OneThirdRule.ToString
                currentRepayment = OneThirdRule.CurrentRepayAmount
                productCode = OneThirdRule.LastLoanProductCode
                currentBalance = OneThirdRule.CurrentBalance
            End If

            Dim shareToLoanRuleStatus As Integer = 0
            Dim shareToLoanRuleStr As String = ""
            Dim shareBalance As Double = 0
            If ShareToLoanRule IsNot Nothing Then
                If ShareToLoanRule.ShareToLoanRulePassed Then
                    shareToLoanRuleStatus = 1
                Else
                    shareToLoanRuleStatus = 2
                End If
                shareToLoanRuleStr = ShareToLoanRule.ToString
                shareBalance = ShareToLoanRule.Share1Balance
            End If

            Dim payDayRuleStatus As Integer = 0
            Dim payDayRuleStr As String = ""
            If PayDayLoanRule IsNot Nothing Then
                If PayDayLoanRule.PayDayLoanRulePassed Then
                    payDayRuleStatus = 1
                Else
                    payDayRuleStatus = 2
                End If
                payDayRuleStr = PayDayLoanRule.ToString
            ElseIf ODLoanRule IsNot Nothing Then
                If ODLoanRule.ODLoanRulePassed Then
                    payDayRuleStatus = 1
                Else
                    payDayRuleStatus = 2
                End If
                payDayRuleStr = ODLoanRule.ToString
            End If


            Dim TotalMaxLoanLimitGivenRepayments As Double = 0
            Dim TotalMaxLoanLimitGivenRepaymentsSTL As Double = 0
            Dim CurrentRisk As Double = 0
            Dim MaxLoan As Double = 0
            Dim MaxRepaymentForLoanFixed As Double = 0
            Dim MaxRepaymentForLoanFixedSTL As Double = 0
            Dim TotalIncomeAfterRatio As Double = 0
            Dim HighestIncomeSelected As Double = 0
            Dim HighestIncomeSelectedSTL As Double = 0
            Dim ShareBalanceAfterSTL As Double = 0
            Dim CurrentLoanBalanceAfterSTL As Double = 0
            Dim STLReductionAmount As Double = 0

            'No Share To Loan Result
            Dim NoSTLLoanEntitlement As Double = 0
            Dim NoSTLTotalLoanBalance As Double = 0
            Dim NoSTLRepaymentAmount As Double = 0
            Dim NoSTLNewLoanTerm As Integer = 0
            Dim NoSTLLoanEntitlementPreRisk As Double = 0
            Dim NoSTLPreRiskLoanBalance As Double = 0
            Dim NoSTLHigherThanMaxReduceFigure As Double = 0

            'Yes Share To Loan Result
            Dim YesSTLLoanEntitlement As Double = 0
            Dim YesSTLTotalLoanBalance As Double = 0
            Dim YesSTLRepaymentAmount As Double = 0
            Dim YesSTLNewLoanTerm As Integer = 0
            Dim YesSTLLoanEntitlementPreRisk As Double = 0
            Dim YesSTLPreRiskLoanBalance As Double = 0
            Dim YesSTLHigherThanMaxReduceFigure As Double = 0
            Dim YesSTLRequestedAmountFinalTerm As Double = 0
            Dim YesSTLRequestedAmountFinalRepayment As Double = 0

            Dim DecisionCalcsString As String = ""

            Dim NoSTLRequestedAmountTotalLoanBalance As Double = 0
            Dim NoSTLRequestedAmountFinalTerm As Integer = 0
            Dim NoSTLRequestedAmountFinalRepayment As Double = 0
            Dim YesSTLRequestedAmountTotalLoanBalance As Double = 0

            If DecisionCalcs IsNot Nothing Then
                With DecisionCalcs
                    TotalMaxLoanLimitGivenRepayments = .TotalMaxLoanLimitGivenRepayments
                    TotalMaxLoanLimitGivenRepaymentsSTL = .TotalMaxLoanLimitGivenRepaymentsSTL
                    CurrentRisk = .CurrentRisk
                    MaxLoan = .MaxLoan
                    MaxRepaymentForLoanFixed = .MaxRepaymentForLoanFixed
                    MaxRepaymentForLoanFixedSTL = .MaxRepaymentForLoanFixedSTL
                    TotalIncomeAfterRatio = .TotalIncomeAfterRatio
                    HighestIncomeSelected = .HighestIncomeSelected
                    HighestIncomeSelectedSTL = .HighestIncomeSelectedSTL
                    ShareBalanceAfterSTL = .ShareBalanceAfterSTL
                    CurrentLoanBalanceAfterSTL = .CurrentLoanBalanceAfterSTL
                    STLReductionAmount = .STLReductionAmount

                    'No Share To Loan Result
                    NoSTLLoanEntitlement = .NoSTLLoanEntitlement
                    NoSTLTotalLoanBalance = .NoSTLTotalLoanBalance
                    NoSTLRepaymentAmount = .NoSTLRepaymentAmount
                    NoSTLNewLoanTerm = .NoSTLNewLoanTerm
                    NoSTLLoanEntitlementPreRisk = .NoSTLLoanEntitlementPreRisk
                    NoSTLPreRiskLoanBalance = .NoSTLPreRiskLoanBalance
                    NoSTLHigherThanMaxReduceFigure = .NoSTLHigherThanMaxReduceFigure

                    'Yes Share To Loan Result
                    YesSTLLoanEntitlement = .YesSTLLoanEntitlement
                    YesSTLTotalLoanBalance = .YesSTLTotalLoanBalance
                    YesSTLRepaymentAmount = .YesSTLRepaymentAmount
                    YesSTLNewLoanTerm = .YesSTLNewLoanTerm
                    YesSTLLoanEntitlementPreRisk = .YesSTLLoanEntitlementPreRisk
                    YesSTLPreRiskLoanBalance = .YesSTLPreRiskLoanBalance
                    YesSTLHigherThanMaxReduceFigure = .YesSTLHigherThanMaxReduceFigure
                    YesSTLRequestedAmountFinalTerm = .YesSTLRequestedAmountFinalTerm
                    YesSTLRequestedAmountFinalRepayment = .YesSTLRequestedAmountFinalRepayment
                    DecisionCalcsString = .ToString

                    NoSTLRequestedAmountTotalLoanBalance = .NoSTLRequestedAmountTotalLoanBalance
                    NoSTLRequestedAmountFinalTerm = .NoSTLRequestedAmountFinalTerm
                    NoSTLRequestedAmountFinalRepayment = .NoSTLRequestedAmountFinalRepayment
                    YesSTLRequestedAmountTotalLoanBalance = .YesSTLRequestedAmountTotalLoanBalance
                End With
            End If

            da.InsertUpdateApplication(AutoLoanID, UserID, LoanType.ToString, ApplyMethod.ToString, MemberID, LoanAmount, Level, CInt(ApplicationStatus), LoanOutcomeFlag, FinalLoanAmount, FinalLoanTerm, FinalMonthlyPayment _
                                       , oneThirdRuleStatus, shareToLoanRuleStatus, payDayRuleStatus _
                                       , oneThirdRuleStr, shareToLoanRuleStr, DecisionCalcsString, payDayRuleStr _
                                        , productCode, currentRepayment, shareBalance, TotalLoansOutstanding + currentBalance, CInt(RiskLimit), TotalIncome _
                                        , TotalMaxLoanLimitGivenRepayments, TotalMaxLoanLimitGivenRepaymentsSTL, CurrentRisk, MaxLoan, MaxRepaymentForLoanFixed _
                                       , MaxRepaymentForLoanFixedSTL, TotalIncomeAfterRatio, HighestIncomeSelected, HighestIncomeSelectedSTL, ShareBalanceAfterSTL _
                                       , CurrentLoanBalanceAfterSTL, STLReductionAmount, NoSTLLoanEntitlement, NoSTLTotalLoanBalance, NoSTLRepaymentAmount, NoSTLNewLoanTerm _
                                       , NoSTLLoanEntitlementPreRisk, NoSTLPreRiskLoanBalance, NoSTLHigherThanMaxReduceFigure, YesSTLLoanEntitlement, YesSTLTotalLoanBalance _
                                       , YesSTLRepaymentAmount, YesSTLNewLoanTerm, YesSTLLoanEntitlementPreRisk, YesSTLPreRiskLoanBalance, YesSTLHigherThanMaxReduceFigure _
                                       , YesSTLRequestedAmountFinalTerm, YesSTLRequestedAmountFinalRepayment, ShareToLoanAccepted, AppealReason, PaymentMethod, AccountNumber, SortCode, SMSSecurityCode, NextPayDay _
                                       , FinalOldLoanAmount, FinalOldLoanTerm, FinalOldMonthlyPayment, NoSTLRequestedAmountTotalLoanBalance, NoSTLRequestedAmountFinalTerm, NoSTLRequestedAmountFinalRepayment, YesSTLRequestedAmountTotalLoanBalance)


        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Load the model from the database
    ''' </summary>
    Public Sub LoadModel()
        Try
            Dim da As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
            Dim modelDetails As DataTable = da.GetAutoLoanDetail(AutoLoanID)
            If modelDetails IsNot Nothing AndAlso modelDetails.Rows.Count > 0 Then

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function IsCUCA() As Boolean
        Return SortCode = "089401" OrElse SortCode = "089409"
    End Function
End Class
