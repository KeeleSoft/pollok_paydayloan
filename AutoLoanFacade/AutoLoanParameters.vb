﻿Public Class AutoLoanParameters
    Public Property IncomeToLoanRatio As Integer
    Public Property InterestRate As Double
    Public Property InterestRateOld As Double
    Public Property DefaultLoanTermMonths As Integer
    Public Property ShareToLoanMinAmount As Double
    Public Property MaxAllowedPDLAmount As Double
    Public Property MaxAllowedRevLoanAmount As Double
    Public Property CUCADefaultSaving As Double
    Public Property FasterPaymentFee As Double
    Public Property MaxAllowedODLoanAmount As Double
End Class
