﻿Public Class BacsLoan2Application
    Public Property BacsLoanID As Integer
    Public Property MemberID As Integer
    Public Property AmountApplied As Double
    Public Property ProductCode As String
    Public Property ApplicationStatus As Integer
    Public Property ApplicationReceivedDate As DateTime
    Public Property ApplicationDueDate As DateTime
    Public Property AmountApproved As Double
    Public Property AmountOffered As Double
    Public Property AmountShareToLoan As Double
    Public Property NewLoanAmount As Double
    Public Property NewLoanRepayment As Double
    Public Property NewTerm As Integer
    Public Property FinalTotalLoanAmount As Double
    Public Property FinalRepaymentAmount As Double
    Public Property FinalTerm As Integer
    Public Property TotalRisk As Double
    Public Property PaymentMethod As Integer
    Public Property AccountNumber As String
    Public Property SortCode As String
    Public Property ApprovedByStaffID As Integer
    Public Property ApprovedByStaffID2 As Integer
    Public Property TotalDeduction As Double
    Public Property RepaymentMethod As Integer
    Public Property UserID As Integer
    Public Property AllowCreditCheck As Boolean

    'Additional Properties
    Public Property MemberName As String
    Public Property Mobile As String
    Public Property Email As String
    Public Property StaffName As String

    Public Sub SaveApplication()
        Dim blDataAccess As PollokCU.DataAccess.Layer.clsBacsLoan2 = New PollokCU.DataAccess.Layer.clsBacsLoan2
        blDataAccess.InsertUpdateApplication(BacsLoanID, UserID, MemberID, AmountApplied, ProductCode, ApplicationStatus, ApplicationReceivedDate, ApplicationDueDate, AmountApproved, AmountOffered _
                                             , AmountShareToLoan, NewLoanAmount, NewLoanRepayment, NewTerm _
                                             , FinalTotalLoanAmount, FinalRepaymentAmount, FinalTerm, TotalRisk, PaymentMethod, AccountNumber, SortCode _
                                             , ApprovedByStaffID, ApprovedByStaffID2, TotalDeduction, RepaymentMethod, AllowCreditCheck)
    End Sub
End Class
