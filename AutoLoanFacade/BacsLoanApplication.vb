﻿Public Class BacsLoanApplication
    Public Property BacsLoanID As Integer
    Public Property MemberID As Integer
    Public Property ProductCode As String
    Public Property ApplicationStatus As Integer '
    Public Property NewLoanAmount As Double
    Public Property NewLoanRepayment As Double
    Public Property NewTerm As Integer
    Public Property FinalTotalLoanAmount As Double
    Public Property FinalRepaymentAmount As Double
    Public Property FinalTerm As Integer
    Public Property TotalRisk As Double
    Public Property PaymentMethod As Integer
    Public Property RepaymentMethod As Integer
    Public Property TotalDeduction As Double
    Public Property AccountNumber As String
    Public Property CurrentAccountNumber As String
    Public Property SortCode As String
    Public Property ApprovedByStaffID As Integer
    Public Property ApprovedByStaffID2 As Integer
    Public Property AuthorisedByStaffID As Integer
    Public Property AuthorisedDateTime As DateTime
    Public Property UserID As Integer

    'Additional Properties
    Public Property MemberName As String
    Public Property MemberDOB As String
    Public Property ProductRate As Double
    Public Property CurrentProductBalance As Double
    Public Property ProductName As String

    Public Sub SaveApplication()
        Dim blDataAccess As PollokCU.DataAccess.Layer.clsBacsLoan = New PollokCU.DataAccess.Layer.clsBacsLoan
        blDataAccess.InsertUpdateApplication(BacsLoanID, UserID, MemberID, ProductCode, ApplicationStatus, NewLoanAmount, NewLoanRepayment, NewTerm _
                                             , FinalTotalLoanAmount, FinalRepaymentAmount, FinalTerm, TotalRisk, PaymentMethod, AccountNumber, SortCode _
                                             , ApprovedByStaffID, ApprovedByStaffID2, TotalDeduction, RepaymentMethod)
    End Sub
End Class
