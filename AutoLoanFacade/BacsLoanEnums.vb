﻿Public Class BacsLoanEnums
    Public Enum BacsLoanApplicationStatus
        Saved = 0
        WaitingForAgreement = 1
        Authorised = 2
        AgreementRecieved = 3
    End Enum

    Public Enum BacsLoanPaymentMethod
        ExternalBACS = 1
        FasterPayment = 2
        CUCABACS = 3
    End Enum

    Public Enum BacsLoanDocumentType
        BacsLoanAgreement = 1
    End Enum

    Public Enum BacsLoan2ApplicationStatus
        Verification = 1
        Unemployed = 2
        AwaitingContact = 3
        FrontCover = 8
        InfoRequest = 9
        AwaitingAgreement = 10
        RejectedMemInformed = 14
        SecondApprovalRequired = 15
        AwaitingPaymentForm = 16
        PaymentFormReceived = 17
    End Enum
End Class
