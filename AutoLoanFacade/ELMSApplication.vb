﻿Public Class ELMSApplication
    Public Property LambethID As Integer
    Public Property Title As String
    Public Property Firstname As String
    Public Property MiddleName As String
    Public Property Surname As String
    Public Property Gender As String
    Public Property AddressLine1 As String
    Public Property AddressLine2 As String
    Public Property City As String
    Public Property PostCode As String
    Public Property DateMovedToAddress As String
    Public Property ResidencyStatus As String
    Public Property HaveMortgage As String
    Public Property HomeTelephone As String
    Public Property Mobile As String
    Public Property Email As String
    Public Property DateOfBirth As String
    Public Property NINumber As String
    Public Property MaritalStatus As String
    Public Property NoOfDependants As String
    Public Property LoanAmount As Double

    Public Sub New()
    End Sub

    Public Sub New(ByVal appData As String)
        Dim appDataArray() As String = appData.Split(","c)
        If appDataArray.Length >= ELMSEnums.ELMSLambethDataIndex.LambethID Then
            Integer.TryParse(appDataArray(ELMSEnums.ELMSLambethDataIndex.LambethID), LambethID)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Title Then
            Title = appDataArray(ELMSEnums.ELMSLambethDataIndex.Title)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.FirstName Then
            Firstname = appDataArray(ELMSEnums.ELMSLambethDataIndex.FirstName)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.MiddleName Then
            MiddleName = appDataArray(ELMSEnums.ELMSLambethDataIndex.MiddleName)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Surname Then
            Surname = appDataArray(ELMSEnums.ELMSLambethDataIndex.Surname)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Gender Then
            Gender = appDataArray(ELMSEnums.ELMSLambethDataIndex.Gender)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.AddressLine1 Then
            AddressLine1 = appDataArray(ELMSEnums.ELMSLambethDataIndex.AddressLine1)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.AddressLine2 Then
            AddressLine2 = appDataArray(ELMSEnums.ELMSLambethDataIndex.AddressLine2)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.City Then
            City = appDataArray(ELMSEnums.ELMSLambethDataIndex.City)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.PostCode Then
            PostCode = appDataArray(ELMSEnums.ELMSLambethDataIndex.PostCode)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.DateMovedToAddress Then
            DateMovedToAddress = appDataArray(ELMSEnums.ELMSLambethDataIndex.DateMovedToAddress)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.ResidencyStatus Then
            ResidencyStatus = appDataArray(ELMSEnums.ELMSLambethDataIndex.ResidencyStatus)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.HaveMortgage Then
            HaveMortgage = appDataArray(ELMSEnums.ELMSLambethDataIndex.HaveMortgage)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.HomeTelephone Then
            HomeTelephone = appDataArray(ELMSEnums.ELMSLambethDataIndex.HomeTelephone)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Mobile Then
            Mobile = appDataArray(ELMSEnums.ELMSLambethDataIndex.Mobile)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Email Then
            Email = appDataArray(ELMSEnums.ELMSLambethDataIndex.Email)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.DOB Then
            DateOfBirth = appDataArray(ELMSEnums.ELMSLambethDataIndex.DOB)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.NINumber Then
            NINumber = appDataArray(ELMSEnums.ELMSLambethDataIndex.NINumber)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.MaritalStatus Then
            MaritalStatus = appDataArray(ELMSEnums.ELMSLambethDataIndex.MaritalStatus)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.Dependants Then
            NoOfDependants = appDataArray(ELMSEnums.ELMSLambethDataIndex.Dependants)
        End If
        If appDataArray.Length > ELMSEnums.ELMSLambethDataIndex.LoanAmount Then
            Double.TryParse(appDataArray(ELMSEnums.ELMSLambethDataIndex.LoanAmount), LoanAmount)
        End If
    End Sub

    Public Overrides Function ToString() As String
        Dim returnData As String = "LambethID=" & LambethID & ", Title=" & Title & ", Firstname=" & Firstname & ", MiddleName=" & MiddleName & _
            ", Surname=" & Surname & ", Gender=" & Gender & ", AddressLine1=" & AddressLine1 & ", AddressLine2=" & AddressLine1 & _
            ",City=" & City & ", PostCode=" & PostCode & ", DateMovedToAddress=" & DateMovedToAddress & ", ResidencyStatus=" & ResidencyStatus & _
            ", HaveMortgage=" & HaveMortgage & ", HomeTelephone=" & HomeTelephone & ", Mobile=" & Mobile & ", Email=" & Email & ", DateOfBirth=" & DateOfBirth & _
            ", NINumber=" & NINumber & ", MaritalStatus=" & MaritalStatus & ", NoOfDependants=" & NoOfDependants & ", LoanAmount=" & LoanAmount
        Return returnData
    End Function
End Class
