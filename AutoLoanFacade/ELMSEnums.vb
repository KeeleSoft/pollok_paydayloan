﻿Public Class ELMSEnums
    Public Enum ELMSApplicationStatus
        ApplicationRecieved = 1
        MovedToELMS = 2
        MovedToMSRIssuing = 3
        Paid = 4
    End Enum

    Public Enum ELMSDocumentType
        WelcomeLetter = 1
        ELMSAgreement = 2
        RevLoanAgreement = 3
    End Enum

    Public Enum ELMSLambethDataIndex
        LambethID = 0
        Title = 1
        FirstName = 2
        MiddleName = 3
        Surname = 4
        Gender = 5
        AddressLine1 = 6
        AddressLine2 = 7
        City = 8
        PostCode = 9
        DateMovedToAddress = 10
        ResidencyStatus = 11
        HaveMortgage = 12
        HomeTelephone = 13
        Mobile = 14
        Email = 15
        DOB = 16
        NINumber = 17
        MaritalStatus = 18
        Dependants = 19
        LoanAmount = 20
    End Enum
End Class
