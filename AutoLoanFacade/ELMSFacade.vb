﻿Public Class ELMSFacade
    Dim elmsData As PollokCU.DataAccess.Layer.clsELMS = New PollokCU.DataAccess.Layer.clsELMS


    ''' <summary>
    ''' Get ELMS Parameters
    ''' </summary>
    Public Sub GetELMSParameters(ByRef model As ELMSModel)
        Try
            Dim params As ELMSParameters = Nothing
            If model IsNot Nothing Then
                Dim dt As DataTable = elmsData.GetELMSParameters()
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    params = New ELMSParameters
                    With dt.Rows(0)
                        params.MaximumLoanAmount = CInt(.Item("MaximumLoanAmount"))
                        params.InterestRate = CDbl(.Item("InterestRate"))
                    End With
                End If
            End If
            model.ElmsParams = params
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class
