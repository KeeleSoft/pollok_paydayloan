﻿Public Class EmailBodyHelper
    Shared Function GetCUCAPDLALPSEmailBody(model As AutoLoanModel) As String
        Dim emailBody As String = ""

        emailBody = "MemberID: " & model.MemberID.ToString & vbNewLine & _
                    "ALPS ID:" & model.AutoLoanID.ToString & vbNewLine & _
                    "Loan Term - Month(s)):" & model.FinalLoanTerm.ToString & vbNewLine & _
                    "Monthly payment:" & FormatNumber(model.FinalMonthlyPayment, 2)

        Return emailBody
    End Function

    Shared Function GetPDLShare2LoanEmailBody(model As AutoLoanModel) As String
        Dim emailBody As String = ""

        emailBody = "Payday Loan granted automatically as there is enough money in the PDLShare account to clear the loan. Please make a Share to Loan to clear the payday loan" & vbNewLine & vbNewLine & _
                    "MemberID: " & model.MemberID.ToString & vbNewLine & _
                    "PDL Repeat ID:" & model.AutoLoanID.ToString & vbNewLine
                    

        Return emailBody
    End Function

    Shared Function GetSettleFromLastSagePaymentEmailBody(ByVal model As AutoLoanModel) As String
        Dim emailBody As String = ""

        emailBody = "Sage Payment collected successfully on " & model.PayDayLoanRule.LastSagePayDate & " for £" & model.PayDayLoanRule.LastSagePayment.ToString & ". Please clear the loan accordingly. As a result a new Payday loan has been granted automatically" & vbNewLine & vbNewLine & _
                    "MemberID: " & model.MemberID.ToString & vbNewLine & _
                    "PDL Repeat ID:" & model.AutoLoanID.ToString & vbNewLine


        Return emailBody
    End Function
End Class
