﻿Public Class LoanEntitlementRuleData
    Public Property MemberID As Integer
    Public Property LoanEntitlement As Double
    Public Property Comment As String
    Public Property LoanLimit As Double
    Public Property TotalLoansOutstanding As Double
    Public Property ShareBalance As Double
    Public Property TotalRisk As Double
    Public Property FutureRepaymentsAmount As Double
    Public Property LoanRepayAmount As Double
    Public Property NextRepayDate As Date
    Public Property LoanEntitlementRulePassed As Boolean

    ''' <summary>
    ''' Loan entitlement is rounded up to next 25
    ''' </summary>
    Public Property LoanEntitlementRounded As Double
        
    Public Overrides Function ToString() As String
        Dim value As String = "Loan Entitlement Amount: " & LoanEntitlement.ToString
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", Loan entitlement rule passed: " & _LoanEntitlementRulePassed.ToString
        value = value & ", Loan Limit (UDF5): " & _LoanLimit.ToString
        value = value & ", Total Loans Outstanding: " & _TotalLoansOutstanding.ToString
        value = value & ", Share Balance: " & _ShareBalance.ToString
        value = value & ", Total Risk: " & _TotalRisk.ToString
        value = value & ", Future Repayments Amount: " & _FutureRepaymentsAmount.ToString
        value = value & ", LoanRepayAmount: " & _LoanRepayAmount.ToString
        value = value & ", NextRepayDate: " & _NextRepayDate.ToShortDateString
        value = value & ", MemberID: " & _MemberID.ToString

        Return value
    End Function
End Class
