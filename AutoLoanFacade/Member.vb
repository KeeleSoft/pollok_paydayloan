﻿Public Class Member

    Public MemberID As Integer
    Public DateOfBirth As Date
    Public Email As String
    Public FirstName As String
    Public Surname As String
    Public MobileNumber As String
    Public Accountstatus As String
    Public PhoneNumberCount As Integer
    Public AddressLine1 As String
    Public City As String
    Public County As String
    Public PostCode As String

    ''' <summary>
    ''' TODO: Waiting for the revised list from lucky till do this member status check
    ''' </summary>
    Public ReadOnly Property AllowProceedALPSForStatus() As Boolean
        Get
            Return AllowAccountStatusForLoan(Accountstatus.ToUpper)
        End Get
    End Property

    Public Shared Function AllowAccountStatusForLoan(ByVal accountstatus As String) As Boolean
        Select Case accountstatus.ToUpper
            Case "A", "E", "M", "H", "I", "K", "L"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public ReadOnly Property Age As Long
        Get
            Return DateDiff(DateInterval.Year, DateOfBirth, Now())
        End Get
    End Property
End Class
