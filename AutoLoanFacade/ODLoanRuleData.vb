﻿Public Class ODLoanRuleData
    Public Property MemberID As Integer
    Public Property ODLoanRulePassed As Boolean
    Public Property Comment As String
    Public Property LastODLoanDate As Date
    Public Property LastODLoanAmount As Double
    Public Property CurrentODLoanBalance As Double
    Public Property MonthlyRepayment As Double
    Public Property ODLoanRiskLimit As Integer
    Public Property LastPaidSortCode As String
    Public Property LastPaidAccNumber As String
    Public Property LastODLoanApplicationID As Integer
    Public Property LastSagePayResponseID As Integer
    Public Property Last4Digits As Integer
    Public Property IsLastODLoanBACS As Boolean

    Public Overrides Function ToString() As String
        Dim value As String = "OD Loan Rule Passed: " & If(_ODLoanRulePassed, "Yes", "No")
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", Last OD Loan Date: " & _LastODLoanDate.ToShortDateString
        value = value & ", Last OD Loan Amount: " & _LastODLoanAmount.ToString
        value = value & ", Current OD Loan Balance: " & _CurrentODLoanBalance.ToString
        value = value & ", Monthly repayment: " & _MonthlyRepayment.ToString
        value = value & ", OD Loan Risk Limit: " & _ODLoanRiskLimit.ToString
        value = value & ", MemberID: " & _MemberID.ToString

        Return value
    End Function
End Class
