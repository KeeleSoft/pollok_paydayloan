﻿Public Class OneThirdRuleData
    Public Property MemberID As Integer
    Public Property OneThirdRulePassed As Boolean
    Public Property Comment As String
    Public Property LastLoanDate As Date
    Public Property LastLoanAmount As Double
    Public Property LastLoanBalance As Double
    Public Property CurrentBalance As Double
    Public Property LastLoanProductCode As String
    Public Property CurrentRepayAmount As Double
    Public Property OneThirdAmount As Double
    Public Property MonthsLeftToSettle As Integer

    Public Overrides Function ToString() As String
        Dim value As String = "One Third Rule Passed: " & If(_OneThirdRulePassed, "Yes", "No")
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", LastLoanProductCode: " & _LastLoanProductCode.ToString
        value = value & ", LastLoanDate: " & _LastLoanDate.ToShortDateString
        value = value & ", LastLoanAmount: " & _LastLoanAmount.ToString
        value = value & ", LastLoanBalance: " & _LastLoanBalance.ToString
        value = value & ", LastLoanCurrentBalance: " & _CurrentBalance.ToString
        value = value & ", LastLoanRepayAmount: " & _CurrentRepayAmount.ToString
        value = value & ", OneThirdAmount: " & _OneThirdAmount.ToString
        value = value & ", MonthsLeftToSettle: " & _MonthsLeftToSettle.ToString
        value = value & ", MemberID: " & _MemberID.ToString

        Return value
    End Function
End Class
