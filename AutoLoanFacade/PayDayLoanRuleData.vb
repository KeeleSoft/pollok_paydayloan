﻿Public Class PayDayLoanRuleData
    Public Property MemberID As Integer
    Public Property PayDayLoanRulePassed As Boolean
    Public Property Comment As String
    Public Property LastPDLLoanDate As Date
    Public Property LastPDLLoanAmount As Double
    Public Property CurrentPDLLoanBalance As Double
    Public Property MonthlyRepayment As Double
    Public Property PDLRiskLimit As Integer
    Public Property LastPaidSortCode As String
    Public Property LastPaidAccNumber As String
    Public Property LastPDLApplicationID As Integer
    Public Property LastSagePayResponseID As Integer
    Public Property Last4Digits As Integer
    Public Property IsLastPDLBACS As Boolean

    Public Property PDLShareBalance As Double
    Public Property PDLShareToLoanRequired As Boolean
    Public Property SettleFromLastSagePayment As Boolean
    Public Property LastSagePayment As Double
    Public Property LoanFound As Boolean
    Public Property LastSagePayDate As String

    Public Overrides Function ToString() As String
        Dim value As String = "Pay Day Loan Rule Passed: " & If(_PayDayLoanRulePassed, "Yes", "No")
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", Last PDL Loan Date: " & _LastPDLLoanDate.ToShortDateString
        value = value & ", Last PDL Loan Amount: " & _LastPDLLoanAmount.ToString
        value = value & ", Current PDL Loan Balance: " & _CurrentPDLLoanBalance.ToString
        value = value & ", Monthly repayment: " & _MonthlyRepayment.ToString
        value = value & ", PDL Risk Limit: " & _PDLRiskLimit.ToString
        value = value & ", MemberID: " & _MemberID.ToString
        value = value & ", PDLShareBalance: " & _PDLShareBalance.ToString
        value = value & ", PDLShareToLoanRequired: " & If(_PDLShareToLoanRequired, "Yes", "No")
        value = value & ", SettleFromLastSagePayment: " & If(_SettleFromLastSagePayment, "Yes", "No")
        value = value & ", LoanFound: " & If(_LoanFound, "Yes", "No")
        value = value & ", LastSagePayDate: " & _LastSagePayDate
        Return value
    End Function
End Class
