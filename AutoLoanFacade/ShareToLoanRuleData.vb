﻿Public Class ShareToLoanRuleData
    Public Property MemberID As Integer
    Public Property ShareToLoanRulePassed As Boolean
    Public Property Comment As String
    Public Property Share1Balance As Double

    Public Overrides Function ToString() As String
        Dim value As String = "Share To Loan Rule Passed: " & If(_ShareToLoanRulePassed, "Yes", "No")
        If _Comment.Length > 0 Then value = value & ", Comment: " & _Comment
        value = value & ", Share1 Balance: " & _Share1Balance.ToString
        value = value & ", MemberID: " & _MemberID.ToString

        Return value
    End Function
End Class
