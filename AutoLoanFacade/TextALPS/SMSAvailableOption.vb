﻿Public Class SMSAvailableOption
    Public Property AutoLoanID As Integer
    Public Property MemberID As Integer
    Public Property MobileNumber As String
    Public Property Option1Amount As Double
    Public Property Option1Term As Integer
    Public Property Option1MonthlyPayment As Double
    Public Property Option2Amount As Double
    Public Property Option2Term As Integer
    Public Property Option2MonthlyPayment As Double
    Public Property Option3Amount As Double
    Public Property Option3Term As Integer
    Public Property Option3MonthlyPayment As Double
    Public Property Option3ShareToLoan As Double

    Public Property OldLoanAmount1 As Double
    Public Property OldLoanTerm1 As Integer
    Public Property OldLoanMonthlyPayment1 As Double
    Public Property OldLoanAmount2 As Double
    Public Property OldLoanTerm2 As Integer
    Public Property OldLoanMonthlyPayment2 As Double
    Public Property OldLoanAmount3 As Double
    Public Property OldLoanTerm3 As Integer
    Public Property OldLoanMonthlyPayment3 As Double

    Public Property LastProductCode As String
End Class
