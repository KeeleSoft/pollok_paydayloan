﻿Public Class SMSConstants
    Public Const YES As String = "yes"
    Public Const NO As String = "no"

    Public Const OPTION_LoanAgreedNoConditions As String = "1"
    Public Const OPTION_NoSTLOptionSelected As String = "2"
    Public Const OPTION_YesSTLOptionSelected As String = "3"
    Public Const OPTION_Appealed As String = "4"
    Public Const OPTION_Refused As String = "5"
End Class
