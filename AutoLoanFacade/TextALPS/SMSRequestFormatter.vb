﻿Public Class SMSRequestFormatter
    Public Property LoanType As AutoLoanEnums.AutoLoanType
    Public Property OptionText As String 'This could be amount, option want to choose, yes\no, etc

#Region "Inbound Message"
    Public Property InboundMessageID As String
    Public Property InboundMessageAccountID As String
    Public Property MessageText As String
    Public Property MobileNumber As String
    Public Property InboundMessageTo As String
#End Region
End Class
