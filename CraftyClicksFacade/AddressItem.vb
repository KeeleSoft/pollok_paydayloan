﻿Imports System.Xml.Serialization
Public Class AddressItem
    <XmlElementAttribute(ElementName:="line_1")> _
    Public Property AddressLine1 As String

    <XmlElementAttribute(ElementName:="line_2")> _
    Public Property AddressLine2 As String

    <XmlElementAttribute(ElementName:="udprn")> _
    Public Property UDPRN As String

    Public ReadOnly Property AddressLine1Formatted As String
        Get
            If RTrim(AddressLine1).Substring(RTrim(AddressLine1).Length - 1, 1) = "," Then
                Return Left(RTrim(AddressLine1), RTrim(AddressLine1).Length - 1)
            Else
                Return AddressLine1
            End If
        End Get
    End Property
End Class
