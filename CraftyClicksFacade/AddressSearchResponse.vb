﻿Imports System.Xml.Serialization
<XmlRoot(ElementName:="CraftyResponse")>
Public Class AddressSearchResponse
    <XmlElementAttribute(ElementName:="address_data_formatted")> _
    Public Property AddressSearchData As AddressSearchResult

    <XmlElementAttribute(ElementName:="error_code")> _
    Public Property ErrorCode As String

    <XmlElementAttribute(ElementName:="error_msg")> _
    Public Property ErrorMsg As String
End Class
