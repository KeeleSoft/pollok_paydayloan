﻿Imports System.Xml.Serialization
Public Class AddressSearchResult
    <XmlElementAttribute(ElementName:="delivery_point")> _
    Public Property AddressLines As List(Of AddressItem)

    <XmlElementAttribute(ElementName:="town")> _
    Public Property Town As String

    <XmlElementAttribute(ElementName:="postal_county")> _
    Public Property PostalCounty As String

    <XmlElementAttribute(ElementName:="traditional_county")> _
    Public Property TraditionalCounty As String

    <XmlElementAttribute(ElementName:="postcode")> _
    Public Property PostCode As String

    Public Function GetComboValue(ByVal addressLine As AddressItem) As String
        Return addressLine.AddressLine1Formatted & "@" & addressLine.AddressLine2 & "@" & Town & "@" & PostalCounty & "@" & PostCode
    End Function
End Class
