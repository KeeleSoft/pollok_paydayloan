﻿Imports System.Net
Imports System.Xml.Serialization
Imports System.IO

Public Class CraftyClicksService
    Implements IDisposable

    Private da As clsDataAccess = New clsDataAccess

    Public Property AddressLinesCount As Int16 = 1

    Public Function GetPostCodeSearchResults(ByVal postCode As String) As AddressSearchResponse
        Dim addresses As AddressSearchResponse = New AddressSearchResponse
        Dim xmlSer As New XmlSerializer(addresses.GetType)
        Dim request As HttpWebRequest = Nothing
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader = Nothing
        Dim token As String = "bb960-8d4c4-bf46a-00f4b"
        Dim url As String = "http://pcls1.craftyclicks.co.uk/xml/rapidaddress?postcode=" & postCode & "&response=data_formatted&lines=" & AddressLinesCount.ToString & "&key=" & token
        request = DirectCast(WebRequest.Create(url), HttpWebRequest)
        response = DirectCast(request.GetResponse(), HttpWebResponse)
        reader = New StreamReader(response.GetResponseStream())
        addresses = CType(xmlSer.Deserialize(reader), AddressSearchResponse)
        reader.Close()
        xmlSer = Nothing
        request = Nothing
        response = Nothing

        RecordPostCodeSearchTrackingInfo(postCode)
        Return addresses
    End Function

    Private Sub RecordPostCodeSearchTrackingInfo(ByVal postCode As String)
        Try
            da.InsertTrackingServiceStat(1, 0, "", postCode, 0)
        Catch ex As Exception
            da.InsertErrorLog(ex, "")
        End Try
    End Sub
#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
