﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsSystemData.vb
'''********************************************************************
''' <Summary>
'''	This module calls lookup data, SystemKey and Command Centre related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 18/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsDataAccess
    Dim clsConn As clsConnection = New clsConnection
    Dim objCon As SqlConnection



#Region "Tracking Data"
    Public Function InsertTrackingServiceStat(ByVal TrackingServiceCategoryID As Integer _
                                        , ByVal MemberID As Integer _
                                        , ByVal IPAddress As String _
                                        , ByVal TrackingData As String _
                                        , ByVal UserID As Integer _
                                        ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_TRACKING_SERVICE_STAT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@TrackingServiceCategoryID", SqlDbType.Int)
            cmdCommand.Parameters("@TrackingServiceCategoryID").Value = TrackingServiceCategoryID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@IPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@IPAddress").Value = IPAddress

            cmdCommand.Parameters.Add("@TrackingData", SqlDbType.VarChar)
            cmdCommand.Parameters("@TrackingData").Value = TrackingData

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery
            InsertTrackingServiceStat = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Error Login"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Log an Error
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  04/11/2011  Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertErrorLog(ByVal ex As Exception, ByVal IPAddress As String, Optional ExtraData As String = "") As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.INSERT_ERROR_LOG", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ErrMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrMessage").Value = ex.Message & "-" & ExtraData

            cmdCommand.Parameters.Add("@StackTrace", SqlDbType.VarChar)
            cmdCommand.Parameters("@StackTrace").Value = ex.StackTrace

            cmdCommand.Parameters.Add("@CreatedIPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreatedIPAddress").Value = IPAddress

            If ex.Message <> "Thread was being aborted." Then
                intResult = cmdCommand.ExecuteNonQuery
            End If
            InsertErrorLog = intResult
        Catch ex2 As Exception
            Throw New ApplicationException("Failed to insert the error" & vbCrLf & ex2.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region


End Class
