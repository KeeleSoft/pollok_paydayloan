﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EquifaxFacade.ConsumerService1;
using System.Xml.Serialization;

namespace EquifaxFacade
{
    public class Class1
    {
         static string username = "LMCUUAT2@LMCUXML2UAT2";
         static string password = "Passw0rd";

        public void testConsumerService()
        {
            // Build a credit search request…
            ConsumerService1.creditSearchRequest creditSearchRequest = new ConsumerService1.creditSearchRequest();
            creditSearchRequest.clientRef = "C_SHARP_TEST";

            // Configure a single applicant enquiry…
            ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();
            ConsumerService1.CreditSearchConfig config = new ConsumerService1.CreditSearchConfig();
            config.optIn = true;
            rss.creditSearchConfig = config;

            ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
            mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.subject = ConsumerService1.MatchCriteriaStatus.required;
            rss.matchCriteria = mc;

            // Add requested data… (ADO)
            ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
            ConsumerService1.OutputAddressRequest oar = new ConsumerService1.OutputAddressRequest();
            oar.maxNumber = 0;
            rd.outputAddressRequest = oar;
            rss.requestedData = rd;

            // Set up the single applicant person…
            ConsumerService1.RequestPerson person = new ConsumerService1.RequestPerson();
            ConsumerService1.Name name = new ConsumerService1.Name();
            name.forename = "DAVID";
            name.surname = "BECKHAM";
            person.name = name;

            // Add an address for the applicant…
            ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
            ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();
            rsa.number = "26";
            rsa.postcode = "CB6 2JR";
            residence.address = rsa;
            person.currentAddress = residence;
            rss.primary = person;

            // Finalise the credit search request…
            creditSearchRequest.soleSearch = rss;

            // Create a service client…
            using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
            {

                //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());

                // Add WS-Security header credentials…
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                // Create a response in advance which will be passed by reference and modified in the service call…
                ConsumerService1.creditSearchResponse creditSearchResponse = new ConsumerService1.creditSearchResponse();

                // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                ConsumerService1.PasswordExpiryInformation cpFlag = client.creditSearch(null, creditSearchRequest, out creditSearchResponse);

                // Output results
                //saveXml(creditSearchRequest, "consumerRequest");
                //saveXml(creditSearchResponse, "consumerResponse");
            }
        }

       
        public void VerifyIdentityCheck()
        {
            // Build a credit search request…
            ConsumerService1.verifyIdentityCheckRequest VerifyIdentityCheckRequest = new verifyIdentityCheckRequest();
            VerifyIdentityCheckRequest.clientRef = "C_SHARP_TEST";

            // Configure a single applicant enquiry…
            ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();
            //ConsumerService1.CreditSearchConfig config = new ConsumerService1.CreditSearchConfig();
            //config.optIn = true;
            //rss.creditSearchConfig = config;

            ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
            mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.subject = ConsumerService1.MatchCriteriaStatus.required;
            rss.matchCriteria = mc;

            // Add requested data… (ADO)
            ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
            ConsumerService1.OutputAddressRequest oar = new ConsumerService1.OutputAddressRequest();
            oar.maxNumber = 0;
            rd.outputAddressRequest = oar;
            rss.requestedData = rd;

            // Set up the single applicant person…
            ConsumerService1.RequestPerson person = new ConsumerService1.RequestPerson();
            ConsumerService1.Name name = new ConsumerService1.Name();
            name.forename = "DAVID";
            name.surname = "BECKHAM";
            person.name = name;

            // Add an address for the applicant…
            ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
            ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();
            rsa.number = "26";
            rsa.postcode = "CB6 2JR";
            residence.address = rsa;
            person.currentAddress = residence;
            rss.primary = person;

            // Finalise the credit search request…
            VerifyIdentityCheckRequest.soleSearch = rss;

            // Create a service client…
            using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
            {

                //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());

                // Add WS-Security header credentials…
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                // Create a response in advance which will be passed by reference and modified in the service call…
                ConsumerService1.verifyIdentityCheckResponse VerifyIdentityCheckResponse = new ConsumerService1.verifyIdentityCheckResponse();

                // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                ConsumerService1.PasswordExpiryInformation cpFlag = client.verifyIdentityCheck(null, VerifyIdentityCheckRequest, out VerifyIdentityCheckResponse);

                // Output results
                //saveXml(creditSearchRequest, "consumerRequest");
                //saveXml(creditSearchResponse, "consumerResponse");
            }
         }
       }
    }

