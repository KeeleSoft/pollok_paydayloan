﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquifaxFacade
{
  public class ConsumerResults
    {
        clsEquifax objData = new clsEquifax();
       
        public void SaveIdCheck(int ApplicationID, ConsumerService1.verifyIdentityCheckResponse output, string ApplicationType = "PDL")
        {
            int appid = ApplicationID;
            string apptype = ApplicationType;
            Int32 memberid = 333;
            try
            {
                string ClientRef = "";
                string AddressStatus = "";
                int index = 0;
                Boolean Present;
                string County = "";
                string district = "";
                string number = "";
                string postcode = "";
                string postTown = "";
                string street1 = "";
                string addressID = "";
                                
                ClientRef = output.clientRef;
                AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                Present = output.soleSearch.primary.suppliedAddressData[0].noticeOfCorrectionOrDisputePresent;
                County = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.county;
                district = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.district;
                number = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.number;
                postcode = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.postcode;
                postTown = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.postTown;
                street1 = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.street1;
                addressID = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.addressID;


                objData.InsertData(appid, apptype, memberid, ClientRef, AddressStatus, index, Present, County, district, number, postcode, postTown,
                 street1, addressID);

            }
            catch (Exception ex)
            {
                objData.InsertErrorLog(ex, "", ApplicationID.ToString());

            }
        }

        public void SavePasswordChange(String UserName, String Password, SecurityService1.changePasswordResponse output)
        {
            string message = "";

            try
            {
                if (output.message != null)
                {
                    message = output.message.ToString();
                    if (message.Equals("success"))
                    {
                        objData.UpdatePassword(UserName, Password);
                    }
                }

            }
            catch (Exception ex)
            {
                objData.InsertErrorLog(ex, "", "Password Change Not Successfull");
            }

        }


        public int SaveBankCheck(int ApplicationID, ConsumerService1.verifyIdentityCheckResponse output, string ApplicationType, Int32 memid)
        {
            int appid = ApplicationID;
            string apptype = ApplicationType;
            bool ispassed=false;
            try
            {
                string ClientRef = "";
                string QSC032 = "";
                string QSC034 = "";
                string QSP032 = "";
                string QSP034 = "";
                string QSE032 = "";
                string QSE034 = "";
                string QSN032 = "";
                string QSN034 = "";
                string QSC031 = "";
                string QSC033 = "";
                string QSC035 = "";
                string QSP030 = "";
                string QSP031 = "";
                string QSP033 = "";
                string QSP035 = "";
                string QSE030 = "";
                string QSE031 = "";
                string QSE033 = "";
                string QSE035 = "";
                string QSN030 = "";
                string QSN031 = "";
                string QSN033 = "";
                string QSN035 = "";
                string QSC030 = "";
                string FBC1 = "";
                string FBC2 = "";
                string FBC5 = "";
                int Option = 0;
                string AddressStatus = "";
                int index = 0;
                Boolean Present = false;

                objData.InsertTrackingServiceStat(6, ApplicationID.ToString());

                ClientRef = output.clientRef;
                if (output.nonAddressSpecificData != null)
                {

                    QSC032 = output.nonAddressSpecificData.decodedCharacteristics.Items[0].ToString();
                    QSC034 = output.nonAddressSpecificData.decodedCharacteristics.Items[1].ToString();
                    QSP032 = output.nonAddressSpecificData.decodedCharacteristics.Items[2].ToString();
                    QSP034 = output.nonAddressSpecificData.decodedCharacteristics.Items[3].ToString();
                    QSE032 = output.nonAddressSpecificData.decodedCharacteristics.Items[4].ToString();
                    QSE034 = output.nonAddressSpecificData.decodedCharacteristics.Items[5].ToString();
                    QSN032 = output.nonAddressSpecificData.decodedCharacteristics.Items[6].ToString();
                    QSN034 = output.nonAddressSpecificData.decodedCharacteristics.Items[7].ToString();
                    QSC031 = output.nonAddressSpecificData.decodedCharacteristics.Items[8].ToString();
                    QSC033 = output.nonAddressSpecificData.decodedCharacteristics.Items[9].ToString();
                    QSC035 = output.nonAddressSpecificData.decodedCharacteristics.Items[10].ToString();
                    QSP030 = output.nonAddressSpecificData.decodedCharacteristics.Items[11].ToString();
                    QSP031 = output.nonAddressSpecificData.decodedCharacteristics.Items[12].ToString();
                    QSP033 = output.nonAddressSpecificData.decodedCharacteristics.Items[13].ToString();
                    QSP035 = output.nonAddressSpecificData.decodedCharacteristics.Items[14].ToString();
                    QSE030 = output.nonAddressSpecificData.decodedCharacteristics.Items[15].ToString();
                    QSE031 = output.nonAddressSpecificData.decodedCharacteristics.Items[16].ToString();
                    QSE033 = output.nonAddressSpecificData.decodedCharacteristics.Items[17].ToString();
                    QSE035 = output.nonAddressSpecificData.decodedCharacteristics.Items[18].ToString();
                    QSN030 = output.nonAddressSpecificData.decodedCharacteristics.Items[19].ToString();
                    QSN031 = output.nonAddressSpecificData.decodedCharacteristics.Items[20].ToString();
                    QSN033 = output.nonAddressSpecificData.decodedCharacteristics.Items[21].ToString();
                    QSN035 = output.nonAddressSpecificData.decodedCharacteristics.Items[22].ToString();
                    QSC030 = output.nonAddressSpecificData.decodedCharacteristics.Items[23].ToString();
                    FBC1 = ((System.Xml.XmlElement)output.nonAddressSpecificData.decodedCharacteristics.Items[24]).InnerText;
                    FBC2 = ((System.Xml.XmlElement)output.nonAddressSpecificData.decodedCharacteristics.Items[25]).InnerText;
                    FBC5 = ((System.Xml.XmlElement)output.nonAddressSpecificData.decodedCharacteristics.Items[26]).InnerText;

                    Option = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.option);
                    AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                    index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                    Present = output.soleSearch.primary.suppliedAddressData[0].noticeOfCorrectionOrDisputePresent;

                    ispassed = IsBankCheckPassed(FBC1, FBC2, FBC5);

                    objData.InsertBankData(appid, apptype, memid, ClientRef, QSC032, QSC034, QSP032, QSP034, QSE032, QSE034, QSN032, QSN034, QSC031, QSC033, QSC035, QSP030, QSP031, QSP033, QSP035, QSE030, QSE031, QSE033, QSE035, QSN030, QSN031, QSN033, QSN035, QSC030, FBC1, FBC2, FBC5, Option, AddressStatus, index, Present);
                }
                else
                {
                    if (output.soleSearch.primary.suppliedAddressData != null)
                    {
                        AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                        index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                        objData.InsertBankData(appid, apptype, memid, ClientRef, QSC032, QSC034, QSP032, QSP034, QSE032, QSE034, QSN032, QSN034, QSC031, QSC033, QSC035, QSP030, QSP031, QSP033, QSP035, QSE030, QSE031, QSE033, QSE035, QSN030, QSN031, QSN033, QSN035, QSC030, FBC1, FBC2, FBC5, Option, AddressStatus, index, Present);
                    }
                    
                }

            }
            catch (Exception ex)
            {                
                objData.InsertErrorLog(ex, "", ApplicationID.ToString());
                return 0;
            }

            if (ispassed)
            { return 1; }
            else
            { return 2; }
        }

        private bool IsBankCheckPassed(string FBC1, string FBC2, string FBC5)
        {
            if (FBC1 == "Y" && FBC2 == "Y" && FBC5 == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int SaveAMLCheck(int ApplicationID, ConsumerService1.antiMoneyLaunderingCheckResponse output, string ApplicationType, Int32 memid, int Years)
        {
            int appid = ApplicationID;
            string apptype = ApplicationType;
            int memberid = memid;
            bool ispassed = false;
            try
            {
                string ClientRef = "";
                string VRC101 = "";
                string ESC941 = "";
                string RSC8 = "";
                string QSC001 = "";
                string CSC903 = "";
                string FSC12 = "";
                string TSC1 = "";
                string QSC034 = "";
                string FSC3 = "";
                string NSC1 = "";
                string NSC2 = "";
                string HSC1 = "";
                string DSC1 = "";
                string DSC3 = "";
                string DSC4 = "";
                string DSC5 = "";
                string ASC8 = "";
                int VRC18=0;
                int VIC3=0;
                int VNC3=0;
                string VXC3 = "";
                int Option=0;
                string AddressStatus = "";
                int index = 0;
                Boolean Present=false;
                string County = "";
                string number = "";
                string postcode = "";
                string postTown = "";
                string street1 = "";
                string addressID = "";
                string PreviousAddressStatus = "";
                int Previousindex = 0;
                Boolean PreviousPresent = false;
                string PreviousCounty = "";
                string Previousnumber = "";
                string Previouspostcode = "";
                string PreviouspostTown = "";
                string Previousstreet1 = "";
                string PreviousaddressID = "";


                objData.InsertTrackingServiceStat(5, ApplicationID.ToString());
                ClientRef = output.clientRef;

                if (output.nonAddressSpecificData != null)
                {
                    if (output.nonAddressSpecificData.decodedCharacteristics.Items.Count() > 0)
                    {
                        VRC101 = output.nonAddressSpecificData.decodedCharacteristics.Items[0].ToString();
                        ESC941 = output.nonAddressSpecificData.decodedCharacteristics.Items[1].ToString();
                        RSC8 = output.nonAddressSpecificData.decodedCharacteristics.Items[2].ToString();
                        QSC001 = output.nonAddressSpecificData.decodedCharacteristics.Items[3].ToString();
                        CSC903 = output.nonAddressSpecificData.decodedCharacteristics.Items[4].ToString();
                        FSC12 = output.nonAddressSpecificData.decodedCharacteristics.Items[5].ToString();
                        TSC1 = output.nonAddressSpecificData.decodedCharacteristics.Items[6].ToString();
                        // QSC034 = output.nonAddressSpecificData.decodedCharacteristics.Items[7].ToString();
                        FSC3 = output.nonAddressSpecificData.decodedCharacteristics.Items[8].ToString();
                        NSC1 = output.nonAddressSpecificData.decodedCharacteristics.Items[9].ToString();
                        NSC2 = output.nonAddressSpecificData.decodedCharacteristics.Items[10].ToString();
                        HSC1 = output.nonAddressSpecificData.decodedCharacteristics.Items[11].ToString();
                        DSC1 = output.nonAddressSpecificData.decodedCharacteristics.Items[12].ToString();
                        DSC3 = output.nonAddressSpecificData.decodedCharacteristics.Items[13].ToString();
                        DSC4 = output.nonAddressSpecificData.decodedCharacteristics.Items[14].ToString();
                        DSC5 = output.nonAddressSpecificData.decodedCharacteristics.Items[15].ToString();
                        ASC8 = output.nonAddressSpecificData.decodedCharacteristics.Items[16].ToString();
                        VRC18 = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.Items[17]);
                        VIC3 = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.Items[20]);
                        VNC3 = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.Items[18]);
                        VXC3 = output.nonAddressSpecificData.decodedCharacteristics.Items[19].ToString();
                        Option = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.option);


                    }

                    if (output.soleSearch.primary.suppliedAddressData.Count() > 0)
                    {
                        AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                        index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                        Present = output.soleSearch.primary.suppliedAddressData[0].noticeOfCorrectionOrDisputePresent;
                        County = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.county;
                        number = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.number;
                        postcode = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.postcode;
                        postTown = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.postTown;
                        street1 = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.street1;
                        addressID = output.soleSearch.primary.suppliedAddressData[0].matchedAddress.address.addressID;


                        if (Years < 3)
                        {
                            PreviousAddressStatus = output.soleSearch.primary.suppliedAddressData[1].addressMatchStatus.ToString();
                            Previousindex = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[1].index);
                            PreviousPresent = output.soleSearch.primary.suppliedAddressData[1].noticeOfCorrectionOrDisputePresent;
                            PreviousCounty = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.county;
                            Previousnumber = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.number;
                            Previouspostcode = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.postcode;
                            PreviouspostTown = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.postTown;
                            Previousstreet1 = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.street1;
                            PreviousaddressID = output.soleSearch.primary.suppliedAddressData[1].matchedAddress.address.addressID;

                        }
                    }

                    ispassed = IsEquifaxIDCheckPassed(VRC18, VIC3, VNC3);

                    objData.InsertAMLData(appid, apptype, memberid, ClientRef, VRC101, ESC941, RSC8, QSC001, CSC903, FSC12, TSC1, QSC034, FSC3, NSC1, NSC2, HSC1, DSC1, DSC3, DSC4, DSC5, ASC8, VRC18, VIC3, VNC3, VXC3, Option, AddressStatus, index, Present, County, number, postcode, postTown,
                     street1, addressID, PreviousAddressStatus, Previousindex, PreviousPresent, PreviousCounty, Previousnumber, Previouspostcode, PreviouspostTown, Previousstreet1, PreviousaddressID);
                }
                else
                {
                    if (output.soleSearch.primary.suppliedAddressData != null)
                    {
                        AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                        index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                        objData.InsertAMLData(appid, apptype, memberid, ClientRef, VRC101, ESC941, RSC8, QSC001, CSC903, FSC12, TSC1, QSC034, FSC3, NSC1, NSC2, HSC1, DSC1, DSC3, DSC4, DSC5, ASC8, VRC18, VIC3, VNC3, VXC3, Option, AddressStatus, index, Present, County, number, postcode, postTown,
                         street1, addressID, PreviousAddressStatus, Previousindex, PreviousPresent, PreviousCounty, Previousnumber, Previouspostcode, PreviouspostTown, Previousstreet1, PreviousaddressID);
                    }
                    
                }


            }
            catch (Exception ex)
            {                
                objData.InsertErrorLog(ex, "", ApplicationID.ToString());
                return 0;
            }

            if (ispassed)
            { return 1; }
            else
            { return 2; }
        }

        public int SaveCreditCheck(int ApplicationID, ConsumerService1.creditSearchResponse output, string ApplicationType, Int32 memberid, int passScore, int Years)
        {
            int appid = ApplicationID;
            string apptype = ApplicationType;
            bool ispasssed=false;
            try
            {
                string ClientRef = "";
                string FTOLF04 = "";
                Boolean FTOLF04_positive = false;
                int FTOLF04_value = 0;
                string RNOLF04 = "";
                Boolean RNOLF04_positive = false;
                int RNOLF04_value = 0;
                string QSC032 = "";
                string QSC034 = "";
                string QSP032 = "";
                string QSP034 = "";
                string QSE032 = "";
                string QSE034 = "";
                string QSN032 = "";
                string QSN034 = "";
                string QSC031 = "";
                string QSC033 = "";
                string QSC035 = "";
                string QSP030 = "";
                string QSP031 = "";
                string QSP033 = "";
                string QSP035 = "";
                string QSE030 = "";
                string QSE031 = "";
                string QSE033 = "";
                string QSE035 = "";
                string QSN030 = "";
                string QSN031 = "";
                string QSN033 = "";
                string QSN035 = "";
                string QSC030 = "";
                string FBC1 = "";
                string FBC2 = "";
                string FBC5 = "";
                int Option = 0;

                string midname = "";
                string surname = "";
                string forename = "";
                string namematchstatus = "";
                string seniority = "";
                string start = "";
                string end = "";

                string AddressStatus = "";
                int index = 0;
                Boolean Present = false;
                string county = "";
                string number = "";
                string postcode = "";
                string posttown = "";
                string street1 = "";
                string addressid = "";

                int Score = 0;
                objData.InsertTrackingServiceStat(7, ApplicationID.ToString());
                ClientRef = output.clientRef;
                if (output.nonAddressSpecificData != null)
                {

                    FTOLF04 = output.nonAddressSpecificData.scores.score[0].scoreLabel;
                    FTOLF04_positive = output.nonAddressSpecificData.scores.score[0].positive;
                    FTOLF04_value = output.nonAddressSpecificData.scores.score[0].value;
                    RNOLF04 = output.nonAddressSpecificData.scores.score[1].scoreLabel;
                    RNOLF04_positive = output.nonAddressSpecificData.scores.score[1].positive;
                    RNOLF04_value = output.nonAddressSpecificData.scores.score[1].value;
                    QSC032 = output.nonAddressSpecificData.decodedCharacteristics.Items[0].ToString();
                    QSC034 = output.nonAddressSpecificData.decodedCharacteristics.Items[1].ToString();
                    QSP032 = output.nonAddressSpecificData.decodedCharacteristics.Items[2].ToString();
                    QSP034 = output.nonAddressSpecificData.decodedCharacteristics.Items[3].ToString();
                    QSE032 = output.nonAddressSpecificData.decodedCharacteristics.Items[4].ToString();
                    QSE034 = output.nonAddressSpecificData.decodedCharacteristics.Items[5].ToString();
                    QSN032 = output.nonAddressSpecificData.decodedCharacteristics.Items[6].ToString();
                    QSN034 = output.nonAddressSpecificData.decodedCharacteristics.Items[7].ToString();
                    QSC031 = output.nonAddressSpecificData.decodedCharacteristics.Items[8].ToString();
                    QSC033 = output.nonAddressSpecificData.decodedCharacteristics.Items[9].ToString();
                    QSC035 = output.nonAddressSpecificData.decodedCharacteristics.Items[10].ToString();
                    QSP030 = output.nonAddressSpecificData.decodedCharacteristics.Items[11].ToString();
                    QSP031 = output.nonAddressSpecificData.decodedCharacteristics.Items[12].ToString();
                    QSP033 = output.nonAddressSpecificData.decodedCharacteristics.Items[13].ToString();
                    QSP035 = output.nonAddressSpecificData.decodedCharacteristics.Items[14].ToString();
                    QSE030 = output.nonAddressSpecificData.decodedCharacteristics.Items[15].ToString();
                    QSE031 = output.nonAddressSpecificData.decodedCharacteristics.Items[16].ToString();
                    QSE033 = output.nonAddressSpecificData.decodedCharacteristics.Items[17].ToString();
                    QSE035 = output.nonAddressSpecificData.decodedCharacteristics.Items[18].ToString();
                    QSN030 = output.nonAddressSpecificData.decodedCharacteristics.Items[19].ToString();
                    QSN031 = output.nonAddressSpecificData.decodedCharacteristics.Items[20].ToString();
                    QSN033 = output.nonAddressSpecificData.decodedCharacteristics.Items[21].ToString();
                    QSN035 = output.nonAddressSpecificData.decodedCharacteristics.Items[22].ToString();
                    QSC030 = output.nonAddressSpecificData.decodedCharacteristics.Items[23].ToString();
                    FBC1 = output.nonAddressSpecificData.decodedCharacteristics.Items[24].ToString();
                    FBC2 = output.nonAddressSpecificData.decodedCharacteristics.Items[25].ToString();
                    FBC5 = output.nonAddressSpecificData.decodedCharacteristics.Items[26].ToString();
                    Option = Convert.ToInt32(output.nonAddressSpecificData.decodedCharacteristics.option);

                    Score = FTOLF04_value + RNOLF04_value;

                    Int32 id = objData.Insert_Equifax_CreaditSearchHeader(appid, apptype, memberid, ClientRef, FTOLF04, FTOLF04_positive, FTOLF04_value, RNOLF04, RNOLF04_positive, RNOLF04_value, QSC032, QSC034, QSP032, QSP034, QSE032, QSE034, QSN032, QSN034, QSC031, QSC033, QSC035, QSP030, QSP031, QSP033, QSP035, QSE030, QSE031, QSE033, QSE035, QSN030, QSN031, QSN033, QSN035, QSC030, FBC1, FBC2, FBC5, Option);
                    
                    if (output.soleSearch.primary.suppliedAddressData.Count() > 0)
                    {
                        int i = 0;
                        foreach (ConsumerService1.SuppliedAddressData spaddress in output.soleSearch.primary.suppliedAddressData)
                        {
                            if (spaddress.addressSpecificData != null)
                            {
                                if (spaddress.addressSpecificData.electoralRollData != null)
                                {
                                    if (spaddress.addressSpecificData.electoralRollData.electoralRoll.Count() > 0)
                                    {
                                        foreach (ConsumerService1.ElectoralRoll elec in output.soleSearch.primary.suppliedAddressData[i].addressSpecificData.electoralRollData.electoralRoll)
                                        {
                                            midname = elec.name.middleName;
                                            surname = elec.name.surname;
                                            forename = elec.name.forename;
                                            namematchstatus = elec.nameMatchStatus.ToString();
                                            seniority = elec.seniority.ToString();
                                            start = elec.annualRegisterPeriod.start;
                                            end = elec.annualRegisterPeriod.end;

                                            objData.Insert_Equifax_CreaditSearchElectoralroll(appid, apptype, memberid, id, midname, surname, forename, namematchstatus, seniority, start, end);
                                        }
                                    }
                                }
                            }
                            if (spaddress != null)
                            {
                                AddressStatus = spaddress.addressMatchStatus.ToString();
                                index = Convert.ToInt32(spaddress.index);
                                Present = spaddress.noticeOfCorrectionOrDisputePresent;
                                county = spaddress.matchedAddress.address.county;
                                number = spaddress.matchedAddress.address.number;
                                postcode = spaddress.matchedAddress.address.postcode;
                                posttown = spaddress.matchedAddress.address.postTown;
                                street1 = spaddress.matchedAddress.address.street1;
                                addressid = spaddress.matchedAddress.address.addressID;

                                objData.Insert_Equifax_CreaditSearchMatchAddress(appid, apptype, memberid, id, AddressStatus, index, Present, county, number, postcode, posttown, street1, addressid);

                            }
                            /* if (Years < 3)
                              {
                                  if (spaddress.addressSpecificData.electoralRollData.electoralRoll.Count() > 0 && i==0)
                                  {
                                      foreach (ConsumerService1.ElectoralRoll elec2 in output.soleSearch.primary.suppliedAddressData[1].addressSpecificData.electoralRollData.electoralRoll)
                                      {
                                          midname = elec2.name.middleName;
                                          surname = elec2.name.surname;
                                          forename = elec2.name.forename;
                                          namematchstatus = elec2.nameMatchStatus.ToString();
                                          seniority = elec2.seniority.ToString();
                                          start = elec2.annualRegisterPeriod.start;
                                          end = elec2.annualRegisterPeriod.end;

                                          objData.Insert_Equifax_CreaditSearchElectoralroll(appid, apptype, memberid, id, midname, surname, forename, namematchstatus, seniority, start, end);
                                      }


                                      AddressStatus = spaddress.addressMatchStatus.ToString();
                                      index = Convert.ToInt32(spaddress.index);
                                      Present = spaddress.noticeOfCorrectionOrDisputePresent;
                                      county = spaddress.matchedAddress.address.county;
                                      number = spaddress.matchedAddress.address.number;
                                      postcode = spaddress.matchedAddress.address.postcode;
                                      posttown = spaddress.matchedAddress.address.postTown;
                                      street1 = spaddress.matchedAddress.address.street1;
                                      addressid = spaddress.matchedAddress.address.addressID;

                                      objData.Insert_Equifax_CreaditSearchMatchAddress(appid, apptype, memberid, id, AddressStatus, index, Present, county, number, postcode, posttown, street1, addressid);
                                  }

                              }*/
                            i++;
                        }

                    }

                    ispasssed = IsEquifaxCreditCheckPassed(Score, passScore);
                }
                else
                {
                    if (output.soleSearch.primary.suppliedAddressData != null)
                    {
                        AddressStatus = output.soleSearch.primary.suppliedAddressData[0].addressMatchStatus.ToString();
                        index = Convert.ToInt32(output.soleSearch.primary.suppliedAddressData[0].index);
                        Int32 id = objData.Insert_Equifax_CreaditSearchHeader(appid, apptype, memberid, ClientRef, FTOLF04, FTOLF04_positive, FTOLF04_value, RNOLF04, RNOLF04_positive, RNOLF04_value, QSC032, QSC034, QSP032, QSP034, QSE032, QSE034, QSN032, QSN034, QSC031, QSC033, QSC035, QSP030, QSP031, QSP033, QSP035, QSE030, QSE031, QSE033, QSE035, QSN030, QSN031, QSN033, QSN035, QSC030, FBC1, FBC2, FBC5, Option);
                        objData.Insert_Equifax_CreaditSearchMatchAddress(appid, apptype, memberid, id, AddressStatus, index, Present, county, number, postcode, posttown, street1, addressid);

                    }
                    
                }

            }
            catch (Exception ex)
            {                
                objData.InsertErrorLog(ex, "", ApplicationID.ToString());
                return 0;
            }

            if (ispasssed)
            { return 1; }
            else
            { return 2; }

        }

        private bool IsEquifaxIDCheckPassed(int recidencyCount, int identityCount, int alertCount)
        {
            bool checkPassed = false;
            if (recidencyCount >= 2 && identityCount >= 0 && alertCount == 0)
            {
                checkPassed = true;
            }
            //else if (identityCount >= 1)
            //{
            //    checkPassed = false;
            //}
            //else if (alertCount > 0)
            //{
            //    checkPassed = false;
            //}

            return checkPassed;
        }

        private bool IsEquifaxCreditCheckPassed(int score, int passScore)
        {
            bool checkPassed = true;
            if (passScore >= score)
            {
                checkPassed = false;
            }
            
            return checkPassed;
        }
    }
}
