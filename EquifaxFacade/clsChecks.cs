﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EquifaxFacade.ConsumerService1;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;

namespace EquifaxFacade
{
    public class clsChecks
    {
        public PollokCU.DataAccess.Layer.clsWorldPay objWorldPay = new PollokCU.DataAccess.Layer.clsWorldPay();
        static PollokCU.DataAccess.Layer.clsConnection objConfig  = new PollokCU.DataAccess.Layer.clsConnection();
        //public static string strConnectTo = objConfig.getConfigKey("ExperianEnv");
        static string username = "";
        static string password = "";
        DateTime LastDate = DateTime.Now;
        DateTime CurrentDate = DateTime.Now;

        clsEquifax Equifax = new clsEquifax();

        public void credentials()
        {
           
                DataTable dtApp = objWorldPay.SelectEquifaxCredentials();
                if (dtApp.Rows.Count > 0)
                {
                    DataRow row = dtApp.Rows[0];

                    username = row["UserName"].ToString();
                    password = row["EquifaxPassword"].ToString();
                    LastDate = Convert.ToDateTime(row["LastPasswordChange"]);
                }
                      
        }

        public void PasswordChange()
        {
            string newPword = string.Empty;
            int days = 0;
            int temp = 0;

            try
            {
                if (string.IsNullOrEmpty(password))
                {
                    credentials();
                }
                TimeSpan difference = CurrentDate.Date - LastDate.Date;
                days = (int)difference.TotalDays;

                if (days > 25)
                {
                    string[] cartparts = password.Split('@');

                    temp = Convert.ToInt32(cartparts[1]);
                    temp = temp + 1;
                    newPword = cartparts[0] + "@" + temp.ToString();

                    // create a change password request
                    SecurityService1.changePasswordRequest ChangePasswordRequest = new SecurityService1.changePasswordRequest();

                    // set the new password property on the request
                    ChangePasswordRequest.newpassword = newPword;

                    using (SecurityService1.EWSSecurityServiceClient client = new SecurityService1.EWSSecurityServiceClient())
                    {

#if DEBUG
                        //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                        var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                        client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());
#endif

                        // Add WS-Security header credentials…
                        client.ClientCredentials.UserName.UserName = username;
                        client.ClientCredentials.UserName.Password = password;

                        // Create a response in advance which will be passed by reference and modified in the service call…
                        SecurityService1.changePasswordResponse response = new SecurityService1.changePasswordResponse();

                        // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                        SecurityService1.PasswordExpiryInformation cpFlag = client.changePassword(null, ChangePasswordRequest, out response);

                        ConsumerResults a2 = new ConsumerResults();
                        a2.SavePasswordChange(username, newPword, response);

                    }
                }
            }
            catch (Exception ex)
            {
                // expect a SOAP fault where the operation fails
                Equifax.InsertErrorLog(ex, "", "New Password Incorrect");

            }
        }


        public void VerifyIdentityCheck()
        {
            // Build a credit search request…
            ConsumerService1.verifyIdentityCheckRequest VerifyIdentityCheckRequest = new verifyIdentityCheckRequest();
            VerifyIdentityCheckRequest.clientRef = "C_SHARP_TEST";

            // Configure a single applicant enquiry…
            ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();
            //ConsumerService1.CreditSearchConfig config = new ConsumerService1.CreditSearchConfig();
            //config.optIn = true;
            //rss.creditSearchConfig = config;

            ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
            mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.subject = ConsumerService1.MatchCriteriaStatus.required;
            rss.matchCriteria = mc;

            // Add requested data… (ADO)
            ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
            ConsumerService1.OutputAddressRequest oar = new ConsumerService1.OutputAddressRequest();
            oar.maxNumber = 0;
            rd.outputAddressRequest = oar;
            rss.requestedData = rd;

            // Set up the single applicant person…
            ConsumerService1.RequestPerson person = new ConsumerService1.RequestPerson();
            ConsumerService1.Name name = new ConsumerService1.Name();
            name.forename = "DAVID";
            name.surname = "BECKHAM";
            person.name = name;

            // Add an address for the applicant…
            ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
            ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();
            rsa.number = "26";
            rsa.postcode = "CB6 2JR";
            residence.address = rsa;
            person.currentAddress = residence;
            rss.primary = person;

            // Finalise the credit search request…
            VerifyIdentityCheckRequest.soleSearch = rss;

            // Create a service client…
            using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
            {

#if DEBUG
                //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());
#endif

                // Add WS-Security header credentials…
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                // Create a response in advance which will be passed by reference and modified in the service call…
                ConsumerService1.verifyIdentityCheckResponse VerifyIdentityCheckResponse = new ConsumerService1.verifyIdentityCheckResponse();

                // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                ConsumerService1.PasswordExpiryInformation cpFlag = client.verifyIdentityCheck(null, VerifyIdentityCheckRequest, out VerifyIdentityCheckResponse);
                Console.WriteLine(VerifyIdentityCheckResponse.clientRef);
                
                int appid = 123;
                string apptype = "PDL";
                ConsumerResults a = new ConsumerResults();
                a.SaveIdCheck(appid, VerifyIdentityCheckResponse, apptype);

               
            }
        }

        public int BankCheck(string accno, string sortcode, string timebank, string sname, string fname, string midname, string timeaddress, string county, string name1, string postcode, string street1, string street2, string previousCounty, string previousname1, string previousPostCode, string previousStreet1, string previousStreet2, int years, int apid, string aptype, Int32 memid)
        { 
              int result = 0;
             
            try
            {
                credentials();
                // Build a credit search request…
                ConsumerService1.verifyIdentityCheckRequest VerifyIdentityCheckRequest = new verifyIdentityCheckRequest();
            VerifyIdentityCheckRequest.clientRef = "Bank Check Request";

            // Configure a single applicant enquiry…
            ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();
            //ConsumerService1.CreditSearchConfig config = new ConsumerService1.CreditSearchConfig();
            //config.optIn = true;
            //rss.creditSearchConfig = config;

            ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
            mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.subject = ConsumerService1.MatchCriteriaStatus.required;
            rss.matchCriteria = mc;

            // Add requested data… (ADO)
            ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
            ConsumerService1.ScoreAndCharacteristicRequests scr = new ConsumerService1.ScoreAndCharacteristicRequests();
            ConsumerService1.CharacteristicsRequest cr = new CharacteristicsRequest();
            cr.index = 2;
            cr.taggedCharacteristics = true;
            scr.characteristicRequests = cr;
            scr.employSameCompanyInsight = true;
            rd.scoreAndCharacteristicRequests = scr;
            rss.requestedData = rd;

            ConsumerService1.RequestPerson person = new ConsumerService1.RequestPerson();
            ConsumerService1.BankingInfo bi = new BankingInfo();
            ConsumerService1.BankAccount b = new BankAccount();
            b.accountNumber = accno;
            b.sortCode = sortcode;
            bi.account = b;
            bi.timeAtBank = timebank;
            person.bankingInfo = bi;



            // Set up the single applicant person…

            ConsumerService1.Name name = new ConsumerService1.Name();
            name.middleName = midname;
            name.surname = sname;
            name.title = "";
            name.forename = fname;
            person.name = name;

            // Add an address for the applicant…
            ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
            ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();

            //ConsumerService1.MatchedResidentialStructuredAddress mra = new ConsumerService1.MatchedResidentialStructuredAddress();

            rsa.country = "";
            rsa.county = county;
            rsa.district = "";
            rsa.name = "";
            rsa.number = "";
            rsa.poBox = "";
            rsa.postcode = postcode;
            rsa.postTown = "";
            rsa.street1 = street1;
            rsa.street2 = street2;
            rsa.subBuilding = "";

            residence.address = rsa;
            residence.timeAtAddress = timeaddress;
            person.currentAddress = residence;

            if (years < 3 && !String.IsNullOrEmpty(previousPostCode) && !String.IsNullOrEmpty(previousStreet1)) //Check current time at address is less than 3 years
            {
                ConsumerService1.ResidenceInstance residencePrevious = new ConsumerService1.ResidenceInstance();
                ConsumerService1.ResidentialStructuredAddress rsaPrevious = new ConsumerService1.ResidentialStructuredAddress();
                ConsumerService1.RequestPreviousResidenceInstance rpr = new RequestPreviousResidenceInstance();
                person.previousAddress = new RequestPreviousResidenceInstance[1];
                rsaPrevious.country = "";
                rsaPrevious.county = previousCounty;
                rsaPrevious.district = "";
                rsaPrevious.name = "";
                rsaPrevious.number = "";
                rsaPrevious.poBox = "";
                rsaPrevious.postcode = previousPostCode;
                rsaPrevious.postTown = "";
                rsaPrevious.street1 = previousStreet1;
                rsaPrevious.street2 = previousStreet2;
                rsaPrevious.subBuilding = "";

                residencePrevious.address = rsaPrevious;
                rpr.residenceInstance = residencePrevious;
                person.previousAddress[0] = rpr;
            }
     
            rss.primary = person;

            // Finalise the credit search request…
            VerifyIdentityCheckRequest.soleSearch = rss;

            // Create a service client…
            using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
            {

#if DEBUG
                //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());
#endif

                // Add WS-Security header credentials…
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;

                // Create a response in advance which will be passed by reference and modified in the service call…
                ConsumerService1.verifyIdentityCheckResponse VerifyIdentityCheckResponse = new ConsumerService1.verifyIdentityCheckResponse();

                // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                ConsumerService1.PasswordExpiryInformation cpFlag = client.verifyIdentityCheck(null, VerifyIdentityCheckRequest, out VerifyIdentityCheckResponse);

                ConsumerResults a2 = new ConsumerResults();
                result = a2.SaveBankCheck(apid, VerifyIdentityCheckResponse, aptype, memid);
                 
                    //saveXml( VerifyIdentityCheckRequest , "consumerRequest");
                    //saveXml( VerifyIdentityCheckResponse , "consumerResponse");

                }
            }
            catch (Exception e)
            {
                result = 0;
                Equifax.InsertErrorLog(e, "", apid.ToString());
            }
            return result;
            
        }

        public int amlCheck(string midnm, string surnm, string foenm, DateTime bday, string county, string name1, string postcode, string street1, string street2, string previousCounty, string previousname1, string previousPostCode, string previousStreet1, string previousStreet2, int years, int apid, string aptype, Int32 memid)
        {
            int result = 0;
            
            try
            {
                PasswordChange();
                credentials();

                // Build a credit search request…
                ConsumerService1.antiMoneyLaunderingCheckRequest AMLCheckRequest = new antiMoneyLaunderingCheckRequest();
                AMLCheckRequest.clientRef = "AML REQUEST";

                // Configure a single applicant enquiry…
                ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();

                ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
                mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
                mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
                mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
                mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
                mc.subject = ConsumerService1.MatchCriteriaStatus.required;
                rss.matchCriteria = mc;

                // Add requested data… (ADO)
                ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
                ConsumerService1.OutputAddressRequest oar = new ConsumerService1.OutputAddressRequest();
                oar.maxNumber = 0;
                rd.outputAddressRequest = oar;
                rss.requestedData = rd;

                ConsumerService1.ScoreAndCharacteristicRequests sr = new ScoreAndCharacteristicRequests();
                ConsumerService1.CharacteristicsRequest cr = new CharacteristicsRequest();
                cr.index = 1;
                cr.taggedCharacteristics = true;
                sr.characteristicRequests = cr;
                sr.employSameCompanyInsight = true;
                rd.scoreAndCharacteristicRequests = sr;

                ConsumerService1.RequestPerson person = new RequestPerson();
                ConsumerService1.Name na = new Name();
                
                na.middleName = midnm;
                na.surname = surnm;
                na.title = "";
                na.forename = foenm;
                person.name = na;
                person.dob = bday;
                person.dobSpecified = true;
                rss.primary = person;

                ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
                ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();

                //ConsumerService1.MatchedResidentialStructuredAddress mra = new ConsumerService1.MatchedResidentialStructuredAddress();

                rsa.country = "";
                rsa.county = county.Replace(" ","");
                rsa.district = "";
                rsa.name = "";
                rsa.number = "";
                rsa.poBox = "";
                rsa.postcode = postcode;
                rsa.postTown = "";
                rsa.street1 = street1;
                rsa.street2 = street2;
                rsa.subBuilding = "";

                residence.address = rsa;
                person.currentAddress = residence;

                if (years < 3 && !String.IsNullOrEmpty(previousPostCode) && !String.IsNullOrEmpty(previousStreet1)) //Check current time at address is less than 3 years
                {
                    ConsumerService1.ResidenceInstance residencePrevious = new ConsumerService1.ResidenceInstance();
                    ConsumerService1.ResidentialStructuredAddress rsaPrevious = new ConsumerService1.ResidentialStructuredAddress();
                    ConsumerService1.RequestPreviousResidenceInstance rpr = new RequestPreviousResidenceInstance();
                    person.previousAddress = new RequestPreviousResidenceInstance[1];
                    rsaPrevious.country = "";
                    rsaPrevious.county = previousCounty.Replace(" ","");
                    rsaPrevious.district = "";
                    rsaPrevious.name = "";
                    rsaPrevious.number = "";
                    rsaPrevious.poBox = "";
                    rsaPrevious.postcode = previousPostCode;
                    rsaPrevious.postTown = "";
                    rsaPrevious.street1 = previousStreet1;
                    rsaPrevious.street2 = previousStreet2;
                    rsaPrevious.subBuilding = "";

                    residencePrevious.address = rsaPrevious;
                    rpr.residenceInstance = residencePrevious;
                    person.previousAddress[0] = rpr;
                }
                rss.primary = person;

                // Finalise the credit search request…
                AMLCheckRequest.soleSearch = rss;

                // Create a service client…
                using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
                {

#if DEBUG
                    //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                    var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                    client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());
#endif

                    // Add WS-Security header credentials…
                    client.ClientCredentials.UserName.UserName = username;
                    client.ClientCredentials.UserName.Password = password;

                    // Create a response in advance which will be passed by reference and modified in the service call…
                    ConsumerService1.antiMoneyLaunderingCheckResponse AMLResponse = new ConsumerService1.antiMoneyLaunderingCheckResponse();

                    // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                    ConsumerService1.PasswordExpiryInformation cpFlag = client.antiMoneyLaunderingCheck(null, AMLCheckRequest, out AMLResponse);
                   
                    ConsumerResults a3 = new ConsumerResults();
                   result = a3.SaveAMLCheck(apid, AMLResponse, aptype, memid, years);

                    //saveXml(AMLCheckRequest, "consumerRequest");
                    //saveXml(AMLResponse, "consumerResponse");
                }
            }
            catch (Exception e)
            {
                result = 0;
                Equifax.InsertErrorLog(e, "", apid.ToString());
            }
           
            return result;
        }

        public int CreditSearch(string midnm, string surnm, string foenm, DateTime bday, string county, string name1, string postcode, string street1, string street2, string previousCounty, string previousname1, string previousPostCode, string previousStreet1, string previousStreet2, int years, int apid, string aptype, Int32 memid, int passScore)
        {
            int result = 0;

            try
            {
                credentials();
            // Build a credit search request…
            ConsumerService1.creditSearchRequest creditSearchRequest = new ConsumerService1.creditSearchRequest();
            creditSearchRequest.clientRef = "FSL001 CREDIT CHECK";

            // Configure a single applicant enquiry…
            ConsumerService1.RequestSoleSearch rss = new ConsumerService1.RequestSoleSearch();
            ConsumerService1.CreditSearchConfig config = new ConsumerService1.CreditSearchConfig();
            config.optIn = false;
            rss.creditSearchConfig = config;

            ConsumerService1.MatchCriteria mc = new ConsumerService1.MatchCriteria();
            mc.associate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.attributable = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.family = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.potentialAssociate = ConsumerService1.MatchCriteriaStatus.notRequired;
            mc.subject = ConsumerService1.MatchCriteriaStatus.required;
            rss.matchCriteria = mc;

            // Add requested data… (ADO)
            ConsumerService1.RequestedData rd = new ConsumerService1.RequestedData();
            ConsumerService1.OutputAddressRequest oar = new ConsumerService1.OutputAddressRequest();
            oar.maxNumber = 0;
            oar.scope = RequestScope.supplied;
            rd.outputAddressRequest = oar;
            //rss.requestedData = rd;

            ConsumerService1.CIFASRequest cr = new ConsumerService1.CIFASRequest();
            cr.maxNumber = 0;
            rd.cifasRequest = cr;

            ConsumerService1.CourtAndInsolvencyInformationRequest cf = new ConsumerService1.CourtAndInsolvencyInformationRequest();
            cf.maxNumber = 0;
            rd.courtAndInsolvencyInformationRequest = cf;

            ConsumerService1.NoticeOfCorrectionOrDisputeRequest nr = new ConsumerService1.NoticeOfCorrectionOrDisputeRequest();
            nr.maxNumber = 0;
            rd.noticeofCorrectionOrDisputeRequest = nr;

            ConsumerService1.ElectoralRollRequest er = new ConsumerService1.ElectoralRollRequest();
            er.maxNumber = 0;
            rd.electoralRollRequest = er;

            ConsumerService1.RollingRegisterRequest rr = new ConsumerService1.RollingRegisterRequest();
            rr.maxNumber = 0;
            rd.rollingRegisterRequest = rr;

            ConsumerService1.ScoreAndCharacteristicRequests scr = new ConsumerService1.ScoreAndCharacteristicRequests();
            ConsumerService1.ScoreRequest sr = new ConsumerService1.ScoreRequest();
            ConsumerService1.CharacteristicsRequest chr = new ConsumerService1.CharacteristicsRequest();

            sr.scoreLabel = new string[] { "FTOLF04", "RNOLF04" };
            chr.index = 3;
            chr.taggedCharacteristics = true;
            scr.employSameCompanyInsight = true;
            scr.scoreRequest = sr;
            scr.characteristicRequests = chr;
            rd.scoreAndCharacteristicRequests = scr;

            rss.requestedData = rd;

            // Set up the single applicant person…
            ConsumerService1.RequestPerson person = new ConsumerService1.RequestPerson();
            ConsumerService1.Name name = new ConsumerService1.Name();
            name.middleName = midnm;
            name.surname = surnm;
            name.title = "";
            name.forename = foenm;
            person.name = name;
            person.dob = bday;
            person.dobSpecified = true;


            // Add an address for the applicant…
            ConsumerService1.ResidenceInstance residence = new ConsumerService1.ResidenceInstance();
            ConsumerService1.ResidentialStructuredAddress rsa = new ConsumerService1.ResidentialStructuredAddress();

            //ConsumerService1.MatchedResidentialStructuredAddress mra = new ConsumerService1.MatchedResidentialStructuredAddress();

            rsa.country = "";
            rsa.county = county.Replace(" ", "");
            rsa.district = "";
            rsa.name = "";
            rsa.number = "";
            rsa.poBox = "";
            rsa.postcode = postcode;
            rsa.postTown = "";
            rsa.street1 = street1;
            rsa.street2 = street2;
            rsa.subBuilding = "";

            residence.address = rsa;
            person.currentAddress = residence;

            if (years < 3 && !String.IsNullOrEmpty(previousPostCode) && !String.IsNullOrEmpty(previousStreet1)) //Check current time at address is less than 3 years
            {
                ConsumerService1.ResidenceInstance residencePrevious = new ConsumerService1.ResidenceInstance();
                ConsumerService1.ResidentialStructuredAddress rsaPrevious = new ConsumerService1.ResidentialStructuredAddress();
                ConsumerService1.RequestPreviousResidenceInstance rpr = new RequestPreviousResidenceInstance();
                person.previousAddress = new RequestPreviousResidenceInstance[1];
                rsaPrevious.country = "";
                rsaPrevious.county = previousCounty.Replace(" ", "");
                rsaPrevious.district = "";
                rsaPrevious.name = "";
                rsaPrevious.number = "";
                rsaPrevious.poBox = "";
                rsaPrevious.postcode = previousPostCode;
                rsaPrevious.postTown = "";
                rsaPrevious.street1 = previousStreet1;
                rsaPrevious.street2 = previousStreet2;
                rsaPrevious.subBuilding = "";

                residencePrevious.address = rsaPrevious;
                rpr.residenceInstance = residencePrevious;
                person.previousAddress[0] = rpr;
            }
            rss.primary = person;



            // Finalise the credit search request…
            creditSearchRequest.soleSearch = rss;

            // Create a service client…
            using (ConsumerService1.EWSConsumerServiceClient client = new ConsumerService1.EWSConsumerServiceClient())
            {

#if DEBUG
                //Console.Out.WriteLine("Removing VS debug info from SOAP request");
                var vs = client.Endpoint.Behaviors.Where((i) => i.GetType().Namespace.Contains("VisualStudio"));
                client.Endpoint.Behaviors.Remove((System.ServiceModel.Description.IEndpointBehavior)vs.Single());
#endif

                    // Add WS-Security header credentials…
                    client.ClientCredentials.UserName.UserName = username;
                    client.ClientCredentials.UserName.Password = password;

                // Create a response in advance which will be passed by reference and modified in the service call…
                ConsumerService1.creditSearchResponse creditSearchResponse = new ConsumerService1.creditSearchResponse();

                // Invoke the web service operation passing a null security header (as this is handled in the app.config binding)…
                ConsumerService1.PasswordExpiryInformation cpFlag = client.creditSearch(null, creditSearchRequest, out creditSearchResponse);

                ConsumerResults a4 = new ConsumerResults();
                result = a4.SaveCreditCheck(apid, creditSearchResponse, aptype, memid, passScore, years);

                    //saveXml(creditSearchRequest, "consumerRequest");
                    //saveXml(creditSearchResponse, "consumerResponse");

                }
            }
            catch (Exception e)
            {
                result = 0;
                Equifax.InsertErrorLog(e, "", apid.ToString());
            }
            return result;
            }

        static void saveXml(Object obj, string filename)
        {
            string folder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\XML\\";

            if (Directory.Exists(folder) == false)
            {
                Directory.CreateDirectory(folder);
            }

            filename = folder + filename + DateTime.Now.ToString("_yyyyMMdd_HHmm") + ".xml";

            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            StringWriter outStream = new StringWriter();
            serializer.Serialize(outStream, obj);

            File.WriteAllText(filename, outStream.ToString());

            Process p = new Process();
            p.StartInfo.FileName = "notepad";
            p.StartInfo.Arguments = filename;
            p.Start();
        }
    }
    }

