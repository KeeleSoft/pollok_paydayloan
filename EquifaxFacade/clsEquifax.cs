﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data; 

namespace EquifaxFacade
{
    class clsEquifax
    {
        static PollokCU.DataAccess.Layer.clsConnection clsConn = new PollokCU.DataAccess.Layer.clsConnection();
        //public static string ConnectionString = objConfig.getConfigKey("SCU.Connection.Key");
        public SqlCommand com1 = new SqlCommand();
        SqlConnection objCon;

        public void InsertData(
                int appid,
                string apptype,
                int memberid,
                string ClientRef,
                string AddressStatus,
                int index,
                Boolean Present,
                string County,
                string district,
                string number,
                string postcode,
                string postTown,
                string street1,
                string addressID)
        {
            try
            {
               
               SqlConnection con1 = new SqlConnection(Properties.Settings.Default.ConStringUAT);

                con1.Open();
                com1 = new SqlCommand("EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@applicationID", appid);
                com1.Parameters.AddWithValue("@applicationType", apptype);
                com1.Parameters.AddWithValue("@memberID", memberid);
                com1.Parameters.AddWithValue("@clientReference", ClientRef);
                com1.Parameters.AddWithValue("@addressMatchStatus", AddressStatus);
                com1.Parameters.AddWithValue("@index", index);
                com1.Parameters.AddWithValue("@noticeOfCorrectionOrDisputePresent", Present);
                com1.Parameters.AddWithValue("@county", County);
                com1.Parameters.AddWithValue("@district", district);
                com1.Parameters.AddWithValue("@number", number);
                com1.Parameters.AddWithValue("@postcode", postcode);
                com1.Parameters.AddWithValue("@postTown", postTown);
                com1.Parameters.AddWithValue("@street1", street1);
                com1.Parameters.AddWithValue("@addressID", addressID);
                com1.ExecuteNonQuery();
                con1.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void UpdatePassword(string UserName, string Password)
        {
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

                com1 = new SqlCommand("UPDATE_EQUIFAX_CREDENTIALS", objCon);
                com1.CommandType = CommandType.StoredProcedure;

                com1.Parameters.AddWithValue("@Username", UserName);
                com1.Parameters.AddWithValue("@Password", Password);

                com1.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception e)
            {

            }
        }


        public void InsertBankData(
              int appid,
              string apptype,
              int memberid,
              string ClientRef,
                string QSC032,
                string QSC034,
                string QSP032,
                string QSP034,
                string QSE032,
                string QSE034,
                string QSN032,
                string QSN034,
                string QSC031,
                string QSC033,
                string QSC035,
                string QSP030,
                string QSP031,
                string QSP033,
                string QSP035,
                string QSE030,
                string QSE031,
                string QSE033,
                string QSE035,
                string QSN030,
                string QSN031,
                string QSN033,
                string QSN035,
                string QSC030,
                string FBC1,
                string FBC2,
                string FBC5,
                int option,
              string AddressStatus,
              int index,
              Boolean Present)
        {
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

               // con1.Open();
                com1 = new SqlCommand("dbo.EQUIFAX_INSERT_BANKCHECK_RESULT", objCon);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@ApplicationID", appid);
                com1.Parameters.AddWithValue("@ApplicationType", apptype);
                com1.Parameters.AddWithValue("@MemberID", memberid);
                com1.Parameters.AddWithValue("@ClientReference", ClientRef);

                com1.Parameters.AddWithValue("@QSC032", QSC032);
                com1.Parameters.AddWithValue("@QSC034", QSC034);
                com1.Parameters.AddWithValue("@QSP032", QSP032);
                com1.Parameters.AddWithValue("@QSP034", QSP034);
                com1.Parameters.AddWithValue("@QSE032", QSE032);
                com1.Parameters.AddWithValue("@QSE034", QSE034);
                com1.Parameters.AddWithValue("@QSN032", QSN032);
                com1.Parameters.AddWithValue("@QSN034", QSN034);
                com1.Parameters.AddWithValue("@QSC031", QSC031);
                com1.Parameters.AddWithValue("@QSC033", QSC033);
                com1.Parameters.AddWithValue("@QSC035", QSC035);
                com1.Parameters.AddWithValue("@QSP030", QSP030);
                com1.Parameters.AddWithValue("@QSP031", QSP031);
                com1.Parameters.AddWithValue("@QSP033", QSP033);
                com1.Parameters.AddWithValue("@QSP035", QSP035);
                com1.Parameters.AddWithValue("@QSE030", QSE030);
                com1.Parameters.AddWithValue("@QSE031", QSE031);
                com1.Parameters.AddWithValue("@QSE033", QSE033);
                com1.Parameters.AddWithValue("@QSE035", QSE035);
                com1.Parameters.AddWithValue("@QSN030", QSN030);
                com1.Parameters.AddWithValue("@QSN031", QSN031);
                com1.Parameters.AddWithValue("@QSN033", QSN033);
                com1.Parameters.AddWithValue("@QSN035", QSN035);
                com1.Parameters.AddWithValue("@QSC030", QSC030);
                com1.Parameters.AddWithValue("@FBC1", FBC1);
                com1.Parameters.AddWithValue("@FBC2", FBC2);
                com1.Parameters.AddWithValue("@FBC5", FBC5);

                com1.Parameters.AddWithValue("@Options", option);
                com1.Parameters.AddWithValue("@AddressMatchStatus", AddressStatus);
                com1.Parameters.AddWithValue("@Index", index);
                com1.Parameters.AddWithValue("@NoticeOfCorrectionOrDisputePresent", Present);

                com1.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void InsertAMLData(
              int appid,
              string apptype,
              int memberid,
              string ClientRef,
               string VRC101,
               string ESC941,
               string RSC8,
               string QSC001,
               string CSC903,
               string FSC12,
               string TSC1,
               string QSC034,
               string FSC3,
               string NSC1,
               string NSC2,
               string HSC1,
               string DSC1,
               string DSC3,
               string DSC4,
               string DSC5,
               string ASC8,
               int VRC18,
               int VIC3,
               int VNC3,
               string VXC3,
              int option,
              string AddressStatus,
              int index,
              Boolean Present,
              string County,
              string number,
              string postcode,
              string postTown,
              string street1,
              string addressID,
              string PreviousAddressStatus,
              int Previousindex,
              Boolean PreviousPresent,
              string PreviousCounty,
              string Previousnumber,
              string Previouspostcode,
              string PreviouspostTown,
              string Previousstreet1,
              string PreviousaddressID)
        {
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

                //con1.Open();
                com1 = new SqlCommand("dbo.EQUIFAX_INSERT__AMLCHECKRESULT", objCon);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@ApplicationID", appid);
                com1.Parameters.AddWithValue("@ApplicationType", apptype);
                com1.Parameters.AddWithValue("@MemberID", memberid);
                com1.Parameters.AddWithValue("@ClientRef", ClientRef);
                com1.Parameters.AddWithValue("@VRC101", VRC101);
                com1.Parameters.AddWithValue("@ESC941", ESC941);
                com1.Parameters.AddWithValue("@RSC8", RSC8);
                com1.Parameters.AddWithValue("@QSC001", QSC001);
                com1.Parameters.AddWithValue("@CSC903", CSC903);
                com1.Parameters.AddWithValue("@FSC12", FSC12);
                com1.Parameters.AddWithValue("@TSC1", TSC1);
                com1.Parameters.AddWithValue("@QSC034", QSC034);
                com1.Parameters.AddWithValue("@FSC3", FSC3);
                com1.Parameters.AddWithValue("@NSC1", NSC1);
                com1.Parameters.AddWithValue("@NSC2", NSC2);
                com1.Parameters.AddWithValue("@HSC1", HSC1);
                com1.Parameters.AddWithValue("@DSC1", DSC1);
                com1.Parameters.AddWithValue("@DSC3", DSC3);
                com1.Parameters.AddWithValue("@DSC4", DSC4);
                com1.Parameters.AddWithValue("@DSC5", DSC5);
                com1.Parameters.AddWithValue("@ASC8", ASC8);
                com1.Parameters.AddWithValue("@VRC18", VRC18);
                com1.Parameters.AddWithValue("@VIC3", VIC3);
                com1.Parameters.AddWithValue("@VNC3", VNC3);
                com1.Parameters.AddWithValue("@VXC3", VXC3);
                com1.Parameters.AddWithValue("@Option", option);
                com1.Parameters.AddWithValue("@AddressMatchStatus", AddressStatus);
                com1.Parameters.AddWithValue("@Index", index);
                com1.Parameters.AddWithValue("@NoticeOfCorrectionOrDisputePresent", Present);
                com1.Parameters.AddWithValue("@County", County);
                com1.Parameters.AddWithValue("@Number", number);
                com1.Parameters.AddWithValue("@PostCode", postcode);
                com1.Parameters.AddWithValue("@PostTown", postTown);
                com1.Parameters.AddWithValue("@Street1", street1);
                com1.Parameters.AddWithValue("@AddressID", addressID);
                com1.Parameters.AddWithValue("@PreviousAddressMatchStatus", PreviousAddressStatus);
                com1.Parameters.AddWithValue("@PreviousIndex", Previousindex);
                com1.Parameters.AddWithValue("@PreviousNoticeOfCorrectionOrDisputePresent", PreviousPresent);
                com1.Parameters.AddWithValue("@PreviousCounty", PreviousCounty);
                com1.Parameters.AddWithValue("@PreviousNumber", Previousnumber);
                com1.Parameters.AddWithValue("@PreviousPostCode", Previouspostcode);
                com1.Parameters.AddWithValue("@PreviousPostTown", PreviouspostTown);
                com1.Parameters.AddWithValue("@PreviousStreet1", Previousstreet1);
                com1.Parameters.AddWithValue("@PreviousAddressID", PreviousaddressID);

                com1.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public Int32 Insert_Equifax_CreaditSearchHeader(
             int appid,
             string apptype,
             int memberid,
             string ClientRef,
             string FTOLF04,
             Boolean FTOLF04_positive,
             int FTOLF04_value,
             string RNOLF04,
             Boolean RNOLF04_positive,
             int RNOLF04_value,
               string QSC032,
               string QSC034,
               string QSP032,
               string QSP034,
               string QSE032,
               string QSE034,
               string QSN032,
               string QSN034,
               string QSC031,
               string QSC033,
               string QSC035,
               string QSP030,
               string QSP031,
               string QSP033,
               string QSP035,
               string QSE030,
               string QSE031,
               string QSE033,
               string QSE035,
               string QSN030,
               string QSN031,
               string QSN033,
               string QSN035,
               string QSC030,
               string FBC1,
               string FBC2,
               string FBC5,
               int option)
        {
            int id = 0;
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

                //con1.Open();
                com1 = new SqlCommand("EQUIFAX_INSERT_CREDITSEARCH_HEADER", objCon);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@ApplicationID", appid);
                com1.Parameters.AddWithValue("@ApplicationType", apptype);
                com1.Parameters.AddWithValue("@MemberID", memberid);
                com1.Parameters.AddWithValue("@ClientRef", ClientRef);
                com1.Parameters.AddWithValue("@FTOLF04", FTOLF04);
                com1.Parameters.AddWithValue("@FTOLF04_Positive", FTOLF04_positive);
                com1.Parameters.AddWithValue("@FTOLF04_Value", FTOLF04_value);
                com1.Parameters.AddWithValue("@RNOLF04", RNOLF04);
                com1.Parameters.AddWithValue("@RNOLF04_Positive", RNOLF04_positive);
                com1.Parameters.AddWithValue("@RNOLF04_Value", RNOLF04_value);

                com1.Parameters.AddWithValue("@QSC032", QSC032);
                com1.Parameters.AddWithValue("@QSC034", QSC034);
                com1.Parameters.AddWithValue("@QSP032", QSP032);
                com1.Parameters.AddWithValue("@QSP034", QSP034);
                com1.Parameters.AddWithValue("@QSE032", QSE032);
                com1.Parameters.AddWithValue("@QSE034", QSE034);
                com1.Parameters.AddWithValue("@QSN032", QSN032);
                com1.Parameters.AddWithValue("@QSN034", QSN034);
                com1.Parameters.AddWithValue("@QSC031", QSC031);
                com1.Parameters.AddWithValue("@QSC033", QSC033);
                com1.Parameters.AddWithValue("@QSC035", QSC035);
                com1.Parameters.AddWithValue("@QSP030", QSP030);
                com1.Parameters.AddWithValue("@QSP031", QSP031);
                com1.Parameters.AddWithValue("@QSP033", QSP033);
                com1.Parameters.AddWithValue("@QSP035", QSP035);
                com1.Parameters.AddWithValue("@QSE030", QSE030);
                com1.Parameters.AddWithValue("@QSE031", QSE031);
                com1.Parameters.AddWithValue("@QSE033", QSE033);
                com1.Parameters.AddWithValue("@QSE035", QSE035);
                com1.Parameters.AddWithValue("@QSN030", QSN030);
                com1.Parameters.AddWithValue("@QSN031", QSN031);
                com1.Parameters.AddWithValue("@QSN033", QSN033);
                com1.Parameters.AddWithValue("@QSN035", QSN035);
                com1.Parameters.AddWithValue("@QSC030", QSC030);
                com1.Parameters.AddWithValue("@FBC1", FBC1);
                com1.Parameters.AddWithValue("@FBC2", FBC2);
                com1.Parameters.AddWithValue("@FBC5", FBC5);
                com1.Parameters.AddWithValue("@Option", option);

                object obj = com1.ExecuteScalar();
                objCon.Close();

                id = Convert.ToInt32(obj);
            }
            catch (Exception e)
            {

            }
            return id;
        }

        public void Insert_Equifax_CreaditSearchElectoralroll(
                int appid,
                string apptype,
                int memberid,
                Int32 id,
                string midname,
                string surname,
                string forename,
                string namematchstatus,
                string seniority,
                string start,
                string end)
        {
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

                //con1.Open();
                com1 = new SqlCommand("EQUIFAX_INSERT_CREDITSEARCH_ELECTORALROLL", objCon);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@applicationID", appid);
                com1.Parameters.AddWithValue("@applicationType", apptype);
                com1.Parameters.AddWithValue("@memberID", memberid);
                com1.Parameters.AddWithValue("@HeaderID", id);
                com1.Parameters.AddWithValue("@MiddleName", midname);
                com1.Parameters.AddWithValue("@SurName", surname);
                com1.Parameters.AddWithValue("@ForeName", forename);
                com1.Parameters.AddWithValue("@NameMatchStatus", namematchstatus);
                com1.Parameters.AddWithValue("@Seniority", seniority);
                com1.Parameters.AddWithValue("@Start", start);
                com1.Parameters.AddWithValue("@End", end);

                com1.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception e)
            {

            }
        }

        public void Insert_Equifax_CreaditSearchMatchAddress(
                int appid,
                string apptype,
                int memberid,
                Int32 id,
                string AddressStatus,
                int index,
                Boolean Present,
                string county,
                string number,
                string postcode,
                string posttown,
                string street1,
                string addressid)
        {
            try
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }

                //con1.Open();
                com1 = new SqlCommand("EQUIFAX_INSERT_CREDITSEARCH_MATCHADDRESS", objCon);
                com1.CommandType = CommandType.StoredProcedure;
                //SqlCommand comm = new SqlCommand("dbo.EQUIFAX_IDENTITYCHECK_INSERTDATA", con1);
                //com1.CommandType = System.Data.CommandType.StoredProcedure;
                com1.Parameters.AddWithValue("@applicationID", appid);
                com1.Parameters.AddWithValue("@applicationType", apptype);
                com1.Parameters.AddWithValue("@memberID", memberid);
                com1.Parameters.AddWithValue("@HeaderID", id);
                com1.Parameters.AddWithValue("@AddressMatchStatus", AddressStatus);
                com1.Parameters.AddWithValue("@Index", index);
                com1.Parameters.AddWithValue("@NoticeOfCorrectionOrDisputePresent", Present);
                com1.Parameters.AddWithValue("@County", county);
                com1.Parameters.AddWithValue("@Number", number);
                com1.Parameters.AddWithValue("@PostCode", postcode);
                com1.Parameters.AddWithValue("@PostTown", posttown);
                com1.Parameters.AddWithValue("@Street1", street1);
                com1.Parameters.AddWithValue("@AddressID", addressid);

                com1.ExecuteNonQuery();
                objCon.Close();
            }
            catch (Exception e)
            {

            }
        }

        public void InsertErrorLog(Exception ex, string IPAddress, string ExtraData = "")
        { 
            
            try 
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }
                
                 com1 = new SqlCommand("dbo.INSERT_ERROR_LOG", objCon);
                 com1.CommandType = CommandType.StoredProcedure;

                //Input parameters
                com1.Parameters.AddWithValue("@ErrMessage", ex.Message + "-" + ExtraData);
                com1.Parameters.AddWithValue("@StackTrace", ex.StackTrace);
                com1.Parameters.AddWithValue("@CreatedIPAddress", IPAddress);
    
                com1.ExecuteNonQuery();
               
            }
            catch (Exception ex2)
            {
                
            }
            finally
            {
                com1.Dispose();
                objCon.Close();
            }
        }

        public void InsertTrackingServiceStat(int TrackingServiceCategoryID, string TrackingData)
        { 
            
            try 
            {
                if (objCon == null || !(objCon.State == ConnectionState.Open))
                {
                    objCon = new SqlConnection(clsConn.getConnectionString());
                    objCon.Open();
                }
                
                 com1 = new SqlCommand("dbo.PDL_INSERT_TRACKING_SERVICE_STAT", objCon);
                 com1.CommandType = CommandType.StoredProcedure;

                //Input parameters
                com1.Parameters.AddWithValue("@TrackingServiceCategoryID", TrackingServiceCategoryID);
                com1.Parameters.AddWithValue("@MemberID", 0);
                com1.Parameters.AddWithValue("@IPAddress", "");
                com1.Parameters.AddWithValue("@TrackingData", TrackingData);
                com1.Parameters.AddWithValue("@UserID", 0);
                com1.ExecuteNonQuery();
               
            }
            catch (Exception ex2)
            {
                
            }
            finally
            {
                com1.Dispose();
                objCon.Close();
            }
        }
    }
}
