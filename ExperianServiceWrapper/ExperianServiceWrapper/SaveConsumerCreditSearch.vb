﻿Public Class SaveConsumerCreditSearch
    Dim objExperianData As clsExperian = New clsExperian
    Dim objSystemData As clsSystemData = New clsSystemData

    ''' <summary>
    ''' One single method to save all data. This method will call individual save method. If one failed it will continue to the other
    ''' </summary>
    Public Function SaveConsumerCreditSearch(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try

            SaveConsumerData_Main(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_ImpairedCH(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_MOSAIC(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_ADB_Utilisationblock(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_ADB_TelecommsBlock(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_ADB_NeverPaidDefsBlock(ApplicationID, output, IPAddress)

            SaveConsumerData_PVD_ADB_APACSCCBehavrlData(ApplicationID, output, IPAddress)

            SaveConsumerData_PublicInfo(ApplicationID, output, IPAddress)

            SaveConsumerData_CIFAS_CML_GAIN_NOC_TPD(ApplicationID, output, IPAddress)

            SaveConsumerData_CAPS(ApplicationID, output, IPAddress)

            SaveConsumerData_CAIS(ApplicationID, output, IPAddress)

            SaveConsumerData_FullConData_CDS_SD_CAISSummary(ApplicationID, output, IPAddress)

            SaveConsumerData_FullConData_CDS_SD_SummaryFlags(ApplicationID, output, IPAddress)

            SaveConsumerData_FullConData_CD_Association(ApplicationID, output, IPAddress)

            SaveConsumerData_FullConData_CD_CAIS(ApplicationID, output, IPAddress)

            SaveConsumerData_FullConData_CD_PreviousApplication(ApplicationID, output, IPAddress)

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Main data, Table: dbo.ExperianConsumerData_Main
    ''' </summary>
    Private Function SaveConsumerData_Main(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try

            Dim ExperianReference As String = ""
            Dim ErrorCode As String = ""
            Dim ErrorMessage As String = ""
            Dim ErrorSeverity As String = ""

            'Scoring
            Dim Scoring_E5S01 As String = ""
            Dim Scoring_E5S02 As String = ""
            Dim Scoring_E5S041 As String = ""
            Dim Scoring_E5S042 As String = ""
            Dim Scoring_E5S043 As String = ""
            Dim Scoring_E5S051 As String = ""
            Dim Scoring_E5S052 As String = ""
            Dim Scoring_E5S053 As String = ""
            Dim Scoring_NDHHOSCORE As String = ""
            Dim Scoring_NDSI21 As String = ""
            Dim Scoring_NDSI22 As String = ""
            Dim Scoring_NDSI23 As String = ""
            Dim Scoring_NDVALSCORE As String = ""

            'Electoral
            Dim ElectoralRoll_E4Q01 As String = ""
            Dim ElectoralRoll_E4Q02 As String = ""
            Dim ElectoralRoll_E4Q03 As String = ""
            Dim ElectoralRoll_E4Q04 As String = ""
            Dim ElectoralRoll_E4Q05 As String = ""
            Dim ElectoralRoll_E4Q06 As String = ""
            Dim ElectoralRoll_E4Q07 As String = ""
            Dim ElectoralRoll_E4Q08 As String = ""
            Dim ElectoralRoll_E4Q09 As String = ""
            Dim ElectoralRoll_E4Q10 As String = ""
            Dim ElectoralRoll_E4Q11 As String = ""
            Dim ElectoralRoll_E4Q12 As String = ""
            Dim ElectoralRoll_E4Q13 As String = ""
            Dim ElectoralRoll_E4Q14 As String = ""
            Dim ElectoralRoll_E4Q15 As String = ""
            Dim ElectoralRoll_E4Q16 As String = ""
            Dim ElectoralRoll_E4Q17 As String = ""
            Dim ElectoralRoll_E4Q18 As String = ""
            Dim ElectoralRoll_E4R01 As String = ""
            Dim ElectoralRoll_E4R02 As String = ""
            Dim ElectoralRoll_E4R03 As String = ""
            Dim ElectoralRoll_EA4R01PM As String = ""
            Dim ElectoralRoll_EA4R01CJ As String = ""
            Dim ElectoralRoll_EA4R01PJ As String = ""
            Dim ElectoralRoll_NDERL01 As String = ""
            Dim ElectoralRoll_NDERL02 As String = ""
            Dim ElectoralRoll_EA2Q01 As String = ""
            Dim ElectoralRoll_EA2Q02 As String = ""
            Dim ElectoralRoll_NDERLMACA As String = ""
            Dim ElectoralRoll_NDERLMAPA As String = ""
            Dim ElectoralRoll_NDERLJACA As String = ""
            Dim ElectoralRoll_NDERLJAPA As String = ""
            Dim ElectoralRoll_EA5U01 As String = ""
            Dim ElectoralRoll_EA5U02 As String = ""

            If output.Output IsNot Nothing AndAlso output.Output.Control IsNot Nothing Then
                ExperianReference = output.Output.Control.ExperianReference
            End If

            If output.Output IsNot Nothing AndAlso output.Output.Error IsNot Nothing Then
                ErrorCode = output.Output.Error.ErrorCode
                ErrorMessage = output.Output.Error.Message
                ErrorSeverity = output.Output.Error.Severity
            End If

            If output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.Scoring IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.Scoring

                    Scoring_E5S01 = .E5S01
                    Scoring_E5S02 = .E5S01
                    Scoring_E5S041 = .E5S041
                    Scoring_E5S042 = .E5S042
                    Scoring_E5S043 = .E5S043
                    Scoring_E5S051 = .E5S051
                    Scoring_E5S052 = .E5S052
                    Scoring_E5S053 = .E5S053
                    Scoring_NDHHOSCORE = .NDHHOSCORE
                    Scoring_NDSI21 = .NDSI21
                    Scoring_NDSI22 = .NDSI22
                    Scoring_NDSI23 = .NDSI23
                    Scoring_NDVALSCORE = .NDVALSCORE
                End With
            End If

            If output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.ElectoralRoll IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.ElectoralRoll

                    ElectoralRoll_E4Q01 = .E4Q01
                    ElectoralRoll_E4Q02 = .E4Q02
                    ElectoralRoll_E4Q03 = .E4Q03
                    ElectoralRoll_E4Q04 = .E4Q04
                    ElectoralRoll_E4Q05 = .E4Q05
                    ElectoralRoll_E4Q06 = .E4Q06
                    ElectoralRoll_E4Q07 = .E4Q07
                    ElectoralRoll_E4Q08 = .E4Q08
                    ElectoralRoll_E4Q09 = .E4Q09
                    ElectoralRoll_E4Q10 = .E4Q10
                    ElectoralRoll_E4Q11 = .E4Q11
                    ElectoralRoll_E4Q12 = .E4Q12
                    ElectoralRoll_E4Q13 = .E4Q13
                    ElectoralRoll_E4Q14 = .E4Q14
                    ElectoralRoll_E4Q15 = .E4Q15
                    ElectoralRoll_E4Q16 = .E4Q16
                    ElectoralRoll_E4Q17 = .E4Q17
                    ElectoralRoll_E4Q18 = .E4Q18
                    ElectoralRoll_E4R01 = .E4R01
                    ElectoralRoll_E4R02 = .E4R02
                    ElectoralRoll_E4R03 = .E4R03
                    ElectoralRoll_EA4R01PM = .EA4R01PM
                    ElectoralRoll_EA4R01CJ = .EA4R01CJ
                    ElectoralRoll_EA4R01PJ = .EA4R01PJ
                    ElectoralRoll_NDERL01 = .NDERL01
                    ElectoralRoll_NDERL02 = .NDERL02
                    ElectoralRoll_EA2Q01 = .EA2Q01
                    ElectoralRoll_EA2Q02 = .EA2Q02
                    ElectoralRoll_NDERLMACA = .NDERLMACA
                    ElectoralRoll_NDERLMAPA = .NDERLMAPA
                    ElectoralRoll_NDERLJACA = .NDERLJACA
                    ElectoralRoll_NDERLJAPA = .NDERLJAPA
                    ElectoralRoll_EA5U01 = .EA5U01
                    ElectoralRoll_EA5U02 = .EA5U02
                End With
            End If

            objExperianData.InsertConsumerData_Main(ApplicationID, ExperianReference _
                                                     , ErrorCode, ErrorMessage, ErrorSeverity _
                                                     , Scoring_E5S01, Scoring_E5S02, Scoring_E5S041, Scoring_E5S042, Scoring_E5S043 _
                                                     , Scoring_E5S051, Scoring_E5S052, Scoring_E5S053, Scoring_NDHHOSCORE, Scoring_NDSI21 _
                                                     , Scoring_NDSI22, Scoring_NDSI23, Scoring_NDVALSCORE _
                                                     , ElectoralRoll_E4Q01, ElectoralRoll_E4Q02, ElectoralRoll_E4Q03, ElectoralRoll_E4Q04 _
                                                     , ElectoralRoll_E4Q05, ElectoralRoll_E4Q06, ElectoralRoll_E4Q07, ElectoralRoll_E4Q08 _
                                                     , ElectoralRoll_E4Q09, ElectoralRoll_E4Q10, ElectoralRoll_E4Q11, ElectoralRoll_E4Q12 _
                                                     , ElectoralRoll_E4Q13, ElectoralRoll_E4Q14, ElectoralRoll_E4Q15, ElectoralRoll_E4Q16 _
                                                     , ElectoralRoll_E4Q17, ElectoralRoll_E4Q18, ElectoralRoll_E4R01, ElectoralRoll_E4R02 _
                                                     , ElectoralRoll_E4R03, ElectoralRoll_EA4R01PM, ElectoralRoll_EA4R01CJ, ElectoralRoll_EA4R01PJ _
                                                     , ElectoralRoll_NDERL01, ElectoralRoll_NDERL02, ElectoralRoll_EA2Q01, ElectoralRoll_EA2Q02 _
                                                     , ElectoralRoll_NDERLMACA, ElectoralRoll_NDERLMAPA, ElectoralRoll_NDERLJACA, ElectoralRoll_NDERLJAPA _
                                                     , ElectoralRoll_EA5U01, ElectoralRoll_EA5U02)


            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_ImpairedCH
    ''' </summary>
    Private Function SaveConsumerData_PVD_ImpairedCH(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.ImpairedCH IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.ImpairedCH
                    Dim NDICH As String = .NDICH
                    Dim NDSECARR As String = .NDSECARR
                    Dim NDUNSECARR As String = .NDUNSECARR
                    Dim NDCCJ As String = .NDCCJ
                    Dim NDIVA As String = .NDIVA
                    Dim NDBANKRUPT As String = .NDBANKRUPT
                    Dim NDMAICH As String = .NDMAICH
                    Dim NDMASECARR As String = .NDMASECARR
                    Dim NDMAUNSECARR As String = .NDMAUNSECARR
                    Dim NDMACCJ As String = .NDMACCJ
                    Dim NDMAIVA As String = .NDMAIVA
                    Dim NDMABANKRUPT As String = .NDMABANKRUPT
                    Dim NDJAICH As String = .NDJAICH
                    Dim NDJASECARR As String = .NDJASECARR
                    Dim NDJAUNSECARR As String = .NDJAUNSECARR
                    Dim NDJACCJ As String = .NDJACCJ
                    Dim NDJAIVA As String = .NDJAIVA
                    Dim NDJABANKRUPT As String = .NDJABANKRUPT

                    objExperianData.InsertConsumerData_PVD_ImpairedCH(ApplicationID _
                                                                      , NDICH, NDSECARR, NDUNSECARR, NDCCJ, NDIVA _
                                                                      , NDBANKRUPT, NDMAICH, NDMASECARR, NDMAUNSECARR _
                                                                      , NDMACCJ, NDMAIVA, NDMABANKRUPT, NDJAICH, NDJASECARR _
                                                                      , NDJAUNSECARR, NDJACCJ, NDJAIVA, NDJABANKRUPT)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_MOSAIC
    ''' </summary>
    Private Function SaveConsumerData_PVD_MOSAIC(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.Mosaic IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.Mosaic
                    Dim EA4M01 As String = .EA4M01
                    Dim EA4M02 As String = .EA4M02
                    Dim EA4M03 As String = .EA4M03
                    Dim EA4M04 As String = .EA4M04
                    Dim EA4M05 As String = .EA4M05
                    Dim EA4M06 As String = .EA4M06
                    Dim EA4M07 As String = .EA4M07
                    Dim EA4M08 As String = .EA4M08
                    Dim EA4M09 As String = .EA4M09
                    Dim EA4M10 As String = .EA4M10
                    Dim EA4M11 As String = .EA4M11
                    Dim EA4M12 As String = .EA4M12
                    Dim EA4T01 As String = .EA4T01
                    Dim EA5T01 As String = .EA5T01
                    Dim EA5T02 As String = .EA5T02
                    Dim NDG01 As String = .NDG01
                    Dim EA4N01 As String = .EA4N01
                    Dim EA4N02 As String = .EA4N02
                    Dim EA4N03 As String = .EA4N03
                    Dim EA4N04 As String = .EA4N04
                    Dim EA4N05 As String = .EA4N05
                    Dim NDG02 As String = .NDG02
                    Dim NDG03 As String = .NDG03
                    Dim NDG04 As String = .NDG04
                    Dim NDG05 As String = .NDG05
                    Dim NDG06 As String = .NDG06
                    Dim NDG07 As String = .NDG07
                    Dim NDG08 As String = .NDG08
                    Dim NDG09 As String = .NDG09
                    Dim NDG10 As String = .NDG10
                    Dim NDG11 As String = .NDG11
                    Dim NDG12 As String = .NDG12
                    Dim UKMOSAIC As String = .UKMOSAIC




                    objExperianData.InsertConsumerData_PVD_MOSAIC(ApplicationID _
                                                                      , EA4M01, EA4M02, EA4M03, EA4M04, EA4M05 _
                                                                      , EA4M06, EA4M07, EA4M08, EA4M09, EA4M10, EA4M11 _
                                                                      , EA4M12, EA4T01, EA5T01, EA5T02, NDG01, EA4N01 _
                                                                      , EA4N02, EA4N03, EA4N04, EA4N05, NDG02, NDG03, NDG04 _
                                                                      , NDG05, NDG06, NDG07, NDG08, NDG09, NDG10, NDG11 _
                                                                      , NDG12, UKMOSAIC)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB
    ''' </summary>
    Private Function SaveConsumerData_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try

            Dim TPD_NDPA As String = ""
            Dim TPD_NDHHO As String = ""
            Dim TPD_NDOPTOUTVALID As String = ""
            Dim CII_NDSPCII As String = ""
            Dim CII_NDSPACII As String = ""
            Dim AddrLink_NDLNK01 As String = ""
            Dim Director_NDDIRSP As String = ""
            Dim Director_NDDIRSPA As String = ""
            Dim AgeDoB_NDDOB As String = ""
            Dim AgeDoB_EA4S02 As String = ""
            Dim AgeDoB_EA4S04 As String = ""
            Dim AgeDoB_EA4S06 As String = ""
            Dim AgeDoB_EA4S08 As String = ""
            Dim AgeDoB_EA5S01 As String = ""
            Dim AgeDoB_EA4S01 As String = ""
            Dim AgeDoB_EA4S03 As String = ""
            Dim AgeDoB_EA4S05 As String = ""
            Dim AgeDoB_EA4S07 As String = ""

            Dim HaveData As Boolean = False

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.TPD IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.TPD
                    HaveData = True

                    TPD_NDPA = .NDPA
                    TPD_NDHHO = .NDHHO
                    TPD_NDOPTOUTVALID = .NDOPTOUTVALID
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.CII IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.CII
                    HaveData = True

                    CII_NDSPCII = .NDSPCII
                    CII_NDSPACII = .NDSPACII
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AddrLink IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AddrLink
                    HaveData = True

                    AddrLink_NDLNK01 = .NDLNK01
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.Director IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.Director
                    HaveData = True

                    Director_NDDIRSP = .NDDIRSP
                    Director_NDDIRSPA = .NDDIRSPA
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AgeDoB IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AgeDoB
                    HaveData = True

                    AgeDoB_NDDOB = If(.NDDOB Is Nothing, "", .NDDOB.ToString)
                    AgeDoB_EA4S02 = If(.EA4S02 Is Nothing, "", .EA4S02.ToString)
                    AgeDoB_EA4S04 = If(.EA4S04 Is Nothing, "", .EA4S04.ToString)
                    AgeDoB_EA4S06 = If(.EA4S06 Is Nothing, "", .EA4S06.ToString)
                    AgeDoB_EA4S08 = If(.EA4S08 Is Nothing, "", .EA4S08.ToString)
                    AgeDoB_EA5S01 = .EA5S01
                    AgeDoB_EA4S01 = .EA4S01
                    AgeDoB_EA4S03 = .EA4S03
                    AgeDoB_EA4S05 = .EA4S05
                    AgeDoB_EA4S07 = .EA4S07

                End With
            End If

            If HaveData Then
                objExperianData.InsertConsumerData_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB(ApplicationID _
                                                                         , TPD_NDPA, TPD_NDHHO, TPD_NDOPTOUTVALID _
                                                                         , CII_NDSPCII, CII_NDSPACII, AddrLink_NDLNK01 _
                                                                         , Director_NDDIRSP, Director_NDDIRSPA, AgeDoB_NDDOB _
                                                                         , AgeDoB_EA4S02, AgeDoB_EA4S04, AgeDoB_EA4S06 _
                                                                         , AgeDoB_EA4S08, AgeDoB_EA5S01, AgeDoB_EA4S01 _
                                                                         , AgeDoB_EA4S03, AgeDoB_EA4S05, AgeDoB_EA4S07)
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_ADB_Utilisationblock
    ''' </summary>
    Private Function SaveConsumerData_PVD_ADB_Utilisationblock(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.Utilisationblock IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.Utilisationblock
                    
                    Dim SPA01 As String = .SPA01
                    Dim SPA02 As String = .SPA02
                    Dim SPA03 As String = .SPA03
                    Dim SPA04 As String = .SPA04
                    Dim SPA05 As String = .SPA05
                    Dim SPA06 As String = .SPA06
                    Dim SPA07 As String = .SPA07
                    Dim SPA08 As String = .SPA08
                    Dim SPA09 As String = .SPA09
                    Dim SPA10 As String = .SPA10
                    Dim SPB111 As String = .SPB111
                    Dim SPB112 As String = .SPB112
                    Dim SPB113 As String = .SPB113
                    Dim SPB114 As String = .SPB114
                    Dim SPB115 As String = .SPB115
                    Dim SPB116 As String = .SPB116
                    Dim SPB117 As String = .SPB117
                    Dim SPB218 As String = .SPB218
                    Dim SPB219 As String = .SPB219
                    Dim SPB220 As String = .SPB220
                    Dim SPB221 As String = .SPB221
                    Dim SPB322 As String = .SPB322
                    Dim SPB323 As String = .SPB323
                    Dim SPC24 As String = .SPC24
                    Dim SPD25 As String = .SPD25
                    Dim SPE126 As String = .SPE126
                    Dim SPE127 As String = .SPE127
                    Dim SPE128 As String = .SPE128
                    Dim SPF129 As String = .SPF129
                    Dim SPF130 As String = .SPF130
                    Dim SPF131 As String = .SPF131
                    Dim SPF232 As String = .SPF232
                    Dim SPF233 As String = .SPF233
                    Dim SPF334 As String = .SPF334
                    Dim SPF335 As String = .SPF335
                    Dim SPF336 As String = .SPF336
                    Dim SPG37 As String = .SPG37
                    Dim SPG38 As String = .SPG38
                    Dim SPH39 As String = .SPH39
                    Dim SPH40 As String = .SPH40
                    Dim SPH41 As String = .SPH41
                    Dim SPCIICHECKDIGIT As String = .SPCIICHECKDIGIT
                    Dim SPAA01 As String = .SPAA01
                    Dim SPAA02 As String = .SPAA02
                    Dim SPAA03 As String = .SPAA03
                    Dim SPAA04 As String = .SPAA04
                    Dim SPAA05 As String = .SPAA05
                    Dim SPAA06 As String = .SPAA06
                    Dim SPAA07 As String = .SPAA07
                    Dim SPAA08 As String = .SPAA08
                    Dim SPAA09 As String = .SPAA09
                    Dim SPAA10 As String = .SPAA10
                    Dim SPAB111 As String = .SPAB111
                    Dim SPAB112 As String = .SPAB112
                    Dim SPAB113 As String = .SPAB113
                    Dim SPAB114 As String = .SPAB114
                    Dim SPAB115 As String = .SPAB115
                    Dim SPAB116 As String = .SPAB116
                    Dim SPAB117 As String = .SPAB117
                    Dim SPAB218 As String = .SPAB218
                    Dim SPAB219 As String = .SPAB219
                    Dim SPAB220 As String = .SPAB220
                    Dim SPAB221 As String = .SPAB221
                    Dim SPAB322 As String = .SPAB322
                    Dim SPAB323 As String = .SPAB323
                    Dim SPAC24 As String = .SPAC24
                    Dim SPAD25 As String = .SPAD25
                    Dim SPAE126 As String = .SPAE126
                    Dim SPAE127 As String = .SPAE127
                    Dim SPAE128 As String = .SPAE128
                    Dim SPAF129 As String = .SPAF129
                    Dim SPAF130 As String = .SPAF130
                    Dim SPAF131 As String = .SPAF131
                    Dim SPAF232 As String = .SPAF232
                    Dim SPAF233 As String = .SPAF233
                    Dim SPAF334 As String = .SPAF334
                    Dim SPAF335 As String = .SPAF335
                    Dim SPAF336 As String = .SPAF336
                    Dim SPAG37 As String = .SPAG37
                    Dim SPAG38 As String = .SPAG38
                    Dim SPAH39 As String = .SPAH39
                    Dim SPAH40 As String = .SPAH40
                    Dim SPAH41 As String = .SPAH41
                    Dim SPACIICHECKDIGIT As String = .SPACIICHECKDIGIT



                    objExperianData.InsertConsumerData_PVD_ADB_Utilisationblock(ApplicationID _
                                                                      , SPA01, SPA02, SPA03, SPA04, SPA05, SPA06, SPA07, SPA08, SPA09 _
                                                                      , SPA10, SPB111, SPB112, SPB113, SPB114, SPB115, SPB116, SPB117 _
                                                                      , SPB218, SPB219, SPB220, SPB221, SPB322, SPB323, SPC24, SPD25 _
                                                                      , SPE126, SPE127, SPE128, SPF129, SPF130, SPF131, SPF232, SPF233 _
                                                                      , SPF334, SPF335, SPF336, SPG37, SPG38, SPH39, SPH40, SPH41, SPCIICHECKDIGIT _
                                                                      , SPAA01, SPAA02, SPAA03, SPAA04, SPAA05, SPAA06, SPAA07, SPAA08, SPAA09 _
                                                                      , SPAA10, SPAB111, SPAB112, SPAB113, SPAB114, SPAB115, SPAB116, SPAB117 _
                                                                      , SPAB218, SPAB219, SPAB220, SPAB221, SPAB322, SPAB323, SPAC24, SPAD25, SPAE126 _
                                                                      , SPAE127, SPAE128, SPAF129, SPAF130, SPAF131, SPAF232, SPAF233, SPAF334, SPAF335 _
                                                                      , SPAF336, SPAG37, SPAG38, SPAH39, SPAH40, SPAH41, SPACIICHECKDIGIT)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_ADB_TelecommsBlock
    ''' </summary>
    Private Function SaveConsumerData_PVD_ADB_TelecommsBlock(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.TelecommsBlock IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.TelecommsBlock
                    Dim SPNODEL As String = .SPNODEL
                    Dim SPVALDEL As String = .SPVALDEL
                    Dim SPTSMRTELDEL As String = .SPTSMRTELDEL
                    Dim SPNOACTTEL As String = .SPNOACTTEL
                    Dim SPBALACTTEL As String = .SPBALACTTEL
                    Dim SPTSMRTEL As String = .SPTSMRTEL
                    Dim SPTOTNOACTTEL As String = .SPTOTNOACTTEL
                    Dim SPNOINACTTELL12 As String = .SPNOINACTTELL12
                    Dim SPNOINACTTELL24 As String = .SPNOINACTTELL24
                    Dim SPNOINACTTEL36 As String = .SPNOINACTTEL36
                    Dim SPTOTNOSET As String = .SPTOTNOSET
                    Dim SPTOTNOSETL12 As String = .SPTOTNOSETL12
                    Dim SPTOTNOSETL24 As String = .SPTOTNOSETL24
                    Dim SPTOTNOSETL36 As String = .SPTOTNOSETL36
                    Dim SPTSMRSETTEL As String = .SPTSMRSETTEL


                    objExperianData.InsertConsumerData_PVD_ADB_TelecommsBlock(ApplicationID _
                                                                      , SPNODEL, SPVALDEL, SPTSMRTELDEL, SPNOACTTEL, SPBALACTTEL, SPTSMRTEL _
                                                                      , SPTOTNOACTTEL, SPNOINACTTELL12, SPNOINACTTELL24, SPNOINACTTEL36, SPTOTNOSET _
                                                                      , SPTOTNOSETL12, SPTOTNOSETL24, SPTOTNOSETL36,SPTSMRSETTEL)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_ADB_NeverPaidDefsBlock
    ''' </summary>
    Private Function SaveConsumerData_PVD_ADB_NeverPaidDefsBlock(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.NeverPaidDefsBlock IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.NeverPaidDefsBlock
                    Dim SPNONPDL12 As String = .SPNONPDL12
                    Dim SPBALNPDL12 As String = .SPBALNPDL12
                    Dim SPNONPD As String = .SPNONPD
                    Dim SPBALNPD As String = .SPBALNPD
                    Dim SPTSMRNPD As String = .SPTSMRNPD
                    Dim SPNOEBADSL12 As String = .SPNOEBADSL12
                    Dim SPBALEBADSL12 As String = .SPBALEBADSL12
                    Dim SPNOEBADS As String = .SPNOEBADS
                    Dim SPBALEBADS As String = .SPBALEBADS
                    Dim SPTSMREBAD As String = .SPTSMREBAD
                    Dim SPMTHFSTEBAD As String = .SPMTHFSTEBAD
                    Dim SPANONPDL12 As String = .SPANONPDL12
                    Dim SPABALNPDL12 As String = .SPABALNPDL12
                    Dim SPANONPD As String = .SPANONPD
                    Dim SPABALNPD As String = .SPABALNPD
                    Dim SPATSMRNPD As String = .SPATSMRNPD
                    Dim SPANOEBADSL12 As String = .SPANOEBADSL12
                    Dim SPABALEBADSL12 As String = .SPABALEBADSL12
                    Dim SPANOEBADS As String = .SPANOEBADS
                    Dim SPABALEBADS As String = .SPABALEBADS
                    Dim SPATSMREBAD As String = .SPATSMREBAD
                    Dim SPAMTHFSTEBAD As String = .SPAMTHFSTEBAD


                    objExperianData.InsertConsumerData_PVD_ADB_NeverPaidDefsBlock(ApplicationID _
                                                                      , SPNONPDL12, SPBALNPDL12, SPNONPD, SPBALNPD, SPTSMRNPD _
                                                                      , SPNOEBADSL12, SPBALEBADSL12, SPNOEBADS, SPBALEBADS _
                                                                      , SPTSMREBAD, SPMTHFSTEBAD, SPANONPDL12, SPABALNPDL12, SPANONPD _
                                                                      , SPABALNPD, SPATSMRNPD, SPANOEBADSL12, SPABALEBADSL12, SPANOEBADS _
                                                                      , SPABALEBADS, SPATSMREBAD,SPAMTHFSTEBAD)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PVD_ADB_APACSCCBehavrlData
    ''' </summary>
    Private Function SaveConsumerData_PVD_ADB_APACSCCBehavrlData(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.APACSCCBehavrlData IsNot Nothing Then
                With output.Output.ConsumerSummary.PremiumValueData.AdditDelphiBlocks.APACSCCBehavrlData
                    Dim CCDATASUPPLIED As String = .CCDATASUPPLIED
                    Dim NOMPMNPRL3M As String = .NOMPMNPRL3M
                    Dim PTBRL3MNPRL3M As String = .PTBRL3MNPRL3M
                    Dim PTBRL6MNPRL6M As String = .PTBRL6MNPRL6M
                    Dim NOCAL1M As String = .NOCAL1M
                    Dim NOCAL3M As String = .NOCAL3M
                    Dim NOMLVCAL1M As String = .NOMLVCAL1M
                    Dim NOMLVCAL3M As String = .NOMLVCAL3M
                    Dim CLUCLIL6M As String = .CLUCLIL6M
                    Dim CLUCLIL6MNPRL6M As String = .CLUCLIL6MNPRL6M
                    Dim CLUNPRL1M As String = .CLUNPRL1M
                    Dim NOCLDL3M As String = .NOCLDL3M
                    Dim NOASBNPRL1M As String = .NOASBNPRL1M


                    objExperianData.InsertConsumerData_PVD_ADB_APACSCCBehavrlData(ApplicationID _
                                                                      , CCDATASUPPLIED, NOMPMNPRL3M, PTBRL3MNPRL3M, PTBRL6MNPRL6M, NOCAL1M, NOCAL3M _
                                                                      , NOMLVCAL1M, NOMLVCAL3M, CLUCLIL6M, CLUCLIL6MNPRL6M, CLUNPRL1M, NOCLDL3M _
                                                                      ,NOASBNPRL1M)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_PublicInfo
    ''' </summary>
    Private Function SaveConsumerData_PublicInfo(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.PublicInfo IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.PublicInfo
                    Dim E1A01 As String = .E1A01
                    Dim E1A02 As String = .E1A02
                    Dim E1A03 As String = .E1A03
                    Dim EA1C01 As String = .EA1C01
                    Dim EA1D01 As String = .EA1D01
                    Dim EA1D02 As String = .EA1D02
                    Dim EA1D03 As String = .EA1D03
                    Dim E2G01 As String = .E2G01
                    Dim E2G02 As String = .E2G02
                    Dim E2G03 As String = .E2G03
                    Dim EA2I01 As String = .EA2I01
                    Dim EA2J01 As String = .EA2J01
                    Dim EA2J02 As String = .EA2J02
                    Dim EA2J03 As String = .EA2J03
                    Dim EA4Q06 As String = .EA4Q06
                    Dim SPABRPRESENT As String = .SPABRPRESENT
                    Dim SPBRPRESENT As String = .SPBRPRESENT


                    objExperianData.InsertConsumerData_PublicInfo(ApplicationID _
                                                                      , E1A01, E1A02, E1A03, EA1C01, EA1D01, EA1D02 _
                                                                      , EA1D03, E2G01, E2G02, E2G03, EA2I01, EA2J01 _
                                                                      , EA2J02, EA2J03, EA4Q06, SPABRPRESENT, SPBRPRESENT)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_CIFAS_CML_GAIN_NOC_TPD
    ''' </summary>
    Private Function SaveConsumerData_CIFAS_CML_GAIN_NOC_TPD(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim CIFAS_EA1A01 As String = ""
            Dim CIFAS_EA2G01 As String = ""
            Dim CIFAS_EA4P01 As String = ""
            Dim CML_EA1C02 As String = ""
            Dim CML_EA2I02 As String = ""
            Dim GAIN_EA1G01 As String = ""
            Dim GAIN_EA1G02 As String = ""
            Dim GAIN_EA2M01 As String = ""
            Dim GAIN_EA2M02 As String = ""
            Dim NOC_EA4Q07 As String = ""
            Dim NOC_EA4Q08 As String = ""
            Dim NOC_EA4Q09 As String = ""
            Dim NOC_EA4Q10 As String = ""
            Dim NOC_EA4Q11 As String = ""
            Dim NOC_EA4Q01 As String = ""
            Dim NOC_EA4Q02 As String = ""
            Dim NOC_EA4Q03 As String = ""
            Dim NOC_EA4Q04 As String = ""
            Dim NOC_EA4Q05 As String = ""
            Dim TPD_NDOPTOUT As String = ""

            Dim Havedata As Boolean = False

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.CIFAS IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.CIFAS

                    Havedata = True

                    CIFAS_EA1A01 = .EA1A01
                    CIFAS_EA2G01 = .EA2G01
                    CIFAS_EA4P01 = .EA4P01
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.CML IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.CML

                    Havedata = True

                    CML_EA1C02 = .EA1C02
                    CML_EA2I02 = .EA2I02
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.GAIN IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.GAIN

                    Havedata = True

                    GAIN_EA1G01 = .EA1G01
                    GAIN_EA1G02 = .EA1G02
                    GAIN_EA2M01 = .EA2M01
                    GAIN_EA2M02 = .EA2M02
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.NOC IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.NOC

                    Havedata = True

                    NOC_EA4Q07 = .EA4Q07
                    NOC_EA4Q08 = .EA4Q08
                    NOC_EA4Q09 = .EA4Q09
                    NOC_EA4Q10 = .EA4Q10
                    NOC_EA4Q11 = .EA4Q11
                    NOC_EA4Q01 = .EA4Q01
                    NOC_EA4Q02 = .EA4Q02
                    NOC_EA4Q03 = .EA4Q03
                    NOC_EA4Q04 = .EA4Q04
                    NOC_EA4Q05 = .EA4Q05
                End With
            End If

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.TPD IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.TPD

                    Havedata = True

                    TPD_NDOPTOUT = .NDOPTOUT
                End With
            End If

            If Havedata Then
                objExperianData.InsertConsumerData_CIFAS_CML_GAIN_NOC_TPD(ApplicationID _
                                                                      , CIFAS_EA1A01, CIFAS_EA2G01, CIFAS_EA4P01, CML_EA1C02, CML_EA2I02 _
                                                                      , GAIN_EA1G01, GAIN_EA1G02, GAIN_EA2M01, GAIN_EA2M02, NOC_EA4Q07 _
                                                                      , NOC_EA4Q08, NOC_EA4Q09, NOC_EA4Q10, NOC_EA4Q11, NOC_EA4Q01 _
                                                                      , NOC_EA4Q02, NOC_EA4Q03, NOC_EA4Q04, NOC_EA4Q05,TPD_NDOPTOUT)
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_CAPS
    ''' </summary>
    Private Function SaveConsumerData_CAPS(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.CAPS IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.CAPS
                    Dim E1E01 As String = .E1E01
                    Dim E1E02 As String = .E1E02
                    Dim EA1B01 As String = .EA1B01
                    Dim NDPSD01 As String = .NDPSD01
                    Dim NDPSD02 As String = .NDPSD02
                    Dim NDPSD03 As String = .NDPSD03
                    Dim NDPSD04 As String = .NDPSD04
                    Dim NDPSD05 As String = .NDPSD05
                    Dim NDPSD06 As String = .NDPSD06
                    Dim EA1E01 As String = .EA1E01
                    Dim EA1E02 As String = .EA1E02
                    Dim EA1E03 As String = .EA1E03
                    Dim EA1E04 As String = .EA1E04
                    Dim E2K01 As String = .E2K01
                    Dim E2K02 As String = .E2K02
                    Dim EA2H01 As String = .EA2H01
                    Dim NDPSD07 As String = .NDPSD07
                    Dim NDPSD08 As String = .NDPSD08
                    Dim NDPSD09 As String = .NDPSD09
                    Dim NDPSD10 As String = .NDPSD10
                    Dim EA2K01 As String = .EA2K01
                    Dim EA2K02 As String = .EA2K02
                    Dim EA2K03 As String = .EA2K03
                    Dim EA2K04 As String = .EA2K04
                    Dim NDPSD11 As String = .NDPSD11


                    objExperianData.InsertConsumerData_CAPS(ApplicationID _
                                                                      , E1E01, E1E02, EA1B01, NDPSD01, NDPSD02, NDPSD03, NDPSD04 _
                                                                      , NDPSD05, NDPSD06, EA1E01, EA1E02, EA1E03, EA1E04, E2K01 _
                                                                      , E2K02, EA2H01, NDPSD07, NDPSD08, NDPSD09, NDPSD10, EA2K01 _
                                                                      , EA2K02, EA2K03, EA2K04, NDPSD11)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_CAIS
    ''' </summary>
    Private Function SaveConsumerData_CAIS(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary IsNot Nothing AndAlso output.Output.ConsumerSummary.Summary.CAIS IsNot Nothing Then
                With output.Output.ConsumerSummary.Summary.CAIS
                    Dim E1A04 As String = .E1A04
                    Dim E1A05 As String = .E1A05
                    Dim E1A06 As String = .E1A06
                    Dim E1A07 As String = .E1A07
                    Dim E1A08 As String = .E1A08
                    Dim E1A09 As String = .E1A09
                    Dim E1A10 As String = .E1A10
                    Dim E1A11 As String = .E1A11
                    Dim E1B01 As String = .E1B01
                    Dim E1B02 As String = .E1B02
                    Dim E1B03 As String = .E1B03
                    Dim E1B04 As String = .E1B04
                    Dim E1B05 As String = .E1B05
                    Dim E1B06 As String = .E1B06
                    Dim E1B07 As String = .E1B07
                    Dim E1B08 As String = .E1B08
                    Dim E1B09 As String = .E1B09
                    Dim E1B10 As String = .E1B10
                    Dim E1B11 As String = .E1B11
                    Dim E1B12 As String = .E1B12
                    Dim E1B13 As String = .E1B13
                    Dim NDECC01 As String = .NDECC01
                    Dim NDECC02 As String = .NDECC02
                    Dim NDECC03 As String = .NDECC03
                    Dim NDECC04 As String = .NDECC04
                    Dim NDECC07 As String = .NDECC07
                    Dim NDECC08 As String = .NDECC08
                    Dim E1C01 As String = .E1C01
                    Dim E1C02 As String = .E1C02
                    Dim E1C03 As String = .E1C03
                    Dim E1C04 As String = .E1C04
                    Dim E1C05 As String = .E1C05
                    Dim E1C06 As String = .E1C06
                    Dim EA1B02 As String = .EA1B02
                    Dim E1D01 As String = .E1D01
                    Dim E1D02 As String = .E1D02
                    Dim E1D03 As String = .E1D03
                    Dim E1D04 As String = .E1D04
                    Dim NDHAC01 As String = .NDHAC01
                    Dim NDHAC02 As String = .NDHAC02
                    Dim NDHAC03 As String = .NDHAC03
                    Dim NDHAC04 As String = .NDHAC04
                    Dim NDHAC05 As String = .NDHAC05
                    Dim NDHAC09 As String = .NDHAC09
                    Dim NDINC01 As String = .NDINC01
                    Dim EA1F01 As String = .EA1F01
                    Dim EA1F02 As String = .EA1F02
                    Dim EA1F03 As String = .EA1F03
                    Dim NDFCS01 As String = .NDFCS01
                    Dim EA1F04 As String = .EA1F04
                    Dim E2G04 As String = .E2G04
                    Dim E2G05 As String = .E2G05
                    Dim E2G06 As String = .E2G06
                    Dim E2G07 As String = .E2G07
                    Dim E2G08 As String = .E2G08
                    Dim E2G09 As String = .E2G09
                    Dim E2G10 As String = .E2G10
                    Dim E2G11 As String = .E2G11
                    Dim E2H01 As String = .E2H01
                    Dim E2H02 As String = .E2H02
                    Dim E2H03 As String = .E2H03
                    Dim E2H04 As String = .E2H04
                    Dim E2H05 As String = .E2H05
                    Dim E2H06 As String = .E2H06
                    Dim E2H07 As String = .E2H07
                    Dim E2H08 As String = .E2H08
                    Dim E2H09 As String = .E2H09
                    Dim E2H10 As String = .E2H10
                    Dim E2H11 As String = .E2H11
                    Dim E2H12 As String = .E2H12
                    Dim E2H13 As String = .E2H13
                    Dim NDECC05 As String = .NDECC05
                    Dim NDECC09 As String = .NDECC09
                    Dim NDECC10 As String = .NDECC10
                    Dim E2I01 As String = .E2I01
                    Dim E2I02 As String = .E2I02
                    Dim E2I03 As String = .E2I03
                    Dim E2I04 As String = .E2I04
                    Dim E2I05 As String = .E2I05
                    Dim E2I06 As String = .E2I06
                    Dim EA2H02 As String = .EA2H02
                    Dim E2J01 As String = .E2J01
                    Dim E2J02 As String = .E2J02
                    Dim E2J03 As String = .E2J03
                    Dim E2J04 As String = .E2J04
                    Dim NDHAC10 As String = .NDHAC10
                    Dim NDHAC06 As String = .NDHAC06
                    Dim NDHAC07 As String = .NDHAC07
                    Dim NDHAC08 As String = .NDHAC08
                    Dim NDINC02 As String = .NDINC02
                    Dim EA2L01 As String = .EA2L01
                    Dim EA2L02 As String = .EA2L02
                    Dim EA2L03 As String = .EA2L03
                    Dim NDFCS02 As String = .NDFCS02
                    Dim EA2I04 As String = ""   '.EA2I04 - Not available for some reason
                    Dim NDECC06 As String = .NDECC06
                    Dim NDINC03 As String = .NDINC03


                    objExperianData.InsertConsumerData_CAIS(ApplicationID _
                                                                      , E1A04, E1A05, E1A06, E1A07, E1A08, E1A09, E1A10, E1A11, E1B01, E1B02 _
                                                                      , E1B03, E1B04, E1B05, E1B06, E1B07, E1B08, E1B09, E1B10, E1B11, E1B12 _
                                                                      , E1B13, NDECC01, NDECC02, NDECC03, NDECC04, NDECC07, NDECC08, E1C01 _
                                                                      , E1C02, E1C03, E1C04, E1C05, E1C06, EA1B02, E1D01, E1D02, E1D03, E1D04 _
                                                                      , NDHAC01, NDHAC02, NDHAC03, NDHAC04, NDHAC05, NDHAC09, NDINC01, EA1F01 _
                                                                      , EA1F02, EA1F03, NDFCS01, EA1F04, E2G04, E2G05, E2G06, E2G07, E2G08, E2G09 _
                                                                      , E2G10, E2G11, E2H01, E2H02, E2H03, E2H04, E2H05, E2H06, E2H07, E2H08, E2H09 _
                                                                      , E2H10, E2H11, E2H12, E2H13, NDECC05, NDECC09, NDECC10, E2I01, E2I02, E2I03 _
                                                                      , E2I04, E2I05, E2I06, EA2H02, E2J01, E2J02, E2J03, E2J04, NDHAC10, NDHAC06, NDHAC07 _
                                                                      , NDHAC08, NDINC02, EA2L01, EA2L02, EA2L03, NDFCS02, EA2I04, NDECC06,NDINC03)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_FullConData_CDS_SD_CAISSummary
    ''' </summary>
    Private Function SaveConsumerData_FullConData_CDS_SD_CAISSummary(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim Num3To6Months As String = ""
            Dim Num6To12Months As String = ""
            Dim NumberUpTo3Months As String = ""
            Dim TotalNumber As String = ""
            Dim GoneAway As String = ""
            Dim Number As String = ""
            Dim TotalBalance As String = ""
            Dim WorstCurrent As String = ""
            Dim WorstHistorical As String = ""

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.FullConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails.CAISSummary IsNot Nothing Then
                With output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails.CAISSummary

                    If .CAISPrevSearches IsNot Nothing Then
                        With .CAISPrevSearches
                            Num3To6Months = .Num3To6Months
                            Num6To12Months = .Num6To12Months
                            NumberUpTo3Months = .NumberUpTo3Months
                            TotalNumber = .TotalNumber

                        End With
                    End If
                    GoneAway = .GoneAway
                    Number = .Number
                    TotalBalance = .TotalBalance
                    WorstCurrent = .WorstCurrent
                    WorstHistorical = .WorstHistorical


                    objExperianData.InsertConsumerData_FULLCONDATA_CDS_SD_CAISSUMMARY(ApplicationID _
                                                                      , Num3To6Months, Num6To12Months, NumberUpTo3Months, TotalNumber _
                                                                      , GoneAway, Number, TotalBalance, WorstCurrent, WorstHistorical)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_FullConData_CDS_SD_SummaryFlags
    ''' </summary>
    Private Function SaveConsumerData_FullConData_CDS_SD_SummaryFlags(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim Num3To6Months As String = ""
            Dim Num6To12Months As String = ""
            Dim NumberUpTo3Months As String = ""
            Dim TotalNumber As String = ""
            Dim GoneAway As String = ""
            Dim Number As String = ""
            Dim TotalBalance As String = ""
            Dim WorstCurrent As String = ""
            Dim WorstHistorical As String = ""

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.FullConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails.SummaryFlags IsNot Nothing Then

                With output.Output.FullConsumerData.ConsumerDataSummary.SummaryDetails.SummaryFlags
                    Dim AccountQuery As String = .AccountQuery
                    Dim AdditionalLocation As String = .AdditionalLocation
                    Dim Arrangement As String = .Arrangement
                    Dim CIFAS As String = .CIFAS
                    Dim ClaimedOnCreditIns As String = .ClaimedOnCreditIns
                    Dim CML As String = .CML
                    Dim DebtAssign As String = .DebtAssign
                    Dim DebtManagement As String = .DebtManagement
                    Dim ForwardLocation As String = .ForwardLocation
                    Dim NoConfirmAtPrevLoc As String = .NoConfirmAtPrevLoc
                    Dim OwnAccount As String = .OwnAccount
                    Dim OwnSearch As String = .OwnSearch
                    Dim PartSettle As String = .PartSettle
                    Dim PreviousLocation As String = .PreviousLocation
                    Dim PreviousOccupant As String = .PreviousOccupant
                    Dim Recourse As String = .Recourse
                    Dim ReportedDeceased As String = .ReportedDeceased
                    Dim ReportedGoneAway As String = .ReportedGoneAway
                    Dim VoluntaryTerm As String = .VoluntaryTerm


                    objExperianData.InsertConsumerData_FULLCONDATA_CDS_SD_SUMMARYFLAGS(ApplicationID _
                                                                      , AccountQuery, AdditionalLocation, Arrangement, CIFAS, ClaimedOnCreditIns _
                                                                      , CML, DebtAssign, DebtManagement, ForwardLocation, NoConfirmAtPrevLoc, OwnAccount _
                                                                      , OwnSearch, PartSettle, PreviousLocation, PreviousOccupant, Recourse, ReportedDeceased, ReportedGoneAway, VoluntaryTerm)
                End With
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_FullConData_CD_Association
    ''' </summary>
    Private Function SaveConsumerData_FullConData_CD_Association(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim ApplicantIndicator As String = ""
            Dim LocationIndicator As String = ""
            Dim AssociateName_Forename As String = ""
            Dim AssociateName_MiddleName As String = ""
            Dim AssociateName_Suffix As String = ""
            Dim AssociateName_Surname As String = ""
            Dim AssociateName_Title As String = ""
            Dim CompanyType As String = ""
            Dim DateOfBirth_CCYY As String = ""
            Dim DateOfBirth_DD As String = ""
            Dim DateOfBirth_MM As String = ""
            Dim DoBAssociateOrAlias_CCYY As String = ""
            Dim DoBAssociateOrAlias_DD As String = ""
            Dim DoBAssociateOrAlias_MM As String = ""
            Dim InformationDate_CCYY As String = ""
            Dim InformationDate_DD As String = ""
            Dim InformationDate_MM As String = ""
            Dim InformationSource As String = ""
            Dim InformationSupplier As String = ""
            Dim InformationType As String = ""
            Dim Location_Country As String = ""
            Dim Location_County As String = ""
            Dim Location_District As String = ""
            Dim Location_District2 As String = ""
            Dim Location_Flat As String = ""
            Dim Location_HouseName As String = ""
            Dim Location_HouseNumber As String = ""
            Dim Location_POBox As String = ""
            Dim Location_Postcode As String = ""
            Dim Location_PostTown As String = ""
            Dim Location_SharedLetterbox As String = ""
            Dim Location_Street As String = ""
            Dim Location_Street2 As String = ""
            Dim MatchDetails_BureauRefCategory As String = ""
            Dim MatchDetails_HouseMatchLevel As String = ""
            Dim MatchDetails_MatchTo As String = ""
            Dim MatchDetails_MatchType As String = ""
            Dim MatchDetails_StreetMatchLevel As String = ""
            Dim Name_Forename As String = ""
            Dim Name_MiddleName As String = ""
            Dim Name_Prefix As String = ""
            Dim Name_Suffix As String = ""
            Dim Name_Surname As String = ""
            Dim Name_Title As String = ""
            Dim NoCReference As String = ""
            Dim Source As String = ""
            Dim SupplierBranch As String = ""
            Dim SupplyCompanyName As String = ""

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.FullConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData.Association IsNot Nothing Then

                'Delete existing rows if any
                objExperianData.DeleteConsumerData_FULLCONDATA_CD_ASSOCIATION(ApplicationID)

                For Each asso As ConsData.OutputFullConsumerDataConsumerDataAssociation In output.Output.FullConsumerData.ConsumerData.Association
                    With asso

                        ApplicantIndicator = .ApplicantIndicator
                        LocationIndicator = .LocationIndicator

                        For Each assoDetail As ConsData.OutputFullConsumerDataConsumerDataAssociationAssociationDetails In .AssociationDetails
                            With assoDetail
                                If .AssociateName IsNot Nothing Then
                                    With .AssociateName
                                        AssociateName_Forename = .Forename
                                        AssociateName_MiddleName = .MiddleName
                                        AssociateName_Suffix = .Suffix
                                        AssociateName_Surname = .Surname
                                        AssociateName_Title = .Title
                                    End With
                                End If
                                CompanyType = .CompanyType

                                If .DateOfBirth IsNot Nothing Then
                                    With .DateOfBirth
                                        DateOfBirth_CCYY = .CCYY
                                        DateOfBirth_DD = .DD
                                        DateOfBirth_MM = .MM
                                    End With
                                End If

                                If .DoBAssociateOrAlias IsNot Nothing Then
                                    With .DoBAssociateOrAlias
                                        DoBAssociateOrAlias_CCYY = .CCYY
                                        DoBAssociateOrAlias_DD = .DD
                                        DoBAssociateOrAlias_MM = .MM
                                    End With
                                End If

                                If .InformationDate IsNot Nothing Then
                                    With .InformationDate
                                        InformationDate_CCYY = .CCYY
                                        InformationDate_DD = .DD
                                        InformationDate_MM = .MM
                                    End With
                                End If

                                InformationSource = .InformationSource
                                InformationSupplier = .InformationSupplier
                                InformationType = .InformationType

                                If .Location IsNot Nothing Then
                                    With .Location
                                        Location_Country = .Country
                                        Location_County = .County
                                        Location_District = .District
                                        Location_District2 = .District2
                                        Location_Flat = .Flat
                                        Location_HouseName = .HouseName
                                        Location_HouseNumber = .HouseNumber
                                        Location_POBox = .POBox
                                        Location_Postcode = .Postcode
                                        Location_PostTown = .PostTown
                                        Location_SharedLetterbox = .SharedLetterbox
                                        Location_Street = .Street
                                        Location_Street2 = .Street2
                                    End With
                                End If

                                If .MatchDetails IsNot Nothing Then
                                    With .MatchDetails
                                        MatchDetails_BureauRefCategory = .BureauRefCategory
                                        MatchDetails_HouseMatchLevel = .HouseMatchLevel
                                        MatchDetails_MatchTo = .MatchTo
                                        MatchDetails_MatchType = .MatchType
                                        MatchDetails_StreetMatchLevel = .StreetMatchLevel
                                    End With
                                End If

                                If .Name IsNot Nothing Then
                                    With .Name
                                        Name_Forename = .Forename
                                        Name_MiddleName = .MiddleName
                                        Name_Prefix = .Prefix
                                        Name_Suffix = .Suffix
                                        Name_Surname = .Surname
                                        Name_Title = .Title
                                    End With
                                End If

                                NoCReference = .NoCReference
                                Source = .Source
                                SupplierBranch = .SupplierBranch
                                SupplyCompanyName = .SupplyCompanyName
                            End With

                            objExperianData.InsertConsumerData_FULLCONDATA_CD_ASSOCIATION(ApplicationID _
                                                                      , ApplicantIndicator, LocationIndicator, AssociateName_Forename, AssociateName_MiddleName, AssociateName_Suffix _
                                                                      , AssociateName_Surname, AssociateName_Title, CompanyType, DateOfBirth_CCYY, DateOfBirth_DD _
                                                                      , DateOfBirth_MM, DoBAssociateOrAlias_CCYY, DoBAssociateOrAlias_DD, DoBAssociateOrAlias_MM, InformationDate_CCYY _
                                                                      , InformationDate_DD, InformationDate_MM, InformationSource, InformationSupplier, InformationType, Location_Country, Location_County _
                                                                      , Location_District, Location_District2, Location_Flat, Location_HouseName, Location_HouseNumber, Location_POBox _
                                                                      , Location_Postcode, Location_PostTown, Location_SharedLetterbox, Location_Street, Location_Street2, MatchDetails_BureauRefCategory _
                                                                      , MatchDetails_HouseMatchLevel, MatchDetails_MatchTo, MatchDetails_MatchType, MatchDetails_StreetMatchLevel _
                                                                      , Name_Forename, Name_MiddleName, Name_Prefix, Name_Suffix, Name_Surname, Name_Title, NoCReference, Source _
                                                                      , SupplierBranch, SupplyCompanyName)


                            AssociateName_Forename = ""
                            AssociateName_MiddleName = ""
                            AssociateName_Suffix = ""
                            AssociateName_Surname = ""
                            AssociateName_Title = ""
                            CompanyType = ""
                            DateOfBirth_CCYY = ""
                            DateOfBirth_DD = ""
                            DateOfBirth_MM = ""
                            DoBAssociateOrAlias_CCYY = ""
                            DoBAssociateOrAlias_DD = ""
                            DoBAssociateOrAlias_MM = ""
                            InformationDate_CCYY = ""
                            InformationDate_DD = ""
                            InformationDate_MM = ""
                            InformationSource = ""
                            InformationSupplier = ""
                            InformationType = ""
                            Location_Country = ""
                            Location_County = ""
                            Location_District = ""
                            Location_District2 = ""
                            Location_Flat = ""
                            Location_HouseName = ""
                            Location_HouseNumber = ""
                            Location_POBox = ""
                            Location_Postcode = ""
                            Location_PostTown = ""
                            Location_SharedLetterbox = ""
                            Location_Street = ""
                            Location_Street2 = ""
                            MatchDetails_BureauRefCategory = ""
                            MatchDetails_HouseMatchLevel = ""
                            MatchDetails_MatchTo = ""
                            MatchDetails_MatchType = ""
                            MatchDetails_StreetMatchLevel = ""
                            Name_Forename = ""
                            Name_MiddleName = ""
                            Name_Prefix = ""
                            Name_Suffix = ""
                            Name_Surname = ""
                            Name_Title = ""
                            NoCReference = ""
                            Source = ""
                            SupplierBranch = ""
                            SupplyCompanyName = ""
                        Next

                        ApplicantIndicator = ""
                        LocationIndicator = ""
                    End With
                Next

                
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_FullConData_CD_CAIS
    ''' </summary>
    Private Function SaveConsumerData_FullConData_CD_CAIS(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim ApplicantIndicator As String = ""
            Dim LocationIndicator As String = ""
            Dim AccountBalancesCount As Integer = 0
            Dim AccountBalance As String = ""
            Dim Status As String = ""
            Dim AccountNumber As String = ""
            Dim AccountStatus As String = ""
            Dim AccountStatusCodes As String = ""
            Dim AccountType As String = ""
            Dim Balance_Amount As String = ""
            Dim Balance_Caption As String = ""
            Dim Balance_Narrative As String = ""
            Dim BankFlag As String = ""
            Dim CAISAccStartDate_CCYY As String = ""
            Dim CAISAccStartDate_DD As String = ""
            Dim CAISAccStartDate_MM As String = ""
            Dim CMLLocationType As String = ""
            Dim CompanyType As String = ""
            Dim CreditLimit_Amount As String = ""
            Dim CreditLimit_Caption As String = ""
            Dim CreditLimit_Narrative As String = ""
            Dim CurrentDefBalance_Amount As String = ""
            Dim CurrentDefBalance_Narrative As String = ""
            Dim DelinquentBalance_Amount As String = ""
            Dim DelinquentBalance_Narrative As String = ""
            Dim FSCCUGIndicator As String = ""
            Dim InformationSource As String = ""
            Dim JointAccount As String = ""
            Dim LastUpdatedDate_CCYY As String = ""
            Dim LastUpdatedDate_DD As String = ""
            Dim LastUpdatedDate_MM As String = ""
            Dim MatchDetails_BureauRefCategory As String = ""
            Dim MatchDetails_ForenameMatchLevel As String = ""
            Dim MatchDetails_HouseMatchLevel As String = ""
            Dim MatchDetails_MatchTo As String = ""
            Dim MatchDetails_MatchType As String = ""
            Dim MatchDetails_StreetMatchLevel As String = ""
            Dim MatchDetails_SurnameMatchLevel As String = ""
            Dim MnthlyRepaymentCount As Integer = 0
            Dim MnthlyRepayment As String = ""
            Dim MnthlyRepyChngDate As String = ""
            Dim Name_Forename As String = ""
            Dim Name_MiddleName As String = ""
            Dim Name_Prefix As String = ""
            Dim Name_Suffix As String = ""
            Dim Name_Surname As String = ""
            Dim Name_Title As String = ""
            Dim NoCReference As String = ""
            Dim NoOfPayments As String = ""
            Dim NoOfRepaymntPeriod As String = ""
            Dim NumAccountBalances As String = ""
            Dim NumAddInfoBlocks As String = ""
            Dim NumCardHistories As String = ""
            Dim NumCreditLimChngs As String = ""
            Dim NumOfMonthsHistory As String = ""
            Dim NumStatuses As String = ""
            Dim OwnData As String = ""
            Dim Payment As String = ""
            Dim PaymentFrequency As String = ""
            Dim RepaymentPeriodsCount As Integer = 0
            Dim RepaymentPeriod As String = ""
            Dim RepayPdChangeDate As String = ""
            Dim SettleDateCaption As String = ""
            Dim SettlementDate_CCYY As String = ""
            Dim SettlementDate_MM As String = ""
            Dim SettlementDate_DD As String = ""
            Dim Sex As String = ""
            Dim SourceCode As String = ""
            Dim Status1To2 As String = ""
            Dim StatusTo3 As String = ""
            Dim SupplyCompanyName As String = ""
            Dim TransColnAcntFlag As String = ""
            Dim WorstStatus As String = ""

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.FullConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData.CAIS IsNot Nothing Then

                'Delete existing rows if any
                objExperianData.DeleteConsumerData_FULLCONDATA_CD_CAIS(ApplicationID)

                For Each asso As ConsData.OutputFullConsumerDataConsumerDataCAIS In output.Output.FullConsumerData.ConsumerData.CAIS
                    With asso

                        ApplicantIndicator = .ApplicantIndicator
                        LocationIndicator = .LocationIndicator

                        For Each assoDetail As ConsData.OutputFullConsumerDataConsumerDataCAISCAISDetails In .CAISDetails
                            With assoDetail
                                If .AccountBalances IsNot Nothing Then
                                    AccountBalancesCount = .AccountBalances.Count

                                    For Each ab In .AccountBalances
                                        AccountBalance = AccountBalance & "," & ab.AccountBalance
                                        Status = Status & "," & ab.Status
                                    Next
                                End If

                                AccountNumber = .AccountNumber
                                AccountStatus = .AccountStatus
                                AccountStatusCodes = .AccountStatusCodes
                                AccountType = .AccountType

                                If .Balance IsNot Nothing Then
                                    With .Balance
                                        Balance_Amount = .Amount
                                        Balance_Caption = .Caption
                                        Balance_Narrative = .Narrative
                                    End With
                                End If
                                BankFlag = .BankFlag

                                If .CAISAccStartDate IsNot Nothing Then
                                    With .CAISAccStartDate
                                        CAISAccStartDate_CCYY = .CCYY
                                        CAISAccStartDate_DD = .DD
                                        CAISAccStartDate_MM = .MM
                                    End With
                                End If
                                CMLLocationType = .CMLLocationType
                                CompanyType = .CompanyType

                                If .CreditLimit IsNot Nothing Then
                                    With .CreditLimit
                                        CreditLimit_Amount = .Amount
                                        CreditLimit_Caption = .Caption
                                        CreditLimit_Narrative = .Narrative
                                    End With
                                End If

                                If .CurrentDefBalance IsNot Nothing Then
                                    With .CurrentDefBalance
                                        CurrentDefBalance_Amount = .Amount
                                        CurrentDefBalance_Narrative = .Narrative
                                    End With
                                End If

                                If .DelinquentBalance IsNot Nothing Then
                                    With .DelinquentBalance
                                        DelinquentBalance_Amount = .Amount
                                        DelinquentBalance_Narrative = .Narrative
                                    End With
                                End If
                                
                                FSCCUGIndicator = .FSCCUGIndicator
                                InformationSource = .InformationSource
                                JointAccount = .JointAccount
                                If .LastUpdatedDate IsNot Nothing Then
                                    With .LastUpdatedDate
                                        LastUpdatedDate_CCYY = .CCYY
                                        LastUpdatedDate_DD = .DD
                                        LastUpdatedDate_MM = .MM
                                    End With
                                End If

                                If .MatchDetails IsNot Nothing Then
                                    With .MatchDetails
                                        MatchDetails_BureauRefCategory = .BureauRefCategory
                                        MatchDetails_ForenameMatchLevel = .ForenameMatchLevel
                                        MatchDetails_HouseMatchLevel = .HouseMatchLevel
                                        MatchDetails_MatchTo = .MatchTo
                                        MatchDetails_MatchType = .MatchType
                                        MatchDetails_StreetMatchLevel = .StreetMatchLevel
                                        MatchDetails_SurnameMatchLevel = .SurnameMatchLevel
                                    End With
                                End If

                                If .MnthlyRepayment IsNot Nothing Then
                                    MnthlyRepaymentCount = .MnthlyRepayment.Count

                                    For Each mr In .MnthlyRepayment
                                        MnthlyRepayment = MnthlyRepayment & "," & mr.MnthlyRepayment
                                        MnthlyRepyChngDate = MnthlyRepyChngDate & "," & If(mr.MnthlyRepyChngDate IsNot Nothing, mr.MnthlyRepyChngDate.ToString, "")
                                    Next
                                End If

                                If .Name IsNot Nothing Then
                                    With .Name
                                        Name_Forename = .Forename
                                        Name_MiddleName = .MiddleName
                                        Name_Prefix = .Prefix
                                        Name_Suffix = .Suffix
                                        Name_Surname = .Surname
                                        Name_Title = .Title
                                    End With
                                End If
                                
                                NoCReference = .NoCReference
                                NoOfPayments = .NoOfPayments
                                NoOfRepaymntPeriod = .NoOfRepaymntPeriod
                                NumAccountBalances = .NumAccountBalances
                                NumAddInfoBlocks = .NumAddInfoBlocks
                                NumCardHistories = .NumCardHistories
                                NumCreditLimChngs = .NumCreditLimChngs
                                NumOfMonthsHistory = .NumOfMonthsHistory
                                NumStatuses = .NumStatuses
                                OwnData = .OwnData
                                Payment = .Payment
                                PaymentFrequency = .PaymentFrequency

                                If .RepaymentPeriods IsNot Nothing Then
                                    RepaymentPeriodsCount = .RepaymentPeriods.Count

                                    For Each rp In .RepaymentPeriods
                                        RepaymentPeriod = RepaymentPeriod & "," & rp.RepaymentPeriod
                                        If rp.RepayPdChangeDate IsNot Nothing Then
                                            RepayPdChangeDate = RepayPdChangeDate & "," & rp.RepayPdChangeDate.RepyPdChangeDateMM & "-" & rp.RepayPdChangeDate.RepyPdChangeDateYY
                                        End If
                                    Next
                                End If

                                SettleDateCaption = .SettleDateCaption
                                If .SettlementDate IsNot Nothing Then
                                    With .SettlementDate
                                        SettlementDate_CCYY = .CCYY
                                        SettlementDate_MM = .MM
                                        SettlementDate_DD = .DD
                                    End With
                                End If
                                Sex = .Sex
                                SourceCode = .SourceCode
                                Status1To2 = .Status1To2
                                StatusTo3 = .StatusTo3
                                SupplyCompanyName = .SupplyCompanyName
                                TransColnAcntFlag = .TransColnAcntFlag
                                WorstStatus = .WorstStatus
                            End With

                            objExperianData.InsertConsumerData_FULLCONDATA_CD_CAIS(ApplicationID _
                                                                      , ApplicantIndicator, LocationIndicator _
                                                                      , AccountBalancesCount, AccountBalance, Status, AccountNumber, AccountStatus, AccountStatusCodes _
                                                                      , AccountType, Balance_Amount, Balance_Caption, Balance_Narrative, BankFlag, CAISAccStartDate_CCYY _
                                                                      , CAISAccStartDate_DD, CAISAccStartDate_MM, CMLLocationType, CompanyType, CreditLimit_Amount _
                                                                      , CreditLimit_Caption, CreditLimit_Narrative, CurrentDefBalance_Amount, CurrentDefBalance_Narrative _
                                                                      , DelinquentBalance_Amount, DelinquentBalance_Narrative, FSCCUGIndicator, InformationSource _
                                                                      , JointAccount, LastUpdatedDate_CCYY, LastUpdatedDate_DD, LastUpdatedDate_MM, MatchDetails_BureauRefCategory _
                                                                      , MatchDetails_ForenameMatchLevel, MatchDetails_HouseMatchLevel, MatchDetails_MatchTo, MatchDetails_MatchType _
                                                                      , MatchDetails_StreetMatchLevel, MatchDetails_SurnameMatchLevel, MnthlyRepaymentCount, MnthlyRepayment _
                                                                      , MnthlyRepyChngDate, Name_Forename, Name_MiddleName, Name_Prefix, Name_Suffix, Name_Surname _
                                                                      , Name_Title, NoCReference, NoOfPayments, NoOfRepaymntPeriod, NumAccountBalances, NumAddInfoBlocks _
                                                                      , NumCardHistories, NumCreditLimChngs, NumOfMonthsHistory, NumStatuses, OwnData, Payment, PaymentFrequency _
                                                                      , RepaymentPeriodsCount, RepaymentPeriod, RepayPdChangeDate, SettleDateCaption, SettlementDate_CCYY _
                                                                      , SettlementDate_MM, SettlementDate_DD, Sex, SourceCode, Status1To2, StatusTo3, SupplyCompanyName, TransColnAcntFlag _
                                                                      , WorstStatus)


                            AccountBalancesCount = 0
                            AccountBalance = ""
                            Status = ""
                            AccountNumber = ""
                            AccountStatus = ""
                            AccountStatusCodes = ""
                            AccountType = ""
                            Balance_Amount = ""
                            Balance_Caption = ""
                            Balance_Narrative = ""
                            BankFlag = ""
                            CAISAccStartDate_CCYY = ""
                            CAISAccStartDate_DD = ""
                            CAISAccStartDate_MM = ""
                            CMLLocationType = ""
                            CompanyType = ""
                            CreditLimit_Amount = ""
                            CreditLimit_Caption = ""
                            CreditLimit_Narrative = ""
                            CurrentDefBalance_Amount = ""
                            CurrentDefBalance_Narrative = ""
                            DelinquentBalance_Amount = ""
                            DelinquentBalance_Narrative = ""
                            FSCCUGIndicator = ""
                            InformationSource = ""
                            JointAccount = ""
                            LastUpdatedDate_CCYY = ""
                            LastUpdatedDate_DD = ""
                            LastUpdatedDate_MM = ""
                            MatchDetails_BureauRefCategory = ""
                            MatchDetails_ForenameMatchLevel = ""
                            MatchDetails_HouseMatchLevel = ""
                            MatchDetails_MatchTo = ""
                            MatchDetails_MatchType = ""
                            MatchDetails_StreetMatchLevel = ""
                            MatchDetails_SurnameMatchLevel = ""
                            MnthlyRepaymentCount = 0
                            MnthlyRepayment = ""
                            MnthlyRepyChngDate = ""
                            Name_Forename = ""
                            Name_MiddleName = ""
                            Name_Prefix = ""
                            Name_Suffix = ""
                            Name_Surname = ""
                            Name_Title = ""
                            NoCReference = ""
                            NoOfPayments = ""
                            NoOfRepaymntPeriod = ""
                            NumAccountBalances = ""
                            NumAddInfoBlocks = ""
                            NumCardHistories = ""
                            NumCreditLimChngs = ""
                            NumOfMonthsHistory = ""
                            NumStatuses = ""
                            OwnData = ""
                            Payment = ""
                            PaymentFrequency = ""
                            RepaymentPeriodsCount = 0
                            RepaymentPeriod = ""
                            RepayPdChangeDate = ""
                            SettleDateCaption = ""
                            SettlementDate_CCYY = ""
                            SettlementDate_MM = ""
                            SettlementDate_DD = ""
                            Sex = ""
                            SourceCode = ""
                            Status1To2 = ""
                            StatusTo3 = ""
                            SupplyCompanyName = ""
                            TransColnAcntFlag = ""
                            WorstStatus = ""
                        Next

                        ApplicantIndicator = ""
                        LocationIndicator = ""
                    End With
                Next


            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' dbo.ExperianConsumerData_FullConData_CD_PreviousApplication
    ''' </summary>
    Private Function SaveConsumerData_FullConData_CD_PreviousApplication(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
        Try
            Dim ApplicantIndicator As String = ""
            Dim LocationIndicator As String = ""
            Dim AccountNumber As String = ""
            Dim Amount As String = ""
            Dim ApplicationDate_CCYY As String = ""
            Dim ApplicationDate_DD As String = ""
            Dim ApplicationDate_MM As String = ""
            Dim ApplicationType As String = ""
            Dim CompanyType As String = ""
            Dim DateOfBirth_CCYY As String = ""
            Dim DateOfBirth_DD As String = ""
            Dim DateOfBirth_MM As String = ""
            Dim JointApplicant As String = ""
            Dim Location_Country As String = ""
            Dim Location_County As String = ""
            Dim Location_District As String = ""
            Dim Location_District2 As String = ""
            Dim Location_Flat As String = ""
            Dim Location_HouseName As String = ""
            Dim Location_HouseNumber As String = ""
            Dim Location_POBox As String = ""
            Dim Location_Postcode As String = ""
            Dim Location_PostTown As String = ""
            Dim Location_SharedLetterbox As String = ""
            Dim Location_Street As String = ""
            Dim Location_Street2 As String = ""
            Dim MatchDetails_BureauRefCategory As String = ""
            Dim MatchDetails_HouseMatchLevel As String = ""
            Dim MatchDetails_MatchTo As String = ""
            Dim MatchDetails_MatchType As String = ""
            Dim MatchDetails_StreetMatchLevel As String = ""
            Dim Name_Forename As String = ""
            Dim Name_MiddleName As String = ""
            Dim Name_Prefix As String = ""
            Dim Name_Suffix As String = ""
            Dim Name_Surname As String = ""
            Dim Name_Title As String = ""
            Dim NoCReference As String = ""
            Dim OptOut As String = ""
            Dim OwnAccountID As String = ""
            Dim SearcherName As String = ""
            Dim SupplyCompanyName As String = ""
            Dim Term As String = ""
            Dim TimeAtLocation_Months As String = ""
            Dim TimeAtLocation_Years As String = ""

            If output IsNot Nothing AndAlso output.Output IsNot Nothing AndAlso output.Output.FullConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData IsNot Nothing AndAlso output.Output.FullConsumerData.ConsumerData.PreviousApplication IsNot Nothing Then

                'Delete existing rows if any
                objExperianData.DeleteConsumerData_FULLCONDATA_CD_PreviousApplication(ApplicationID)

                For Each PreApp As ConsData.OutputFullConsumerDataConsumerDataPreviousApplication In output.Output.FullConsumerData.ConsumerData.PreviousApplication
                    With PreApp

                        ApplicantIndicator = .ApplicantIndicator
                        LocationIndicator = .LocationIndicator

                        For Each preAppDetail As ConsData.OutputFullConsumerDataConsumerDataPreviousApplicationPrevApplnDetails In .PrevApplnDetails
                            With preAppDetail
                                AccountNumber = .AccountNumber
                                Amount = .Amount
                                If .ApplicationDate IsNot Nothing Then
                                    With .ApplicationDate
                                        ApplicationDate_CCYY = .CCYY
                                        ApplicationDate_DD = .DD
                                        ApplicationDate_MM = .MM
                                    End With
                                End If
                                
                                ApplicationType = .ApplicationType
                                CompanyType = .CompanyType
                                If .DateOfBirth IsNot Nothing Then
                                    With .DateOfBirth
                                        DateOfBirth_CCYY = .CCYY
                                        DateOfBirth_DD = .DD
                                        DateOfBirth_MM = .MM
                                    End With
                                End If
                                
                                JointApplicant = .JointApplicant
                                If .Location IsNot Nothing Then
                                    With .Location
                                        Location_Country = .Country
                                        Location_County = .County
                                        Location_District = .District
                                        Location_District2 = .District2
                                        Location_Flat = .Flat
                                        Location_HouseName = .HouseName
                                        Location_HouseNumber = .HouseNumber
                                        Location_POBox = .POBox
                                        Location_Postcode = .Postcode
                                        Location_PostTown = .PostTown
                                        Location_SharedLetterbox = .SharedLetterbox
                                        Location_Street = .Street
                                        Location_Street2 = .Street2
                                    End With
                                End If

                                If .MatchDetails IsNot Nothing Then
                                    With .MatchDetails
                                        MatchDetails_BureauRefCategory = .BureauRefCategory
                                        MatchDetails_HouseMatchLevel = .HouseMatchLevel
                                        MatchDetails_MatchTo = .MatchTo
                                        MatchDetails_MatchType = .MatchType
                                        MatchDetails_StreetMatchLevel = .StreetMatchLevel
                                    End With
                                End If

                                If .Name IsNot Nothing Then
                                    With .Name
                                        Name_Forename = .Forename
                                        Name_MiddleName = .MiddleName
                                        Name_Prefix = .Prefix
                                        Name_Suffix = .Suffix
                                        Name_Surname = .Surname
                                        Name_Title = .Title
                                    End With
                                End If
                                
                                NoCReference = .NoCReference
                                OptOut = .OptOut
                                OwnAccountID = .OwnAccountID
                                SearcherName = .SearcherName
                                SupplyCompanyName = .SupplyCompanyName
                                Term = .Term
                                If .TimeAtLocation IsNot Nothing Then
                                    With .TimeAtLocation
                                        TimeAtLocation_Months = .Months
                                        TimeAtLocation_Years = .Years
                                    End With
                                End If
                                
                            End With

                            objExperianData.InsertConsumerData_FULLCONDATA_CD_PreviousApplication(ApplicationID _
                                                                      , ApplicantIndicator, LocationIndicator _
                                                                      , AccountNumber, Amount, ApplicationDate_CCYY, ApplicationDate_DD, ApplicationDate_MM _
                                                                      , ApplicationType, CompanyType, DateOfBirth_CCYY, DateOfBirth_DD, DateOfBirth_MM _
                                                                      , JointApplicant, Location_Country, Location_County, Location_District, Location_District2 _
                                                                      , Location_Flat, Location_HouseName, Location_HouseNumber, Location_POBox, Location_Postcode _
                                                                      , Location_PostTown, Location_SharedLetterbox, Location_Street, Location_Street2 _
                                                                      , MatchDetails_BureauRefCategory, MatchDetails_HouseMatchLevel, MatchDetails_MatchTo _
                                                                      , MatchDetails_MatchType, MatchDetails_StreetMatchLevel, Name_Forename, Name_MiddleName _
                                                                      , Name_Prefix, Name_Suffix, Name_Surname, Name_Title, NoCReference, OptOut, OwnAccountID _
                                                                      , SearcherName, SupplyCompanyName, Term, TimeAtLocation_Months,TimeAtLocation_Years)


                            AccountNumber = ""
                            Amount = ""
                            ApplicationDate_CCYY = ""
                            ApplicationDate_DD = ""
                            ApplicationDate_MM = ""
                            ApplicationType = ""
                            CompanyType = ""
                            DateOfBirth_CCYY = ""
                            DateOfBirth_DD = ""
                            DateOfBirth_MM = ""
                            JointApplicant = ""
                            Location_Country = ""
                            Location_County = ""
                            Location_District = ""
                            Location_District2 = ""
                            Location_Flat = ""
                            Location_HouseName = ""
                            Location_HouseNumber = ""
                            Location_POBox = ""
                            Location_Postcode = ""
                            Location_PostTown = ""
                            Location_SharedLetterbox = ""
                            Location_Street = ""
                            Location_Street2 = ""
                            MatchDetails_BureauRefCategory = ""
                            MatchDetails_HouseMatchLevel = ""
                            MatchDetails_MatchTo = ""
                            MatchDetails_MatchType = ""
                            MatchDetails_StreetMatchLevel = ""
                            Name_Forename = ""
                            Name_MiddleName = ""
                            Name_Prefix = ""
                            Name_Suffix = ""
                            Name_Surname = ""
                            Name_Title = ""
                            NoCReference = ""
                            OptOut = ""
                            OwnAccountID = ""
                            SearcherName = ""
                            SupplyCompanyName = ""
                            Term = ""
                            TimeAtLocation_Months = ""
                            TimeAtLocation_Years = ""
                        Next

                        ApplicantIndicator = ""
                        LocationIndicator = ""
                    End With
                Next
            End If

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, IPAddress)
            Return False
        End Try
    End Function

End Class
