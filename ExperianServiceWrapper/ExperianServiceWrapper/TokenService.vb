﻿Namespace LMCU.ExperianAuthentication
    Public Class TokenService

        Dim TokenServiceURL_UAT As String = "https://secure.authenticator.uat.uk.experian.com/WASPAuthenticator/tokenService.asmx"
        Dim TokenServiceURL_LIVE As String = "https://secure.authenticator.uk.experian.com/WASPAuthenticator/TokenService.asmx"

        Private _ExperianEnv As String = "DEV"
        Public Property ExperianEnv As String
            Get
                Return _ExperianEnv
            End Get
            Set(ByVal value As String)
                _ExperianEnv = value
            End Set
        End Property

        Private _ctxt As WSSecurityContext
        Public Property CTXT As WSSecurityContext
            Get
                Return _ctxt
            End Get
            Set(ByVal value As WSSecurityContext)
                _ctxt = value
            End Set
        End Property

        Private _ctxtBankWizard As WSSecurityContext
        Public Property CTXTBankWizard As WSSecurityContext
            Get
                Return _ctxtBankWizard
            End Get
            Set(ByVal value As WSSecurityContext)
                _ctxtBankWizard = value
            End Set
        End Property

        Public Function CallTokenService(ByVal env As String) As WSSecurityContext
            ' Specify the path to the certificate
            _ExperianEnv = env
            Dim _certFile As String = "C:\cer\lmcu.cer"
            If env = "LIVE" Then
                _certFile = "C:\cer\lmcu_live.cer"
            End If

            ' Attempt to get a security token
            If GetSecurityToken(_certFile) Then
                ' The security token was receieved successfully 
                ' so make the request to the protected web service
                Return _ctxt
            Else
                ' Security token couldn't be obtained
                Return Nothing
            End If
        End Function

        Public Function CallTokenServiceBankWizard(ByVal env As String) As WSSecurityContext
            ' Specify the path to the certificate
            _ExperianEnv = env
            Dim _certFile As String = "C:\cer\bank.cer"
            If env = "LIVE" Then
                _certFile = "C:\cer\bank_live.cer"
            End If

            ' Attempt to get a security token
            If GetSecurityTokenBankWizard(_certFile) Then
                ' The security token was receieved successfully 
                ' so make the request to the protected web service
                Return _ctxtBankWizard
            Else
                ' Security token couldn't be obtained
                Return Nothing
            End If
        End Function

        Private Function GetSecurityToken(ByVal certFile As String) As Boolean
            If _ctxt Is Nothing Then
                ' Initialise the Security Context object
                _ctxt = New WSSecurityContext("LMCU Client")
                _ctxt.SetCertificateFromFile(certFile)
                If ExperianEnv = "LIVE" Then
                    _ctxt.SignonLocation = TokenServiceURL_LIVE
                End If
            End If

            ' Check to see if we already have a valid token
            If _ctxt.SecurityToken Is Nothing OrElse _ctxt.SecurityToken.IsExpired Then
                ' Make a request for a security token from the authenticator
                _ctxt.SignOn()
            End If

            If _ctxt.SecurityToken Is Nothing Then
                'Could not obtain a security token
                Return False
            Else
                Return True
            End If
        End Function

        Private Function GetSecurityTokenBankWizard(ByVal certFile As String) As Boolean
            If _ctxtBankWizard Is Nothing Then
                ' Initialise the Security Context object
                _ctxtBankWizard = New WSSecurityContext("LMCU Client")
                _ctxtBankWizard.SetCertificateFromFile(certFile)
                If ExperianEnv = "LIVE" Then
                    _ctxtBankWizard.SignonLocation = TokenServiceURL_LIVE
                End If
            End If

            ' Check to see if we already have a valid token
            If _ctxtBankWizard.SecurityToken Is Nothing OrElse _ctxtBankWizard.SecurityToken.IsExpired Then
                ' Make a request for a security token from the authenticator
                _ctxtBankWizard.SignOn()
            End If

            If _ctxtBankWizard.SecurityToken Is Nothing Then
                'Could not obtain a security token
                Return False
            Else
                Return True
            End If
        End Function
    End Class
End Namespace
