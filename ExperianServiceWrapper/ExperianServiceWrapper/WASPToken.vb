﻿Imports System.Text
Imports Microsoft.Web.Services3
Imports Microsoft.Web.Services3.Security
Imports Microsoft.Web.Services3.Security.Tokens
Imports Microsoft.Web.Services3.Security.Cryptography

Namespace LMCU.ExperianAuthentication
    Public Class WASPToken
        Inherits BinarySecurityToken
        ' The lifetime of the actual token is not known,
        ' the current default is two hours but can be specified
        ' differently.  The LifeTime here allows the software to
        ' refresh the token after a period of time.  This should
        ' help prevent several failed calls to the protected 
        ' service when the token does actually expire provided the 
        ' time period chosen is the same or less than that of the 
        ' actual token expiry.
        Private _lifeTime As LifeTime

        Public Sub New(ByVal token As String)
            MyBase.New("ExperianWASP")
            ' Store the token data into
            RawData = (New UTF8Encoding()).GetBytes(token)
            ' Set the token to become invalid after 90
            ' minutes
            _lifeTime = New LifeTime(DateTime.Now.AddMinutes(90))
        End Sub

        Public Overrides Function ToString() As String
            Return UTF8Encoding.ASCII.GetString(RawData)
        End Function

        Public Overrides Function Equals(ByVal token As SecurityToken) As Boolean
            If token Is Nothing OrElse token.GetType() IsNot Me.GetType() Then
                Return False
            Else
                Dim t As WASPToken = DirectCast(token, WASPToken)
                Return True
            End If
        End Function

        Public Overrides Function GetHashCode() As Integer
            If RawData IsNot Nothing Then
                Return RawData.GetHashCode()
            Else
                Return 0
            End If
        End Function

        Public Overrides ReadOnly Property IsExpired() As Boolean
            Get
                Return _lifeTime.IsExpired
            End Get
        End Property

        Public Overrides ReadOnly Property IsCurrent() As Boolean
            Get
                Return _lifeTime.IsCurrent
            End Get
        End Property

        Public Overrides ReadOnly Property SupportsDigitalSignature() As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property SupportsDataEncryption() As Boolean
            Get
                Return False
            End Get
        End Property
        Public Overrides ReadOnly Property Key() As KeyAlgorithm
            Get
                Return Nothing
            End Get
        End Property
    End Class
End Namespace
