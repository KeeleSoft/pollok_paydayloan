﻿Imports System.Net
Imports System.IO
Imports Microsoft.Web.Services3

Namespace LMCU.ExperianAuthentication
    Public Class LMCUWSClient
        Private _ctxt As WSSecurityContext
        Private _ctxtBankWizard As WSSecurityContext
        Private TokenSer As TokenService = New TokenService

        Dim objExperianData As clsExperian = New clsExperian
        Dim objSystemData As clsSystemData = New clsSystemData
        Dim objSaveCreditSearch As SaveConsumerCreditSearch = New SaveConsumerCreditSearch

        Dim ConsDataURL_UAT As String = "https://scems.uat.uk.experian.com/experian/wbsv/v100/interactive.asmx"
        Dim ConsDataURL_LIVE As String = "https://scems.uk.experian.com/experian/wbsv/v100/interactive/interactive.asmx"

        Dim BankWizardURL_LIVE As String = "https://bankwizardondemand.uk.experian.com/absolute/server/bankwizardservice.asmx"

#Region "Public Properties"
        Private _ExperianEnv As String = "DEV"
        Public Property ExperianEnv As String
            Get
                Return _ExperianEnv
            End Get
            Set(ByVal value As String)
                _ExperianEnv = value
            End Set
        End Property
#End Region

#Region "Testing Methods"
        Public Function CallAuth() As String
            Dim ReturnData As String

            If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxt = TokenSer.CallTokenService(_ExperianEnv)
                If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New ConsData.InteractiveWSWse

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxt.SecurityToken)
            service.ClientCertificates.Add(_ctxt.ExperianCertificate)

            ' Create a simple input message
            Dim root As New ConsData.Root()
            root.Input = New ConsData.Input()
            root.Input.Control = New ConsData.InputControl()
            root.Input.Control.ClientAccountNumber = "H0708" '"D1056"
            'root.Input.Control.ExperianReference = "0123456789"
            root.Input.Control.Parameters = New ConsData.InputControlParameters
            root.Input.Control.Parameters.InteractiveMode = "Interactive"
            root.Input.Control.Parameters.FullFBLRequired = "Y"
            root.Input.Control.Parameters.AuthPlusRequired = "N"
            'root.Input.Control.Parameters.DetectRequired = "Y"
            root.Input.Control.Parameters.ShowConsumer = "Y"
            'root.Input.Control.Parameters.ShowDetect = "Y"
            root.Input.Control.Parameters.ShowAuthenticate = "Y"
            root.Input.Control.Parameters.ShowAddress = "Y"
            root.Input.Control.Parameters.ShowCaseHistory = "Y"
            root.Input.Control.Parameters.SearchConsent = "Y"

            root.Input.ThirdPartyData = New ConsData.InputThirdPartyData
            root.Input.ThirdPartyData.OptOut = "N"
            root.Input.ThirdPartyData.TransientAssocs = "Y"
            root.Input.ThirdPartyData.HHOAllowed = "Y"

            Dim applicant(0) As ConsData.InputApplicant
            applicant(0) = New ConsData.InputApplicant

            root.Input.Applicant = applicant
            root.Input.Applicant(0).Name = New ConsData.InputApplicantName
            root.Input.Applicant(0).ApplicantIdentifier = 1
            root.Input.Applicant(0).Name.Title = "Mr"
            root.Input.Applicant(0).Name.Forename = "Gayeshan"
            root.Input.Applicant(0).Name.MiddleName = "Disman"
            root.Input.Applicant(0).Name.Surname = "Hewa Mithige"
            root.Input.Applicant(0).Gender = "M"

            root.Input.Applicant(0).DateOfBirth = New ConsData.InputApplicantDateOfBirth
            root.Input.Applicant(0).DateOfBirth.CCYY = 1979
            root.Input.Applicant(0).DateOfBirth.CCYYSpecified = True
            root.Input.Applicant(0).DateOfBirth.MM = 12
            root.Input.Applicant(0).DateOfBirth.MMSpecified = True
            root.Input.Applicant(0).DateOfBirth.DD = 5
            root.Input.Applicant(0).DateOfBirth.DDSpecified = True
            root.Input.Applicant(0).FormattedName = "Mr, G D Hewa Mithige"
            root.Input.Applicant(0).FormattedDOB = "05/12/1979"

            Dim LocationDetails(1) As ConsData.InputLocationDetails
            LocationDetails(0) = New ConsData.InputLocationDetails
            LocationDetails(1) = New ConsData.InputLocationDetails

            root.Input.LocationDetails = LocationDetails
            root.Input.LocationDetails(0).LocationIdentifierSpecified = True
            root.Input.LocationDetails(0).LocationIdentifier = 1
            root.Input.LocationDetails(0).UKLocation = New ConsData.InputLocationDetailsUKLocation
            root.Input.LocationDetails(0).UKLocation.HouseNumber = "15"
            root.Input.LocationDetails(0).UKLocation.Street = "Brent Close"
            root.Input.LocationDetails(0).UKLocation.PostTown = "Newcastle"
            root.Input.LocationDetails(0).UKLocation.County = "Staffordshire"
            root.Input.LocationDetails(0).UKLocation.Postcode = "ST52GD"
            root.Input.LocationDetails(0).UKLocation.Country = "UK"

            root.Input.LocationDetails(1).LocationIdentifierSpecified = True
            root.Input.LocationDetails(1).LocationIdentifier = 2
            root.Input.LocationDetails(1).UKLocation = New ConsData.InputLocationDetailsUKLocation
            root.Input.LocationDetails(1).UKLocation.HouseNumber = "17"
            root.Input.LocationDetails(1).UKLocation.Street = "Greenwood Avenue"
            root.Input.LocationDetails(1).UKLocation.PostTown = "Stoke-On-Trent"
            root.Input.LocationDetails(1).UKLocation.County = "Staffordshire"
            root.Input.LocationDetails(1).UKLocation.Postcode = "ST46NF"
            root.Input.LocationDetails(1).UKLocation.Country = "UK"

            Dim Residency(1) As ConsData.InputResidency
            Residency(0) = New ConsData.InputResidency
            Residency(1) = New ConsData.InputResidency

            root.Input.Residency = Residency
            root.Input.Residency(0).SearchFlag = "Y"
            root.Input.Residency(0).ApplicantIdentifier = "1"
            root.Input.Residency(0).LocationIdentifier = "1"
            root.Input.Residency(0).LocationCode = "01"
            root.Input.Residency(0).ResidencyDateFrom = New ConsData.InputResidencyResidencyDateFrom
            root.Input.Residency(0).ResidencyDateFrom.CCYY = "2011"
            root.Input.Residency(0).ResidencyDateFrom.CCYYSpecified = True
            root.Input.Residency(0).ResidencyDateFrom.MM = "10"
            root.Input.Residency(0).ResidencyDateFrom.MMSpecified = True
            root.Input.Residency(0).ResidencyDateFrom.DD = "01"
            root.Input.Residency(0).ResidencyDateFrom.DDSpecified = True
            root.Input.Residency(0).ResidencyDateTo = New ConsData.InputResidencyResidencyDateTo
            root.Input.Residency(0).ResidencyDateTo.CCYY = "2011"
            root.Input.Residency(0).ResidencyDateTo.CCYYSpecified = True
            root.Input.Residency(0).ResidencyDateTo.MM = "12"
            root.Input.Residency(0).ResidencyDateTo.MMSpecified = True
            root.Input.Residency(0).ResidencyDateTo.DD = "08"
            root.Input.Residency(0).ResidencyDateTo.DDSpecified = True

            root.Input.Residency(1).SearchFlag = "Y"
            root.Input.Residency(1).ApplicantIdentifier = "1"
            root.Input.Residency(1).LocationIdentifier = "2"
            root.Input.Residency(1).LocationCode = "02"
            root.Input.Residency(1).ResidencyDateFrom = New ConsData.InputResidencyResidencyDateFrom
            root.Input.Residency(1).ResidencyDateFrom.CCYY = "2003"
            root.Input.Residency(1).ResidencyDateFrom.CCYYSpecified = True
            root.Input.Residency(1).ResidencyDateFrom.MM = "06"
            root.Input.Residency(1).ResidencyDateFrom.MMSpecified = True
            root.Input.Residency(1).ResidencyDateFrom.DD = "27"
            root.Input.Residency(1).ResidencyDateFrom.DDSpecified = True
            root.Input.Residency(1).ResidencyDateTo = New ConsData.InputResidencyResidencyDateTo
            root.Input.Residency(1).ResidencyDateTo.CCYY = "2011"
            root.Input.Residency(1).ResidencyDateTo.CCYYSpecified = True
            root.Input.Residency(1).ResidencyDateTo.MM = "09"
            root.Input.Residency(1).ResidencyDateTo.MMSpecified = True
            root.Input.Residency(1).ResidencyDateTo.DD = "30"
            root.Input.Residency(1).ResidencyDateTo.DDSpecified = True

            root.Input.Application = New ConsData.InputApplication
            root.Input.Application.ApplicationType = "CU"
            root.Input.Application.Amount = "400"
            root.Input.Application.Term = "3"
            root.Input.Application.Purpose = "8"
            root.Input.Application.ApplicationChannel = "IT"
            'root.Input.Application.AuthenticationType = "C"
            root.Input.Application.SearchConsent = "Y"
            ' Declare the output message
            Dim output As ConsData.OutputRoot
            Try
                ' Call the service and get the response

                output = service.Interactive(root)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    ' Do something with the response
                    Dim sw As New System.IO.StringWriter()
                    Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(ConsData.OutputRoot))
                    xs.Serialize(sw, output)
                    ReturnData = sw.ToString()
                Else
                    ' Invalid response
                    ReturnData = "Invalid response from service"
                End If

                Return ReturnData
            Catch exc As Exception
                ' An exception was received when calling the web service 
                Throw exc
                'Return ReturnData
            End Try

        End Function

        Public Function CallFullAuth() As String
            Dim ReturnData As String

            If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxt = TokenSer.CallTokenService(_ExperianEnv)
                If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New FullAuthenticate.InteractiveWSWSe

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxt.SecurityToken)
            service.ClientCertificates.Add(_ctxt.ExperianCertificate)

            ' Create a simple input message
            Dim root As New FullAuthenticate.Root()
            root.Input = New FullAuthenticate.Input()
            root.Input.Control = New FullAuthenticate.InputControl()
            root.Input.Control.ExperianReference = "SWD36CNDDK"
            root.Input.Control.ClientRef = "LMCU"


            ' Declare the output message
            Dim output As FullAuthenticate.OutputRoot
            Try
                ' Call the service and get the response

                output = service.Interactive(root)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    ' Do something with the response
                    Dim sw As New System.IO.StringWriter()
                    Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(FullAuthenticate.OutputRoot))
                    xs.Serialize(sw, output)
                    ReturnData = sw.ToString()
                Else
                    ' Invalid response
                    ReturnData = "Invalid response from service"
                End If

                Return ReturnData
            Catch exc As Exception
                ' An exception was received when calling the web service 
                Throw exc
                'Return ReturnData
            End Try

        End Function

        Public Function CallHHO() As String
            Dim ReturnData As String

            If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxt = TokenSer.CallTokenService(_ExperianEnv)
                If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New HHO.InteractiveWSWSe

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxt.SecurityToken)
            service.ClientCertificates.Add(_ctxt.ExperianCertificate)

            ' Create a simple input message
            Dim root As New HHO.Root()
            root.Input = New HHO.Input()
            root.Input.Control = New HHO.InputControl()
            root.Input.Control.ExperianReference = "ST63HKDCAR"
            root.Input.Control.ClientRef = "LMCU"


            ' Declare the output message
            Dim output As HHO.OutputRoot
            Try
                ' Call the service and get the response

                output = service.Interactive(root)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    ' Do something with the response
                    Dim sw As New System.IO.StringWriter()
                    Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(HHO.OutputRoot))
                    xs.Serialize(sw, output)
                    ReturnData = sw.ToString()
                Else
                    ' Invalid response
                    ReturnData = "Invalid response from service"
                End If

                Return ReturnData
            Catch exc As Exception
                ' An exception was received when calling the web service 
                Throw exc
                'Return ReturnData
            End Try

        End Function

        Public Function CallBankWizardValidate() As String
            Dim ReturnData As String

            If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxtBankWizard = TokenSer.CallTokenServiceBankWizard(_ExperianEnv)
                If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New BankWizard.BankWizard_v1_0_Service

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxtBankWizard.SecurityToken)
            service.ClientCertificates.Add(_ctxtBankWizard.ExperianCertificate)

            Dim ValidateRequest As New BankWizard.ValidateRequest
            ValidateRequest.reportString = "certificatechecker"
            ValidateRequest.itemisationID = "1234"
            ValidateRequest.language = "en"
            ValidateRequest.ISOCountry = "GB"
            ValidateRequest.checkingLevel = BankWizard.CheckingLevel.Account

            Dim bban(1) As BankWizard.BBANBaseType
            bban(0) = New BankWizard.BBANBaseType
            bban(1) = New BankWizard.BBANBaseType

            bban(0).index = "1"
            bban(0).Value = "888888" '"070116"

            bban(1).index = "2"
            bban(1).Value = "89899834" '"00003036"

            ValidateRequest.BBAN = bban

            Dim output As BankWizard.ValidateResponse

            Try
                ' Call the service and get the response

                output = service.Validate(ValidateRequest)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    ' Do something with the response
                    Dim sw As New System.IO.StringWriter()
                    Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(BankWizard.ValidateResponse))
                    xs.Serialize(sw, output)
                    ReturnData = sw.ToString()
                Else
                    ' Invalid response
                    ReturnData = "Invalid response from service"
                End If

                Return ReturnData
            Catch exc As Exception
                ' An exception was received when calling the web service 
                Throw exc
                'Return ReturnData
            End Try

            Return (ReturnData)

        End Function

        Public Function CallBankWizardVerify() As String
            Dim ReturnData As String

            If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxtBankWizard = TokenSer.CallTokenServiceBankWizard(_ExperianEnv)
                If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New BankWizard.BankWizard_v1_0_Service

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxtBankWizard.SecurityToken)
            service.ClientCertificates.Add(_ctxtBankWizard.ExperianCertificate)

            Dim VerifyRequest As BankWizard.VerifyRequest = New BankWizard.VerifyRequest
            VerifyRequest.language = "en"
            VerifyRequest.itemisationID = "LMCU"
            VerifyRequest.reportString = "CUOK"


            Dim AccountInfo As New BankWizard.VerifyAccountRequestType
            AccountInfo.sortCode = "070118"
            AccountInfo.accountNumber = "00003038"
            AccountInfo.checkContext = BankWizard.CheckContextType.DirectCredit
            AccountInfo.accountVerification = New BankWizard.VerifyBankAccountRequestType
            AccountInfo.accountVerification.accountSetupDate = New BankWizard.AccountDateType
            AccountInfo.accountVerification.accountSetupDate.year = "2002"
            AccountInfo.accountVerification.accountSetupDate.month = "6"
            AccountInfo.accountVerification.accountTypeInformation = New BankWizard.AccountTypeInformation
            AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.Current
            AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Personal

            VerifyRequest.accountInformation = AccountInfo

            Dim PersonalInfo As BankWizard.VerifyPersonalRequestType = New BankWizard.VerifyPersonalRequestType
            PersonalInfo.personal = New BankWizard.PersonalDetails
            PersonalInfo.personal.firstName = "Gayeshan" '"Ashley"
            PersonalInfo.personal.surname = "Mithige" '"Marma"
            PersonalInfo.personal.dob = New DateTime(1979, 12, 5)
            PersonalInfo.personal.dobSpecified = True

            PersonalInfo.address = New BankWizard.Address

            Dim deliveryPoint(1) As BankWizard.DeliveryPoint
            deliveryPoint(0) = New BankWizard.DeliveryPoint
            deliveryPoint(0).deliveryType = BankWizard.DeliveryPointType.houseNumber
            deliveryPoint(0).Value = "1"
            PersonalInfo.address.deliveryPoint = deliveryPoint

            Dim PostalPoint(2) As BankWizard.PostalPoint
            PostalPoint(0) = New BankWizard.PostalPoint
            PostalPoint(0).postalType = BankWizard.PostalPointType.street
            PostalPoint(0).Value = "Abbey Lodge"

            PostalPoint(1) = New BankWizard.PostalPoint
            PostalPoint(1).postalType = BankWizard.PostalPointType.postcode
            PostalPoint(1).Value = "SE120ES"

            PersonalInfo.address.postalPoint = PostalPoint

            PersonalInfo.ownerType = BankWizard.OwnerType.Single
            PersonalInfo.ownerTypeSpecified = True

            VerifyRequest.personalInformation = PersonalInfo

            Dim output As BankWizard.VerifyResponse

            Try
                ' Call the service and get the response

                output = service.Verify(VerifyRequest)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    ' Do something with the response
                    Dim sw As New System.IO.StringWriter()
                    Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(BankWizard.VerifyResponse))
                    xs.Serialize(sw, output)
                    ReturnData = sw.ToString()
                Else
                    ' Invalid response
                    ReturnData = "Invalid response from service"
                End If

                Return ReturnData
            Catch exc As Exception
                ' An exception was received when calling the web service 
                Throw exc
                'Return ReturnData
            End Try

            Return (ReturnData)

        End Function
#End Region

#Region "Authentication Plus"

        '''--------------------------------------------------------------------
        ''' <summary>
        ''' 	Do the experian authentication plus
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <returns>
        ''' 	Authentication success status
        ''' </returns>
        ''' <history>
        '''     [Shan HM]  12/12/2011   - Created
        ''' </history>
        '''--------------------------------------------------------------------
        Public Function DoAuthenticationPlus(ByVal Title As String _
                                             , ByVal Forename As String _
                                             , ByVal MiddleName As String _
                                             , ByVal Surname As String _
                                             , ByVal Gender As String _
                                             , ByVal DateOfBirth_CCYY As Integer _
                                             , ByVal DateOfBirth_MM As Integer _
                                             , ByVal DateOfBirth_DD As Integer _
                                             , ByVal Address1_HouseNumber As String _
                                             , ByVal Address1_Street As String _
                                             , ByVal Address1_PostTown As String _
                                             , ByVal Address1_County As String _
                                             , ByVal Address1_PostCode As String _
                                             , ByVal Address1_TimeAtYears As String _
                                             , ByVal Address1_TimeAtMonths As String _
                                             , ByVal Address2_HouseNumber As String _
                                             , ByVal Address2_Street As String _
                                             , ByVal Address2_PostTown As String _
                                             , ByVal Address2_County As String _
                                             , ByVal Address2_PostCode As String _
                                             , ByVal Address2_TimeAtYears As String _
                                             , ByVal Address2_TimeAtMonths As String _
                                             , ByVal Amount As String _
                                             , ByVal Term As String _
                                             , ByVal Purpose As String _
                                             , ByVal ApplicationID As Integer _
                                             , ByVal IPAddress As String _
                                             , ByVal AuthResultSuccessValue As Integer _
                                             ) As Boolean

            If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxt = TokenSer.CallTokenService(_ExperianEnv)
                If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New ConsData.InteractiveWSWse

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxt.SecurityToken)
            service.ClientCertificates.Add(_ctxt.ExperianCertificate)

            ' Create a simple input message
            Dim root As New ConsData.Root()
            root.Input = New ConsData.Input()
            root.Input.Control = New ConsData.InputControl()
            'root.Input.Control.ExperianReference = "0123456789"
            root.Input.Control.Parameters = New ConsData.InputControlParameters
            root.Input.Control.Parameters.InteractiveMode = "Interactive"
            root.Input.Control.Parameters.FullFBLRequired = "Y"
            root.Input.Control.Parameters.AuthPlusRequired = "E"
            'root.Input.Control.Parameters.DetectRequired = "Y"
            root.Input.Control.Parameters.ShowConsumer = "Y"
            'root.Input.Control.Parameters.ShowDetect = "Y"
            root.Input.Control.Parameters.ShowAuthenticate = "Y"
            root.Input.Control.Parameters.ShowAddress = "Y"
            root.Input.Control.Parameters.ShowCaseHistory = "Y"
            root.Input.Control.Parameters.SearchConsent = "Y"

            root.Input.ThirdPartyData = New ConsData.InputThirdPartyData
            root.Input.ThirdPartyData.OptOut = "N"
            root.Input.ThirdPartyData.TransientAssocs = "Y"
            root.Input.ThirdPartyData.HHOAllowed = "Y"

            Dim applicant(0) As ConsData.InputApplicant
            applicant(0) = New ConsData.InputApplicant

            root.Input.Applicant = applicant
            root.Input.Applicant(0).Name = New ConsData.InputApplicantName
            root.Input.Applicant(0).ApplicantIdentifier = 1
            root.Input.Applicant(0).Name.Title = Title
            root.Input.Applicant(0).Name.Forename = Forename
            root.Input.Applicant(0).Name.MiddleName = MiddleName
            root.Input.Applicant(0).Name.Surname = Surname
            root.Input.Applicant(0).Gender = Gender

            root.Input.Applicant(0).DateOfBirth = New ConsData.InputApplicantDateOfBirth
            root.Input.Applicant(0).DateOfBirth.CCYY = DateOfBirth_CCYY
            root.Input.Applicant(0).DateOfBirth.CCYYSpecified = True
            root.Input.Applicant(0).DateOfBirth.MM = DateOfBirth_MM
            root.Input.Applicant(0).DateOfBirth.MMSpecified = True
            root.Input.Applicant(0).DateOfBirth.DD = DateOfBirth_DD
            root.Input.Applicant(0).DateOfBirth.DDSpecified = True
            root.Input.Applicant(0).FormattedName = Title & ", " & Left(Forename, 1).ToUpper & " " & IIf(MiddleName.Length > 0, Left(MiddleName, 1).ToUpper, "").ToString & " " & Surname
            root.Input.Applicant(0).FormattedDOB = DateOfBirth_DD.ToString & "/" & DateOfBirth_MM.ToString & "/" & DateOfBirth_CCYY.ToString

            Dim LocationDetails(0) As ConsData.InputLocationDetails
            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                ReDim LocationDetails(1)
                LocationDetails(1) = New ConsData.InputLocationDetails
            End If
            LocationDetails(0) = New ConsData.InputLocationDetails

            root.Input.LocationDetails = LocationDetails
            root.Input.LocationDetails(0).LocationIdentifierSpecified = True
            root.Input.LocationDetails(0).LocationIdentifier = 1
            root.Input.LocationDetails(0).UKLocation = New ConsData.InputLocationDetailsUKLocation
            root.Input.LocationDetails(0).UKLocation.HouseNumber = Address1_HouseNumber
            root.Input.LocationDetails(0).UKLocation.Street = Address1_Street
            root.Input.LocationDetails(0).UKLocation.PostTown = Address1_PostTown
            root.Input.LocationDetails(0).UKLocation.County = Address1_County
            root.Input.LocationDetails(0).UKLocation.Postcode = Address1_PostCode.Trim.ToUpper
            root.Input.LocationDetails(0).UKLocation.Country = "UK"

            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                root.Input.LocationDetails(1).LocationIdentifierSpecified = True
                root.Input.LocationDetails(1).LocationIdentifier = 2
                root.Input.LocationDetails(1).UKLocation = New ConsData.InputLocationDetailsUKLocation
                root.Input.LocationDetails(1).UKLocation.HouseNumber = Address2_HouseNumber
                root.Input.LocationDetails(1).UKLocation.Street = Address2_Street
                root.Input.LocationDetails(1).UKLocation.PostTown = Address2_PostTown
                root.Input.LocationDetails(1).UKLocation.County = Address2_County
                root.Input.LocationDetails(1).UKLocation.Postcode = Address2_PostCode.Trim.ToUpper
                root.Input.LocationDetails(1).UKLocation.Country = "UK"
            End If


            Dim Residency(0) As ConsData.InputResidency
            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                ReDim Residency(1)
                Residency(1) = New ConsData.InputResidency
            End If
            Residency(0) = New ConsData.InputResidency

            root.Input.Residency = Residency
            root.Input.Residency(0).SearchFlag = "Y"
            root.Input.Residency(0).ApplicantIdentifier = "1"
            root.Input.Residency(0).LocationIdentifier = "1"
            root.Input.Residency(0).LocationCode = "01"
            root.Input.Residency(0).TimeAt = New ConsData.InputResidencyTimeAt
            root.Input.Residency(0).TimeAt.Years = Address1_TimeAtYears
            root.Input.Residency(0).TimeAt.Months = Address1_TimeAtMonths

            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                root.Input.Residency(1).SearchFlag = "Y"
                root.Input.Residency(1).ApplicantIdentifier = "1"
                root.Input.Residency(1).LocationIdentifier = "2"
                root.Input.Residency(1).LocationCode = "02"
                root.Input.Residency(1).TimeAt = New ConsData.InputResidencyTimeAt
                root.Input.Residency(1).TimeAt.Years = Address2_TimeAtYears
                root.Input.Residency(1).TimeAt.Months = Address2_TimeAtMonths
            End If

            root.Input.Application = New ConsData.InputApplication
            root.Input.Application.ApplicationType = "CU"
            root.Input.Application.Amount = Amount
            root.Input.Application.Term = Term
            root.Input.Application.Purpose = Purpose
            root.Input.Application.ApplicationChannel = "IT"
            'root.Input.Application.AuthenticationType = "C"
            root.Input.Application.SearchConsent = "Y"
            ' Declare the output message
            Dim output As ConsData.OutputRoot
            Dim AuthStatus As Boolean = False
            Try
                ' Call the service and get the response

                output = service.Interactive(root)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    'Save the response in DB
                    If SaveAuthenticationPlus(ApplicationID, output, IPAddress) Then
                        If output.Output.Error IsNot Nothing Then
                            AuthStatus = False
                        Else
                            If output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 AndAlso output.Output.AuthPlus(0).Decision IsNot Nothing _
                                AndAlso output.Output.AuthPlus(0).Decision.AuthIndex >= AuthResultSuccessValue Then

                                AuthStatus = True
                            Else

                                AuthStatus = False
                            End If
                        End If
                    Else
                        AuthStatus = False
                    End If
                Else
                    ' Invalid response
                    AuthStatus = False
                End If

                Return AuthStatus
            Catch exc As Exception
                ' An exception was received when calling the web service 
                objSystemData.InsertErrorLog(exc, IPAddress)

                Return False
                'Return ReturnData
            End Try

        End Function

        Private Function SaveAuthenticationPlus(ByVal ApplicationID As Integer, ByVal output As ConsData.OutputRoot, ByVal IPAddress As String) As Boolean
            Try

                Dim ExperianReference As String = ""
                Dim DecCode As String = ""
                Dim DecText As String = ""
                Dim AuthIndex As String = ""
                Dim AuthText As String = ""
                Dim IDConfLvl As String = ""
                Dim IDConfText As String = ""
                Dim HighRiskCount As Integer = 0
                Dim ErrorCode As String = ""
                Dim ErrorMessage As String = ""
                Dim ErrorSeverity As String = ""

                'IACA
                Dim IACA_PrimaryDataItems As String = ""
                Dim IACA_PrimaryDataSources As String = ""
                Dim IACA_StartDateOldestPrimaryItem As String = ""
                Dim IACA_SecondaryItems As String = ""
                Dim IACA_SecondarySources As String = ""
                Dim IACA_StartDateOldestSecondaryItem As String = ""

                'AOCA
                Dim AOCA_PrimaryDataItems As String = ""
                Dim AOCA_PrimaryDataSources As String = ""
                Dim AOCA_StartDateOldestPrimaryItem As String = ""
                Dim AOCA_SecondaryItems As String = ""
                Dim AOCA_SecondarySources As String = ""
                Dim AOCA_StartDateOldestSecondaryItem As String = ""

                'IAPA
                Dim IAPA_PrimaryDataItems As String = ""
                Dim IAPA_PrimaryDataSources As String = ""
                Dim IAPA_StartDateOldestPrimaryItem As String = ""
                Dim IAPA_SecondaryItems As String = ""
                Dim IAPA_SecondarySources As String = ""
                Dim IAPA_StartDateOldestSecondaryItem As String = ""

                'AOPA
                Dim AOPA_PrimaryDataItems As String = ""
                Dim AOPA_PrimaryDataSources As String = ""
                Dim AOPA_StartDateOldestPrimaryItem As String = ""
                Dim AOPA_SecondaryItems As String = ""
                Dim AOPA_SecondarySources As String = ""
                Dim AOPA_StartDateOldestSecondaryItem As String = ""

                'Data Matches
                Dim DataMatch_NoAgePri As String = ""
                Dim DataMatch_NoAgeSec As String = ""
                Dim DataMatch_NoTACAPri As String = ""
                Dim DataMatch_NoTACASec As String = ""

                If output.Output IsNot Nothing AndAlso output.Output.Control IsNot Nothing Then
                    ExperianReference = output.Output.Control.ExperianReference
                End If

                If output.Output IsNot Nothing AndAlso output.Output.Error IsNot Nothing Then
                    ErrorCode = output.Output.Error.ErrorCode
                    ErrorMessage = output.Output.Error.Message
                    ErrorSeverity = output.Output.Error.Severity
                End If

                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 Then
                    With output.Output.AuthPlus(0)
                        If .Decision IsNot Nothing Then
                            DecCode = .Decision.DecCode
                            DecText = .Decision.DecText
                            AuthIndex = .Decision.AuthIndex
                            AuthText = .Decision.AuthText
                            IDConfLvl = .Decision.IDConfLvl
                            IDConfText = .Decision.IDConfText
                            HighRiskCount = .Decision.HighRiskCount
                        End If
                    End With
                End If

                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 _
                    AndAlso output.Output.AuthPlus(0).IACA IsNot Nothing Then
                    With output.Output.AuthPlus(0).IACA
                        IACA_PrimaryDataItems = .NoPriItem
                        IACA_PrimaryDataSources = .NoPrimSrc
                        IACA_StartDateOldestPrimaryItem = .StrtOldPri
                        IACA_SecondaryItems = .NoSecItem
                        IACA_SecondarySources = .NoSecSrc
                        IACA_StartDateOldestSecondaryItem = .StrtOldSec
                    End With
                End If

                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 _
                    AndAlso output.Output.AuthPlus(0).AOCA IsNot Nothing Then
                    With output.Output.AuthPlus(0).AOCA
                        AOCA_PrimaryDataItems = .NoPriItem
                        AOCA_PrimaryDataSources = .NoPrimSrc
                        AOCA_StartDateOldestPrimaryItem = .StrtOldPri
                        AOCA_SecondaryItems = .NoSecItem
                        AOCA_SecondarySources = .NoSecSrc
                        AOCA_StartDateOldestSecondaryItem = .StrtOldSec
                    End With
                End If

                'IAPA
                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 _
                    AndAlso output.Output.AuthPlus(0).IAPA IsNot Nothing Then
                    With output.Output.AuthPlus(0).IAPA
                        IAPA_PrimaryDataItems = .NoPriItem
                        IAPA_PrimaryDataSources = .NoPrimSrc
                        IAPA_StartDateOldestPrimaryItem = .StrtOldPri
                        IAPA_SecondaryItems = .NoSecItem
                        IAPA_SecondarySources = .NoSecSrc
                        IAPA_StartDateOldestSecondaryItem = .StrtOldSec
                    End With
                End If

                'AOPA
                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 _
                    AndAlso output.Output.AuthPlus(0).AOPA IsNot Nothing Then
                    With output.Output.AuthPlus(0).AOPA
                        AOPA_PrimaryDataItems = .NoPriItem
                        AOPA_PrimaryDataSources = .NoPrimSrc
                        AOPA_StartDateOldestPrimaryItem = .StrtOldPri
                        AOPA_SecondaryItems = .NoSecItem
                        AOPA_SecondarySources = .NoSecSrc
                        AOPA_StartDateOldestSecondaryItem = .StrtOldSec
                    End With
                End If

                'Data Match
                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 _
                    AndAlso output.Output.AuthPlus(0).DataMatches IsNot Nothing Then
                    With output.Output.AuthPlus(0).DataMatches
                        DataMatch_NoAgePri = .NoAgePri
                        DataMatch_NoAgeSec = .NoAgeSec
                        DataMatch_NoTACAPri = .NoTACAPri
                        DataMatch_NoTACASec = .NoTACASec
                    End With
                End If

                objExperianData.InsertAuthenticationPlus(ApplicationID, ExperianReference, DecCode, DecText, AuthIndex, AuthText, IDConfLvl, IDConfText, HighRiskCount _
                                                         , ErrorCode, ErrorMessage, ErrorSeverity _
                                                         , IACA_PrimaryDataItems, IACA_PrimaryDataSources, IACA_StartDateOldestPrimaryItem, IACA_SecondaryItems, IACA_SecondarySources, IACA_StartDateOldestSecondaryItem _
                                                         , AOCA_PrimaryDataItems, AOCA_PrimaryDataSources, AOCA_StartDateOldestPrimaryItem, AOCA_SecondaryItems, AOCA_SecondarySources, AOCA_StartDateOldestSecondaryItem _
                                                         , IAPA_PrimaryDataItems, IAPA_PrimaryDataSources, IAPA_StartDateOldestPrimaryItem, IAPA_SecondaryItems, IAPA_SecondarySources, IAPA_StartDateOldestSecondaryItem _
                                                         , AOPA_PrimaryDataItems, AOPA_PrimaryDataSources, AOPA_StartDateOldestPrimaryItem, AOPA_SecondaryItems, AOPA_SecondarySources, AOPA_StartDateOldestSecondaryItem _
                                                         , DataMatch_NoAgePri, DataMatch_NoAgeSec, DataMatch_NoTACAPri, DataMatch_NoTACASec)

                If output.Output IsNot Nothing AndAlso output.Output.AuthPlus IsNot Nothing AndAlso output.Output.AuthPlus.Count > 0 AndAlso output.Output.AuthPlus(0).Decision IsNot Nothing AndAlso output.Output.AuthPlus(0).Decision.HighRisk IsNot Nothing Then
                    For Each hr As ConsData.OutputAuthPlusDecisionHighRisk In output.Output.AuthPlus(0).Decision.HighRisk
                        objExperianData.InsertAuthenticationPlusHighRisk(ApplicationID, hr.Rule, hr.Text)
                    Next
                End If

                Return True
            Catch ex As Exception
                objSystemData.InsertErrorLog(ex, IPAddress)
                Return False
            End Try
        End Function
#End Region

#Region "Consumer Data Search"

        '''--------------------------------------------------------------------
        ''' <summary>
        ''' 	Do the experian consumer data search (credit check)
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <returns>
        ''' 	Credit Search success status
        ''' </returns>
        ''' <history>
        '''     [Shan HM]  19/12/2011   - Created
        ''' </history>
        '''--------------------------------------------------------------------
        Public Function DoConsumerCreditSearch(ByVal Title As String _
                                             , ByVal Forename As String _
                                             , ByVal MiddleName As String _
                                             , ByVal Surname As String _
                                             , ByVal Gender As String _
                                             , ByVal DateOfBirth_CCYY As Integer _
                                             , ByVal DateOfBirth_MM As Integer _
                                             , ByVal DateOfBirth_DD As Integer _
                                             , ByVal Address1_HouseNumber As String _
                                             , ByVal Address1_Street As String _
                                             , ByVal Address1_PostTown As String _
                                             , ByVal Address1_County As String _
                                             , ByVal Address1_PostCode As String _
                                             , ByVal Address1_TimeAtYears As String _
                                             , ByVal Address1_TimeAtMonths As String _
                                             , ByVal Address2_HouseNumber As String _
                                             , ByVal Address2_Street As String _
                                             , ByVal Address2_PostTown As String _
                                             , ByVal Address2_County As String _
                                             , ByVal Address2_PostCode As String _
                                             , ByVal Address2_TimeAtYears As String _
                                             , ByVal Address2_TimeAtMonths As String _
                                             , ByVal Amount As String _
                                             , ByVal Term As String _
                                             , ByVal Purpose As String _
                                             , ByVal ApplicationID As Integer _
                                             , ByVal IPAddress As String _
                                             , ByVal CreditScoreResultSuccessValue As Integer _
                                             ) As Boolean

            If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxt = TokenSer.CallTokenService(_ExperianEnv)
                If _ctxt Is Nothing OrElse _ctxt.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New ConsData.InteractiveWSWse
            If _ExperianEnv = "LIVE" Then
                service.Url = ConsDataURL_LIVE
            End If

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxt.SecurityToken)
            service.ClientCertificates.Add(_ctxt.ExperianCertificate)

            ' Create a simple input message
            Dim root As New ConsData.Root()
            root.Input = New ConsData.Input()
            root.Input.Control = New ConsData.InputControl()
            'root.Input.Control.ExperianReference = "0123456789"
            root.Input.Control.Parameters = New ConsData.InputControlParameters
            root.Input.Control.Parameters.InteractiveMode = "Interactive"
            root.Input.Control.Parameters.FullFBLRequired = "Y"
            root.Input.Control.Parameters.AuthPlusRequired = "N"
            'root.Input.Control.Parameters.DetectRequired = "Y"
            root.Input.Control.Parameters.ShowConsumer = "Y"
            'root.Input.Control.Parameters.ShowDetect = "Y"
            root.Input.Control.Parameters.ShowAuthenticate = "Y"
            root.Input.Control.Parameters.ShowAddress = "Y"
            root.Input.Control.Parameters.ShowCaseHistory = "Y"
            root.Input.Control.Parameters.SearchConsent = "Y"

            root.Input.ThirdPartyData = New ConsData.InputThirdPartyData
            root.Input.ThirdPartyData.OptOut = "N"
            root.Input.ThirdPartyData.TransientAssocs = "Y"
            root.Input.ThirdPartyData.HHOAllowed = "Y"

            Dim applicant(0) As ConsData.InputApplicant
            applicant(0) = New ConsData.InputApplicant

            root.Input.Applicant = applicant
            root.Input.Applicant(0).Name = New ConsData.InputApplicantName
            root.Input.Applicant(0).ApplicantIdentifier = 1
            root.Input.Applicant(0).Name.Title = Title
            root.Input.Applicant(0).Name.Forename = Forename
            root.Input.Applicant(0).Name.MiddleName = MiddleName
            root.Input.Applicant(0).Name.Surname = Surname
            root.Input.Applicant(0).Gender = Gender

            root.Input.Applicant(0).DateOfBirth = New ConsData.InputApplicantDateOfBirth
            root.Input.Applicant(0).DateOfBirth.CCYY = DateOfBirth_CCYY
            root.Input.Applicant(0).DateOfBirth.CCYYSpecified = True
            root.Input.Applicant(0).DateOfBirth.MM = DateOfBirth_MM
            root.Input.Applicant(0).DateOfBirth.MMSpecified = True
            root.Input.Applicant(0).DateOfBirth.DD = DateOfBirth_DD
            root.Input.Applicant(0).DateOfBirth.DDSpecified = True
            root.Input.Applicant(0).FormattedName = Title & ", " & Left(Forename, 1).ToUpper & " " & IIf(MiddleName.Length > 0, Left(MiddleName, 1).ToUpper, "").ToString & " " & Surname
            root.Input.Applicant(0).FormattedDOB = DateOfBirth_DD.ToString & "/" & DateOfBirth_MM.ToString & "/" & DateOfBirth_CCYY.ToString

            Dim LocationDetails(0) As ConsData.InputLocationDetails
            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                ReDim LocationDetails(1)
                LocationDetails(1) = New ConsData.InputLocationDetails
            End If
            LocationDetails(0) = New ConsData.InputLocationDetails

            root.Input.LocationDetails = LocationDetails
            root.Input.LocationDetails(0).LocationIdentifierSpecified = True
            root.Input.LocationDetails(0).LocationIdentifier = 1
            root.Input.LocationDetails(0).UKLocation = New ConsData.InputLocationDetailsUKLocation
            root.Input.LocationDetails(0).UKLocation.HouseNumber = Address1_HouseNumber
            root.Input.LocationDetails(0).UKLocation.Street = Address1_Street
            root.Input.LocationDetails(0).UKLocation.PostTown = Address1_PostTown
            root.Input.LocationDetails(0).UKLocation.County = Address1_County
            root.Input.LocationDetails(0).UKLocation.Postcode = Address1_PostCode.Trim.ToUpper
            root.Input.LocationDetails(0).UKLocation.Country = "UK"

            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                root.Input.LocationDetails(1).LocationIdentifierSpecified = True
                root.Input.LocationDetails(1).LocationIdentifier = 2
                root.Input.LocationDetails(1).UKLocation = New ConsData.InputLocationDetailsUKLocation
                root.Input.LocationDetails(1).UKLocation.HouseNumber = Address2_HouseNumber
                root.Input.LocationDetails(1).UKLocation.Street = Address2_Street
                root.Input.LocationDetails(1).UKLocation.PostTown = Address2_PostTown
                root.Input.LocationDetails(1).UKLocation.County = Address2_County
                root.Input.LocationDetails(1).UKLocation.Postcode = Address2_PostCode.Trim.ToUpper
                root.Input.LocationDetails(1).UKLocation.Country = "UK"
            End If


            Dim Residency(0) As ConsData.InputResidency
            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                ReDim Residency(1)
                Residency(1) = New ConsData.InputResidency
            End If
            Residency(0) = New ConsData.InputResidency

            root.Input.Residency = Residency
            root.Input.Residency(0).SearchFlag = "Y"
            root.Input.Residency(0).ApplicantIdentifier = "1"
            root.Input.Residency(0).LocationIdentifier = "1"
            root.Input.Residency(0).LocationCode = "01"
            root.Input.Residency(0).TimeAt = New ConsData.InputResidencyTimeAt
            root.Input.Residency(0).TimeAt.Years = Address1_TimeAtYears
            root.Input.Residency(0).TimeAt.Months = Address1_TimeAtMonths

            If Address2_HouseNumber.Length > 0 AndAlso Address2_Street.Length > 0 Then
                root.Input.Residency(1).SearchFlag = "Y"
                root.Input.Residency(1).ApplicantIdentifier = "1"
                root.Input.Residency(1).LocationIdentifier = "2"
                root.Input.Residency(1).LocationCode = "02"
                root.Input.Residency(1).TimeAt = New ConsData.InputResidencyTimeAt
                root.Input.Residency(1).TimeAt.Years = Address2_TimeAtYears
                root.Input.Residency(1).TimeAt.Months = Address2_TimeAtMonths
            End If

            root.Input.Application = New ConsData.InputApplication
            root.Input.Application.ApplicationType = "CU"
            root.Input.Application.Amount = Amount
            root.Input.Application.Term = Term
            root.Input.Application.Purpose = IIf(Purpose >= 1000, 97, Purpose)
            root.Input.Application.ApplicationChannel = "IT"
            'root.Input.Application.AuthenticationType = "C"
            root.Input.Application.SearchConsent = "Y"
            ' Declare the output message
            Dim output As ConsData.OutputRoot
            Dim CreditScoreStatus As Boolean = False
            Try
                ' Call the service and get the response

                output = service.Interactive(root)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    'Save the response in DB
                    If objSaveCreditSearch.SaveConsumerCreditSearch(ApplicationID, output, IPAddress) Then
                        If output.Output.Error IsNot Nothing Then
                            CreditScoreStatus = False
                        Else
                            If output.Output.ConsumerSummary IsNot Nothing AndAlso output.Output.ConsumerSummary.PremiumValueData IsNot Nothing _
                                AndAlso output.Output.ConsumerSummary.PremiumValueData.Scoring IsNot Nothing _
                                AndAlso output.Output.ConsumerSummary.PremiumValueData.Scoring.E5S051 IsNot Nothing _
                                AndAlso output.Output.ConsumerSummary.PremiumValueData.Scoring.E5S051 >= CreditScoreResultSuccessValue Then

                                CreditScoreStatus = True
                            Else

                                CreditScoreStatus = False
                            End If
                        End If
                    Else
                        CreditScoreStatus = False
                    End If
                Else
                    ' Invalid response
                    CreditScoreStatus = False
                End If

                Return CreditScoreStatus
            Catch exc As Exception
                ' An exception was received when calling the web service 
                objSystemData.InsertErrorLog(exc, IPAddress)

                Return False
                'Return ReturnData
            End Try

        End Function

        
#End Region

#Region "Bank Wizard"
        '''--------------------------------------------------------------------
        ''' <summary>
        ''' 	Do the experian bank wizard verify
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <returns>
        ''' 	verify success status
        ''' </returns>
        ''' <history>
        '''     [Shan HM]  06/01/2012   - Created
        ''' </history>
        '''--------------------------------------------------------------------
        Public Function DoBankWizard_Verify(ByVal SortCode As String _
                                             , ByVal AccountNumber As String _
                                             , ByVal AccountSetupDateYear As String _
                                             , ByVal AccountSetupDateMonth As String _
                                             , ByVal AccountType As String _
                                             , ByVal CustomerAccountType As String _
                                             , ByVal FirstName As String _
                                             , ByVal SurName As String _
                                             , ByVal DOBYear As String _
                                             , ByVal DOBMonth As String _
                                             , ByVal DOBDay As String _
                                             , ByVal HouseNumber As String _
                                             , ByVal FlatNumber As String _
                                             , ByVal Street As String _
                                             , ByVal PostCode As String _
                                             , ByVal OwnerType As String _
                                             , ByVal ApplicationID As Integer _
                                             , ByVal IPAddress As String _
                                             , Optional ApplicationType As String = "PDL"
                                             ) As Boolean

            If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                ' Security Context hasn't been initialised 
                ' or the token couldn't be obtained
                _ctxtBankWizard = TokenSer.CallTokenServiceBankWizard(_ExperianEnv)
                If _ctxtBankWizard Is Nothing OrElse _ctxtBankWizard.SecurityToken Is Nothing Then
                    Return "Security Context hasn't been initialised"
                End If
            End If

            ' Create the secured service – note the use of the Wse version of the service
            Dim service As New BankWizard.BankWizard_v1_0_Service
            If _ExperianEnv = "LIVE" Then
                service.Url = BankWizardURL_LIVE
            End If

            ' Add the secure token to the secured service
            service.RequestSoapContext.Security.MustUnderstand = False
            service.RequestSoapContext.Security.Tokens.Add(_ctxtBankWizard.SecurityToken)
            service.ClientCertificates.Add(_ctxtBankWizard.ExperianCertificate)

            Dim VerifyRequest As BankWizard.VerifyRequest = New BankWizard.VerifyRequest
            VerifyRequest.language = "en"
            VerifyRequest.itemisationID = "LMCU"
            VerifyRequest.reportString = "CUOK"


            Dim AccountInfo As New BankWizard.VerifyAccountRequestType
            AccountInfo.sortCode = SortCode
            AccountInfo.accountNumber = AccountNumber
            AccountInfo.checkContext = BankWizard.CheckContextType.DirectCredit
            AccountInfo.accountVerification = New BankWizard.VerifyBankAccountRequestType
            AccountInfo.accountVerification.accountSetupDate = New BankWizard.AccountDateType
            AccountInfo.accountVerification.accountSetupDate.year = AccountSetupDateYear
            AccountInfo.accountVerification.accountSetupDate.month = AccountSetupDateMonth
            AccountInfo.accountVerification.accountTypeInformation = New BankWizard.AccountTypeInformation

            Select Case AccountType
                Case "Basic"
                    AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.Basic
                Case "Current"
                    AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.Current
                Case "ISA"
                    AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.ISA
                Case "Mortgage"
                    AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.Mortgage
                Case "Savings"
                    AccountInfo.accountVerification.accountTypeInformation.accountType = BankWizard.AccountType.Savings
            End Select

            Select Case CustomerAccountType
                Case "Business"
                    AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Business
                Case "Child"
                    AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Child
                Case "Corporate"
                    AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Corporate
                Case "Internal"
                    AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Internal
                Case "Personal"
                    AccountInfo.accountVerification.accountTypeInformation.customerAccountType = BankWizard.CustomerAccountType.Personal
            End Select


            VerifyRequest.accountInformation = AccountInfo

            Dim PersonalInfo As BankWizard.VerifyPersonalRequestType = New BankWizard.VerifyPersonalRequestType
            PersonalInfo.personal = New BankWizard.PersonalDetails
            PersonalInfo.personal.firstName = FirstName
            PersonalInfo.personal.surname = SurName
            PersonalInfo.personal.dob = New DateTime(DOBYear, DOBMonth, DOBDay)
            PersonalInfo.personal.dobSpecified = True

            PersonalInfo.address = New BankWizard.Address

            Dim deliveryPoint(1) As BankWizard.DeliveryPoint
            deliveryPoint(0) = New BankWizard.DeliveryPoint

            If FlatNumber.Length > 0 Then
                deliveryPoint(0).deliveryType = BankWizard.DeliveryPointType.flat
                deliveryPoint(0).Value = FlatNumber
            ElseIf HouseNumber.Length > 0 Then
                deliveryPoint(0).deliveryType = BankWizard.DeliveryPointType.houseNumber
                deliveryPoint(0).Value = HouseNumber
            End If
            PersonalInfo.address.deliveryPoint = deliveryPoint

            Dim PostalPoint(2) As BankWizard.PostalPoint
            PostalPoint(0) = New BankWizard.PostalPoint
            PostalPoint(0).postalType = BankWizard.PostalPointType.street
            PostalPoint(0).Value = Street

            PostalPoint(1) = New BankWizard.PostalPoint
            PostalPoint(1).postalType = BankWizard.PostalPointType.postcode
            PostalPoint(1).Value = PostCode

            PersonalInfo.address.postalPoint = PostalPoint

            Select Case OwnerType
                Case "Joint"
                    PersonalInfo.ownerType = BankWizard.OwnerType.Joint
                    PersonalInfo.ownerTypeSpecified = True
                Case "Single"
                    PersonalInfo.ownerType = BankWizard.OwnerType.Single
                    PersonalInfo.ownerTypeSpecified = True
                Case Else
                    PersonalInfo.ownerTypeSpecified = False
            End Select

            VerifyRequest.personalInformation = PersonalInfo

            Dim output As BankWizard.VerifyResponse
            Dim VerifyStatus As Boolean = False
            Try
                ' Call the service and get the response

                output = service.Verify(VerifyRequest)
                ' Simple sanity check on response
                If output IsNot Nothing Then
                    If SaveBankWizard_Verify(ApplicationID, output, IPAddress, ApplicationType) Then
                        Dim personalScore As Integer = 0
                        If output.personalInformation IsNot Nothing AndAlso Not String.IsNullOrEmpty(output.personalInformation.personalDetailsScore) Then
                            Integer.TryParse(output.personalInformation.personalDetailsScore, personalScore)
                        End If
                        If output.accountInformation IsNot Nothing AndAlso output.accountInformation.accountVerificationStatus = BankWizard.AccountVerificationType.Match AndAlso personalScore > 6 Then
                            VerifyStatus = True
                            If output.conditions IsNot Nothing AndAlso output.conditions.Count > 0 Then
                                For Each con As BankWizard.Condition In output.conditions
                                    If Not AllowBankWizardCondition(con) Then   'If there is an error condition then fail
                                        VerifyStatus = False
                                        Exit For
                                    End If
                                Next
                            End If
                        Else
                            VerifyStatus = False
                        End If
                    Else
                        VerifyStatus = False
                    End If
                Else
                    ' Invalid response
                    VerifyStatus = False
                End If

                Return VerifyStatus
            Catch exc As Exception
                ' An exception was received when calling the web service 
                objSystemData.InsertErrorLog(exc, IPAddress)

                Return False
            End Try
        End Function

        'Ignore below errors and consider as a pass
        Private Function AllowBankWizardCondition(ByVal con As BankWizard.Condition) As Boolean
            Try
                Dim AllowError As Boolean = False

                If con.severity = BankWizard.ConditionSeverity.error Then
                    Select Case con.code
                        Case 11, 12
                            AllowError = True
                        Case Else
                            AllowError = False
                    End Select
                ElseIf con.severity = BankWizard.ConditionSeverity.warning Then
                    Select Case con.code
                        Case 4, 5, 6, 7, 11, 18, 19, 20, 21, 47, 70, 79, 103
                            AllowError = True
                        Case Else
                            AllowError = False
                    End Select
                ElseIf con.severity = BankWizard.ConditionSeverity.information Then
                    Select Case con.code
                        Case 1, 2, 3, 4, 5, 6, 7, 64, 65, 66
                            AllowError = True
                        Case Else
                            AllowError = False
                    End Select
                Else
                    AllowError = False
                End If

                Return AllowError
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Function SaveBankWizard_Verify(ByVal ApplicationID As Integer, ByVal output As BankWizard.VerifyResponse, ByVal IPAddress As String, Optional ApplicationType As String = "PDL")
            Try

                Dim ConditionCount As Integer = 0
                Dim VerificationStatus As String = ""
                Dim BacsCode As String = ""
                Dim DataAccessKey As String = ""
                Dim PersonalDetailsScore As Integer = 0
                Dim AddressScore As Integer = 0
                Dim AccountTypeMatch As String = ""
                Dim AccountSetupDateScore As Integer = 0
                Dim AccountSetupDateMatch As String = ""
                Dim AccountOwnerMatch As String = ""


                If output.conditions IsNot Nothing Then
                    ConditionCount = output.conditions.Count
                End If

                If output.accountInformation IsNot Nothing Then
                    Select Case output.accountInformation.accountVerificationStatus
                        Case BankWizard.AccountVerificationType.Match
                            VerificationStatus = "Match"
                        Case BankWizard.AccountVerificationType.NoMatch
                            VerificationStatus = "NoMatch"
                        Case BankWizard.AccountVerificationType.Unabletocheck
                            VerificationStatus = "Unabletocheck"
                        Case BankWizard.AccountVerificationType.Insufficientdetailstocheck
                            VerificationStatus = "Insufficientdetailstocheck"
                    End Select

                    If output.accountInformation.bacsCode IsNot Nothing Then
                        BacsCode = output.accountInformation.bacsCode.ToString
                    End If
                    If output.accountInformation.dataAccessKey IsNot Nothing Then
                        DataAccessKey = output.accountInformation.dataAccessKey
                    End If

                End If
                If output.personalInformation IsNot Nothing Then
                    PersonalDetailsScore = output.personalInformation.personalDetailsScore
                    AddressScore = output.personalInformation.addressScore
                    AccountTypeMatch = output.personalInformation.accountTypeMatch
                    AccountSetupDateScore = output.personalInformation.accountSetupDateScore
                    AccountSetupDateMatch = output.personalInformation.accountSetupDateMatch
                    AccountOwnerMatch = output.personalInformation.accountOwnerMatch
                End If

                objExperianData.InsertBankWizard_Verify(ApplicationID, ConditionCount, VerificationStatus, BacsCode, DataAccessKey, PersonalDetailsScore _
                                                        , AddressScore, AccountTypeMatch, AccountSetupDateScore, AccountSetupDateMatch, AccountOwnerMatch, ApplicationType)

                If output.conditions IsNot Nothing AndAlso output.conditions.Count > 0 Then
                    For Each con As BankWizard.Condition In output.conditions
                        objExperianData.InsertBankWizard_VerifyCondition(ApplicationID, con.code, con.severity.ToString, con.Value)
                    Next
                End If

                Return True
            Catch ex As Exception
                objSystemData.InsertErrorLog(ex, IPAddress)
                Return False
            End Try
        End Function
#End Region
    End Class
End Namespace
