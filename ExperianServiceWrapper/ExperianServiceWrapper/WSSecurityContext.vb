﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml
Imports System.Security.Cryptography.X509Certificates
Imports Microsoft.Web.Services3.Security.Tokens

Namespace LMCU.ExperianAuthentication
    Public Class WSSecurityContext
        ' The authentication block xml sent to the authenticator
        Private _authBlock As String

        ' The URL of the sign on service.
        ' This is made public so that the URI can be changed at runtime
        Private _signonLocation As String
        Public Property SignonLocation() As String
            Get
                Return _signonLocation
            End Get
            Set(ByVal value As String)
                _signonLocation = value
            End Set
        End Property

        ' The application name of the client
        Private _applicationName As String

        ' The WASP Authenticator Web Service
        Private _wsSignon As ExperianAuthenticator.TokenService

        ' The Client certificate
        Private _experianCertificate As X509Certificate
        Public ReadOnly Property ExperianCertificate As X509Certificate
            Get
                Return _experianCertificate
            End Get
        End Property

        ' The constructor
        Public Sub New(ByVal ApplicationName As String)
            _wsSignon = New ExperianAuthenticator.TokenService()

            _applicationName = ApplicationName

            ' Now that we have the application name the authentication
            ' block can be defined
            _authBlock = String.Format(vbCr & vbLf & vbCr & vbLf & "<WASPAuthenticationRequest>" & vbCr & vbLf & "  <ApplicationName>{0}</ApplicationName>" & vbCr & vbLf & "  <AuthenticationLevel>CertificateAuthentication,IPAuthentication</AuthenticationLevel>" & vbCr & vbLf & "  <AuthenticationParameters/>" & vbCr & vbLf & "</WASPAuthenticationRequest>" & vbCr & vbLf & vbCr & vbLf & "        ", ApplicationName)
        End Sub

        ' The security token returned from the WASP authenticator
        Private _waspToken As WASPToken = Nothing
        Public ReadOnly Property SecurityToken() As WASPToken
            Get
                Return _waspToken
            End Get
        End Property
        ' This method takes a path and filename to a certificate file (*.cer)
        ' and creates a certificate object from it
        Public Function SetCertificateFromFile(ByVal certificateFileName As String) As Boolean
            _experianCertificate = X509Certificate.CreateFromCertFile(certificateFileName)
            Return True
        End Function
        ' This methods pulls everything together and consumes the 
        ' WASP authenticator web service by calling the STS web method
        ' to obtain the security token.
        Public Sub SignOn()
            _waspToken = Nothing
            ' Overwrite the web service URL just in case the client wants
            ' to use a different authenticator at runtime
            If _signonLocation <> String.Empty Then
                _wsSignon.Url = _signonLocation
            End If

            '  Add the X.509 certificate To the request
            _wsSignon.ClientCertificates.Add(_experianCertificate)

            '_wsSignon.Timeout = 1200000
            Dim response As String = _wsSignon.STS(_authBlock)

            ' Check the response for errors, if there are no errors
            ' output the result into the WASPToken
            If response IsNot Nothing AndAlso response.IndexOf("Error") < 0 Then
                _waspToken = New WASPToken(response)
                ' The token could not be retrieved from the authenticator
            Else
            End If
        End Sub
    End Class
End Namespace
