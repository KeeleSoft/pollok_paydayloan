<?xml version="1.0" encoding="utf-8"?>
<xsd:schema xmlns:bankwizard="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:bwacommon="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" xmlns:bwcommon="http://experianpayments.com/bankwizard/common/xsd/2009/09" xmlns="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified" targetNamespace="http://experianpayments.com/bankwizard/xsd/2009/07" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:import schemaLocation="BankWizardCommon-v1-0.xsd" namespace="http://experianpayments.com/bankwizard/common/xsd/2009/09" />
  <xsd:import schemaLocation="BankWizardAbsoluteCommon-v1-0.xsd" namespace="http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09" />
  <xsd:annotation>
    <xsd:documentation xml:lang="en">
       Bank Wizard Service schema.
       
       @Author : Amarjit Basra
       @Date   : 30 September 2009
       @Version: Major 1 Minor 0
       
       Copyright (c) Experian Limited 1997-2009. All Rights Reserved.
    </xsd:documentation>
  </xsd:annotation>
  <xsd:complexType name="GetCountriesRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Request a list of all available countries on the server, with
        an indication of those that are the user is licensed for. 
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="GetCountriesResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Response containing the countries available on the server, with
        an indication of those that are licensed for the user.             
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="unbounded" name="country" type="bankwizard:CountryInformation" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="GetCountryInputRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Request the Bank Wizard checking levels available for an ISO 3166-1 two-alpha country code the input BBAN details
        associated with the checking levels.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="bwcommon:ISO3166-1">
        <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="GetCountryInputResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code.              
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="4" name="validationCheck" type="bankwizard:BankWizardCheckType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="ValidateIBANRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Validation of an IBAN.

        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported. 
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="bwcommon:IBAN">
        <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
        <xsd:attribute name="reportString" type="bwcommon:ReportString" use="optional" />
        <xsd:attribute name="itemisationID" type="bwcommon:ItemisationID" use="optional" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="ValidateIBANResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Validation of an IBAN response.
        
       For a successful validation the 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">BBAN</code>: decomposed IBAN 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>: for accessing lookup functions and 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">conditions</code>: BankWizard core conditions
         *<code xmlns="http://www.w3.org/2001/XMLSchema">ISOcountry</code>: country code associated with the IBAN
        are returned.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="5" name="BBAN" type="bankwizard:BBANResponseType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="dataAccessKey" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="conditions" type="bwcommon:Conditions" />
    </xsd:sequence>
    <xsd:attribute name="ISOcountry" type="bwcommon:ISO3166-1" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="ValidateRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Validation of a bank account from BBANs request.

        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:ValidationBBANRequestType">
        <xsd:sequence />
        <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
        <xsd:attribute name="reportString" type="bwcommon:ReportString" use="optional" />
        <xsd:attribute name="itemisationID" type="bwcommon:ItemisationID" use="optional" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="ValidateResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
       Validation of a bank account response.
              
       For a successful validation the 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">BBAN</code>: transposed BBANs
         *<code xmlns="http://www.w3.org/2001/XMLSchema">IBAN</code> 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>: for accessing lookup functions and 
         *<code xmlns="http://www.w3.org/2001/XMLSchema">conditions</code>: BankWizard core conditions
        are returned.  
     </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="5" name="BBAN" type="bankwizard:BBANBaseType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="IBAN" type="bwcommon:IBAN" />
      <xsd:element minOccurs="0" maxOccurs="1" name="dataAccessKey" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="conditions" type="bwcommon:Conditions" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VerifyRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verify a bank account with personal details request.

        An optional language attribute can be passed in. English (en) is the
        default language returned if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="accountInformation" type="bankwizard:VerifyAccountRequestType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="personalInformation" type="bankwizard:VerifyPersonalRequestType" />
    </xsd:sequence>
    <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
    <xsd:attribute name="reportString" type="bwcommon:ReportString" use="optional" />
    <xsd:attribute name="itemisationID" type="bwcommon:ItemisationID" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="VerifyResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification of a bank account with personal details response.
        
        Fatal conditions will be returned as SOAP Exceptions. 
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="accountInformation" type="bankwizard:VerifiedAccountType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="personalInformation" type="bankwizard:VerifyPersonalResponseType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="conditions" type="bwcommon:Conditions" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VerifiedAccountType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Details of the account details verified, note these may be different to what was requested following transposition. 
        The <code xmlns="http://www.w3.org/2001/XMLSchema">accountVerificationStatus</code> attribute is <b xmlns="http://www.w3.org/2001/XMLSchema">present</b> details the matching status from the absolute service.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="sortCode" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="accountNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="rollNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="bacsCode" type="bwacommon:BACSCodeType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="dataAccessKey" type="xsd:string" />
    </xsd:sequence>
    <xsd:attribute name="accountVerificationStatus" type="bwacommon:AccountVerificationType" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="ValidationBBANRequestType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        <code xmlns="http://www.w3.org/2001/XMLSchema">ValidationRequestType</code> element, containing a <code xmlns="http://www.w3.org/2001/XMLSchema">BBAN</code> element 
        with <code xmlns="http://www.w3.org/2001/XMLSchema">ISOCountry</code> and <code xmlns="http://www.w3.org/2001/XMLSchema">checkingLevel</code> attributes.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="5" name="BBAN" type="bankwizard:BBANBaseType" />
    </xsd:sequence>
    <xsd:attribute name="ISOCountry" type="bwcommon:ISO3166-1" use="required" />
    <xsd:attribute name="checkingLevel" type="bankwizard:CheckingLevel" use="required" />
  </xsd:complexType>
  <xsd:complexType name="GetBranchDataRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Lookup a Branch address based on <code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>.
        
        The <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> attribute is used to indicate if sub-branches are to be returned. 
        If false or missing only the main branch is requested.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:DataAccessKeyType">
        <xsd:sequence />
        <xsd:attribute default="false" name="returnSubBranches" type="xsd:boolean" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="GetBranchDataResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Branch details response.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was false or missing, only the main branch is returned and <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchesAvailable</code>
        is set to indicate if sub-branches exist.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was true, then sub-branches are also returned in the response as additional
        <code xmlns="http://www.w3.org/2001/XMLSchema">branchData</code> elements.                       
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="branchData" type="bankwizard:BranchDataType" />
    </xsd:sequence>
    <xsd:attribute name="subBranchesAvailable" type="xsd:boolean" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="GetSWIFTDataRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Lookup a SWIFT data based on the based on <code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>.
        
        The <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> attribute is used to indicate if sub-branches are to be returned. 
        If false or missing only the main branch is requested.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:DataAccessKeyType">
        <xsd:sequence />
        <xsd:attribute default="false" name="returnSubBranches" type="xsd:boolean" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="GetSWIFTDataResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        SWIFT branch details response.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was false or missing, only the main branch is returned and <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchesAvailable</code>
        is set to indicate if sub-branches exist.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was true, then sub-branches are also returned in the response as additional
        <code xmlns="http://www.w3.org/2001/XMLSchema">swiftData</code> elements.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="swiftData" type="bankwizard:SWIFTDataType" />
    </xsd:sequence>
    <xsd:attribute name="subBranchesAvailable" type="xsd:boolean" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="GetSEPADataRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Lookup a SEPA data based on the based on <code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>.
        
        The <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> attribute is used to indicate if sub-branches are to be returned. 
        If false or missing only the main branch is requested.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:DataAccessKeyType">
        <xsd:sequence />
        <xsd:attribute default="false" name="returnSubBranches" type="xsd:boolean" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="GetSEPADataResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        SEPA branch details response.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was false or missing, only the main branch is returned and <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchesAvailable</code>
        is set to indicate if sub-branches exist.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was true, then sub-branches are also returned in the response as additional
        <code xmlns="http://www.w3.org/2001/XMLSchema">sepaData</code> elements.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="sepaData" type="bankwizard:SEPADataType" />
    </xsd:sequence>
    <xsd:attribute name="subBranchesAvailable" type="xsd:boolean" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="GetFasterPaymentsDataRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Lookup a Faster Payment data based on the <code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>.
        
        The <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> attribute is used to indicate if sub-branches are to be returned. 
        If false or missing only the main branch is requested.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:DataAccessKeyType">
        <xsd:sequence />
        <xsd:attribute default="false" name="returnSubBranches" type="xsd:boolean" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="GetFasterPaymentsDataResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Faster payments branch details response.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was false or missing, only the main branch is returned and <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchesAvailable</code>
        is set to indicate if sub-branches exist.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was true, then sub-branches are also returned in the response as additional
        <code xmlns="http://www.w3.org/2001/XMLSchema">fasterPaymentsData</code> elements.             
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="fasterPaymentsData" type="bankwizard:FasterPaymentDataType" />
    </xsd:sequence>
    <xsd:attribute name="subBranchesAvailable" type="xsd:boolean" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="GetAdditionalDataRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Request all the additional country specific data based on the <code xmlns="http://www.w3.org/2001/XMLSchema">dataAccessKey</code>.

        The <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> attribute is used to indicate if sub-branches are to be returned. 
        If false or missing only the main branch is requested.
        
        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.        
      </xsd:documentation>
    </xsd:annotation>
    <xsd:complexContent mixed="false">
      <xsd:extension base="bankwizard:DataAccessKeyType">
        <xsd:sequence />
        <xsd:attribute default="false" name="returnSubBranches" type="xsd:boolean" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:complexType name="GetAdditionalDataResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Additional data response.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was false or missing, only the main branch is returned and <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchesAvailable</code>
        is set to indicate if sub-branches exist.
        
        If the <code xmlns="http://www.w3.org/2001/XMLSchema">returnSubBranches</code> was true, then sub-branches are also returned in the response as additional
        elements of the <code xmlns="http://www.w3.org/2001/XMLSchema">additionalBranchData</code> element.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="additionalBranchData" type="bankwizard:AdditionalBranchDataType" />
    </xsd:sequence>
    <xsd:attribute name="subBranchesAvailable" type="xsd:boolean" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="DataAccessKeyType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        <code xmlns="http://www.w3.org/2001/XMLSchema">key</code> is that returned from the Validate or Verify operation, which can be used to 
        access branch data
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="dataAccessKey" type="xsd:string" />
    </xsd:sequence>
    <xsd:attribute default="en" name="language" type="xsd:language" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="AdditionalBranchDataType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Additional Branch Data.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" name="branchInfo" type="bankwizard:AdditionalBranchElement" />
    </xsd:sequence>
    <xsd:attribute name="subBranchNumber" type="xsd:integer" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="AdditionalBranchElement">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
         Complete listing of branch related data, where
         code : intended for use by software to provide an algorithmic mechanism to identify the condition.
         description : human readable description of the data         
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="code" type="xsd:integer" use="required" />
        <xsd:attribute name="description" type="xsd:string" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="FasterPaymentDataType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Faster Payments Data.
        For the main branch the <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchNumber</code> will be zero.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="status" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="agencyType" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="lastChangeDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="redirectedToBranch" type="xsd:boolean" />
      <xsd:element minOccurs="0" maxOccurs="1" name="redirectingSortcode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="clearingClosedDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="handlingBankConnType" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="handlingBankCode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountsNumberedFlag" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="settlementBankConnection" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="settlementBankcode" type="xsd:string" />
    </xsd:sequence>
    <xsd:attribute name="subBranchNumber" type="xsd:integer" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="SWIFTDataType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        SWIFT Data.
        For the main branch the <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchNumber</code> will be zero.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="swiftBranchBIC" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="chipsUID" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="swiftCategory" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="swiftService" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="paymentRoutingBIC" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="institutionPOBoxNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="institutionPOBoxPostcode" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="institutionPOBoxLocation" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="institutionPOBoxCountry" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="swiftExtraInformation" type="xsd:string" />
    </xsd:sequence>
    <xsd:attribute name="subBranchNumber" type="xsd:integer" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="SEPADataType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        SWIFT Data.
        For the main branch the <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchNumber</code> will be zero.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="ibanBIC" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="SSIBIC" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="creditTransferAdherenceDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="creditTransferNonComplianceDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="directDebitAdherenceDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="directDebitNonComplianceDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="priorityPaymentAdherenceDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="priorityPaymentNonComplianceDate" type="xsd:date" />
    </xsd:sequence>
    <xsd:attribute name="subBranchNumber" type="xsd:integer" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="BranchDataType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Standard Bank Wizard branch data type.
        For the main branch the <code xmlns="http://www.w3.org/2001/XMLSchema">subBranchNumber</code> will be zero.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="institutionName" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="branchName" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="address" type="bankwizard:BranchAddressType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="telephoneNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="faxNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="closureDate" type="xsd:date" />
      <xsd:element minOccurs="0" maxOccurs="1" name="redirectedTo" type="xsd:string" />
    </xsd:sequence>
    <xsd:attribute name="subBranchNumber" type="xsd:integer" use="optional" />
  </xsd:complexType>
  <xsd:complexType name="BranchAddressType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Branch Address Type.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="5" name="addressLine" type="bankwizard:AddressLineType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="postOrZipCode" type="xsd:string" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="AddressLineType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Branch Address line.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="line" type="bankwizard:RestrictedInteger" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="CountryInformation">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Country element contains the country name  and the following attributes
          * ISO3166  : the 2-alpha 3166-1 ISO code for the country.
          * Licensed : boolean indication to identify the country module is licensed.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="ISOCountry" type="bwcommon:ISO3166-1" use="required" />
        <xsd:attribute name="licensed" type="xsd:boolean" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="BankWizardCheckType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        The BBAN required to be passed in for a specific validation checking level.
        The element is made up of the following:
         * BBAN  : input BBAN details
         * checkingLevel : the validation checking level 
         * description : the country modules description of the checking level         
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="5" name="BBAN" type="bankwizard:BBANResponseType" />
    </xsd:sequence>
    <xsd:attribute name="checkingLevel" type="bankwizard:CheckingLevel" />
    <xsd:attribute name="description" type="xsd:string" />
  </xsd:complexType>
  <xsd:complexType name="BBANResponseType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Extension of <code xmlns="http://www.w3.org/2001/XMLSchema">BBANBase</code> element to include the following optional attributes
        * <code xmlns="http://www.w3.org/2001/XMLSchema">description</code> of the BBAN field.
        * <code xmlns="http://www.w3.org/2001/XMLSchema">maxSize</code> maximum size for the BBAN.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="bankwizard:BBANBaseType">
        <xsd:attribute name="description" type="xsd:string" />
        <xsd:attribute name="maxSize" type="xsd:integer" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:complexType name="BBANBaseType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        BBAN element which contains the BBAN value (string) and its input index for Bank Wizard validation.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="index" type="bankwizard:RestrictedInteger" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:simpleType name="RestrictedInteger">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
         Restricted integer used for BBAN indexes and Branch address elements.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:integer">
      <xsd:minInclusive value="1" />
      <xsd:maxInclusive value="5" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name="CheckingLevel">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Validation checking levels.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="Branch" />
      <xsd:enumeration value="Account" />
      <xsd:enumeration value="Domestic" />
      <xsd:enumeration value="BIC" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name="VerifyBankAccountRequestType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification account details request element.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="accountSetupDate" type="bankwizard:AccountDateType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountTypeInformation" type="bankwizard:AccountTypeInformation" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="AccountTypeInformation">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        The account information.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="accountType" type="bwacommon:AccountType" />
      <xsd:element minOccurs="1" maxOccurs="1" name="customerAccountType" type="bwacommon:CustomerAccountType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VerifyPersonalRequestType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification personal details request element.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="personal" type="bankwizard:PersonalDetails" />
      <xsd:element minOccurs="0" maxOccurs="1" name="address" type="bankwizard:Address" />
      <xsd:element minOccurs="0" maxOccurs="1" name="ownerType" nillable="true" type="bwacommon:OwnerType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VerifyPersonalResponseType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification personal details response element.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="personalDetailsScore" type="bwacommon:VerifyScoreType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="addressScore" type="bwacommon:VerifyScoreType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountSetupDateMatch" type="bwacommon:PersonalDetailsVerificationType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountSetupDateScore" type="bwacommon:VerifyScoreType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountTypeMatch" type="bwacommon:PersonalDetailsVerificationType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountOwnerMatch" type="bwacommon:PersonalDetailsVerificationType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="VerifyAccountRequestType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verify a bank account request.

        An optional language attribute can be passed in. English (en) is the
        default language returned if the requested type is not supported.      
        
        The <code xmlns="http://www.w3.org/2001/XMLSchema">userID</code> and <code xmlns="http://www.w3.org/2001/XMLSchema">branchNumber</code> are optional client defined fields.   
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="sortCode" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="accountNumber" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="rollNumber" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="checkContext" type="bwacommon:CheckContextType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="accountVerification" type="bankwizard:VerifyBankAccountRequestType" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="PersonalDetails">
    <xsd:annotation>
      <xsd:documentation xml:lang="en" />
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" name="firstName" type="xsd:string" />
      <xsd:element minOccurs="1" maxOccurs="1" name="surname" type="xsd:string" />
      <xsd:element minOccurs="0" maxOccurs="1" name="dob" type="xsd:date" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="Address">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Address which consists of :
           * <code xmlns="http://www.w3.org/2001/XMLSchema">DeliveryPoint</code> which is the house name and/or house number and/or flat.
           * <code xmlns="http://www.w3.org/2001/XMLSchema">PostalDetails</code> which is the street and/or post code.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="3" name="deliveryPoint" type="bankwizard:DeliveryPoint">
        <xsd:unique name="uniqueDeliveryPoint">
          <xsd:selector xpath="deliveryPoint" />
          <xsd:field xpath="@deliveryType" />
        </xsd:unique>
      </xsd:element>
      <xsd:element minOccurs="1" maxOccurs="2" name="postalPoint" type="bankwizard:PostalPoint">
        <xsd:unique name="uniquePostalDetails">
          <xsd:selector xpath="postaldetails" />
          <xsd:field xpath="@postalType" />
        </xsd:unique>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="DeliveryPoint">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
         The delivery point.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="deliveryType" type="bankwizard:DeliveryPointType" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:simpleType name="DeliveryPointType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification account type enumeration.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="houseName" />
      <xsd:enumeration value="houseNumber" />
      <xsd:enumeration value="flat" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name="PostalPoint">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
         The delivery point.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="postalType" type="bankwizard:PostalPointType" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>
  <xsd:simpleType name="PostalPointType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification account type enumeration.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="street" />
      <xsd:enumeration value="postcode" />
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:complexType name="AccountDateType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Verification date element.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="1" name="day" type="bwacommon:DayType" />
      <xsd:element minOccurs="0" maxOccurs="1" name="month" type="bwacommon:MonthType" />
      <xsd:element minOccurs="1" maxOccurs="1" name="year" type="bwacommon:YearType" />
    </xsd:sequence>
  </xsd:complexType>
</xsd:schema>