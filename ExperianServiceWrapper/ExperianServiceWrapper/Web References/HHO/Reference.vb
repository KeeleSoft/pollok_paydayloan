﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.239
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.239.
'
Namespace HHO
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1"), _
     System.Diagnostics.DebuggerStepThroughAttribute(), _
     System.ComponentModel.DesignerCategoryAttribute("code"), _
     System.Web.Services.WebServiceBindingAttribute(Name:="InteractiveWSSoap", [Namespace]:="http://www.uk.experian.com/experian/wbsv/peinteractive/v100")> _
    Partial Public Class InteractiveWSWSe
        Inherits Microsoft.Web.Services3.WebServicesClientProtocol

        Private InteractiveOperationCompleted As System.Threading.SendOrPostCallback

        Private useDefaultCredentialsSetExplicitly As Boolean

        '''<remarks/>
        Public Sub New()
            MyBase.New()
            Me.Url = Global.ExperianServiceWrapper.My.MySettings.Default.ExperianServiceWrapper_HHO_InteractiveWS
            If (Me.IsLocalFileSystemWebService(Me.Url) = True) Then
                Me.UseDefaultCredentials = True
                Me.useDefaultCredentialsSetExplicitly = False
            Else
                Me.useDefaultCredentialsSetExplicitly = True
            End If
        End Sub

        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set(ByVal value As String)
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = True) _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = False)) _
                            AndAlso (Me.IsLocalFileSystemWebService(Value) = False)) Then
                    MyBase.UseDefaultCredentials = False
                End If
                MyBase.Url = Value
            End Set
        End Property

        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set(ByVal value As Boolean)
                MyBase.UseDefaultCredentials = Value
                Me.useDefaultCredentialsSetExplicitly = True
            End Set
        End Property

        '''<remarks/>
        Public Event InteractiveCompleted As InteractiveCompletedEventHandler

        '''<remarks/>
        <System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.uk.experian.com/experian/wbsv/peinteractive/interactive", RequestNamespace:="http://www.uk.experian.com/experian/wbsv/peinteractive/v100", ResponseNamespace:="http://www.uk.experian.com/experian/wbsv/peinteractive/v100", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)> _
        Public Function Interactive(<System.Xml.Serialization.XmlElementAttribute([Namespace]:="http://schemas.microsoft.com/BizTalk/2003/Any")> ByVal Root As Root) As <System.Xml.Serialization.XmlElementAttribute("OutputRoot", [Namespace]:="http://schemas.microsoft.com/BizTalk/2003/Any")> OutputRoot
            Dim results() As Object = Me.Invoke("Interactive", New Object() {Root})
            Return CType(results(0), OutputRoot)
        End Function

        '''<remarks/>
        Public Overloads Sub InteractiveAsync(ByVal Root As Root)
            Me.InteractiveAsync(Root, Nothing)
        End Sub

        '''<remarks/>
        Public Overloads Sub InteractiveAsync(ByVal Root As Root, ByVal userState As Object)
            If (Me.InteractiveOperationCompleted Is Nothing) Then
                Me.InteractiveOperationCompleted = AddressOf Me.OnInteractiveOperationCompleted
            End If
            Me.InvokeAsync("Interactive", New Object() {Root}, Me.InteractiveOperationCompleted, userState)
        End Sub

        Private Sub OnInteractiveOperationCompleted(ByVal arg As Object)
            If (Not (Me.InteractiveCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg, System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent InteractiveCompleted(Me, New InteractiveCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub

        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub

        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing) _
                        OrElse (url Is String.Empty)) Then
                Return False
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024) _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return True
            End If
            Return False
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class Root
        
        Private inputField As Input
        
        '''<remarks/>
        Public Property Input() As Input
            Get
                Return Me.inputField
            End Get
            Set
                Me.inputField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class Input
        
        Private controlField As InputControl
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Control() As InputControl
            Get
                Return Me.controlField
            End Get
            Set
                Me.controlField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class InputControl
        
        Private experianReferenceField As String
        
        Private clientAccountNumberField As String
        
        Private clientBranchNumberField As String
        
        Private userIdentityField As String
        
        Private testDatabaseField As String
        
        Private reprocessFlagField As String
        
        Private clientRefField As String
        
        Private jobNumberField As String
        
        Private parametersField As InputControlParameters
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ExperianReference() As String
            Get
                Return Me.experianReferenceField
            End Get
            Set
                Me.experianReferenceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientAccountNumber() As String
            Get
                Return Me.clientAccountNumberField
            End Get
            Set
                Me.clientAccountNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientBranchNumber() As String
            Get
                Return Me.clientBranchNumberField
            End Get
            Set
                Me.clientBranchNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property UserIdentity() As String
            Get
                Return Me.userIdentityField
            End Get
            Set
                Me.userIdentityField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property TestDatabase() As String
            Get
                Return Me.testDatabaseField
            End Get
            Set
                Me.testDatabaseField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ReprocessFlag() As String
            Get
                Return Me.reprocessFlagField
            End Get
            Set
                Me.reprocessFlagField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientRef() As String
            Get
                Return Me.clientRefField
            End Get
            Set
                Me.clientRefField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property JobNumber() As String
            Get
                Return Me.jobNumberField
            End Get
            Set
                Me.jobNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Parameters() As InputControlParameters
            Get
                Return Me.parametersField
            End Get
            Set
                Me.parametersField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class InputControlParameters
        
        Private interactiveModeField As String
        
        Private fullFBLRequiredField As String
        
        Private authPlusRequiredField As String
        
        Private detectRequiredField As String
        
        Private testModeField As String
        
        Private showConsumerField As String
        
        Private showDetectField As String
        
        Private showAuthenticateField As String
        
        Private showAddressField As String
        
        Private showCaseHistoryField As String
        
        Private showHHOField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property InteractiveMode() As String
            Get
                Return Me.interactiveModeField
            End Get
            Set
                Me.interactiveModeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property FullFBLRequired() As String
            Get
                Return Me.fullFBLRequiredField
            End Get
            Set
                Me.fullFBLRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property AuthPlusRequired() As String
            Get
                Return Me.authPlusRequiredField
            End Get
            Set
                Me.authPlusRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property DetectRequired() As String
            Get
                Return Me.detectRequiredField
            End Get
            Set
                Me.detectRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property TestMode() As String
            Get
                Return Me.testModeField
            End Get
            Set
                Me.testModeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowConsumer() As String
            Get
                Return Me.showConsumerField
            End Get
            Set
                Me.showConsumerField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowDetect() As String
            Get
                Return Me.showDetectField
            End Get
            Set
                Me.showDetectField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowAuthenticate() As String
            Get
                Return Me.showAuthenticateField
            End Get
            Set
                Me.showAuthenticateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowAddress() As String
            Get
                Return Me.showAddressField
            End Get
            Set
                Me.showAddressField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowCaseHistory() As String
            Get
                Return Me.showCaseHistoryField
            End Get
            Set
                Me.showCaseHistoryField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowHHO() As String
            Get
                Return Me.showHHOField
            End Get
            Set
                Me.showHHOField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class Output
        
        Private controlField As OutputControl
        
        Private secondPhaseHHOField As OutputSecondPhaseHHO
        
        Private errorField As OutputError
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Control() As OutputControl
            Get
                Return Me.controlField
            End Get
            Set
                Me.controlField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property SecondPhaseHHO() As OutputSecondPhaseHHO
            Get
                Return Me.secondPhaseHHOField
            End Get
            Set
                Me.secondPhaseHHOField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property [Error]() As OutputError
            Get
                Return Me.errorField
            End Get
            Set
                Me.errorField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputControl
        
        Private experianReferenceField As String
        
        Private clientAccountNumberField As String
        
        Private clientBranchNumberField As String
        
        Private userIdentityField As String
        
        Private testDatabaseField As String
        
        Private reprocessFlagField As String
        
        Private clientRefField As String
        
        Private jobNumberField As String
        
        Private parametersField As OutputControlParameters
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ExperianReference() As String
            Get
                Return Me.experianReferenceField
            End Get
            Set
                Me.experianReferenceField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientAccountNumber() As String
            Get
                Return Me.clientAccountNumberField
            End Get
            Set
                Me.clientAccountNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientBranchNumber() As String
            Get
                Return Me.clientBranchNumberField
            End Get
            Set
                Me.clientBranchNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property UserIdentity() As String
            Get
                Return Me.userIdentityField
            End Get
            Set
                Me.userIdentityField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property TestDatabase() As String
            Get
                Return Me.testDatabaseField
            End Get
            Set
                Me.testDatabaseField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ReprocessFlag() As String
            Get
                Return Me.reprocessFlagField
            End Get
            Set
                Me.reprocessFlagField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ClientRef() As String
            Get
                Return Me.clientRefField
            End Get
            Set
                Me.clientRefField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property JobNumber() As String
            Get
                Return Me.jobNumberField
            End Get
            Set
                Me.jobNumberField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Parameters() As OutputControlParameters
            Get
                Return Me.parametersField
            End Get
            Set
                Me.parametersField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputControlParameters
        
        Private interactiveModeField As String
        
        Private fullFBLRequiredField As String
        
        Private authPlusRequiredField As String
        
        Private detectRequiredField As String
        
        Private testModeField As String
        
        Private showConsumerField As String
        
        Private showDetectField As String
        
        Private showAuthenticateField As String
        
        Private showAddressField As String
        
        Private showCaseHistoryField As String
        
        Private showHHOField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property InteractiveMode() As String
            Get
                Return Me.interactiveModeField
            End Get
            Set
                Me.interactiveModeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property FullFBLRequired() As String
            Get
                Return Me.fullFBLRequiredField
            End Get
            Set
                Me.fullFBLRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property AuthPlusRequired() As String
            Get
                Return Me.authPlusRequiredField
            End Get
            Set
                Me.authPlusRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property DetectRequired() As String
            Get
                Return Me.detectRequiredField
            End Get
            Set
                Me.detectRequiredField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property TestMode() As String
            Get
                Return Me.testModeField
            End Get
            Set
                Me.testModeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowConsumer() As String
            Get
                Return Me.showConsumerField
            End Get
            Set
                Me.showConsumerField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowDetect() As String
            Get
                Return Me.showDetectField
            End Get
            Set
                Me.showDetectField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowAuthenticate() As String
            Get
                Return Me.showAuthenticateField
            End Get
            Set
                Me.showAuthenticateField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowAddress() As String
            Get
                Return Me.showAddressField
            End Get
            Set
                Me.showAddressField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowCaseHistory() As String
            Get
                Return Me.showCaseHistoryField
            End Get
            Set
                Me.showCaseHistoryField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ShowHHO() As String
            Get
                Return Me.showHHOField
            End Get
            Set
                Me.showHHOField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputSecondPhaseHHO
        
        Private hHOScoreField As String
        
        Private thirdPartyDataField As OutputSecondPhaseHHOThirdPartyData
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property HHOScore() As String
            Get
                Return Me.hHOScoreField
            End Get
            Set
                Me.hHOScoreField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ThirdPartyData() As OutputSecondPhaseHHOThirdPartyData
            Get
                Return Me.thirdPartyDataField
            End Get
            Set
                Me.thirdPartyDataField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputSecondPhaseHHOThirdPartyData
        
        Private tPDOutcomeCodeField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property TPDOutcomeCode() As String
            Get
                Return Me.tPDOutcomeCodeField
            End Get
            Set
                Me.tPDOutcomeCodeField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=true, [Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputError
        
        Private errorCodeField As String
        
        Private messageField As String
        
        Private severityField As String
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property ErrorCode() As String
            Get
                Return Me.errorCodeField
            End Get
            Set
                Me.errorCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Message() As String
            Get
                Return Me.messageField
            End Get
            Set
                Me.messageField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlElementAttribute(Form:=System.Xml.Schema.XmlSchemaForm.Unqualified)>  _
        Public Property Severity() As String
            Get
                Return Me.severityField
            End Get
            Set
                Me.severityField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://schema.uk.experian.com/experian/cems/msgs/v1.0/SecondPhaseHHO")>  _
    Partial Public Class OutputRoot
        
        Private outputField As Output
        
        '''<remarks/>
        Public Property Output() As Output
            Get
                Return Me.outputField
            End Get
            Set
                Me.outputField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")>  _
    Public Delegate Sub InteractiveCompletedEventHandler(ByVal sender As Object, ByVal e As InteractiveCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class InteractiveCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As OutputRoot
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),OutputRoot)
            End Get
        End Property
    End Class
End Namespace
