﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : LeedsCU.DataAccess.Layer
''' Class      : clsSystemData.vb
'''********************************************************************
''' <Summary>
'''	This module calls lookup data, SystemKey and Command Centre related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 18/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsSystemData
    Dim clsConn As clsConnection = New clsConnection
    Dim objCon As SqlConnection

#Region "Lookup Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Get Lookup data From Category Code
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  18/09/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetLookupFromCatCode(ByVal sCatCode As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLookupData As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_LOOKUP_CODE_FROM_CATEGORYCODE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@CategoryCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CategoryCode").Value = sCatCode

            Dim daLookupData As New SqlDataAdapter(cmdCommand)
            daLookupData.Fill(dtLookupData)
            GetLookupFromCatCode = dtLookupData

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve lookup data for category " & sCatCode & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Get Lookup data From Category ID
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  18/09/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetLookupFromCatID(ByVal iCatID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLookupData As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_LOOKUP_CODE_FROM_CATEGORYID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@CategoryCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CategoryCode").Value = iCatID

            Dim daLookupData As New SqlDataAdapter(cmdCommand)
            daLookupData.Fill(dtLookupData)
            GetLookupFromCatID = dtLookupData

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve lookup data for categoryid " & iCatID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "System Data"
    Public Function GetSystemKeyValue(ByRef systemKeyCode As String) As String
        Dim cmdCommand As New SqlCommand

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_SYSTEMKEY_VALUE_FROM_CODE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SystemKeyCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SystemKeyCode").Value = systemKeyCode

            GetSystemKeyValue = cmdCommand.ExecuteScalar()

        Catch ex As Exception
            Throw New ApplicationException("Failed to get system key value" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Get email addresses
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectEmailSettings() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLookupData As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_EMAIL_SETTINGS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daLookupData As New SqlDataAdapter(cmdCommand)
            daLookupData.Fill(dtLookupData)
            SelectEmailSettings = dtLookupData

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve email settings " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update System Key
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/03/2011  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateSystemKey(ByVal sSystemKeyCode As String _
                                        , ByVal sSystemKeyValue As String _
                                        ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("UPDATE_SYSTEM_KEY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SystemKeyCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SystemKeyCode").Value = sSystemKeyCode

            cmdCommand.Parameters.Add("@SystemKeyValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@SystemKeyValue").Value = sSystemKeyValue

            intResult = cmdCommand.ExecuteNonQuery
            UpdateSystemKey = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update system key" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Email Body"
    Public Function GetEmailBody(ByVal TemplateCode As String _
                                 , ByVal MemberID As String _
                                 , ByVal FromAccount As String _
                                 , ByVal SortCode As String _
                                 , ByVal AccountNumber As String _
                                 , ByVal AccountName As String _
                                 , ByVal Amount As String _
                                 , ByVal Reference As String _
                                 , ByVal CUAccount As String _
                                 , ByVal StartingDate As String _
                                 , ByVal Frequency As String _
                                 , ByVal NoOfPayments As String _
                                 , Optional ByVal ContactNo As String = "" _
                                 , Optional ByVal Nature As String = "" _
                                 , Optional ByVal MessageText As String = "" _
                                 , Optional ByVal ToAccount As String = "" _
                                 , Optional ByVal Password As String = "" _
                                 , Optional ByVal MemInfo As String = "" _
                                 , Optional ByVal Text1 As String = "" _
                                 , Optional ByVal Text2 As String = "" _
                                 , Optional ByVal Text3 As String = "" _
                                 , Optional ByVal Text4 As String = "" _
                                 , Optional ByVal Text5 As String = "" _
                                 , Optional ByVal Text6 As String = "" _
                                 , Optional ByVal Text7 As String = "" _
                                 , Optional ByVal Text8 As String = "" _
                                 , Optional ByVal Text9 As String = "" _
                                 , Optional ByVal Text10 As String = "" _
                                 , Optional ByVal Text11 As String = "" _
                                 , Optional ByVal Text12 As String = "" _
                                 , Optional ByVal Text13 As String = "" _
                                 , Optional ByVal Text14 As String = "" _
                                 , Optional ByVal Text15 As String = "" _
                                ) As String
        Dim cmdCommand As New SqlCommand

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_EMAIL_BODY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@TemplateCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@TemplateCode").Value = TemplateCode

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@FromAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@FromAccount").Value = FromAccount

            cmdCommand.Parameters.Add("@SortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SortCode").Value = SortCode

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@AccountName", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountName").Value = AccountName

            cmdCommand.Parameters.Add("@Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@Reference", SqlDbType.VarChar)
            cmdCommand.Parameters("@Reference").Value = Reference

            cmdCommand.Parameters.Add("@CUAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@CUAccount").Value = CUAccount

            cmdCommand.Parameters.Add("@StartingDate", SqlDbType.VarChar)
            cmdCommand.Parameters("@StartingDate").Value = StartingDate

            cmdCommand.Parameters.Add("@Frequency", SqlDbType.VarChar)
            cmdCommand.Parameters("@Frequency").Value = Frequency

            cmdCommand.Parameters.Add("@NoOfPayments", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoOfPayments").Value = NoOfPayments

            cmdCommand.Parameters.Add("@ContactNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@ContactNo").Value = ContactNo

            cmdCommand.Parameters.Add("@Nature", SqlDbType.VarChar)
            cmdCommand.Parameters("@Nature").Value = Nature

            cmdCommand.Parameters.Add("@MessageText", SqlDbType.VarChar)
            cmdCommand.Parameters("@MessageText").Value = MessageText

            cmdCommand.Parameters.Add("@ToAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@ToAccount").Value = ToAccount

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = Password

            cmdCommand.Parameters.Add("@MemInfo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemInfo").Value = MemInfo

            cmdCommand.Parameters.Add("@Text1", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text1").Value = Text1

            cmdCommand.Parameters.Add("@Text2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text2").Value = Text2

            cmdCommand.Parameters.Add("@Text3", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text3").Value = Text3

            cmdCommand.Parameters.Add("@Text4", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text4").Value = Text4

            cmdCommand.Parameters.Add("@Text5", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text5").Value = Text5

            cmdCommand.Parameters.Add("@Text6", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text6").Value = Text6

            cmdCommand.Parameters.Add("@Text7", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text7").Value = Text7

            cmdCommand.Parameters.Add("@Text8", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text8").Value = Text8

            cmdCommand.Parameters.Add("@Text9", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text9").Value = Text9

            cmdCommand.Parameters.Add("@Text10", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text10").Value = Text10

            cmdCommand.Parameters.Add("@Text11", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text11").Value = Text11

            cmdCommand.Parameters.Add("@Text12", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text12").Value = Text12

            cmdCommand.Parameters.Add("@Text13", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text13").Value = Text13

            cmdCommand.Parameters.Add("@Text14", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text14").Value = Text14

            cmdCommand.Parameters.Add("@Text15", SqlDbType.VarChar)
            cmdCommand.Parameters("@Text15").Value = Text15

            GetEmailBody = cmdCommand.ExecuteScalar()

        Catch ex As Exception
            Throw New ApplicationException("Failed to get email body " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Command Centre"
    Public Function execCommand(ByVal sCommand As String) As Boolean
        Dim cmdCommand As New SqlCommand
        Dim bReturn As Boolean
        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand(sCommand, objCon)
            cmdCommand.CommandType = CommandType.Text
            cmdCommand.ExecuteNonQuery()
            bReturn = True
        Catch ex As Exception
            bReturn = False
            Throw New ApplicationException("Failed to execute command " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
        Return bReturn
    End Function

    Public Function execSelect(ByVal sCommand As String) As DataSet
        Dim cmdCommand As New SqlCommand
        Dim dtOP As New DataSet
        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand(sCommand, objCon)
            cmdCommand.CommandType = CommandType.Text

            Dim daAdaptor As New SqlDataAdapter(cmdCommand)
            daAdaptor.Fill(dtOP)
            Return dtOP
        Catch ex As Exception
            Throw New ApplicationException("Failed to execute select " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Salutations"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all salutations
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	salutations datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  21/05/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectSalutations() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtSal As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_SALUTATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daSal As New SqlDataAdapter(cmdCommand)
            daSal.Fill(dtSal)
            SelectSalutations = dtSal

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve salutations." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Post codes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all post codes
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	post codes datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  02/10/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPostCodes() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtPC As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_POST_CODES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daPC As New SqlDataAdapter(cmdCommand)
            daPC.Fill(dtPC)
            SelectPostCodes = dtPC

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve post codes." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get borough and ward details for a given postcode
    ''' </summary>
    Public Function SelectBoroughAndWardDetails(ByVal postCode As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_BOROUGH_WARD_DETAILS_FOR_POSTCODE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = postCode

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            Return dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Employers"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all employers
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	employers datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  28/04/2010  520.BU01 - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectEmployers() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtE As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_EMPLOYERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daE As New SqlDataAdapter(cmdCommand)
            daE.Fill(dtE)
            SelectEmployers = dtE

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve employer list." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Site Content"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Get site content from name
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/12/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectSiteContentDetail(ByVal sSiteContentName As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtSiteContent As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_SITECONTENT_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SiteContentName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SiteContentName").Value = sSiteContentName

            Dim daSiteContent As New SqlDataAdapter(cmdCommand)
            daSiteContent.Fill(dtSiteContent)
            SelectSiteContentDetail = dtSiteContent

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve sitecontent" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update Site Content
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  19/04/2010  520.BU01 - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateSiteContent(ByVal sSiteContentName As String _
                                            , ByVal sSiteContent As String _
                                            , ByVal bDisplay As Boolean _
                                            , ByVal iUserID As Integer _
                                     ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("UPDATE_SITECONTENT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SiteContentName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SiteContentName").Value = sSiteContentName


            cmdCommand.Parameters.Add("@SiteContent", SqlDbType.VarChar)
            cmdCommand.Parameters("@SiteContent").Value = sSiteContent

            cmdCommand.Parameters.Add("@Display", SqlDbType.Bit)
            cmdCommand.Parameters("@Display").Value = bDisplay

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = iUserID

            intResult = cmdCommand.ExecuteNonQuery
            UpdateSiteContent = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update site content" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Error Login"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Log an Error
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  04/11/2011  Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertErrorLog(ByVal ex As Exception, ByVal IPAddress As String, Optional ExtraData As String = "") As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.INSERT_ERROR_LOG", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ErrMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrMessage").Value = ex.Message & "-" & ExtraData

            cmdCommand.Parameters.Add("@StackTrace", SqlDbType.VarChar)
            cmdCommand.Parameters("@StackTrace").Value = ex.StackTrace

            cmdCommand.Parameters.Add("@CreatedIPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreatedIPAddress").Value = IPAddress

            If ex.Message <> "Thread was being aborted." Then
                intResult = cmdCommand.ExecuteNonQuery
            End If
            InsertErrorLog = intResult
        Catch ex2 As Exception
            Throw New ApplicationException("Failed to insert the error" & vbCrLf & ex2.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Country List"
    ''' <summary>
    ''' Get the full country list
    ''' </summary>
    Public Function SelectCountryList() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtCountries As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_COUNTRY_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            Dim daCountries As New SqlDataAdapter(cmdCommand)
            daCountries.Fill(dtCountries)
            SelectCountryList = dtCountries

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region
End Class
