﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Text
Imports System.Net

Public Class clsSMTPEmail
    'Send emails with an array of To list
    'This procedure takes string array parameters for multiple recipients and files
    Public Sub SendEmailMessage(ByVal strFrom As String _
                                , ByVal strTo() As String _
                                , ByVal strSubject As String _
                                , ByVal strMessage As String _
                                , ByVal fileList() As String)
        Try
            For Each item As String In strTo
                'For each to address create a mail message
                Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(item))
                MailMsg.BodyEncoding = Encoding.Default
                MailMsg.Subject = strSubject.Trim()
                MailMsg.Body = strMessage.Trim() & vbCrLf
                MailMsg.Priority = MailPriority.High
                MailMsg.IsBodyHtml = True

                'attach each file attachment
                For Each strfile As String In fileList
                    If Not strfile = "" Then
                        Dim MsgAttach As New Attachment(strfile)
                        MailMsg.Attachments.Add(MsgAttach)
                    End If
                Next

                'Smtpclient to send the mail message
                Dim SmtpMail As New SmtpClient
                'SmtpMail.Host = "mail.nettune.co.uk"
                'SmtpMail.Credentials = New NetworkCredential("testprojects@nettune.co.uk", "malli2000")
                SmtpMail.Send(MailMsg)
            Next
            'Message Successful
        Catch ex As Exception
            Throw New ApplicationException("Error occured while sending smtp email" & vbCrLf & ex.Message & vbCrLf & ex.InnerException.Message)
        End Try
    End Sub
    'Send email to 1 To address
    'This procedure overrides the first procedure and accepts a single
    'string for the recipient and file attachement 
    Public Sub SendEmailMessage(ByVal strFrom As String _
                                , ByVal strTo As String _
                                , ByVal strSubject As String _
                                , ByVal strMessage As String _
                                , ByVal file As String _
                                , Optional ByVal strCC As String = "" _
                                , Optional ByVal fileList As List(Of String) = Nothing)
        Try
            Dim MailMsg As New MailMessage(New MailAddress(strFrom.Trim()), New MailAddress(strTo))
            If strCC <> "" AndAlso Not strCC.Contains(",") Then
                MailMsg.CC.Add(strCC)
            ElseIf strCC.Contains(",") Then
                Dim ccList() As String = strCC.Split(","c)
                For Each ccAddress In ccList
                    MailMsg.CC.Add(ccAddress)
                Next
            End If

            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = strSubject.Trim()
            MailMsg.Body = strMessage.Trim() & vbCrLf
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True

            If Not file = "" Then
                Dim MsgAttach As New Attachment(file)
                MailMsg.Attachments.Add(MsgAttach)
            End If

            'attach each file attachment
            If fileList IsNot Nothing AndAlso fileList.Count > 0 Then
                For Each strfile As String In fileList
                    If Not strfile = "" Then
                        Dim MsgAttach As New Attachment(strfile)
                        MailMsg.Attachments.Add(MsgAttach)
                    End If
                Next
            End If
            'Smtpclient to send the mail message
            Dim SmtpMail As New SmtpClient
            'SmtpMail.Host = "10.0.20.2"
            'SmtpMail.Credentials = New NetworkCredential("noreply", "Sl*0987")
            SmtpMail.Send(MailMsg)
            MailMsg = Nothing
            SmtpMail = Nothing

        Catch ex As Exception
            Throw New SmtpException("Error occured while sending smtp email" & vbCrLf & ex.Message & vbCrLf & ex.InnerException.Message)
        End Try
    End Sub
End Class
