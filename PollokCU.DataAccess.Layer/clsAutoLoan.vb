﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsAutoLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls Auto Loan Related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 11/04/2012 : Created
'''     [Isuru] 17/08/2016 : Updated
''' </history>
'''********************************************************************
Public Class clsAutoLoan
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Parameters"
    Public Function GetAutoLoanParameters() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PARAMETERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Get Previous PayDayLoan details"
    Public Function GetPreviousPayDayLoanDetails(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PAYDAYLOAN_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region


#Region "One Third Rule & All other initail details"
    Public Function GetInitialMemberDetails(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_INITIAL_MEMBER_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "One Third Rule"
    Public Function GetOneThirdRuleData(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_ONETHIRD_RULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Entitlement Rule"
    Public Function GetLoanEntitlementRuleData(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_LOAN_ENTITLEMENT_RULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Affordability Rule"
    Public Function GetAffordabilityRuleData(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_AFFORDABILITY_RULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Share To Loan Rule"
    Public Function GetShareToLoanRuleData(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_SHARE_TO_LOAN_RULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Pay Day Loan Rule"
    Public Function GetPayDayLoanRuleData(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PAY_DAY_LOAN_RULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Insert Update Auto Loan"
    Public Function InsertUpdateApplication(ByRef AutoLoanID As Integer _
                                       , ByVal UserID As Integer _
                                       , Optional ByVal AppType As String = "" _
                                       , Optional ByVal ApplyMethod As String = "" _
                                       , Optional ByVal MemberID As Integer = 0 _
                                       , Optional ByVal LoanAmount As Double = 0 _
                                       , Optional ByVal CompletedLevel As Integer = 0 _
                                       , Optional ByVal ApplicationStatus As Integer = 0 _
                                       , Optional ByVal LoanOutcomeFlag As Integer = 0 _
                                       , Optional ByVal FinalLoanAmount As Double = 0 _
                                       , Optional ByVal FinalLoanTerm As Integer = 0 _
                                       , Optional ByVal FinalMonthlyPayment As Double = 0 _
                                       , Optional ByVal OneThirdRuleStatus As Integer = 0 _
                                       , Optional ByVal ShareToLoanRuleStatus As Integer = 0 _
                                       , Optional ByVal PayDayLoanRuleStatus As Integer = 0 _
                                       , Optional ByVal OneThirdRuleResultString As String = "" _
                                       , Optional ByVal ShareToLoanRuleResultString As String = "" _
                                       , Optional ByVal DecisionCalcsDataString As String = "" _
                                       , Optional ByVal PayDayLoanRuleResultString As String = "" _
                                       , Optional ByVal InputProductCode As String = "" _
                                        , Optional ByVal InputCurrentRepayments As Double = 0 _
                                        , Optional ByVal InputShareBalance As Double = 0 _
                                        , Optional ByVal InputExistingLoanBalance As Double = 0 _
                                        , Optional ByVal InputRiskLimit As Integer = 0 _
                                        , Optional ByVal InputTotalIncome As Double = 0 _
                                        , Optional ByVal TotalMaxLoanLimitGivenRepayments As Double = 0 _
                                        , Optional ByVal TotalMaxLoanLimitGivenRepaymentsSTL As Double = 0 _
                                        , Optional ByVal CurrentRisk As Double = 0 _
                                        , Optional ByVal MaxLoan As Double = 0 _
                                        , Optional ByVal MaxRepaymentForLoanFixed As Double = 0 _
                                        , Optional ByVal MaxRepaymentForLoanFixedSTL As Double = 0 _
                                        , Optional ByVal TotalIncomeAfterRatio As Double = 0 _
                                        , Optional ByVal HighestIncomeSelected As Double = 0 _
                                        , Optional ByVal HighestIncomeSelectedSTL As Double = 0 _
                                        , Optional ByVal ShareBalanceAfterSTL As Double = 0 _
                                        , Optional ByVal CurrentLoanBalanceAfterSTL As Double = 0 _
                                        , Optional ByVal STLReductionAmount As Double = 0 _
                                        , Optional ByVal NoSTLLoanEntitlement As Double = 0 _
                                        , Optional ByVal NoSTLTotalLoanBalance As Double = 0 _
                                        , Optional ByVal NoSTLRepaymentAmount As Double = 0 _
                                        , Optional ByVal NoSTLNewLoanTerm As Integer = 0 _
                                        , Optional ByVal NoSTLLoanEntitlementPreRisk As Double = 0 _
                                        , Optional ByVal NoSTLPreRiskLoanBalance As Double = 0 _
                                        , Optional ByVal NoSTLHigherThanMaxReduceFigure As Double = 0 _
                                        , Optional ByVal YesSTLLoanEntitlement As Double = 0 _
                                        , Optional ByVal YesSTLTotalLoanBalance As Double = 0 _
                                        , Optional ByVal YesSTLRepaymentAmount As Double = 0 _
                                        , Optional ByVal YesSTLNewLoanTerm As Integer = 0 _
                                        , Optional ByVal YesSTLLoanEntitlementPreRisk As Double = 0 _
                                        , Optional ByVal YesSTLPreRiskLoanBalance As Double = 0 _
                                        , Optional ByVal YesSTLHigherThanMaxReduceFigure As Double = 0 _
                                        , Optional ByVal YesSTLRequestedAmountFinalTerm As Double = 0 _
                                        , Optional ByVal YesSTLRequestedAmountFinalRepayment As Double = 0 _
                                        , Optional ByVal ShareToLoanAccepted As Boolean = False _
                                       , Optional ByVal AppealReason As String = "" _
                                       , Optional ByVal PaymentMethod As String = "" _
                                       , Optional ByVal PaymentAccountNumber As String = "" _
                                       , Optional ByVal PaymentSortCode As String = "" _
                                       , Optional ByVal SMSSecurityCode As Integer = 0 _
                                       , Optional ByVal NextPayDay As DateTime = Nothing _
                                       , Optional ByVal FinalOldLoanAmount As Double = 0 _
                                       , Optional ByVal FinalOldLoanTerm As Integer = 0 _
                                       , Optional ByVal FinalOldMonthlyPayment As Double = 0 _
                                       , Optional ByVal NoSTLRequestedAmountTotalLoanBalance As Double = 0 _
                                       , Optional ByVal NoSTLRequestedAmountFinalTerm As Integer = 0 _
                                       , Optional ByVal NoSTLRequestedAmountFinalRepayment As Double = 0 _
                                       , Optional ByVal YesSTLRequestedAmountTotalLoanBalance As Double = 0 _
                                      ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_UPDATE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            cmdCommand.Parameters.Add("@ApplyMethod", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplyMethod").Value = ApplyMethod

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@LoanAmount ", SqlDbType.Money)
            cmdCommand.Parameters("@LoanAmount ").Value = IIf(LoanAmount > 0, LoanAmount, System.DBNull.Value)

            cmdCommand.Parameters.Add("@CompletedLevel", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CompletedLevel").Value = IIf(CompletedLevel > 0, CompletedLevel, System.DBNull.Value)

            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = IIf(ApplicationStatus > 0, ApplicationStatus, System.DBNull.Value)

            cmdCommand.Parameters.Add("@OneThirdRuleStatus", SqlDbType.TinyInt)
            cmdCommand.Parameters("@OneThirdRuleStatus").Value = IIf(OneThirdRuleStatus > 0, OneThirdRuleStatus, System.DBNull.Value)

            cmdCommand.Parameters.Add("@ShareToLoanRuleStatus", SqlDbType.TinyInt)
            cmdCommand.Parameters("@ShareToLoanRuleStatus").Value = IIf(ShareToLoanRuleStatus > 0, ShareToLoanRuleStatus, System.DBNull.Value)

            cmdCommand.Parameters.Add("@LoanOutcomeFlag", SqlDbType.TinyInt)
            cmdCommand.Parameters("@LoanOutcomeFlag").Value = LoanOutcomeFlag

            cmdCommand.Parameters.Add("@FinalLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalLoanAmount").Value = FinalLoanAmount

            cmdCommand.Parameters.Add("@FinalLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalLoanTerm").Value = FinalLoanTerm

            cmdCommand.Parameters.Add("@FinalMonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@FinalMonthlyPayment").Value = FinalMonthlyPayment

            cmdCommand.Parameters.Add("@PayDayLoanRuleStatus", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PayDayLoanRuleStatus").Value = IIf(PayDayLoanRuleStatus > 0, PayDayLoanRuleStatus, System.DBNull.Value)

            cmdCommand.Parameters.Add("@OneThirdRuleResultString", SqlDbType.VarChar)
            cmdCommand.Parameters("@OneThirdRuleResultString").Value = OneThirdRuleResultString

            cmdCommand.Parameters.Add("@ShareToLoanRuleResultString", SqlDbType.VarChar)
            cmdCommand.Parameters("@ShareToLoanRuleResultString").Value = ShareToLoanRuleResultString

            cmdCommand.Parameters.Add("@DecisionCalcsDataString", SqlDbType.VarChar)
            cmdCommand.Parameters("@DecisionCalcsDataString").Value = DecisionCalcsDataString

            cmdCommand.Parameters.Add("@PayDayLoanRuleResultString", SqlDbType.VarChar)
            cmdCommand.Parameters("@PayDayLoanRuleResultString").Value = PayDayLoanRuleResultString

            'Decision calculate aided input data
            cmdCommand.Parameters.Add("@InputProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@InputProductCode").Value = InputProductCode

            cmdCommand.Parameters.Add("@InputCurrentRepayments", SqlDbType.Money)
            cmdCommand.Parameters("@InputCurrentRepayments").Value = InputCurrentRepayments

            cmdCommand.Parameters.Add("@InputShareBalance", SqlDbType.Money)
            cmdCommand.Parameters("@InputShareBalance").Value = InputShareBalance

            cmdCommand.Parameters.Add("@InputExistingLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@InputExistingLoanBalance").Value = InputExistingLoanBalance

            cmdCommand.Parameters.Add("@InputRiskLimit", SqlDbType.Int)
            cmdCommand.Parameters("@InputRiskLimit").Value = InputRiskLimit

            cmdCommand.Parameters.Add("@InputTotalIncome", SqlDbType.Money)
            cmdCommand.Parameters("@InputTotalIncome").Value = InputTotalIncome

            'Decision Calculation Data
            cmdCommand.Parameters.Add("@TotalMaxLoanLimitGivenRepayments", SqlDbType.Money)
            cmdCommand.Parameters("@TotalMaxLoanLimitGivenRepayments").Value = TotalMaxLoanLimitGivenRepayments

            cmdCommand.Parameters.Add("@TotalMaxLoanLimitGivenRepaymentsSTL", SqlDbType.Money)
            cmdCommand.Parameters("@TotalMaxLoanLimitGivenRepaymentsSTL").Value = TotalMaxLoanLimitGivenRepaymentsSTL

            cmdCommand.Parameters.Add("@CurrentRisk", SqlDbType.Money)
            cmdCommand.Parameters("@CurrentRisk").Value = CurrentRisk

            cmdCommand.Parameters.Add("@MaxLoan", SqlDbType.Money)
            cmdCommand.Parameters("@MaxLoan").Value = MaxLoan

            cmdCommand.Parameters.Add("@MaxRepaymentForLoanFixed", SqlDbType.Money)
            cmdCommand.Parameters("@MaxRepaymentForLoanFixed").Value = MaxRepaymentForLoanFixed

            cmdCommand.Parameters.Add("@MaxRepaymentForLoanFixedSTL", SqlDbType.Money)
            cmdCommand.Parameters("@MaxRepaymentForLoanFixedSTL").Value = MaxRepaymentForLoanFixedSTL

            cmdCommand.Parameters.Add("@TotalIncomeAfterRatio", SqlDbType.Money)
            cmdCommand.Parameters("@TotalIncomeAfterRatio").Value = TotalIncomeAfterRatio

            cmdCommand.Parameters.Add("@HighestIncomeSelected", SqlDbType.Money)
            cmdCommand.Parameters("@HighestIncomeSelected").Value = HighestIncomeSelected

            cmdCommand.Parameters.Add("@HighestIncomeSelectedSTL", SqlDbType.Money)
            cmdCommand.Parameters("@HighestIncomeSelectedSTL").Value = HighestIncomeSelectedSTL

            cmdCommand.Parameters.Add("@ShareBalanceAfterSTL", SqlDbType.Money)
            cmdCommand.Parameters("@ShareBalanceAfterSTL").Value = ShareBalanceAfterSTL

            cmdCommand.Parameters.Add("@CurrentLoanBalanceAfterSTL", SqlDbType.Money)
            cmdCommand.Parameters("@CurrentLoanBalanceAfterSTL").Value = CurrentLoanBalanceAfterSTL

            cmdCommand.Parameters.Add("@STLReductionAmount", SqlDbType.Money)
            cmdCommand.Parameters("@STLReductionAmount").Value = STLReductionAmount

            cmdCommand.Parameters.Add("@NoSTLLoanEntitlement", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLLoanEntitlement").Value = NoSTLLoanEntitlement

            cmdCommand.Parameters.Add("@NoSTLTotalLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLTotalLoanBalance").Value = NoSTLTotalLoanBalance

            cmdCommand.Parameters.Add("@NoSTLRepaymentAmount", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLRepaymentAmount").Value = NoSTLRepaymentAmount

            cmdCommand.Parameters.Add("@NoSTLNewLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NoSTLNewLoanTerm").Value = NoSTLNewLoanTerm

            cmdCommand.Parameters.Add("@NoSTLLoanEntitlementPreRisk", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLLoanEntitlementPreRisk").Value = NoSTLLoanEntitlementPreRisk

            cmdCommand.Parameters.Add("@NoSTLPreRiskLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLPreRiskLoanBalance").Value = NoSTLPreRiskLoanBalance

            cmdCommand.Parameters.Add("@NoSTLHigherThanMaxReduceFigure", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLHigherThanMaxReduceFigure").Value = NoSTLHigherThanMaxReduceFigure

            cmdCommand.Parameters.Add("@YesSTLLoanEntitlement", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLLoanEntitlement").Value = YesSTLLoanEntitlement

            cmdCommand.Parameters.Add("@YesSTLTotalLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLTotalLoanBalance").Value = YesSTLTotalLoanBalance

            cmdCommand.Parameters.Add("@YesSTLRepaymentAmount", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLRepaymentAmount").Value = YesSTLRepaymentAmount

            cmdCommand.Parameters.Add("@YesSTLNewLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@YesSTLNewLoanTerm").Value = YesSTLNewLoanTerm

            cmdCommand.Parameters.Add("@YesSTLLoanEntitlementPreRisk", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLLoanEntitlementPreRisk").Value = YesSTLLoanEntitlementPreRisk

            cmdCommand.Parameters.Add("@YesSTLPreRiskLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLPreRiskLoanBalance").Value = YesSTLPreRiskLoanBalance

            cmdCommand.Parameters.Add("@YesSTLHigherThanMaxReduceFigure", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLHigherThanMaxReduceFigure").Value = YesSTLHigherThanMaxReduceFigure

            cmdCommand.Parameters.Add("@YesSTLRequestedAmountFinalTerm", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLRequestedAmountFinalTerm").Value = YesSTLRequestedAmountFinalTerm

            cmdCommand.Parameters.Add("@YesSTLRequestedAmountFinalRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLRequestedAmountFinalRepayment").Value = YesSTLRequestedAmountFinalRepayment

            'End of decision calculation data

            cmdCommand.Parameters.Add("@ShareToLoanAccepted", SqlDbType.Bit)
            cmdCommand.Parameters("@ShareToLoanAccepted").Value = ShareToLoanAccepted

            cmdCommand.Parameters.Add("@AppealReason", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppealReason").Value = AppealReason

            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentMethod").Value = If(Not String.IsNullOrEmpty(PaymentMethod), PaymentMethod, System.DBNull.Value)

            cmdCommand.Parameters.Add("@PaymentAccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentAccountNumber").Value = IIf(Not String.IsNullOrEmpty(PaymentAccountNumber), PaymentAccountNumber, System.DBNull.Value)

            cmdCommand.Parameters.Add("@PaymentSortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentSortCode").Value = IIf(Not String.IsNullOrEmpty(PaymentSortCode), PaymentSortCode, System.DBNull.Value)

            cmdCommand.Parameters.Add("@SMSSecurityCode", SqlDbType.Int)
            cmdCommand.Parameters("@SMSSecurityCode").Value = SMSSecurityCode

            cmdCommand.Parameters.Add("@NextPayDay", SqlDbType.DateTime)
            If IsNothing(NextPayDay) OrElse NextPayDay = Date.MinValue Then
                cmdCommand.Parameters("@NextPayDay").Value = System.DBNull.Value
            Else
                cmdCommand.Parameters("@NextPayDay").Value = NextPayDay
            End If

            cmdCommand.Parameters.Add("@FinalOldLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalOldLoanAmount").Value = FinalOldLoanAmount

            cmdCommand.Parameters.Add("@FinalOldLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalOldLoanTerm").Value = FinalOldLoanTerm

            cmdCommand.Parameters.Add("@FinalOldMonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@FinalOldMonthlyPayment").Value = FinalOldMonthlyPayment

            cmdCommand.Parameters.Add("@NoSTLRequestedAmountTotalLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLRequestedAmountTotalLoanBalance").Value = NoSTLRequestedAmountTotalLoanBalance

            cmdCommand.Parameters.Add("@NoSTLRequestedAmountFinalTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NoSTLRequestedAmountFinalTerm").Value = NoSTLRequestedAmountFinalTerm

            cmdCommand.Parameters.Add("@NoSTLRequestedAmountFinalRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@NoSTLRequestedAmountFinalRepayment").Value = NoSTLRequestedAmountFinalRepayment

            cmdCommand.Parameters.Add("@YesSTLRequestedAmountTotalLoanBalance", SqlDbType.Money)
            cmdCommand.Parameters("@YesSTLRequestedAmountTotalLoanBalance").Value = YesSTLRequestedAmountTotalLoanBalance

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            If AutoLoanID > 0 Then
                cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@AutoLoanID").Direction = ParameterDirection.Output
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If AutoLoanID <= 0 Then
                AutoLoanID = CInt(cmdCommand.Parameters("@AutoLoanID").Value)
            End If

            InsertUpdateApplication = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert/update application" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Auto Loan Duplicate Check"
    Public Function GeAutoLoanDuplicateCheck(ByVal MemberID As Integer, ByVal AppType As String) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_LOAN_DUPLICATED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            Dim duplicated As Boolean = cmdCommand.ExecuteScalar

            Return duplicated
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Statuses"
    Public Function GetAllAutoLoanApplicationStatuses() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_APPLICATION_STATUSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Staff - Loan Applications"
    Public Function GetAllAutoloanApplications(Optional ByVal FirstName As String = "" _
                                          , Optional ByVal SurName As String = "" _
                                          , Optional ByVal Level As Int16 = 0 _
                                          , Optional ByVal ApplicationStatus As Integer = 0 _
                                          , Optional ByVal MemberID As Integer = 0) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_APPLICATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If Not FirstName Is Nothing AndAlso FirstName.Length > 0 Then
                cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
                cmdCommand.Parameters("@FirstName").Value = FirstName
            End If

            If Not SurName Is Nothing AndAlso SurName.Length > 0 Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            If Level > 0 Then
                cmdCommand.Parameters.Add("@CompletedLevel", SqlDbType.TinyInt)
                cmdCommand.Parameters("@CompletedLevel").Value = Level
            End If

            If ApplicationStatus > 0 Then
                cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
                cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus
            End If

            If MemberID > 0 Then
                cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
                cmdCommand.Parameters("@MemberID").Value = MemberID
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateAutoLoanApplicationByStaff(ByVal ID As Integer _
                                         , ByVal ApplicationStatus As Integer _
                                         , ByVal UserID As Integer _
                                         , Optional CompletedLevel As Integer = 0) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_APPLICATION_BY_STAFF", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            cmdCommand.Parameters.Add("@CompletedLevel", SqlDbType.Int)
            cmdCommand.Parameters("@CompletedLevel").Value = CompletedLevel


            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanApplicationByStaff = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update application" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateAutoLoanApprovedByStaffID(ByVal ID As Integer _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_APPLICATION_APPROVE_BY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanApprovedByStaffID = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateAutoLoanSendToBACS(ByVal ID As Integer _
                                         , ByVal StaffID As Integer _
                                         ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_SENT_TO_BACS_FILE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanSendToBACS = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateAutoLoanGrantedDate(ByVal ID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_APPLICATION_GRANTED_DATE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanGrantedDate = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAutoLoanPendingBACSEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PENDING_BACS_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateAutoLoanFinalFigures(ByVal ID As Integer _
                                         , ByVal FinalLoanAmount As Double _
                                         , ByVal FinalLoanTerm As Integer _
                                         , ByVal FinalMonthlyPayment As Double _
                                         , ByVal STLAmount As Double _
                                         , ByVal StaffID As Integer _
                                         , ByVal FinalOldLoanAmount As Double _
                                         , ByVal FinalOldLoanTerm As Integer _
                                         , ByVal FinalOldMonthlyPayment As Double) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_APPLICATION_FINAL_FIGURES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@FinalLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalLoanAmount").Value = FinalLoanAmount

            cmdCommand.Parameters.Add("@FinalLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalLoanTerm").Value = FinalLoanTerm

            cmdCommand.Parameters.Add("@FinalMonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@FinalMonthlyPayment").Value = FinalMonthlyPayment

            cmdCommand.Parameters.Add("@STLAmount", SqlDbType.Money)
            cmdCommand.Parameters("@STLAmount").Value = STLAmount

            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@FinalOldLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalOldLoanAmount").Value = FinalOldLoanAmount

            cmdCommand.Parameters.Add("@FinalOldLoanTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalOldLoanTerm").Value = FinalOldLoanTerm

            cmdCommand.Parameters.Add("@FinalOldMonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@FinalOldMonthlyPayment").Value = FinalOldMonthlyPayment

            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanFinalFigures = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function DeleteAutoLoan(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_DELETE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            DeleteAutoLoan = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemoveAutoLoanPendingBacs(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_REMOVE_PENDING_BACS_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemoveAutoLoanPendingBacs = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateForceSagePayAllow(ByVal ApplicationID As Integer, ByVal Status As Boolean _
                                                           , ByVal StaffID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_FORCE_SAGE_PAY_ALLOW", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@Status", SqlDbType.Bit)
            cmdCommand.Parameters("@Status").Value = Status

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function InsertAutoLoanNote(ByVal AutoLoanID As Integer _
                                     , ByVal Note As String _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertAutoLoanNote = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert note" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAutoLoanNotes(ByVal AutoLoanID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select pay day loan details for a given id
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	PDLoan Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  15/08/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetAutoLoanDetail(ByVal ID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("AUTOLOAN_SELECT_APPLICATION_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetAutoLoanDetail = dtLoanDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve application details for id " & ID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function


#End Region

#Region "SMS Auto Loan Text Related"
    Public Function InsertSMSCommunicationEntry(ByVal AutoLoanID As Integer _
                                  , ByVal MemberID As Integer _
                                  , ByVal MobileNumber As String _
                                  , ByVal TextMessage As String _
                                  , ByVal InboundMessageID As String _
                                  , ByVal InboundMessageAccountID As String _
                                  , ByVal InboundMessageTo As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_SMS_COMMUNICATION_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            cmdCommand.Parameters.Add("@TextMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@TextMessage").Value = TextMessage

            cmdCommand.Parameters.Add("@InboundMessageID", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageID").Value = InboundMessageID

            cmdCommand.Parameters.Add("@InboundMessageAccountID", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageAccountID").Value = InboundMessageAccountID

            cmdCommand.Parameters.Add("@InboundMessageTo", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageTo").Value = InboundMessageTo

            intResult = cmdCommand.ExecuteNonQuery

            InsertSMSCommunicationEntry = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertSMSAvailableOption(ByVal AutoLoanID As Integer _
                                  , ByVal MemberID As Integer _
                                  , ByVal MobileNumber As String _
                                  , ByVal Option1Amount As Double _
                                  , ByVal Option1Term As Integer _
                                  , ByVal Option1MonthlyPayment As Double _
                                  , ByVal Option2Amount As Double _
                                  , ByVal Option2Term As Integer _
                                  , ByVal Option2MonthlyPayment As Double _
                                  , ByVal Option3Amount As Double _
                                  , ByVal Option3Term As Integer _
                                  , ByVal Option3MonthlyPayment As Double _
                                  , ByVal Option3ShareToLoan As Double _
                                  , ByVal OldLoanAmount1 As Double _
                                  , ByVal OldLoanTerm1 As Integer _
                                  , ByVal OldLoanMonthlyPayment1 As Double _
                                  , ByVal OldLoanAmount2 As Double _
                                  , ByVal OldLoanTerm2 As Integer _
                                  , ByVal OldLoanMonthlyPayment2 As Double _
                                  , ByVal OldLoanAmount3 As Double _
                                  , ByVal OldLoanTerm3 As Integer _
                                  , ByVal OldLoanMonthlyPayment3 As Double) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_SMS_AVAILABLE_OPTION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            cmdCommand.Parameters.Add("@Option1Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Option1Amount").Value = Option1Amount

            cmdCommand.Parameters.Add("@Option1Term", SqlDbType.Int)
            cmdCommand.Parameters("@Option1Term").Value = Option1Term

            cmdCommand.Parameters.Add("@Option1MonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@Option1MonthlyPayment").Value = Option1MonthlyPayment

            cmdCommand.Parameters.Add("@Option2Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Option2Amount").Value = Option2Amount

            cmdCommand.Parameters.Add("@Option2Term", SqlDbType.Int)
            cmdCommand.Parameters("@Option2Term").Value = Option2Term

            cmdCommand.Parameters.Add("@Option2MonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@Option2MonthlyPayment").Value = Option2MonthlyPayment

            cmdCommand.Parameters.Add("@Option3Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Option3Amount").Value = Option3Amount

            cmdCommand.Parameters.Add("@Option3Term", SqlDbType.Int)
            cmdCommand.Parameters("@Option3Term").Value = Option3Term

            cmdCommand.Parameters.Add("@Option3MonthlyPayment", SqlDbType.Money)
            cmdCommand.Parameters("@Option3MonthlyPayment").Value = Option3MonthlyPayment

            cmdCommand.Parameters.Add("@Option3ShareToLoan", SqlDbType.Money)
            cmdCommand.Parameters("@Option3ShareToLoan").Value = Option3ShareToLoan

            cmdCommand.Parameters.Add("@OldLoanAmount1", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanAmount1").Value = OldLoanAmount1

            cmdCommand.Parameters.Add("@OldLoanTerm1", SqlDbType.Int)
            cmdCommand.Parameters("@OldLoanTerm1").Value = OldLoanTerm1

            cmdCommand.Parameters.Add("@OldLoanMonthlyPayment1", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanMonthlyPayment1").Value = OldLoanMonthlyPayment1

            cmdCommand.Parameters.Add("@OldLoanAmount2", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanAmount2").Value = OldLoanAmount2

            cmdCommand.Parameters.Add("@OldLoanTerm2", SqlDbType.Int)
            cmdCommand.Parameters("@OldLoanTerm2").Value = OldLoanTerm2

            cmdCommand.Parameters.Add("@OldLoanMonthlyPayment2", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanMonthlyPayment2").Value = OldLoanMonthlyPayment2

            cmdCommand.Parameters.Add("@OldLoanAmount3", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanAmount3").Value = OldLoanAmount3

            cmdCommand.Parameters.Add("@OldLoanTerm3", SqlDbType.Int)
            cmdCommand.Parameters("@OldLoanTerm3").Value = OldLoanTerm3

            cmdCommand.Parameters.Add("@OldLoanMonthlyPayment3", SqlDbType.Money)
            cmdCommand.Parameters("@OldLoanMonthlyPayment3").Value = OldLoanMonthlyPayment3

            intResult = cmdCommand.ExecuteNonQuery

            InsertSMSAvailableOption = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAutoLoanLastAvailableSMSOption(ByVal MobileNumber As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("AUTOLOAN_SELECT_LAST_SMS_AVAILABLE_OPTION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetAutoLoanLastAvailableSMSOption = dtLoanDetail

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAutoLoanSMSCommunicationHistory(ByVal AutoLoanID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If
            cmdCommand = New SqlCommand("AUTOLOAN_SELECT_SMS_COMMUNICATION_HISTORY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = AutoLoanID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetAutoLoanSMSCommunicationHistory = dtLoanDetail
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertEsendexSMSCommunicationEntry(ByVal MemberID As Integer _
                                  , ByVal MobileNumber As String _
                                  , ByVal TextMessage As String _
                                  , ByVal InboundMessageID As String _
                                  , ByVal InboundMessageAccountID As String _
                                  , ByVal InboundMessageTo As String _
                                  , ByVal ResponseMessage As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_ESENDEX_SMS_COMMUNICATION_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            cmdCommand.Parameters.Add("@TextMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@TextMessage").Value = TextMessage

            cmdCommand.Parameters.Add("@InboundMessageID", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageID").Value = InboundMessageID

            cmdCommand.Parameters.Add("@InboundMessageAccountID", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageAccountID").Value = InboundMessageAccountID

            cmdCommand.Parameters.Add("@InboundMessageTo", SqlDbType.VarChar)
            cmdCommand.Parameters("@InboundMessageTo").Value = InboundMessageTo

            cmdCommand.Parameters.Add("@ResponseMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ResponseMessage").Value = ResponseMessage

            intResult = cmdCommand.ExecuteNonQuery

            InsertEsendexSMSCommunicationEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetEsendexSMSCommunicationHistory(ByVal MemberID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If
            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_ESENDEX_SMS_COMMUNICATION_HISTORY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetEsendexSMSCommunicationHistory = dtLoanDetail
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Mobile App Access"
    Public Function UpdateAutoLoanApplicationOptionSelectedForMobile(ByVal ID As Integer _
                                         , ByVal OptionSelected As Integer _
                                         , ByVal AppealReason As String _
                                         , ByVal UserID As Integer _
                                         ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_UPDATE_APPLICATION_OPTION_SELECTED_FOR_MOBILE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AutoLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@AutoLoanID").Value = ID

            cmdCommand.Parameters.Add("@OptionSelected", SqlDbType.Int)
            cmdCommand.Parameters("@OptionSelected").Value = OptionSelected

            cmdCommand.Parameters.Add("@AppealReason", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppealReason").Value = AppealReason

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateAutoLoanApplicationOptionSelectedForMobile = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region
End Class
