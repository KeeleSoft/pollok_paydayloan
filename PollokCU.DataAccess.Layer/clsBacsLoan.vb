﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsAutoLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls Bacs Loan Related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 28/04/2012 : Created
''' </history>
'''********************************************************************
Public Class clsBacsLoan
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Parameters"
    Public Function GetBacsLoanParameters() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PARAMETERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Insert Update Auto Loan"
    Public Function InsertUpdateApplication(ByRef BacsLoanID As Integer _
                                       , ByVal UserID As Integer _
                                       , Optional ByVal MemberID As Integer = 0 _
                                       , Optional ByVal ProductCode As String = "" _
                                       , Optional ByVal ApplicationStatus As Integer = 0 _
                                       , Optional ByVal NewLoanAmount As Double = 0 _
                                       , Optional ByVal NewLoanRepayment As Double = 0 _
                                       , Optional ByVal NewTerm As Integer = 0 _
                                       , Optional ByVal FinalTotalLoanAmount As Double = 0 _
                                       , Optional ByVal FinalRepaymentAmount As Double = 0 _
                                       , Optional ByVal FinalTerm As Integer = 0 _
                                       , Optional ByVal TotalRisk As Double = 0 _
                                       , Optional ByVal PaymentMethod As Integer = 0 _
                                       , Optional ByVal AccountNumber As String = "" _
                                       , Optional ByVal SortCode As String = "" _
                                       , Optional ByVal ApprovedByStaffID As Integer = 0 _
                                       , Optional ByVal ApprovedByStaffID2 As Integer = 0 _
                                       , Optional ByVal TotalDeduction As Double = 0 _
                                       , Optional ByVal RepaymentMethod As Integer = 0 _
                                      ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_INSERT_UPDATE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            cmdCommand.Parameters.Add("@NewLoanAmount ", SqlDbType.Money)
            cmdCommand.Parameters("@NewLoanAmount ").Value = NewLoanAmount

            cmdCommand.Parameters.Add("@NewLoanRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@NewLoanRepayment").Value = NewLoanRepayment

            cmdCommand.Parameters.Add("@NewTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NewTerm").Value = NewTerm

            cmdCommand.Parameters.Add("@FinalTotalLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalTotalLoanAmount").Value = FinalTotalLoanAmount

            cmdCommand.Parameters.Add("@FinalRepaymentAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalRepaymentAmount").Value = FinalRepaymentAmount

            cmdCommand.Parameters.Add("@FinalTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalTerm").Value = FinalTerm

            cmdCommand.Parameters.Add("@TotalRisk", SqlDbType.Money)
            cmdCommand.Parameters("@TotalRisk").Value = TotalRisk

            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@SortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SortCode").Value = SortCode

            cmdCommand.Parameters.Add("@TotalDeduction", SqlDbType.Money)
            cmdCommand.Parameters("@TotalDeduction").Value = TotalDeduction

            cmdCommand.Parameters.Add("@RepaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@RepaymentMethod").Value = RepaymentMethod

            cmdCommand.Parameters.Add("@ApprovedByStaffID", SqlDbType.Int)
            cmdCommand.Parameters("@ApprovedByStaffID").Value = ApprovedByStaffID

            If ApprovedByStaffID2 > 0 Then
                cmdCommand.Parameters.Add("@ApprovedByStaffID2", SqlDbType.Int)
                cmdCommand.Parameters("@ApprovedByStaffID2").Value = ApprovedByStaffID2
            End If

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            If BacsLoanID > 0 Then
                cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@BacsLoanID").Direction = ParameterDirection.Output
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If BacsLoanID <= 0 Then
                BacsLoanID = CInt(cmdCommand.Parameters("@BacsLoanID").Value)
            End If

            InsertUpdateApplication = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertUpdateRescheduleLoan(ByVal MemberID As Integer, ByVal UserID As Integer _
                                       , ByVal ProductCode As String _
                                       , ByVal NewRepayment As Double _
                                       , ByVal NewTerm As Integer _
                                      ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_INSERT_UPDATE_RESCHEDULE_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            cmdCommand.Parameters.Add("@NewRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@NewRepayment").Value = NewRepayment

            cmdCommand.Parameters.Add("@NewTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NewTerm").Value = NewTerm

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertUpdateRescheduleLoan = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanReportName(ByVal BacsLoanID As Integer, ByVal ReportType As Integer, ByVal FileName As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_APPLICATION_REPORT_NAME", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters          
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@ReportType", SqlDbType.Int)
            cmdCommand.Parameters("@ReportType").Value = ReportType

            cmdCommand.Parameters.Add("@FileName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FileName").Value = FileName

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function DeleteBacsLoanApplication(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_DELETE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            DeleteBacsLoanApplication = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Statuses"
    Public Function GetAllBacsLoanApplicationStatuses() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_APPLICATION_STATUSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Staff - Loan Applications"
    Public Function GetAllBacsLoanApplications(Optional ByVal FirstName As String = "" _
                                          , Optional ByVal SurName As String = "" _
                                          , Optional ByVal ApplicationStatus As Integer = 0 _
                                          , Optional ByVal MemberID As Integer = 0) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_APPLICATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If Not FirstName Is Nothing AndAlso FirstName.Length > 0 Then
                cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
                cmdCommand.Parameters("@FirstName").Value = FirstName
            End If

            If Not SurName Is Nothing AndAlso SurName.Length > 0 Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            If ApplicationStatus > 0 Then
                cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
                cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus
            End If

            If MemberID > 0 Then
                cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
                cmdCommand.Parameters("@MemberID").Value = MemberID
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetCurrentBalances(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_CURRENT_BALANCES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanAuthorise(ByVal ID As Integer _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_APPLICATION_AUTHORISE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateBacsLoanAuthorise = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanAgreementRecieved(ByVal ID As Integer _
                                             , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_APPLICATION_AGREEMENT_RECIEVED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateBacsLoanAgreementRecieved = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingLoanFileEntries(ByVal PaymentMethod As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_LOAN_FILE_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingCUCABacsFileEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_CUCA_BACS_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingExternalBacsFileEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_EXTERNAL_BACS_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get he list to send smss for all bacs loan cuca bacs
    ''' </summary>
    Public Function SelectCUCABACSFileBatchSMSSendList(ByVal fileID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_CUCA_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectCUCABACSFileBatchSMSSendList = dtTXNs

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function


    Public Function SelectExtBACSFileBatchSMSSendList(ByVal fileID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_EXTERNAL_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectExtBACSFileBatchSMSSendList = dtTXNs

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingLoanFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_LOAN_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingLoanFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingCUCABacsFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_CUCA_BACS_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingCUCABacsFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingExternalBacsFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_EXT_BACS_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingExternalBacsFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanPendingLoanEntriesToProcessed(ByVal UserID As Integer, ByVal PaymentMethod As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_PENDING_LOAN_ENTRIES_TO_PROCESSED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan approver staff list
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	BacsLoan approver datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanApproverStaffList() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_APPROVER_STAFF_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanApproverStaffList = dtLoanDetail
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function InsertBacsLoanNote(ByVal BacsLoanID As Integer _
                                     , ByVal Note As String _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertBacsLoanNote = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanNotes(ByVal BacsLoanID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan details for a given id
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	BacsLoan Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  28/05/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanDetail(ByVal BacsLoanID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("BACSLOAN_SELECT_APPLICATION_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanDetail = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan initial application data
    ''' </summary>
    ''' <history>
    '''     [Shan HM]  29/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanInitialMemberData(ByVal MemberID As Integer, ByVal ProductCode As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_INITIAL_APPLICATION_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanInitialMemberData = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Products & Additional accounts"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all available products
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Shan HM]  29/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanProducts() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("BACSLOAN_SELECT_PRODUCTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanProducts = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAdditionalMemberAccounts(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_ADDITIONAL_MEMBER_ACCOUNTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Bacs Loan Duplicate Check"
    Public Function GeBacsLoanDuplicateCheck(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_LOAN_DUPLICATED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
End Class
