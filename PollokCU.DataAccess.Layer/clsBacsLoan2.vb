﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsAutoLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls Bacs Loan Related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 28/04/2012 : Created
''' </history>
'''********************************************************************
Public Class clsBacsLoan2
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Parameters"
    Public Function GetBacsLoanParameters() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_PARAMETERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Insert Update Auto Loan"
    Public Function InsertUpdateApplication(ByRef BacsLoanID As Integer _
                                       , ByVal UserID As Integer _
                                       , Optional ByVal MemberID As Integer = 0 _
                                       , Optional ByVal AmountApplied As Double = 0 _
                                       , Optional ByVal ProductCode As String = "" _
                                       , Optional ByVal ApplicationStatus As Integer = 0 _
                                       , Optional ByVal ApplicationReceivedDate As DateTime = Nothing _
                                       , Optional ByVal ApplicationDueDate As DateTime = Nothing _
                                       , Optional ByVal AmountApproved As Double = 0 _
                                       , Optional ByVal AmountOffered As Double = 0 _
                                       , Optional ByVal AmountShareToLoan As Double = 0 _
                                       , Optional ByVal NewLoanAmount As Double = 0 _
                                       , Optional ByVal NewLoanRepayment As Double = 0 _
                                       , Optional ByVal NewTerm As Integer = 0 _
                                       , Optional ByVal FinalTotalLoanAmount As Double = 0 _
                                       , Optional ByVal FinalRepaymentAmount As Double = 0 _
                                       , Optional ByVal FinalTerm As Integer = 0 _
                                       , Optional ByVal TotalRisk As Double = 0 _
                                       , Optional ByVal PaymentMethod As Integer = 0 _
                                       , Optional ByVal AccountNumber As String = "" _
                                       , Optional ByVal SortCode As String = "" _
                                       , Optional ByVal ApprovedByStaffID As Integer = 0 _
                                       , Optional ByVal ApprovedByStaffID2 As Integer = 0 _
                                       , Optional ByVal TotalDeduction As Double = 0 _
                                       , Optional ByVal RepaymentMethod As Integer = 0 _
                                       , Optional ByVal AllowCreditCheck As Boolean = False _
                                       , Optional ByVal Stage3Completed As Boolean = False _
                                      ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_INSERT_UPDATE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@AmountApplied ", SqlDbType.Money)
            cmdCommand.Parameters("@AmountApplied ").Value = AmountApplied

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            If ApplicationReceivedDate > Date.MinValue Then
                cmdCommand.Parameters.Add("@ApplicationReceivedDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@ApplicationReceivedDate").Value = ApplicationReceivedDate
            End If

            If ApplicationDueDate > Date.MinValue Then
                cmdCommand.Parameters.Add("@ApplicationDueDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@ApplicationDueDate").Value = ApplicationDueDate
            End If

            cmdCommand.Parameters.Add("@AmountApproved ", SqlDbType.Money)
            cmdCommand.Parameters("@AmountApproved ").Value = AmountApproved

            cmdCommand.Parameters.Add("@AmountOffered ", SqlDbType.Money)
            cmdCommand.Parameters("@AmountOffered ").Value = AmountOffered

            cmdCommand.Parameters.Add("@AmountShareToLoan ", SqlDbType.Money)
            cmdCommand.Parameters("@AmountShareToLoan ").Value = AmountShareToLoan

            cmdCommand.Parameters.Add("@NewLoanAmount ", SqlDbType.Money)
            cmdCommand.Parameters("@NewLoanAmount ").Value = NewLoanAmount

            cmdCommand.Parameters.Add("@NewLoanRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@NewLoanRepayment").Value = NewLoanRepayment

            cmdCommand.Parameters.Add("@NewTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NewTerm").Value = NewTerm

            cmdCommand.Parameters.Add("@FinalTotalLoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalTotalLoanAmount").Value = FinalTotalLoanAmount

            cmdCommand.Parameters.Add("@FinalRepaymentAmount", SqlDbType.Money)
            cmdCommand.Parameters("@FinalRepaymentAmount").Value = FinalRepaymentAmount

            cmdCommand.Parameters.Add("@FinalTerm", SqlDbType.Int)
            cmdCommand.Parameters("@FinalTerm").Value = FinalTerm

            cmdCommand.Parameters.Add("@TotalRisk", SqlDbType.Money)
            cmdCommand.Parameters("@TotalRisk").Value = TotalRisk

            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@SortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SortCode").Value = SortCode

            cmdCommand.Parameters.Add("@TotalDeduction", SqlDbType.Money)
            cmdCommand.Parameters("@TotalDeduction").Value = TotalDeduction

            cmdCommand.Parameters.Add("@RepaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@RepaymentMethod").Value = RepaymentMethod

            cmdCommand.Parameters.Add("@ApprovedByStaffID", SqlDbType.Int)
            cmdCommand.Parameters("@ApprovedByStaffID").Value = ApprovedByStaffID

            If ApprovedByStaffID2 > 0 Then
                cmdCommand.Parameters.Add("@ApprovedByStaffID2", SqlDbType.Int)
                cmdCommand.Parameters("@ApprovedByStaffID2").Value = ApprovedByStaffID2
            End If

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@AllowCreditCheck", SqlDbType.Bit)
            cmdCommand.Parameters("@AllowCreditCheck").Value = AllowCreditCheck

            cmdCommand.Parameters.Add("@Stage3Completed", SqlDbType.Bit)
            cmdCommand.Parameters("@Stage3Completed").Value = Stage3Completed

            If BacsLoanID > 0 Then
                cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
                cmdCommand.Parameters("@BacsLoanID").Direction = ParameterDirection.Output
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If BacsLoanID <= 0 Then
                BacsLoanID = CInt(cmdCommand.Parameters("@BacsLoanID").Value)
            End If

            InsertUpdateApplication = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateApplicationStage2(ByVal BacsLoanID As Integer _
                                           , ByVal AmountApproved As Double _
                                           , ByVal AmountOffered As Double _
                                           , ByVal AmountShareToLoan As Double _
                                           , ByVal ApprovedByStaffID As Integer _
                                           , ByVal Rejected As Boolean _
                                           , ByVal ApplicationStatus As Integer _
                                           , ByVal PaymentMethod As Integer _
                                           , ByVal ProductCode As String _
                                           , ByVal ApprovedByStaffID2 As Integer _
                                           ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_STAGE2", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@AmountApproved ", SqlDbType.Money)
            cmdCommand.Parameters("@AmountApproved ").Value = AmountApproved

            cmdCommand.Parameters.Add("@AmountOffered", SqlDbType.Money)
            cmdCommand.Parameters("@AmountOffered").Value = AmountOffered

            cmdCommand.Parameters.Add("@AmountShareToLoan", SqlDbType.Money)
            cmdCommand.Parameters("@AmountShareToLoan").Value = AmountShareToLoan

            cmdCommand.Parameters.Add("@ApprovedByStaffID", SqlDbType.Int)
            cmdCommand.Parameters("@ApprovedByStaffID").Value = ApprovedByStaffID

            cmdCommand.Parameters.Add("@Rejected", SqlDbType.Bit)
            cmdCommand.Parameters("@Rejected").Value = Rejected

            cmdCommand.Parameters.Add("@ApplicationStatus ", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus ").Value = ApplicationStatus

            cmdCommand.Parameters.Add("@PaymentMethod ", SqlDbType.Int)
            cmdCommand.Parameters("@PaymentMethod ").Value = PaymentMethod

            cmdCommand.Parameters.Add("@ProductCode ", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode ").Value = ProductCode

            cmdCommand.Parameters.Add("@ApprovedByStaffID2 ", SqlDbType.Int)
            cmdCommand.Parameters("@ApprovedByStaffID2 ").Value = ApprovedByStaffID2

            intResult = cmdCommand.ExecuteNonQuery


            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertUpdateRescheduleLoan(ByVal MemberID As Integer, ByVal UserID As Integer _
                                       , ByVal ProductCode As String _
                                       , ByVal NewRepayment As Double _
                                       , ByVal NewTerm As Integer _
                                      ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_INSERT_UPDATE_RESCHEDULE_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            cmdCommand.Parameters.Add("@NewRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@NewRepayment").Value = NewRepayment

            cmdCommand.Parameters.Add("@NewTerm", SqlDbType.Int)
            cmdCommand.Parameters("@NewTerm").Value = NewTerm

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertUpdateRescheduleLoan = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoan2ReportName(ByVal BacsLoanID As Integer, ByVal ReportType As Integer, ByVal FileName As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_REPORT_NAME", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters          
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@ReportType", SqlDbType.Int)
            cmdCommand.Parameters("@ReportType").Value = ReportType

            cmdCommand.Parameters.Add("@FileName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FileName").Value = FileName

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function DeleteBacsLoanApplication2(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_DELETE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            DeleteBacsLoanApplication2 = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateApplicationCreditCheckData(ByVal BacsLoanID As Integer _
                                               , ByVal MemberTitle As String _
                                               , ByVal CurrentAddressLine1 As String _
                                               , ByVal CurrentCity As String _
                                               , ByVal CurrentCounty As String _
                                               , ByVal CurrentPostCode As String _
                                               , ByVal PreviousAddressLine1 As String _
                                               , ByVal PreviousCity As String _
                                               , ByVal PreviousCounty As String _
                                               , ByVal PreviousPostCode As String _
                                               ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_CREDITCHECK_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@MemberTitle ", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberTitle ").Value = MemberTitle

            cmdCommand.Parameters.Add("@CurrentAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentAddressLine1").Value = CurrentAddressLine1

            cmdCommand.Parameters.Add("@CurrentCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCity").Value = CurrentCity

            cmdCommand.Parameters.Add("@CurrentCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCounty").Value = CurrentCounty

            cmdCommand.Parameters.Add("@CurrentPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentPostCode").Value = CurrentPostCode

            cmdCommand.Parameters.Add("@PreviousAddressLine1 ", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine1 ").Value = PreviousAddressLine1

            cmdCommand.Parameters.Add("@PreviousCity ", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCity ").Value = PreviousCity

            cmdCommand.Parameters.Add("@PreviousCounty ", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCounty ").Value = PreviousCounty

            cmdCommand.Parameters.Add("@PreviousPostCode ", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousPostCode ").Value = PreviousPostCode

            intResult = cmdCommand.ExecuteNonQuery


            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub UpdateBacsLoanApplication2Status(ByVal ID As Integer, ByVal Status As Integer)

        Dim cmdCommand As New SqlCommand
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            cmdCommand.Parameters.Add("@Status", SqlDbType.Int)
            cmdCommand.Parameters("@Status").Value = Status

            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub

#End Region

#Region "Loan Statuses"
    Public Function GetAllBacsLoanApplication2Statuses(ByVal stage As Int16) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_SELECT_APPLICATION_STATUSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@Stage", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Stage").Value = stage

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Staff - Loan Applications"
    Public Function GetAllBacsLoan2Applications(Optional ByVal FirstName As String = "" _
                                          , Optional ByVal SurName As String = "" _
                                          , Optional ByVal ApplicationStatus As Integer = 0 _
                                          , Optional ByVal MemberID As Integer = 0) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_SELECT_APPLICATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If Not FirstName Is Nothing AndAlso FirstName.Length > 0 Then
                cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
                cmdCommand.Parameters("@FirstName").Value = FirstName
            End If

            If Not SurName Is Nothing AndAlso SurName.Length > 0 Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            If ApplicationStatus > 0 Then
                cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
                cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus
            End If

            If MemberID > 0 Then
                cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
                cmdCommand.Parameters("@MemberID").Value = MemberID
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetCurrentBalances(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_CURRENT_BALANCES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoan2Authorise(ByVal ID As Integer _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_AUTHORISE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateBacsLoan2Authorise = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanAgreementRecieved(ByVal ID As Integer _
                                             , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_UPDATE_APPLICATION_AGREEMENT_RECIEVED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateBacsLoanAgreementRecieved = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingLoanFileEntries(ByVal PaymentMethod As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_LOAN_FILE_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingCUCABacsFileEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_CUCA_BACS_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanPendingExternalBacsFileEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_PENDING_EXTERNAL_BACS_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get he list to send smss for all bacs loan cuca bacs
    ''' </summary>
    Public Function SelectCUCABACSFileBatchSMSSendList(ByVal fileID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_CUCA_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectCUCABACSFileBatchSMSSendList = dtTXNs

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function


    Public Function SelectExtBACSFileBatchSMSSendList(ByVal fileID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_EXTERNAL_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectExtBACSFileBatchSMSSendList = dtTXNs

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingLoanFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_LOAN_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingLoanFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingCUCABacsFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_CUCA_BACS_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingCUCABacsFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function RemovePendingExternalBacsFileEntry(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_REMOVE_PENDING_EXT_BACS_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemovePendingExternalBacsFileEntry = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBacsLoanPendingLoanEntriesToProcessed(ByVal UserID As Integer, ByVal PaymentMethod As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_UPDATE_PENDING_LOAN_ENTRIES_TO_PROCESSED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@PaymentMethod", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PaymentMethod").Value = PaymentMethod

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan approver staff list
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	BacsLoan approver datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanApproverStaffList() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_APPROVER_STAFF_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanApproverStaffList = dtLoanDetail
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function InsertBacsLoanNote(ByVal BacsLoanID As Integer _
                                     , ByVal Note As String _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertBacsLoanNote = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetBacsLoanNotes(ByVal BacsLoanID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan details for a given id
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	BacsLoan Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  28/05/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoan2Detail(ByVal BacsLoanID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("BACSLOAN2_SELECT_APPLICATION_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BacsLoanID", SqlDbType.Int)
            cmdCommand.Parameters("@BacsLoanID").Value = BacsLoanID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoan2Detail = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select bacsloan initial application data
    ''' </summary>
    ''' <history>
    '''     [Shan HM]  29/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanInitialMemberData(ByVal MemberID As Integer, ByVal ProductCode As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_INITIAL_APPLICATION_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = ProductCode

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanInitialMemberData = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Products & Additional accounts"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all available products
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Shan HM]  29/04/2013  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function GetBacsLoanProducts() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("BACSLOAN_SELECT_PRODUCTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            GetBacsLoanProducts = dtLoanDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetAdditionalMemberAccounts(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN_SELECT_ADDITIONAL_MEMBER_ACCOUNTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Bacs Loan Duplicate Check"
    Public Function GeBacsLoanDuplicateCheck(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.BACSLOAN2_SELECT_LOAN_DUPLICATED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
End Class
