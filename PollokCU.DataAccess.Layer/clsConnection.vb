﻿'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsConnection.vb
'''********************************************************************
''' <Summary>
'''	Connection related methods used by data access layer classses
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 03/09/2009 : 520.Baseline Created
'''     [Isuru] 17/08/2016 : Updated to PollokCU.DataAccess.Layer
''' </history>
'''********************************************************************
Public Class clsConnection
    Dim objConfig As New System.Configuration.AppSettingsReader
    Public Function getConnectionString() As String
        getConnectionString = CType(objConfig.GetValue("SCU.Connection.Key", GetType(System.String)), System.String)
    End Function

    Public Function getConfigKey(ByVal sKey As String) As String
        getConfigKey = CType(objConfig.GetValue(sKey, GetType(System.String)), System.String)
    End Function
End Class
