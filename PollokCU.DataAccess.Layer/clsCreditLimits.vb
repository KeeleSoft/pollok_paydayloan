﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsCreditLimits.vb
'''********************************************************************
''' <Summary>
'''	Credit limits related
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 14/01/2013 : Created
''' </history>
'''********************************************************************
Public Class clsCreditLimits
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

    ''' <summary>
    ''' Get credit limit list for members. Can search as well.
    ''' </summary>
    Public Function GetCreditLimitMemberList(ByVal blankRevLoan As Boolean, _
                                             ByVal blankPDLLoan As Boolean, _
                                             ByVal blankODLoan As Boolean, _
                                             ByVal firstName As String, _
                                             ByVal surname As String, _
                                             ByVal memberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDILIMIT_SELECT_MEMBER_LIMIT_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@BlankRevLoan", SqlDbType.Bit)
            cmdCommand.Parameters("@BlankRevLoan").Value = blankRevLoan

            cmdCommand.Parameters.Add("@BlankPDLLoan", SqlDbType.Bit)
            cmdCommand.Parameters("@BlankPDLLoan").Value = blankPDLLoan

            cmdCommand.Parameters.Add("@BlankODLoan", SqlDbType.Bit)
            cmdCommand.Parameters("@BlankODLoan").Value = blankODLoan

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = firstName

            cmdCommand.Parameters.Add("@Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Surname").Value = surname

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get products and limits for a member
    ''' </summary>
    Public Function GetCreditLimitProductsByMember(ByVal memberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDILIMIT_SELECT_PRODUCT_LIMIT_BY_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get product change history
    ''' </summary>
    Public Function GetCreditLimitProductChangeHistory(ByVal limitID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDILIMIT_SELECT_PRODUCT_LIMIT_HISTORY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@LimitID", SqlDbType.Int)
            cmdCommand.Parameters("@LimitID").Value = limitID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get product change history
    ''' </summary>
    Public Function GetCreditLimitsForMemberAndProduct(ByVal memberID As Integer, ByVal creditLimitProductID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDILIMIT_SELECT_PRODUCT_LIMITS_BY_MEMBER_AND_PRODUCT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            cmdCommand.Parameters.Add("@CreditLimitProductID", SqlDbType.Int)
            cmdCommand.Parameters("@CreditLimitProductID").Value = creditLimitProductID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function InsertUpdateCreditLimitProduct(ByVal MemberID As Integer _
                                                     , ByVal CreditLimitProductID As Integer _
                                                     , ByVal ProductAmountLimit As Double _
                                                     , ByVal ProductCountLimit As Integer _
                                                     , ByVal Note As String _
                                                     , ByVal UserID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDITLIMIT_INSERT_UPDATE_PRODUCT_LIMIT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@CreditLimitProductID", SqlDbType.Int)
            cmdCommand.Parameters("@CreditLimitProductID").Value = CreditLimitProductID

            cmdCommand.Parameters.Add("@ProductAmountLimit", SqlDbType.Money)
            cmdCommand.Parameters("@ProductAmountLimit").Value = ProductAmountLimit

            cmdCommand.Parameters.Add("@ProductCountLimit", SqlDbType.Int)
            cmdCommand.Parameters("@ProductCountLimit").Value = ProductCountLimit

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get products and limits available for a member
    ''' </summary>
    Public Function GetCreditLimitProductsAvailableForMember(ByVal memberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.CREDILIMIT_SELECT_PRODUCTS_AVAILABLE_FOR_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
End Class
