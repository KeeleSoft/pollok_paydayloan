﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsELMS.vb
'''********************************************************************
''' <Summary>
'''	This module callsELMS Related SPs
''' </Summary>
''' <history>
'''     [Shan HM] 05/02/2013 : Created
''' </history>
'''********************************************************************
Public Class clsELMS
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

    Public Function GetAllELMSApplications(ByVal ApplicationStatus As Integer _
                                           , ByVal MemberID As Integer _
                                           , ByVal FirstName As String _
                                           , ByVal SurName As String _
                                          ) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_APPLICATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            If MemberID > 0 Then
                cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
                cmdCommand.Parameters("@MemberID").Value = MemberID
            End If

            If Not FirstName Is Nothing AndAlso FirstName.Length > 0 Then
                cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
                cmdCommand.Parameters("@FirstName").Value = FirstName
            End If

            If Not SurName Is Nothing AndAlso SurName.Length > 0 Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetLMSApplicationDetails(ByVal ELMSApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_APPLICATION_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetLMSApplicationStatuses() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_APPLICATION_STATUSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function InsertUpdateELMSApplication(ByRef ELMSApplicationID As Integer _
                                          , ByVal UserID As Integer _
                                          , Optional ByVal LambethID As String = "" _
                                          , Optional ByVal Title As String = "" _
                                          , Optional ByVal Firstname As String = "" _
                                          , Optional ByVal MiddleName As String = "" _
                                          , Optional ByVal Surname As String = "" _
                                          , Optional ByVal Gender As Char = "" _
                                          , Optional ByVal AddressLine1 As String = "" _
                                          , Optional ByVal AddressLine2 As String = "" _
                                          , Optional ByVal City As String = "" _
                                          , Optional ByVal PostCode As String = "" _
                                          , Optional ByVal CurrentAddressNoOfYears As Integer = 0 _
                                          , Optional ByVal CurrentAddressNoOfMonths As Integer = 0 _
                                          , Optional ByVal PreviousAddressLine1 As String = "" _
                                          , Optional ByVal PreviousAddressLine2 As String = "" _
                                          , Optional ByVal PreviousCity As String = "" _
                                          , Optional ByVal PreviousCounty As String = "" _
                                          , Optional ByVal PreviousPostCode As String = "" _
                                          , Optional ByVal ResidencyStatus As Integer = 0 _
                                          , Optional ByVal HaveMortgage As String = "" _
                                          , Optional ByVal HomeTelephone As String = "" _
                                          , Optional ByVal Mobile As String = "" _
                                          , Optional ByVal Email As String = "" _
                                          , Optional ByVal DateOfBirth As Date = Nothing _
                                          , Optional ByVal NINumber As String = "" _
                                          , Optional ByVal MaritalStatus As Integer = 0 _
                                          , Optional ByVal NoOfDependants As Integer = 0 _
                                          , Optional ByVal JobTitle As String = "" _
                                          , Optional ByVal EmploymentStatus As Integer = 0 _
                                          , Optional ByVal NatureOfBusiness As String = "" _
                                          , Optional ByVal WithEmployerYears As Integer = 0 _
                                          , Optional ByVal WithEmployerMonths As Integer = 0 _
                                          , Optional ByVal AnnualIncome As Double = 0 _
                                          , Optional ByVal WorkCompanyName As String = "" _
                                          , Optional ByVal WorkAddressLine1 As String = "" _
                                          , Optional ByVal WorkAddressLine2 As String = "" _
                                          , Optional ByVal WorkCity As String = "" _
                                          , Optional ByVal WorkPostCode As String = "" _
                                          , Optional ByVal WorkTelephone As String = "" _
                                          , Optional ByVal BeneficiaryTitle As String = "" _
                                          , Optional ByVal BeneficiaryFirstName As String = "" _
                                          , Optional ByVal BeneficiarySurname As String = "" _
                                          , Optional ByVal BeneficiaryAddressLine1 As String = "" _
                                          , Optional ByVal BeneficiaryAddressLine2 As String = "" _
                                          , Optional ByVal BeneficiaryCity As String = "" _
                                          , Optional ByVal BeneficiaryPostCode As String = "" _
                                          , Optional ByVal BeneficiaryRelationship As String = "" _
                                          , Optional ByVal CreditCardsHeld As String = "" _
                                          , Optional ByVal BankName As String = "" _
                                          , Optional ByVal AccountNumber As String = "" _
                                          , Optional ByVal SortCode As String = "") As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_INSERT_UPDATE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@LambethID", SqlDbType.VarChar)
            cmdCommand.Parameters("@LambethID").Value = LambethID

            cmdCommand.Parameters.Add("@Title", SqlDbType.VarChar)
            cmdCommand.Parameters("@Title").Value = Title

            cmdCommand.Parameters.Add("@Firstname ", SqlDbType.VarChar)
            cmdCommand.Parameters("@Firstname ").Value = Firstname

            cmdCommand.Parameters.Add("@MiddleName ", SqlDbType.VarChar)
            cmdCommand.Parameters("@MiddleName ").Value = MiddleName

            cmdCommand.Parameters.Add("@Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Surname").Value = Surname

            cmdCommand.Parameters.Add("@Gender", SqlDbType.Char)
            cmdCommand.Parameters("@Gender").Value = Gender

            cmdCommand.Parameters.Add("@AddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressLine1").Value = AddressLine1

            cmdCommand.Parameters.Add("@AddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressLine2").Value = AddressLine2

            cmdCommand.Parameters.Add("@City", SqlDbType.VarChar)
            cmdCommand.Parameters("@City").Value = City

            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = PostCode

            cmdCommand.Parameters.Add("@CurrentAddressNoOfYears", SqlDbType.Int)
            cmdCommand.Parameters("@CurrentAddressNoOfYears").Value = CurrentAddressNoOfYears

            cmdCommand.Parameters.Add("@CurrentAddressNoOfMonths", SqlDbType.Int)
            cmdCommand.Parameters("@CurrentAddressNoOfMonths").Value = CurrentAddressNoOfMonths

            cmdCommand.Parameters.Add("@PreviousAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine1").Value = PreviousAddressLine1

            cmdCommand.Parameters.Add("@PreviousAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine2").Value = PreviousAddressLine2

            cmdCommand.Parameters.Add("@PreviousCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCity").Value = PreviousCity

            cmdCommand.Parameters.Add("@PreviousCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCounty").Value = PreviousCounty

            cmdCommand.Parameters.Add("@PreviousPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousPostCode").Value = PreviousPostCode

            cmdCommand.Parameters.Add("@ResidencyStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ResidencyStatus").Value = ResidencyStatus

            cmdCommand.Parameters.Add("@HaveMortgage", SqlDbType.VarChar)
            cmdCommand.Parameters("@HaveMortgage").Value = HaveMortgage

            cmdCommand.Parameters.Add("@HomeTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@HomeTelephone").Value = HomeTelephone

            cmdCommand.Parameters.Add("@Mobile", SqlDbType.VarChar)
            cmdCommand.Parameters("@Mobile").Value = Mobile

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = Email

            cmdCommand.Parameters.Add("@DateOfBirth", SqlDbType.Date)
            cmdCommand.Parameters("@DateOfBirth").Value = IIf(DateOfBirth <> Date.MinValue, DateOfBirth, System.DBNull.Value)

            cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@NINumber").Value = NINumber

            cmdCommand.Parameters.Add("@MaritalStatus", SqlDbType.Int)
            cmdCommand.Parameters("@MaritalStatus").Value = MaritalStatus

            cmdCommand.Parameters.Add("@NoOfDependants", SqlDbType.Int)
            cmdCommand.Parameters("@NoOfDependants").Value = NoOfDependants

            cmdCommand.Parameters.Add("@JobTitle", SqlDbType.VarChar)
            cmdCommand.Parameters("@JobTitle").Value = JobTitle

            cmdCommand.Parameters.Add("@EmploymentStatus", SqlDbType.Int)
            cmdCommand.Parameters("@EmploymentStatus").Value = EmploymentStatus

            cmdCommand.Parameters.Add("@NatureOfBusiness", SqlDbType.VarChar)
            cmdCommand.Parameters("@NatureOfBusiness").Value = NatureOfBusiness

            cmdCommand.Parameters.Add("@WithEmployerYears", SqlDbType.Int)
            cmdCommand.Parameters("@WithEmployerYears").Value = WithEmployerYears

            cmdCommand.Parameters.Add("@WithEmployerMonths", SqlDbType.Int)
            cmdCommand.Parameters("@WithEmployerMonths").Value = WithEmployerMonths

            cmdCommand.Parameters.Add("@AnnualIncome", SqlDbType.Money)
            cmdCommand.Parameters("@AnnualIncome").Value = AnnualIncome

            cmdCommand.Parameters.Add("@WorkCompanyName", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCompanyName").Value = WorkCompanyName

            cmdCommand.Parameters.Add("@WorkAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine1").Value = WorkAddressLine1

            cmdCommand.Parameters.Add("@WorkAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine2").Value = WorkAddressLine2

            cmdCommand.Parameters.Add("@WorkCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCity").Value = WorkCity

            cmdCommand.Parameters.Add("@WorkPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkPostCode").Value = WorkPostCode

            cmdCommand.Parameters.Add("@WorkTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkTelephone").Value = WorkTelephone

            cmdCommand.Parameters.Add("@BeneficiaryTitle", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryTitle").Value = BeneficiaryTitle

            cmdCommand.Parameters.Add("@BeneficiaryFirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryFirstName").Value = BeneficiaryFirstName

            cmdCommand.Parameters.Add("@BeneficiarySurname", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiarySurname").Value = BeneficiarySurname

            cmdCommand.Parameters.Add("@BeneficiaryAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryAddressLine1").Value = BeneficiaryAddressLine1

            cmdCommand.Parameters.Add("@BeneficiaryAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryAddressLine2").Value = BeneficiaryAddressLine2

            cmdCommand.Parameters.Add("@BeneficiaryCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryCity").Value = BeneficiaryCity

            cmdCommand.Parameters.Add("@BeneficiaryPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryPostCode").Value = BeneficiaryPostCode

            cmdCommand.Parameters.Add("@BeneficiaryRelationship", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryRelationship").Value = BeneficiaryRelationship

            cmdCommand.Parameters.Add("@CreditCardsHeld", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreditCardsHeld").Value = CreditCardsHeld

            cmdCommand.Parameters.Add("@BankName", SqlDbType.VarChar)
            cmdCommand.Parameters("@BankName").Value = BankName

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@SortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SortCode").Value = SortCode

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            If ELMSApplicationID > 0 Then
                cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
                cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
                cmdCommand.Parameters("@ELMSApplicationID").Direction = ParameterDirection.Output
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If ELMSApplicationID <= 0 Then
                ELMSApplicationID = CInt(cmdCommand.Parameters("@ELMSApplicationID").Value)
            End If

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationStatus(ByVal ELMSApplicationID As Integer _
                                         , ByVal ApplicationStatus As Integer _
                                         , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSRemovePendingLoanEntry(ByVal ELMSApplicationID As Integer _
                                             , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_REMOVE_PENDING_LOAN_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSPendingLoanEntriesToProcessed(ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_PENDING_LOAN_ENTRIES_TO_PROCESSED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSLoanDetails(ByVal ELMSApplicationID As Integer _
                                         , ByVal LoanAmount As Double _
                                         , ByVal LoanPeriod As Integer _
                                         , ByVal LoanRepayment As Double _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_LOAN_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@LoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@LoanAmount").Value = LoanAmount

            cmdCommand.Parameters.Add("@LoanPeriod", SqlDbType.Int)
            cmdCommand.Parameters("@LoanPeriod").Value = LoanPeriod

            cmdCommand.Parameters.Add("@LoanRepayment", SqlDbType.Money)
            cmdCommand.Parameters("@LoanRepayment").Value = LoanRepayment

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationApprovals(ByVal ELMSApplicationID As Integer _
                                         , ByVal Approval1Or2 As Integer _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_APPROVALS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@Approval1Or2", SqlDbType.Int)
            cmdCommand.Parameters("@Approval1Or2").Value = Approval1Or2

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationMoveToELMSArea(ByVal ELMSApplicationID As Integer _
                                         , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_MOVE_TO_ELMS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationToComplete(ByVal ELMSApplicationID As Integer _
                                            , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_TO_COMPLETE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationMoveToMSRIssuing(ByVal ELMSApplicationID As Integer _
                                             , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_MOVE_TO_MSRISSUE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSApplicationEnableReCalculate(ByVal ELMSApplicationID As Integer _
                                                 , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_RE_ENABLE_RECALCULATE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetELMSPendingNewAccounts() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_PENDING_NEW_ACCOUNTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetELMSPendingLoanFileEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_PENDING_LOAN_FILE_ENTRIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function RemoveELMSPendingNewAccount(ByVal ELMSApplicationID As Integer _
                                             , ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_REMOVE_PENDING_NEW_ACCOUNT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSPendingNewAccountsToProcessed(ByVal StaffID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_PENDING_NEW_ACCOUNTS_TO_PROCESSED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters          
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = StaffID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateELMSReportName(ByVal ELMSApplicationID As Integer, ByVal ReportType As Integer, ByVal FileName As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_UPDATE_APPLICATION_REPORT_NAME", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters          
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@ReportType", SqlDbType.Int)
            cmdCommand.Parameters("@ReportType").Value = ReportType

            cmdCommand.Parameters.Add("@FileName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FileName").Value = FileName

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetELMSMemberListForVerify(ByVal NINumber As String _
                                           , ByVal AddressLine1 As String _
                                           , ByVal PostCode As String _
                                           , ByVal SurName As String _
                                           , ByVal DOB As Date _
                                          ) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_MEMBER_LIST_FOR_VERIFY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If Not String.IsNullOrEmpty(NINumber) Then
                cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
                cmdCommand.Parameters("@NINumber").Value = NINumber
            End If

            If Not String.IsNullOrEmpty(AddressLine1) Then
                cmdCommand.Parameters.Add("@AddressLine1", SqlDbType.VarChar)
                cmdCommand.Parameters("@AddressLine1").Value = AddressLine1
            End If

            If Not String.IsNullOrEmpty(PostCode) Then
                cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
                cmdCommand.Parameters("@PostCode").Value = PostCode
            End If

            If Not String.IsNullOrEmpty(SurName) Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            If DOB > DateTime.MinValue AndAlso DateDiff(DateInterval.Day, DOB, Now()) > 0 Then
                cmdCommand.Parameters.Add("@DOB", SqlDbType.Date)
                cmdCommand.Parameters("@DOB").Value = DOB
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetELMSMemberVerifyDetails(ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_MEMBER_DETAILS_TO_VERIFY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function InsertELMSNote(ByVal ELMSApplicationID As Integer _
                                     , ByVal Note As String _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetELMSNotes(ByVal ELMSApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.ELMS_SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ELMSApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ELMSApplicationID").Value = ELMSApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Parameters"
    Public Function GetELMSParameters() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SELECT_PARAMETERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
End Class
