﻿Imports System.Data.SqlClient
Imports System.Data

Public Class clsEquifax
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection
#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetAllConsumerDataSerachTables(ByVal ApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_CREDITSEARCH_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetCreditSearchElectroRollData(ByVal ApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_CREDITSEARCH_ELECTORAL_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetCreditSearchMatchAddressData(ByVal ApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_CREDITSEARCH_MATCHADDRESS_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetAMLData(ByVal ApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_AML_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetBankCheckData(ByVal ApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_BANKCHECK_DATA", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
End Class
