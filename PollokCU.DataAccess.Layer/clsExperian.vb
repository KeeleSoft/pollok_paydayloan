﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls experian related sps
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 12/12/2011 : Created
''' </history>
'''********************************************************************
Public Class clsExperian
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Authentication Plus"
    Public Sub InsertAuthenticationPlus(ByVal ApplicationID As Integer _
                                        , ByVal ExperianReference As String _
                                        , ByVal DecCode As String _
                                        , ByVal DecText As String _
                                        , ByVal AuthIndex As String _
                                        , ByVal AuthText As String _
                                        , ByVal IDConfLvl As String _
                                        , ByVal IDConfText As String _
                                        , ByVal HighRiskCount As Integer _
                                        , ByVal ErrorCode As String _
                                        , ByVal ErrorMessage As String _
                                        , ByVal ErrorSeverity As String _
                                        , ByVal IACA_PrimaryDataItems As String _
                                        , ByVal IACA_PrimaryDataSources As String _
                                        , ByVal IACA_StartDateOldestPrimaryItem As String _
                                        , ByVal IACA_SecondaryItems As String _
                                        , ByVal IACA_SecondarySources As String _
                                        , ByVal IACA_StartDateOldestSecondaryItem As String _
                                        , ByVal AOCA_PrimaryDataItems As String _
                                        , ByVal AOCA_PrimaryDataSources As String _
                                        , ByVal AOCA_StartDateOldestPrimaryItem As String _
                                        , ByVal AOCA_SecondaryItems As String _
                                        , ByVal AOCA_SecondarySources As String _
                                        , ByVal AOCA_StartDateOldestSecondaryItem As String _
                                        , ByVal IAPA_PrimaryDataItems As String _
                                        , ByVal IAPA_PrimaryDataSources As String _
                                        , ByVal IAPA_StartDateOldestPrimaryItem As String _
                                        , ByVal IAPA_SecondaryItems As String _
                                        , ByVal IAPA_SecondarySources As String _
                                        , ByVal IAPA_StartDateOldestSecondaryItem As String _
                                        , ByVal AOPA_PrimaryDataItems As String _
                                        , ByVal AOPA_PrimaryDataSources As String _
                                        , ByVal AOPA_StartDateOldestPrimaryItem As String _
                                        , ByVal AOPA_SecondaryItems As String _
                                        , ByVal AOPA_SecondarySources As String _
                                        , ByVal AOPA_StartDateOldestSecondaryItem As String _
                                        , ByVal DataMatch_NoAgePri As String _
                                        , ByVal DataMatch_NoAgeSec As String _
                                        , ByVal DataMatch_NoTACAPri As String _
                                        , ByVal DataMatch_NoTACASec As String)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_AUTHPLUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ExperianReference", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianReference").Value = ExperianReference

            cmdCommand.Parameters.Add("@DecCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@DecCode").Value = DecCode

            cmdCommand.Parameters.Add("@DecText", SqlDbType.VarChar)
            cmdCommand.Parameters("@DecText").Value = DecText

            cmdCommand.Parameters.Add("@AuthIndex", SqlDbType.VarChar)
            cmdCommand.Parameters("@AuthIndex").Value = AuthIndex

            cmdCommand.Parameters.Add("@AuthText", SqlDbType.VarChar)
            cmdCommand.Parameters("@AuthText").Value = AuthText

            cmdCommand.Parameters.Add("@IDConfLvl", SqlDbType.VarChar)
            cmdCommand.Parameters("@IDConfLvl").Value = IDConfLvl

            cmdCommand.Parameters.Add("@IDConfText", SqlDbType.VarChar)
            cmdCommand.Parameters("@IDConfText").Value = IDConfText

            cmdCommand.Parameters.Add("@HighRiskCount", SqlDbType.Int)
            cmdCommand.Parameters("@HighRiskCount").Value = HighRiskCount

            cmdCommand.Parameters.Add("@ErrorCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorCode").Value = ErrorCode

            cmdCommand.Parameters.Add("@ErrorMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorMessage").Value = ErrorMessage

            cmdCommand.Parameters.Add("@ErrorSeverity", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorSeverity").Value = ErrorSeverity

            cmdCommand.Parameters.Add("@IACA_PrimaryDataItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_PrimaryDataItems").Value = IACA_PrimaryDataItems

            cmdCommand.Parameters.Add("@IACA_PrimaryDataSources", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_PrimaryDataSources").Value = IACA_PrimaryDataSources

            cmdCommand.Parameters.Add("@IACA_StartDateOldestPrimaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_StartDateOldestPrimaryItem").Value = IACA_StartDateOldestPrimaryItem

            cmdCommand.Parameters.Add("@IACA_SecondaryItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_SecondaryItems").Value = IACA_SecondaryItems

            cmdCommand.Parameters.Add("@IACA_SecondarySources", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_SecondarySources").Value = IACA_SecondarySources

            cmdCommand.Parameters.Add("@IACA_StartDateOldestSecondaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@IACA_StartDateOldestSecondaryItem").Value = IACA_StartDateOldestSecondaryItem

            'AOCA
            cmdCommand.Parameters.Add("@AOCA_PrimaryDataItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_PrimaryDataItems").Value = AOCA_PrimaryDataItems

            cmdCommand.Parameters.Add("@AOCA_PrimaryDataSources", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_PrimaryDataSources").Value = AOCA_PrimaryDataSources

            cmdCommand.Parameters.Add("@AOCA_StartDateOldestPrimaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_StartDateOldestPrimaryItem").Value = AOCA_StartDateOldestPrimaryItem

            cmdCommand.Parameters.Add("@AOCA_SecondaryItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_SecondaryItems").Value = AOCA_SecondaryItems

            cmdCommand.Parameters.Add("@AOCA_SecondarySources", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_SecondarySources").Value = AOCA_SecondarySources

            cmdCommand.Parameters.Add("@AOCA_StartDateOldestSecondaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOCA_StartDateOldestSecondaryItem").Value = AOCA_StartDateOldestSecondaryItem

            'IAPA
            cmdCommand.Parameters.Add("@IAPA_PrimaryDataItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_PrimaryDataItems").Value = IAPA_PrimaryDataItems

            cmdCommand.Parameters.Add("@IAPA_PrimaryDataSources", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_PrimaryDataSources").Value = IAPA_PrimaryDataSources

            cmdCommand.Parameters.Add("@IAPA_StartDateOldestPrimaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_StartDateOldestPrimaryItem").Value = IAPA_StartDateOldestPrimaryItem

            cmdCommand.Parameters.Add("@IAPA_SecondaryItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_SecondaryItems").Value = IAPA_SecondaryItems

            cmdCommand.Parameters.Add("@IAPA_SecondarySources", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_SecondarySources").Value = IAPA_SecondarySources

            cmdCommand.Parameters.Add("@IAPA_StartDateOldestSecondaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@IAPA_StartDateOldestSecondaryItem").Value = IAPA_StartDateOldestSecondaryItem

            'AOPA
            cmdCommand.Parameters.Add("@AOPA_PrimaryDataItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_PrimaryDataItems").Value = AOPA_PrimaryDataItems

            cmdCommand.Parameters.Add("@AOPA_PrimaryDataSources", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_PrimaryDataSources").Value = AOPA_PrimaryDataSources

            cmdCommand.Parameters.Add("@AOPA_StartDateOldestPrimaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_StartDateOldestPrimaryItem").Value = AOPA_StartDateOldestPrimaryItem

            cmdCommand.Parameters.Add("@AOPA_SecondaryItems", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_SecondaryItems").Value = AOPA_SecondaryItems

            cmdCommand.Parameters.Add("@AOPA_SecondarySources", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_SecondarySources").Value = AOPA_SecondarySources

            cmdCommand.Parameters.Add("@AOPA_StartDateOldestSecondaryItem", SqlDbType.VarChar)
            cmdCommand.Parameters("@AOPA_StartDateOldestSecondaryItem").Value = AOPA_StartDateOldestSecondaryItem

            'Data Match
            cmdCommand.Parameters.Add("@DataMatch_NoAgePri", SqlDbType.VarChar)
            cmdCommand.Parameters("@DataMatch_NoAgePri").Value = DataMatch_NoAgePri

            cmdCommand.Parameters.Add("@DataMatch_NoAgeSec", SqlDbType.VarChar)
            cmdCommand.Parameters("@DataMatch_NoAgeSec").Value = DataMatch_NoAgeSec

            cmdCommand.Parameters.Add("@DataMatch_NoTACAPri", SqlDbType.VarChar)
            cmdCommand.Parameters("@DataMatch_NoTACAPri").Value = DataMatch_NoTACAPri

            cmdCommand.Parameters.Add("@DataMatch_NoTACASec", SqlDbType.VarChar)
            cmdCommand.Parameters("@DataMatch_NoTACASec").Value = DataMatch_NoTACASec

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Function GetAuthenticationPlusData(ByVal ApplicationID As Integer) As DataSet
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataSet = New DataSet

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_SELECT_AUTHPLUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Authentication Plus High Risk"
    Public Sub InsertAuthenticationPlusHighRisk(ByVal ApplicationID As Integer _
                                        , ByVal HighRiskRule As String _
                                        , ByVal HighRiskRuleText As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_AUTHPLUS_HIGHRISK", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@HighRiskRule", SqlDbType.VarChar)
            cmdCommand.Parameters("@HighRiskRule").Value = HighRiskRule

            cmdCommand.Parameters.Add("@HighRiskRuleText", SqlDbType.VarChar)
            cmdCommand.Parameters("@HighRiskRuleText").Value = HighRiskRuleText

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "Authentication - Old System"
    Public Sub UpdateAuthenticationOldData(ByVal ApplicationID As Integer _
                                        , ByVal ExperianFailedMessage As String _
                                        , ByVal ExperianDecisionCode As String _
                                        , ByVal ExperianRef As String _
                                        , ByVal ExperianAuthIndexCode As String _
                                        , ByVal ExperianAuthIndexText As String _
                                        , ByVal ExperianAuthDecisionCode As String _
                                        , ByVal ExperianAuthDecisionText As String _
                                        , ByVal ExperianPolicyCode As String _
                                        , ByVal ExperianPolicyRuleText As String _
                                       )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_UPDATE_OLD_AUTHENTICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ExperianFailedMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianFailedMessage").Value = ExperianFailedMessage

            cmdCommand.Parameters.Add("@ExperianDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianDecisionCode").Value = ExperianDecisionCode

            cmdCommand.Parameters.Add("@ExperianRef", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianRef").Value = ExperianRef

            cmdCommand.Parameters.Add("@ExperianAuthIndexCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexCode").Value = ExperianAuthIndexCode

            cmdCommand.Parameters.Add("@ExperianAuthIndexText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexText").Value = ExperianAuthIndexText

            cmdCommand.Parameters.Add("@ExperianAuthDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionCode").Value = ExperianAuthDecisionCode

            cmdCommand.Parameters.Add("@ExperianAuthDecisionText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionText").Value = ExperianAuthDecisionText

            cmdCommand.Parameters.Add("@ExperianPolicyCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyCode").Value = ExperianPolicyCode

            cmdCommand.Parameters.Add("@ExperianPolicyRuleText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyRuleText").Value = ExperianPolicyRuleText

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "Consumer Data Search"
    Public Sub InsertConsumerData_Main(ByVal ApplicationID As Integer _
                                        , ByVal ExperianReference As String _
                                        , ByVal ErrorCode As String _
                                        , ByVal ErrorMessage As String _
                                        , ByVal ErrorSeverity As String _
                                        , ByVal Scoring_E5S01 As String _
                                        , ByVal Scoring_E5S02 As String _
                                        , ByVal Scoring_E5S041 As String _
                                        , ByVal Scoring_E5S042 As String _
                                        , ByVal Scoring_E5S043 As String _
                                        , ByVal Scoring_E5S051 As String _
                                        , ByVal Scoring_E5S052 As String _
                                        , ByVal Scoring_E5S053 As String _
                                        , ByVal Scoring_NDHHOSCORE As String _
                                        , ByVal Scoring_NDSI21 As String _
                                        , ByVal Scoring_NDSI22 As String _
                                        , ByVal Scoring_NDSI23 As String _
                                        , ByVal Scoring_NDVALSCORE As String _
                                        , ByVal ElectoralRoll_E4Q01 As String _
                                        , ByVal ElectoralRoll_E4Q02 As String _
                                        , ByVal ElectoralRoll_E4Q03 As String _
                                        , ByVal ElectoralRoll_E4Q04 As String _
                                        , ByVal ElectoralRoll_E4Q05 As String _
                                        , ByVal ElectoralRoll_E4Q06 As String _
                                        , ByVal ElectoralRoll_E4Q07 As String _
                                        , ByVal ElectoralRoll_E4Q08 As String _
                                        , ByVal ElectoralRoll_E4Q09 As String _
                                        , ByVal ElectoralRoll_E4Q10 As String _
                                        , ByVal ElectoralRoll_E4Q11 As String _
                                        , ByVal ElectoralRoll_E4Q12 As String _
                                        , ByVal ElectoralRoll_E4Q13 As String _
                                        , ByVal ElectoralRoll_E4Q14 As String _
                                        , ByVal ElectoralRoll_E4Q15 As String _
                                        , ByVal ElectoralRoll_E4Q16 As String _
                                        , ByVal ElectoralRoll_E4Q17 As String _
                                        , ByVal ElectoralRoll_E4Q18 As String _
                                        , ByVal ElectoralRoll_E4R01 As String _
                                        , ByVal ElectoralRoll_E4R02 As String _
                                        , ByVal ElectoralRoll_E4R03 As String _
                                        , ByVal ElectoralRoll_EA4R01PM As String _
                                        , ByVal ElectoralRoll_EA4R01CJ As String _
                                        , ByVal ElectoralRoll_EA4R01PJ As String _
                                        , ByVal ElectoralRoll_NDERL01 As String _
                                        , ByVal ElectoralRoll_NDERL02 As String _
                                        , ByVal ElectoralRoll_EA2Q01 As String _
                                        , ByVal ElectoralRoll_EA2Q02 As String _
                                        , ByVal ElectoralRoll_NDERLMACA As String _
                                        , ByVal ElectoralRoll_NDERLMAPA As String _
                                        , ByVal ElectoralRoll_NDERLJACA As String _
                                        , ByVal ElectoralRoll_NDERLJAPA As String _
                                        , ByVal ElectoralRoll_EA5U01 As String _
                                        , ByVal ElectoralRoll_EA5U02 As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_MAIN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ExperianReference", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianReference").Value = ExperianReference

            cmdCommand.Parameters.Add("@ErrorCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorCode").Value = ErrorCode

            cmdCommand.Parameters.Add("@ErrorMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorMessage").Value = ErrorMessage

            cmdCommand.Parameters.Add("@ErrorSeverity", SqlDbType.VarChar)
            cmdCommand.Parameters("@ErrorSeverity").Value = ErrorSeverity

            cmdCommand.Parameters.Add("@Scoring_E5S01", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S01").Value = Scoring_E5S01

            cmdCommand.Parameters.Add("@Scoring_E5S02", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S02").Value = Scoring_E5S02

            cmdCommand.Parameters.Add("@Scoring_E5S041", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S041").Value = Scoring_E5S041

            cmdCommand.Parameters.Add("@Scoring_E5S042", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S042").Value = Scoring_E5S042

            cmdCommand.Parameters.Add("@Scoring_E5S043", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S043").Value = Scoring_E5S043

            cmdCommand.Parameters.Add("@Scoring_E5S051", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S051").Value = Scoring_E5S051

            cmdCommand.Parameters.Add("@Scoring_E5S052", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S052").Value = Scoring_E5S052

            cmdCommand.Parameters.Add("@Scoring_E5S053", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_E5S053").Value = Scoring_E5S053

            cmdCommand.Parameters.Add("@Scoring_NDHHOSCORE", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_NDHHOSCORE").Value = Scoring_NDHHOSCORE

            cmdCommand.Parameters.Add("@Scoring_NDSI21", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_NDSI21").Value = Scoring_NDSI21

            cmdCommand.Parameters.Add("@Scoring_NDSI22", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_NDSI22").Value = Scoring_NDSI22

            cmdCommand.Parameters.Add("@Scoring_NDSI23", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_NDSI23").Value = Scoring_NDSI23

            cmdCommand.Parameters.Add("@Scoring_NDVALSCORE", SqlDbType.VarChar)
            cmdCommand.Parameters("@Scoring_NDVALSCORE").Value = Scoring_NDVALSCORE

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q01", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q01").Value = ElectoralRoll_E4Q01

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q02", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q02").Value = ElectoralRoll_E4Q02

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q03", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q03").Value = ElectoralRoll_E4Q03

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q04", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q04").Value = ElectoralRoll_E4Q04

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q05", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q05").Value = ElectoralRoll_E4Q05

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q06", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q06").Value = ElectoralRoll_E4Q06

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q07", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q07").Value = ElectoralRoll_E4Q07

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q08", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q08").Value = ElectoralRoll_E4Q08

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q09", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q09").Value = ElectoralRoll_E4Q09

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q10", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q10").Value = ElectoralRoll_E4Q10

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q11", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q11").Value = ElectoralRoll_E4Q11

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q12", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q12").Value = ElectoralRoll_E4Q12

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q13", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q13").Value = ElectoralRoll_E4Q13

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q14", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q14").Value = ElectoralRoll_E4Q14

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q15", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q15").Value = ElectoralRoll_E4Q15

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q16", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q16").Value = ElectoralRoll_E4Q16

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q17", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q17").Value = ElectoralRoll_E4Q17

            cmdCommand.Parameters.Add("@ElectoralRoll_E4Q18", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4Q18").Value = ElectoralRoll_E4Q18

            cmdCommand.Parameters.Add("@ElectoralRoll_E4R01", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4R01").Value = ElectoralRoll_E4R01

            cmdCommand.Parameters.Add("@ElectoralRoll_E4R02", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4R02").Value = ElectoralRoll_E4R02

            cmdCommand.Parameters.Add("@ElectoralRoll_E4R03", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_E4R03").Value = ElectoralRoll_E4R03

            cmdCommand.Parameters.Add("@ElectoralRoll_EA4R01PM", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA4R01PM").Value = ElectoralRoll_EA4R01PM

            cmdCommand.Parameters.Add("@ElectoralRoll_EA4R01CJ", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA4R01CJ").Value = ElectoralRoll_EA4R01CJ

            cmdCommand.Parameters.Add("@ElectoralRoll_EA4R01PJ", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA4R01PJ").Value = ElectoralRoll_EA4R01PJ

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERL01", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERL01").Value = ElectoralRoll_NDERL01

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERL02", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERL02").Value = ElectoralRoll_NDERL02

            cmdCommand.Parameters.Add("@ElectoralRoll_EA2Q01", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA2Q01").Value = ElectoralRoll_EA2Q01

            cmdCommand.Parameters.Add("@ElectoralRoll_EA2Q02", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA2Q02").Value = ElectoralRoll_EA2Q02

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERLMACA", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERLMACA").Value = ElectoralRoll_NDERLMACA

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERLMAPA", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERLMAPA").Value = ElectoralRoll_NDERLMAPA

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERLJACA", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERLJACA").Value = ElectoralRoll_NDERLJACA

            cmdCommand.Parameters.Add("@ElectoralRoll_NDERLJAPA", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_NDERLJAPA").Value = ElectoralRoll_NDERLJAPA

            cmdCommand.Parameters.Add("@ElectoralRoll_EA5U01", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA5U01").Value = ElectoralRoll_EA5U01

            cmdCommand.Parameters.Add("@ElectoralRoll_EA5U02", SqlDbType.VarChar)
            cmdCommand.Parameters("@ElectoralRoll_EA5U02").Value = ElectoralRoll_EA5U02


            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_ImpairedCH(ByVal ApplicationID As Integer _
                                            , ByVal NDICH As String _
                                            , ByVal NDSECARR As String _
                                            , ByVal NDUNSECARR As String _
                                            , ByVal NDCCJ As String _
                                            , ByVal NDIVA As String _
                                            , ByVal NDBANKRUPT As String _
                                            , ByVal NDMAICH As String _
                                            , ByVal NDMASECARR As String _
                                            , ByVal NDMAUNSECARR As String _
                                            , ByVal NDMACCJ As String _
                                            , ByVal NDMAIVA As String _
                                            , ByVal NDMABANKRUPT As String _
                                            , ByVal NDJAICH As String _
                                            , ByVal NDJASECARR As String _
                                            , ByVal NDJAUNSECARR As String _
                                            , ByVal NDJACCJ As String _
                                            , ByVal NDJAIVA As String _
                                            , ByVal NDJABANKRUPT As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_ImpairedCH", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@NDICH", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDICH").Value = NDICH

            cmdCommand.Parameters.Add("@NDSECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDSECARR").Value = NDSECARR

            cmdCommand.Parameters.Add("@NDUNSECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDUNSECARR").Value = NDUNSECARR

            cmdCommand.Parameters.Add("@NDCCJ", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDCCJ").Value = NDCCJ

            cmdCommand.Parameters.Add("@NDIVA", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDIVA").Value = NDIVA

            cmdCommand.Parameters.Add("@NDBANKRUPT", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDBANKRUPT").Value = NDBANKRUPT

            cmdCommand.Parameters.Add("@NDMAICH", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMAICH").Value = NDMAICH

            cmdCommand.Parameters.Add("@NDMASECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMASECARR").Value = NDMASECARR

            cmdCommand.Parameters.Add("@NDMAUNSECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMAUNSECARR").Value = NDMAUNSECARR

            cmdCommand.Parameters.Add("@NDMACCJ", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMACCJ").Value = NDMACCJ

            cmdCommand.Parameters.Add("@NDMAIVA", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMAIVA").Value = NDMAIVA

            cmdCommand.Parameters.Add("@NDMABANKRUPT", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDMABANKRUPT").Value = NDMABANKRUPT

            cmdCommand.Parameters.Add("@NDJAICH", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJAICH").Value = NDJAICH

            cmdCommand.Parameters.Add("@NDJASECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJASECARR").Value = NDJASECARR

            cmdCommand.Parameters.Add("@NDJAUNSECARR", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJAUNSECARR").Value = NDJAUNSECARR

            cmdCommand.Parameters.Add("@NDJACCJ", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJACCJ").Value = NDJACCJ

            cmdCommand.Parameters.Add("@NDJAIVA", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJAIVA").Value = NDJAIVA

            cmdCommand.Parameters.Add("@NDJABANKRUPT", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDJABANKRUPT").Value = NDJABANKRUPT

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_MOSAIC(ByVal ApplicationID As Integer _
                                            , ByVal EA4M01 As String _
                                            , ByVal EA4M02 As String _
                                            , ByVal EA4M03 As String _
                                            , ByVal EA4M04 As String _
                                            , ByVal EA4M05 As String _
                                            , ByVal EA4M06 As String _
                                            , ByVal EA4M07 As String _
                                            , ByVal EA4M08 As String _
                                            , ByVal EA4M09 As String _
                                            , ByVal EA4M10 As String _
                                            , ByVal EA4M11 As String _
                                            , ByVal EA4M12 As String _
                                            , ByVal EA4T01 As String _
                                            , ByVal EA5T01 As String _
                                            , ByVal EA5T02 As String _
                                            , ByVal NDG01 As String _
                                            , ByVal EA4N01 As String _
                                            , ByVal EA4N02 As String _
                                            , ByVal EA4N03 As String _
                                            , ByVal EA4N04 As String _
                                            , ByVal EA4N05 As String _
                                            , ByVal NDG02 As String _
                                            , ByVal NDG03 As String _
                                            , ByVal NDG04 As String _
                                            , ByVal NDG05 As String _
                                            , ByVal NDG06 As String _
                                            , ByVal NDG07 As String _
                                            , ByVal NDG08 As String _
                                            , ByVal NDG09 As String _
                                            , ByVal NDG10 As String _
                                            , ByVal NDG11 As String _
                                            , ByVal NDG12 As String _
                                            , ByVal UKMOSAIC As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_MOSAIC", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@EA4M01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M01").Value = EA4M01

            cmdCommand.Parameters.Add("@EA4M02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M02").Value = EA4M02

            cmdCommand.Parameters.Add("@EA4M03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M03").Value = EA4M03

            cmdCommand.Parameters.Add("@EA4M04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M04").Value = EA4M04

            cmdCommand.Parameters.Add("@EA4M05", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M05").Value = EA4M05

            cmdCommand.Parameters.Add("@EA4M06", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M06").Value = EA4M06

            cmdCommand.Parameters.Add("@EA4M07", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M07").Value = EA4M07

            cmdCommand.Parameters.Add("@EA4M08", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M08").Value = EA4M08

            cmdCommand.Parameters.Add("@EA4M09", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M09").Value = EA4M09

            cmdCommand.Parameters.Add("@EA4M10", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M10").Value = EA4M10

            cmdCommand.Parameters.Add("@EA4M11", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M11").Value = EA4M11

            cmdCommand.Parameters.Add("@EA4M12", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4M12").Value = EA4M12

            cmdCommand.Parameters.Add("@EA4T01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4T01").Value = EA4T01

            cmdCommand.Parameters.Add("@EA5T01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA5T01").Value = EA5T01

            cmdCommand.Parameters.Add("@EA5T02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA5T02").Value = EA5T02

            cmdCommand.Parameters.Add("@NDG01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG01").Value = NDG01

            cmdCommand.Parameters.Add("@EA4N01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4N01").Value = EA4N01

            cmdCommand.Parameters.Add("@EA4N02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4N02").Value = EA4N02

            cmdCommand.Parameters.Add("@EA4N03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4N03").Value = EA4N03

            cmdCommand.Parameters.Add("@EA4N04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4N04").Value = EA4N04

            cmdCommand.Parameters.Add("@EA4N05", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4N05").Value = EA4N05

            cmdCommand.Parameters.Add("@NDG02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG02").Value = NDG02

            cmdCommand.Parameters.Add("@NDG03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG03").Value = NDG03

            cmdCommand.Parameters.Add("@NDG04", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG04").Value = NDG04

            cmdCommand.Parameters.Add("@NDG05", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG05").Value = NDG05

            cmdCommand.Parameters.Add("@NDG06", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG06").Value = NDG06

            cmdCommand.Parameters.Add("@NDG07", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG07").Value = NDG07

            cmdCommand.Parameters.Add("@NDG08", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG08").Value = NDG08

            cmdCommand.Parameters.Add("@NDG09", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG09").Value = NDG09

            cmdCommand.Parameters.Add("@NDG10", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG10").Value = NDG10

            cmdCommand.Parameters.Add("@NDG11", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG11").Value = NDG11

            cmdCommand.Parameters.Add("@NDG12", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDG12").Value = NDG12

            cmdCommand.Parameters.Add("@UKMOSAIC", SqlDbType.VarChar)
            cmdCommand.Parameters("@UKMOSAIC").Value = UKMOSAIC
            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB(ByVal ApplicationID As Integer _
                                                        , ByVal TPD_NDPA As String _
                                                        , ByVal TPD_NDHHO As String _
                                                        , ByVal TPD_NDOPTOUTVALID As String _
                                                        , ByVal CII_NDSPCII As String _
                                                        , ByVal CII_NDSPACII As String _
                                                        , ByVal AddrLink_NDLNK01 As String _
                                                        , ByVal Director_NDDIRSP As String _
                                                        , ByVal Director_NDDIRSPA As String _
                                                        , ByVal AgeDoB_NDDOB As String _
                                                        , ByVal AgeDoB_EA4S02 As String _
                                                        , ByVal AgeDoB_EA4S04 As String _
                                                        , ByVal AgeDoB_EA4S06 As String _
                                                        , ByVal AgeDoB_EA4S08 As String _
                                                        , ByVal AgeDoB_EA5S01 As String _
                                                        , ByVal AgeDoB_EA4S01 As String _
                                                        , ByVal AgeDoB_EA4S03 As String _
                                                        , ByVal AgeDoB_EA4S05 As String _
                                                        , ByVal AgeDoB_EA4S07 As String _
                                                        )


        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_TPD_CII_ADDLINK_DIRECTORS_AGEDOB", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@TPD_NDPA", SqlDbType.VarChar)
            cmdCommand.Parameters("@TPD_NDPA").Value = TPD_NDPA

            cmdCommand.Parameters.Add("@TPD_NDHHO", SqlDbType.VarChar)
            cmdCommand.Parameters("@TPD_NDHHO").Value = TPD_NDHHO

            cmdCommand.Parameters.Add("@TPD_NDOPTOUTVALID", SqlDbType.VarChar)
            cmdCommand.Parameters("@TPD_NDOPTOUTVALID").Value = TPD_NDOPTOUTVALID

            cmdCommand.Parameters.Add("@CII_NDSPCII", SqlDbType.VarChar)
            cmdCommand.Parameters("@CII_NDSPCII").Value = CII_NDSPCII

            cmdCommand.Parameters.Add("@CII_NDSPACII", SqlDbType.VarChar)
            cmdCommand.Parameters("@CII_NDSPACII").Value = CII_NDSPACII

            cmdCommand.Parameters.Add("@AddrLink_NDLNK01", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddrLink_NDLNK01").Value = AddrLink_NDLNK01

            cmdCommand.Parameters.Add("@Director_NDDIRSP", SqlDbType.VarChar)
            cmdCommand.Parameters("@Director_NDDIRSP").Value = Director_NDDIRSP

            cmdCommand.Parameters.Add("@Director_NDDIRSPA", SqlDbType.VarChar)
            cmdCommand.Parameters("@Director_NDDIRSPA").Value = Director_NDDIRSPA

            cmdCommand.Parameters.Add("@AgeDoB_NDDOB", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_NDDOB").Value = AgeDoB_NDDOB

            cmdCommand.Parameters.Add("@AgeDoB_EA4S02", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S02").Value = AgeDoB_EA4S02

            cmdCommand.Parameters.Add("@AgeDoB_EA4S04", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S04").Value = AgeDoB_EA4S04

            cmdCommand.Parameters.Add("@AgeDoB_EA4S06", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S06").Value = AgeDoB_EA4S06

            cmdCommand.Parameters.Add("@AgeDoB_EA4S08", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S08").Value = AgeDoB_EA4S08

            cmdCommand.Parameters.Add("@AgeDoB_EA5S01", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA5S01").Value = AgeDoB_EA5S01

            cmdCommand.Parameters.Add("@AgeDoB_EA4S01", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S01").Value = AgeDoB_EA4S01

            cmdCommand.Parameters.Add("@AgeDoB_EA4S03", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S03").Value = AgeDoB_EA4S03

            cmdCommand.Parameters.Add("@AgeDoB_EA4S05", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S05").Value = AgeDoB_EA4S05

            cmdCommand.Parameters.Add("@AgeDoB_EA4S07", SqlDbType.VarChar)
            cmdCommand.Parameters("@AgeDoB_EA4S07").Value = AgeDoB_EA4S07

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_ADB_Utilisationblock(ByVal ApplicationID As Integer _
                                                        , ByVal SPA01 As String _
                                                        , ByVal SPA02 As String _
                                                        , ByVal SPA03 As String _
                                                        , ByVal SPA04 As String _
                                                        , ByVal SPA05 As String _
                                                        , ByVal SPA06 As String _
                                                        , ByVal SPA07 As String _
                                                        , ByVal SPA08 As String _
                                                        , ByVal SPA09 As String _
                                                        , ByVal SPA10 As String _
                                                        , ByVal SPB111 As String _
                                                        , ByVal SPB112 As String _
                                                        , ByVal SPB113 As String _
                                                        , ByVal SPB114 As String _
                                                        , ByVal SPB115 As String _
                                                        , ByVal SPB116 As String _
                                                        , ByVal SPB117 As String _
                                                        , ByVal SPB218 As String _
                                                        , ByVal SPB219 As String _
                                                        , ByVal SPB220 As String _
                                                        , ByVal SPB221 As String _
                                                        , ByVal SPB322 As String _
                                                        , ByVal SPB323 As String _
                                                        , ByVal SPC24 As String _
                                                        , ByVal SPD25 As String _
                                                        , ByVal SPE126 As String _
                                                        , ByVal SPE127 As String _
                                                        , ByVal SPE128 As String _
                                                        , ByVal SPF129 As String _
                                                        , ByVal SPF130 As String _
                                                        , ByVal SPF131 As String _
                                                        , ByVal SPF232 As String _
                                                        , ByVal SPF233 As String _
                                                        , ByVal SPF334 As String _
                                                        , ByVal SPF335 As String _
                                                        , ByVal SPF336 As String _
                                                        , ByVal SPG37 As String _
                                                        , ByVal SPG38 As String _
                                                        , ByVal SPH39 As String _
                                                        , ByVal SPH40 As String _
                                                        , ByVal SPH41 As String _
                                                        , ByVal SPCIICHECKDIGIT As String _
                                                        , ByVal SPAA01 As String _
                                                        , ByVal SPAA02 As String _
                                                        , ByVal SPAA03 As String _
                                                        , ByVal SPAA04 As String _
                                                        , ByVal SPAA05 As String _
                                                        , ByVal SPAA06 As String _
                                                        , ByVal SPAA07 As String _
                                                        , ByVal SPAA08 As String _
                                                        , ByVal SPAA09 As String _
                                                        , ByVal SPAA10 As String _
                                                        , ByVal SPAB111 As String _
                                                        , ByVal SPAB112 As String _
                                                        , ByVal SPAB113 As String _
                                                        , ByVal SPAB114 As String _
                                                        , ByVal SPAB115 As String _
                                                        , ByVal SPAB116 As String _
                                                        , ByVal SPAB117 As String _
                                                        , ByVal SPAB218 As String _
                                                        , ByVal SPAB219 As String _
                                                        , ByVal SPAB220 As String _
                                                        , ByVal SPAB221 As String _
                                                        , ByVal SPAB322 As String _
                                                        , ByVal SPAB323 As String _
                                                        , ByVal SPAC24 As String _
                                                        , ByVal SPAD25 As String _
                                                        , ByVal SPAE126 As String _
                                                        , ByVal SPAE127 As String _
                                                        , ByVal SPAE128 As String _
                                                        , ByVal SPAF129 As String _
                                                        , ByVal SPAF130 As String _
                                                        , ByVal SPAF131 As String _
                                                        , ByVal SPAF232 As String _
                                                        , ByVal SPAF233 As String _
                                                        , ByVal SPAF334 As String _
                                                        , ByVal SPAF335 As String _
                                                        , ByVal SPAF336 As String _
                                                        , ByVal SPAG37 As String _
                                                        , ByVal SPAG38 As String _
                                                        , ByVal SPAH39 As String _
                                                        , ByVal SPAH40 As String _
                                                        , ByVal SPAH41 As String _
                                                        , ByVal SPACIICHECKDIGIT As String _
                                                        )


        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_ADB_Utilisationblock", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@SPA01", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA01").Value = SPA01

            cmdCommand.Parameters.Add("@SPA02", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA02").Value = SPA02

            cmdCommand.Parameters.Add("@SPA03", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA03").Value = SPA03

            cmdCommand.Parameters.Add("@SPA04", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA04").Value = SPA04

            cmdCommand.Parameters.Add("@SPA05", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA05").Value = SPA05

            cmdCommand.Parameters.Add("@SPA06", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA06").Value = SPA06

            cmdCommand.Parameters.Add("@SPA07", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA07").Value = SPA07

            cmdCommand.Parameters.Add("@SPA08", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA08").Value = SPA08

            cmdCommand.Parameters.Add("@SPA09", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA09").Value = SPA09

            cmdCommand.Parameters.Add("@SPA10", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPA10").Value = SPA10

            cmdCommand.Parameters.Add("@SPB111", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB111").Value = SPB111

            cmdCommand.Parameters.Add("@SPB112", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB112").Value = SPB112

            cmdCommand.Parameters.Add("@SPB113", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB113").Value = SPB113

            cmdCommand.Parameters.Add("@SPB114", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB114").Value = SPB114

            cmdCommand.Parameters.Add("@SPB115", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB115").Value = SPB115

            cmdCommand.Parameters.Add("@SPB116", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB116").Value = SPB116

            cmdCommand.Parameters.Add("@SPB117", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB117").Value = SPB117

            cmdCommand.Parameters.Add("@SPB218", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB218").Value = SPB218

            cmdCommand.Parameters.Add("@SPB219", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB219").Value = SPB219

            cmdCommand.Parameters.Add("@SPB220", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB220").Value = SPB220

            cmdCommand.Parameters.Add("@SPB221", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB221").Value = SPB221

            cmdCommand.Parameters.Add("@SPB322", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB322").Value = SPB322

            cmdCommand.Parameters.Add("@SPB323", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPB323").Value = SPB323

            cmdCommand.Parameters.Add("@SPC24", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPC24").Value = SPC24

            cmdCommand.Parameters.Add("@SPD25", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPD25").Value = SPD25

            cmdCommand.Parameters.Add("@SPE126", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPE126").Value = SPE126

            cmdCommand.Parameters.Add("@SPE127", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPE127").Value = SPE127

            cmdCommand.Parameters.Add("@SPE128", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPE128").Value = SPE128

            cmdCommand.Parameters.Add("@SPF129", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF129").Value = SPF129

            cmdCommand.Parameters.Add("@SPF130", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF130").Value = SPF130

            cmdCommand.Parameters.Add("@SPF131", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF131").Value = SPF131

            cmdCommand.Parameters.Add("@SPF232", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF232").Value = SPF232

            cmdCommand.Parameters.Add("@SPF233", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF233").Value = SPF233

            cmdCommand.Parameters.Add("@SPF334", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF334").Value = SPF334

            cmdCommand.Parameters.Add("@SPF335", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF335").Value = SPF335

            cmdCommand.Parameters.Add("@SPF336", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPF336").Value = SPF336

            cmdCommand.Parameters.Add("@SPG37", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPG37").Value = SPG37

            cmdCommand.Parameters.Add("@SPG38", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPG38").Value = SPG38

            cmdCommand.Parameters.Add("@SPH39", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPH39").Value = SPH39

            cmdCommand.Parameters.Add("@SPH40", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPH40").Value = SPH40

            cmdCommand.Parameters.Add("@SPH41", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPH41").Value = SPH41

            cmdCommand.Parameters.Add("@SPCIICHECKDIGIT", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPCIICHECKDIGIT").Value = SPCIICHECKDIGIT

            cmdCommand.Parameters.Add("@SPAA01", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA01").Value = SPAA01

            cmdCommand.Parameters.Add("@SPAA02", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA02").Value = SPAA02

            cmdCommand.Parameters.Add("@SPAA03", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA03").Value = SPAA03

            cmdCommand.Parameters.Add("@SPAA04", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA04").Value = SPAA04

            cmdCommand.Parameters.Add("@SPAA05", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA05").Value = SPAA05

            cmdCommand.Parameters.Add("@SPAA06", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA06").Value = SPAA06

            cmdCommand.Parameters.Add("@SPAA07", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA07").Value = SPAA07

            cmdCommand.Parameters.Add("@SPAA08", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA08").Value = SPAA08

            cmdCommand.Parameters.Add("@SPAA09", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA09").Value = SPAA09

            cmdCommand.Parameters.Add("@SPAA10", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAA10").Value = SPAA10

            cmdCommand.Parameters.Add("@SPAB111", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB111").Value = SPAB111

            cmdCommand.Parameters.Add("@SPAB112", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB112").Value = SPAB112

            cmdCommand.Parameters.Add("@SPAB113", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB113").Value = SPAB113

            cmdCommand.Parameters.Add("@SPAB114", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB114").Value = SPAB114

            cmdCommand.Parameters.Add("@SPAB115", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB115").Value = SPAB115

            cmdCommand.Parameters.Add("@SPAB116", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB116").Value = SPAB116

            cmdCommand.Parameters.Add("@SPAB117", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB117").Value = SPAB117

            cmdCommand.Parameters.Add("@SPAB218", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB218").Value = SPAB218

            cmdCommand.Parameters.Add("@SPAB219", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB219").Value = SPAB219

            cmdCommand.Parameters.Add("@SPAB220", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB220").Value = SPAB220

            cmdCommand.Parameters.Add("@SPAB221", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB221").Value = SPAB221

            cmdCommand.Parameters.Add("@SPAB322", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB322").Value = SPAB322

            cmdCommand.Parameters.Add("@SPAB323", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAB323").Value = SPAB323

            cmdCommand.Parameters.Add("@SPAC24", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAC24").Value = SPAC24

            cmdCommand.Parameters.Add("@SPAD25", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAD25").Value = SPAD25

            cmdCommand.Parameters.Add("@SPAE126", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAE126").Value = SPAE126

            cmdCommand.Parameters.Add("@SPAE127", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAE127").Value = SPAE127

            cmdCommand.Parameters.Add("@SPAE128", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAE128").Value = SPAE128

            cmdCommand.Parameters.Add("@SPAF129", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF129").Value = SPAF129

            cmdCommand.Parameters.Add("@SPAF130", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF130").Value = SPAF130

            cmdCommand.Parameters.Add("@SPAF131", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF131").Value = SPAF131

            cmdCommand.Parameters.Add("@SPAF232", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF232").Value = SPAF232

            cmdCommand.Parameters.Add("@SPAF233", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF233").Value = SPAF233

            cmdCommand.Parameters.Add("@SPAF334", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF334").Value = SPAF334

            cmdCommand.Parameters.Add("@SPAF335", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF335").Value = SPAF335

            cmdCommand.Parameters.Add("@SPAF336", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAF336").Value = SPAF336

            cmdCommand.Parameters.Add("@SPAG37", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAG37").Value = SPAG37

            cmdCommand.Parameters.Add("@SPAG38", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAG38").Value = SPAG38

            cmdCommand.Parameters.Add("@SPAH39", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAH39").Value = SPAH39

            cmdCommand.Parameters.Add("@SPAH40", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAH40").Value = SPAH40

            cmdCommand.Parameters.Add("@SPAH41", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAH41").Value = SPAH41

            cmdCommand.Parameters.Add("@SPACIICHECKDIGIT", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPACIICHECKDIGIT").Value = SPACIICHECKDIGIT

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_ADB_TelecommsBlock(ByVal ApplicationID As Integer _
                                              , ByVal SPNODEL As String _
                                              , ByVal SPVALDEL As String _
                                              , ByVal SPTSMRTELDEL As String _
                                              , ByVal SPNOACTTEL As String _
                                              , ByVal SPBALACTTEL As String _
                                              , ByVal SPTSMRTEL As String _
                                              , ByVal SPTOTNOACTTEL As String _
                                              , ByVal SPNOINACTTELL12 As String _
                                              , ByVal SPNOINACTTELL24 As String _
                                              , ByVal SPNOINACTTEL36 As String _
                                              , ByVal SPTOTNOSET As String _
                                              , ByVal SPTOTNOSETL12 As String _
                                              , ByVal SPTOTNOSETL24 As String _
                                              , ByVal SPTOTNOSETL36 As String _
                                              , ByVal SPTSMRSETTEL As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_ADB_TelecommsBlock", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@SPNODEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNODEL").Value = SPNODEL

            cmdCommand.Parameters.Add("@SPVALDEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPVALDEL").Value = SPVALDEL

            cmdCommand.Parameters.Add("@SPTSMRTELDEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTSMRTELDEL").Value = SPTSMRTELDEL

            cmdCommand.Parameters.Add("@SPNOACTTEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOACTTEL").Value = SPNOACTTEL

            cmdCommand.Parameters.Add("@SPBALACTTEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBALACTTEL").Value = SPBALACTTEL

            cmdCommand.Parameters.Add("@SPTSMRTEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTSMRTEL").Value = SPTSMRTEL

            cmdCommand.Parameters.Add("@SPTOTNOACTTEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTOTNOACTTEL").Value = SPTOTNOACTTEL

            cmdCommand.Parameters.Add("@SPNOINACTTELL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOINACTTELL12").Value = SPNOINACTTELL12

            cmdCommand.Parameters.Add("@SPNOINACTTELL24", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOINACTTELL24").Value = SPNOINACTTELL24

            cmdCommand.Parameters.Add("@SPNOINACTTEL36", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOINACTTEL36").Value = SPNOINACTTEL36

            cmdCommand.Parameters.Add("@SPTOTNOSET", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTOTNOSET").Value = SPTOTNOSET

            cmdCommand.Parameters.Add("@SPTOTNOSETL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTOTNOSETL12").Value = SPTOTNOSETL12

            cmdCommand.Parameters.Add("@SPTOTNOSETL24", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTOTNOSETL24").Value = SPTOTNOSETL24

            cmdCommand.Parameters.Add("@SPTOTNOSETL36", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTOTNOSETL36").Value = SPTOTNOSETL36

            cmdCommand.Parameters.Add("@SPTSMRSETTEL", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTSMRSETTEL").Value = SPTSMRSETTEL

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_ADB_NeverPaidDefsBlock(ByVal ApplicationID As Integer _
                                              , ByVal SPNONPDL12 As String _
                                              , ByVal SPBALNPDL12 As String _
                                              , ByVal SPNONPD As String _
                                              , ByVal SPBALNPD As String _
                                              , ByVal SPTSMRNPD As String _
                                              , ByVal SPNOEBADSL12 As String _
                                              , ByVal SPBALEBADSL12 As String _
                                              , ByVal SPNOEBADS As String _
                                              , ByVal SPBALEBADS As String _
                                              , ByVal SPTSMREBAD As String _
                                              , ByVal SPMTHFSTEBAD As String _
                                              , ByVal SPANONPDL12 As String _
                                              , ByVal SPABALNPDL12 As String _
                                              , ByVal SPANONPD As String _
                                              , ByVal SPABALNPD As String _
                                              , ByVal SPATSMRNPD As String _
                                              , ByVal SPANOEBADSL12 As String _
                                              , ByVal SPABALEBADSL12 As String _
                                              , ByVal SPANOEBADS As String _
                                              , ByVal SPABALEBADS As String _
                                              , ByVal SPATSMREBAD As String _
                                              , ByVal SPAMTHFSTEBAD As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_ADB_NeverPaidDefsBlock", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@SPNONPDL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNONPDL12").Value = SPNONPDL12

            cmdCommand.Parameters.Add("@SPBALNPDL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBALNPDL12").Value = SPBALNPDL12

            cmdCommand.Parameters.Add("@SPNONPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNONPD").Value = SPNONPD

            cmdCommand.Parameters.Add("@SPBALNPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBALNPD").Value = SPBALNPD

            cmdCommand.Parameters.Add("@SPTSMRNPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTSMRNPD").Value = SPTSMRNPD

            cmdCommand.Parameters.Add("@SPNOEBADSL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOEBADSL12").Value = SPNOEBADSL12

            cmdCommand.Parameters.Add("@SPBALEBADSL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBALEBADSL12").Value = SPBALEBADSL12

            cmdCommand.Parameters.Add("@SPNOEBADS", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPNOEBADS").Value = SPNOEBADS

            cmdCommand.Parameters.Add("@SPBALEBADS", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBALEBADS").Value = SPBALEBADS

            cmdCommand.Parameters.Add("@SPTSMREBAD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPTSMREBAD").Value = SPTSMREBAD

            cmdCommand.Parameters.Add("@SPMTHFSTEBAD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPMTHFSTEBAD").Value = SPMTHFSTEBAD

            cmdCommand.Parameters.Add("@SPANONPDL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPANONPDL12").Value = SPANONPDL12

            cmdCommand.Parameters.Add("@SPABALNPDL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPABALNPDL12").Value = SPABALNPDL12

            cmdCommand.Parameters.Add("@SPANONPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPANONPD").Value = SPANONPD

            cmdCommand.Parameters.Add("@SPABALNPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPABALNPD").Value = SPABALNPD

            cmdCommand.Parameters.Add("@SPATSMRNPD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPATSMRNPD").Value = SPATSMRNPD

            cmdCommand.Parameters.Add("@SPANOEBADSL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPANOEBADSL12").Value = SPANOEBADSL12

            cmdCommand.Parameters.Add("@SPABALEBADSL12", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPABALEBADSL12").Value = SPABALEBADSL12

            cmdCommand.Parameters.Add("@SPANOEBADS", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPANOEBADS").Value = SPANOEBADS

            cmdCommand.Parameters.Add("@SPABALEBADS", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPABALEBADS").Value = SPABALEBADS

            cmdCommand.Parameters.Add("@SPATSMREBAD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPATSMREBAD").Value = SPATSMREBAD

            cmdCommand.Parameters.Add("@SPAMTHFSTEBAD", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPAMTHFSTEBAD").Value = SPAMTHFSTEBAD

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PVD_ADB_APACSCCBehavrlData(ByVal ApplicationID As Integer _
                                                , ByVal CCDATASUPPLIED As String _
                                                , ByVal NOMPMNPRL3M As String _
                                                , ByVal PTBRL3MNPRL3M As String _
                                                , ByVal PTBRL6MNPRL6M As String _
                                                , ByVal NOCAL1M As String _
                                                , ByVal NOCAL3M As String _
                                                , ByVal NOMLVCAL1M As String _
                                                , ByVal NOMLVCAL3M As String _
                                                , ByVal CLUCLIL6M As String _
                                                , ByVal CLUCLIL6MNPRL6M As String _
                                                , ByVal CLUNPRL1M As String _
                                                , ByVal NOCLDL3M As String _
                                                , ByVal NOASBNPRL1M As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PVD_ADB_APACSCCBehavrlData", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@CCDATASUPPLIED", SqlDbType.VarChar)
            cmdCommand.Parameters("@CCDATASUPPLIED").Value = CCDATASUPPLIED

            cmdCommand.Parameters.Add("@NOMPMNPRL3M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOMPMNPRL3M").Value = NOMPMNPRL3M

            cmdCommand.Parameters.Add("@PTBRL3MNPRL3M", SqlDbType.VarChar)
            cmdCommand.Parameters("@PTBRL3MNPRL3M").Value = PTBRL3MNPRL3M

            cmdCommand.Parameters.Add("@PTBRL6MNPRL6M", SqlDbType.VarChar)
            cmdCommand.Parameters("@PTBRL6MNPRL6M").Value = PTBRL6MNPRL6M

            cmdCommand.Parameters.Add("@NOCAL1M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOCAL1M").Value = NOCAL1M

            cmdCommand.Parameters.Add("@NOCAL3M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOCAL3M").Value = NOCAL3M

            cmdCommand.Parameters.Add("@NOMLVCAL1M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOMLVCAL1M").Value = NOMLVCAL1M

            cmdCommand.Parameters.Add("@NOMLVCAL3M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOMLVCAL3M").Value = NOMLVCAL3M

            cmdCommand.Parameters.Add("@CLUCLIL6M", SqlDbType.VarChar)
            cmdCommand.Parameters("@CLUCLIL6M").Value = CLUCLIL6M

            cmdCommand.Parameters.Add("@CLUCLIL6MNPRL6M", SqlDbType.VarChar)
            cmdCommand.Parameters("@CLUCLIL6MNPRL6M").Value = CLUCLIL6MNPRL6M

            cmdCommand.Parameters.Add("@CLUNPRL1M", SqlDbType.VarChar)
            cmdCommand.Parameters("@CLUNPRL1M").Value = CLUNPRL1M

            cmdCommand.Parameters.Add("@NOCLDL3M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOCLDL3M").Value = NOCLDL3M

            cmdCommand.Parameters.Add("@NOASBNPRL1M", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOASBNPRL1M").Value = NOASBNPRL1M

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_PublicInfo(ByVal ApplicationID As Integer _
                                                , ByVal E1A01 As String _
                                                , ByVal E1A02 As String _
                                                , ByVal E1A03 As String _
                                                , ByVal EA1C01 As String _
                                                , ByVal EA1D01 As String _
                                                , ByVal EA1D02 As String _
                                                , ByVal EA1D03 As String _
                                                , ByVal E2G01 As String _
                                                , ByVal E2G02 As String _
                                                , ByVal E2G03 As String _
                                                , ByVal EA2I01 As String _
                                                , ByVal EA2J01 As String _
                                                , ByVal EA2J02 As String _
                                                , ByVal EA2J03 As String _
                                                , ByVal EA4Q06 As String _
                                                , ByVal SPABRPRESENT As String _
                                                , ByVal SPBRPRESENT As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_PublicInfo", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@E1A01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A01").Value = E1A01

            cmdCommand.Parameters.Add("@E1A02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A02").Value = E1A02

            cmdCommand.Parameters.Add("@E1A03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A03").Value = E1A03

            cmdCommand.Parameters.Add("@EA1C01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1C01").Value = EA1C01

            cmdCommand.Parameters.Add("@EA1D01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1D01").Value = EA1D01

            cmdCommand.Parameters.Add("@EA1D02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1D02").Value = EA1D02

            cmdCommand.Parameters.Add("@EA1D03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1D03").Value = EA1D03

            cmdCommand.Parameters.Add("@E2G01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G01").Value = E2G01

            cmdCommand.Parameters.Add("@E2G02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G02").Value = E2G02

            cmdCommand.Parameters.Add("@E2G03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G03").Value = E2G03

            cmdCommand.Parameters.Add("@EA2I01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2I01").Value = EA2I01

            cmdCommand.Parameters.Add("@EA2J01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2J01").Value = EA2J01

            cmdCommand.Parameters.Add("@EA2J02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2J02").Value = EA2J02

            cmdCommand.Parameters.Add("@EA2J03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2J03").Value = EA2J03

            cmdCommand.Parameters.Add("@EA4Q06", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA4Q06").Value = EA4Q06

            cmdCommand.Parameters.Add("@SPABRPRESENT", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPABRPRESENT").Value = SPABRPRESENT

            cmdCommand.Parameters.Add("@SPBRPRESENT", SqlDbType.VarChar)
            cmdCommand.Parameters("@SPBRPRESENT").Value = SPBRPRESENT

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_CIFAS_CML_GAIN_NOC_TPD(ByVal ApplicationID As Integer _
                                                , ByVal CIFAS_EA1A01 As String _
                                                , ByVal CIFAS_EA2G01 As String _
                                                , ByVal CIFAS_EA4P01 As String _
                                                , ByVal CML_EA1C02 As String _
                                                , ByVal CML_EA2I02 As String _
                                                , ByVal GAIN_EA1G01 As String _
                                                , ByVal GAIN_EA1G02 As String _
                                                , ByVal GAIN_EA2M01 As String _
                                                , ByVal GAIN_EA2M02 As String _
                                                , ByVal NOC_EA4Q07 As String _
                                                , ByVal NOC_EA4Q08 As String _
                                                , ByVal NOC_EA4Q09 As String _
                                                , ByVal NOC_EA4Q10 As String _
                                                , ByVal NOC_EA4Q11 As String _
                                                , ByVal NOC_EA4Q01 As String _
                                                , ByVal NOC_EA4Q02 As String _
                                                , ByVal NOC_EA4Q03 As String _
                                                , ByVal NOC_EA4Q04 As String _
                                                , ByVal NOC_EA4Q05 As String _
                                                , ByVal TPD_NDOPTOUT As String _
                                                                )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_CIFAS_CML_GAIN_NOC_TPD", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@CIFAS_EA1A01", SqlDbType.VarChar)
            cmdCommand.Parameters("@CIFAS_EA1A01").Value = CIFAS_EA1A01

            cmdCommand.Parameters.Add("@CIFAS_EA2G01", SqlDbType.VarChar)
            cmdCommand.Parameters("@CIFAS_EA2G01").Value = CIFAS_EA2G01

            cmdCommand.Parameters.Add("@CIFAS_EA4P01", SqlDbType.VarChar)
            cmdCommand.Parameters("@CIFAS_EA4P01").Value = CIFAS_EA4P01

            cmdCommand.Parameters.Add("@CML_EA1C02", SqlDbType.VarChar)
            cmdCommand.Parameters("@CML_EA1C02").Value = CML_EA1C02

            cmdCommand.Parameters.Add("@CML_EA2I02", SqlDbType.VarChar)
            cmdCommand.Parameters("@CML_EA2I02").Value = CML_EA2I02

            cmdCommand.Parameters.Add("@GAIN_EA1G01", SqlDbType.VarChar)
            cmdCommand.Parameters("@GAIN_EA1G01").Value = GAIN_EA1G01

            cmdCommand.Parameters.Add("@GAIN_EA1G02", SqlDbType.VarChar)
            cmdCommand.Parameters("@GAIN_EA1G02").Value = GAIN_EA1G02

            cmdCommand.Parameters.Add("@GAIN_EA2M01", SqlDbType.VarChar)
            cmdCommand.Parameters("@GAIN_EA2M01").Value = GAIN_EA2M01

            cmdCommand.Parameters.Add("@GAIN_EA2M02", SqlDbType.VarChar)
            cmdCommand.Parameters("@GAIN_EA2M02").Value = GAIN_EA2M02

            cmdCommand.Parameters.Add("@NOC_EA4Q07", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q07").Value = NOC_EA4Q07

            cmdCommand.Parameters.Add("@NOC_EA4Q08", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q08").Value = NOC_EA4Q08

            cmdCommand.Parameters.Add("@NOC_EA4Q09", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q09").Value = NOC_EA4Q09

            cmdCommand.Parameters.Add("@NOC_EA4Q10", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q10").Value = NOC_EA4Q10

            cmdCommand.Parameters.Add("@NOC_EA4Q11", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q11").Value = NOC_EA4Q11

            cmdCommand.Parameters.Add("@NOC_EA4Q01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q01").Value = NOC_EA4Q01

            cmdCommand.Parameters.Add("@NOC_EA4Q02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q02").Value = NOC_EA4Q02

            cmdCommand.Parameters.Add("@NOC_EA4Q03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q03").Value = NOC_EA4Q03

            cmdCommand.Parameters.Add("@NOC_EA4Q04", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q04").Value = NOC_EA4Q04

            cmdCommand.Parameters.Add("@NOC_EA4Q05", SqlDbType.VarChar)
            cmdCommand.Parameters("@NOC_EA4Q05").Value = NOC_EA4Q05

            cmdCommand.Parameters.Add("@TPD_NDOPTOUT", SqlDbType.VarChar)
            cmdCommand.Parameters("@TPD_NDOPTOUT").Value = TPD_NDOPTOUT

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_CAPS(ByVal ApplicationID As Integer _
                                                , ByVal E1E01 As String _
                                                , ByVal E1E02 As String _
                                                , ByVal EA1B01 As String _
                                                , ByVal NDPSD01 As String _
                                                , ByVal NDPSD02 As String _
                                                , ByVal NDPSD03 As String _
                                                , ByVal NDPSD04 As String _
                                                , ByVal NDPSD05 As String _
                                                , ByVal NDPSD06 As String _
                                                , ByVal EA1E01 As String _
                                                , ByVal EA1E02 As String _
                                                , ByVal EA1E03 As String _
                                                , ByVal EA1E04 As String _
                                                , ByVal E2K01 As String _
                                                , ByVal E2K02 As String _
                                                , ByVal EA2H01 As String _
                                                , ByVal NDPSD07 As String _
                                                , ByVal NDPSD08 As String _
                                                , ByVal NDPSD09 As String _
                                                , ByVal NDPSD10 As String _
                                                , ByVal EA2K01 As String _
                                                , ByVal EA2K02 As String _
                                                , ByVal EA2K03 As String _
                                                , ByVal EA2K04 As String _
                                                , ByVal NDPSD11 As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_CAPS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@E1E01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1E01").Value = E1E01

            cmdCommand.Parameters.Add("@E1E02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1E02").Value = E1E02

            cmdCommand.Parameters.Add("@EA1B01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1B01").Value = EA1B01

            cmdCommand.Parameters.Add("@NDPSD01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD01").Value = NDPSD01

            cmdCommand.Parameters.Add("@NDPSD02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD02").Value = NDPSD02

            cmdCommand.Parameters.Add("@NDPSD03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD03").Value = NDPSD03

            cmdCommand.Parameters.Add("@NDPSD04", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD04").Value = NDPSD04

            cmdCommand.Parameters.Add("@NDPSD05", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD05").Value = NDPSD05

            cmdCommand.Parameters.Add("@NDPSD06", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD06").Value = NDPSD06

            cmdCommand.Parameters.Add("@EA1E01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1E01").Value = EA1E01

            cmdCommand.Parameters.Add("@EA1E02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1E02").Value = EA1E02

            cmdCommand.Parameters.Add("@EA1E03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1E03").Value = EA1E03

            cmdCommand.Parameters.Add("@EA1E04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1E04").Value = EA1E04

            cmdCommand.Parameters.Add("@E2K01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2K01").Value = E2K01

            cmdCommand.Parameters.Add("@E2K02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2K02").Value = E2K02

            cmdCommand.Parameters.Add("@EA2H01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2H01").Value = EA2H01

            cmdCommand.Parameters.Add("@NDPSD07", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD07").Value = NDPSD07

            cmdCommand.Parameters.Add("@NDPSD08", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD08").Value = NDPSD08

            cmdCommand.Parameters.Add("@NDPSD09", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD09").Value = NDPSD09

            cmdCommand.Parameters.Add("@NDPSD10", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD10").Value = NDPSD10

            cmdCommand.Parameters.Add("@EA2K01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2K01").Value = EA2K01

            cmdCommand.Parameters.Add("@EA2K02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2K02").Value = EA2K02

            cmdCommand.Parameters.Add("@EA2K03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2K03").Value = EA2K03

            cmdCommand.Parameters.Add("@EA2K04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2K04").Value = EA2K04

            cmdCommand.Parameters.Add("@NDPSD11", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDPSD11").Value = NDPSD11

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_CAIS(ByVal ApplicationID As Integer _
                                                  , ByVal E1A04 As String _
                                                  , ByVal E1A05 As String _
                                                  , ByVal E1A06 As String _
                                                  , ByVal E1A07 As String _
                                                  , ByVal E1A08 As String _
                                                  , ByVal E1A09 As String _
                                                  , ByVal E1A10 As String _
                                                  , ByVal E1A11 As String _
                                                  , ByVal E1B01 As String _
                                                  , ByVal E1B02 As String _
                                                  , ByVal E1B03 As String _
                                                  , ByVal E1B04 As String _
                                                  , ByVal E1B05 As String _
                                                  , ByVal E1B06 As String _
                                                  , ByVal E1B07 As String _
                                                  , ByVal E1B08 As String _
                                                  , ByVal E1B09 As String _
                                                  , ByVal E1B10 As String _
                                                  , ByVal E1B11 As String _
                                                  , ByVal E1B12 As String _
                                                  , ByVal E1B13 As String _
                                                  , ByVal NDECC01 As String _
                                                  , ByVal NDECC02 As String _
                                                  , ByVal NDECC03 As String _
                                                  , ByVal NDECC04 As String _
                                                  , ByVal NDECC07 As String _
                                                  , ByVal NDECC08 As String _
                                                  , ByVal E1C01 As String _
                                                  , ByVal E1C02 As String _
                                                  , ByVal E1C03 As String _
                                                  , ByVal E1C04 As String _
                                                  , ByVal E1C05 As String _
                                                  , ByVal E1C06 As String _
                                                  , ByVal EA1B02 As String _
                                                  , ByVal E1D01 As String _
                                                  , ByVal E1D02 As String _
                                                  , ByVal E1D03 As String _
                                                  , ByVal E1D04 As String _
                                                  , ByVal NDHAC01 As String _
                                                  , ByVal NDHAC02 As String _
                                                  , ByVal NDHAC03 As String _
                                                  , ByVal NDHAC04 As String _
                                                  , ByVal NDHAC05 As String _
                                                  , ByVal NDHAC09 As String _
                                                  , ByVal NDINC01 As String _
                                                  , ByVal EA1F01 As String _
                                                  , ByVal EA1F02 As String _
                                                  , ByVal EA1F03 As String _
                                                  , ByVal NDFCS01 As String _
                                                  , ByVal EA1F04 As String _
                                                  , ByVal E2G04 As String _
                                                  , ByVal E2G05 As String _
                                                  , ByVal E2G06 As String _
                                                  , ByVal E2G07 As String _
                                                  , ByVal E2G08 As String _
                                                  , ByVal E2G09 As String _
                                                  , ByVal E2G10 As String _
                                                  , ByVal E2G11 As String _
                                                  , ByVal E2H01 As String _
                                                  , ByVal E2H02 As String _
                                                  , ByVal E2H03 As String _
                                                  , ByVal E2H04 As String _
                                                  , ByVal E2H05 As String _
                                                  , ByVal E2H06 As String _
                                                  , ByVal E2H07 As String _
                                                  , ByVal E2H08 As String _
                                                  , ByVal E2H09 As String _
                                                  , ByVal E2H10 As String _
                                                  , ByVal E2H11 As String _
                                                  , ByVal E2H12 As String _
                                                  , ByVal E2H13 As String _
                                                  , ByVal NDECC05 As String _
                                                  , ByVal NDECC09 As String _
                                                  , ByVal NDECC10 As String _
                                                  , ByVal E2I01 As String _
                                                  , ByVal E2I02 As String _
                                                  , ByVal E2I03 As String _
                                                  , ByVal E2I04 As String _
                                                  , ByVal E2I05 As String _
                                                  , ByVal E2I06 As String _
                                                  , ByVal EA2H02 As String _
                                                  , ByVal E2J01 As String _
                                                  , ByVal E2J02 As String _
                                                  , ByVal E2J03 As String _
                                                  , ByVal E2J04 As String _
                                                  , ByVal NDHAC10 As String _
                                                  , ByVal NDHAC06 As String _
                                                  , ByVal NDHAC07 As String _
                                                  , ByVal NDHAC08 As String _
                                                  , ByVal NDINC02 As String _
                                                  , ByVal EA2L01 As String _
                                                  , ByVal EA2L02 As String _
                                                  , ByVal EA2L03 As String _
                                                  , ByVal NDFCS02 As String _
                                                  , ByVal EA2I04 As String _
                                                  , ByVal NDECC06 As String _
                                                  , ByVal NDINC03 As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_CAIS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@E1A04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A04").Value = E1A04

            cmdCommand.Parameters.Add("@E1A05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A05").Value = E1A05

            cmdCommand.Parameters.Add("@E1A06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A06").Value = E1A06

            cmdCommand.Parameters.Add("@E1A07", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A07").Value = E1A07

            cmdCommand.Parameters.Add("@E1A08", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A08").Value = E1A08

            cmdCommand.Parameters.Add("@E1A09", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A09").Value = E1A09

            cmdCommand.Parameters.Add("@E1A10", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A10").Value = E1A10

            cmdCommand.Parameters.Add("@E1A11", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1A11").Value = E1A11

            cmdCommand.Parameters.Add("@E1B01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B01").Value = E1B01

            cmdCommand.Parameters.Add("@E1B02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B02").Value = E1B02

            cmdCommand.Parameters.Add("@E1B03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B03").Value = E1B03

            cmdCommand.Parameters.Add("@E1B04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B04").Value = E1B04

            cmdCommand.Parameters.Add("@E1B05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B05").Value = E1B05

            cmdCommand.Parameters.Add("@E1B06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B06").Value = E1B06

            cmdCommand.Parameters.Add("@E1B07", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B07").Value = E1B07

            cmdCommand.Parameters.Add("@E1B08", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B08").Value = E1B08

            cmdCommand.Parameters.Add("@E1B09", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B09").Value = E1B09

            cmdCommand.Parameters.Add("@E1B10", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B10").Value = E1B10

            cmdCommand.Parameters.Add("@E1B11", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B11").Value = E1B11

            cmdCommand.Parameters.Add("@E1B12", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B12").Value = E1B12

            cmdCommand.Parameters.Add("@E1B13", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1B13").Value = E1B13

            cmdCommand.Parameters.Add("@NDECC01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC01").Value = NDECC01

            cmdCommand.Parameters.Add("@NDECC02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC02").Value = NDECC02

            cmdCommand.Parameters.Add("@NDECC03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC03").Value = NDECC03

            cmdCommand.Parameters.Add("@NDECC04", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC04").Value = NDECC04

            cmdCommand.Parameters.Add("@NDECC07", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC07").Value = NDECC07

            cmdCommand.Parameters.Add("@NDECC08", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC08").Value = NDECC08

            cmdCommand.Parameters.Add("@E1C01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C01").Value = E1C01

            cmdCommand.Parameters.Add("@E1C02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C02").Value = E1C02

            cmdCommand.Parameters.Add("@E1C03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C03").Value = E1C03

            cmdCommand.Parameters.Add("@E1C04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C04").Value = E1C04

            cmdCommand.Parameters.Add("@E1C05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C05").Value = E1C05

            cmdCommand.Parameters.Add("@E1C06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1C06").Value = E1C06

            cmdCommand.Parameters.Add("@EA1B02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1B02").Value = EA1B02

            cmdCommand.Parameters.Add("@E1D01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1D01").Value = E1D01

            cmdCommand.Parameters.Add("@E1D02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1D02").Value = E1D02

            cmdCommand.Parameters.Add("@E1D03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1D03").Value = E1D03

            cmdCommand.Parameters.Add("@E1D04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E1D04").Value = E1D04

            cmdCommand.Parameters.Add("@NDHAC01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC01").Value = NDHAC01

            cmdCommand.Parameters.Add("@NDHAC02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC02").Value = NDHAC02

            cmdCommand.Parameters.Add("@NDHAC03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC03").Value = NDHAC03

            cmdCommand.Parameters.Add("@NDHAC04", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC04").Value = NDHAC04

            cmdCommand.Parameters.Add("@NDHAC05", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC05").Value = NDHAC05

            cmdCommand.Parameters.Add("@NDHAC09", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC09").Value = NDHAC09

            cmdCommand.Parameters.Add("@NDINC01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDINC01").Value = NDINC01

            cmdCommand.Parameters.Add("@EA1F01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1F01").Value = EA1F01

            cmdCommand.Parameters.Add("@EA1F02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1F02").Value = EA1F02

            cmdCommand.Parameters.Add("@EA1F03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1F03").Value = EA1F03

            cmdCommand.Parameters.Add("@NDFCS01", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDFCS01").Value = NDFCS01

            cmdCommand.Parameters.Add("@EA1F04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA1F04").Value = EA1F04

            cmdCommand.Parameters.Add("@E2G04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G04").Value = E2G04

            cmdCommand.Parameters.Add("@E2G05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G05").Value = E2G05

            cmdCommand.Parameters.Add("@E2G06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G06").Value = E2G06

            cmdCommand.Parameters.Add("@E2G07", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G07").Value = E2G07

            cmdCommand.Parameters.Add("@E2G08", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G08").Value = E2G08

            cmdCommand.Parameters.Add("@E2G09", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G09").Value = E2G09

            cmdCommand.Parameters.Add("@E2G10", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G10").Value = E2G10

            cmdCommand.Parameters.Add("@E2G11", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2G11").Value = E2G11

            cmdCommand.Parameters.Add("@E2H01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H01").Value = E2H01

            cmdCommand.Parameters.Add("@E2H02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H02").Value = E2H02

            cmdCommand.Parameters.Add("@E2H03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H03").Value = E2H03

            cmdCommand.Parameters.Add("@E2H04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H04").Value = E2H04

            cmdCommand.Parameters.Add("@E2H05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H05").Value = E2H05

            cmdCommand.Parameters.Add("@E2H06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H06").Value = E2H06

            cmdCommand.Parameters.Add("@E2H07", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H07").Value = E2H07

            cmdCommand.Parameters.Add("@E2H08", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H08").Value = E2H08

            cmdCommand.Parameters.Add("@E2H09", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H09").Value = E2H09

            cmdCommand.Parameters.Add("@E2H10", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H10").Value = E2H10

            cmdCommand.Parameters.Add("@E2H11", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H11").Value = E2H11

            cmdCommand.Parameters.Add("@E2H12", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H12").Value = E2H12

            cmdCommand.Parameters.Add("@E2H13", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2H13").Value = E2H13

            cmdCommand.Parameters.Add("@NDECC05", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC05").Value = NDECC05

            cmdCommand.Parameters.Add("@NDECC09", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC09").Value = NDECC09

            cmdCommand.Parameters.Add("@NDECC10", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC10").Value = NDECC10

            cmdCommand.Parameters.Add("@E2I01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I01").Value = E2I01

            cmdCommand.Parameters.Add("@E2I02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I02").Value = E2I02

            cmdCommand.Parameters.Add("@E2I03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I03").Value = E2I03

            cmdCommand.Parameters.Add("@E2I04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I04").Value = E2I04

            cmdCommand.Parameters.Add("@E2I05", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I05").Value = E2I05

            cmdCommand.Parameters.Add("@E2I06", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2I06").Value = E2I06

            cmdCommand.Parameters.Add("@EA2H02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2H02").Value = EA2H02

            cmdCommand.Parameters.Add("@E2J01", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2J01").Value = E2J01

            cmdCommand.Parameters.Add("@E2J02", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2J02").Value = E2J02

            cmdCommand.Parameters.Add("@E2J03", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2J03").Value = E2J03

            cmdCommand.Parameters.Add("@E2J04", SqlDbType.VarChar)
            cmdCommand.Parameters("@E2J04").Value = E2J04

            cmdCommand.Parameters.Add("@NDHAC10", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC10").Value = NDHAC10

            cmdCommand.Parameters.Add("@NDHAC06", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC06").Value = NDHAC06

            cmdCommand.Parameters.Add("@NDHAC07", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC07").Value = NDHAC07

            cmdCommand.Parameters.Add("@NDHAC08", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDHAC08").Value = NDHAC08

            cmdCommand.Parameters.Add("@NDINC02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDINC02").Value = NDINC02

            cmdCommand.Parameters.Add("@EA2L01", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2L01").Value = EA2L01

            cmdCommand.Parameters.Add("@EA2L02", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2L02").Value = EA2L02

            cmdCommand.Parameters.Add("@EA2L03", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2L03").Value = EA2L03

            cmdCommand.Parameters.Add("@NDFCS02", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDFCS02").Value = NDFCS02

            cmdCommand.Parameters.Add("@EA2I04", SqlDbType.VarChar)
            cmdCommand.Parameters("@EA2I04").Value = EA2I04

            cmdCommand.Parameters.Add("@NDECC06", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDECC06").Value = NDECC06

            cmdCommand.Parameters.Add("@NDINC03", SqlDbType.VarChar)
            cmdCommand.Parameters("@NDINC03").Value = NDINC03

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_FULLCONDATA_CDS_SD_CAISSUMMARY(ByVal ApplicationID As Integer _
                                                , ByVal Num3To6Months As String _
                                                , ByVal Num6To12Months As String _
                                                , ByVal NumberUpTo3Months As String _
                                                , ByVal TotalNumber As String _
                                                , ByVal GoneAway As String _
                                                , ByVal Number As String _
                                                , ByVal TotalBalance As String _
                                                , ByVal WorstCurrent As String _
                                                , ByVal WorstHistorical As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_FULLCONDATA_CDS_SD_CAISSUMMARY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@Num3To6Months", SqlDbType.VarChar)
            cmdCommand.Parameters("@Num3To6Months").Value = Num3To6Months

            cmdCommand.Parameters.Add("@Num6To12Months", SqlDbType.VarChar)
            cmdCommand.Parameters("@Num6To12Months").Value = Num6To12Months

            cmdCommand.Parameters.Add("@NumberUpTo3Months", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumberUpTo3Months").Value = NumberUpTo3Months

            cmdCommand.Parameters.Add("@TotalNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@TotalNumber").Value = TotalNumber

            cmdCommand.Parameters.Add("@GoneAway", SqlDbType.VarChar)
            cmdCommand.Parameters("@GoneAway").Value = GoneAway

            cmdCommand.Parameters.Add("@Number", SqlDbType.VarChar)
            cmdCommand.Parameters("@Number").Value = Number

            cmdCommand.Parameters.Add("@TotalBalance", SqlDbType.VarChar)
            cmdCommand.Parameters("@TotalBalance").Value = TotalBalance

            cmdCommand.Parameters.Add("@WorstCurrent", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorstCurrent").Value = WorstCurrent

            cmdCommand.Parameters.Add("@WorstHistorical", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorstHistorical").Value = WorstHistorical

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_FULLCONDATA_CDS_SD_SUMMARYFLAGS(ByVal ApplicationID As Integer _
                                                , ByVal AccountQuery As String _
                                                , ByVal AdditionalLocation As String _
                                                , ByVal Arrangement As String _
                                                , ByVal CIFAS As String _
                                                , ByVal ClaimedOnCreditIns As String _
                                                , ByVal CML As String _
                                                , ByVal DebtAssign As String _
                                                , ByVal DebtManagement As String _
                                                , ByVal ForwardLocation As String _
                                                , ByVal NoConfirmAtPrevLoc As String _
                                                , ByVal OwnAccount As String _
                                                , ByVal OwnSearch As String _
                                                , ByVal PartSettle As String _
                                                , ByVal PreviousLocation As String _
                                                , ByVal PreviousOccupant As String _
                                                , ByVal Recourse As String _
                                                , ByVal ReportedDeceased As String _
                                                , ByVal ReportedGoneAway As String _
                                                , ByVal VoluntaryTerm As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_FULLCONDATA_CDS_SD_SUMMARYFLAGS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@AccountQuery", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountQuery").Value = AccountQuery

            cmdCommand.Parameters.Add("@AdditionalLocation", SqlDbType.VarChar)
            cmdCommand.Parameters("@AdditionalLocation").Value = AdditionalLocation

            cmdCommand.Parameters.Add("@Arrangement", SqlDbType.VarChar)
            cmdCommand.Parameters("@Arrangement").Value = Arrangement

            cmdCommand.Parameters.Add("@CIFAS", SqlDbType.VarChar)
            cmdCommand.Parameters("@CIFAS").Value = CIFAS

            cmdCommand.Parameters.Add("@ClaimedOnCreditIns", SqlDbType.VarChar)
            cmdCommand.Parameters("@ClaimedOnCreditIns").Value = ClaimedOnCreditIns

            cmdCommand.Parameters.Add("@CML", SqlDbType.VarChar)
            cmdCommand.Parameters("@CML").Value = CML

            cmdCommand.Parameters.Add("@DebtAssign", SqlDbType.VarChar)
            cmdCommand.Parameters("@DebtAssign").Value = DebtAssign

            cmdCommand.Parameters.Add("@DebtManagement", SqlDbType.VarChar)
            cmdCommand.Parameters("@DebtManagement").Value = DebtManagement

            cmdCommand.Parameters.Add("@ForwardLocation", SqlDbType.VarChar)
            cmdCommand.Parameters("@ForwardLocation").Value = ForwardLocation

            cmdCommand.Parameters.Add("@NoConfirmAtPrevLoc", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoConfirmAtPrevLoc").Value = NoConfirmAtPrevLoc

            cmdCommand.Parameters.Add("@OwnAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@OwnAccount").Value = OwnAccount

            cmdCommand.Parameters.Add("@OwnSearch", SqlDbType.VarChar)
            cmdCommand.Parameters("@OwnSearch").Value = OwnSearch

            cmdCommand.Parameters.Add("@PartSettle", SqlDbType.VarChar)
            cmdCommand.Parameters("@PartSettle").Value = PartSettle

            cmdCommand.Parameters.Add("@PreviousLocation", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousLocation").Value = PreviousLocation

            cmdCommand.Parameters.Add("@PreviousOccupant", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousOccupant").Value = PreviousOccupant

            cmdCommand.Parameters.Add("@Recourse", SqlDbType.VarChar)
            cmdCommand.Parameters("@Recourse").Value = Recourse

            cmdCommand.Parameters.Add("@ReportedDeceased", SqlDbType.VarChar)
            cmdCommand.Parameters("@ReportedDeceased").Value = ReportedDeceased

            cmdCommand.Parameters.Add("@ReportedGoneAway", SqlDbType.VarChar)
            cmdCommand.Parameters("@ReportedGoneAway").Value = ReportedGoneAway

            cmdCommand.Parameters.Add("@VoluntaryTerm", SqlDbType.VarChar)
            cmdCommand.Parameters("@VoluntaryTerm").Value = VoluntaryTerm


            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub DeleteConsumerData_FULLCONDATA_CD_ASSOCIATION(ByVal ApplicationID As Integer)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_DELETE_CONSUMERDATA_FULLCONDATA_CD_ASSOCIATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_FULLCONDATA_CD_ASSOCIATION(ByVal ApplicationID As Integer _
                                                , ByVal ApplicantIndicator As String _
                                                  , ByVal LocationIndicator As String _
                                                  , ByVal AssociateName_Forename As String _
                                                  , ByVal AssociateName_MiddleName As String _
                                                  , ByVal AssociateName_Suffix As String _
                                                  , ByVal AssociateName_Surname As String _
                                                  , ByVal AssociateName_Title As String _
                                                  , ByVal CompanyType As String _
                                                  , ByVal DateOfBirth_CCYY As String _
                                                  , ByVal DateOfBirth_DD As String _
                                                  , ByVal DateOfBirth_MM As String _
                                                  , ByVal DoBAssociateOrAlias_CCYY As String _
                                                  , ByVal DoBAssociateOrAlias_DD As String _
                                                  , ByVal DoBAssociateOrAlias_MM As String _
                                                  , ByVal InformationDate_CCYY As String _
                                                  , ByVal InformationDate_DD As String _
                                                  , ByVal InformationDate_MM As String _
                                                  , ByVal InformationSource As String _
                                                  , ByVal InformationSupplier As String _
                                                  , ByVal InformationType As String _
                                                  , ByVal Location_Country As String _
                                                  , ByVal Location_County As String _
                                                  , ByVal Location_District As String _
                                                  , ByVal Location_District2 As String _
                                                  , ByVal Location_Flat As String _
                                                  , ByVal Location_HouseName As String _
                                                  , ByVal Location_HouseNumber As String _
                                                  , ByVal Location_POBox As String _
                                                  , ByVal Location_Postcode As String _
                                                  , ByVal Location_PostTown As String _
                                                  , ByVal Location_SharedLetterbox As String _
                                                  , ByVal Location_Street As String _
                                                  , ByVal Location_Street2 As String _
                                                  , ByVal MatchDetails_BureauRefCategory As String _
                                                  , ByVal MatchDetails_HouseMatchLevel As String _
                                                  , ByVal MatchDetails_MatchTo As String _
                                                  , ByVal MatchDetails_MatchType As String _
                                                  , ByVal MatchDetails_StreetMatchLevel As String _
                                                  , ByVal Name_Forename As String _
                                                  , ByVal Name_MiddleName As String _
                                                  , ByVal Name_Prefix As String _
                                                  , ByVal Name_Suffix As String _
                                                  , ByVal Name_Surname As String _
                                                  , ByVal Name_Title As String _
                                                  , ByVal NoCReference As String _
                                                  , ByVal Source As String _
                                                  , ByVal SupplierBranch As String _
                                                  , ByVal SupplyCompanyName As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_FULLCONDATA_CD_ASSOCIATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicantIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicantIndicator").Value = ApplicantIndicator

            cmdCommand.Parameters.Add("@LocationIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@LocationIndicator").Value = LocationIndicator

            cmdCommand.Parameters.Add("@AssociateName_Forename", SqlDbType.VarChar)
            cmdCommand.Parameters("@AssociateName_Forename").Value = AssociateName_Forename

            cmdCommand.Parameters.Add("@AssociateName_MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@AssociateName_MiddleName").Value = AssociateName_MiddleName

            cmdCommand.Parameters.Add("@AssociateName_Suffix", SqlDbType.VarChar)
            cmdCommand.Parameters("@AssociateName_Suffix").Value = AssociateName_Suffix

            cmdCommand.Parameters.Add("@AssociateName_Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@AssociateName_Surname").Value = AssociateName_Surname

            cmdCommand.Parameters.Add("@AssociateName_Title", SqlDbType.VarChar)
            cmdCommand.Parameters("@AssociateName_Title").Value = AssociateName_Title

            cmdCommand.Parameters.Add("@CompanyType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyType").Value = CompanyType

            cmdCommand.Parameters.Add("@DateOfBirth_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_CCYY").Value = DateOfBirth_CCYY

            cmdCommand.Parameters.Add("@DateOfBirth_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_DD").Value = DateOfBirth_DD

            cmdCommand.Parameters.Add("@DateOfBirth_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_MM").Value = DateOfBirth_MM

            cmdCommand.Parameters.Add("@DoBAssociateOrAlias_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@DoBAssociateOrAlias_CCYY").Value = DoBAssociateOrAlias_CCYY

            cmdCommand.Parameters.Add("@DoBAssociateOrAlias_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@DoBAssociateOrAlias_DD").Value = DoBAssociateOrAlias_DD

            cmdCommand.Parameters.Add("@DoBAssociateOrAlias_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@DoBAssociateOrAlias_MM").Value = DoBAssociateOrAlias_MM

            cmdCommand.Parameters.Add("@InformationDate_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationDate_CCYY").Value = InformationDate_CCYY

            cmdCommand.Parameters.Add("@InformationDate_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationDate_DD").Value = InformationDate_DD

            cmdCommand.Parameters.Add("@InformationDate_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationDate_MM").Value = InformationDate_MM

            cmdCommand.Parameters.Add("@InformationSource", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationSource").Value = InformationSource

            cmdCommand.Parameters.Add("@InformationSupplier", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationSupplier").Value = InformationSupplier

            cmdCommand.Parameters.Add("@InformationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationType").Value = InformationType

            cmdCommand.Parameters.Add("@Location_Country", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Country").Value = Location_Country

            cmdCommand.Parameters.Add("@Location_County", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_County").Value = Location_County

            cmdCommand.Parameters.Add("@Location_District", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_District").Value = Location_District

            cmdCommand.Parameters.Add("@Location_District2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_District2").Value = Location_District2

            cmdCommand.Parameters.Add("@Location_Flat", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Flat").Value = Location_Flat

            cmdCommand.Parameters.Add("@Location_HouseName", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_HouseName").Value = Location_HouseName

            cmdCommand.Parameters.Add("@Location_HouseNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_HouseNumber").Value = Location_HouseNumber

            cmdCommand.Parameters.Add("@Location_POBox", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_POBox").Value = Location_POBox

            cmdCommand.Parameters.Add("@Location_Postcode", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Postcode").Value = Location_Postcode

            cmdCommand.Parameters.Add("@Location_PostTown", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_PostTown").Value = Location_PostTown

            cmdCommand.Parameters.Add("@Location_SharedLetterbox", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_SharedLetterbox").Value = Location_SharedLetterbox

            cmdCommand.Parameters.Add("@Location_Street", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Street").Value = Location_Street

            cmdCommand.Parameters.Add("@Location_Street2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Street2").Value = Location_Street2

            cmdCommand.Parameters.Add("@MatchDetails_BureauRefCategory", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_BureauRefCategory").Value = MatchDetails_BureauRefCategory

            cmdCommand.Parameters.Add("@MatchDetails_HouseMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_HouseMatchLevel").Value = MatchDetails_HouseMatchLevel

            cmdCommand.Parameters.Add("@MatchDetails_MatchTo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchTo").Value = MatchDetails_MatchTo

            cmdCommand.Parameters.Add("@MatchDetails_MatchType", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchType").Value = MatchDetails_MatchType

            cmdCommand.Parameters.Add("@MatchDetails_StreetMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_StreetMatchLevel").Value = MatchDetails_StreetMatchLevel

            cmdCommand.Parameters.Add("@Name_Forename", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Forename").Value = Name_Forename

            cmdCommand.Parameters.Add("@Name_MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_MiddleName").Value = Name_MiddleName

            cmdCommand.Parameters.Add("@Name_Prefix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Prefix").Value = Name_Prefix

            cmdCommand.Parameters.Add("@Name_Suffix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Suffix").Value = Name_Suffix

            cmdCommand.Parameters.Add("@Name_Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Surname").Value = Name_Surname

            cmdCommand.Parameters.Add("@Name_Title", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Title").Value = Name_Title

            cmdCommand.Parameters.Add("@NoCReference", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoCReference").Value = NoCReference

            cmdCommand.Parameters.Add("@Source", SqlDbType.VarChar)
            cmdCommand.Parameters("@Source").Value = Source

            cmdCommand.Parameters.Add("@SupplierBranch", SqlDbType.VarChar)
            cmdCommand.Parameters("@SupplierBranch").Value = SupplierBranch

            cmdCommand.Parameters.Add("@SupplyCompanyName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SupplyCompanyName").Value = SupplyCompanyName
            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub DeleteConsumerData_FULLCONDATA_CD_CAIS(ByVal ApplicationID As Integer)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_DELETE_CONSUMERDATA_FULLCONDATA_CD_CAIS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_FULLCONDATA_CD_CAIS(ByVal ApplicationID As Integer _
                                                , ByVal ApplicantIndicator As String _
                                                , ByVal LocationIndicator As String _
                                                , ByVal AccountBalancesCount As Integer _
                                                , ByVal AccountBalance As String _
                                                , ByVal Status As String _
                                                , ByVal AccountNumber As String _
                                                , ByVal AccountStatus As String _
                                                , ByVal AccountStatusCodes As String _
                                                , ByVal AccountType As String _
                                                , ByVal Balance_Amount As String _
                                                , ByVal Balance_Caption As String _
                                                , ByVal Balance_Narrative As String _
                                                , ByVal BankFlag As String _
                                                , ByVal CAISAccStartDate_CCYY As String _
                                                , ByVal CAISAccStartDate_DD As String _
                                                , ByVal CAISAccStartDate_MM As String _
                                                , ByVal CMLLocationType As String _
                                                , ByVal CompanyType As String _
                                                , ByVal CreditLimit_Amount As String _
                                                , ByVal CreditLimit_Caption As String _
                                                , ByVal CreditLimit_Narrative As String _
                                                , ByVal CurrentDefBalance_Amount As String _
                                                , ByVal CurrentDefBalance_Narrative As String _
                                                , ByVal DelinquentBalance_Amount As String _
                                                , ByVal DelinquentBalance_Narrative As String _
                                                , ByVal FSCCUGIndicator As String _
                                                , ByVal InformationSource As String _
                                                , ByVal JointAccount As String _
                                                , ByVal LastUpdatedDate_CCYY As String _
                                                , ByVal LastUpdatedDate_DD As String _
                                                , ByVal LastUpdatedDate_MM As String _
                                                , ByVal MatchDetails_BureauRefCategory As String _
                                                , ByVal MatchDetails_ForenameMatchLevel As String _
                                                , ByVal MatchDetails_HouseMatchLevel As String _
                                                , ByVal MatchDetails_MatchTo As String _
                                                , ByVal MatchDetails_MatchType As String _
                                                , ByVal MatchDetails_StreetMatchLevel As String _
                                                , ByVal MatchDetails_SurnameMatchLevel As String _
                                                , ByVal MnthlyRepaymentCount As Integer _
                                                , ByVal MnthlyRepayment As String _
                                                , ByVal MnthlyRepyChngDate As String _
                                                , ByVal Name_Forename As String _
                                                , ByVal Name_MiddleName As String _
                                                , ByVal Name_Prefix As String _
                                                , ByVal Name_Suffix As String _
                                                , ByVal Name_Surname As String _
                                                , ByVal Name_Title As String _
                                                , ByVal NoCReference As String _
                                                , ByVal NoOfPayments As String _
                                                , ByVal NoOfRepaymntPeriod As String _
                                                , ByVal NumAccountBalances As String _
                                                , ByVal NumAddInfoBlocks As String _
                                                , ByVal NumCardHistories As String _
                                                , ByVal NumCreditLimChngs As String _
                                                , ByVal NumOfMonthsHistory As String _
                                                , ByVal NumStatuses As String _
                                                , ByVal OwnData As String _
                                                , ByVal Payment As String _
                                                , ByVal PaymentFrequency As String _
                                                , ByVal RepaymentPeriodsCount As Integer _
                                                , ByVal RepaymentPeriod As String _
                                                , ByVal RepayPdChangeDate As String _
                                                , ByVal SettleDateCaption As String _
                                                , ByVal SettlementDate_CCYY As String _
                                                , ByVal SettlementDate_MM As String _
                                                , ByVal SettlementDate_DD As String _
                                                , ByVal Sex As String _
                                                , ByVal SourceCode As String _
                                                , ByVal Status1To2 As String _
                                                , ByVal StatusTo3 As String _
                                                , ByVal SupplyCompanyName As String _
                                                , ByVal TransColnAcntFlag As String _
                                                , ByVal WorstStatus As String _
       )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_FULLCONDATA_CD_CAIS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicantIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicantIndicator").Value = ApplicantIndicator

            cmdCommand.Parameters.Add("@LocationIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@LocationIndicator").Value = LocationIndicator

            cmdCommand.Parameters.Add("@AccountBalancesCount", SqlDbType.Int)
            cmdCommand.Parameters("@AccountBalancesCount").Value = AccountBalancesCount

            cmdCommand.Parameters.Add("@Status", SqlDbType.VarChar)
            cmdCommand.Parameters("@Status").Value = Status

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@AccountStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountStatus").Value = AccountStatus

            cmdCommand.Parameters.Add("@AccountStatusCodes", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountStatusCodes").Value = AccountStatusCodes

            cmdCommand.Parameters.Add("@AccountType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountType").Value = AccountType

            cmdCommand.Parameters.Add("@Balance_Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@Balance_Amount").Value = Balance_Amount

            cmdCommand.Parameters.Add("@Balance_Caption", SqlDbType.VarChar)
            cmdCommand.Parameters("@Balance_Caption").Value = Balance_Caption

            cmdCommand.Parameters.Add("@Balance_Narrative", SqlDbType.VarChar)
            cmdCommand.Parameters("@Balance_Narrative").Value = Balance_Narrative

            cmdCommand.Parameters.Add("@BankFlag", SqlDbType.VarChar)
            cmdCommand.Parameters("@BankFlag").Value = BankFlag

            cmdCommand.Parameters.Add("@CAISAccStartDate_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@CAISAccStartDate_CCYY").Value = CAISAccStartDate_CCYY

            cmdCommand.Parameters.Add("@CAISAccStartDate_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@CAISAccStartDate_DD").Value = CAISAccStartDate_DD

            cmdCommand.Parameters.Add("@CAISAccStartDate_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@CAISAccStartDate_MM").Value = CAISAccStartDate_MM

            cmdCommand.Parameters.Add("@CMLLocationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CMLLocationType").Value = CMLLocationType

            cmdCommand.Parameters.Add("@CompanyType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyType").Value = CompanyType

            cmdCommand.Parameters.Add("@CreditLimit_Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreditLimit_Amount").Value = CreditLimit_Amount

            cmdCommand.Parameters.Add("@CreditLimit_Caption", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreditLimit_Caption").Value = CreditLimit_Caption

            cmdCommand.Parameters.Add("@CreditLimit_Narrative", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreditLimit_Narrative").Value = CreditLimit_Narrative

            cmdCommand.Parameters.Add("@CurrentDefBalance_Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentDefBalance_Amount").Value = CurrentDefBalance_Amount

            cmdCommand.Parameters.Add("@CurrentDefBalance_Narrative", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentDefBalance_Narrative").Value = CurrentDefBalance_Narrative

            cmdCommand.Parameters.Add("@DelinquentBalance_Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@DelinquentBalance_Amount").Value = DelinquentBalance_Amount

            cmdCommand.Parameters.Add("@DelinquentBalance_Narrative", SqlDbType.VarChar)
            cmdCommand.Parameters("@DelinquentBalance_Narrative").Value = DelinquentBalance_Narrative

            cmdCommand.Parameters.Add("@FSCCUGIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@FSCCUGIndicator").Value = FSCCUGIndicator

            cmdCommand.Parameters.Add("@InformationSource", SqlDbType.VarChar)
            cmdCommand.Parameters("@InformationSource").Value = InformationSource

            cmdCommand.Parameters.Add("@JointAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@JointAccount").Value = JointAccount

            cmdCommand.Parameters.Add("@LastUpdatedDate_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@LastUpdatedDate_CCYY").Value = LastUpdatedDate_CCYY

            cmdCommand.Parameters.Add("@LastUpdatedDate_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@LastUpdatedDate_DD").Value = LastUpdatedDate_DD

            cmdCommand.Parameters.Add("@LastUpdatedDate_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@LastUpdatedDate_MM").Value = LastUpdatedDate_MM

            cmdCommand.Parameters.Add("@MatchDetails_BureauRefCategory", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_BureauRefCategory").Value = MatchDetails_BureauRefCategory

            cmdCommand.Parameters.Add("@MatchDetails_ForenameMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_ForenameMatchLevel").Value = MatchDetails_ForenameMatchLevel

            cmdCommand.Parameters.Add("@MatchDetails_HouseMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_HouseMatchLevel").Value = MatchDetails_HouseMatchLevel

            cmdCommand.Parameters.Add("@MatchDetails_MatchTo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchTo").Value = MatchDetails_MatchTo

            cmdCommand.Parameters.Add("@MatchDetails_MatchType", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchType").Value = MatchDetails_MatchType

            cmdCommand.Parameters.Add("@MatchDetails_StreetMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_StreetMatchLevel").Value = MatchDetails_StreetMatchLevel

            cmdCommand.Parameters.Add("@MatchDetails_SurnameMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_SurnameMatchLevel").Value = MatchDetails_SurnameMatchLevel

            cmdCommand.Parameters.Add("@MnthlyRepaymentCount", SqlDbType.Int)
            cmdCommand.Parameters("@MnthlyRepaymentCount").Value = MnthlyRepaymentCount

            cmdCommand.Parameters.Add("@MnthlyRepayment", SqlDbType.VarChar)
            cmdCommand.Parameters("@MnthlyRepayment").Value = MnthlyRepayment

            cmdCommand.Parameters.Add("@MnthlyRepyChngDate", SqlDbType.VarChar)
            cmdCommand.Parameters("@MnthlyRepyChngDate").Value = MnthlyRepyChngDate

            cmdCommand.Parameters.Add("@Name_Forename", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Forename").Value = Name_Forename

            cmdCommand.Parameters.Add("@Name_MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_MiddleName").Value = Name_MiddleName

            cmdCommand.Parameters.Add("@Name_Prefix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Prefix").Value = Name_Prefix

            cmdCommand.Parameters.Add("@Name_Suffix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Suffix").Value = Name_Suffix

            cmdCommand.Parameters.Add("@Name_Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Surname").Value = Name_Surname

            cmdCommand.Parameters.Add("@Name_Title", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Title").Value = Name_Title

            cmdCommand.Parameters.Add("@NoCReference", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoCReference").Value = NoCReference

            cmdCommand.Parameters.Add("@NoOfPayments", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoOfPayments").Value = NoOfPayments

            cmdCommand.Parameters.Add("@NoOfRepaymntPeriod", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoOfRepaymntPeriod").Value = NoOfRepaymntPeriod

            cmdCommand.Parameters.Add("@NumAccountBalances", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumAccountBalances").Value = NumAccountBalances

            cmdCommand.Parameters.Add("@NumAddInfoBlocks", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumAddInfoBlocks").Value = NumAddInfoBlocks

            cmdCommand.Parameters.Add("@NumCardHistories", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumCardHistories").Value = NumCardHistories

            cmdCommand.Parameters.Add("@NumCreditLimChngs", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumCreditLimChngs").Value = NumCreditLimChngs

            cmdCommand.Parameters.Add("@NumOfMonthsHistory", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumOfMonthsHistory").Value = NumOfMonthsHistory

            cmdCommand.Parameters.Add("@NumStatuses", SqlDbType.VarChar)
            cmdCommand.Parameters("@NumStatuses").Value = NumStatuses

            cmdCommand.Parameters.Add("@OwnData", SqlDbType.VarChar)
            cmdCommand.Parameters("@OwnData").Value = OwnData

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub DeleteConsumerData_FULLCONDATA_CD_PreviousApplication(ByVal ApplicationID As Integer)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_DELETE_CONSUMERDATA_FULLCONDATA_CD_PreviousApplication", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertConsumerData_FULLCONDATA_CD_PreviousApplication(ByVal ApplicationID As Integer _
                                                , ByVal ApplicantIndicator As String _
                                                , ByVal LocationIndicator As String _
                                                , ByVal AccountNumber As String _
                                                , ByVal Amount As String _
                                                , ByVal ApplicationDate_CCYY As String _
                                                , ByVal ApplicationDate_DD As String _
                                                , ByVal ApplicationDate_MM As String _
                                                , ByVal ApplicationType As String _
                                                , ByVal CompanyType As String _
                                                , ByVal DateOfBirth_CCYY As String _
                                                , ByVal DateOfBirth_DD As String _
                                                , ByVal DateOfBirth_MM As String _
                                                , ByVal JointApplicant As String _
                                                , ByVal Location_Country As String _
                                                , ByVal Location_County As String _
                                                , ByVal Location_District As String _
                                                , ByVal Location_District2 As String _
                                                , ByVal Location_Flat As String _
                                                , ByVal Location_HouseName As String _
                                                , ByVal Location_HouseNumber As String _
                                                , ByVal Location_POBox As String _
                                                , ByVal Location_Postcode As String _
                                                , ByVal Location_PostTown As String _
                                                , ByVal Location_SharedLetterbox As String _
                                                , ByVal Location_Street As String _
                                                , ByVal Location_Street2 As String _
                                                , ByVal MatchDetails_BureauRefCategory As String _
                                                , ByVal MatchDetails_HouseMatchLevel As String _
                                                , ByVal MatchDetails_MatchTo As String _
                                                , ByVal MatchDetails_MatchType As String _
                                                , ByVal MatchDetails_StreetMatchLevel As String _
                                                , ByVal Name_Forename As String _
                                                , ByVal Name_MiddleName As String _
                                                , ByVal Name_Prefix As String _
                                                , ByVal Name_Suffix As String _
                                                , ByVal Name_Surname As String _
                                                , ByVal Name_Title As String _
                                                , ByVal NoCReference As String _
                                                , ByVal OptOut As String _
                                                , ByVal OwnAccountID As String _
                                                , ByVal SearcherName As String _
                                                , ByVal SupplyCompanyName As String _
                                                , ByVal Term As String _
                                                , ByVal TimeAtLocation_Months As String _
                                                , ByVal TimeAtLocation_Years As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_CONSUMERDATA_FULLCONDATA_CD_PreviousApplication", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicantIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicantIndicator").Value = ApplicantIndicator

            cmdCommand.Parameters.Add("@LocationIndicator", SqlDbType.VarChar)
            cmdCommand.Parameters("@LocationIndicator").Value = LocationIndicator

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@ApplicationDate_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationDate_CCYY").Value = ApplicationDate_CCYY

            cmdCommand.Parameters.Add("@ApplicationDate_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationDate_DD").Value = ApplicationDate_DD

            cmdCommand.Parameters.Add("@ApplicationDate_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationDate_MM").Value = ApplicationDate_MM

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            cmdCommand.Parameters.Add("@CompanyType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyType").Value = CompanyType

            cmdCommand.Parameters.Add("@DateOfBirth_CCYY", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_CCYY").Value = DateOfBirth_CCYY

            cmdCommand.Parameters.Add("@DateOfBirth_DD", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_DD").Value = DateOfBirth_DD

            cmdCommand.Parameters.Add("@DateOfBirth_MM", SqlDbType.VarChar)
            cmdCommand.Parameters("@DateOfBirth_MM").Value = DateOfBirth_MM

            cmdCommand.Parameters.Add("@JointApplicant", SqlDbType.VarChar)
            cmdCommand.Parameters("@JointApplicant").Value = JointApplicant

            cmdCommand.Parameters.Add("@Location_Country", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Country").Value = Location_Country

            cmdCommand.Parameters.Add("@Location_County", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_County").Value = Location_County

            cmdCommand.Parameters.Add("@Location_District", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_District").Value = Location_District

            cmdCommand.Parameters.Add("@Location_District2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_District2").Value = Location_District2

            cmdCommand.Parameters.Add("@Location_Flat", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Flat").Value = Location_Flat

            cmdCommand.Parameters.Add("@Location_HouseName", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_HouseName").Value = Location_HouseName

            cmdCommand.Parameters.Add("@Location_HouseNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_HouseNumber").Value = Location_HouseNumber

            cmdCommand.Parameters.Add("@Location_POBox", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_POBox").Value = Location_POBox

            cmdCommand.Parameters.Add("@Location_Postcode", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Postcode").Value = Location_Postcode

            cmdCommand.Parameters.Add("@Location_PostTown", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_PostTown").Value = Location_PostTown

            cmdCommand.Parameters.Add("@Location_SharedLetterbox", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_SharedLetterbox").Value = Location_SharedLetterbox

            cmdCommand.Parameters.Add("@Location_Street", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Street").Value = Location_Street

            cmdCommand.Parameters.Add("@Location_Street2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Location_Street2").Value = Location_Street2

            cmdCommand.Parameters.Add("@MatchDetails_BureauRefCategory", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_BureauRefCategory").Value = MatchDetails_BureauRefCategory

            cmdCommand.Parameters.Add("@MatchDetails_HouseMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_HouseMatchLevel").Value = MatchDetails_HouseMatchLevel

            cmdCommand.Parameters.Add("@MatchDetails_MatchTo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchTo").Value = MatchDetails_MatchTo

            cmdCommand.Parameters.Add("@MatchDetails_MatchType", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_MatchType").Value = MatchDetails_MatchType

            cmdCommand.Parameters.Add("@MatchDetails_StreetMatchLevel", SqlDbType.VarChar)
            cmdCommand.Parameters("@MatchDetails_StreetMatchLevel").Value = MatchDetails_StreetMatchLevel

            cmdCommand.Parameters.Add("@Name_Forename", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Forename").Value = Name_Forename

            cmdCommand.Parameters.Add("@Name_MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_MiddleName").Value = Name_MiddleName

            cmdCommand.Parameters.Add("@Name_Prefix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Prefix").Value = Name_Prefix

            cmdCommand.Parameters.Add("@Name_Suffix", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Suffix").Value = Name_Suffix

            cmdCommand.Parameters.Add("@Name_Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Surname").Value = Name_Surname

            cmdCommand.Parameters.Add("@Name_Title", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name_Title").Value = Name_Title

            cmdCommand.Parameters.Add("@NoCReference", SqlDbType.VarChar)
            cmdCommand.Parameters("@NoCReference").Value = NoCReference

            cmdCommand.Parameters.Add("@OptOut", SqlDbType.VarChar)
            cmdCommand.Parameters("@OptOut").Value = OptOut

            cmdCommand.Parameters.Add("@OwnAccountID", SqlDbType.VarChar)
            cmdCommand.Parameters("@OwnAccountID").Value = OwnAccountID

            cmdCommand.Parameters.Add("@SearcherName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SearcherName").Value = SearcherName

            cmdCommand.Parameters.Add("@SupplyCompanyName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SupplyCompanyName").Value = SupplyCompanyName

            cmdCommand.Parameters.Add("@Term", SqlDbType.VarChar)
            cmdCommand.Parameters("@Term").Value = Term

            cmdCommand.Parameters.Add("@TimeAtLocation_Months", SqlDbType.VarChar)
            cmdCommand.Parameters("@TimeAtLocation_Months").Value = TimeAtLocation_Months

            cmdCommand.Parameters.Add("@TimeAtLocation_Years", SqlDbType.VarChar)
            cmdCommand.Parameters("@TimeAtLocation_Years").Value = TimeAtLocation_Years

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "Consumer Credit Search Reading Proc Calls"
    Public Function GetAllConsumerDataSerachTables(ByVal ApplicationID As Integer) As DataSet
        Try
            Dim cmdCommand As New SqlCommand
            Dim ds As DataSet = New DataSet

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_SELECT_CONSUMERDATA_ALL_TABLES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(ds)

            Return ds
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Bank Wizard"
    Public Sub InsertBankWizard_Verify(ByVal ApplicationID As Integer _
                                        , ByVal ConditionCount As Integer _
                                        , ByVal VerificationStatus As String _
                                        , ByVal BacsCode As String _
                                        , ByVal DataAccessKey As String _
                                        , ByVal PersonalDetailsScore As Integer _
                                        , ByVal AddressScore As Integer _
                                        , ByVal AccountTypeMatch As String _
                                        , ByVal AccountSetupDateScore As Integer _
                                        , ByVal AccountSetupDateMatch As String _
                                        , ByVal AccountOwnerMatch As String _
                                        , Optional ApplicationType As String = "PDL")

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_BANKWIZARD_VERIFY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ConditionCount", SqlDbType.Int)
            cmdCommand.Parameters("@ConditionCount").Value = ConditionCount

            cmdCommand.Parameters.Add("@VerificationStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@VerificationStatus").Value = VerificationStatus

            cmdCommand.Parameters.Add("@BacsCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@BacsCode").Value = BacsCode

            cmdCommand.Parameters.Add("@DataAccessKey", SqlDbType.VarChar)
            cmdCommand.Parameters("@DataAccessKey").Value = DataAccessKey

            cmdCommand.Parameters.Add("@PersonalDetailsScore", SqlDbType.Int)
            cmdCommand.Parameters("@PersonalDetailsScore").Value = PersonalDetailsScore

            cmdCommand.Parameters.Add("@AddressScore", SqlDbType.Int)
            cmdCommand.Parameters("@AddressScore").Value = AddressScore

            cmdCommand.Parameters.Add("@AccountTypeMatch", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountTypeMatch").Value = AccountTypeMatch

            cmdCommand.Parameters.Add("@AccountSetupDateScore", SqlDbType.Int)
            cmdCommand.Parameters("@AccountSetupDateScore").Value = AccountSetupDateScore

            cmdCommand.Parameters.Add("@AccountSetupDateMatch", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountSetupDateMatch").Value = AccountSetupDateMatch

            cmdCommand.Parameters.Add("@AccountOwnerMatch", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountOwnerMatch").Value = AccountOwnerMatch

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType
            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Sub InsertBankWizard_VerifyCondition(ByVal ApplicationID As Integer _
                                        , ByVal ErrorCode As Integer _
                                        , ByVal Sevirity As String _
                                        , ByVal Value As String _
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_INSERT_BANKWIZARD_VERIFYCONDITION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ErrorCode", SqlDbType.Int)
            cmdCommand.Parameters("@ErrorCode").Value = ErrorCode

            cmdCommand.Parameters.Add("@Sevirity", SqlDbType.VarChar)
            cmdCommand.Parameters("@Sevirity").Value = Sevirity

            cmdCommand.Parameters.Add("@Value", SqlDbType.VarChar)
            cmdCommand.Parameters("@Value").Value = Value

            'Execute
            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Sub

    Public Function GetBankCheckData(ByVal ApplicationID As Integer) As DataSet
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataSet = New DataSet

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_EXPERIAN_SELECT_BANKWIZARD_VERIFY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        clsConn = Nothing
        objCon = Nothing
    End Sub
End Class
