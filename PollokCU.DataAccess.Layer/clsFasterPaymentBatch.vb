﻿Imports System.Data.SqlClient
Imports System.Data

Public Class clsFasterPaymentBatch
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Get Faster Payment File Details"
    Public Function GetAllFasterPaymentFiles() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_FASTERPAYMENT_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Update Faster Payment File Details"
    Public Function UpdateFasterPaymentFiles(ByVal BatchID As Integer, ByVal StaffID As Integer)
        Try
            Dim cmdCommand As New SqlCommand
            'Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_FASTERPAYMENT_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@BatchID", SqlDbType.Int)
            cmdCommand.Parameters("@BatchID").Value = BatchID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Get Faster Payment Pending Details"
    Public Function GetAllPendingPaymentFiles() As DataTable

        Try

            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_FASTER_PAYMENTS_TO_PAY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@UpdateRunTime", SqlDbType.Bit)
            cmdCommand.Parameters("@UpdateRunTime").Value = 0

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

End Class
