﻿Imports System.Data.SqlClient
Imports System.Data
Public Class clsFundTransfer
    Dim clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objCon As SqlConnection

#Region "Fund Transfers"
    Public Function InsertFundTransferRequest(MemberID As Integer _
                                      , FromMemberAccountID As Integer _
                                      , FromAccountTypeCode As String _
                                      , ToMemberAccountID As Integer _
                                      , ToAccountTypeCode As String _
                                      , ToExternalAccount As Boolean _
                                      , ToExternalAccountNumber As String _
                                      , ToExternalSortCode As String _
                                      , Reference As String _
                                      , Instructions As String _
                                      , Amount As Double) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.INSERT_FUND_TRANSFER_REQUEST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@FromMemberAccountID", SqlDbType.Int)
            cmdCommand.Parameters("@FromMemberAccountID").Value = FromMemberAccountID

            cmdCommand.Parameters.Add("@FromAccountTypeCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@FromAccountTypeCode").Value = FromAccountTypeCode

            cmdCommand.Parameters.Add("@ToMemberAccountID", SqlDbType.Int)
            cmdCommand.Parameters("@ToMemberAccountID").Value = ToMemberAccountID

            cmdCommand.Parameters.Add("@ToAccountTypeCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ToAccountTypeCode").Value = ToAccountTypeCode

            cmdCommand.Parameters.Add("@ToExternalAccount", SqlDbType.Bit)
            cmdCommand.Parameters("@ToExternalAccount").Value = ToExternalAccount

            cmdCommand.Parameters.Add("@ToExternalAccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@ToExternalAccountNumber").Value = ToExternalAccountNumber

            cmdCommand.Parameters.Add("@ToExternalSortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ToExternalSortCode").Value = ToExternalSortCode

            cmdCommand.Parameters.Add("@Reference", SqlDbType.VarChar)
            cmdCommand.Parameters("@Reference").Value = Reference

            cmdCommand.Parameters.Add("@Instructions", SqlDbType.VarChar)
            cmdCommand.Parameters("@Instructions").Value = Instructions

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = Amount

            intResult = cmdCommand.ExecuteNonQuery
            InsertFundTransferRequest = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetFundTransferRequestPendingEntries() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_PENDING_FUND_TRANSFER_REQUESTS_FOR_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function RemoveFundTransferPendingBacs(ByVal ID As Integer _
                                  ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_REMOVE_PENDING_FUND_TRANSFER_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@TransferRequestID", SqlDbType.Int)
            cmdCommand.Parameters("@TransferRequestID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            RemoveFundTransferPendingBacs = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region
End Class
