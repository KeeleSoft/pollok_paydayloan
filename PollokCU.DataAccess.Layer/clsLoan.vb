﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls loan related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 03/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsLoan
    Dim clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objCon As SqlConnection

#Region "Insert Update loan"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert update loan
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/09/2009  520.Baseline - Created
    '''     [Shan HM]  09/05/2011  New fiels address (Nationality,PreviousAddressNoOfYears1,PreviousAddressNoOfMonths1,JobTitle)
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertUpdateLoan(ByVal AppType As String _
                                     , ByVal SalutationID As Int32 _
                                     , ByVal FirstName As String _
                                     , ByVal SurName As String _
                                     , ByVal DateOfBirth As DateTime _
                                     , ByVal CurrentAddressLine1 As String _
                                     , ByVal CurrentAddressLine2 As String _
                                     , ByVal CurrentCity As String _
                                     , ByVal CurrentCounty As String _
                                     , ByVal CurrentPostCode As String _
                                     , ByVal CurrentAddressNoOfYears As Int32 _
                                     , ByVal HomeTelephone As String _
                                     , ByVal Mobile As String _
                                     , ByVal Email As String _
                                     , ByVal NINumber As String _
                                     , ByVal MaritalStatus As Integer _
                                     , ByVal NoOfDependants As Int32 _
                                     , ByVal PreviousAddressLine1 As String _
                                     , ByVal PreviousAddressLine2 As String _
                                     , ByVal PreviousCity As String _
                                     , ByVal PreviousCounty As String _
                                     , ByVal PreviousPostCode As String _
                                     , ByVal ResidencyStatus As Integer _
                                     , ByVal LoanAmount As Decimal _
                                     , ByVal RepaymentAmount As Decimal _
                                     , ByVal RepaymentYears As Int32 _
                                     , ByVal Purpose As String _
                                     , ByVal EmploymentStatus As Int32 _
                                     , ByVal CompanyName As String _
                                     , ByVal CompanyDepartment As String _
                                     , ByVal Permanent As Boolean _
                                     , ByVal WorkAddressLine1 As String _
                                     , ByVal WorkAddressLine2 As String _
                                     , ByVal WorkCity As String _
                                     , ByVal WorkCounty As String _
                                     , ByVal WorkPostCode As String _
                                     , ByVal WithEmployerYears As Int32 _
                                     , ByVal WithEmployerMonths As Int32 _
                                     , ByVal WorkTelephone As String _
                                     , ByVal IncomeSalary As String _
                                     , ByVal IncomePartners As String _
                                     , ByVal IncomeBenefits As String _
                                     , ByVal IncomeSupport As String _
                                     , ByVal IncomeTaxCredits As String _
                                     , ByVal IncomePrivatePension As String _
                                     , ByVal IncomeOther As String _
                                     , ByVal ExpenseRent As String _
                                     , ByVal ExpenseMortgage As String _
                                     , ByVal ExpenseCouncilTax As String _
                                     , ByVal ExpenseElectricity As String _
                                     , ByVal ExpenseGas As String _
                                     , ByVal ExpenseTelephone As String _
                                     , ByVal ExpenseWater As String _
                                     , ByVal ExpenseGroceries As String _
                                     , ByVal ExpenseCataloguesPayments As String _
                                     , ByVal ExpenseCreditCards As String _
                                     , ByVal ExpenseHirePurchase As String _
                                     , ByVal ExpenseCarLoan As String _
                                     , ByVal ExpenseTravel As String _
                                     , ByVal ExpenseClothing As String _
                                     , ByVal ExpensePension As String _
                                     , ByVal ExpenseSavingsInvestment As String _
                                     , ByVal ExpenseHomeInsurance As String _
                                     , ByVal ExpenseCarInsurance As String _
                                     , ByVal ExpenseLifeInsurance As String _
                                     , ByVal ExpenseOther As String _
                                     , ByVal TermsAccepted As Boolean _
                                     , ByVal SortCode As String _
                                     , ByVal AccountNo As String _
                                     , ByVal BankRollNo As String _
                                     , ByVal BankName As String _
                                     , ByVal TimeWithBank As String _
                                     , ByVal CreditCardsHeld As String _
                                     , ByVal UserID As Integer _
                                     , ByRef LoanID As Integer _
                                     , Optional ByVal Password As String = "" _
                                     , Optional ByVal MemorableInfo As String = "" _
                                     , Optional ByVal BeneficiaryOK As Boolean = False _
                                     , Optional ByVal BeneficiaryTitle As Integer = 0 _
                                     , Optional ByVal BeneficiaryFirstName As String = "" _
                                     , Optional ByVal BeneficiarySurname As String = "" _
                                     , Optional ByVal BeneficiaryAddressLine1 As String = "" _
                                     , Optional ByVal BeneficiaryAddressLine2 As String = "" _
                                     , Optional ByVal BeneficiaryCity As String = "" _
                                     , Optional ByVal BeneficiaryCounty As String = "" _
                                     , Optional ByVal BeneficiaryPostCode As String = "" _
                                     , Optional ByVal BeneficiaryRelationship As String = "" _
                                     , Optional ByVal PaymentProtection As Integer = 0 _
                                     , Optional ByVal CurrentAddressNoOfMonths As Int32 = 0 _
                                     , Optional ByVal ContractDuration As Int32 = 0 _
                                     , Optional ByVal IncapacityBenefit As String = "" _
                                     , Optional ByVal CarersAllowance As String = "" _
                                     , Optional ByVal DrivingAllowance As String = "" _
                                     , Optional ByVal GoodHealth As Boolean = False _
                                     , Optional ByVal NormalOccupation As Boolean = False _
                                     , Optional ByVal LoanPayoutMethod As String = "" _
                                     , Optional ByVal LoanPayoutBankName As String = "" _
                                     , Optional ByVal LoanPayoutBankAccNo As String = "" _
                                     , Optional ByVal LoanPayoutBankSortCode As String = "" _
                                     , Optional ByVal LoanPayoutBankAddress As String = "" _
                                     , Optional ByVal ExperianFailedMessage As String = "" _
                                     , Optional ByVal ExperianDecisionCode As String = "" _
                                     , Optional ByVal ExperianRef As String = "" _
                                     , Optional ByVal ExperianAuthIndexCode As String = "" _
                                     , Optional ByVal ExperianAuthIndexText As String = "" _
                                     , Optional ByVal ExperianAuthDecisionCode As String = "" _
                                     , Optional ByVal ExperianAuthDecisionText As String = "" _
                                     , Optional ByVal ExperianPolicyCode As String = "" _
                                     , Optional ByVal ExperianPolicyRuleText As String = "" _
                                     , Optional ByVal MemberID As String = "" _
                                     , Optional ByVal LoanSavingsAmount As Double = 0 _
                                     , Optional ByVal CCJHave As String = "" _
                                     , Optional ByVal CCJDetails As String = "" _
                                     , Optional ByVal LotteryCount As Integer = 0 _
                                     , Optional ByVal LotteryAmount As String = "" _
                                     , Optional ByVal PlaceOfBirth As String = "" _
                                     , Optional ByVal MotherName As String = "" _
                                     , Optional ByVal LoanEmpHomeStatus As String = "" _
                                     , Optional ByVal LoanEmpDoorstep As String = "" _
                                     , Optional ByVal LoanEmpLoanOutstanding As String = "" _
                                     , Optional ByVal LoanEmpDisabilityAllowance As String = "" _
                                     , Optional ByVal LoanEmpBankAccount As String = "" _
                                     , Optional ByVal LoanEmpSocialFundLoan As String = "" _
                                     , Optional ByVal LoanEmpBenefits As String = "" _
                                     , Optional ByVal LoanEmpEthnicity As String = "" _
                                     , Optional ByVal LoanPayrollDeduct As String = "NA" _
                                     , Optional ByVal LoanPayrollDeductCompanyID As Integer = 0 _
                                     , Optional ByVal LoanPayrollNo As String = "" _
                                     , Optional ByVal LoanPayrollAmount As String = "" _
                                     , Optional ByVal LoanPayrollDeductPeriod As String = "" _
                                     , Optional ByVal ExpenseMortgageOutstanding As String = "" _
                                     , Optional ByVal ExpenseMortgageAssetValue As String = "" _
                                     , Optional ByVal ExpenseLoanOutstanding As String = "" _
                                     , Optional ByVal ExpenseLoanAssetValue As String = "" _
                                     , Optional ByVal ExpensePensionOutstanding As String = "" _
                                     , Optional ByVal ExpensePensionAssetValue As String = "" _
                                     , Optional ByVal ExpenseSavingOutstanding As String = "" _
                                     , Optional ByVal ExpenseSavingAssetValue As String = "" _
                                     , Optional ByVal ExpenseLifeOutstanding As String = "" _
                                     , Optional ByVal ExpenseLifeAssetValue As String = "" _
                                     , Optional ByVal ContractDurationMonths As Int16 = -1 _
                                     , Optional ByVal ChequeCollectBranch As String = "" _
                                     , Optional ByVal Nationality As String = "" _
                                     , Optional ByVal PreviousAddressNoOfYears1 As Int32 = 0 _
                                     , Optional ByVal PreviousAddressNoOfMonths1 As Int32 = 0 _
                                     , Optional ByVal JobTitle As String = "" _
                                     , Optional ByVal MiddleName As String = "" _
                                     , Optional ByVal Gender As String = "" _
                                     , Optional ByVal HaveMortgage As String = "" _
                                     , Optional ByVal NatureOfBusiness As String = "" _
                                     , Optional ByVal TimeAsSelfEmployed As String = "" _
                                     , Optional ByVal IncomeFrequency As String = "" _
                                     , Optional ByVal IntroducedBy As String = "" _
                                     ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_UPDATE_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            cmdCommand.Parameters.Add("@SalutationID", SqlDbType.Int)
            cmdCommand.Parameters("@SalutationID").Value = SalutationID

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = FirstName

            cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SurName").Value = SurName

            cmdCommand.Parameters.Add("@DateOfBirth", SqlDbType.DateTime)
            cmdCommand.Parameters("@DateOfBirth").Value = IIf(DateOfBirth = "01/01/1900", System.DBNull.Value, DateOfBirth)

            cmdCommand.Parameters.Add("@CurrentAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentAddressLine1").Value = CurrentAddressLine1

            cmdCommand.Parameters.Add("@CurrentAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentAddressLine2").Value = CurrentAddressLine2

            cmdCommand.Parameters.Add("@CurrentCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCity").Value = CurrentCity

            cmdCommand.Parameters.Add("@CurrentCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCounty").Value = CurrentCounty

            cmdCommand.Parameters.Add("@CurrentPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentPostCode").Value = CurrentPostCode

            cmdCommand.Parameters.Add("@CurrentAddressNoOfYears", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CurrentAddressNoOfYears").Value = CurrentAddressNoOfYears

            cmdCommand.Parameters.Add("@HomeTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@HomeTelephone").Value = HomeTelephone

            cmdCommand.Parameters.Add("@Mobile", SqlDbType.VarChar)
            cmdCommand.Parameters("@Mobile").Value = Mobile

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = Email

            cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@NINumber").Value = NINumber

            cmdCommand.Parameters.Add("@MaritalStatus", SqlDbType.Int)
            cmdCommand.Parameters("@MaritalStatus").Value = MaritalStatus

            cmdCommand.Parameters.Add("@NoOfDependants", SqlDbType.TinyInt)
            cmdCommand.Parameters("@NoOfDependants").Value = NoOfDependants

            cmdCommand.Parameters.Add("@PreviousAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine1").Value = PreviousAddressLine1

            cmdCommand.Parameters.Add("@PreviousAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine2").Value = PreviousAddressLine2

            cmdCommand.Parameters.Add("@PreviousCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCity").Value = PreviousCity

            cmdCommand.Parameters.Add("@PreviousCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCounty").Value = PreviousCounty

            cmdCommand.Parameters.Add("@PreviousPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousPostCode").Value = PreviousPostCode

            cmdCommand.Parameters.Add("@ResidencyStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ResidencyStatus").Value = IIf(ResidencyStatus > 0, ResidencyStatus, 0)

            cmdCommand.Parameters.Add("@LoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@LoanAmount").Value = LoanAmount

            cmdCommand.Parameters.Add("@RepaymentAmount", SqlDbType.Money)
            cmdCommand.Parameters("@RepaymentAmount").Value = RepaymentAmount

            cmdCommand.Parameters.Add("@RepaymentYears", SqlDbType.TinyInt)
            cmdCommand.Parameters("@RepaymentYears").Value = RepaymentYears

            cmdCommand.Parameters.Add("@Purpose", SqlDbType.VarChar)
            cmdCommand.Parameters("@Purpose").Value = Purpose

            cmdCommand.Parameters.Add("@EmploymentStatus", SqlDbType.Int)
            cmdCommand.Parameters("@EmploymentStatus").Value = EmploymentStatus

            cmdCommand.Parameters.Add("@CompanyName", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyName").Value = CompanyName

            cmdCommand.Parameters.Add("@CompanyDepartment", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyDepartment").Value = CompanyDepartment

            cmdCommand.Parameters.Add("@Permanent", SqlDbType.Bit)
            cmdCommand.Parameters("@Permanent").Value = Permanent

            cmdCommand.Parameters.Add("@WorkAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine1").Value = WorkAddressLine1

            cmdCommand.Parameters.Add("@WorkAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine2").Value = WorkAddressLine2

            cmdCommand.Parameters.Add("@WorkCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCity").Value = WorkCity

            cmdCommand.Parameters.Add("@WorkCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCounty").Value = WorkCounty

            cmdCommand.Parameters.Add("@WorkPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkPostCode").Value = WorkPostCode

            cmdCommand.Parameters.Add("@WithEmployerYears", SqlDbType.TinyInt)
            cmdCommand.Parameters("@WithEmployerYears").Value = WithEmployerYears

            cmdCommand.Parameters.Add("@WithEmployerMonths", SqlDbType.TinyInt)
            cmdCommand.Parameters("@WithEmployerMonths").Value = WithEmployerMonths

            cmdCommand.Parameters.Add("@WorkTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkTelephone").Value = WorkTelephone

            cmdCommand.Parameters.Add("@IncomeSalary", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeSalary").Value = IncomeSalary

            cmdCommand.Parameters.Add("@IncomePartners", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomePartners").Value = IncomePartners

            cmdCommand.Parameters.Add("@IncomeBenefits", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeBenefits").Value = IncomeBenefits

            cmdCommand.Parameters.Add("@IncomeSupport", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeSupport").Value = IncomeSupport

            cmdCommand.Parameters.Add("@IncomeTaxCredits", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeTaxCredits").Value = IncomeTaxCredits

            cmdCommand.Parameters.Add("@IncomePrivatePension", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomePrivatePension").Value = IncomePrivatePension

            cmdCommand.Parameters.Add("@IncomeOther", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeOther").Value = IncomeOther

            cmdCommand.Parameters.Add("@ExpenseRent", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseRent").Value = ExpenseRent

            cmdCommand.Parameters.Add("@ExpenseMortgage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseMortgage").Value = ExpenseMortgage

            cmdCommand.Parameters.Add("@ExpenseCouncilTax", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseCouncilTax").Value = ExpenseCouncilTax

            cmdCommand.Parameters.Add("@ExpenseElectricity", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseElectricity").Value = ExpenseElectricity

            cmdCommand.Parameters.Add("@ExpenseGas", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseGas").Value = ExpenseGas

            cmdCommand.Parameters.Add("@ExpenseTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseTelephone").Value = ExpenseTelephone

            cmdCommand.Parameters.Add("@ExpenseWater", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseWater").Value = ExpenseWater

            cmdCommand.Parameters.Add("@ExpenseGroceries", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseGroceries").Value = ExpenseGroceries

            cmdCommand.Parameters.Add("@ExpenseCataloguesPayments", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseCataloguesPayments").Value = ExpenseCataloguesPayments

            cmdCommand.Parameters.Add("@ExpenseCreditCards", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseCreditCards").Value = ExpenseCreditCards

            cmdCommand.Parameters.Add("@ExpenseHirePurchase", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseHirePurchase").Value = ExpenseHirePurchase

            cmdCommand.Parameters.Add("@ExpenseCarLoan", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseCarLoan").Value = ExpenseCarLoan

            cmdCommand.Parameters.Add("@ExpenseTravel", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseTravel").Value = ExpenseTravel

            cmdCommand.Parameters.Add("@ExpenseClothing", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseClothing").Value = ExpenseClothing

            cmdCommand.Parameters.Add("@ExpensePension", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpensePension").Value = ExpensePension

            cmdCommand.Parameters.Add("@ExpenseSavingsInvestment", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseSavingsInvestment").Value = ExpenseSavingsInvestment

            cmdCommand.Parameters.Add("@ExpenseHomeInsurance", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseHomeInsurance").Value = ExpenseHomeInsurance

            cmdCommand.Parameters.Add("@ExpenseCarInsurance", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseCarInsurance").Value = ExpenseCarInsurance

            cmdCommand.Parameters.Add("@ExpenseLifeInsurance", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseLifeInsurance").Value = ExpenseLifeInsurance

            cmdCommand.Parameters.Add("@ExpenseOther", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseOther").Value = ExpenseOther

            cmdCommand.Parameters.Add("@TermsAccepted", SqlDbType.Bit)
            cmdCommand.Parameters("@TermsAccepted").Value = TermsAccepted

            cmdCommand.Parameters.Add("@SortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@SortCode").Value = SortCode

            cmdCommand.Parameters.Add("@AccountNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNo").Value = AccountNo

            cmdCommand.Parameters.Add("@BankRollNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@BankRollNo").Value = BankRollNo

            cmdCommand.Parameters.Add("@BankName", SqlDbType.VarChar)
            cmdCommand.Parameters("@BankName").Value = BankName

            cmdCommand.Parameters.Add("@TimeWithBank", SqlDbType.VarChar)
            cmdCommand.Parameters("@TimeWithBank").Value = TimeWithBank

            cmdCommand.Parameters.Add("@CreditCardsHeld", SqlDbType.VarChar)
            cmdCommand.Parameters("@CreditCardsHeld").Value = CreditCardsHeld

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = Password

            cmdCommand.Parameters.Add("@MemorableInfo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemorableInfo").Value = MemorableInfo

            cmdCommand.Parameters.Add("@BeneficiaryOK", SqlDbType.Bit)
            cmdCommand.Parameters("@BeneficiaryOK").Value = BeneficiaryOK

            cmdCommand.Parameters.Add("@BeneficiaryTitle", SqlDbType.Int)
            cmdCommand.Parameters("@BeneficiaryTitle").Value = BeneficiaryTitle

            cmdCommand.Parameters.Add("@BeneficiaryFirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryFirstName").Value = BeneficiaryFirstName

            cmdCommand.Parameters.Add("@BeneficiarySurname", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiarySurname").Value = BeneficiarySurname

            cmdCommand.Parameters.Add("@BeneficiaryAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryAddressLine1").Value = BeneficiaryAddressLine1

            cmdCommand.Parameters.Add("@BeneficiaryAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryAddressLine2").Value = BeneficiaryAddressLine2

            cmdCommand.Parameters.Add("@BeneficiaryCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryCity").Value = BeneficiaryCity

            cmdCommand.Parameters.Add("@BeneficiaryCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryCounty").Value = BeneficiaryCounty

            cmdCommand.Parameters.Add("@BeneficiaryPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryPostCode").Value = BeneficiaryPostCode

            cmdCommand.Parameters.Add("@BeneficiaryRelationship", SqlDbType.VarChar)
            cmdCommand.Parameters("@BeneficiaryRelationship").Value = BeneficiaryRelationship

            cmdCommand.Parameters.Add("@PaymentProtection", SqlDbType.Int)
            cmdCommand.Parameters("@PaymentProtection").Value = PaymentProtection

            cmdCommand.Parameters.Add("@CurrentAddressNoOfMonths", SqlDbType.SmallInt)
            cmdCommand.Parameters("@CurrentAddressNoOfMonths").Value = CurrentAddressNoOfMonths

            cmdCommand.Parameters.Add("@ContractDuration", SqlDbType.SmallInt)
            cmdCommand.Parameters("@ContractDuration").Value = ContractDuration

            cmdCommand.Parameters.Add("@IncapacityBenefit", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncapacityBenefit").Value = IncapacityBenefit

            cmdCommand.Parameters.Add("@CarersAllowance", SqlDbType.VarChar)
            cmdCommand.Parameters("@CarersAllowance").Value = CarersAllowance

            cmdCommand.Parameters.Add("@DrivingAllowance", SqlDbType.VarChar)
            cmdCommand.Parameters("@DrivingAllowance").Value = DrivingAllowance

            cmdCommand.Parameters.Add("@GoodHealth", SqlDbType.Bit)
            cmdCommand.Parameters("@GoodHealth").Value = GoodHealth

            cmdCommand.Parameters.Add("@NormalOccupation", SqlDbType.VarChar)
            cmdCommand.Parameters("@NormalOccupation").Value = NormalOccupation

            cmdCommand.Parameters.Add("@LoanPayoutMethod", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayoutMethod").Value = LoanPayoutMethod

            cmdCommand.Parameters.Add("@LoanPayoutBankName", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayoutBankName").Value = LoanPayoutBankName

            cmdCommand.Parameters.Add("@LoanPayoutBankAccNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayoutBankAccNo").Value = LoanPayoutBankAccNo

            cmdCommand.Parameters.Add("@LoanPayoutBankSortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayoutBankSortCode").Value = LoanPayoutBankSortCode

            cmdCommand.Parameters.Add("@LoanPayoutBankAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayoutBankAddress").Value = LoanPayoutBankAddress

            'Experian Authentication details
            cmdCommand.Parameters.Add("@ExperianFailedMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianFailedMessage").Value = ExperianFailedMessage

            cmdCommand.Parameters.Add("@ExperianDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianDecisionCode").Value = ExperianDecisionCode

            cmdCommand.Parameters.Add("@ExperianRef", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianRef").Value = ExperianRef

            cmdCommand.Parameters.Add("@ExperianAuthIndexCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexCode").Value = ExperianAuthIndexCode

            cmdCommand.Parameters.Add("@ExperianAuthIndexText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexText").Value = ExperianAuthIndexText

            cmdCommand.Parameters.Add("@ExperianAuthDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionCode").Value = ExperianAuthDecisionCode

            cmdCommand.Parameters.Add("@ExperianAuthDecisionText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionText").Value = ExperianAuthDecisionText

            cmdCommand.Parameters.Add("@ExperianPolicyCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyCode").Value = ExperianPolicyCode

            cmdCommand.Parameters.Add("@ExperianPolicyRuleText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyRuleText").Value = ExperianPolicyRuleText

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            'New fields Apr 2010
            cmdCommand.Parameters.Add("@LoanSavingsAmount", SqlDbType.Money)
            cmdCommand.Parameters("@LoanSavingsAmount").Value = LoanSavingsAmount

            cmdCommand.Parameters.Add("@CCJHave", SqlDbType.VarChar)
            cmdCommand.Parameters("@CCJHave").Value = CCJHave

            cmdCommand.Parameters.Add("@CCJDetails", SqlDbType.VarChar)
            cmdCommand.Parameters("@CCJDetails").Value = CCJDetails

            cmdCommand.Parameters.Add("@LotteryCount", SqlDbType.Int)
            cmdCommand.Parameters("@LotteryCount").Value = LotteryCount

            cmdCommand.Parameters.Add("@LotteryAmount", SqlDbType.VarChar)
            cmdCommand.Parameters("@LotteryAmount").Value = LotteryAmount

            cmdCommand.Parameters.Add("@PlaceOfBirth", SqlDbType.VarChar)
            cmdCommand.Parameters("@PlaceOfBirth").Value = PlaceOfBirth

            cmdCommand.Parameters.Add("@MotherName", SqlDbType.VarChar)
            cmdCommand.Parameters("@MotherName").Value = MotherName

            cmdCommand.Parameters.Add("@LoanEmpHomeStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpHomeStatus").Value = LoanEmpHomeStatus

            cmdCommand.Parameters.Add("@LoanEmpDoorstep", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpDoorstep").Value = LoanEmpDoorstep

            cmdCommand.Parameters.Add("@LoanEmpLoanOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpLoanOutstanding").Value = LoanEmpLoanOutstanding

            cmdCommand.Parameters.Add("@LoanEmpDisabilityAllowance", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpDisabilityAllowance").Value = LoanEmpDisabilityAllowance

            cmdCommand.Parameters.Add("@LoanEmpBankAccount", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpBankAccount").Value = LoanEmpBankAccount

            cmdCommand.Parameters.Add("@LoanEmpSocialFundLoan", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpSocialFundLoan").Value = LoanEmpSocialFundLoan

            cmdCommand.Parameters.Add("@LoanEmpBenefits", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpBenefits").Value = LoanEmpBenefits

            cmdCommand.Parameters.Add("@LoanEmpEthnicity", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanEmpEthnicity").Value = LoanEmpEthnicity

            cmdCommand.Parameters.Add("@LoanPayrollDeduct", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayrollDeduct").Value = LoanPayrollDeduct

            cmdCommand.Parameters.Add("@LoanPayrollDeductCompanyID", SqlDbType.Int)
            cmdCommand.Parameters("@LoanPayrollDeductCompanyID").Value = LoanPayrollDeductCompanyID

            cmdCommand.Parameters.Add("@LoanPayrollNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayrollNo").Value = LoanPayrollNo

            cmdCommand.Parameters.Add("@LoanPayrollAmount", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayrollAmount").Value = LoanPayrollAmount

            cmdCommand.Parameters.Add("@LoanPayrollDeductPeriod", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanPayrollDeductPeriod").Value = LoanPayrollDeductPeriod

            cmdCommand.Parameters.Add("@ExpenseMortgageOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseMortgageOutstanding").Value = ExpenseMortgageOutstanding

            cmdCommand.Parameters.Add("@ExpenseMortgageAssetValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseMortgageAssetValue").Value = ExpenseMortgageAssetValue

            cmdCommand.Parameters.Add("@ExpenseLoanOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseLoanOutstanding").Value = ExpenseLoanOutstanding

            cmdCommand.Parameters.Add("@ExpenseLoanAssetValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseLoanAssetValue").Value = ExpenseLoanAssetValue

            cmdCommand.Parameters.Add("@ExpensePensionOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpensePensionOutstanding").Value = ExpensePensionOutstanding

            cmdCommand.Parameters.Add("@ExpensePensionAssetValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpensePensionAssetValue").Value = ExpensePensionAssetValue

            cmdCommand.Parameters.Add("@ExpenseSavingOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseSavingOutstanding").Value = ExpenseSavingOutstanding

            cmdCommand.Parameters.Add("@ExpenseSavingAssetValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseSavingAssetValue").Value = ExpenseSavingAssetValue

            cmdCommand.Parameters.Add("@ExpenseLifeOutstanding", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseLifeOutstanding").Value = ExpenseLifeOutstanding

            cmdCommand.Parameters.Add("@ExpenseLifeAssetValue", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExpenseLifeAssetValue").Value = ExpenseLifeAssetValue

            cmdCommand.Parameters.Add("@ContractDurationMonths", SqlDbType.SmallInt)
            cmdCommand.Parameters("@ContractDurationMonths").Value = ContractDurationMonths

            cmdCommand.Parameters.Add("@ChequeCollectBranch", SqlDbType.VarChar)
            cmdCommand.Parameters("@ChequeCollectBranch").Value = ChequeCollectBranch

            'New fiels May 2011
            cmdCommand.Parameters.Add("@Nationality", SqlDbType.VarChar)
            cmdCommand.Parameters("@Nationality").Value = Nationality

            cmdCommand.Parameters.Add("@PreviousAddressNoOfYears1", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PreviousAddressNoOfYears1").Value = PreviousAddressNoOfYears1

            cmdCommand.Parameters.Add("@PreviousAddressNoOfMonths1", SqlDbType.TinyInt)
            cmdCommand.Parameters("@PreviousAddressNoOfMonths1").Value = PreviousAddressNoOfMonths1

            cmdCommand.Parameters.Add("@JobTitle", SqlDbType.VarChar)
            cmdCommand.Parameters("@JobTitle").Value = JobTitle

            cmdCommand.Parameters.Add("@MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@MiddleName").Value = MiddleName

            cmdCommand.Parameters.Add("@Gender", SqlDbType.VarChar)
            cmdCommand.Parameters("@Gender").Value = Gender

            cmdCommand.Parameters.Add("@HaveMortgage", SqlDbType.VarChar)
            cmdCommand.Parameters("@HaveMortgage").Value = HaveMortgage

            cmdCommand.Parameters.Add("@NatureOfBusiness", SqlDbType.VarChar)
            cmdCommand.Parameters("@NatureOfBusiness").Value = NatureOfBusiness

            cmdCommand.Parameters.Add("@TimeAsSelfEmployed", SqlDbType.VarChar)
            cmdCommand.Parameters("@TimeAsSelfEmployed").Value = TimeAsSelfEmployed

            cmdCommand.Parameters.Add("@IncomeFrequency", SqlDbType.VarChar)
            cmdCommand.Parameters("@IncomeFrequency").Value = IncomeFrequency

            cmdCommand.Parameters.Add("@IntroducedBy", SqlDbType.VarChar)
            cmdCommand.Parameters("@IntroducedBy").Value = IntroducedBy

            If LoanID > 0 Then
                cmdCommand.Parameters.Add("@LoanID", SqlDbType.Int)
                cmdCommand.Parameters("@LoanID").Value = LoanID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@LoanID", SqlDbType.Int)
                cmdCommand.Parameters("@LoanID").Direction = ParameterDirection.Output
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If LoanID <= 0 Then
                LoanID = CInt(cmdCommand.Parameters("@LoanID").Value)
            End If

            InsertUpdateLoan = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert/update loan" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

#End Region

#Region "Loan Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select loan details for a given loanid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Loan Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectLoanDetail(ByVal iLoanID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_LOAN_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@LoanID", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoanID").Value = iLoanID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            SelectLoanDetail = dtLoanDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve application details for memberid " & iLoanID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Email Body"
    Public Function GetApplicationEmailBody(ByVal LoanID As Integer _
                                            , ByVal TemplateCode As String _
                                 , ByVal MemberID As String _
                                 , Optional ByVal PDFUrl As String = "" _
                                 ) As String
        Dim cmdCommand As New SqlCommand

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_APPLICATION_EMAIL_BODY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@LoanID", SqlDbType.Int)
            cmdCommand.Parameters("@LoanID").Value = LoanID

            cmdCommand.Parameters.Add("@TemplateCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@TemplateCode").Value = TemplateCode

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@PDFUrl", SqlDbType.VarChar)
            cmdCommand.Parameters("@PDFUrl").Value = PDFUrl

            GetApplicationEmailBody = cmdCommand.ExecuteScalar()

        Catch ex As Exception
            Throw New ApplicationException("Failed to get application email body " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region
End Class

