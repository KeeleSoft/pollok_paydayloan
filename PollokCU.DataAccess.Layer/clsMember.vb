﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsUser.vb
'''********************************************************************
''' <Summary>
'''	This module calls user related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 03/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsMember
    Dim clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objCon As SqlConnection

#Region "User Data Manipulation Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert user
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/03/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertUser(ByVal sUserName As String _
                                        , ByVal iSalutationID As Integer _
                                        , ByVal sFirstName As String _
                                        , ByVal sSurName As String _
                                        , ByVal sEmail As String _
                                        , ByVal sTelephone As String _
                                        , ByVal sMobile As String _
                                        , ByVal sPassword As String _
                                        , ByVal iCreatedBy As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_USER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserName", SqlDbType.VarChar)
            cmdCommand.Parameters("@UserName").Value = sUserName

            cmdCommand.Parameters.Add("@SalutationID", SqlDbType.Int)
            cmdCommand.Parameters("@SalutationID").Value = iSalutationID

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = sFirstName

            cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SurName").Value = sSurName

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = sEmail

            cmdCommand.Parameters.Add("@Telephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@Telephone").Value = sTelephone

            cmdCommand.Parameters.Add("@Mobile", SqlDbType.VarChar)
            cmdCommand.Parameters("@Mobile").Value = sMobile

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = sPassword

            cmdCommand.Parameters.Add("@CreatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@CreatedBy").Value = iCreatedBy

            intResult = cmdCommand.ExecuteNonQuery
            InsertUser = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert user" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update user
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/03/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateUser(ByVal iUserID As Integer _
                                        , ByVal sUserName As String _
                                        , ByVal iSalutationID As Integer _
                                        , ByVal sFirstName As String _
                                        , ByVal sSurName As String _
                                        , ByVal sEmail As String _
                                        , ByVal sTelephone As String _
                                        , ByVal sMobile As String _
                                        , ByVal sPassword As String _
                                        , ByVal iUpdatedBy As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("UPDATE_USER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = iUserID

            cmdCommand.Parameters.Add("@UserName", SqlDbType.VarChar)
            cmdCommand.Parameters("@UserName").Value = sUserName

            cmdCommand.Parameters.Add("@SalutationID", SqlDbType.Int)
            cmdCommand.Parameters("@SalutationID").Value = iSalutationID

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = sFirstName

            cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SurName").Value = sSurName

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = sEmail

            cmdCommand.Parameters.Add("@Telephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@Telephone").Value = sTelephone

            cmdCommand.Parameters.Add("@Mobile", SqlDbType.VarChar)
            cmdCommand.Parameters("@Mobile").Value = sMobile

            If sPassword.Length > 0 Then
                cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
                cmdCommand.Parameters("@Password").Value = sPassword
            End If

            cmdCommand.Parameters.Add("@UpdatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@UpdatedBy").Value = iUpdatedBy

            intResult = cmdCommand.ExecuteNonQuery
            UpdateUser = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update user" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update user password
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/03/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateUserPassword(ByVal iUserID As Integer _
                                        , ByVal sPassword As String _
                                        , ByVal iUpdatedBy As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("UPDATE_USER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = iUserID

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = sPassword

            cmdCommand.Parameters.Add("@UpdatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@UpdatedBy").Value = iUpdatedBy

            intResult = cmdCommand.ExecuteNonQuery
            UpdateUserPassword = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update user password" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member Login Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Authenticate member login
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member ID
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  03/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function AuthenticateMemberLogin(ByVal iMemberID As Integer _
                                            , ByVal sPassword As String _
                                            , ByVal iUserID As Integer _
                                            , ByVal Source As Integer _
                                            , ByVal OSInformation As String _
                                            , ByRef sMemorableInfo As String _
                                            , ByRef bAllowLogin As Boolean _
                                            , ByRef sUserMessage As String _
                                            , ByRef Token As String) As Integer
        Dim cmdCommand As New SqlCommand
        Dim iReturn As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_AUTHENTICATE_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = iMemberID

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = sPassword

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = iUserID

            cmdCommand.Parameters.Add("@Source", SqlDbType.Int)
            cmdCommand.Parameters("@Source").Value = Source

            cmdCommand.Parameters.Add("@OSInformation", SqlDbType.VarChar)
            cmdCommand.Parameters("@OSInformation").Value = OSInformation

            'Output parameters
            cmdCommand.Parameters.Add("@MemorableInfo", SqlDbType.VarChar, 30)
            cmdCommand.Parameters("@MemorableInfo").Direction = ParameterDirection.Output

            cmdCommand.Parameters.Add("@AllowLogin", SqlDbType.Bit)
            cmdCommand.Parameters("@AllowLogin").Direction = ParameterDirection.Output

            cmdCommand.Parameters.Add("@UserMessage", SqlDbType.VarChar, 200)
            cmdCommand.Parameters("@UserMessage").Direction = ParameterDirection.Output

            cmdCommand.Parameters.Add("@Token", SqlDbType.VarChar, 100)
            cmdCommand.Parameters("@Token").Direction = ParameterDirection.Output

            iReturn = cmdCommand.ExecuteNonQuery

            'Read all outputp variables
            sMemorableInfo = IIf(IsDBNull(cmdCommand.Parameters("@MemorableInfo").Value), "", cmdCommand.Parameters("@MemorableInfo").Value)
            Boolean.TryParse(cmdCommand.Parameters("@AllowLogin").Value, bAllowLogin)
            sUserMessage = IIf(IsDBNull(cmdCommand.Parameters("@UserMessage").Value), "", cmdCommand.Parameters("@UserMessage").Value)
            Token = IIf(IsDBNull(cmdCommand.Parameters("@Token").Value), "", cmdCommand.Parameters("@Token").Value)

        Catch ex As Exception
            iReturn = -1
            sUserMessage = ex.Message
            Throw New ApplicationException("Failed to authentice the member" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
            AuthenticateMemberLogin = iReturn
        End Try
    End Function

    ''' <summary>
    ''' Authenticate mobile token
    ''' </summary>
    Public Function AuthenticateMobileToken(ByVal memberID As Integer _
                                            , ByVal Token As String) As Boolean
        Dim cmdCommand As New SqlCommand
        Dim iReturn As Boolean

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_VALIDATE_MOBILE_TOKEN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            cmdCommand.Parameters.Add("@Token", SqlDbType.VarChar)
            cmdCommand.Parameters("@Token").Value = Token


            iReturn = cmdCommand.ExecuteScalar

        Catch ex As Exception
            iReturn = False
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
            AuthenticateMobileToken = iReturn
        End Try
    End Function
#End Region

#Region "Member Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member details for a given memberid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  07/09/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectMemberDetail(ByVal iMemberID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_MEMBER_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = iMemberID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectMemberDetail = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve member details for memberid " & iMemberID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member details for a given mobile number
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectMemberDetailByMobile(ByVal MobileNumber As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_MEMBER_DETAIL_FROM_MOBILE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectMemberDetailByMobile = dtMemberDetail

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member details for a given NI number
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectMemberDetailByNINumber(ByVal NINumber As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_MEMBER_DETAILS_BY_NINUMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@NINumber").Value = NINumber

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            Return dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all users
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/03/2009  1000.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectAllUsers() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtUsers As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_ALL_USERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daUsers As New SqlDataAdapter(cmdCommand)
            daUsers.Fill(dtUsers)
            SelectAllUsers = dtUsers

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve all users." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectMemberLastLogin(ByVal memberID As Integer) As String
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable
        Dim lastLogin As String = String.Empty
        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_MEMBER_LAST_LOGIN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = memberID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            If dtMemberDetail IsNot Nothing AndAlso dtMemberDetail.Rows.Count > 0 Then
                lastLogin = dtMemberDetail.Rows(0).Item("LastLoginDate").ToString
            End If
            Return lastLogin
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member accounts"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member accounts for a given memberid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member accounts datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  15/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectMemberAccountsByMemberID(ByVal iMemberID As Integer _
                                                   , Optional ByVal bIgnoreLoan As Boolean = False _
                                                   , Optional ByVal bIgnoreTransferAllowed As Boolean = False) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_MEMBER_ACCOUNTS_BY_MEMBERID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = iMemberID

            cmdCommand.Parameters.Add("@IgnoreLoan", SqlDbType.Bit)
            cmdCommand.Parameters("@IgnoreLoan").Value = bIgnoreLoan

            cmdCommand.Parameters.Add("@IgnoreTransferAllowed", SqlDbType.Bit)
            cmdCommand.Parameters("@IgnoreTransferAllowed").Value = bIgnoreTransferAllowed

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectMemberAccountsByMemberID = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve member accounts for memberid " & iMemberID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member account details
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member account datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectMemberAccountDetails(ByVal iMemberAccountID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_MEMBER_ACCOUNT_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberAccountID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberAccountID").Value = iMemberAccountID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectMemberAccountDetails = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve account details for memberaccountid " & iMemberAccountID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member transactions"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member transactions for a given memberaccountid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member accounts datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  16/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectTransactionsByMemberAccount(ByVal iMemberAccountID As Integer, ByVal dTransactionDate As DateTime) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_TRANSACTIONS_BY_MEMBER_ACCOUNT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberAccountID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberAccountID").Value = iMemberAccountID

            cmdCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@TransactionDate").Value = IIf(dTransactionDate = "01/01/1900", System.DBNull.Value, dTransactionDate)

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectTransactionsByMemberAccount = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve member transactions for memberaccountid " & iMemberAccountID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member pending transactions for a given memberid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member pending txn datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  16/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPendingTransactionsByMember(ByVal iMemberID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PENDING_TRANSACTIONS_BY_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = iMemberID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectPendingTransactionsByMember = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve pending transactions for member " & iMemberID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select member pending transactions for a given memberid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member pending txn datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  16/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPendingTransactionCount(ByVal iMemberID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PENDING_TRANSACTIONS_COUNT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = iMemberID

            SelectPendingTransactionCount = cmdCommand.ExecuteScalar

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve pending transactions count " & iMemberID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Pending transactions"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert update pendinf transaction
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  22/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertUpdatePendingTransaction(ByVal MemberID As Int32 _
                                     , ByVal RequestedDate As DateTime _
                                     , ByVal TransactionType As Int32 _
                                     , ByVal TransactionDetails As String _
                                     , ByVal RequestCompletedDate As DateTime _
                                     , ByVal StaffID As Int32 _
                                     , ByVal PendingTransactionStatus As Int32 _
                                     , ByVal UserID As Integer _
                                     , ByRef PendingTransactionID As Integer _
                                     , ByVal PDFURL As String _
                                     , ByVal EmailSent As String _
                                     , ByVal MemberMessage As String _
                                     , ByVal ApplicationID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_UPDATE_PENDING_TRANSACTION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID = 0, System.DBNull.Value, MemberID)

            cmdCommand.Parameters.Add("@RequestedDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@RequestedDate").Value = IIf(RequestedDate = "01/01/1900", System.DBNull.Value, RequestedDate)

            cmdCommand.Parameters.Add("@TransactionType", SqlDbType.Int)
            cmdCommand.Parameters("@TransactionType").Value = IIf(TransactionType = 0, System.DBNull.Value, TransactionType)

            cmdCommand.Parameters.Add("@TransactionDetails", SqlDbType.VarChar)
            cmdCommand.Parameters("@TransactionDetails").Value = IIf(TransactionDetails.Length = 0, System.DBNull.Value, TransactionDetails)

            cmdCommand.Parameters.Add("@RequestCompletedDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@RequestCompletedDate").Value = IIf(RequestCompletedDate = "01/01/1900", System.DBNull.Value, RequestCompletedDate)

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = IIf(StaffID = 0, System.DBNull.Value, StaffID)

            cmdCommand.Parameters.Add("@PendingTransactionStatus", SqlDbType.Int)
            cmdCommand.Parameters("@PendingTransactionStatus").Value = IIf(PendingTransactionStatus = 0, System.DBNull.Value, PendingTransactionStatus)

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            If PendingTransactionID > 0 Then
                cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
                cmdCommand.Parameters("@PendingTransactionID").Value = PendingTransactionID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
                cmdCommand.Parameters("@PendingTransactionID").Direction = ParameterDirection.Output
            End If

            If PDFURL.Length > 0 Then
                cmdCommand.Parameters.Add("@PDFURL", SqlDbType.VarChar)
                cmdCommand.Parameters("@PDFURL").Value = PDFURL
            End If

            If EmailSent.Length > 0 Then
                cmdCommand.Parameters.Add("@EmailSent", SqlDbType.VarChar)
                cmdCommand.Parameters("@EmailSent").Value = EmailSent
            End If

            If MemberMessage.Length > 0 Then
                cmdCommand.Parameters.Add("@MemberMessage", SqlDbType.VarChar)
                cmdCommand.Parameters("@MemberMessage").Value = MemberMessage
            End If

            If ApplicationID > 0 Then
                cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
                cmdCommand.Parameters("@ApplicationID").Value = ApplicationID
            End If

            intResult = cmdCommand.ExecuteNonQuery

            If PendingTransactionID <= 0 Then
                PendingTransactionID = CInt(cmdCommand.Parameters("@PendingTransactionID").Value)
            End If

            InsertUpdatePendingTransaction = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert/update pending transaction" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    'Public Function InsertUpdatePendingTransaction(ByVal MemberID As Int32 _
    '                                 , ByVal RequestedDate As DateTime _
    '                                 , ByVal TransactionType As Int32 _
    '                                 , ByVal TransactionDetails As String _
    '                                 , ByVal RequestCompletedDate As DateTime _
    '                                 , ByVal StaffID As Int32 _
    '                                 , ByVal PendingTransactionStatus As Int32 _
    '                                 , ByVal UserID As Integer _
    '                                 , ByRef PendingTransactionID As Integer _
    '                                 , Optional ByVal PDFURL As String = "" _
    '                                 , Optional ByVal EmailSent As String = "" _
    '                                 , Optional ByVal MemberMessage As String = "") As Integer

    '    Dim cmdCommand As New SqlCommand
    '    Dim intResult As Integer

    '    Try
    '        objCon = New SqlConnection(clsConn.getConnectionString)
    '        objCon.Open()

    '        cmdCommand = New SqlCommand("INSERT_UPDATE_PENDING_TRANSACTION", objCon)
    '        cmdCommand.CommandType = CommandType.StoredProcedure

    '        'Input parameters
    '        cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
    '        cmdCommand.Parameters("@MemberID").Value = IIf(MemberID = 0, System.DBNull.Value, MemberID)

    '        cmdCommand.Parameters.Add("@RequestedDate", SqlDbType.DateTime)
    '        cmdCommand.Parameters("@RequestedDate").Value = IIf(RequestedDate = "01/01/1900", System.DBNull.Value, RequestedDate)

    '        cmdCommand.Parameters.Add("@TransactionType", SqlDbType.Int)
    '        cmdCommand.Parameters("@TransactionType").Value = IIf(TransactionType = 0, System.DBNull.Value, TransactionType)

    '        cmdCommand.Parameters.Add("@TransactionDetails", SqlDbType.VarChar)
    '        cmdCommand.Parameters("@TransactionDetails").Value = IIf(TransactionDetails.Length = 0, System.DBNull.Value, TransactionDetails)

    '        cmdCommand.Parameters.Add("@RequestCompletedDate", SqlDbType.DateTime)
    '        cmdCommand.Parameters("@RequestCompletedDate").Value = IIf(RequestCompletedDate = "01/01/1900", System.DBNull.Value, RequestCompletedDate)

    '        cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
    '        cmdCommand.Parameters("@StaffID").Value = IIf(StaffID = 0, System.DBNull.Value, StaffID)

    '        cmdCommand.Parameters.Add("@PendingTransactionStatus", SqlDbType.Int)
    '        cmdCommand.Parameters("@PendingTransactionStatus").Value = IIf(PendingTransactionStatus = 0, System.DBNull.Value, PendingTransactionStatus)

    '        cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
    '        cmdCommand.Parameters("@UserID").Value = UserID

    '        If PendingTransactionID > 0 Then
    '            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
    '            cmdCommand.Parameters("@PendingTransactionID").Value = PendingTransactionID
    '        Else
    '            'Output parameters
    '            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
    '            cmdCommand.Parameters("@PendingTransactionID").Direction = ParameterDirection.Output
    '        End If

    '        If PDFURL.Length > 0 Then
    '            cmdCommand.Parameters.Add("@PDFURL", SqlDbType.VarChar)
    '            cmdCommand.Parameters("@PDFURL").Value = PDFURL
    '        End If

    '        If EmailSent.Length > 0 Then
    '            cmdCommand.Parameters.Add("@EmailSent", SqlDbType.VarChar)
    '            cmdCommand.Parameters("@EmailSent").Value = EmailSent
    '        End If

    '        If MemberMessage.Length > 0 Then
    '            cmdCommand.Parameters.Add("@MemberMessage", SqlDbType.VarChar)
    '            cmdCommand.Parameters("@MemberMessage").Value = MemberMessage
    '        End If


    '        intResult = cmdCommand.ExecuteNonQuery

    '        If PendingTransactionID <= 0 Then
    '            PendingTransactionID = CInt(cmdCommand.Parameters("@PendingTransactionID").Value)
    '        End If

    '        InsertUpdatePendingTransaction = intResult
    '    Catch ex As Exception
    '        Throw New ApplicationException("Failed to insert/update pending transaction" & vbCrLf & ex.Message)
    '    Finally
    '        cmdCommand.Dispose()
    '        objCon.Close()
    '    End Try
    'End Function
#End Region

#Region "Next MemberID From the quota"
    Public Function SelectNextMemberID() As Integer
        Dim cmdCommand As New SqlCommand
        Dim dtMemberID As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NEXT_MEMBERID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberID)

            If dtMemberID.Rows.Count > 0 Then
                SelectNextMemberID = CInt(dtMemberID.Rows(0).Item("NextMemberID"))
            Else
                SelectNextMemberID = 0
            End If

        Catch ex As Exception
            Throw New ApplicationException("Failed to generate next memberid " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member Updates"
    Public Sub UpdateMemberDetails(ByVal MemberID As Integer _
                                         , ByVal MobileNumber As String)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_MEMBER_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@MobileNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@MobileNumber").Value = MobileNumber

            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "Credit Check Related"
    Public Function SelectPendingCreditCheckApps() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PENDING_CREDIT_CHECKS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectPendingCreditCheckApps = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub UpdatePendingCreditCheckCreated(ByVal ApplicationType As String _
                                         , ByVal ApplicationID As Integer)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_PENDING_CREDIT_CHECKS_CREATED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub
#End Region
End Class
