﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls pay day loan related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 12/07/2011 : Created
''' </history>
'''********************************************************************
Public Class clsPayDayLoan
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

#Region "Calculator Activity"
    Public Function InsertCalcActivity(ByVal IPAddress As String, ByVal ChangedBar As Int16, ByVal ValueChanged As Double, ByVal SessionID As String, ByVal CUID As Integer, ByVal MemberID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_CALC_ACTIVITY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@IPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@IPAddress").Value = IPAddress

            cmdCommand.Parameters.Add("@ChangedBar", SqlDbType.TinyInt)
            cmdCommand.Parameters("@ChangedBar").Value = ChangedBar

            cmdCommand.Parameters.Add("@ValueChanged", SqlDbType.Money)
            cmdCommand.Parameters("@ValueChanged").Value = ValueChanged

            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@CUID", SqlDbType.Int)
            cmdCommand.Parameters("@CUID").Value = CUID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Preconfirmed Loan"
    Public Function UpdateCreatePreConfirmedLoan(ByVal SessionID As String _
                                                        , ByVal CUID As Integer _
                                                        , ByVal LoanAmount As Double _
                                                        , ByVal LoanPeriod As Int16 _
                                                        , ByVal Affordability As Double _
                                                        , ByVal TotalInterest As Double _
                                                        , ByVal TotalToPay As Double _
                                                        , ByVal Month1Payment As Double _
                                                        , ByVal Month2Payment As Double _
                                                        , ByVal Month3Payment As Double _
                                                        , ByVal Month4Payment As Double _
                                                        , ByVal Month5Payment As Double _
                                                        , ByVal Month6Payment As Double _
                                                        , ByVal Month7Payment As Double _
                                                        , ByVal Month8Payment As Double _
                                                        , ByVal Month9Payment As Double _
                                                        , ByVal Month10Payment As Double _
                                                        , ByVal Month11Payment As Double _
                                                        , ByVal Month12Payment As Double _
                                                        , ByVal MemberID As Integer _
                                                        , ByVal FirstSavings As Double _
                                                        , ByVal MembershipFee As Double _
                                                        , ByVal InstantLoan As Double _
                                                        , ByVal SameDayPay As Double _
                                                        , ByVal RepeatAccFacility As Double _
                                                        , ByVal IPAddress As String) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_UPDATE_PRECONFIRMED_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@CUID", SqlDbType.Int)
            cmdCommand.Parameters("@CUID").Value = CUID

            cmdCommand.Parameters.Add("@LoanAmount", SqlDbType.Money)
            cmdCommand.Parameters("@LoanAmount").Value = LoanAmount

            cmdCommand.Parameters.Add("@LoanPeriod", SqlDbType.Int)
            cmdCommand.Parameters("@LoanPeriod").Value = LoanPeriod

            cmdCommand.Parameters.Add("@Affordability", SqlDbType.Money)
            cmdCommand.Parameters("@Affordability").Value = Affordability

            cmdCommand.Parameters.Add("@TotalInterest", SqlDbType.Money)
            cmdCommand.Parameters("@TotalInterest").Value = TotalInterest

            cmdCommand.Parameters.Add("@TotalToPay", SqlDbType.Money)
            cmdCommand.Parameters("@TotalToPay").Value = TotalToPay

            cmdCommand.Parameters.Add("@Month1Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month1Payment").Value = Month1Payment

            cmdCommand.Parameters.Add("@Month2Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month2Payment").Value = Month2Payment

            cmdCommand.Parameters.Add("@Month3Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month3Payment").Value = Month3Payment

            cmdCommand.Parameters.Add("@Month4Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month4Payment").Value = Month4Payment

            cmdCommand.Parameters.Add("@Month5Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month5Payment").Value = Month5Payment

            cmdCommand.Parameters.Add("@Month6Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month6Payment").Value = Month6Payment

            cmdCommand.Parameters.Add("@Month7Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month7Payment").Value = Month7Payment

            cmdCommand.Parameters.Add("@Month8Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month8Payment").Value = Month8Payment

            cmdCommand.Parameters.Add("@Month9Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month9Payment").Value = Month9Payment

            cmdCommand.Parameters.Add("@Month10Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month10Payment").Value = Month10Payment

            cmdCommand.Parameters.Add("@Month11Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month11Payment").Value = Month11Payment

            cmdCommand.Parameters.Add("@Month12Payment", SqlDbType.Money)
            cmdCommand.Parameters("@Month12Payment").Value = Month12Payment

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@FirstSavings", SqlDbType.Money)
            cmdCommand.Parameters("@FirstSavings").Value = FirstSavings

            cmdCommand.Parameters.Add("@MembershipFee", SqlDbType.Money)
            cmdCommand.Parameters("@MembershipFee").Value = MembershipFee

            cmdCommand.Parameters.Add("@InstantLoan", SqlDbType.Money)
            cmdCommand.Parameters("@InstantLoan").Value = InstantLoan

            cmdCommand.Parameters.Add("@SameDayPay", SqlDbType.Money)
            cmdCommand.Parameters("@SameDayPay").Value = SameDayPay

            cmdCommand.Parameters.Add("@RepeatAccFacility", SqlDbType.Money)
            cmdCommand.Parameters("@RepeatAccFacility").Value = RepeatAccFacility

            cmdCommand.Parameters.Add("@IPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@IPAddress").Value = IPAddress

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetPreconfirmedLoan(ByVal SessionID As String, ByVal MemberID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_PRECONFIRMED_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = IIf(SessionID.Length > 0, SessionID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdatePreLoggedActivitiesWithMember(ByVal SessionID As String _
                                                        , ByVal MemberID As Integer _
                                                        ) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_PRELOGGED_ACTIVITIES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdatePreConfirmedLoanReApplyEmailSent(ByVal ApplicationID As Integer, ByVal SessionID As String _
                                                           , ByVal StaffID As Integer _
                                                          , Optional ByVal Reset As Boolean = False) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_PRECONFIRMED_LOAN_RECONFIRM_DATE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@Reset", SqlDbType.Bit)
            cmdCommand.Parameters("@Reset").Value = Reset

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateCUCAButPayingFromDifferentAccount(ByVal ApplicationID As Integer, ByVal Status As Boolean _
                                                           , ByVal StaffID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_LOAN_CUCABUTPAYING_DIFFERENT_ACCOUNT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@Status", SqlDbType.Bit)
            cmdCommand.Parameters("@Status").Value = Status

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetReConfirmPDLVerifyStatus(ByVal SessionID As String, ByVal ApplicationID As Integer) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VERIFY_RECONFIRM_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso Convert.ToBoolean(dtTable.Rows(0).Item("ValidStatus")) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function IsSessionIDAlreadyUsed(ByVal SessionID As String) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_SESSIONID_EXISTS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso Convert.ToBoolean(dtTable.Rows(0).Item("SessionExists")) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetPDLPaymentSentStatus(ByVal ApplicationID As Integer, Optional ApplicationType As String = "PDL") As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VERIFY_PAYMENT_SENT_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso Convert.ToBoolean(dtTable.Rows(0).Item("PaymentSent")) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "CU INfo"
    Public Function GetCUInfo(ByVal CUCOde As String) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_CREDITUNION_INFO_BY_CODE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@CUCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CUCode").Value = CUCOde

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Applicant Type"
    Public Function GetApplicantTypeDetails(ByVal ApplicantTypeID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_APPLICANT_TYPE_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicantTypeID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicantTypeID").Value = ApplicantTypeID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Insert Update application"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert update application
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  12/08/2011   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertUpdateApplication(ByVal AppType As String _
                                     , ByVal SessionID As String _
                                     , ByVal MemberID As Integer _
                                     , ByVal CompletedLevel As Int16 _
                                     , ByVal SalutationID As Int32 _
                                     , ByVal FirstName As String _
                                     , ByVal SurName As String _
                                     , ByVal MiddleName As String _
                                     , ByVal Gender As String _
                                     , ByVal DateOfBirth As DateTime _
                                     , ByVal PlaceOfBirth As String _
                                     , ByVal MotherName As String _
                                     , ByVal CurrentAddressLine1 As String _
                                     , ByVal CurrentAddressLine2 As String _
                                     , ByVal CurrentCity As String _
                                     , ByVal CurrentCounty As String _
                                     , ByVal CurrentPostCode As String _
                                     , ByVal CurrentAddressNoOfYears As Int32 _
                                     , ByVal CurrentAddressNoOfMonths As Int32 _
                                     , ByVal HomeTelephone As String _
                                     , ByVal Mobile As String _
                                     , ByVal Email As String _
                                     , ByVal NINumber As String _
                                     , ByVal MaritalStatus As Integer _
                                     , ByVal NoOfDependants As Int32 _
                                     , ByVal PreviousAddressLine1 As String _
                                     , ByVal PreviousAddressLine2 As String _
                                     , ByVal PreviousCity As String _
                                     , ByVal PreviousCounty As String _
                                     , ByVal PreviousPostCode As String _
                                     , ByVal ResidencyStatus As Integer _
                                     , ByVal Q1Answer As Int16 _
                                     , ByVal Q2Answer As Int16 _
                                     , ByVal Q3Answer As Int16 _
                                     , ByVal Q4Answer As Int16 _
                                     , ByVal Q5Answer As Int16 _
                                     , ByVal UserID As Integer _
                                     , ByRef ID As Integer _
                                     , Optional ByVal Purpose As String = "" _
                                     , Optional ByVal EmploymentStatus As Int32 = 0 _
                                     , Optional ByVal CompanyID As Int16 = 0 _
                                     , Optional ByVal CompanyName As String = "" _
                                     , Optional ByVal CompanyDepartment As String = "" _
                                     , Optional ByVal Permanent As Boolean = False _
                                     , Optional ByVal ContractDurationYears As Int32 = 0 _
                                     , Optional ByVal ContractDurationMonths As Int32 = 0 _
                                     , Optional ByVal WorkAddressLine1 As String = "" _
                                     , Optional ByVal WorkAddressLine2 As String = "" _
                                     , Optional ByVal WorkCity As String = "" _
                                     , Optional ByVal WorkCounty As String = "" _
                                     , Optional ByVal WorkPostCode As String = "" _
                                     , Optional ByVal WithEmployerYears As Int32 = 0 _
                                     , Optional ByVal WithEmployerMonths As Int32 = 0 _
                                     , Optional ByVal WorkTelephone As String = "" _
                                     , Optional ByVal JobTitle As String = "" _
                                     , Optional ByVal MonthlyIncomeAfterTax As Double = 0 _
                                     , Optional ByVal WageFrequency As String = "" _
                                     , Optional ByVal NextPayDay As DateTime = Nothing _
                                     , Optional ByVal PayrollNo As String = "" _
                                     , Optional ByVal AccountName As String = "" _
                                     , Optional ByVal AccountNumber As String = "" _
                                     , Optional ByVal AccountSortCode As String = "" _
                                     , Optional ByVal DebitCardNumber As String = "" _
                                     , Optional ByVal BankName As String = "" _
                                     , Optional ByVal TimeWithBankYears As Int16 = 0 _
                                     , Optional ByVal TimeWithBankMonths As Int16 = 0 _
                                     , Optional ByVal TermsAccepted As Boolean = False _
                                     , Optional ByVal Password As String = "" _
                                     , Optional ByVal MemorableInfo As String = "" _
                                     , Optional ByVal GoodHealth As Boolean = False _
                                     , Optional ByVal NormalOccupation As Boolean = False _
                                     , Optional ByVal SecurityCode As Integer = 0 _
                                     , Optional ByVal ApplicantTypeID As Integer = 0 _
                                     , Optional ByVal ExperianFailedMessage As String = "" _
                                     , Optional ByVal ExperianDecisionCode As String = "" _
                                     , Optional ByVal ExperianRef As String = "" _
                                     , Optional ByVal ExperianAuthIndexCode As String = "" _
                                     , Optional ByVal ExperianAuthIndexText As String = "" _
                                     , Optional ByVal ExperianAuthDecisionCode As String = "" _
                                     , Optional ByVal ExperianAuthDecisionText As String = "" _
                                     , Optional ByVal ExperianPolicyCode As String = "" _
                                     , Optional ByVal ExperianPolicyRuleText As String = "" _
                                     , Optional ByVal AccountType As String = "" _
                                     , Optional ByVal CustomerAccountType As String = "" _
                                     , Optional ByVal AccountOwner As String = "" _
                                     , Optional ByVal PaymentFrequencyText As String = "" _
                                     , Optional ByVal IPAddress As String = "" _
                                     , Optional ByVal Epic360 As Boolean = False _
                                     , Optional ByVal PartnerCode As String = ""
                                     ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_UPDATE_APPLICATION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            cmdCommand.Parameters.Add("@SessionID", SqlDbType.VarChar)
            cmdCommand.Parameters("@SessionID").Value = SessionID

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = IIf(MemberID > 0, MemberID, System.DBNull.Value)

            cmdCommand.Parameters.Add("@CompletedLevel", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CompletedLevel").Value = CompletedLevel

            cmdCommand.Parameters.Add("@SalutationID", SqlDbType.Int)
            cmdCommand.Parameters("@SalutationID").Value = SalutationID

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = FirstName

            cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SurName").Value = SurName

            cmdCommand.Parameters.Add("@MiddleName", SqlDbType.VarChar)
            cmdCommand.Parameters("@MiddleName").Value = MiddleName

            cmdCommand.Parameters.Add("@Gender", SqlDbType.VarChar)
            cmdCommand.Parameters("@Gender").Value = Gender

            cmdCommand.Parameters.Add("@DateOfBirth", SqlDbType.DateTime)
            cmdCommand.Parameters("@DateOfBirth").Value = IIf(DateOfBirth = Date.MinValue, System.DBNull.Value, DateOfBirth)

            cmdCommand.Parameters.Add("@PlaceOfBirth", SqlDbType.VarChar)
            cmdCommand.Parameters("@PlaceOfBirth").Value = PlaceOfBirth

            cmdCommand.Parameters.Add("@MotherName", SqlDbType.VarChar)
            cmdCommand.Parameters("@MotherName").Value = MotherName

            cmdCommand.Parameters.Add("@CurrentAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentAddressLine1").Value = CurrentAddressLine1

            cmdCommand.Parameters.Add("@CurrentAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentAddressLine2").Value = CurrentAddressLine2

            cmdCommand.Parameters.Add("@CurrentCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCity").Value = CurrentCity

            cmdCommand.Parameters.Add("@CurrentCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentCounty").Value = CurrentCounty

            cmdCommand.Parameters.Add("@CurrentPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@CurrentPostCode").Value = CurrentPostCode

            cmdCommand.Parameters.Add("@CurrentAddressNoOfYears", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CurrentAddressNoOfYears").Value = CurrentAddressNoOfYears

            cmdCommand.Parameters.Add("@CurrentAddressNoOfMonths", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CurrentAddressNoOfMonths").Value = CurrentAddressNoOfMonths

            cmdCommand.Parameters.Add("@HomeTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@HomeTelephone").Value = HomeTelephone

            cmdCommand.Parameters.Add("@Mobile", SqlDbType.VarChar)
            cmdCommand.Parameters("@Mobile").Value = Mobile

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = Email

            cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@NINumber").Value = NINumber

            cmdCommand.Parameters.Add("@MaritalStatus", SqlDbType.Int)
            cmdCommand.Parameters("@MaritalStatus").Value = MaritalStatus

            cmdCommand.Parameters.Add("@NoOfDependants", SqlDbType.TinyInt)
            cmdCommand.Parameters("@NoOfDependants").Value = NoOfDependants

            cmdCommand.Parameters.Add("@PreviousAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine1").Value = PreviousAddressLine1

            cmdCommand.Parameters.Add("@PreviousAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousAddressLine2").Value = PreviousAddressLine2

            cmdCommand.Parameters.Add("@PreviousCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCity").Value = PreviousCity

            cmdCommand.Parameters.Add("@PreviousCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousCounty").Value = PreviousCounty

            cmdCommand.Parameters.Add("@PreviousPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PreviousPostCode").Value = PreviousPostCode

            cmdCommand.Parameters.Add("@ResidencyStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ResidencyStatus").Value = IIf(ResidencyStatus > 0, ResidencyStatus, 0)

            cmdCommand.Parameters.Add("@Q1Answer", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Q1Answer").Value = Q1Answer

            cmdCommand.Parameters.Add("@Q2Answer", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Q2Answer").Value = Q2Answer

            cmdCommand.Parameters.Add("@Q3Answer", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Q3Answer").Value = Q3Answer

            cmdCommand.Parameters.Add("@Q4Answer", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Q4Answer").Value = Q4Answer

            cmdCommand.Parameters.Add("@Q5Answer", SqlDbType.TinyInt)
            cmdCommand.Parameters("@Q5Answer").Value = Q5Answer

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            If ID > 0 Then
                cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
                cmdCommand.Parameters("@ID").Value = ID
            Else
                'Output parameters
                cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
                cmdCommand.Parameters("@ID").Direction = ParameterDirection.Output
            End If

            'Optional Parameters
            cmdCommand.Parameters.Add("@Purpose", SqlDbType.VarChar)
            cmdCommand.Parameters("@Purpose").Value = IIf(Purpose.Length > 0, Purpose, System.DBNull.Value)

            cmdCommand.Parameters.Add("@EmploymentStatus", SqlDbType.Int)
            cmdCommand.Parameters("@EmploymentStatus").Value = EmploymentStatus

            cmdCommand.Parameters.Add("@CompanyID", SqlDbType.TinyInt)
            cmdCommand.Parameters("@CompanyID").Value = CompanyID

            cmdCommand.Parameters.Add("@CompanyName", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyName").Value = CompanyName

            cmdCommand.Parameters.Add("@CompanyDepartment", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompanyDepartment").Value = CompanyDepartment

            cmdCommand.Parameters.Add("@Permanent", SqlDbType.Bit)
            cmdCommand.Parameters("@Permanent").Value = Permanent

            cmdCommand.Parameters.Add("@ContractDurationYears", SqlDbType.SmallInt)
            cmdCommand.Parameters("@ContractDurationYears").Value = ContractDurationYears

            cmdCommand.Parameters.Add("@ContractDurationMonths", SqlDbType.SmallInt)
            cmdCommand.Parameters("@ContractDurationMonths").Value = ContractDurationMonths

            cmdCommand.Parameters.Add("@WorkAddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine1").Value = WorkAddressLine1

            cmdCommand.Parameters.Add("@WorkAddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkAddressLine2").Value = WorkAddressLine2

            cmdCommand.Parameters.Add("@WorkCity", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCity").Value = WorkCity

            cmdCommand.Parameters.Add("@WorkCounty", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkCounty").Value = WorkCounty

            cmdCommand.Parameters.Add("@WorkPostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkPostCode").Value = WorkPostCode

            cmdCommand.Parameters.Add("@WithEmployerYears", SqlDbType.TinyInt)
            cmdCommand.Parameters("@WithEmployerYears").Value = WithEmployerYears

            cmdCommand.Parameters.Add("@WithEmployerMonths", SqlDbType.TinyInt)
            cmdCommand.Parameters("@WithEmployerMonths").Value = WithEmployerMonths

            cmdCommand.Parameters.Add("@WorkTelephone", SqlDbType.VarChar)
            cmdCommand.Parameters("@WorkTelephone").Value = WorkTelephone

            cmdCommand.Parameters.Add("@JobTitle", SqlDbType.VarChar)
            cmdCommand.Parameters("@JobTitle").Value = JobTitle

            cmdCommand.Parameters.Add("@MonthlyIncomeAfterTax", SqlDbType.Money)
            cmdCommand.Parameters("@MonthlyIncomeAfterTax").Value = MonthlyIncomeAfterTax

            cmdCommand.Parameters.Add("@WageFrequency", SqlDbType.VarChar)
            cmdCommand.Parameters("@WageFrequency").Value = WageFrequency

            cmdCommand.Parameters.Add("@NextPayDay", SqlDbType.DateTime)
            If IsNothing(NextPayDay) OrElse NextPayDay = Date.MinValue Then
                cmdCommand.Parameters("@NextPayDay").Value = System.DBNull.Value
            Else
                cmdCommand.Parameters("@NextPayDay").Value = NextPayDay
            End If

            cmdCommand.Parameters.Add("@PayrollNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@PayrollNo").Value = PayrollNo

            cmdCommand.Parameters.Add("@AccountName", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountName").Value = AccountName

            cmdCommand.Parameters.Add("@AccountNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountNumber").Value = AccountNumber

            cmdCommand.Parameters.Add("@AccountSortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountSortCode").Value = AccountSortCode

            cmdCommand.Parameters.Add("@AccountType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountType").Value = AccountType

            cmdCommand.Parameters.Add("@CustomerAccountType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CustomerAccountType").Value = CustomerAccountType

            cmdCommand.Parameters.Add("@AccountOwner", SqlDbType.VarChar)
            cmdCommand.Parameters("@AccountOwner").Value = AccountOwner

            cmdCommand.Parameters.Add("@DebitCardNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@DebitCardNumber").Value = DebitCardNumber

            cmdCommand.Parameters.Add("@BankName", SqlDbType.VarChar)
            cmdCommand.Parameters("@BankName").Value = BankName

            cmdCommand.Parameters.Add("@TimeWithBankYear", SqlDbType.Int)
            cmdCommand.Parameters("@TimeWithBankYear").Value = TimeWithBankYears

            cmdCommand.Parameters.Add("@TimeWithBankMonth", SqlDbType.Int)
            cmdCommand.Parameters("@TimeWithBankMonth").Value = TimeWithBankMonths

            cmdCommand.Parameters.Add("@TermsAccepted", SqlDbType.Bit)
            cmdCommand.Parameters("@TermsAccepted").Value = TermsAccepted

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = Password

            cmdCommand.Parameters.Add("@MemorableInfo", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemorableInfo").Value = MemorableInfo

            cmdCommand.Parameters.Add("@GoodHealth", SqlDbType.Bit)
            cmdCommand.Parameters("@GoodHealth").Value = GoodHealth

            cmdCommand.Parameters.Add("@NormalOccupation", SqlDbType.VarChar)
            cmdCommand.Parameters("@NormalOccupation").Value = NormalOccupation

            cmdCommand.Parameters.Add("@SecurityCode", SqlDbType.Int)
            cmdCommand.Parameters("@SecurityCode").Value = SecurityCode

            cmdCommand.Parameters.Add("@ApplicantTypeID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicantTypeID").Value = ApplicantTypeID

            'Experian Authentication details
            cmdCommand.Parameters.Add("@ExperianFailedMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianFailedMessage").Value = ExperianFailedMessage

            cmdCommand.Parameters.Add("@ExperianDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianDecisionCode").Value = ExperianDecisionCode

            cmdCommand.Parameters.Add("@ExperianRef", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianRef").Value = ExperianRef

            cmdCommand.Parameters.Add("@ExperianAuthIndexCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexCode").Value = ExperianAuthIndexCode

            cmdCommand.Parameters.Add("@ExperianAuthIndexText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthIndexText").Value = ExperianAuthIndexText

            cmdCommand.Parameters.Add("@ExperianAuthDecisionCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionCode").Value = ExperianAuthDecisionCode

            cmdCommand.Parameters.Add("@ExperianAuthDecisionText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianAuthDecisionText").Value = ExperianAuthDecisionText

            cmdCommand.Parameters.Add("@ExperianPolicyCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyCode").Value = ExperianPolicyCode

            cmdCommand.Parameters.Add("@ExperianPolicyRuleText", SqlDbType.VarChar)
            cmdCommand.Parameters("@ExperianPolicyRuleText").Value = ExperianPolicyRuleText

            cmdCommand.Parameters.Add("@PaymentFrequencyText", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentFrequencyText").Value = PaymentFrequencyText

            cmdCommand.Parameters.Add("@IPAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@IPAddress").Value = IPAddress

            cmdCommand.Parameters.Add("@Epic360", SqlDbType.Bit)
            cmdCommand.Parameters("@Epic360").Value = Epic360

            cmdCommand.Parameters.Add("@PartnerCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PartnerCode").Value = PartnerCode

            intResult = cmdCommand.ExecuteNonQuery

            If ID <= 0 Then
                ID = CInt(cmdCommand.Parameters("@ID").Value)
            End If

            InsertUpdateApplication = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert/update application" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	update application check statuses
    ''' </summary>
    ''' <remarks>
    ''' </remarks>   
    ''' <history>
    '''     [Shan HM]  19/12/2011   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Sub UpdateApplicationCheckStatus(ByVal ApplicationID As Integer _
                                     , Optional ByVal AuthenticationStatus As Integer = -1 _
                                     , Optional ByVal CreditCheckStatus As Integer = -1 _
                                     , Optional ByVal BankCheckStatus As Integer = -1 _
                                     , Optional ByVal AccountActiveStatus As Integer = -1 _
                                     , Optional ByVal EmploymentCheckStatus As Integer = -1 _
                                     )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_APPLICATION_CHECK_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
            cmdCommand.Parameters("@ID").Value = ApplicationID

            If AuthenticationStatus > 0 Then
                cmdCommand.Parameters.Add("@AuthenticationStatus", SqlDbType.Int)
                cmdCommand.Parameters("@AuthenticationStatus").Value = AuthenticationStatus
            End If

            If CreditCheckStatus > 0 Then
                cmdCommand.Parameters.Add("@CreditCheckStatus", SqlDbType.Int)
                cmdCommand.Parameters("@CreditCheckStatus").Value = CreditCheckStatus
            End If

            If AccountActiveStatus > 0 Then
                cmdCommand.Parameters.Add("@AccountActiveStatus", SqlDbType.Int)
                cmdCommand.Parameters("@AccountActiveStatus").Value = AccountActiveStatus
            End If

            If EmploymentCheckStatus > 0 Then
                cmdCommand.Parameters.Add("@EmploymentCheckStatus", SqlDbType.Int)
                cmdCommand.Parameters("@EmploymentCheckStatus").Value = EmploymentCheckStatus
            End If

            If BankCheckStatus > 0 Then
                cmdCommand.Parameters.Add("@BankCheckStatus", SqlDbType.Int)
                cmdCommand.Parameters("@BankCheckStatus").Value = BankCheckStatus
            End If


            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub


    Public Sub UpdateApplicationSendToNewMember(ByVal ApplicationID As Integer)

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_APPLICATION_SEND_TO_NEW_MEMBERCSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
            cmdCommand.Parameters("@ID").Value = ApplicationID

            intResult = cmdCommand.ExecuteNonQuery

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "Loan Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select pay day loan details for a given id
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	PDLoan Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  15/08/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPayDayLoanDetail(ByVal ID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("PDL_SELECT_APPLICATION_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
            cmdCommand.Parameters("@ID").Value = ID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            SelectPayDayLoanDetail = dtLoanDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve application details for id " & ID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Personality Questions"
    Public Function GetPersonalityQuestions() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_PERSONALITY_QUESTIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "PostCode Validation"
    Public Function ValidatePostCode(ByVal PostCode As String, ByVal CUID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable
            Dim Valid As Boolean = False

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_POSTCODE_VALID_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = PostCode

            cmdCommand.Parameters.Add("@CUID", SqlDbType.Int)
            cmdCommand.Parameters("@CUID").Value = CUID

            'Valid = cmdCommand.ExecuteScalar()
            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            'objCon.Close()
        End Try
    End Function
#End Region

#Region "Member Validation"
    Public Function ValidateMember(ByVal FirstName As String, ByVal SurName As String, ByVal DOB As DateTime _
                                   , ByVal PostCode As String, ByVal AddressLine1 As String, ByVal City As String _
                                   , ByVal County As String, ByVal NINumber As String, ByRef AccountStatus As String) As Integer
        Try
            Dim cmdCommand As New SqlCommand
            Dim MemberID As Integer = 0
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_MEMBER_VERIFY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = FirstName

            cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
            cmdCommand.Parameters("@SurName").Value = SurName

            cmdCommand.Parameters.Add("@DateOfBirth", SqlDbType.DateTime)
            cmdCommand.Parameters("@DateOfBirth").Value = DOB

            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = PostCode

            cmdCommand.Parameters.Add("@AddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressLine1").Value = AddressLine1

            cmdCommand.Parameters.Add("@City", SqlDbType.VarChar)
            cmdCommand.Parameters("@City").Value = City

            cmdCommand.Parameters.Add("@County", SqlDbType.VarChar)
            cmdCommand.Parameters("@County").Value = County

            If NINumber.Length > 0 Then
                cmdCommand.Parameters.Add("@NINumber", SqlDbType.VarChar)
                cmdCommand.Parameters("@NINumber").Value = NINumber
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                MemberID = dtTable.Rows(0).Item("MemberID")
                AccountStatus = dtTable.Rows(0).Item("AccountStatus")
            End If

            Return MemberID
        Catch ex As Exception
            Throw ex
        Finally
            'objCon.Close()
        End Try
    End Function
#End Region

#Region "Current Account Validation"
    Public Function ValidateCurrentAccount(ByVal MemberID As Integer, ByVal CurrentAccountNumber As Integer, ByVal ApplicationID As Integer, Optional ApplicationType As String = "PDL") As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VALIDATE_MEMBER_CURRENT_ACCOUNT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@CurrentAccountNumber", SqlDbType.Int)
            cmdCommand.Parameters("@CurrentAccountNumber").Value = CurrentAccountNumber

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            'objCon.Close()
        End Try
    End Function
#End Region

#Region "Payment Instructions"
    Public Function InsertPaymentInstruction(ByVal ApplicationID As Integer, ByVal MemberID As Integer, ByVal DestinationSortCode As String _
                                             , ByVal DestinationAccNumber As String, ByVal DestinationAccName As String, ByVal PaymentRef As String, ByVal AmountInPence As Integer, ByVal TransactionType As Integer _
                                             , ByVal StaffID As Integer, ByVal InstantLoan As Boolean _
                                             , Optional ApplicationType As String = "PDL") As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer = 0
        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_PDL_PAYMENT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@DestinationSortCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@DestinationSortCode").Value = DestinationSortCode

            cmdCommand.Parameters.Add("@DestinationAccNumber", SqlDbType.VarChar)
            cmdCommand.Parameters("@DestinationAccNumber").Value = DestinationAccNumber

            cmdCommand.Parameters.Add("@DestinationAccName", SqlDbType.VarChar)
            cmdCommand.Parameters("@DestinationAccName").Value = DestinationAccName

            cmdCommand.Parameters.Add("@PaymentRef", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentRef").Value = PaymentRef

            cmdCommand.Parameters.Add("@AmountInPence", SqlDbType.Int)
            cmdCommand.Parameters("@AmountInPence").Value = AmountInPence

            cmdCommand.Parameters.Add("@TransactionType", SqlDbType.Int)
            cmdCommand.Parameters("@TransactionType").Value = TransactionType

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@InstantLoan", SqlDbType.Int)
            cmdCommand.Parameters("@InstantLoan").Value = InstantLoan

            'Execute
            intResult = cmdCommand.ExecuteNonQuery
            Return intResult

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member Loan Status"
    Public Function GetMemberLoanExistStatus(ByVal MemberID As Integer) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VALIDATE_MEMBER_PAYING_LOAN", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso dtTable.Rows(0).Item("LoanExists") = 1 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Member Loan Status"
    Public Function GetMemberPayrollStatus(ByVal MemberID As Integer) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VALIDATE_PAYROLL_POSTED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso dtTable.Rows(0).Item("PayrollStatus") = 1 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Previous Pay Day Loan Status"
    Public Function GetPreviousPayDayLoanExistStatus(ByVal ApplicationID As Integer) As Boolean
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable


            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_VALIDATE_PAY_DAY_LOAN_ALREADY_APPLIED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 AndAlso Convert.ToBoolean(dtTable.Rows(0).Item("PDLExists")) Then
                Return True 'Loan exists
            Else
                Return False
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "PDL Product Code"
    Public Function GetPDLProductCodeForMember(ByVal MemberID As Integer) As String
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_PAYDAYLOAN_PRODUCT_CODE_FOR_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Return dtTable.Rows(0).Item("PDLProductCode")
            Else
                Return "PDLSH"
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        clsConn = Nothing
        objCon = Nothing
    End Sub
End Class
