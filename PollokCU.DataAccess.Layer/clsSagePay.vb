﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsSagePay.vb
'''********************************************************************
''' <Summary>
'''	Sage pay response related
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 05/03/2012 : Created
''' </history>
'''********************************************************************
Public Class clsSagePay
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Create Sage Pay Response Server Registration
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  13/03/2012   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function CreateSagePayResponseServerRegistration(ByVal SuccessStatus As Boolean _
                                        , ByVal MemberID As String _
                                        , ByVal FirstName As String _
                                        , ByVal Surname As String _
                                        , ByVal Email As String _
                                        , ByVal AddressLine1 As String _
                                        , ByVal AddressLine2 As String _
                                        , ByVal City As String _
                                        , ByVal PostCode As String _
                                        , ByVal PaymentReason As String _
                                        , ByVal VendorTxCode As String _
                                        , ByVal Status As String _
                                        , ByVal StatusDetail As String _
                                        , ByVal Amount As String _
                                        , ByVal VPSTxId As String _
                                        , ByVal SecurityKey As String _
                                        , ByVal NextURL As String _
                                        , Optional ByVal ApplicationID As Integer = 0 _
                                        , Optional ByVal ApplicationType As String = "PDL" _
                                        ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.INSERT_SAGE_PAY_RESPONSE_SERVER_REGISTER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SuccessStatus", SqlDbType.Bit)
            cmdCommand.Parameters("@SuccessStatus").Value = SuccessStatus

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.VarChar)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FirstName").Value = FirstName

            cmdCommand.Parameters.Add("@Surname", SqlDbType.VarChar)
            cmdCommand.Parameters("@Surname").Value = Surname

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = Email

            cmdCommand.Parameters.Add("@AddressLine1", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressLine1").Value = AddressLine1

            cmdCommand.Parameters.Add("@AddressLine2", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressLine2").Value = AddressLine2

            cmdCommand.Parameters.Add("@City", SqlDbType.VarChar)
            cmdCommand.Parameters("@City").Value = City

            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = PostCode

            cmdCommand.Parameters.Add("@PaymentReason", SqlDbType.VarChar)
            cmdCommand.Parameters("@PaymentReason").Value = PaymentReason

            cmdCommand.Parameters.Add("@VendorTxCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@VendorTxCode").Value = VendorTxCode

            cmdCommand.Parameters.Add("@Status", SqlDbType.VarChar)
            cmdCommand.Parameters("@Status").Value = Status

            cmdCommand.Parameters.Add("@StatusDetail", SqlDbType.VarChar)
            cmdCommand.Parameters("@StatusDetail").Value = StatusDetail

            cmdCommand.Parameters.Add("@Amount", SqlDbType.VarChar)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@VPSTxId", SqlDbType.VarChar)
            cmdCommand.Parameters("@VPSTxId").Value = VPSTxId

            cmdCommand.Parameters.Add("@SecurityKey", SqlDbType.VarChar)
            cmdCommand.Parameters("@SecurityKey").Value = SecurityKey

            cmdCommand.Parameters.Add("@NextURL", SqlDbType.VarChar)
            cmdCommand.Parameters("@NextURL").Value = NextURL

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update Sage Pay Response - Server Notification
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  05/03/2012   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateSagePayResponse(ByVal SuccessStatus As Boolean _
                                        , ByVal VendorTxCode As String _
                                        , ByVal NotificationStatus As String _
                                        , ByVal TxAuthNo As String _
                                        , ByVal AVSCV2 As String _
                                        , ByVal AddressResult As String _
                                        , ByVal PostCodeResult As String _
                                        , ByVal CV2Result As String _
                                        , ByVal GiftAid As String _
                                        , ByVal SecureStatus3D As String _
                                        , ByVal CAVV As String _
                                        , ByVal AddressStatus As String _
                                        , ByVal PayerStatus As String _
                                        , ByVal CardType As String _
                                        , ByVal Last4Digits As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_SAGE_PAY_RESPONSE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SuccessStatus", SqlDbType.Bit)
            cmdCommand.Parameters("@SuccessStatus").Value = SuccessStatus

            cmdCommand.Parameters.Add("@VendorTxCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@VendorTxCode").Value = VendorTxCode

            cmdCommand.Parameters.Add("@NotificationStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@NotificationStatus").Value = NotificationStatus

            cmdCommand.Parameters.Add("@TxAuthNo", SqlDbType.VarChar)
            cmdCommand.Parameters("@TxAuthNo").Value = TxAuthNo

            cmdCommand.Parameters.Add("@AVSCV2", SqlDbType.VarChar)
            cmdCommand.Parameters("@AVSCV2").Value = AVSCV2

            cmdCommand.Parameters.Add("@AddressResult", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressResult").Value = AddressResult

            cmdCommand.Parameters.Add("@PostCodeResult", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCodeResult").Value = PostCodeResult

            cmdCommand.Parameters.Add("@CV2Result", SqlDbType.VarChar)
            cmdCommand.Parameters("@CV2Result").Value = CV2Result

            cmdCommand.Parameters.Add("@GiftAid", SqlDbType.VarChar)
            cmdCommand.Parameters("@GiftAid").Value = GiftAid

            cmdCommand.Parameters.Add("@SecureStatus3D", SqlDbType.VarChar)
            cmdCommand.Parameters("@SecureStatus3D").Value = SecureStatus3D

            cmdCommand.Parameters.Add("@CAVV", SqlDbType.VarChar)
            cmdCommand.Parameters("@CAVV").Value = CAVV

            cmdCommand.Parameters.Add("@AddressStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@AddressStatus").Value = AddressStatus

            cmdCommand.Parameters.Add("@PayerStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@PayerStatus").Value = PayerStatus

            cmdCommand.Parameters.Add("@CardType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CardType").Value = CardType

            cmdCommand.Parameters.Add("@Last4Digits", SqlDbType.VarChar)
            cmdCommand.Parameters("@Last4Digits").Value = Last4Digits

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetSagePayments() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_PAYMENT_RESPONSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetSagePaymentByVendorTxnCode(ByVal VendorTxCode As String) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_PAYMENT_RESPONSE_BY_TXCODE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@VendorTxCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@VendorTxCode").Value = VendorTxCode

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

#Region "Collections Related"
    Public Function GetSageCollectionsSchedule(ByVal ApplicationID As Integer, ByVal ApplicationType As String) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_SAGE_COLLECTION_SCHEDULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetSageCollectionsScheduleByCollectionID(ByVal SagePayCollectionID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_SAGE_COLLECTION_SCHEDULE_BY_COLLECTIONID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@SagePayCollectionID", SqlDbType.Int)
            cmdCommand.Parameters("@SagePayCollectionID").Value = SagePayCollectionID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function UpdateSagePayCollectionEntry(ByVal SagePayCollectionID As Integer _
                                                 , ByVal Amount As Double _
                                                 , ByVal ScheduleDateTime As DateTime _
                                                 , ByVal PaymentTaken As Boolean _
                                                 , ByVal Note As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_SAGE_COLLECTION_SCHEDULE_BY_COLLECTIONID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SagePayCollectionID", SqlDbType.Int)
            cmdCommand.Parameters("@SagePayCollectionID").Value = SagePayCollectionID

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@ScheduleDateTime", SqlDbType.DateTime)
            cmdCommand.Parameters("@ScheduleDateTime").Value = ScheduleDateTime

            cmdCommand.Parameters.Add("@PaymentTaken", SqlDbType.Bit)
            cmdCommand.Parameters("@PaymentTaken").Value = PaymentTaken

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function DeleteSagePayCollectionEntry(ByVal SagePayCollectionID As Integer _
                                                 ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_DELETE_SAGE_COLLECTION_SCHEDULE_BY_COLLECTIONID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SagePayCollectionID", SqlDbType.Int)
            cmdCommand.Parameters("@SagePayCollectionID").Value = SagePayCollectionID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertSagePayCollectionEntry(ByVal ApplicationID As Integer _
                                                 , ByVal Amount As Double _
                                                 , ByVal ScheduleDateTime As DateTime _
                                                 , ByVal PaymentTaken As Boolean _
                                                 , ByVal Note As String _
                                                 , ByVal ApplicationType As String) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_SAGE_COLLECTION_SCHEDULE_ENTRY", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@ScheduleDateTime", SqlDbType.DateTime)
            cmdCommand.Parameters("@ScheduleDateTime").Value = ScheduleDateTime

            cmdCommand.Parameters.Add("@PaymentTaken", SqlDbType.Bit)
            cmdCommand.Parameters("@PaymentTaken").Value = PaymentTaken

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertSagePayCollection(ByVal ApplicationID As Integer _
                                                 ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_SAGE_COLLECTION_SCHEDULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertSagePayCollectionALPSPDL(ByVal ApplicationID As Integer, Optional ByVal ApplicationType As String = "PDLALPS" _
                                                 ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_PDL_INSERT_SAGE_COLLECTION_SCHEDULE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function InsertDuplicateSagePayResponse(ByVal SagePayResponseID As Integer, _
                                                   ByVal ApplicationID As Integer, _
                                                   ByVal ApplicationType As String _
                                                 ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_INSERT_DUPLICATE_SAGE_PAY_RESPONSE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@SagePayResponseID", SqlDbType.Int)
            cmdCommand.Parameters("@SagePayResponseID").Value = SagePayResponseID

            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetNextAvailableSageCollectionsScheduled(Optional ByVal UpdateRuntime As Boolean = False) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_COLLECTION_SCHEDULE_AVAILABLE_TO_REPEAT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@UpdateRunTime", SqlDbType.Bit)
            cmdCommand.Parameters("@UpdateRunTime").Value = UpdateRuntime

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetAllSageCollectionsScheduledUnProcessed() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_COLLECTION_SCHEDULE_ALL_UNPROCESSED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetAllSageCollectionsFailed(ByVal FailedDate As DateTime) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_COLLECTION_SCHEDULE_ALL_FAILED", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If FailedDate <> Date.MinValue Then
                cmdCommand.Parameters.Add("@FailedDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@FailedDate").Value = FailedDate
            End If

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

    Public Function GetAllSageCollectionsSuccess() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_SAGE_COLLECTION_SCHEDULE_ALL_SUCCESS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function

#Region "Other Loan Product Code"
    Public Function GetOtherLoanProductCodeForMember(ByVal MemberID As Integer) As String
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_OTHERLOAN_PRODUCT_CODE_FOR_MEMBER", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Return dtTable.Rows(0).Item("OtherProductCode")
            Else
                Return "PDLSH"
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
#End Region
End Class
