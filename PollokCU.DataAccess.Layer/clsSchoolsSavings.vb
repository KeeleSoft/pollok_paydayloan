﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsUser.vb
'''********************************************************************
''' <Summary>
'''	This module calls user related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 03/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsSchoolsSavings
    Dim clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objCon As SqlConnection



#Region "School Savings Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select school details for a given email and password. Used for login
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectSchoolDetailsByEmalAndPassword(ByVal email As String, ByVal password As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_AUTHENTICATE_SCHOOL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = email

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = password

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolDetailsByEmalAndPassword = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function


    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select school members
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectSchoolMembers(ByVal payrunno As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBERS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@Payrunno", SqlDbType.Int)
            cmdCommand.Parameters("@Payrunno").Value = payrunno

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolMembers = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select school members
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectSchoolMemberDeposits(ByVal payRunNo As Integer, ByVal memberID As Integer, ByVal txnDate As String) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBER_DEPOSITS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PayRunNo", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNo").Value = payRunNo

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            If Not String.IsNullOrEmpty(txnDate) Then
                cmdCommand.Parameters.Add("@TransactionDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@TransactionDate").Value = CDate(txnDate)
            End If

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolMemberDeposits = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectSchoolMemberDepositsByDate(ByVal payRunNo As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBER_DEPOSITS_BY_DATE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PayRunNo", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNo").Value = payRunNo

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolMemberDepositsByDate = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub InsertSchoolMemberDeposit(ByVal payRunNo As Integer, ByVal memberID As Integer, ByVal amount As Double, ByVal transactionDate As Date, ByVal productCode As String)
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.INSERT_SCHOOL_MEMBER_DEPOSITS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PayRunNo", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNo").Value = payRunNo

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = amount

            cmdCommand.Parameters.Add("@TransactionDate", SqlDbType.Date)
            cmdCommand.Parameters("@TransactionDate").Value = transactionDate

            cmdCommand.Parameters.Add("@CreatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@CreatedBy").Value = payRunNo

            cmdCommand.Parameters.Add("@ProductCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@ProductCode").Value = productCode


            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub
#End Region

#Region "School Savings Staff Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select school deposits
    ''' </summary>
    '''--------------------------------------------------------------------
    Public Function SelectSchoolDepositsByStaff(ByVal payRunNo As Integer, ByVal memberID As Integer, Optional ByVal depositDate As String = "01/01/1999") As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBER_DEPOSITS_BY_STAFF", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PayRunNo", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNo").Value = payRunNo

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
            cmdCommand.Parameters("@MemberID").Value = memberID

            If depositDate <> "01/01/1999" Then
                cmdCommand.Parameters.Add("@DepositDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@DepositDate").Value = depositDate
            End If

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolDepositsByStaff = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectSchools() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_ALL_SCHOOLS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchools = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectYSADepositsForCSV(ByVal payRunNumber As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBER_DEPOSITS_FOR_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PayRunNumber", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNumber").Value = payRunNumber

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectYSADepositsForCSV = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub UpdateSchoolDepositArchive(ByVal depositID As Integer, ByVal archive As Boolean, ByVal payRunNo As Integer)
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.UPDTAE_SCHOOL_DEPOSIT_ARCHIVE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@DepositID", SqlDbType.Int)
            cmdCommand.Parameters("@DepositID").Value = depositID

            cmdCommand.Parameters.Add("@Archive", SqlDbType.Bit)
            cmdCommand.Parameters("@Archive").Value = archive

            cmdCommand.Parameters.Add("@PayRunNo", SqlDbType.Int)
            cmdCommand.Parameters("@PayRunNo").Value = payRunNo

            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub

    Public Function SelectYSADepositByID(ByVal depositID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_MEMBER_DEPOSITS_BY_ID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@DepositID", SqlDbType.Int)
            cmdCommand.Parameters("@DepositID").Value = depositID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectYSADepositByID = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub UpdateSchoolDeposit(ByVal depositID As Integer, ByVal amount As Double, ByVal updatedBy As Integer)
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.UPDATE_SCHOOL_MEMBER_DEPOSIT", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@DepositID", SqlDbType.Int)
            cmdCommand.Parameters("@DepositID").Value = depositID

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = amount

            cmdCommand.Parameters.Add("@UpdatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@UpdatedBy").Value = updatedBy

            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub

    Public Function SelectSchoolDetails(ByVal payrunno As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_SCHOOL_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@Payrunno", SqlDbType.Int)
            cmdCommand.Parameters("@Payrunno").Value = payrunno

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectSchoolDetails = dtMemberDetail

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Sub UpdateSchoolDetails(ByVal Payrunno As Integer, ByVal email As String, ByVal password As String, ByVal updatedBy As Integer)
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.UPDATE_SCHOOL_DETAILS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@Payrunno", SqlDbType.Int)
            cmdCommand.Parameters("@Payrunno").Value = Payrunno

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = email

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = password

            cmdCommand.Parameters.Add("@UpdatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@UpdatedBy").Value = updatedBy

            cmdCommand.ExecuteNonQuery()

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Sub
#End Region
End Class
