﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsUser.vb
'''********************************************************************
''' <Summary>
'''	This module calls staff related SPs
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 03/09/2009 : 520.Baseline Created
''' </history>
'''********************************************************************
Public Class clsStaffAdmin
    Dim clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objCon As SqlConnection

#Region "Staff Login Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Authenticate member login
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Member ID
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  03/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function AuthenticateStaffLogin(ByVal sStaffName As String _
                                            , ByVal sPassword As String _
                                            , ByRef bAllowLogin As Boolean _
                                            , ByRef iStaffID As Integer) As Integer
        Dim cmdCommand As New SqlCommand
        Dim iReturn As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_STAFF_LOGIN_AUTHENTICATE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@LoginName", SqlDbType.VarChar)
            cmdCommand.Parameters("@LoginName").Value = sStaffName

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = sPassword

            'Output parameters        
            cmdCommand.Parameters.Add("@AllowLogin", SqlDbType.Bit)
            cmdCommand.Parameters("@AllowLogin").Direction = ParameterDirection.Output

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Direction = ParameterDirection.Output


            iReturn = cmdCommand.ExecuteNonQuery

            'Read all outputp variables
            Boolean.TryParse(cmdCommand.Parameters("@AllowLogin").Value, bAllowLogin)
            Integer.TryParse(cmdCommand.Parameters("@StaffID").Value, iStaffID)

        Catch ex As Exception
            iReturn = -1
            Throw New ApplicationException("Failed to authentice the member" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
            AuthenticateStaffLogin = iReturn
        End Try
    End Function
#End Region

#Region "Staff Details Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select Staff details for a given memberid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Staff Details datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  07/09/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectStaffDetail(ByVal iStaffID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_STAFF_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@StaffID", SqlDbType.VarChar)
            cmdCommand.Parameters("@StaffID").Value = iStaffID

            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectStaffDetail = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve staff details for memberid " & iStaffID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all Staff 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Staff datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  09/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectAllStaff() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtMemberDetail As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_ALL_STAFF", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            Dim daMemberDetail As New SqlDataAdapter(cmdCommand)
            daMemberDetail.Fill(dtMemberDetail)
            SelectAllStaff = dtMemberDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve all staff " & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateStaffPassword(ByVal StaffID As Integer _
                                         , ByVal Password As String _
                                         , ByVal UserID As Integer _
                                         ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_STAFF_PASSWORD", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@Password", SqlDbType.VarChar)
            cmdCommand.Parameters("@Password").Value = Password

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateStaffPassword = intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Data load batch Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all batches
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  08/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectFileControlBatches() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtBatches As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_FILE_CONTROL_BATCH_WITH_STATUS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daBatches As New SqlDataAdapter(cmdCommand)
            daBatches.Fill(dtBatches)
            SelectFileControlBatches = dtBatches

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve all file control batches." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all failed error records for batch
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/05/2010  520.BU01 - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectFileControlErrorsByBatch(ByVal iBatchID) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtErrs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_FILE_CONTROL_ERRORS_BY_BATCH", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@BatchID", SqlDbType.VarChar)
            cmdCommand.Parameters("@BatchID").Value = iBatchID

            Dim daErrs As New SqlDataAdapter(cmdCommand)
            daErrs.Fill(dtErrs)
            SelectFileControlErrorsByBatch = dtErrs

        Catch ex As Exception
            Throw New ApplicationException("Failed to errors." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Staff Pending transaction Related Methods"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all pending transactions
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  08/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPendingTransactionsByStaff(ByVal iStaffID As Integer _
                                                     , Optional MemberID As Integer = 0 _
                                                     , Optional SearchDate As Date = Nothing _
                                                     , Optional Details As String = "" _
                                                     , Optional Status As Integer = 0 _
                                                     , Optional SearchStaffID As Integer = 0) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PENDING_TRANSACTIONS_BY_STAFF", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = iStaffID

            'Search criterias
            If MemberID > 0 Then
                cmdCommand.Parameters.Add("@MemberID", SqlDbType.Int)
                cmdCommand.Parameters("@MemberID").Value = MemberID
            End If

            If SearchDate > Date.MinValue AndAlso SearchDate > "01/01/1900" Then
                cmdCommand.Parameters.Add("@Date", SqlDbType.DateTime)
                cmdCommand.Parameters("@Date").Value = SearchDate
            End If

            If Not String.IsNullOrEmpty(Details) Then
                cmdCommand.Parameters.Add("@Details", SqlDbType.VarChar)
                cmdCommand.Parameters("@Details").Value = Details
            End If

            If Status > 0 Then
                cmdCommand.Parameters.Add("@Status", SqlDbType.Int)
                cmdCommand.Parameters("@Status").Value = Status
            End If

            If SearchStaffID > 0 Then
                cmdCommand.Parameters.Add("@SearchStaffID", SqlDbType.Int)
                cmdCommand.Parameters("@SearchStaffID").Value = SearchStaffID
            End If
            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectPendingTransactionsByStaff = dtTXNs

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve pending transactions." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all pending transactions
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  08/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectPendingTransactionDetail(ByVal iPendingTransactionID) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PENDING_TRANSACTIONS_DETAIL", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
            cmdCommand.Parameters("@PendingTransactionID").Value = iPendingTransactionID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectPendingTransactionDetail = dtTXNs

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve pending transaction detail." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Update a pending txn
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  09/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdatePendingTransaction(ByVal iPendingTransactionID As Integer _
                                            , ByVal iUserID As Integer _
                                            , Optional ByVal dRequestCompletedDate As Date = Nothing _
                                            , Optional ByVal iStaffID As Integer = 0 _
                                            , Optional ByVal iPendingTransactionStatus As Integer = 0 _
                                            , Optional ByVal sMemberMessage As String = "" _
                                     ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("UPDATE_PENDING_TRANSACTION", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
            cmdCommand.Parameters("@PendingTransactionID").Value = iPendingTransactionID

            If dRequestCompletedDate <> "01/01/1900" Then
                cmdCommand.Parameters.Add("@RequestCompletedDate", SqlDbType.DateTime)
                cmdCommand.Parameters("@RequestCompletedDate").Value = dRequestCompletedDate
            End If

            If iStaffID > 0 Then
                cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
                cmdCommand.Parameters("@StaffID").Value = iStaffID
            End If

            If iPendingTransactionStatus > 0 Then
                cmdCommand.Parameters.Add("@PendingTransactionStatus", SqlDbType.Int)
                cmdCommand.Parameters("@PendingTransactionStatus").Value = iPendingTransactionStatus
            End If

            If sMemberMessage.Length > 0 Then
                cmdCommand.Parameters.Add("@MemberMessage", SqlDbType.VarChar)
                cmdCommand.Parameters("@MemberMessage").Value = sMemberMessage
            End If

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = iUserID

            intResult = cmdCommand.ExecuteNonQuery
            UpdatePendingTransaction = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update pending transaction" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note for a given Claim
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  09/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertNote(ByVal iPendingTransactionID As Integer _
                                     , ByVal sNote As String _
                                     , ByVal iCreatedBy As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
            cmdCommand.Parameters("@PendingTransactionID").Value = iPendingTransactionID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = sNote

            cmdCommand.Parameters.Add("@CreatedBy", SqlDbType.Int)
            cmdCommand.Parameters("@CreatedBy").Value = iCreatedBy

            intResult = cmdCommand.ExecuteNonQuery
            InsertNote = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert note" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select all notes for a claim
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	notes datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  09/11/2009  520.Baseline - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectNotes(ByVal iPendingTransactionID As Integer _
                                     ) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtClaimNotes As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            cmdCommand.Parameters.Add("@PendingTransactionID", SqlDbType.Int)
            cmdCommand.Parameters("@PendingTransactionID").Value = iPendingTransactionID

            Dim daClaimNotes As New SqlDataAdapter(cmdCommand)
            daClaimNotes.Fill(dtClaimNotes)
            SelectNotes = dtClaimNotes

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve notes" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "New Member File Download"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select new member file downloads
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/05/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectNewMemberFileDownload(Optional ByVal AppType As String = "Membership") As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NEWMEMBER_FILE_DOWNLOAD_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectNewMemberFileDownload = dtTXNs

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve new member file download list." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select new member file downloads variables
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/05/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectNewMemberFileGenerateVariables(Optional ByVal AppType As String = "Membership") As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NEWMEMBER_FILE_GENERATE_VARIABLES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectNewMemberFileGenerateVariables = dtTable

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve new member file generate variables." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a new member file
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  12/05/2011  Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertNewMemberFileDownload(ByVal FileDate As Date _
                                            , ByVal FileName As String _
                                            , ByVal MembersCount As Integer _
                                            , ByVal UserID As Integer _
                                            , Optional ByVal AppType As String = "Membership" _
                                     ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_NEWMEMBER_FILE_DOWNLOAD", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            cmdCommand.Parameters.Add("@FileName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FileName").Value = FileName

            cmdCommand.Parameters.Add("@MembersCount", SqlDbType.Int)
            cmdCommand.Parameters("@MembersCount").Value = MembersCount

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            intResult = cmdCommand.ExecuteNonQuery
            InsertNewMemberFileDownload = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert new member file" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select new members to generate the CSV
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/05/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectNewMembersToGeneratCSV(ByVal FileDate As Date) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NEWMEMBERS_To_GENERATE_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectNewMembersToGeneratCSV = dtTable

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve new members to generate csv." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select new cuca applications to generate the CSV
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  11/07/2011  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectNewCUCAApplicationsToGeneratCSV(ByVal FileDate As Date) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_NEW_CUCA_APPLICATIONS_TO_GENERATE_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectNewCUCAApplicationsToGeneratCSV = dtTable

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectCUOKNewMembersToGeneratCSV(ByVal FileDate As Date) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("SELECT_PDL_NEWMEMBERS_To_GENERATE_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectCUOKNewMembersToGeneratCSV = dtTable

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve new members to generate csv." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectCUOKApplicationsToGeneratCSV(ByVal FileDate As Date) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_PDL_APPLICATIONS_To_GENERATE_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectCUOKApplicationsToGeneratCSV = dtTable

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function SelectALPSApplicationsToGeneratCSV() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTable As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.AUTOLOAN_SELECT_APPLICATIONS_TO_GENERATE_CSV", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            'cmdCommand.Parameters("@FileDate").Value = FileDate

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)
            SelectALPSApplicationsToGeneratCSV = dtTable

        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "BACS File Downloads"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Select BACS file downloads
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Users datatable
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  17/05/2012  - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function SelectBACSFileDownload(Optional ByVal AppType As String = "ALPS") As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_BACS_FILE_DOWNLOAD_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectBACSFileDownload = dtTXNs

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve bacs file download list." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Insert a new bacs file
    ''' </summary>
    Public Function InsertBacsFileDownload(ByVal FileDate As Date _
                                                , ByVal FileName As String _
                                                , ByVal BacsCount As Integer _
                                                , ByVal UserID As Integer _
                                                , Optional ByVal AppType As String = "ALPS" _
                                         ) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("INSERT_BACS_FILE_DOWNLOAD", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@FileDate", SqlDbType.DateTime)
            cmdCommand.Parameters("@FileDate").Value = FileDate

            cmdCommand.Parameters.Add("@FileName", SqlDbType.VarChar)
            cmdCommand.Parameters("@FileName").Value = FileName

            cmdCommand.Parameters.Add("@BacsCount", SqlDbType.Int)
            cmdCommand.Parameters("@BacsCount").Value = BacsCount

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@AppType", SqlDbType.VarChar)
            cmdCommand.Parameters("@AppType").Value = AppType

            intResult = cmdCommand.ExecuteScalar
            InsertBacsFileDownload = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert new bacs file" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    ''' <summary>
    ''' Get he list to send smss for all alps bacs
    ''' </summary>
    Public Function SelectBACSFileBatchSMSSendList(ByVal fileID As Integer) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtTXNs As New DataTable

        Try
            objCon = New SqlConnection(clsConn.getConnectionString)
            objCon.Open()

            cmdCommand = New SqlCommand("dbo.SELECT_ALPS_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            Dim daTXNs As New SqlDataAdapter(cmdCommand)
            daTXNs.Fill(dtTXNs)
            SelectBACSFileBatchSMSSendList = dtTXNs

        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function UpdateBACSFileBatchSMSSendList(ByVal fileID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.UPDATE_ALPS_BACS_BATCH_SMS_SEND_LIST", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@FileID", SqlDbType.Int)
            cmdCommand.Parameters("@FileID").Value = fileID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateBACSFileBatchSMSSendList = intResult
        Catch ex As Exception
            Throw
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region
End Class
