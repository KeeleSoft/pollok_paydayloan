﻿Imports System.Data.SqlClient
Imports System.Data
'''********************************************************************
''' Project	   : PollokCU.DataAccess.Layer
''' Class      : clsLoan.vb
'''********************************************************************
''' <Summary>
'''	This module calls pay day loan related SPs for staff login
''' </Summary>
''' <remarks>
''' </remarks>
''' <history>
'''     [Shan HM] 12/07/2011 : Created
''' </history>
'''********************************************************************
Public Class clsStaffPayDayLoan
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objCon As SqlConnection





#Region "Loan Applications"
    Public Function GetAllPDLApplications(Optional ByVal FirstName As String = "" _
                                          , Optional ByVal SurName As String = "" _
                                          , Optional ByVal Level As Int16 = 0 _
                                          , Optional ByVal ApplicationStatus As Integer = 0 _
                                          , Optional ByVal PartnerType As String = "" _
                                          , Optional ByVal Epic360 As String = "") As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable
            Dim Epic As Boolean

            If Epic360.Equals("Epic 360") Then
                Epic = True
            End If
            If Epic360.Equals("Non Epic 360") Then
                Epic = False
            End If
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_PDL_APPLICATIONS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            If Not FirstName Is Nothing AndAlso FirstName.Length > 0 Then
                cmdCommand.Parameters.Add("@FirstName", SqlDbType.VarChar)
                cmdCommand.Parameters("@FirstName").Value = FirstName
            End If

            If Not SurName Is Nothing AndAlso SurName.Length > 0 Then
                cmdCommand.Parameters.Add("@SurName", SqlDbType.VarChar)
                cmdCommand.Parameters("@SurName").Value = SurName
            End If

            If Level > 0 Then
                cmdCommand.Parameters.Add("@CompletedLevel", SqlDbType.TinyInt)
                cmdCommand.Parameters("@CompletedLevel").Value = Level
            End If

            If ApplicationStatus > 0 Then
                cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
                cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus
            End If

            If Not PartnerType Is Nothing AndAlso PartnerType.Length > 0 Then
                If Not PartnerType.Equals("All") Then
                    cmdCommand.Parameters.Add("@PartnerCode", SqlDbType.VarChar)
                    cmdCommand.Parameters("@PartnerCode").Value = PartnerType
                End If
            End If
            If Not Epic360 Is Nothing AndAlso Epic360.Length > 0 Then
                If Not Epic360.Equals("All") Then
                    cmdCommand.Parameters.Add("@Epic360", SqlDbType.Bit)
                    cmdCommand.Parameters("@Epic360").Value = Epic
                End If
            End If


            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Loan Statuses"
    Public Function GetAllPDLApplicationStatuses() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_PDL_APPLICATION_STATUSES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "PDL Application"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	update application by staff
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/11/2011   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function UpdateApplicationByStaff(ByVal ID As Integer _
                                     , ByVal ApplicationStatus As Integer _
                                     , ByVal StaffID As Integer _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_UPDATE_APPLICATION_BY_STAFF", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationStatus", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationStatus").Value = ApplicationStatus

            cmdCommand.Parameters.Add("@StaffID", SqlDbType.Int)
            cmdCommand.Parameters("@StaffID").Value = StaffID

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            cmdCommand.Parameters.Add("@ID", SqlDbType.Int)
            cmdCommand.Parameters("@ID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery

            UpdateApplicationByStaff = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to update application" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Notes"
    '''--------------------------------------------------------------------
    ''' <summary>
    ''' 	Insert a note
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <returns>
    ''' 	Execute status
    ''' </returns>
    ''' <history>
    '''     [Shan HM]  30/11/2011   - Created
    ''' </history>
    '''--------------------------------------------------------------------
    Public Function InsertPDLNote(ByVal PDLApplicationID As Integer _
                                     , ByVal Note As String _
                                     , ByVal UserID As Integer) As Integer

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_INSERT_NOTE", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PDLApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@PDLApplicationID").Value = PDLApplicationID

            cmdCommand.Parameters.Add("@Note", SqlDbType.VarChar)
            cmdCommand.Parameters("@Note").Value = Note

            cmdCommand.Parameters.Add("@UserID", SqlDbType.Int)
            cmdCommand.Parameters("@UserID").Value = UserID

            intResult = cmdCommand.ExecuteNonQuery

            InsertPDLNote = intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to insert loan" & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objCon.Close()
        End Try
    End Function

    Public Function GetPDLNotes(ByVal PDLApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_NOTES", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@PDLApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@PDLApplicationID").Value = PDLApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Calc Activity"
    Public Function GetCalcActivityByAppID(ByVal PDLApplicationID As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_CALC_ACTIVITY_BY_APPID", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = PDLApplicationID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region

#Region "Credit Union Usage Stats"
    Public Function GetUsageStatsByCU(ByVal CUID As Integer, ByVal ReportMonth As Integer, ByVal ReportYear As Integer) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objCon Is Nothing OrElse Not objCon.State = ConnectionState.Open Then
                objCon = New SqlConnection(clsConn.getConnectionString)
                objCon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.PDL_SELECT_TRACKING_SERVICE_STATS", objCon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@CUID", SqlDbType.Int)
            cmdCommand.Parameters("@CUID").Value = CUID

            cmdCommand.Parameters.Add("@Month", SqlDbType.Int)
            cmdCommand.Parameters("@Month").Value = ReportMonth

            cmdCommand.Parameters.Add("@Year", SqlDbType.Int)
            cmdCommand.Parameters("@Year").Value = ReportYear

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objCon.Close()
        End Try
    End Function
#End Region
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        clsConn = Nothing
        objCon = Nothing
    End Sub
End Class
