﻿Imports System.Data.SqlClient
Imports System.Data

Public Class clsWorldPay
    Private clsConn As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Private objcon As SqlConnection

    Public Function InsertworldPayData(ByVal ApplicationID As Integer _
                                        , ByVal ApplicationType As String _
                                        , ByVal MemberID As Integer _
                                        , ByVal Country As String _
                                        , ByVal AuthCost As Double _
                                        , ByVal MsgType As String _
                                        , ByVal RouteKey As String _
                                        , ByVal TransId As Int64 _
                                        , ByVal CountryMatch As String _
                                        , ByVal RawAuthMessage As String _
                                        , ByVal AuthCurrency As String _
                                        , ByVal CharEnc As String _
                                        , ByVal CompName As String _
                                        , ByVal RawAuthCode As String _
                                        , ByVal AmountString As String _
                                        , ByVal Installation As Integer _
                                        , ByVal Currency As String _
                                        , ByVal Tel As String _
                                        , ByVal Fax As String _
                                        , ByVal Lang As String _
                                        , ByVal CountryString As String _
                                        , ByVal Email As String _
                                        , ByVal TransStatus As String _
                                        , ByVal SpCharEnc As String _
                                        , ByVal Amount As Double _
                                        , ByVal Address As String _
                                        , ByVal TransTime As String _
                                        , ByVal Cost As Double _
                                        , ByVal Town As String _
                                        , ByVal Address3 As String _
                                        , ByVal Address2 As String _
                                        , ByVal Address1 As String _
                                        , ByVal CartID As String _
                                        , ByVal PostCode As String _
                                        , ByVal IpAddress As String _
                                        , ByVal CardType As String _
                                        , ByVal AuthAmount As Double _
                                        , ByVal AuthMode As String _
                                        , ByVal FuturePayID As Int64 _
                                        , ByVal InstID As Integer _
                                        , ByVal Name As String _
                                        , ByVal CallbackPW As String _
                                        , ByVal Region As String _
                                        , ByVal Avs As Integer _
                                        , ByVal Description As String _
                                        , ByVal AuthAccountString As String
                                        )

        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer

        Try
            If objcon Is Nothing OrElse Not objcon.State = ConnectionState.Open Then
                objcon = New SqlConnection(clsConn.getConnectionString)
                objcon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.INSERT_WORLDPAY_RESPONSE", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@ApplicationID", SqlDbType.Int)
            cmdCommand.Parameters("@ApplicationID").Value = ApplicationID

            cmdCommand.Parameters.Add("@ApplicationType", SqlDbType.VarChar)
            cmdCommand.Parameters("@ApplicationType").Value = ApplicationType

            cmdCommand.Parameters.Add("@MemberID", SqlDbType.BigInt)
            cmdCommand.Parameters("@MemberID").Value = MemberID

            cmdCommand.Parameters.Add("@Country", SqlDbType.VarChar)
            cmdCommand.Parameters("@Country").Value = Country

            cmdCommand.Parameters.Add("@AuthCost", SqlDbType.Money)
            cmdCommand.Parameters("@AuthCost").Value = AuthCost

            cmdCommand.Parameters.Add("@MsgType", SqlDbType.VarChar)
            cmdCommand.Parameters("@MsgType").Value = MsgType

            cmdCommand.Parameters.Add("@RouteKey", SqlDbType.VarChar)
            cmdCommand.Parameters("@RouteKey").Value = RouteKey

            cmdCommand.Parameters.Add("@TransId", SqlDbType.BigInt)
            cmdCommand.Parameters("@TransId").Value = TransId

            cmdCommand.Parameters.Add("@CountryMatch", SqlDbType.VarChar)
            cmdCommand.Parameters("@CountryMatch").Value = CountryMatch

            cmdCommand.Parameters.Add("@RawAuthMessage", SqlDbType.VarChar)
            cmdCommand.Parameters("@RawAuthMessage").Value = RawAuthMessage

            cmdCommand.Parameters.Add("@AuthCurrency", SqlDbType.VarChar)
            cmdCommand.Parameters("@AuthCurrency").Value = AuthCurrency

            cmdCommand.Parameters.Add("@CharEnc", SqlDbType.VarChar)
            cmdCommand.Parameters("@CharEnc").Value = CharEnc

            cmdCommand.Parameters.Add("@CompName", SqlDbType.VarChar)
            cmdCommand.Parameters("@CompName").Value = CompName

            cmdCommand.Parameters.Add("@RawAuthCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@RawAuthCode").Value = RawAuthCode

            cmdCommand.Parameters.Add("@AmountString", SqlDbType.VarChar)
            cmdCommand.Parameters("@AmountString").Value = AmountString

            cmdCommand.Parameters.Add("@Installation", SqlDbType.BigInt)
            cmdCommand.Parameters("@Installation").Value = Installation

            cmdCommand.Parameters.Add("@Currency", SqlDbType.VarChar)
            cmdCommand.Parameters("@Currency").Value = Currency

            cmdCommand.Parameters.Add("@Tel", SqlDbType.VarChar)
            cmdCommand.Parameters("@Tel").Value = Tel

            cmdCommand.Parameters.Add("@Fax", SqlDbType.VarChar)
            cmdCommand.Parameters("@Fax").Value = Fax

            cmdCommand.Parameters.Add("@Lang", SqlDbType.VarChar)
            cmdCommand.Parameters("@Lang").Value = Lang

            cmdCommand.Parameters.Add("@CountryString", SqlDbType.VarChar)
            cmdCommand.Parameters("@CountryString").Value = CountryString

            cmdCommand.Parameters.Add("@Email", SqlDbType.VarChar)
            cmdCommand.Parameters("@Email").Value = Email

            cmdCommand.Parameters.Add("@TransStatus", SqlDbType.VarChar)
            cmdCommand.Parameters("@TransStatus").Value = TransStatus

            cmdCommand.Parameters.Add("@SpCharEnc", SqlDbType.VarChar)
            cmdCommand.Parameters("@SpCharEnc").Value = SpCharEnc

            cmdCommand.Parameters.Add("@Amount", SqlDbType.Money)
            cmdCommand.Parameters("@Amount").Value = Amount

            cmdCommand.Parameters.Add("@Address", SqlDbType.VarChar)
            cmdCommand.Parameters("@Address").Value = Address

            cmdCommand.Parameters.Add("@TransTime", SqlDbType.VarChar)
            cmdCommand.Parameters("@TransTime").Value = TransTime

            cmdCommand.Parameters.Add("@Cost", SqlDbType.Money)
            cmdCommand.Parameters("@Cost").Value = Cost

            cmdCommand.Parameters.Add("@Town", SqlDbType.VarChar)
            cmdCommand.Parameters("@Town").Value = Town

            cmdCommand.Parameters.Add("@Address3", SqlDbType.VarChar)
            cmdCommand.Parameters("@Address3").Value = Address3

            cmdCommand.Parameters.Add("@Address2", SqlDbType.VarChar)
            cmdCommand.Parameters("@Address2").Value = Address2

            cmdCommand.Parameters.Add("@Address1", SqlDbType.VarChar)
            cmdCommand.Parameters("@Address1").Value = Address1

            cmdCommand.Parameters.Add("@CartID", SqlDbType.VarChar)
            cmdCommand.Parameters("@CartID").Value = CartID

            cmdCommand.Parameters.Add("@PostCode", SqlDbType.VarChar)
            cmdCommand.Parameters("@PostCode").Value = PostCode

            cmdCommand.Parameters.Add("@IpAddress", SqlDbType.VarChar)
            cmdCommand.Parameters("@IpAddress").Value = IpAddress

            cmdCommand.Parameters.Add("@CardType", SqlDbType.VarChar)
            cmdCommand.Parameters("@CardType").Value = CardType

            cmdCommand.Parameters.Add("@AuthAmount", SqlDbType.Money)
            cmdCommand.Parameters("@AuthAmount").Value = AuthAmount

            cmdCommand.Parameters.Add("@AuthMode", SqlDbType.VarChar)
            cmdCommand.Parameters("@AuthMode").Value = AuthMode

            cmdCommand.Parameters.Add("@FuturePayID", SqlDbType.BigInt)
            cmdCommand.Parameters("@FuturePayID").Value = FuturePayID

            cmdCommand.Parameters.Add("@InstID", SqlDbType.BigInt)
            cmdCommand.Parameters("@InstID").Value = InstID

            cmdCommand.Parameters.Add("@Name", SqlDbType.VarChar)
            cmdCommand.Parameters("@Name").Value = Name

            cmdCommand.Parameters.Add("@CallbackPW", SqlDbType.VarChar)
            cmdCommand.Parameters("@CallbackPW").Value = CallbackPW

            cmdCommand.Parameters.Add("@Region", SqlDbType.VarChar)
            cmdCommand.Parameters("@Region").Value = Region

            cmdCommand.Parameters.Add("@Avs", SqlDbType.Int)
            cmdCommand.Parameters("@Avs").Value = Avs

            cmdCommand.Parameters.Add("@Description", SqlDbType.VarChar)
            cmdCommand.Parameters("@Description").Value = Description

            cmdCommand.Parameters.Add("@AuthAccountString", SqlDbType.VarChar)
            cmdCommand.Parameters("@AuthAccountString").Value = AuthAccountString

            intResult = cmdCommand.ExecuteNonQuery

            Return intResult
        Catch ex As Exception
            Throw ex
        Finally
            cmdCommand.Dispose()
            objcon.Close()
        End Try
    End Function

    Public Function SelectWorldPayDetail(ByVal ID As Int64) As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtLoanDetail As New DataTable

        Try
            If objcon Is Nothing OrElse Not objcon.State = ConnectionState.Open Then
                objcon = New SqlConnection(clsConn.getConnectionString)
                objcon.Open()
            End If

            cmdCommand = New SqlCommand("PDL_SELECT_WORLDPAY_DETAIL", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@transID", SqlDbType.BigInt)
            cmdCommand.Parameters("@transID").Value = ID

            Dim daLoanDetail As New SqlDataAdapter(cmdCommand)
            daLoanDetail.Fill(dtLoanDetail)
            SelectWorldPayDetail = dtLoanDetail

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve Worldpay details for id " & ID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objcon.Close()
        End Try
    End Function

    Public Function UpdatetWorldPayDetail(ByVal ID As Int64) As Integer
        Dim cmdCommand As New SqlCommand
        Dim intResult As Integer
        Dim dtLoanDetail As New DataTable

        Try
            If objcon Is Nothing OrElse Not objcon.State = ConnectionState.Open Then
                objcon = New SqlConnection(clsConn.getConnectionString)
                objcon.Open()
            End If

            cmdCommand = New SqlCommand("PDL_UPDATE_WORLDPAY_DETAIL", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters
            cmdCommand.Parameters.Add("@transID", SqlDbType.BigInt)
            cmdCommand.Parameters("@transID").Value = ID

            intResult = cmdCommand.ExecuteNonQuery
            Return intResult
        Catch ex As Exception
            Throw New ApplicationException("Failed to Update Worldpay details for id " & ID & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objcon.Close()
        End Try
    End Function

    Public Function GetWorldPayments() As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objcon Is Nothing OrElse Not objcon.State = ConnectionState.Open Then
                objcon = New SqlConnection(clsConn.getConnectionString)
                objcon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_WORLD_PAYMENT_RESPONSES", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objcon.Close()
        End Try
    End Function

    Public Function GetWorldPaymentByTransID(ByVal TransID As String) As DataTable
        Try
            Dim cmdCommand As New SqlCommand
            Dim dtTable As DataTable = New DataTable

            If objcon Is Nothing OrElse Not objcon.State = ConnectionState.Open Then
                objcon = New SqlConnection(clsConn.getConnectionString)
                objcon.Open()
            End If

            cmdCommand = New SqlCommand("dbo.SELECT_WORLD_PAYMENT_RESPONSE_BY_TRANSID", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            cmdCommand.Parameters.Add("@TransId", SqlDbType.VarChar)
            cmdCommand.Parameters("@TransId").Value = TransID

            Dim daAdapter As New SqlDataAdapter(cmdCommand)
            daAdapter.Fill(dtTable)

            Return dtTable
        Catch ex As Exception
            Throw ex
        Finally
            objcon.Close()
        End Try
    End Function


    Public Function SelectEquifaxCredentials() As DataTable
        Dim cmdCommand As New SqlCommand
        Dim dtE As New DataTable

        Try
            objcon = New SqlConnection(clsConn.getConnectionString)
            objcon.Open()

            cmdCommand = New SqlCommand("SELECT_EQUIFAX_CREDENTIALS", objcon)
            cmdCommand.CommandType = CommandType.StoredProcedure

            'Input parameters

            Dim daE As New SqlDataAdapter(cmdCommand)
            daE.Fill(dtE)
            SelectEquifaxCredentials = dtE

        Catch ex As Exception
            Throw New ApplicationException("Failed to retrieve Equifax Credentials." & vbCrLf & ex.Message)
        Finally
            cmdCommand.Dispose()
            objcon.Close()
        End Try
    End Function

End Class
