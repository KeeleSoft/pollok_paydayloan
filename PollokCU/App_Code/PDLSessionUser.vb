﻿Imports Microsoft.VisualBasic
Imports PollokCU.DataAccess.Layer
Imports System.Data
Imports System


Public Class PDLSessionUser
    Dim PDLDataAccess As clsPayDayLoan = New clsPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Private _SSID As String
    Public Property SSID() As String
        Get
            Return _SSID
        End Get
        Set(ByVal value As String)
            _SSID = value
        End Set
    End Property

    Private _CUID As Integer
    Public Property CUID() As Integer
        Get
            Return _CUID
        End Get
        Set(ByVal value As Integer)
            _CUID = value
        End Set
    End Property

    Private _CUCode As String
    Public Property CUCode() As String
        Get
            Return _CUCode
        End Get
        Set(ByVal value As String)
            _CUCode = value
        End Set
    End Property

    Private _CUName As String
    Public Property CUName() As String
        Get
            Return _CUName
        End Get
        Set(ByVal value As String)
            _CUName = value
        End Set
    End Property

    Private _AccountStatus As String
    Public Property AccountStatus As String
        Get
            Return _AccountStatus
        End Get
        Set(value As String)
            _AccountStatus = value
        End Set
    End Property


    Private _MinimumSavingRequirement As Double
    Public Property MinimumSavingRequirement() As Double
        Get
            Return _MinimumSavingRequirement
        End Get
        Set(ByVal value As Double)
            _MinimumSavingRequirement = value
        End Set
    End Property

    Private _MembershipFee As Double
    Public Property MembershipFee() As Double
        Get
            Return _MembershipFee
        End Get
        Set(ByVal value As Double)
            _MembershipFee = value
        End Set
    End Property

    Private _InstantDecisionFee As Double
    Public Property InstantDecisionFee() As Double
        Get
            Return _InstantDecisionFee
        End Get
        Set(ByVal value As Double)
            _InstantDecisionFee = value
        End Set
    End Property

    Private _SamedayPaymentFee As Double
    Public Property SamedayPaymentFee() As Double
        Get
            Return _SamedayPaymentFee
        End Get
        Set(ByVal value As Double)
            _SamedayPaymentFee = value
        End Set
    End Property

    Private _RepeatAccFacility As Double
    Public Property RepeatAccFacility() As Double
        Get
            Return _RepeatAccFacility
        End Get
        Set(ByVal value As Double)
            _RepeatAccFacility = value
        End Set
    End Property

    Private _MemberID As Integer
    Public Property MemberID() As Integer
        Get
            Return _MemberID
        End Get
        Set(ByVal value As Integer)
            _MemberID = value
        End Set
    End Property

    Private _LoanAPR As Double
    Public Property LoanAPR() As Double
        Get
            Return _LoanAPR
        End Get
        Set(ByVal value As Double)
            _LoanAPR = value
        End Set
    End Property

    Private _SecurityCode As Integer = 0
    Public Property SecurityCode() As Integer
        Get
            Return _SecurityCode
        End Get
        Set(ByVal value As Integer)
            _SecurityCode = value
        End Set
    End Property

    Private _ExperianAuthenticateValue As Integer = 0
    Public Property ExperianAuthenticateValue() As Integer
        Get
            Return _ExperianAuthenticateValue
        End Get
        Set(ByVal value As Integer)
            _ExperianAuthenticateValue = value
        End Set
    End Property

    Private _ExperianCreditPassValue As Integer = 0
    Public Property ExperianCreditPassValue() As Integer
        Get
            Return _ExperianCreditPassValue
        End Get
        Set(ByVal value As Integer)
            _ExperianCreditPassValue = value
        End Set
    End Property

    Public Sub New(ByVal SessionID As String, ByVal CUCode As String)
        _SSID = SessionID
        _CUCode = CUCode

        Dim dtCUInfo As DataTable = PDLDataAccess.GetCUInfo(CUCode)
        If dtCUInfo IsNot Nothing AndAlso dtCUInfo.Rows.Count > 0 Then
            CUID = CInt(dtCUInfo.Rows(0)("CUID"))
            CUName = CStr(dtCUInfo.Rows(0)("CUName"))
            MinimumSavingRequirement = CDbl(dtCUInfo.Rows(0)("MinimumSavingRequirement"))
            MembershipFee = CDbl(dtCUInfo.Rows(0)("MembershipFee"))
            InstantDecisionFee = CDbl(dtCUInfo.Rows(0)("InstantDecisionFee"))
            SamedayPaymentFee = CDbl(dtCUInfo.Rows(0)("SamedayPaymentFee"))
            RepeatAccFacility = CDbl(dtCUInfo.Rows(0)("RepeatAccFacility"))
            LoanAPR = CDbl(dtCUInfo.Rows(0)("LoanAPR"))
            ExperianAuthenticateValue = CInt(dtCUInfo.Rows(0)("ExperianAuthenticateValue"))
            ExperianCreditPassValue = CInt(dtCUInfo.Rows(0)("ExperianCreditPassValue"))
        End If
    End Sub

    Public Sub UpdateAllPreLoggedActivitiesWithMemberID()
        Try
            If _MemberID > 0 Then
                PDLDataAccess.UpdatePreLoggedActivitiesWithMember(_SSID, _MemberID)
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, "")
        End Try
    End Sub
End Class

