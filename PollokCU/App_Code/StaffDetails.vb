﻿Imports Microsoft.VisualBasic

Public Class StaffDetails
    Public Enum StaffLevelName
        Admin = 1
        Manager = 2
        StaffMember = 2
    End Enum

    Private _StaffID As Integer
    Public Property StaffID As Integer
        Get
            Return _StaffID
        End Get
        Set(value As Integer)
            _StaffID = value
        End Set
    End Property

    Private _StaffName As String
    Public Property StaffName As String
        Get
            Return _StaffName
        End Get
        Set(value As String)
            _StaffName = value
        End Set
    End Property

    Private _Department As String
    Public Property Department As String
        Get
            Return _Department
        End Get
        Set(value As String)
            _Department = value
        End Set
    End Property

    Private _StaffLevel As Integer
    Public Property StaffLevel As Integer
        Get
            Return _StaffLevel
        End Get
        Set(value As Integer)
            _StaffLevel = value
        End Set
    End Property

    Private _PDLAllowApprove As Boolean
    Public Property PDLAllowApprove As Boolean
        Get
            Return _PDLAllowApprove
        End Get
        Set(value As Boolean)
            _PDLAllowApprove = value
        End Set
    End Property

    Private _PDLAllowPayment As Boolean
    Public Property PDLAllowPayment As Boolean
        Get
            Return _PDLAllowPayment
        End Get
        Set(value As Boolean)
            _PDLAllowPayment = value
        End Set
    End Property

    Private _AllowViewSage As Boolean
    Public Property AllowViewSage As Boolean
        Get
            Return _AllowViewSage
        End Get
        Set(value As Boolean)
            _AllowViewSage = value
        End Set
    End Property

    Private _AllowViewSageCollection As Boolean
    Public Property AllowViewSageCollection As Boolean
        Get
            Return _AllowViewSageCollection
        End Get
        Set(value As Boolean)
            _AllowViewSageCollection = value
        End Set
    End Property

    Private _AllowViewALPS As Boolean
    Public Property AllowViewALPS As Boolean
        Get
            Return _AllowViewALPS
        End Get
        Set(value As Boolean)
            _AllowViewALPS = value
        End Set
    End Property

    Private _AllowManageALPS As Boolean
    Public Property AllowManageALPS As Boolean
        Get
            Return _AllowManageALPS
        End Get
        Set(value As Boolean)
            _AllowManageALPS = value
        End Set
    End Property

    Private _AllowViewCUCA As Boolean
    Public Property AllowViewCUCA As Boolean
        Get
            Return _AllowViewCUCA
        End Get
        Set(value As Boolean)
            _AllowViewCUCA = value
        End Set
    End Property

    Private _AllowViewMemberships As Boolean
    Public Property AllowViewMemberships As Boolean
        Get
            Return _AllowViewMemberships
        End Get
        Set(value As Boolean)
            _AllowViewMemberships = value
        End Set
    End Property

    Private _AllowViewLoans As Boolean
    Public Property AllowViewLoans As Boolean
        Get
            Return _AllowViewLoans
        End Get
        Set(value As Boolean)
            _AllowViewLoans = value
        End Set
    End Property

    Private _AllowViewRegistrations As Boolean
    Public Property AllowViewRegistrations As Boolean
        Get
            Return _AllowViewRegistrations
        End Get
        Set(value As Boolean)
            _AllowViewRegistrations = value
        End Set
    End Property

    Private _AllowFileDownload As Boolean
    Public Property AllowFileDownload As Boolean
        Get
            Return _AllowFileDownload
        End Get
        Set(value As Boolean)
            _AllowFileDownload = value
        End Set
    End Property

    Private _AllowViewCreditLimits As Boolean
    Public Property AllowViewCreditLimits As Boolean
        Get
            Return _AllowViewCreditLimits
        End Get
        Set(value As Boolean)
            _AllowViewCreditLimits = value
        End Set
    End Property

    Private _AllowViewMemberVerify As Boolean
    Public Property AllowViewMemberVerify As Boolean
        Get
            Return _AllowViewMemberVerify
        End Get
        Set(value As Boolean)
            _AllowViewMemberVerify = value
        End Set
    End Property

    Private _AllowViewELMS As Boolean
    Public Property AllowViewELMS As Boolean
        Get
            Return _AllowViewELMS
        End Get
        Set(value As Boolean)
            _AllowViewELMS = value
        End Set
    End Property

    Private _AllowViewELMSMSR As Boolean
    Public Property AllowViewELMSMSR As Boolean
        Get
            Return _AllowViewELMSMSR
        End Get
        Set(value As Boolean)
            _AllowViewELMSMSR = value
        End Set
    End Property

    Private _AllowELMSApprove As Boolean
    Public Property AllowELMSApprove As Boolean
        Get
            Return _AllowELMSApprove
        End Get
        Set(value As Boolean)
            _AllowELMSApprove = value
        End Set
    End Property

    Private _AllowELMSOverMaxApprove As Boolean
    Public Property AllowELMSOverMaxApprove As Boolean
        Get
            Return _AllowELMSOverMaxApprove
        End Get
        Set(value As Boolean)
            _AllowELMSOverMaxApprove = value
        End Set
    End Property

    Private _AllowELMSEnableReCalc As Boolean
    Public Property AllowELMSEnableReCalc As Boolean
        Get
            Return _AllowELMSEnableReCalc
        End Get
        Set(value As Boolean)
            _AllowELMSEnableReCalc = value
        End Set
    End Property

    Private _AllowViewBacsLoans As Boolean
    Public Property AllowViewBacsLoans As Boolean
        Get
            Return _AllowViewBacsLoans
        End Get
        Set(value As Boolean)
            _AllowViewBacsLoans = value
        End Set
    End Property

    Private _AllowBacsLoanAuth As Boolean
    Public Property AllowBacsLoanAuth As Boolean
        Get
            Return _AllowBacsLoanAuth
        End Get
        Set(value As Boolean)
            _AllowBacsLoanAuth = value
        End Set
    End Property

    Private _AllowViewBacsLoanMSR As Boolean
    Public Property AllowViewBacsLoanMSR As Boolean
        Get
            Return _AllowViewBacsLoanMSR
        End Get
        Set(value As Boolean)
            _AllowViewBacsLoanMSR = value
        End Set
    End Property

    Private _AllowViewSMSHistory As Boolean
    Public Property AllowViewSMSHistory As Boolean
        Get
            Return _AllowViewSMSHistory
        End Get
        Set(value As Boolean)
            _AllowViewSMSHistory = value
        End Set
    End Property

    Private _AllowViewCUOK As Boolean
    Public Property AllowViewCUOK As Boolean
        Get
            Return _AllowViewCUOK
        End Get
        Set(value As Boolean)
            _AllowViewCUOK = value
        End Set
    End Property

    Private _AllowViewCUStats As Boolean
    Public Property AllowViewCUStats As Boolean
        Get
            Return _AllowViewCUStats
        End Get
        Set(value As Boolean)
            _AllowViewCUStats = value
        End Set
    End Property

    Private _AllowViewYSA As Boolean
    Public Property AllowViewYSA As Boolean
        Get
            Return _AllowViewYSA
        End Get
        Set(value As Boolean)
            _AllowViewYSA = value
        End Set
    End Property

    Public ReadOnly Property StaffLevelEnum As StaffLevelName
        Get
            Select Case StaffLevel
                Case 1
                    Return StaffLevelName.Admin
                Case 2
                    Return StaffLevelName.Manager
                Case Else
                    Return StaffLevelName.StaffMember
            End Select
        End Get
    End Property

    Public Sub New()
    End Sub

    Public Sub New(StaffDataRow As System.Data.DataRow)
        With StaffDataRow
            _StaffID = .Item("StaffID")
            _StaffName = .Item("StaffName")
            _Department = .Item("Department")
            _StaffLevel = .Item("StaffLevel")
            _PDLAllowApprove = .Item("PDLAllowApprove")
            _PDLAllowPayment = .Item("PDLAllowPayment")
            _AllowViewSage = .Item("AllowViewSage")
            _AllowViewSageCollection = .Item("AllowViewSageCollection")
            _AllowViewALPS = .Item("AllowViewALPS")
            _AllowViewELMS = .Item("AllowViewELMS")
            _AllowViewELMSMSR = .Item("AllowViewELMSMSR")
            _AllowManageALPS = .Item("AllowManageALPS")
            _AllowViewCUCA = .Item("AllowViewCUCA")
            _AllowViewMemberships = .Item("AllowViewMemberships")
            _AllowViewLoans = .Item("AllowViewLoans")
            _AllowViewRegistrations = .Item("AllowViewRegistrations")
            _AllowFileDownload = .Item("AllowFileDownload")
            _AllowViewCreditLimits = .Item("AllowViewCreditLimits")
            _AllowViewMemberVerify = .Item("AllowViewMemberVerify")
            _AllowELMSApprove = .Item("AllowELMSApprove")
            _AllowELMSOverMaxApprove = .Item("AllowELMSOverMaxApprove")
            _AllowELMSEnableReCalc = .Item("AllowELMSEnableReCalc")
            _AllowViewBacsLoans = .Item("AllowViewBacsLoans")
            _AllowBacsLoanAuth = .Item("AllowBacsLoanAuth")
            _AllowViewBacsLoanMSR = .Item("AllowViewBacsLoanMSR")
            _AllowViewSMSHistory = .Item("AllowViewSMSHistory")
            _AllowViewCUOK = .Item("AllowViewCUOK")
            _AllowViewCUStats = .Item("AllowViewCUStats")
            _AllowViewYSA = .Item("AllowViewYSA")
        End With
    End Sub
End Class
