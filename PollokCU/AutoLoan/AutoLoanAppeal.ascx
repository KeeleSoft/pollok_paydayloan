﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AutoLoanAppeal.ascx.vb" Inherits="AutoLoan_AutoLoanAppeal" %>
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Appeal ALPS Applications </span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
</tr>
<tr>
    <td valign="middle">
        <table class="BodyText" style="width:100%;">
            <tr>
                <td>
                    Member ID:</td>
                <td>
                                <strong><asp:Literal ID="litMemberID" runat="server"></asp:Literal></strong>
                            </td>
                <td>
                    Member name:</td>
                <td>
                                <strong><asp:Literal ID="litMemberName" runat="server"></asp:Literal></strong>
                            </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                                &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;<strong>Unfortunately your application cannot be approved automatically. Please enter a brief description and click "Appeal" button to refer your application to the loan department.</strong></td>
</tr>
<tr runat ="server" id="SMSCodePanel" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    Please enter the SMS security code recieved:</td>
                <td>
                    <asp:TextBox ID="txtSecurityCode" runat="server" MaxLength="6" Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr runat ="server" id="ManualVerifyPanel" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    Or verify manually:</td>
                <td>
                    <asp:CheckBox ID="chkManualVerify" runat="server" />
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Enter a brief explanation:</td>
                <td>
                    <asp:TextBox ID="txtExplain" runat="server" Width="300px" Rows="8" 
                        TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnClose" runat="server" Text="I don't want this loan" 
                        Width="170px" BackColor="#00a79d"/>
                </td>
                <td>
                    <asp:Button ID="btnAppeal" runat="server" Text="Appeal" Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
</table>
            
