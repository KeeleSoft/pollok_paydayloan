﻿Imports PollokCU
Partial Class AutoLoan_AutoLoanAppeal
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoanData As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

    Private model As PollokCU.AutoLoanFacade.AutoLoanModel

    Public Event AppealSent()
    Public Event RefuseSent()
    Public Event SessionExpired()

    Public Sub LoadAppealControl()
        If Session("AutoLoanModel") IsNot Nothing Then
            model = TryCast(Session("AutoLoanModel"), PollokCU.AutoLoanFacade.AutoLoanModel)
        End If
        If model IsNot Nothing Then
            litMemberID.Text = model.MemberID.ToString
            litMemberName.Text = model.MemberDetails.FirstName & " " & model.MemberDetails.Surname

            If Not String.IsNullOrEmpty(model.MemberDetails.MobileNumber) Then

                If model.MemberDetails.DateOfBirth > Date.MinValue Then
                    model.SMSSecurityCode = PollokCU.AutoLoanFacade.AutoLoanEngine.GenerateSecurityCode(model.MemberDetails.DateOfBirth) 'objRandom.Next(100000, 999999)
                Else
                    Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))
                    model.SMSSecurityCode = objRandom.Next(100000, 999999)
                End If

                model.SaveAutoLoanModelDetails(2)
                Session("AutoLoanModel") = model
                SMSSecurityCode(model)
                SMSCodePanel.Visible = True
            Else
                SMSCodePanel.Visible = False
            End If
            If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.MemberSite Then
                ManualVerifyPanel.Visible = False
            Else
                ManualVerifyPanel.Visible = True
            End If
        Else    'Session expired
            RaiseEvent SessionExpired()
        End If
    End Sub

    Private Sub SMSSecurityCode(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        lblMessage.Visible = False
        Try
            Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
            Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
            Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
            Body = model.SMSSecurityCode.ToString
            Recipient = model.MemberDetails.MobileNumber


            If Recipient.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No mobile number found to send the security code"
                Return
            End If

            Dim header As esendex.MessengerHeader

            header = New esendex.MessengerHeader
            header.Username = Username
            header.Password = Password
            header.Account = Account

            Dim service As esendex.SendService

            service = New esendex.SendService
            service.MessengerHeaderValue = header
            service.SendMessage(Recipient, Body, esendex.MessageType.Text)

            Dim userID As Integer = 26
            If Session("StaffID") > 0 Then
                userID = Session("StaffID")
            End If
            objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, userID)
            objAutoLoanData.InsertAutoLoanNote(model.AutoLoanID, "Security code " & Body & " Sent To " & Recipient.ToString, userID)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAppeal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppeal.Click
        Try
            lblMessage.Visible = False

            Dim explain As String = txtExplain.Text
            If Not String.IsNullOrEmpty(explain) AndAlso Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)

                If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.MemberSite _
                    AndAlso Not String.IsNullOrEmpty(model.MemberDetails.MobileNumber) _
                    AndAlso SMSCodePanel.Visible _
                    AndAlso (String.IsNullOrEmpty(txtSecurityCode.Text) OrElse txtSecurityCode.Text <> model.SMSSecurityCode) Then

                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid SMS Security code. Please enter the correct security code"
                    Return
                End If
                Dim manualVerify As Boolean = False
                If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.StaffLogin Then
                    If chkManualVerify.Checked Then
                        manualVerify = True
                    Else
                        If String.IsNullOrEmpty(txtSecurityCode.Text) OrElse txtSecurityCode.Text <> model.SMSSecurityCode Then
                            lblMessage.Visible = True
                            lblMessage.Text = "Invalid SMS Security code. Please enter the correct security code"
                            Return
                        End If
                    End If
                End If

                model.AppealReason = explain
                model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Appealed
                model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.Appealed
                model.SaveAutoLoanModelDetails(2)
                RaiseEvent AppealSent() 'Pass back to main page
            Else    'No explanation
                lblMessage.Visible = True
                lblMessage.Text = "Please enter a brief explanation in order to appeal"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            lblMessage.Visible = False

            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            objAutoLoanData.DeleteAutoLoan(model.AutoLoanID)

            'model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Refused_Automatically
            'model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.Refused
            'model.SaveAutoLoanModelDetails(3)
            RaiseEvent RefuseSent() 'Pass back to main page
            
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
