﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AutoLoanMessages.ascx.vb" Inherits="AutoLoan_AutoLoanMessages" %>
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;ALPS Message</span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
        &nbsp;</td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
    <asp:Panel ID="pnlM1" runat="server" Visible="False"><strong>Your application is sent to the loan department. Please contact the PCU for more information. Thank you</strong></asp:Panel>
    <asp:Panel ID="pnlM2" runat="server" Visible="False"><strong>Congratulations, your application has been submitted successfully.  If your application is approved before 3pm during your office opening hours, Your will receive your money on the same day</strong></asp:Panel>
    <asp:Panel ID="pnlM3" runat="server" Visible="False"><strong>Your session is expired. Please start again</strong></asp:Panel>
    <asp:Panel ID="pnlM4" runat="server" Visible="False"><strong>Thank you for your time.</strong></asp:Panel>
    <asp:Panel ID="pnlM5" runat="server" Visible="False"><strong>Thank you for your loan request from the PCU. At present you are not being set up for our Revolving Loan facility. Please get in touch with the credit union for more information.</strong></asp:Panel>
    <asp:Panel ID="pnlM6" runat="server" Visible="False"><strong>Security check failed. Please proceed with the manual security check</strong></asp:Panel>
    <asp:Panel ID="pnlM7" runat="server" Visible="False"><strong>One third rule failed. Loan is saved to process manually. Please use the staff login area to process this loan</strong></asp:Panel>
    <asp:Panel ID="pnlCustom" runat="server" Visible="False"><strong>
        <asp:Literal ID="litCustomMessage" runat="server"></asp:Literal></strong></asp:Panel>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        &nbsp;</td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        &nbsp;</td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
</table>
            
