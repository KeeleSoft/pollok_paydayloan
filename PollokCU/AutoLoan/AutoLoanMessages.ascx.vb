﻿
Partial Class AutoLoan_AutoLoanMessages
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Public Sub ShowMessage(ByVal code As String, Optional customMessage As String = "")
        pnlM1.Visible = (code = "M1")
        pnlM2.Visible = (code = "M2")
        pnlM3.Visible = (code = "M3")
        pnlM4.Visible = (code = "M4")
        pnlM5.Visible = (code = "M5")
        pnlM6.Visible = (code = "M6")
        pnlM7.Visible = (code = "M7")
        pnlCustom.Visible = (code = "custom")
        If Not String.IsNullOrEmpty(customMessage) Then
            litCustomMessage.Text = customMessage
        End If
    End Sub

End Class
