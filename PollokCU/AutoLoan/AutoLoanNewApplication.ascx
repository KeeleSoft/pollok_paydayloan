﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AutoLoanNewApplication.ascx.vb" Inherits="AutoLoan_AutoLoanNewApplication" %>

<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Automated Loan Processing System (ALPS) </span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="40%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Membership No:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                        runat="server" ControlToValidate="txtMemberID" 
                        ErrorMessage="Member ID required">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:TextBox ID="txtMemberID" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Loan amount required:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                        runat="server" ControlToValidate="txtLoanAmount" 
                        ErrorMessage="Loan amount required">*</asp:RequiredFieldValidator>
                </td>
                <td class="style1">
                    <asp:TextBox ID="txtLoanAmount" runat="server" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Loan type:</td>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td width="30%">
                                <asp:DropDownList ID="ddlLoanType" runat="server" Width="204px" AutoPostBack="True">
                    </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkIgnoreOneThirdFailure" runat="server" 
                                    Text="Ignore 1/3rd rule failure" Visible="False" />
                            </td>
                        </tr>
                    </table>
                 </td>
            </tr>
            <tr runat="Server" id="PDLTermRow" visible="True">
                <td>
                    Pay day\OD loan term (months):</td>
                <td>
                    <asp:DropDownList ID="ddlLoanTerm" runat="server" Width="204px">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                    </asp:DropDownList>
                 </td>
            </tr>
             <tr>
                <td>
                    &nbsp;
                    </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="HiddenField1" runat="server" Value="0" />
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" BackColor="#00a79d" />
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
<tr runat="server" id = "memberMessageRowRevLoan" visible="false">
    <td valign="middle" class="BodyText">
    Dear Member<br />
    <strong>New Automated Loan Processing System (ALPS)</strong><br /><br />
    <strong>You could now apply for WeeGlasgow Loan revolving loan provided that you have already set up this facility with the PCU. Please contact us if you would like to set up our WeeGlasgow Loan revolving loan.</strong><br /><br />
    To apply for a loan simply select the loan product that you want to apply from the dropdown menu. The system will prompt you to enter some information. Please go through all those steps to complete the application process. It will only take less than 5 minutes to complete the process successfully.<br /><br />
    Please keep your mobile handset closer to you as we will be sending a SMS security code to your mobile. You will have to enter this security code into the system to complete the process.<br /><br />
    Could you please read the Terms & Conditions of the loan. If you need any further information or if you are having problems of using this facility, please contact our Loan Department on: <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com</a><br /><br />
    Thank you for being a loyal member of the Pollok City Credit Union & selecting us for your financial services.<br /><br />
    
    <strong>Pollok City Credit Union</strong>
    </td>
</tr>
<tr runat="server" id = "memberMessageRowPDL" visible="false">
    <td valign="middle" class="BodyText">
    Dear Member<br />
    <strong>New Automated Loan Processing System (ALPS)</strong><br /><br />
    Assuming that you have already signed up for Revolving Pay Day Loan facility and none of your personal details have changed, you don’t need to complete any application forms.<br /><br />
    Simply enter you member number and the amount you wanted to borrow. Select the 
        Pay Day Loan option and click Submit button.<br /><br />
    The system will calculate your eligibility and let you know if you have been approved/declined for the loan instantly.<br /><br />
    <strong>Pollok City Credit Union</strong>
    </td>
</tr>
</table>
            
