﻿Imports PollokCU
Imports System.Data

Partial Class AutoLoan_AutoLoanNewApplication
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoanData As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

    Private model As AutoLoanFacade.AutoLoanModel

    Public Event PayDayLoanPassed(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event PayDayLoanFailed(ByVal model As AutoLoanFacade.AutoLoanModel)

    Public Event RevLoanPassed(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event RevLoanFailed(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event ShowAvailableOptions(ByVal model As AutoLoanFacade.AutoLoanModel)

    'Dim LoanID As Integer = 0

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            lblMessage.Visible = False

            Dim MemberID As Integer = 0
            Dim LoanAmount As Double = 0
            Dim Type As String = ddlLoanType.SelectedValue
            Dim PDLLoanTerm As Integer = 0
            Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection


            If ValidateInput(MemberID, LoanAmount) Then
                model = New AutoLoanFacade.AutoLoanModel With {.MemberID = MemberID, .LoanAmount = LoanAmount}
                'model.AutoLoanID = 1235
                'model.LoadModel()

                If Session("StaffID") > 0 Then
                    model.UserID = Session("StaffID")
                    model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.StaffLogin
                    model.IgnoreOneThirdFailure = chkIgnoreOneThirdFailure.Checked
                Else
                    model.UserID = 26   'Online User
                    model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.MemberSite
                End If
                If Type = "PayDay" Then
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay
                    model.PDLLoanTerm = ddlLoanTerm.SelectedValue
                ElseIf Type = "ODLoan" Then
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.ODLoan
                    model.PDLLoanTerm = ddlLoanTerm.SelectedValue
                Else
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan
                End If

                Dim Engine As AutoLoanFacade.AutoLoanEngine = New AutoLoanFacade.AutoLoanEngine

                Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters

                Engine.GetMemberDetails(model)

                If model.MemberDetails Is Nothing Then
                    lblMessage.Visible = True
                    lblMessage.Text = "MemberID not found"
                ElseIf model.MemberDetails IsNot Nothing AndAlso Not model.MemberDetails.AllowProceedALPSForStatus Then
                    lblMessage.Visible = True
                    lblMessage.Text = "You are not currently set-up to use this service.  Please contact PCU"
                ElseIf model.MemberDetails IsNot Nothing AndAlso model.MemberDetails.Age >= 70 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Thank you for your loan request, I am afraid this facility is not available to you. If you would like to apply please use the ""apply for a loan"" option."
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso LoanAmount > model.AutoLoanParams.MaxAllowedPDLAmount Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Maximum repeat pay day loan allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedPDLAmount, 2)
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.ODLoan AndAlso LoanAmount > model.AutoLoanParams.MaxAllowedODLoanAmount Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Maximum OD loan allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedODLoanAmount, 2)
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso LoanAmount > model.AutoLoanParams.MaxAllowedRevLoanAmount Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Maximum repeat loan amount allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedRevLoanAmount, 2)
                ElseIf objConfig.getConfigKey("AutoLoanPerformDuplicateCheck") = "TRUE" AndAlso Engine.AutoLoanDuplicated(MemberID, model.LoanType.ToString) Then    'Duplicate check is controlled from the web.config
                    lblMessage.Visible = True
                    lblMessage.Text = "There is an existing loan applied within the last month against your member number. Could not proceed with the loan"
                Else    'Member found, so carry on with tests
                    If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                        Engine.PerformPayDayLoanRule(model)
                        If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PayDayLoanRulePassed AndAlso model.PayDayLoanRule.PDLRiskLimit >= model.LoanAmount Then    'Passed
                            model.FinalLoanAmount = CDbl(txtLoanAmount.Text)
                            model.FinalLoanTerm = CInt(ddlLoanTerm.SelectedValue)
                            'model.Flag = 1
                            'Session("Flag") = 1
                            model.SaveAutoLoanModelDetails(1)
                            Session("Loan") = model.AutoLoanID
                            'HiddenField1.Value = model.AutoLoanID
                            'objAutoLoanData.UpdateAutoLoanGrantedDate(model.AutoLoanID)
                            RaiseEvent PayDayLoanPassed(model)
                            'AutoLoanPDLChecksControl.LoadPDLChecks()
                            'AutoLoanPDLChecksControl.Visible = True

                        Else    'Failed
                            model.SaveAutoLoanModelDetails(1)
                            RaiseEvent PayDayLoanFailed(model)
                        End If
                    ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.ODLoan Then
                        Engine.PerformODLoanRule(model)
                        If model.ODLoanRule IsNot Nothing AndAlso model.ODLoanRule.ODLoanRulePassed AndAlso model.ODLoanRule.ODLoanRiskLimit >= model.LoanAmount Then    'Passed
                            model.SaveAutoLoanModelDetails(1)
                            'objAutoLoanData.UpdateAutoLoanGrantedDate(model.AutoLoanID)
                            RaiseEvent PayDayLoanPassed(model)
                        Else    'Failed
                            model.SaveAutoLoanModelDetails(1)
                            RaiseEvent PayDayLoanFailed(model)
                        End If
                    Else    'Rev loan
                        Engine.GetInitialMemberDetails(model)   'including 1/3rd rule
                        'Engine.PerformOneThirdRule(model)
                        If model.IgnoreOneThirdFailure AndAlso model.OneThirdRule IsNot Nothing AndAlso Not model.OneThirdRule.OneThirdRulePassed Then  '1/3rd rule failed bt staff selected to ignore and proceed
                            model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.OneThirdRuleFailed
                            model.SaveAutoLoanModelDetails(1)
                            Engine.PerformShareToLoanRule(model)    'Do the Share To Loan
                            Engine.PerformDecisionCalculations(model)   'Calculate all possible combinations
                            model.SaveAutoLoanModelDetails(1)
                            RaiseEvent RevLoanFailed(model)
                            Return

                        ElseIf model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.OneThirdRulePassed AndAlso model.RiskLimit > 0 AndAlso IsNumeric(model.AccountNumber) AndAlso CInt(model.AccountNumber) > 0 Then
                            model.SaveAutoLoanModelDetails(1)
                            Engine.PerformShareToLoanRule(model)    'Do the Share To Loan
                            Engine.PerformDecisionCalculations(model)   'Calculate all possible combinations
                            model.SaveAutoLoanModelDetails(1)

                            If model.DecisionCalcs.NoSTLLoanEntitlement >= model.LoanAmount Then
                                model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.LoanAgreedNoConditions
                                model.FinalLoanAmount = model.LoanAmount
                                model.FinalLoanTerm = model.DecisionCalcs.NoSTLRequestedAmountFinalTerm_New
                                model.FinalMonthlyPayment = model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment_New
                                model.FinalOldLoanAmount = model.DecisionCalcs.NoSTLRequestedAmountTotalLoanBalance
                                model.FinalOldLoanTerm = model.DecisionCalcs.NoSTLRequestedAmountFinalTerm
                                model.FinalOldMonthlyPayment = model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment
                                model.FinalMonthlyPaymentForCUCA = model.FinalMonthlyPayment + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment, model.TotalLoansRepayment)

                                model.SaveAutoLoanModelDetails(1)
                                RaiseEvent RevLoanPassed(model) 'All checks passed so can give the loan
                                Return
                            Else
                                RaiseEvent ShowAvailableOptions(model)
                                Return
                            End If
                        Else    'If the one third rule is failed cannot go forward anymore
                            RaiseEvent RevLoanFailed(model)
                            Return
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub SendRescheduleEmail(ByVal model As AutoLoanFacade.AutoLoanModel, ByVal loanAmount As Double, ByVal loanProduct As String, ByVal loanRepay As Double, ByVal loanTerm As Integer)
        Dim msg As String = "Member Number: " & model.MemberID.ToString & " has rescheduled their " & loanAmount & " " & loanProduct & " to " & loanRepay & " over " & loanTerm

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS Current Loan Reschedule" _
                                          , msg, "")

        Dim userID As Integer = 26
        If Session("StaffID") > 0 Then
            userID = Session("StaffID")
        End If
        objAutoLoanData.InsertAutoLoanNote(model.AutoLoanID, "Current loan reschedule email sent " & msg, userID)

    End Sub
    Private Function ValidateInput(ByRef MemberID As Integer, ByRef LoanAmount As Double) As Boolean
        Try
            Dim validateOK As Boolean = True

            Integer.TryParse(txtMemberID.Text, MemberID)
            If MemberID <= 0 Then
                validateOK = False
                lblMessage.Visible = True
                lblMessage.Text = "Invalid MemberID"
                Return validateOK
            End If

            Double.TryParse(txtLoanAmount.Text, LoanAmount)
            If LoanAmount < 50 Then
                validateOK = False
                lblMessage.Visible = True
                lblMessage.Text = "Invalid loan amount"
                Return validateOK
            End If

            Return validateOK
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
            Return False
        End Try
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoanLoanTypeCombo()
            Dim Borrow As Double = CDbl(Request.QueryString("Amount"))
            Dim HowLong As Int16 = Convert.ToInt16(Request.QueryString("Term"))
            'Dim LoanID As Integer = CInt(Request.QueryString("LoanID"))
            txtLoanAmount.Text = Borrow.ToString
            ddlLoanTerm.SelectedIndex = HowLong - 1
            If Session("Loan") IsNot Nothing Then
                Dim dt As DataTable = objAutoLoanData.GetAutoLoanDetail(CInt(Session("Loan")))
                If dt.Rows.Count > 0 Then
                    With dt.Rows(0)
                        txtLoanAmount.Text = .Item("FinalLoanAmount").ToString
                        ddlLoanTerm.SelectedIndex = CInt(.Item("FinalLoanTerm") - 1)
                    End With
                End If
            End If
        End If

        If Session("StaffID") IsNot Nothing AndAlso Session("StaffID") > 0 Then
            txtMemberID.Enabled = True
            memberMessageRowRevLoan.Visible = False
            chkIgnoreOneThirdFailure.Visible = True
        Else
            txtMemberID.Enabled = False
            txtMemberID.Text = Session("MemberID")
            memberMessageRowRevLoan.Visible = True
            chkIgnoreOneThirdFailure.Visible = False
        End If
    End Sub

    Protected Sub ddlLoanType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlLoanType.SelectedIndexChanged
        If ddlLoanType.SelectedValue = "PayDay" Then
            PDLTermRow.Visible = True
            If Session("MemberID") IsNot Nothing AndAlso Session("MemberID") > 0 Then
                memberMessageRowPDL.Visible = True
                memberMessageRowRevLoan.Visible = False
            End If
        ElseIf ddlLoanType.SelectedValue = "RevLoan" Then
            PDLTermRow.Visible = False
            If Session("MemberID") IsNot Nothing AndAlso Session("MemberID") > 0 Then
                memberMessageRowPDL.Visible = False
                memberMessageRowRevLoan.Visible = True
            End If
        ElseIf ddlLoanType.SelectedValue = "ODLoan" Then
            PDLTermRow.Visible = True
            If Session("MemberID") IsNot Nothing AndAlso Session("MemberID") > 0 Then
                memberMessageRowPDL.Visible = False
                memberMessageRowRevLoan.Visible = False
            End If
        End If
    End Sub

    Private Sub LoanLoanTypeCombo()
        'ddlLoanType.Items.Add(New ListItem("Rev Loan", "RevLoan"))
        If Session("MemberID") IsNot Nothing AndAlso Session("MemberID") > 0 Then
            ddlLoanType.Items.Add(New ListItem("Pay Day Loan", "PayDay"))
            'ddlLoanType.Items.Add(New ListItem("CU Through Loan", "ODLoan"))
        End If
    End Sub
End Class
