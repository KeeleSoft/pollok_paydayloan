﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AutoLoanOptions.ascx.vb" Inherits="AutoLoanOptions" %>
<style type="text/css">
    .style1
    {
        height: 15px;
    }
</style>
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Option </span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
</tr>
<tr>
    <td valign="middle" class="style1">
        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
    </td>
</tr>
<tr>
    <td valign="middle">
        <table class="BodyText" style="width:100%;">
            <tr>
                <td>
                    Member ID:</td>
                <td>
                                <strong><asp:Literal ID="litMemberID" runat="server"></asp:Literal></strong>
                            </td>
                <td>
                    Member name:</td>
                <td>
                                <strong><asp:Literal ID="litMemberName" runat="server"></asp:Literal></strong>
                            </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                                &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionNoSTL">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litNoSTLText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionNoSTL" runat="server" Text="Select" BackColor="#00a79d"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionYesSTL" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litYesSTLText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionYesSTL" runat="server" Text="Select" BackColor="#00a79d"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionYesSTLRequestedAmount" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litYesSTLRequestedAmountText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionYesSTLReqAmount" runat="server" Text="Select" BackColor="#00a79d"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionAppeal">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litAppealText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnAppeal" runat="server" Text="Select" BackColor="#00a79d"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionDelete">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litDeleteText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnDelete" runat="server" Text="Select" BackColor="#00a79d"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
</table>
            
