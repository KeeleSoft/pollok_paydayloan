﻿Imports PollokCU

Partial Class AutoLoanOptions
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoanData As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

    Private model As AutoLoanFacade.AutoLoanModel

    Public Event SessionExpired()
    Public Event RevLoanPassed(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event RevLoanFailed(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event ActionAppeal(ByVal model As AutoLoanFacade.AutoLoanModel)
    Public Event LoanAborted()

    Public Sub LoadOption()
        If Session("AutoLoanModel") IsNot Nothing Then
            model = TryCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
        End If
        If model IsNot Nothing Then
            litMemberID.Text = model.MemberID.ToString
            litMemberName.Text = model.MemberDetails.FirstName & " " & model.MemberDetails.Surname

            If model.DecisionCalcs.NoSTLLoanEntitlement > 0 Then
                'Dim noSTLText As String = String.Format("Would you like to borrow {0} for the period of {1} months? Your total loan balance will be {2} and monthly repayment is {3}", FormatNumber(model.DecisionCalcs.NoSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.NoSTLNewLoanTerm, 0), FormatNumber(model.DecisionCalcs.NoSTLTotalLoanBalance, 2), FormatNumber(Math.Ceiling(model.DecisionCalcs.NoSTLRepaymentAmount), 2))
                Dim noSTLText As String = String.Format("Would you like to borrow {0}? Your total monthly repayment would be £{1}.", FormatNumber(model.DecisionCalcs.NoSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.NoSTLRepaymentAmount_New + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.NoSTLRepaymentAmount, model.TotalLoansRepayment), 2))
                litNoSTLText.Text = noSTLText
                OptionNoSTL.Visible = True
            Else
                OptionNoSTL.Visible = False
            End If


            If model.ShareToLoanRule.ShareToLoanRulePassed AndAlso (model.TotalLoansOutstanding > 0 OrElse model.OneThirdRule.CurrentBalance > 0) Then 'Do not offer S2L if current outstanding balance is 0 or less
                If model.LoanAmount < model.DecisionCalcs.YesSTLLoanEntitlement Then    'When the requested amount is less than after STL entitlemenet then give the option for requested amount
                    OptionYesSTL.Visible = False
                    OptionYesSTLRequestedAmount.Visible = True
                    'Dim yesSTLTextReqText As String = String.Format("Would you like to do a share to loan and borrow {0} for the period of {1} months? Your total loan balance will be {2} and monthly repayment is {3}. Share To Loan transfer amount is {4}", FormatNumber(model.LoanAmount, 2), FormatNumber(model.DecisionCalcs.YesSTLRequestedAmountFinalTerm, 0), FormatNumber(model.DecisionCalcs.YesSTLRequestedAmountTotalLoanBalance, 2), FormatNumber(Math.Ceiling(model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment), 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2))
                    Dim yesSTLTextReqText As String = String.Format("Would you like to do a share to loan and borrow {0}. Share To Loan transfer amount is {1}. Your total monthly repayment would be £{2}.", FormatNumber(model.LoanAmount, 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2), FormatNumber(If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment, model.TotalLoansRepayment) + model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment_New, 2))
                    litYesSTLRequestedAmountText.Text = yesSTLTextReqText
                ElseIf model.DecisionCalcs.YesSTLLoanEntitlement > 0 Then
                    OptionYesSTL.Visible = True
                    OptionYesSTLRequestedAmount.Visible = False
                    'Dim yesSTLText As String = String.Format("Would you like to do a share to loan and borrow {0} for the period of {1} months? Your total loan balance will be {2} and monthly repayment is {3}. Share To Loan transfer amount is {4}", FormatNumber(model.DecisionCalcs.YesSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.YesSTLNewLoanTerm, 0), FormatNumber(model.DecisionCalcs.YesSTLTotalLoanBalance, 2), FormatNumber(Math.Ceiling(model.DecisionCalcs.YesSTLRepaymentAmount), 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2))
                    Dim yesSTLText As String = String.Format("Would you like to do a share to loan and borrow {0}? Share To Loan transfer amount is {1}. Your total monthly repayment would be £{2}.", FormatNumber(model.DecisionCalcs.YesSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2), FormatNumber(model.DecisionCalcs.YesSTLRepaymentAmount_New + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.YesSTLRepaymentAmount, model.TotalLoansRepayment), 2))
                    litYesSTLText.Text = yesSTLText
                End If
            Else
                OptionYesSTL.Visible = False
            End If

            litAppealText.Text = "Could not select any of the options and would like to make an appeal?"
            litDeleteText.Text = "I don't want to apply for a loan"
        Else    'Session expired
            RaiseEvent SessionExpired()
        End If
    End Sub



    Protected Sub btnOptionNoSTL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionNoSTL.Click
        If Session("AutoLoanModel") IsNot Nothing Then
            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.NoSTLOptionSelected
            model.FinalLoanAmount = model.DecisionCalcs.NoSTLLoanEntitlement
            model.FinalLoanTerm = model.DecisionCalcs.NoSTLNewLoanTerm_New
            model.FinalMonthlyPayment = model.DecisionCalcs.NoSTLRepaymentAmount_New
            model.ShareToLoanAccepted = False
            model.FinalOldLoanAmount = model.DecisionCalcs.NoSTLTotalLoanBalance
            model.FinalOldLoanTerm = model.DecisionCalcs.NoSTLNewLoanTerm
            model.FinalOldMonthlyPayment = model.DecisionCalcs.NoSTLRepaymentAmount
            model.SaveAutoLoanModelDetails(2)
            model.FinalMonthlyPaymentForCUCA = model.FinalMonthlyPayment + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.NoSTLRepaymentAmount, model.TotalLoansRepayment)
            Session("AutoLoanModel") = model

            RaiseEvent RevLoanPassed(model) 'Slected reduce loan with no STL, So passed
        Else
            RaiseEvent SessionExpired()
        End If
    End Sub


    Protected Sub btnAppeal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppeal.Click
        If Session("AutoLoanModel") IsNot Nothing Then
            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.Appealed
            model.SaveAutoLoanModelDetails(2)
            Session("AutoLoanModel") = model
            RaiseEvent ActionAppeal(model)
        Else
            RaiseEvent SessionExpired()
        End If
    End Sub

    Protected Sub btnOptionYesSTL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionYesSTL.Click
        If Session("AutoLoanModel") IsNot Nothing Then
            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.YesSTLOptionSelected
            model.FinalLoanAmount = model.DecisionCalcs.YesSTLLoanEntitlement
            model.FinalLoanTerm = model.DecisionCalcs.YesSTLNewLoanTerm_New
            model.FinalMonthlyPayment = model.DecisionCalcs.YesSTLRepaymentAmount_New
            model.ShareToLoanAccepted = True
            model.FinalOldLoanAmount = model.DecisionCalcs.YesSTLTotalLoanBalance
            model.FinalOldLoanTerm = model.DecisionCalcs.YesSTLNewLoanTerm
            model.FinalOldMonthlyPayment = model.DecisionCalcs.YesSTLRepaymentAmount
            model.SaveAutoLoanModelDetails(2)
            Session("AutoLoanModel") = model
            model.FinalMonthlyPaymentForCUCA = model.FinalMonthlyPayment + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.YesSTLRepaymentAmount, model.TotalLoansRepayment)
            RaiseEvent RevLoanPassed(model) 'Slected reduce loan with no STL, So passed
        Else
            RaiseEvent SessionExpired()
        End If
    End Sub

    Protected Sub btnOptionYesSTLReqAmount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionYesSTLReqAmount.Click
        If Session("AutoLoanModel") IsNot Nothing Then
            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.YesSTLOptionSelected
            model.FinalLoanAmount = model.LoanAmount
            model.FinalLoanTerm = model.DecisionCalcs.YesSTLRequestedAmountFinalTerm_New
            model.FinalMonthlyPayment = model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment_New
            model.ShareToLoanAccepted = True
            model.FinalOldLoanAmount = model.DecisionCalcs.YesSTLRequestedAmountTotalLoanBalance
            model.FinalOldLoanTerm = model.DecisionCalcs.YesSTLRequestedAmountFinalTerm
            model.FinalOldMonthlyPayment = model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment
            model.SaveAutoLoanModelDetails(2)
            Session("AutoLoanModel") = model
            model.FinalMonthlyPaymentForCUCA = model.FinalMonthlyPayment + If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment, model.TotalLoansRepayment)
            RaiseEvent RevLoanPassed(model) 'Slected reduce loan with no STL, So passed
        Else
            RaiseEvent SessionExpired()
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)

                objAutoLoanData.DeleteAutoLoan(model.AutoLoanID)
                RaiseEvent LoanAborted() 'Loan aborted
            Else
                RaiseEvent SessionExpired()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub SendRescheduleEmail(ByVal model As AutoLoanFacade.AutoLoanModel, ByVal loanAmount As Double, ByVal loanProduct As String, ByVal loanRepay As Double, ByVal loanTerm As Integer)
        Dim msg As String = "Member Number: " & model.MemberID.ToString & " has rescheduled their " & loanAmount & " " & loanProduct & " to " & loanRepay & " over " & loanTerm

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS Current Loan Reschedule" _
                                          , msg, "")

        Dim userID As Integer = 26
        If Session("StaffID") > 0 Then
            userID = Session("StaffID")
        End If
        objAutoLoanData.InsertAutoLoanNote(model.AutoLoanID, "Current loan reschedule email sent " & msg, userID)

    End Sub
End Class
