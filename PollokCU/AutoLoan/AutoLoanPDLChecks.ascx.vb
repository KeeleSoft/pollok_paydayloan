﻿Imports System.Data
Imports SageIncludesServer
Imports System.IO
Imports System.Net
Imports PollokCU
Imports EquifaxFacade

Partial Class AutoLoanPDLChecks
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Shared objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection


    Private model As AutoLoanFacade.AutoLoanModel

    Public Event PaymentInformationCaptured(manualVerifyRequired As Boolean)
    Public Event SessionExpired()
    Public Event PayDayLoanFailed(ByVal model As AutoLoanFacade.AutoLoanModel)

    Private Enum PanelsAvailable
        PaymentOptions
        BankDetails
        CardDetails
        SMSDetails
        SageDetails
    End Enum

    Public Sub LoadPDLChecks()
        If Session("AutoLoanModel") IsNot Nothing Then
            model = TryCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
        End If
        If model IsNot Nothing Then
            litPaymentText.Text = "Please select the prefered payment option."
            ShowHidePanels(PanelsAvailable.PaymentOptions)

            litMemberID.Text = model.MemberID.ToString
            litMemberName.Text = model.MemberDetails.FirstName & " " & model.MemberDetails.Surname
        Else    'Session expired
            RaiseEvent SessionExpired()
        End If
    End Sub

    ''' <summary>
    ''' Confirmed the last 4 digits of the bank acc number aand payment option
    ''' </summary>
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Try
            lblMessage.Visible = False

            Dim PayOption As String = ""
            If Radio1.Checked Then
                PayOption = "FP"
            End If
            If Radio2.Checked Then
                PayOption = "BACS"
            End If
            Dim NextPayDay As Date = Nothing
            Date.TryParse(txtNextPayDay.Text, NextPayDay)
            If (IsDBNull(NextPayDay) OrElse NextPayDay <= Now) Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid next pay day"
                Return
            End If

            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                model.PaymentMethod = PayOption
                model.NextPayDay = NextPayDay
                If model.PaymentMethod = "FP" AndAlso Not model.FasterPayFeeAddedToFinalAmount Then  'Faster payment should be added to final monthly payment
                    model.FinalMonthlyPayment = model.FinalMonthlyPayment + (model.AutoLoanParams.FasterPaymentFee / model.PDLLoanTerm)
                    model.FasterPayFeeAddedToFinalAmount = True
                    model.SaveAutoLoanModelDetails(2)
                End If
                'Check last 4 digits of the bank acc number is a match
                If Not String.IsNullOrEmpty(txtBankAcc4Digits.Text) AndAlso IsNumeric(txtBankAcc4Digits.Text) AndAlso txtBankAcc4Digits.Text.Length = 4 Then
                    Dim last4BankNo As String = txtBankAcc4Digits.Text
                    If model.PayDayLoanRule IsNot Nothing AndAlso Right(model.PayDayLoanRule.LastPaidAccNumber, 4) = last4BankNo Then  'Bank account matched, so check card number last 4 digits
                        model.AccountNumber = model.PayDayLoanRule.LastPaidAccNumber
                        model.SortCode = model.PayDayLoanRule.LastPaidSortCode
                        model.SaveAutoLoanModelDetails(2)
                        ShowHidePanels(PanelsAvailable.SageDetails)
                        'If Not model.IsCUCA Then
                        'ShowHidePanels(PanelsAvailable.CardDetails)
                        'Else    'If CUCA send the SMS 
                        'ProceedWithSMSCheck(model)
                        'End If
                    ElseIf model.ODLoanRule IsNot Nothing AndAlso Right(model.ODLoanRule.LastPaidAccNumber, 4) = last4BankNo Then  'Bank account matched, so check card number last 4 digits
                        model.AccountNumber = model.ODLoanRule.LastPaidAccNumber
                        model.SortCode = model.ODLoanRule.LastPaidSortCode
                        model.SaveAutoLoanModelDetails(2)
                        If Not model.IsCUCA Then
                            ShowHidePanels(PanelsAvailable.CardDetails)
                        Else    'If CUCA send the SMS 
                            ProceedWithSMSCheck(model)
                        End If
                    Else    'Bank account is not matched so proceed with capturing new bank account details
                        ShowHidePanels(PanelsAvailable.BankDetails)
                        For i As Integer = Year(Now) To Year(Now) - 20 Step -1
                            ddlAccYear.Items.Add(i)
                        Next
                    End If

                Else    'Not entered in valid format
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid bank account last 4 digits. Please enter the correct bank account 4 digits"
                    Return
                End If
                Session("AutoLoanModel") = model
            Else    'No explanation
                RaiseEvent SessionExpired()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendCUCAEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim msg As String = "Member Number: " & model.MemberID.ToString & ", " & vbCrLf
        msg &= "Current repayment: " & model.OneThirdRule.CurrentRepayAmount.ToString & ", " & vbCrLf
        msg &= "New repayment: " & Math.Ceiling((Double.Parse(model.FinalMonthlyPayment) + Double.Parse(model.AutoLoanParams.CUCADefaultSaving))).ToString & ", " & vbCrLf
        msg &= "New term (months): " & model.FinalLoanTerm.ToString & ", " & vbCrLf

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS New Repayment Confirmation" _
                                          , msg, "")
        objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Email Sent To CUCA: " & msg, Session("StaffID"))
    End Sub

    Private Sub SendS2LoanEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim msg As String = "Member Number: " & model.MemberID & ", " & vbCrLf
        msg &= "Member name: " & model.MemberDetails.FirstName & " " & model.MemberDetails.Surname & ", " & vbCrLf
        msg &= "Share To Loan Amount: " & model.DecisionCalcs.STLReductionAmount.ToString & ", " & vbCrLf

        Dim email As String = "theweeloan@pollokcu.com"
        'Dim email() As String = {"gayeshan@yahoo.com", "theweeloan@pollokcu.com", "lucky@creditunion.co.uk", "gayeshan@gmail.com"}

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , email _
                                          , "ALPS New S2L Confirmation" _
                                          , msg, "")

        objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Email Sent To Loans: " & msg, Session("StaffID"))
    End Sub

    Private Sub SMSSecurityCode(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        lblMessage.Visible = False
        Try
            Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
            Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
            Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
            Body = model.SMSSecurityCode.ToString
            Recipient = model.MemberDetails.MobileNumber


            If Recipient.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No mobile number found to send the security code"
                Return
            End If

            Dim header As esendex.MessengerHeader

            header = New esendex.MessengerHeader
            header.Username = Username
            header.Password = Password
            header.Account = Account

            Dim service As esendex.SendService

            service = New esendex.SendService
            service.MessengerHeaderValue = header
            service.SendMessage(Recipient, Body, esendex.MessageType.Text)
            objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, Session("StaffID"))
            objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Security code " & Body & " Sent To " & Recipient.ToString, Session("StaffID"))
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnConfirmCardDigits_Click(sender As Object, e As System.EventArgs) Handles btnConfirmCardDigits.Click
        Try
            lblMessage.Visible = False
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                If Not String.IsNullOrEmpty(txtBankCard4Digits.Text) AndAlso IsNumeric(txtBankCard4Digits.Text) AndAlso txtBankCard4Digits.Text.Length = 4 Then
                    Dim last4cardNumbers As String = txtBankCard4Digits.Text
                    If (model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.Last4Digits = last4cardNumbers) OrElse (model.ODLoanRule IsNot Nothing AndAlso model.ODLoanRule.Last4Digits = last4cardNumbers) Then
                        'Proceed with SMS security code
                        ProceedWithSMSCheck(model)
                    Else    'Not matched proceed with sage setup
                        ProceedWithSMSCheck(model)
                    End If
                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid card last 4 digits. Please enter the correct card last 4 digits"
                    Return
                End If

            Else
                lblMessage.Visible = True
                lblMessage.Text = "Session expired. Please start again"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub ProceedWithSMSCheck(model As AutoLoanFacade.AutoLoanModel)
        If model.MemberDetails.DateOfBirth > Date.MinValue Then
            model.SMSSecurityCode = AutoLoanFacade.AutoLoanEngine.GenerateSecurityCode(model.MemberDetails.DateOfBirth) 'objRandom.Next(100000, 999999)
        Else
            Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))
            model.SMSSecurityCode = objRandom.Next(100000, 999999)
        End If

        model.SaveAutoLoanModelDetails(2)

        Session("AutoLoanModel") = model
        SMSSecurityCode(model)
        ShowHidePanels(PanelsAvailable.SMSDetails)
    End Sub

    Protected Sub btnConfirmBankDetails_Click(sender As Object, e As System.EventArgs) Handles btnConfirmBankDetails.Click
        Try
            lblMessage.Visible = False
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                Dim AccNum As String = ""
                If IsNumeric(txtAccountNumber.Text) Then
                    AccNum = txtAccountNumber.Text
                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid account number"
                    Return
                End If

                Dim SortCode As String = ""
                If IsNumeric(txtSortCode.Text) AndAlso txtSortCode.Text.Length = 6 Then
                    SortCode = txtSortCode.Text
                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid sort code"
                    Return
                End If

                model.AccountNumber = AccNum
                model.SortCode = SortCode
                model.AccountOpenYear = ddlAccYear.SelectedValue
                model.AccountOpenMonth = ddlAccMonth.SelectedValue
                model.SaveAutoLoanModelDetails(2)
                Session("AutoLoanModel") = model
                'Do a bank wizard check if not CUCA
                If Not model.IsCUCA Then
                    If BankWizardCheckPassed(model) Then
                        ShowHidePanels(PanelsAvailable.SageDetails)
                        'ProceedWithSMSCheck(model)
                    Else
                        model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Refused_Automatically
                        model.SaveAutoLoanModelDetails(2)
                        Response.Redirect("AutoLoanMessages.aspx?M=M4")
                    End If
                Else    ' CUCA
                    If InternalBankCheckPassed(model) Then
                        ProceedWithSMSCheck(model)
                    Else    'CUCA check failed.
                        model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Refused_Automatically
                        model.SaveAutoLoanModelDetails(2)
                        Response.Redirect("AutoLoanMessages.aspx?M=M4")
                    End If
                End If
            Else
                RaiseEvent SessionExpired()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function BankWizardCheckPassed(mdl As AutoLoanFacade.AutoLoanModel) As Boolean
        Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
        Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
        Dim ExperianWrapper As ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient = New ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient

        Dim PassedBankCheck As Boolean = False

        Dim MemberID As String = mdl.MemberID
        Dim SortCode As String = mdl.SortCode
        Dim AccountNumber As String = mdl.AccountNumber
        Dim AccountSetupDateYear As String = mdl.AccountOpenYear
        Dim AccountSetupDateMonth As String = mdl.AccountOpenMonth
        Dim AccountType As String = "Current"
        Dim CustomerAccountType As String = "Personal"
        Dim FirstName As String = mdl.MemberDetails.FirstName
        Dim SurName As String = mdl.MemberDetails.Surname
        Dim DOBYear As String = Year(mdl.MemberDetails.DateOfBirth)
        Dim DOBMonth As String = Month(mdl.MemberDetails.DateOfBirth)
        Dim DOBDay As String = Day(mdl.MemberDetails.DateOfBirth)

        Dim CurrentAL1 As String = mdl.MemberDetails.AddressLine1
        Dim HouseNumber As String = ""
        Dim FlatNumber As String = ""
        Dim Street As String = ""

        If SortCode = "089401" OrElse SortCode = "089409" Then 'Do the internal bank check as LMCU sort code
            Dim dtBankCheck As DataTable = objPDL.ValidateCurrentAccount(MemberID, AccountNumber, model.AutoLoanID)
            If dtBankCheck IsNot Nothing AndAlso dtBankCheck.Rows.Count > 0 AndAlso dtBankCheck.Rows(0).Item("ValidAccount") = True AndAlso dtBankCheck.Rows(0).Item("ValidDeposits") = True Then
                PassedBankCheck = True
            Else
                PassedBankCheck = False
            End If
        Else
            If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                If HouseNumber.ToLower.Contains("flat") Then
                    FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                    HouseNumber = ""
                End If
                Street = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
            ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                If HouseNumber.ToLower.Contains("flat") Then
                    FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                    HouseNumber = ""
                End If
                Street = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
            End If

            Dim PostCode As String = mdl.MemberDetails.PostCode
            Dim OwnerType As String = "Single"

            'ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
            'If ExperianWrapper.DoBankWizard_Verify(SortCode, AccountNumber, AccountSetupDateYear, AccountSetupDateMonth, AccountType, CustomerAccountType _
            ', FirstName, SurName, DOBYear, DOBMonth, DOBDay, HouseNumber, FlatNumber, Street, PostCode, OwnerType _
            ', mdl.AutoLoanID, Request.ServerVariables("REMOTE_ADDR"), If(mdl.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay, "PDLALPS", "ODLOAN")) Then
            'PassedBankCheck = 1
            'Else
            'PassedBankCheck = 2
            'End If
        End If
        'Return PassedBankCheck

        Dim dtTXN As DataTable
        dtTXN = objAutoLoan.GetAutoLoanDetail(model.AutoLoanID)
        With dtTXN.Rows(0)
            Dim id As Integer = CInt(dtTXN.Rows(0).Item("AutoLoanID"))
            Dim mid As Integer = CInt(IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), 0))
            Dim fname As String = dtTXN.Rows(0).Item("FirstName")
            Dim sname As String = dtTXN.Rows(0).Item("Surname")
            Dim apptype As String = dtTXN.Rows(0).Item("AppType")
            Dim midname As String = "" 'dtTXN.Rows(0).Item("MiddleName")
            Dim SrtCode As String = dtTXN.Rows(0).Item("PaymentSortCodeDec")
            Dim AccNumber As String = dtTXN.Rows(0).Item("PaymentAccountNumberDec")

            Dim AddressYear As String = dtTXN.Rows(0).Item("CurrentAddressNoOfYears")
            Dim AddressMonth As String = dtTXN.Rows(0).Item("CurrentAddressNoOfMonths")
            Dim BankYear As String = mdl.AccountOpenYear 'dtTXN.Rows(0).Item("TimeWithBankYear")
            Dim BankMonth As String = mdl.AccountOpenMonth 'dtTXN.Rows(0).Item("TimeWithBankMonth")

            Dim CurrentAddressLine1 As String = dtTXN.Rows(0).Item("CurrentAddressLine1")
            Dim CurrentAddressLine2 As String = "" 'dtTXN.Rows(0).Item("CurrentAddressLine2")
            Dim CurrentCity As String = dtTXN.Rows(0).Item("CurrentCity")
            Dim CurrentCounty As String = dtTXN.Rows(0).Item("CurrentCounty")
            Dim CurrentPostCode As String = dtTXN.Rows(0).Item("CurrentPostCode")

            Dim PreviousAddressLine1 As String = dtTXN.Rows(0).Item("PreviousAddressLine1")
            Dim PreviousAddressLine2 As String = "" 'dtTXN.Rows(0).Item("PreviousAddressLine2")
            Dim PreviousCity As String = dtTXN.Rows(0).Item("PreviousCity")
            Dim PreviousCounty As String = dtTXN.Rows(0).Item("PreviousCounty")
            Dim PreviousPostcode As String = dtTXN.Rows(0).Item("PreviousPostCode")
            Dim Years As Integer = CInt(dtTXN.Rows(0).Item("CurrentAddressNoOfYears"))

            PreviousCounty = PreviousCounty.Replace(" ", "")

            Dim substring1 As String = SortCode.Substring(0, 2)
            Dim substring2 As String = SortCode.Substring(2, 2)
            Dim substring3 As String = SortCode.Substring(4)

            Dim srtcd As String = substring1 & "-" & substring2 & "-" & substring3

            Dim BYear As Integer = DateTime.Now.Year - BankYear

            Dim timebank As String = "P" & BYear & "Y" & BankMonth & "M"
            Dim timeaddress As String = "P" & AddressYear & "Y" & AddressMonth & "M"
            'Dim timebank As String = "P11Y5M"
            'Dim timeaddress As String = "P9Y11M"
            'Dim addressid As String = "58150004386"

            Dim V As clsChecks = New clsChecks()
            Dim status As Integer = V.BankCheck(AccountNumber, srtcd, timebank, sname, fname, midname, timeaddress, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, id, apptype, mid)

            ' objPDL.UpdateApplicationCheckStatus(id, , , status)

            If status = 1 Then
                PassedBankCheck = True
            ElseIf status = 2 Then
                PassedBankCheck = False
            Else
                PassedBankCheck = False
            End If
        End With
        Return PassedBankCheck
    End Function

    Private Function InternalBankCheckPassed(mdl As AutoLoanFacade.AutoLoanModel) As Boolean
        Try
            Dim PassedBankCheck As Boolean = False
            Dim dtBankCheck As DataTable = objPayDayLoan.ValidateCurrentAccount(mdl.MemberID, mdl.AccountNumber, mdl.AutoLoanID, "PDLALPS")
            If dtBankCheck IsNot Nothing AndAlso dtBankCheck.Rows.Count > 0 AndAlso dtBankCheck.Rows(0).Item("ValidAccount") = True AndAlso dtBankCheck.Rows(0).Item("ValidDeposits") = True Then
                PassedBankCheck = True
            End If

            Return PassedBankCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Protected Sub btnSMSCodeConfirm_Click(sender As Object, e As System.EventArgs) Handles btnSMSCodeConfirm.Click
        Try
            lblMessage.Visible = False
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                If txtSMSCode.Text = model.SMSSecurityCode Then
                    If model.IsCUCA Then
                        'Send the cuca email to setup SO
                        SendCUCAPDLALPSEmail(model)
                    ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                        'Duplicate the current 1p payment record as we are not going through a new sage pay setup
                        objSage.InsertDuplicateSagePayResponse(model.PayDayLoanRule.LastSagePayResponseID, model.AutoLoanID, "PDLALPS")
                        objSage.InsertSagePayCollectionALPSPDL(model.AutoLoanID)
                        SendPDLShare2Loan(model)    'Sed the PDLShare email if required
                        SendSettleFromLastSagePaymentEmail(model) 'Send Settle from last sage payment email if required
                    ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.ODLoan Then
                        'Duplicate the current 1p payment record as we are not going through a new sage pay setup
                        objSage.InsertDuplicateSagePayResponse(model.ODLoanRule.LastSagePayResponseID, model.AutoLoanID, "ODLOAN")
                        objSage.InsertSagePayCollectionALPSPDL(model.AutoLoanID, "ODLOAN")
                    End If

                    If model.PaymentMethod = "FP" Then
                        Dim PayRef As String = model.AutoLoanID.ToString & "-" & model.MemberID.ToString
                        Dim LoanAmountInPence As Integer = Integer.Parse(model.FinalLoanAmount) * 100
                        Dim TxnType As Integer = 99

                        objPayDayLoan.InsertPaymentInstruction(model.AutoLoanID, model.MemberID _
                                                               , model.SortCode, model.AccountNumber _
                                                               , model.MemberDetails.FirstName & " " & model.MemberDetails.Surname _
                                                               , PayRef, LoanAmountInPence, TxnType, 26, True, If(model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay, "PDLALPS", "ODLOAN")) '26=Online.User Staff Account
                    ElseIf model.PaymentMethod = "BACS" Then
                        objAutoLoan.UpdateAutoLoanSendToBACS(model.AutoLoanID, 26)
                    End If
                    model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically
                    model.SaveAutoLoanModelDetails(3)
                    If model.PaymentMethod = "FP" Then
                        ShowHidePanels(PanelsAvailable.SageDetails)
                    Else
                        Response.Redirect("AutoLoanMessages.aspx?M=MSG1&T=" & If(model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.ODLoan, "OD", "PDL"))
                    End If
                Else
                    model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Security_Failed
                    model.SaveAutoLoanModelDetails(2)
                    Response.Redirect("AutoLoanMessages.aspx?M=M4")
                    'lblMessage.Visible = True
                    'lblMessage.Text = "Security code entered is not valid"
                End If
            Else
                RaiseEvent SessionExpired()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendCUCAPDLALPSEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            Dim sEmailFrom As String = "noreply@cuonline.org.uk"
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

            Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetCUCAPDLALPSEmailBody(model)

            objEmail.SendEmailMessage(sEmailFrom _
                                         , "theweeloan@pollokcu.com" _
                                         , "PDL ALPS Standing Order Setup - AUTO EMAIL" _
                                         , emailBody _
                                         , "" _
                                         , ""
                                         )

            objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "CUCA email sent: " & emailBody, Session("StaffID"))

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendPDLShare2Loan(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PDLShareToLoanRequired Then
                Dim sEmailFrom As String = "noreply@cuonline.org.uk"
                Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

                Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetPDLShare2LoanEmailBody(model)

                objEmail.SendEmailMessage(sEmailFrom _
                                             , "theweeloan@pollokcu.com" _
                                             , "PDL ALPS PDLShare Required - AUTO EMAIL" _
                                             , emailBody _
                                             , "" _
                                             , ""
                                             )

                objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "PDL Share2Loan email sent: " & emailBody, Session("StaffID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendSettleFromLastSagePaymentEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.SettleFromLastSagePayment Then
                Dim sEmailFrom As String = "noreply@cuonline.org.uk"
                Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

                Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetSettleFromLastSagePaymentEmailBody(model)

                objEmail.SendEmailMessage(sEmailFrom _
                                             , "theweeloan@pollokcu.com" _
                                             , "PDL ALPS Settle From Last Sage Payment Required - AUTO EMAIL" _
                                             , emailBody _
                                             , "" _
                                             , ""
                                             )

                objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "PDL Settle From Last WorldPay Payment email sent: " & emailBody, Session("StaffID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub ShowHidePanels(showPanel As PanelsAvailable)
        paymentOptionsPanel.Visible = False
        rowBankDetails.Visible = False
        rowBankCardDetails.Visible = False
        rowSMSCode.Visible = False
        rowSage.Visible = False

        If showPanel = PanelsAvailable.PaymentOptions Then
            paymentOptionsPanel.Visible = True
            row1.Visible = False
            If Radio1.Checked Then
                Dim MonthlyPayment As Double = Math.Round(model.FinalMonthlyPayment, 2)
                txtMonthlyPayment.Text = MonthlyPayment.ToString
            End If
        End If
        If showPanel = PanelsAvailable.BankDetails Then
            rowBankDetails.Visible = True
        End If
        If showPanel = PanelsAvailable.CardDetails Then
            rowBankCardDetails.Visible = True
        End If
        If showPanel = PanelsAvailable.SMSDetails Then
            rowSMSCode.Visible = True
        End If
        If showPanel = PanelsAvailable.SageDetails Then
            rowSage.Visible = True
        End If
    End Sub

    Protected Sub btnSageProceed_Click(sender As Object, e As System.EventArgs) Handles btnSageProceed.Click
        Try
            lblMessage.Visible = False
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                'SendToSageServer(model)
                Dim iLoanID As Integer = model.AutoLoanID
                Dim iUserID As Integer
                Dim normalAmount As String = ""
                Dim noOfPayments As String = ""
                Dim startDate As String = ""
                Dim stDate As DateTime
                Dim nextpayDate As DateTime = DateAdd(DateInterval.Month, 1, Now)

                Dim AppData As DataTable = objAutoLoan.GetAutoLoanDetail(iLoanID)

                If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
                    With AppData.Rows(0)
                        iUserID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                        normalAmount = .Item("FinalMonthlyPayment")
                        noOfPayments = .Item("FinalLoanTerm")
                        If .Item("NextPayDay") IsNot System.DBNull.Value Then Date.TryParse(.Item("NextPayDay"), nextpayDate)
                        'stDate = .Item("NextPayDay")
                    End With
                    startDate = Format(nextpayDate, "yyyy-MM-dd")

                    Dim cartid As String = "PDLALPS-" & iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & iUserID.ToString

                    RedirectAndPOST(Me.Page, cartid, noOfPayments, normalAmount, normalAmount, startDate)
                End If

            Else
                RaiseEvent SessionExpired()
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Public Shared Sub RedirectAndPOST(page As Page, cartid As String, noOfPayments As String, normalAmount As String, FirstAmount As String, startDate As String)
        Dim data As New NameValueCollection()
        Dim count As Integer = CInt(noOfPayments)

        data.Add("instId", "1140028")
        data.Add("cartId", cartid)
        data.Add("amount", "0.01")
        data.Add("currency", "GBP")
        data.Add("futurePayType", "regular")
        data.Add("option", "1")
        data.Add("intervalMult", "1")
        data.Add("intervalUnit", "3")
        If noOfPayments > 1 Then data.Add("noOfPayments", noOfPayments)
        'If (count > 1) Then
        data.Add("normalAmount", normalAmount)
        'Else
        'data.Add("normalAmount", "0.00")
        'End If
        data.Add("startDate", startDate)
        data.Add("initialAmount", FirstAmount)

        Dim postUrl As String = String.Empty
        'Dim testMode As Boolean = True 
        'Set this from Web.Config SageTestmode

        Dim strConnectTo As String = objConfig.getConfigKey("SagePayEnv")
        If strConnectTo = "LIVE" Then
            postUrl = "https://secure.worldpay.com/wcc/purchase"
            data.Add("testMode", "0")
        Else
            postUrl = "https://secure-test.worldpay.com/wcc/purchase"
            data.Add("testMode", "100")
        End If

        'Prepare the Posting form
        Dim strForm As String = PreparePOSTForm(postUrl, data)


        'Add a literal control the specified page holding the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(New LiteralControl(strForm))
    End Sub

    Private Shared Function PreparePOSTForm(url As String, data As NameValueCollection) As [String]
        'Set a name for the form
        Dim formID As String = "PostForm"

        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append((Convert.ToString((Convert.ToString((Convert.ToString("<form id=""") & formID) + """ name=""") & formID) + """ action=""") & url) + """ method=""POST"">")
        For Each key As String In data
            strForm.Append((Convert.ToString("<input type=""hidden"" name=""") & key) + """ value=""" + data(key) + """>")
        Next
        strForm.Append("</form>")

        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")
        strScript.Append((Convert.ToString((Convert.ToString("var v") & formID) + " = document.") & formID) + ";")
        strScript.Append((Convert.ToString("v") & formID) + ".submit();")
        strScript.Append("</script>")

        'Return the form and the script concatenated. (The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function

    'Protected Sub radPaymentOption_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radPaymentOption.SelectedIndexChanged
    'If radPaymentOption.SelectedValue = "FP" Then
    'Dim MonthlyPayment As Double = model.FinalMonthlyPayment + (model.AutoLoanParams.FasterPaymentFee / model.PDLLoanTerm)
    'txtMonthlyPayment.Text = MonthlyPayment.ToString
    'End If
    ' If radPaymentOption.SelectedValue = "BACS" Then
    'txtMonthlyPayment.Text = model.FinalMonthlyPayment.ToString
    'End If
    ' End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

        paymentOptionsPanel.Visible = False
           row1.Visible = True
        'Response.Redirect("AutoLoanMemberNewApplication.aspx?A=True&Amount=" & model.FinalLoanAmount.ToString & "&Term=" & model.FinalLoanTerm.ToString & "&LoanID=" & model.AutoLoanID.ToString)

    End Sub
    Protected Sub Radio1_CheckedChanged(sender As Object, e As EventArgs) Handles Radio1.CheckedChanged
        Dim MonthlyPayment As Double = Math.Round(model.FinalMonthlyPayment, 2)
        txtMonthlyPayment.Text = MonthlyPayment.ToString
    End Sub
    Protected Sub Radio2_CheckedChanged(sender As Object, e As EventArgs) Handles Radio2.CheckedChanged
        txtMonthlyPayment.Text = Math.Round(model.FinalMonthlyPayment - 5, 2).ToString
    End Sub
End Class
