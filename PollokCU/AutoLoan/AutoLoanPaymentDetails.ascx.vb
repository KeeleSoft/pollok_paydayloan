﻿Imports PollokCU

Partial Class AutoLoanAutoLoanPaymentDetails
    Inherits System.Web.UI.UserControl

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan

    Private model As AutoLoanFacade.AutoLoanModel

    Public Event PaymentInformationCaptured(manualVerifyRequired As Boolean)
    Public Event SessionExpired()

    Public Sub LoadPaymentOptions()
        If Session("AutoLoanModel") IsNot Nothing Then
            model = TryCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
        End If
        If model IsNot Nothing Then
            If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                litPaymentText.Text = "Congratulations. Your loan is approved. Please select the prefered payment option and enter your bank details."
                paymentOptionsPanel.Visible = True
            Else
                litPaymentText.Text = "Congratulations. Your loan is approved. Please confirm to accept your loan and the money will be transfered to your Credit Union current account."
                paymentOptionsPanel.Visible = False
            End If
            litMemberID.Text = model.MemberID.ToString
            litMemberName.Text = model.MemberDetails.FirstName & " " & model.MemberDetails.Surname

            If Not String.IsNullOrEmpty(model.MemberDetails.MobileNumber) Then

                If model.MemberDetails.DateOfBirth > Date.MinValue Then
                    model.SMSSecurityCode = AutoLoanFacade.AutoLoanEngine.GenerateSecurityCode(model.MemberDetails.DateOfBirth) 'objRandom.Next(100000, 999999)
                Else
                    Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))
                    model.SMSSecurityCode = objRandom.Next(100000, 999999)
                End If

                model.SaveAutoLoanModelDetails(2)
                Session("AutoLoanModel") = model
                SMSSecurityCode(model)
                SMSCodePanel.Visible = True
            Else
                SMSCodePanel.Visible = False
            End If
            If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.MemberSite Then
                ManualVerifyPanel.Visible = False
            Else
                ManualVerifyPanel.Visible = True
            End If
        Else    'Session expired
            RaiseEvent SessionExpired()
        End If
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Try
            lblMessage.Visible = False

            Dim PayOption As String = radPaymentOption.SelectedValue

            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)

                If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.MemberSite _
                    AndAlso Not String.IsNullOrEmpty(model.MemberDetails.MobileNumber) _
                    AndAlso SMSCodePanel.Visible _
                    AndAlso (String.IsNullOrEmpty(txtSecurityCode.Text) OrElse txtSecurityCode.Text <> model.SMSSecurityCode) Then

                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid SMS Security code. Please enter the correct security code"
                    Return
                End If

                Dim manualVerify As Boolean = False
                If model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.StaffLogin Then
                    If chkManualVerify.Checked Then
                        manualVerify = True
                    Else
                        If String.IsNullOrEmpty(txtSecurityCode.Text) OrElse txtSecurityCode.Text <> model.SMSSecurityCode Then
                            lblMessage.Visible = True
                            lblMessage.Text = "Invalid SMS Security code. Please enter the correct security code"
                            Return
                        End If
                    End If
                End If

                Dim AccNum As Integer = 0
                Integer.TryParse(txtAccNumber.Text, AccNum)
                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso AccNum = 0 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid account number"
                    Return
                End If

                Dim SortCode As Integer = 0
                Integer.TryParse(txtSortCode.Text, SortCode)
                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso SortCode = 0 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid sort code"
                    Return
                End If

                Dim NextPayDay As Date = Nothing
                Date.TryParse(txtNextPayDay.Text, NextPayDay)
                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso (IsDBNull(NextPayDay) OrElse NextPayDay <= Now) Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid next pay day"
                    Return
                End If

                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                    model.PaymentMethod = PayOption
                    model.AccountNumber = AccNum
                    model.SortCode = SortCode
                    model.NextPayDay = NextPayDay
                Else    'Rev loan
                    model.PaymentMethod = "BACS"    'Default payment option is CU current account
                    'Account number and sort code already set initiall for this member
                End If

                If manualVerify Then
                    model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Security_Failed
                    model.SaveAutoLoanModelDetails(2)
                Else
                    model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically
                    model.SaveAutoLoanModelDetails(3)

                    Dim userID As Integer = 26 'Online User
                    If Session("StaffID") > 0 Then
                        userID = Session("StaffID")
                    End If
                    objAutoLoan.UpdateAutoLoanApprovedByStaffID(model.AutoLoanID, userID)   'Set the approved by staff id
                    If model.PaymentMethod = "BACS" Then
                        objAutoLoan.UpdateAutoLoanSendToBACS(model.AutoLoanID, userID)
                    ElseIf model.PaymentMethod = "FP" Then
                        Dim PayRef As String = "ALPS" & model.AutoLoanID.ToString & "-" & model.MemberID.ToString
                        Dim LoanAmountInPence As Integer = CInt(model.FinalLoanAmount) * 100

                        objPayDayLoan.InsertPaymentInstruction(model.AutoLoanID, model.MemberID, model.SortCode _
                                                               , model.AccountNumber, model.MemberDetails.FirstName & " " & model.MemberDetails.Surname _
                                                               , PayRef, LoanAmountInPence, 99, userID, True, "ALPS") '26=Online.User Staff Account
                    End If

                    objAutoLoan.UpdateAutoLoanGrantedDate(model.AutoLoanID)

                    If model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.CurrentRepayAmount <> model.FinalMonthlyPaymentForCUCA Then
                        'Send an email to CUCA for the new repayment
                        SendCUCAEmail(model)
                    End If
                    If model.FinalOldLoanAmount > 0 AndAlso model.OneThirdRule.LastLoanProductCode <> "PLUSLO" Then
                        SendRescheduleEmail(model, model.FinalOldLoanAmount, model.OneThirdRule.LastLoanProductCode, model.FinalOldMonthlyPayment, model.FinalOldLoanTerm)
                    End If
                    If model.ShareToLoanAccepted AndAlso model.DecisionCalcs.STLReductionAmount > 0 Then
                        'Send an email to Loans for the new shareto loan
                        SendS2LoanEmail(model)
                    End If
                End If

                RaiseEvent PaymentInformationCaptured(manualVerify) 'Pass back to main page
            Else    'No explanation
                lblMessage.Visible = True
                lblMessage.Text = "Session expired. Please start again"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendCUCAEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim msg As String = "Member Number: " & model.MemberID.ToString & ", " & vbCrLf
        msg &= "Current repayment: " & model.OneThirdRule.CurrentRepayAmount.ToString & ", " & vbCrLf
        msg &= "New repayment: " & Math.Ceiling((Double.Parse(model.FinalMonthlyPaymentForCUCA) + Double.Parse(model.AutoLoanParams.CUCADefaultSaving))).ToString & ", " & vbCrLf
        msg &= "New term (months): " & model.FinalLoanTerm.ToString & ", " & vbCrLf

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS New Repayment Confirmation" _
                                          , msg, "")
        objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Email Sent To CUCA: " & msg, Session("StaffID"))
    End Sub

    Private Sub SendS2LoanEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim msg As String = "Member Number: " & model.MemberID & ", " & vbCrLf
        msg &= "Member name: " & model.MemberDetails.FirstName & " " & model.MemberDetails.Surname & ", " & vbCrLf
        msg &= "Share To Loan Amount: " & model.DecisionCalcs.STLReductionAmount.ToString & ", " & vbCrLf

        Dim email As String = "theweeloan@pollokcu.com"
        'Dim email() As String = {"gayeshan@yahoo.com", "theweeloan@pollokcu.com", "lucky@creditunion.co.uk", "gayeshan@gmail.com"}

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , email _
                                          , "ALPS New S2L Confirmation" _
                                          , msg, "")

        objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Email Sent To Loans: " & msg, Session("StaffID"))
    End Sub

    Private Sub SendRescheduleEmail(ByVal model As AutoLoanFacade.AutoLoanModel, ByVal loanAmount As Double, ByVal loanProduct As String, ByVal loanRepay As Double, ByVal loanTerm As Integer)
        Dim msg As String = "Member Number: " & model.MemberID.ToString & " has rescheduled their " & loanAmount & " " & loanProduct & " to " & loanRepay & " over " & loanTerm

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS Current Loan Reschedule" _
                                          , msg, "")

        Dim userID As Integer = 26
        If Session("StaffID") > 0 Then
            userID = Session("StaffID")
        End If
        objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Current loan reschedule email sent " & msg, userID)

    End Sub

    Private Sub SMSSecurityCode(ByVal model As AutoLoanFacade.AutoLoanModel)
        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        lblMessage.Visible = False
        Try
            Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
            Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
            Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
            Body = model.SMSSecurityCode.ToString
            Recipient = model.MemberDetails.MobileNumber


            If Recipient.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No mobile number found to send the security code"
                Return
            End If

            Dim header As esendex.MessengerHeader

            header = New esendex.MessengerHeader
            header.Username = Username
            header.Password = Password
            header.Account = Account

            Dim service As esendex.SendService

            service = New esendex.SendService
            service.MessengerHeaderValue = header
            service.SendMessage(Recipient, Body, esendex.MessageType.Text)
            objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, Session("StaffID"))
            objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "Security code " & Body & " Sent To " & Recipient.ToString, Session("StaffID"))
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
