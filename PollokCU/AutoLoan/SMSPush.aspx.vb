﻿Imports System.Data
Imports PollokCU

Partial Class AutoLoan_SMSPush
    Inherits System.Web.UI.Page
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

    Private model As AutoLoanFacade.AutoLoanModel
    Private AutoLoanDT As DataTable = Nothing


    Private Const LOAN_PREFIX As String = "rev"
    Private Const CUOK_PREFIX As String = "cuok"

    Dim userID As Integer = 26    'Online user

    Private msg_approved As String = "Thank you. your loan of <<amount>> is approved"
    Private msg_invalidamount As String = "Invalid loan amount. Minimum loan amount is £50"
    Private msg_invalidResponse As String = "Invalid response sent"
    Private msg_invalidmobile As String = "Your mobile number not registered with us"
    Private msg_maxamountrequest As String = "You have requested a more than allowed amount. Amount should be less than <<amount>>"
    Private msg_duplicatedLoan As String = "Existing loan applied within the last 6 months against your member number. Could not proceed with the loan"
    Private msg_loanFailed As String = "Failed to approve your loan. Please contact LMCU"
    Private msg_cuokPassResponse As String = "your loan can be aproved. please reply ""CUOK YES"" if agree or ""CUOK NO"" if disagree"
    Private msg_revloanPassResponse As String = "your loan can be aproved. please reply ""REV YES"" if agree or ""REV NO"" if disagree"
    Private msg_Aborted As String = "Thank you, your application is closed"
    Private msg_AlreadyAproved As String = "Your application is already aproved"
    Private msg_NoAccountDetails As String = "Previous CUOK application account number and sortcode cannot be found. Please contact LMCU with your faster payment account details"
    Private msg_NoCUCA As String = "No valid current account found. Please contact LMCU"
    Private msg_invalidStatus As String = "Your member status is not authorised to apply for revolving loans. Please contact LMCU"
    Private msg_DuplicatedPhoneNumber As String = "Your mobile number registered with more than one membership accounts. Please contact LMCU"
    Private msg_ageover70 As String = "Thank you for your loan request, I am afraid this facility is not available to you. If you would like to apply please use the ""apply for a loan"" option"
    Private msg_AutoLoanNotAvailable As String = "This loan facility currently not available. Please try again later. Thank you."

    'CanITopUp related messages
    Private msg_invalidRistAmount As String = "You are currently not setup to apply for revolving loans. Please contact LMCU"
    'Private msg_YesMessage As String = "YES you could apply for £<<amount>>. If the share to loan option is available to you, it may offer you a higher amount."
    Private msg_YesMessage As String = "Yes. you are currently eligible to apply for a loan with us."
    Private msg_NoEntitlement As String = "NO, could not determine the loan entitlement. Please contact LMCU"
    Private msg_NoOneThirdFailed As String = "NO, You may be able to apply after <<date>>"
    Private msg_SystemException As String = "Failed to process your request. Please contact LMCU"
    Private msg_CannotDetermine As String = "Could not determine your request. Please contact LMCU"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim textReader = New IO.StreamReader(Request.InputStream)
            Request.InputStream.Seek(0, IO.SeekOrigin.Begin)
            textReader.DiscardBufferedData()
            If IsPostBack OrElse textReader.BaseStream.Length = 0 Then
                Return
            End If

            Dim xmlOut = XDocument.Load(textReader)

            Dim ID As String = xmlOut.<InboundMessage>.<Id>.Value
            Dim AccountID As String = xmlOut.<InboundMessage>.<AccountId>.Value
            Dim MessageText As String = xmlOut.<InboundMessage>.<MessageText>.Value
            Dim FromPhone As String = xmlOut.<InboundMessage>.<From>.Value
            Dim ToPhone As String = xmlOut.<InboundMessage>.<To>.Value

            Dim msgFormatter As AutoLoanFacade.SMSRequestFormatter = New AutoLoanFacade.SMSRequestFormatter
            With msgFormatter
                .InboundMessageID = ID
                .InboundMessageAccountID = AccountID
                .MessageText = MessageText
                .MobileNumber = FromPhone
                .InboundMessageTo = ToPhone
            End With
            Dim message As String = "ID:" & ID & "AccountID:" & AccountID & ",Message:" & MessageText & ",From: " & FromPhone & ", To:" & ToPhone
            If IsValidALPSMessage(MessageText, msgFormatter) Then
                ProcessInboundMessage(msgFormatter)
                'SendEmail(message)  ''TODO: Remove
            ElseIf IsValidCanITopUpMessage(MessageText) Then
                ProcessCanITopUpMessage(msgFormatter)
                'SendEmail(message)
            End If

            'SendEmail(message)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        End Try

    End Sub

    Private Function IsValidALPSMessage(ByVal msg As String, ByRef msgFormatter As AutoLoanFacade.SMSRequestFormatter) As Boolean
        Dim valid As Boolean = False
        Dim msgParts() As String
        If Not String.IsNullOrEmpty(msg) AndAlso msg.Contains(" ") Then
            msgParts = msg.Split(" ")
            If msgParts.Count > 1 Then
                If msgParts(0).Trim.ToLower = LOAN_PREFIX Then
                    msgFormatter.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan
                    msgFormatter.OptionText = msgParts(1).Trim
                    valid = True
                ElseIf msgParts(0).Trim.ToLower = CUOK_PREFIX Then
                    msgFormatter.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay
                    msgFormatter.OptionText = msgParts(1).Trim
                    valid = True
                End If
            End If
        End If

        Return valid
    End Function

    Private Function IsValidCanITopUpMessage(ByVal msg As String) As Boolean
        Dim valid As Boolean = False
        If Not String.IsNullOrEmpty(msg) AndAlso msg.ToLower.Contains("canitopup") Then
            valid = True
        End If
        Return valid
    End Function


    Private Sub SendEmail(ByVal msg As String)

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "gayeshan@yahoo.com" _
                                          , "Esendex SMS Received" _
                                          , msg, "")
    End Sub

    ''' <summary>
    ''' Test harness button to simulate a push sms
    ''' </summary>
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            lblMessage.Visible = False
            Dim msg As String = txtMessage.Text
            Dim msgFormatter As AutoLoanFacade.SMSRequestFormatter = New AutoLoanFacade.SMSRequestFormatter

            Dim ID As String = "100"
            Dim AccountID As String = "101"
            Dim MessageText As String = msg
            Dim ToPhone As String = "60600"

            With msgFormatter
                .InboundMessageID = ID
                .InboundMessageAccountID = AccountID
                .MessageText = MessageText
                .MobileNumber = txtMobile.Text
                .InboundMessageTo = ToPhone
            End With
            If IsValidALPSMessage(msg, msgFormatter) Then
                ProcessInboundMessage(msgFormatter)
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Save an inbound message
    ''' </summary>
    Private Sub SaveInboundOutboundMessage(ByVal AutoLoanID As Integer _
                                  , ByVal MemberID As Integer _
                                  , ByVal MobileNumber As String _
                                  , ByVal TextMessage As String _
                                  , ByVal InboundMessageID As String _
                                  , ByVal InboundMessageAccountID As String _
                                  , ByVal InboundMessageTo As String)
        objAutoLoan.InsertSMSCommunicationEntry(AutoLoanID, MemberID, MobileNumber, TextMessage, InboundMessageID, InboundMessageAccountID, InboundMessageTo)
    End Sub

    Private Sub ProcessInboundMessage(msgRequest As AutoLoanFacade.SMSRequestFormatter)
        If objSystemData.GetSystemKeyValue("EnableAutoLoans") = 0 Then
            SendSMSMessage(msg_AutoLoanNotAvailable, msgRequest.MobileNumber, 0, 0)
            Return
        End If

        Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
        'Check whether there is already a record in the options table expecting a response
        Dim lastOptionDT As DataTable = objAutoLoan.GetAutoLoanLastAvailableSMSOption(msgRequest.MobileNumber)
        Dim loanGranted As Boolean = False
        Dim paymentMethod As String = ""
        If lastOptionDT IsNot Nothing AndAlso lastOptionDT.Rows.Count > 0 Then  'Existing application expecting a request
            Dim lastOption As AutoLoanFacade.SMSAvailableOption = New AutoLoanFacade.SMSAvailableOption
            With lastOption
                .AutoLoanID = lastOptionDT.Rows(0).Item("AutoLoanID")
                .MemberID = lastOptionDT.Rows(0).Item("MemberID")
                .MobileNumber = lastOptionDT.Rows(0).Item("MobileNumber")
                .Option1Amount = lastOptionDT.Rows(0).Item("Option1Amount")
                .Option1Term = lastOptionDT.Rows(0).Item("Option1Term")
                .Option1MonthlyPayment = lastOptionDT.Rows(0).Item("Option1MonthlyPayment")
                .Option2Amount = lastOptionDT.Rows(0).Item("Option2Amount")
                .Option2Term = lastOptionDT.Rows(0).Item("Option2Term")
                .Option2MonthlyPayment = lastOptionDT.Rows(0).Item("Option2MonthlyPayment")
                .Option3Amount = lastOptionDT.Rows(0).Item("Option3Amount")
                .Option3Term = lastOptionDT.Rows(0).Item("Option3Term")
                .Option3MonthlyPayment = lastOptionDT.Rows(0).Item("Option3MonthlyPayment")
                .Option3ShareToLoan = lastOptionDT.Rows(0).Item("Option3ShareToLoan")

                If lastOptionDT.Rows(0).Item("OldLoanAmount1") IsNot System.DBNull.Value Then .OldLoanAmount1 = lastOptionDT.Rows(0).Item("OldLoanAmount1")
                If lastOptionDT.Rows(0).Item("OldLoanTerm1") IsNot System.DBNull.Value Then .OldLoanTerm1 = lastOptionDT.Rows(0).Item("OldLoanTerm1")
                If lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment1") IsNot System.DBNull.Value Then .OldLoanMonthlyPayment1 = lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment1")
                If lastOptionDT.Rows(0).Item("OldLoanAmount2") IsNot System.DBNull.Value Then .OldLoanAmount2 = lastOptionDT.Rows(0).Item("OldLoanAmount2")
                If lastOptionDT.Rows(0).Item("OldLoanTerm2") IsNot System.DBNull.Value Then .OldLoanTerm2 = lastOptionDT.Rows(0).Item("OldLoanTerm2")
                If lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment2") IsNot System.DBNull.Value Then .OldLoanMonthlyPayment2 = lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment2")
                If lastOptionDT.Rows(0).Item("OldLoanAmount3") IsNot System.DBNull.Value Then .OldLoanAmount3 = lastOptionDT.Rows(0).Item("OldLoanAmount3")
                If lastOptionDT.Rows(0).Item("OldLoanTerm3") IsNot System.DBNull.Value Then .OldLoanTerm3 = lastOptionDT.Rows(0).Item("OldLoanTerm3")
                If lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment3") IsNot System.DBNull.Value Then .OldLoanMonthlyPayment3 = lastOptionDT.Rows(0).Item("OldLoanMonthlyPayment3")
                If lastOptionDT.Rows(0).Item("LastProductCode") IsNot System.DBNull.Value Then .LastProductCode = lastOptionDT.Rows(0).Item("LastProductCode")
            End With

            'Save the inbound message first
            SaveInboundOutboundMessage(lastOption.AutoLoanID, lastOption.MemberID, msgRequest.MobileNumber, msgRequest.MessageText, msgRequest.InboundMessageID, msgRequest.InboundMessageAccountID, msgRequest.InboundMessageTo)
            AutoLoanDT = objAutoLoan.GetAutoLoanDetail(lastOption.AutoLoanID)
            loanGranted = Not (AutoLoanDT.Rows(0).Item("LoanGrantedDate") Is System.DBNull.Value)
            If AutoLoanDT.Rows(0).Item("PaymentMethod") IsNot System.DBNull.Value Then paymentMethod = AutoLoanDT.Rows(0).Item("PaymentMethod")
            If msgRequest.LoanType = PollokCU.AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                If msgRequest.OptionText.ToLower = PollokCU.AutoLoanFacade.SMSConstants.YES Then 'Accepted
                    If lastOption.Option1Amount > 0 Then
                        If paymentMethod = "BACS" Then
                            objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, PollokCU.AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically, userID, 3)
                            objAutoLoan.UpdateAutoLoanSendToBACS(lastOption.AutoLoanID, userID)
                            objAutoLoan.UpdateAutoLoanGrantedDate(lastOption.AutoLoanID)
                            SendSMSMessage(msg_approved.Replace("<<amount>>", FormatNumber(lastOption.Option1Amount, 0)), msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                        ElseIf paymentMethod = "FP" Then
                            Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
                            Dim PayRef As String = "ALPS" & lastOption.AutoLoanID.ToString & "-" & lastOption.MemberID.ToString
                            Dim LoanAmountInPence As Integer = CInt(lastOption.Option1Amount) * 100
                            Dim SortCode As String = ""
                            Dim AccountNumber As String = ""
                            Dim MemberName As String = ""
                            If AutoLoanDT.Rows(0).Item("PaymentSortCodeDec") IsNot System.DBNull.Value Then SortCode = AutoLoanDT.Rows(0).Item("PaymentSortCodeDec")
                            If AutoLoanDT.Rows(0).Item("PaymentAccountNumberDec") IsNot System.DBNull.Value Then AccountNumber = AutoLoanDT.Rows(0).Item("PaymentAccountNumberDec")
                            If AutoLoanDT.Rows(0).Item("FullName") IsNot System.DBNull.Value Then MemberName = AutoLoanDT.Rows(0).Item("FullName")

                            If Not String.IsNullOrEmpty(SortCode) AndAlso Not String.IsNullOrEmpty(AccountNumber) Then
                                objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically, userID, 3)
                                objPayDayLoan.InsertPaymentInstruction(lastOption.AutoLoanID, lastOption.MemberID, SortCode _
                                                                   , AccountNumber, MemberName _
                                                                   , PayRef, LoanAmountInPence, 99, userID, True, "ALPS") '26=Online.User Staff Account
                                objAutoLoan.UpdateAutoLoanGrantedDate(lastOption.AutoLoanID)
                                SendSMSMessage(msg_approved.Replace("<<amount>>", FormatNumber(lastOption.Option1Amount, 0)), msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                            Else 'No previos PDL application account number and sort can be found for the memberID
                                SendSMSMessage(msg_NoAccountDetails, msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                            End If
                        End If

                    End If
                ElseIf msgRequest.OptionText.ToLower = AutoLoanFacade.SMSConstants.NO Then 'Rejected
                    objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Refused_Automatically, userID)
                    SendSMSMessage(msg_Aborted, msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                End If
            Else    'Rev loan
                If Not loanGranted Then
                    If msgRequest.OptionText.ToLower = AutoLoanFacade.SMSConstants.YES Then 'Accepted
                        If lastOption.Option1Amount > 0 Then
                            'Set the application status to auto aproved
                            objAutoLoan.UpdateAutoLoanFinalFigures(lastOption.AutoLoanID, lastOption.Option1Amount, lastOption.Option1Term, lastOption.Option1MonthlyPayment, 0, userID, lastOption.OldLoanAmount1, lastOption.OldLoanTerm1, lastOption.OldLoanMonthlyPayment1)
                            objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically, userID, 3)
                            objAutoLoan.UpdateAutoLoanSendToBACS(lastOption.AutoLoanID, userID)
                            objAutoLoan.UpdateAutoLoanGrantedDate(lastOption.AutoLoanID)
                            objAutoLoan.UpdateAutoLoanApprovedByStaffID(lastOption.AutoLoanID, userID)   'Set the approved by staff id
                            SendCUCAEmailIfRequired(lastOption.AutoLoanID, lastOption.MemberID)
                            If lastOption.LastProductCode.ToUpper <> "PLUSLO" Then SendRescheduleEmail(lastOption.AutoLoanID, lastOption.MemberID, lastOption.OldLoanAmount1, "", lastOption.OldLoanMonthlyPayment1, lastOption.OldLoanTerm1)
                            SendSMSMessage(msg_approved.Replace("<<amount>>", FormatNumber(lastOption.Option1Amount, 0)), msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                        End If
                    ElseIf msgRequest.OptionText.ToLower = AutoLoanFacade.SMSConstants.NO Then 'Rejected
                        objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Refused_Automatically, userID)
                        SendSMSMessage(msg_Aborted, msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                    ElseIf msgRequest.OptionText.ToLower = AutoLoanFacade.SMSConstants.OPTION_NoSTLOptionSelected Then  '2
                        'Set the application status to auto aproved
                        objAutoLoan.UpdateAutoLoanFinalFigures(lastOption.AutoLoanID, lastOption.Option2Amount, lastOption.Option2Term, lastOption.Option2MonthlyPayment, 0, userID, lastOption.OldLoanAmount2, lastOption.OldLoanTerm2, lastOption.OldLoanMonthlyPayment2)
                        objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically, userID, 3)
                        objAutoLoan.UpdateAutoLoanSendToBACS(lastOption.AutoLoanID, userID)
                        objAutoLoan.UpdateAutoLoanGrantedDate(lastOption.AutoLoanID)
                        SendCUCAEmailIfRequired(lastOption.AutoLoanID, lastOption.MemberID)
                        If lastOption.LastProductCode.ToUpper <> "PLUSLO" Then SendRescheduleEmail(lastOption.AutoLoanID, lastOption.MemberID, lastOption.OldLoanAmount2, "", lastOption.OldLoanMonthlyPayment2, lastOption.OldLoanTerm2)
                        SendSMSMessage(msg_approved.Replace("<<amount>>", FormatNumber(lastOption.Option2Amount, 0)), msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                    ElseIf msgRequest.OptionText.ToLower = AutoLoanFacade.SMSConstants.OPTION_YesSTLOptionSelected Then  '3
                        'Set the application status to auto aproved
                        objAutoLoan.UpdateAutoLoanFinalFigures(lastOption.AutoLoanID, lastOption.Option3Amount, lastOption.Option3Term, lastOption.Option3MonthlyPayment, lastOption.Option3ShareToLoan, userID, lastOption.OldLoanAmount3, lastOption.OldLoanTerm3, lastOption.OldLoanMonthlyPayment3)
                        objAutoLoan.UpdateAutoLoanApplicationByStaff(lastOption.AutoLoanID, AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically, userID, 3)
                        objAutoLoan.UpdateAutoLoanSendToBACS(lastOption.AutoLoanID, userID)
                        objAutoLoan.UpdateAutoLoanGrantedDate(lastOption.AutoLoanID)
                        SendCUCAEmailIfRequired(lastOption.AutoLoanID, lastOption.MemberID)
                        If lastOption.LastProductCode.ToUpper <> "PLUSLO" Then SendRescheduleEmail(lastOption.AutoLoanID, lastOption.MemberID, lastOption.OldLoanAmount3, "", lastOption.OldLoanMonthlyPayment3, lastOption.OldLoanTerm3)
                        Dim MemberName As String = ""
                        If AutoLoanDT IsNot Nothing AndAlso AutoLoanDT.Rows(0).Item("FullName") IsNot System.DBNull.Value Then MemberName = AutoLoanDT.Rows(0).Item("FullName")
                        SendS2LoanEmail(lastOption.AutoLoanID, lastOption.MemberID, lastOption.Option3ShareToLoan, MemberName)
                        SendSMSMessage(msg_approved.Replace("<<amount>>", FormatNumber(lastOption.Option3Amount, 0)), msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                    Else
                        SendSMSMessage(msg_invalidResponse, msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                    End If
                Else    'Loan already granted
                    SendSMSMessage(msg_AlreadyAproved, msgRequest.MobileNumber, lastOption.AutoLoanID, lastOption.MemberID)
                End If

            End If
        Else    'New application
            Dim loanAmount As Double = 0
            Double.TryParse(msgRequest.OptionText, loanAmount)
            If loanAmount >= 50 Then
                model = New AutoLoanFacade.AutoLoanModel With {.MemberMobileNumber = msgRequest.MobileNumber, .LoanAmount = msgRequest.OptionText}
                model.UserID = 26   'Online User
                model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.TextSystem
                model.LoanType = msgRequest.LoanType
                Dim Engine As AutoLoanFacade.AutoLoanEngine = New AutoLoanFacade.AutoLoanEngine
                Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters
                Engine.GetMemberDetailsByMobile(model)
                If model.MemberDetails Is Nothing Then 'No member details found. So the mobile number is not registered
                    SendSMSMessage(msg_invalidmobile, msgRequest.MobileNumber, 0, 0)
                ElseIf model.MemberDetails IsNot Nothing AndAlso model.MemberDetails.PhoneNumberCount > 1 Then
                    SendSMSMessage(msg_DuplicatedPhoneNumber, msgRequest.MobileNumber, 0, 0)
                ElseIf model.MemberDetails IsNot Nothing AndAlso model.MemberDetails.Age >= 70 Then
                    SendSMSMessage(msg_ageover70, msgRequest.MobileNumber, 0, 0)
                ElseIf model.MemberDetails IsNot Nothing AndAlso Not model.MemberDetails.AllowProceedALPSForStatus Then
                    SendSMSMessage(msg_invalidStatus, msgRequest.MobileNumber, 0, 0)
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso loanAmount > model.AutoLoanParams.MaxAllowedPDLAmount Then
                    SendSMSMessage(msg_maxamountrequest.Replace("<<amount>>", FormatNumber(model.AutoLoanParams.MaxAllowedPDLAmount, 2)), msgRequest.MobileNumber, 0, 0)
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso loanAmount > model.AutoLoanParams.MaxAllowedRevLoanAmount Then
                    SendSMSMessage(msg_maxamountrequest.Replace("<<amount>>", FormatNumber(model.AutoLoanParams.MaxAllowedRevLoanAmount, 2)), msgRequest.MobileNumber, 0, 0)
                ElseIf objConfig.getConfigKey("AutoLoanPerformDuplicateCheck") = "TRUE" AndAlso Engine.AutoLoanDuplicated(model.MemberID, model.LoanType.ToString) Then    'Duplicate check is controlled from the web.config
                    SendSMSMessage(msg_duplicatedLoan, msgRequest.MobileNumber, 0, 0)
                Else 'Member found, so carry on with tests

                    If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                        Engine.PerformPayDayLoanRule(model)
                        If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PayDayLoanRulePassed AndAlso model.PayDayLoanRule.PDLRiskLimit >= model.LoanAmount Then    'Passed
                            model.SaveAutoLoanModelDetails(1)
                            'Save the inbound message first
                            SaveInboundOutboundMessage(model.AutoLoanID, model.MemberID, msgRequest.MobileNumber, msgRequest.MessageText, msgRequest.InboundMessageID, msgRequest.InboundMessageAccountID, msgRequest.InboundMessageTo)
                            objAutoLoan.InsertSMSAvailableOption(model.AutoLoanID, model.MemberID, model.MemberMobileNumber, model.FinalLoanAmount, model.FinalLoanTerm, model.FinalMonthlyPayment, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
                            SendSMSMessage(msg_cuokPassResponse, msgRequest.MobileNumber, model.AutoLoanID, model.MemberID)
                        Else    'Failed
                            model.SaveAutoLoanModelDetails(1)
                            SaveInboundOutboundMessage(model.AutoLoanID, model.MemberID, msgRequest.MobileNumber, msgRequest.MessageText, msgRequest.InboundMessageID, msgRequest.InboundMessageAccountID, msgRequest.InboundMessageTo)
                            SendSMSMessage(msg_loanFailed, msgRequest.MobileNumber, model.AutoLoanID, model.MemberID)
                        End If
                    Else    'Rev loan
                        'Engine.PerformOneThirdRule(model)
                        Engine.GetInitialMemberDetails(model)
                        If model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.OneThirdRulePassed AndAlso model.RiskLimit > 0 AndAlso IsNumeric(model.AccountNumber) AndAlso CInt(model.AccountNumber) > 0 Then
                            model.SaveAutoLoanModelDetails(1)
                            SaveInboundOutboundMessage(model.AutoLoanID, model.MemberID, msgRequest.MobileNumber, msgRequest.MessageText, msgRequest.InboundMessageID, msgRequest.InboundMessageAccountID, msgRequest.InboundMessageTo)
                            Engine.PerformShareToLoanRule(model)    'Do the Share To Loan
                            Engine.PerformDecisionCalculations(model)   'Calculate all possible combinations
                            model.SaveAutoLoanModelDetails(1)

                            If model.DecisionCalcs.NoSTLLoanEntitlement >= model.LoanAmount Then
                                model.LoanOutcomeFlag = AutoLoanFacade.AutoLoanEnums.LoanOutcome.LoanAgreedNoConditions
                                model.FinalLoanAmount = model.LoanAmount
                                model.FinalLoanTerm = model.DecisionCalcs.NoSTLRequestedAmountFinalTerm_New
                                model.FinalMonthlyPayment = model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment_New
                                model.FinalOldLoanAmount = model.DecisionCalcs.NoSTLRequestedAmountTotalLoanBalance
                                model.FinalOldLoanTerm = model.DecisionCalcs.NoSTLRequestedAmountFinalTerm
                                model.FinalOldMonthlyPayment = model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment

                                model.SaveAutoLoanModelDetails(1)
                                objAutoLoan.InsertSMSAvailableOption(model.AutoLoanID, model.MemberID, model.MemberMobileNumber, model.LoanAmount, model.FinalLoanTerm, model.FinalMonthlyPayment, 0, 0, 0, 0, 0, 0, 0, model.FinalOldLoanAmount, model.FinalOldLoanTerm, If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.FinalOldMonthlyPayment, model.TotalLoansRepayment), 0, 0, 0, 0, 0, 0)
                                SendSMSMessage(msg_revloanPassResponse, msgRequest.MobileNumber, model.AutoLoanID, model.MemberID)
                            Else 'Send options
                                Dim opt3OldLoanAmount As Double = 0
                                Dim opt3OldLoanTerm As Integer = 0
                                Dim opt3OldLoanMonthlyPayment As Double = 0

                                Dim newOption As AutoLoanFacade.SMSAvailableOption = New AutoLoanFacade.SMSAvailableOption
                                With newOption
                                    .AutoLoanID = model.AutoLoanID
                                    .MemberID = model.MemberID
                                    .MobileNumber = model.MemberMobileNumber
                                    .Option1Amount = 0
                                    .Option1Term = 0
                                    .Option1MonthlyPayment = 0
                                    .Option2Amount = model.DecisionCalcs.NoSTLLoanEntitlement
                                    .Option2Term = model.DecisionCalcs.NoSTLNewLoanTerm_New
                                    .Option2MonthlyPayment = model.DecisionCalcs.NoSTLRepaymentAmount_New

                                    If model.ShareToLoanRule.ShareToLoanRulePassed AndAlso model.TotalLoansOutstanding > 0 Then 'Offer share to loan only if the current balance is more than 0
                                        If model.LoanAmount < model.DecisionCalcs.YesSTLLoanEntitlement Then
                                            .Option3Amount = model.LoanAmount
                                            .Option3Term = model.DecisionCalcs.YesSTLRequestedAmountFinalTerm_New
                                            .Option3MonthlyPayment = model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment_New
                                            .Option3ShareToLoan = model.DecisionCalcs.STLReductionAmount

                                            opt3OldLoanAmount = model.DecisionCalcs.YesSTLRequestedAmountTotalLoanBalance
                                            opt3OldLoanTerm = model.DecisionCalcs.YesSTLRequestedAmountFinalTerm
                                            opt3OldLoanMonthlyPayment = model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment
                                        Else
                                            .Option3Amount = model.DecisionCalcs.YesSTLLoanEntitlement
                                            .Option3Term = model.DecisionCalcs.YesSTLNewLoanTerm_New
                                            .Option3MonthlyPayment = model.DecisionCalcs.YesSTLRepaymentAmount_New
                                            .Option3ShareToLoan = model.DecisionCalcs.STLReductionAmount

                                            opt3OldLoanAmount = model.DecisionCalcs.YesSTLTotalLoanBalance
                                            opt3OldLoanTerm = model.DecisionCalcs.YesSTLNewLoanTerm
                                            opt3OldLoanMonthlyPayment = If(model.OneThirdRule.LastLoanProductCode <> "PLUSLO", model.DecisionCalcs.YesSTLRepaymentAmount, model.TotalLoansRepayment)

                                        End If
                                    Else
                                        .Option3Amount = 0
                                        .Option3Term = 0
                                        .Option3MonthlyPayment = 0
                                        .Option3ShareToLoan = 0
                                    End If
                                End With
                                objAutoLoan.InsertSMSAvailableOption(model.AutoLoanID, model.MemberID, model.MemberMobileNumber, newOption.Option1Amount, newOption.Option1Term, newOption.Option1MonthlyPayment, newOption.Option2Amount, newOption.Option2Term, newOption.Option2MonthlyPayment, newOption.Option3Amount, newOption.Option3Term, newOption.Option3MonthlyPayment, newOption.Option3ShareToLoan, 0, 0, 0, model.DecisionCalcs.NoSTLTotalLoanBalance, model.DecisionCalcs.NoSTLNewLoanTerm, model.DecisionCalcs.NoSTLRepaymentAmount, opt3OldLoanAmount, opt3OldLoanTerm, opt3OldLoanMonthlyPayment)
                                Dim optionMsg As String = "Requested amount cannot approve."
                                If newOption.Option2Amount > 0 Then
                                    optionMsg &= " Reply ""REV 2"" if want to borrow " & FormatNumber(newOption.Option2Amount, 0)
                                End If
                                If newOption.Option3Amount > 0 Then
                                    optionMsg &= " Reply ""REV 3"" if want to borrow " & FormatNumber(newOption.Option3Amount, 0) & " with Share to loan of " & FormatNumber(newOption.Option3ShareToLoan, 0)
                                End If
                                SendSMSMessage(optionMsg, msgRequest.MobileNumber, model.AutoLoanID, model.MemberID)
                            End If
                        Else    'If the one third rule is failed cannot go forward anymore
                            If IsNumeric(model.AccountNumber) AndAlso CInt(model.AccountNumber) = 0 Then
                                SendSMSMessage(msg_NoCUCA, msgRequest.MobileNumber, 0, 0)
                            Else
                                SendSMSMessage(msg_loanFailed, msgRequest.MobileNumber, 0, 0)
                            End If

                        End If
                    End If
                End If
            Else
                SendSMSMessage(msg_invalidamount, msgRequest.MobileNumber, 0, 0)
            End If
        End If
    End Sub

    Private Sub SendSMSMessage(message As String, mobile As String, AutoLoanID As Integer, MemberID As Integer)
        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
        Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
        Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
        Body = message
        Recipient = mobile

        Dim header As esendex.MessengerHeader

        header = New esendex.MessengerHeader
        header.Username = Username
        header.Password = Password
        header.Account = Account

        Dim service As esendex.SendService

        service = New esendex.SendService
        service.MessengerHeaderValue = header
        service.SendMessage(Recipient, Body, esendex.MessageType.Text)
        objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, 0)
        If AutoLoanID > 0 Then SaveInboundOutboundMessage(AutoLoanID, MemberID, mobile, message, "", "", "")
    End Sub

    Private Sub SendCUCAEmailIfRequired(AutoLoanID As Integer, MemberID As Integer)
        AutoLoanDT = objAutoLoan.GetAutoLoanDetail(AutoLoanID)  'Reload the loan details as final figures might have been updated
        If AutoLoanDT IsNot Nothing AndAlso AutoLoanDT.Rows.Count > 0 Then
            Dim currentRepayment As Double = AutoLoanDT.Rows(0).Item("InputCurrentRepayments")
            Dim newRepayment As Double = AutoLoanDT.Rows(0).Item("FinalMonthlyPayment") + If(AutoLoanDT.Rows(0).Item("FinalOldMonthlyPayment") IsNot System.DBNull.Value, AutoLoanDT.Rows(0).Item("FinalOldMonthlyPayment"), 0)
            Dim newTerm As Integer = AutoLoanDT.Rows(0).Item("FinalLoanTerm")

            If currentRepayment <> newRepayment Then
                SendCUCAEmail(AutoLoanID, MemberID, currentRepayment, newRepayment, newTerm)
            End If
        End If
    End Sub

    Private Sub SendCUCAEmail(AutoLoanID As Integer, MemberID As Integer, CurrentRepayAmount As Double, FinalMonthlyPayment As Double, FinalLoanTerm As Integer)
        Dim dtParams As DataTable = objAutoLoan.GetAutoLoanParameters()
        Dim CUCADefaultSaving As Double = dtParams.Rows(0).Item("CUCADefaultSaving")

        Dim msg As String = "Member Number: " & MemberID.ToString & ", " & vbCrLf
        msg &= "Current repayment: " & CurrentRepayAmount.ToString & ", " & vbCrLf
        msg &= "New repayment: " & Math.Ceiling(FinalMonthlyPayment + CUCADefaultSaving).ToString & ", " & vbCrLf
        msg &= "New term (months): " & FinalLoanTerm.ToString & ", " & vbCrLf

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS New Repayment Confirmation" _
                                          , msg, "")
        objAutoLoan.InsertAutoLoanNote(AutoLoanID, "Email Sent To CUCA: " & msg, userID)
    End Sub

    Private Sub SendS2LoanEmail(ByVal AutoLoanID As Integer, ByVal MemberID As Integer, ByVal S2LoanAmount As Double, ByVal MemberName As String)
        Dim msg As String = "Member Number: " & MemberID & ", " & vbCrLf
        If Not String.IsNullOrEmpty(MemberName) Then msg &= "Member Name: " & MemberName & ", " & vbCrLf
        msg &= "Share To Loan Amount: " & FormatNumber(S2LoanAmount, 2) & ", " & vbCrLf

        Dim email As String = "theweeloan@pollokcu.com"
        'Dim email() As String = {"gayeshan@yahoo.com", "theweeloan@pollokcu.com", "lucky@creditunion.co.uk", "gayeshan@gmail.com"}

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , email _
                                          , "ALPS New S2L Confirmation" _
                                          , msg, "")

        objAutoLoan.InsertAutoLoanNote(AutoLoanID, "Email Sent To Loans: " & msg, userID)
    End Sub

    Private Sub SendRescheduleEmail(ByVal AutoLoanID As Integer, ByVal MemberID As Integer, ByVal loanAmount As Double, ByVal loanProduct As String, ByVal loanRepay As Double, ByVal loanTerm As Integer)
        Dim msg As String = "Member Number: " & MemberID.ToString & " has rescheduled their " & loanAmount & " " & loanProduct & " to " & loanRepay & " over " & loanTerm

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS Current Loan Reschedule" _
                                          , msg, "")

        Dim userID As Integer = 26
        If Session("StaffID") > 0 Then
            userID = Session("StaffID")
        End If
        objAutoLoan.InsertAutoLoanNote(AutoLoanID, "Current loan reschedule email sent " & msg, userID)

    End Sub

    Private Sub ProcessCanITopUpMessage(msgRequest As AutoLoanFacade.SMSRequestFormatter)
        Try
            model = New AutoLoanFacade.AutoLoanModel With {.MemberMobileNumber = msgRequest.MobileNumber, .LoanAmount = 50}
            model.UserID = 26   'Online User
            model.ApplyMethod = AutoLoanFacade.AutoLoanEnums.ApplyMethod.TextSystem
            model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan
            Dim Engine As AutoLoanFacade.AutoLoanEngine = New AutoLoanFacade.AutoLoanEngine
            Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters
            Engine.GetMemberDetailsByMobile(model)
            If model.MemberDetails Is Nothing Then 'No member details found. So the mobile number is not registered
                SendSMSMessage(msg_invalidmobile, msgRequest.MobileNumber, 0, 0)
                SaveCanITopUpMessageMessage(0, msg_invalidmobile, msgRequest)
            ElseIf model.MemberDetails IsNot Nothing AndAlso model.MemberDetails.PhoneNumberCount > 1 Then
                SendSMSMessage(msg_DuplicatedPhoneNumber, msgRequest.MobileNumber, 0, 0)
                SaveCanITopUpMessageMessage(0, msg_DuplicatedPhoneNumber, msgRequest)
            ElseIf model.MemberDetails IsNot Nothing AndAlso Not model.MemberDetails.AllowProceedALPSForStatus Then
                SendSMSMessage(msg_invalidStatus, msgRequest.MobileNumber, 0, 0)
                SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_invalidStatus, msgRequest)
            Else 'Member found, so carry on with tests
                Engine.GetInitialMemberDetails(model)
                If model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.OneThirdRulePassed Then
                    'If model.RiskLimit <= 0 Then    'No credit limites setup
                    '    SendSMSMessage(msg_invalidRistAmount, msgRequest.MobileNumber, 0, 0)
                    '    SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_invalidRistAmount, msgRequest)
                    'Else
                    Engine.PerformShareToLoanRule(model)    'Do the Share To Loan
                    Engine.PerformDecisionCalculations(model)   'Calculate all possible combinations
                    If model.DecisionCalcs.NoSTLLoanEntitlement > 0 Then
                        SendSMSMessage(msg_YesMessage.Replace("<<amount>>", model.DecisionCalcs.NoSTLLoanEntitlement), msgRequest.MobileNumber, 0, 0)
                        SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_YesMessage.Replace("<<amount>>", model.DecisionCalcs.NoSTLLoanEntitlement), msgRequest)
                    ElseIf model.DecisionCalcs.NoSTLLoanEntitlement = 0 Then
                        SendSMSMessage(msg_NoEntitlement, msgRequest.MobileNumber, 0, 0)
                        SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_NoEntitlement, msgRequest)
                    End If
                    'End If
                ElseIf model.OneThirdRule IsNot Nothing AndAlso Not model.OneThirdRule.OneThirdRulePassed AndAlso model.OneThirdRule.MonthsLeftToSettle > 0 Then
                    SendSMSMessage(msg_NoOneThirdFailed.Replace("<<date>>", DateAdd(DateInterval.Month, model.OneThirdRule.MonthsLeftToSettle, Now()).ToShortDateString), msgRequest.MobileNumber, 0, 0)
                    SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_NoOneThirdFailed.Replace("<<date>>", DateAdd(DateInterval.Month, model.OneThirdRule.MonthsLeftToSettle, Now()).ToShortDateString), msgRequest)
                Else
                    SendSMSMessage(msg_CannotDetermine, msgRequest.MobileNumber, 0, 0)
                    SaveCanITopUpMessageMessage(model.MemberDetails.MemberID, msg_CannotDetermine, msgRequest)
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            SendSMSMessage(msg_SystemException, msgRequest.MobileNumber, 0, 0)
        End Try
    End Sub

    Private Sub SaveCanITopUpMessageMessage(ByVal MemberID As Integer _
                                  , ByVal ResponseMessage As String _
                                  , ByVal msgRequest As AutoLoanFacade.SMSRequestFormatter)
        objAutoLoan.InsertEsendexSMSCommunicationEntry(MemberID, msgRequest.MobileNumber, msgRequest.MessageText, msgRequest.InboundMessageID, msgRequest.InboundMessageAccountID, msgRequest.InboundMessageTo, ResponseMessage)
    End Sub
End Class
