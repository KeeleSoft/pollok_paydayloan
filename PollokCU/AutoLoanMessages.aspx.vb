﻿Imports System.Data

Partial Class AutoLoanMessages
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MessageCode As String = Request.QueryString("M")
        Dim AppType As String = If(Not String.IsNullOrEmpty(Request.QueryString("T")), Request.QueryString("T"), "PDL")


        pnlMsg1.Visible = False
        pnlMsg2.Visible = False
        pnlMsg3.Visible = False
        pnlM1.Visible = False
        pnlM2.Visible = False
        pnlM3.Visible = False
        pnlM4.Visible = False
        pnlUnknown.Visible = False

        Select Case MessageCode
            Case "MSG1"
                If AppType = "OD" Then
                    litLoanName1.Text = "CU Through"
                    'litLoanName2.Text = "CU Through"
                Else
                    litLoanName1.Text = "WeeGlasgow Loan"
                    'litLoanName2.Text = "handiloan"
                End If
                'litNextFP.Text = GetNextFasterPaymentTimeTexr()
                pnlMsg1.Visible = True
            Case "MSG2"
                pnlMsg2.Visible = True
            Case "MSG3"
                pnlMsg3.Visible = True
            Case "M1"
                pnlM1.Visible = True
            Case "M2"
                pnlM2.Visible = True
            Case "M3"
                pnlM3.Visible = True
            Case "M4"
                pnlM4.Visible = True
            Case Else
                pnlUnknown.Visible = True

        End Select

        If Session("MemberID") Is Nothing Then
            'MenuLogged1.Visible = False
            'MenuPreLogged1.Visible = True
        Else
            'MenuLogged1.Visible = True
            'MenuPreLogged1.Visible = False
        End If
    End Sub

    Private Function GetNextFasterPaymentTimeTexr() As String
        Dim fpText As String = String.Empty
        Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
        Try
            Dim FPTime = objSystemData.GetSystemKeyValue("LastFasterPaymentProcessed")

            If Not String.IsNullOrEmpty(FPTime) Then
                Dim FPTimeConverted As DateTime = Nothing
                If DateTime.TryParse(FPTime, FPTimeConverted) Then
                    fpText = "Our Next Faster payment schedule is on " & DateAdd(DateInterval.Hour, 6, FPTimeConverted).ToString
                End If
            End If
            Return fpText
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return fpText
        End Try
    End Function
End Class
