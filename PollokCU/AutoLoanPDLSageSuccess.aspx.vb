﻿Imports System.Data
Imports PollokCU

Partial Class AutoLoanPDLSageSuccess
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        'If Session("MemberID") Is Nothing Then
        '    Response.Redirect("Login.aspx")
        'Else
        '    If Session("JuniorMember") Then
        '        Response.Redirect("JuniorMemberMessage.aspx")
        '    End If
        'End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString("AutoLoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("AutoLoanID")

            If objPayDayLoan.GetPDLPaymentSentStatus(Request.QueryString("AutoLoanID"), "PDLALPS") Then    'When there is a payment recorded against this ID do not proceed
                Response.Redirect("PDL\PDLError.aspx?C=E7")
            End If

            If Not IsPostBack Then
                SMSSecurityCode(Request.QueryString("AutoLoanID"))
            End If
        Else
            Response.Redirect("AutoLoanMemberNewApplication.aspx")
        End If
    End Sub

    Private Sub SMSSecurityCode(ByVal iLoanID As Integer)
        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        lblMessege.Visible = False
        Try
            Dim model As AutoLoanFacade.AutoLoanModel
            Dim dob As Date = Now
            If Session("AutoLoanModel") IsNot Nothing Then
                model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
                dob = model.MemberDetails.DateOfBirth
                Recipient = model.MemberDetails.MobileNumber
            Else
                model = New AutoLoanFacade.AutoLoanModel
                model.AutoLoanID = iLoanID
            End If

            Dim SecurityCode As String = AutoLoanFacade.AutoLoanEngine.GenerateSecurityCode(dob)
            If model IsNot Nothing Then
                model.SMSSecurityCode = SecurityCode
            End If
            Session("AutoLoanModel") = model

            Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
            Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
            Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
            Body = SecurityCode

            Dim LoanDetails As DataTable = objAutoLoan.GetAutoLoanDetail(iLoanID)
            If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                With LoanDetails.Rows(0)
                    Recipient = .Item("MobileNumber")
                End With
            End If

            If Recipient.Length <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "No mobile number found to send the security code"
                Return
            End If

            Dim header As esendex.MessengerHeader

            header = New esendex.MessengerHeader
            header.Username = Username
            header.Password = Password
            header.Account = Account

            Dim service As esendex.SendService

            service = New esendex.SendService
            service.MessengerHeaderValue = header
            service.SendMessage(Recipient, Body, esendex.MessageType.Text)
            objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, 0)


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Protected Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False
            Dim model As AutoLoanFacade.AutoLoanModel
            Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            If objPayDayLoan.GetPDLPaymentSentStatus(iLoanID, "PDLALPS") Then    'When there is a payment recorded against this ID do not proceed
                Response.Redirect("PDL\PDLError.aspx?C=E7")
                Return
            End If

            lblMessege.Visible = True
            If txtSecurityCode.Text <> txtSecurityCodeConfirm.Text Then
                lblMessege.Text = "Please verify the security code and the confirmed security code are same."
                Return
            End If
            model = DirectCast(Session("AutoLoanModel"), AutoLoanFacade.AutoLoanModel)
            If hfAttempt.Value >= 3 Then
                ''Refer to staff with the status
                model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Security_Failed
                model.SaveAutoLoanModelDetails(2)
                Response.Redirect("PDL\PDLMessage.aspx?M=MSG2")
            End If

            If txtSecurityCode.Text <> model.SMSSecurityCode Then
                hfAttempt.Value = CInt(hfAttempt.Value) + 1
                lblMessege.Text = "Security code does not match. Please re-try. (Attempt " & hfAttempt.Value & ")"
                Return
            End If

            'SMS Is OK, Generate all payments and schedules
            If model.SortCode = "089401" OrElse model.SortCode = "089409" Then
                'Send the cuca email to setup SO
                SendCUCAPDLALPSEmail(model)
            Else
                objSage.InsertSagePayCollectionALPSPDL(model.AutoLoanID, If(model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay, "PDLALPS", "ODLOAN"))
            End If
            If model.PaymentMethod = "FP" Then
                Dim PayRef As String = model.AutoLoanID.ToString & "-" & model.MemberID.ToString
                Dim LoanAmountInPence As Integer = Integer.Parse(model.FinalLoanAmount) * 100
                Dim TxnType As Integer = 99

                objPayDayLoan.InsertPaymentInstruction(model.AutoLoanID, model.MemberID _
                                                       , model.SortCode, model.AccountNumber _
                                                       , model.MemberDetails.FirstName & " " & model.MemberDetails.Surname _
                                                       , PayRef, LoanAmountInPence, TxnType, 26, True, If(model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay, "PDLALPS", "ODLOAN")) '26=Online.User Staff Account
            ElseIf model.PaymentMethod = "BACS" Then
                objAutoLoan.UpdateAutoLoanSendToBACS(model.AutoLoanID, 26)
            End If
            model.ApplicationStatus = AutoLoanFacade.AutoLoanEnums.ApplicationStatus.Approved_Automatically
            model.SaveAutoLoanModelDetails(3)
            SendPDLShare2Loan(model)    'Sed the PDLShare email if required
            SendSettleFromLastSagePaymentEmail(model) 'Send Settle from last sage payment email if required
            Response.Redirect("AutoLoanMessages.aspx?M=MSG1")
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Private Sub SendCUCAPDLALPSEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            Dim sEmailFrom As String = "noreply@cuonline.org.uk"
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

            Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetCUCAPDLALPSEmailBody(model)

            objEmail.SendEmailMessage(sEmailFrom _
                                         , "theweeloan@pollokcu.com" _
                                         , "PDL ALPS Standing Order Setup - AUTO EMAIL" _
                                         , emailBody _
                                         , "" _
                                         , ""
                                         )
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Private Sub SendPDLShare2Loan(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PDLShareToLoanRequired Then
                Dim sEmailFrom As String = "noreply@cuonline.org.uk"
                Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

                Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetPDLShare2LoanEmailBody(model)

                objEmail.SendEmailMessage(sEmailFrom _
                                             , "theweeloan@pollokcu.com" _
                                             , "PDL ALPS PDLShare Required - AUTO EMAIL" _
                                             , emailBody _
                                             , "" _
                                             , ""
                                             )

                objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "PDL Share2Loan email sent: " & emailBody, Session("StaffID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        End Try
    End Sub

    Private Sub SendSettleFromLastSagePaymentEmail(ByVal model As AutoLoanFacade.AutoLoanModel)
        Try
            If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.SettleFromLastSagePayment Then
                Dim sEmailFrom As String = "noreply@cuonline.org.uk"
                Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

                Dim emailBody As String = AutoLoanFacade.EmailBodyHelper.GetSettleFromLastSagePaymentEmailBody(model)

                objEmail.SendEmailMessage(sEmailFrom _
                                             , "theweeloan@pollokcu.com" _
                                             , "PDL ALPS Settle From Last Sage Payment Required - AUTO EMAIL" _
                                             , emailBody _
                                             , "" _
                                             , ""
                                             )

                objAutoLoan.InsertAutoLoanNote(model.AutoLoanID, "PDL Settle From Last Sage Payment email sent: " & emailBody, Session("StaffID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        End Try
    End Sub
End Class
