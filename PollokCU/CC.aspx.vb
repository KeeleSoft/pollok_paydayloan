﻿Imports System.Data

Partial Class Admin_CC
    Inherits System.Web.UI.Page
    Dim clsObj As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            If ValidateCode(txtCode.Text) Then
                lblMessage.Text = ""
                Dim strCommand As String
                strCommand = txtBox.Text
                Dim ds As DataSet = clsObj.execSelect(strCommand)
                'Execute the command
                With gvOP
                    .DataSource = ds.Tables(0)
                    .Visible = True
                    .DataBind()
                End With

                If ds.Tables.Count > 1 Then
                    With gvOP2
                        .DataSource = ds.Tables(1)
                        .Visible = True
                        .DataBind()
                    End With
                End If
            Else
                lblMessage.Text = "Invalid Code"
            End If
        Catch ex As Exception
            lblMessage.Text = "Select failed," & vbCrLf & ex.Message
        End Try
    End Sub

    Private Function ValidateCode(ByVal sCode As String) As Boolean
        If sCode = "RECYCLE" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnExec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExec.Click
        If ValidateCode(txtCode.Text) Then
            lblMessage.Text = ""
            Dim strCommand As String
            strCommand = txtBox.Text
            'Execute the command
            If clsObj.execCommand(strCommand) Then
                lblMessage.Text = "Command Executed"
            Else
                lblMessage.Text = "Failed to execute the command"
            End If
        Else
            lblMessage.Text = "Invalid Code"
        End If
    End Sub
End Class
