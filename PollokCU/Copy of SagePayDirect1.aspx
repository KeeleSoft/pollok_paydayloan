﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageHeaderOnly2.master" AutoEventWireup="false" CodeFile="Copy of SagePayDirect1.aspx.vb" Inherits="SagePayDirect1" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="130" valign="top">
                <uc1:MenuLogged ID="MenuLogged1" runat="server" />
                <uc4:MenuPreLogged ID="MenuPreLogged1" runat="server" />
            </td>
            <td width="23">
                &nbsp;</td>
            <td valign="top">
                <table width="466" border="0" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
                    <tr>
                        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5">
                            <span class="style37 style80">&nbsp;Make a payment to LCCU</span></td>
                    </tr>
                    <tr>
                        <td height="25" valign="middle" class="Button style5">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                        </td>
                    </tr>
                    <tr>
                        <td height="10" valign="middle" class="BodyText">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="BodyText" style="width: 100%;">
                                            <tr>
                                                <td width="30%">
                                                    Member Number:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                                        runat="server" ControlToValidate="txtMemberNumber" 
                                                        ErrorMessage="Member number required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtMemberNumber" runat="server" Width="150px" MaxLength="15"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Reason for payment:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReason" runat="server" Width="152px">
                                                        <asp:ListItem>Pay Day Loan Repayment</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Amount to pay:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                                        runat="server" ControlToValidate="txtAmount" ErrorMessage="Amount required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtAmount" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    First name:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                                        runat="server" ControlToValidate="txtFirstName" 
                                                        ErrorMessage="First name required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" Width="150px" MaxLength="7"></asp:TextBox>
                                                </td>
                                                <td>
                                                    (name as it appears on card)</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Surname:<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                        ControlToValidate="txtSurname" ErrorMessage="Surname required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtSurname" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                                </td>
                                                <td>
                                                    (name as it appears on card)</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email:<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                        ControlToValidate="txtEmail" ErrorMessage="Email required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Billing address line 1:<asp:RequiredFieldValidator ID="RequiredFieldValidator6" 
                                                        runat="server" ControlToValidate="txtAddressLine1" 
                                                        ErrorMessage="Billing address line 1 required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtAddressLine1" runat="server" Width="150px" MaxLength="30"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Billing address line 2:</td>
                                                <td>
                    <asp:TextBox ID="txtAddressLine2" runat="server" Width="150px" MaxLength="30"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Billing city:<asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                                                        runat="server" ControlToValidate="txtCity" ErrorMessage="Billing city required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtCity" runat="server" Width="150px" MaxLength="20"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Billing post code:<asp:RequiredFieldValidator ID="RequiredFieldValidator8" 
                                                        runat="server" ControlToValidate="txtPostCode" 
                                                        ErrorMessage="Billing post code required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtPostCode" runat="server" Width="150px" MaxLength="8"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br>Leeds City Credit Union will never ask you for your memorable data or Password in an e-mail. Never disclose this information to anyone.<br />
                            <br />For security reasons, when you have finished using Internet Banking always select LOG OUT.
                        </td>
                    </tr>
          <!-- TemplateEndEditable -->
                </table>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

