﻿Imports SageIncludes
Partial Class SagePayDirect1
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
            txtMemberNumber.Text = Session("MemberID")
            txtMemberNumber.Enabled = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            lblMessege.Visible = False

            'MemberID Validation
            Dim MemberID As Integer = 0
            Integer.TryParse(txtMemberNumber.Text, MemberID)
            If MemberID <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid member number"
                Return
            End If

            'Amount Validation
            Dim Amount As Double = 0
            Double.TryParse(txtAmount.Text, Amount)
            If Amount <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid amount to pays"
                Return
            End If

            'Firstname Validation
            Dim FirstName As String = txtFirstName.Text
            If Not isValidNameField(FirstName) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid first name"
                Return
            End If
            
            'Surname Validation
            Dim SurName As String = txtSurname.Text
            If Not isValidNameField(SurName) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid surname"
                Return
            End If

            'Email Validation
            Dim Email As String = txtEmail.Text
            If Not isValidEmailField(Email) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid email"
                Return
            End If

            'Address line 1 Validation
            Dim AddressLine1 As String = txtAddressLine1.Text
            If Not isValidAddressField(AddressLine1, True) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid address line 1"
                Return
            End If

            'Address line 2 Validation
            Dim AddressLine2 As String = txtAddressLine2.Text
            If Not isValidAddressField(AddressLine2, False) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid address line 2"
                Return
            End If

            'City Validation
            Dim City As String = txtCity.Text
            If Not isValidCityField(City, True) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid city"
                Return
            End If

            'City Validation
            Dim PostCode As String = txtPostCode.Text
            If Not isValidPostcodeField(PostCode) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid city"
                Return
            End If

            Dim Reason As String = ddlReason.SelectedValue

            'When all entered are OK, goto confirm page
            Session("MemberIDPayment") = MemberID
            Session("Amount") = Amount
            Session("FirstName") = FirstName
            Session("SurName") = SurName
            Session("Email") = Email
            Session("AddressLine1") = AddressLine1
            Session("AddressLine2") = AddressLine2
            Session("City") = City
            Session("PostCode") = PostCode
            Session("Reason") = Reason
            Response.Redirect("SagePayDirect2.aspx")
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub
End Class
