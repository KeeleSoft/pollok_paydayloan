﻿
Partial Class MasterPageHeaderOnly
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''''Google Analytics Javascript Code begins here
        Dim litGoogleAnalytics As System.Web.UI.LiteralControl = New System.Web.UI.LiteralControl
        Dim sbGoogleScript As System.Text.StringBuilder = New System.Text.StringBuilder
        sbGoogleScript.Append("<script type='text/javascript'>")
        sbGoogleScript.Append("var gaJsHost = ((""https:"" == document.location.protocol) ? ""https://ssl."" : ""http://www."");")
        sbGoogleScript.Append("document.write(unescape(""%3Cscript src='"" + gaJsHost + ""google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E""));")
        sbGoogleScript.Append("</script>")
        sbGoogleScript.Append("<script type=""text/javascript"">")
        sbGoogleScript.Append("if (typeof(_gat) == 'object') {var pageTracker = _gat._getTracker(""" + System.Configuration.ConfigurationManager.AppSettings("GoogleAnalyticsLIVE") + """);")
        sbGoogleScript.Append("pageTracker._trackPageview();}")
        sbGoogleScript.Append("</script>")
        litGoogleAnalytics.Text = sbGoogleScript.ToString()
        Page.Header.Controls.Add(litGoogleAnalytics)

        ''''Google Analytics Javascript Code ends here
    End Sub
End Class

