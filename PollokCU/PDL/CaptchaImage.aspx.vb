﻿Imports System.Drawing.Imaging

Partial Class PDL_CaptchaImage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Declare The CaptchaObject
        Dim xcaptcha As Captcha_Image.CaptchaClass.CaptchaImage
        'Create The Captcha Object      
        xcaptcha = New Captcha_Image.CaptchaClass.CaptchaImage(150, 60, "Verdana", Captcha_Image.CaptchaClass.CaptchaImage.HatchStyles.Cross, Drawing.Color.GhostWhite, Drawing.Color.LawnGreen)
        'Set the Captcha Text to a session variable
        Session("CaptchaText") = xcaptcha.catpchaText
        'Set the response of this page to an image
        Response.ContentType = "image/jpeg"
        'output the image to the response
        xcaptcha.Image.Save(Response.OutputStream, ImageFormat.Jpeg)
        'Close and Dispose the Captcha Object
        xcaptcha.Dispose()

    End Sub
End Class
