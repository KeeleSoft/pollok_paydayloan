﻿
Partial Class PDL_CreateSes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SessionID As String = Request.QueryString("S")
        If SessionID <> "" Then
            Session("SessionUserObj") = GetSessionDetails(SessionID)
            Response.Write("DONE")
        Else
            Response.Write("INVALID")
        End If
    End Sub

    Private Function GetSessionDetails(ByVal SessionID As String) As PDLSessionUser
        Dim SessObj As PDLSessionUser = New PDLSessionUser(SessionID, "LCCU")
        Return SessObj
    End Function
End Class
