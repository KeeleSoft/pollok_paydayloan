﻿Imports PollokCU.DataAccess.Layer
Partial Class PDL_Default
    Inherits System.Web.UI.Page

    Private Shared SessionObj As PDLSessionUser = Nothing
    Private PDLAccess As PollokCU.DataAccess.Layer.clsPayDayLoan = New clsPayDayLoan
    Dim HowLong As Int16
    Dim Borrow As Double


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ScriptManager1.RegisterAsyncPostBackControl(Slider1)
        'ScriptManager1.RegisterAsyncPostBackControl(Slider2)

        Dim MinAfford As Double
        Dim MaxAfford As Double
        Dim ChangedValue As Double
        Dim Total As Double
        Dim Interest As Double
        Dim SessID As String = ""
        Dim interestRateAPR As Double = 0.0

        lblMessage.Visible = False
        If Session("SessionUserObj") Is Nothing Then
            Dim CU As String = ""
            CU = Request.QueryString("CU")

            If CU Is Nothing OrElse CU.Length = 0 Then
                'Error saying not proper CU
                Response.Redirect("Default.aspx?CU=PCU")
            End If
            If CU <> "PCU" Then
                CU = "PCU"
            End If

            For i = 1 To 10 'Loop 10 times to find a unique SessionID. Hopefully at first attempt it should find a one. Just in case not loop.
                SessID = System.Guid.NewGuid.ToString
                If Not PDLAccess.IsSessionIDAlreadyUsed(SessID) Then
                    Exit For
                End If
            Next

            SessionObj = New PDLSessionUser(SessID, CU)
            Session("SessionUserObj") = SessionObj
        End If
        interestRateAPR = 0.24  ' CType(Session("SessionUserObj"), PDLSessionUser).LoanAPR / 100
        'If SessionObj.CUID = 0 Then
        '    Integer.TryParse(Request.QueryString("CU"), SessionObj.CUID)
        'End If
        If (Page.IsPostBack) Then
            Dim CtrlID As String = String.Empty
            If Request.Form("__EVENTTARGET") IsNot Nothing And Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            HowLong = Math.Min(CInt(txtHowLong.Text), 12)
            Borrow = Math.Min(CDbl(txtHowMuch.Text), 400)
            If CtrlID.Contains("txtAfford") Then
                ChangedValue = txtAfford.Text
                Dim AffordPeriod As Double = NPer(interestRateAPR / 12, -1 * txtAfford.Text, Borrow)
                txtHowLong.Text = Math.Ceiling(Math.Round(AffordPeriod, 2))
                HowLong = txtHowLong.Text

                Dim TotalToPay As Double = AffordPeriod * txtAfford.Text
                Interest = Math.Round(TotalToPay - Borrow, 2)

                PopulateResultsTable(Borrow, HowLong, Interest, txtAfford.Text)
                'litResultText.Text = AffordPeriod.ToString & ": " & Math.Ceiling(AffordPeriod) & " Borrowing: £" & Borrow.ToString & " + interest & fees £" & txtInterest.Text.ToString & " = Total to repay £" & txtTotal.Text.ToString
                RecordActivity(CtrlID, ChangedValue)
            ElseIf CtrlID <> "" Then
                If CtrlID.Contains("txtHowMuch") Then
                    ChangedValue = txtHowMuch.Text
                ElseIf CtrlID.Contains("txtHowLong") Then
                    ChangedValue = txtHowLong.Text
                End If

                MaxAfford = Pmt(interestRateAPR / 12, 1, -1 * Borrow)
                MinAfford = Pmt(interestRateAPR / 12, 3, -1 * Borrow)
                txtAfford_SliderExtender.Minimum = MinAfford
                txtAfford_SliderExtender.Maximum = MaxAfford
                If ChangedValue > 0 Then
                    txtAfford.Text = MinAfford
                    Dim X As Double = Math.Round(Pmt(interestRateAPR / 12, HowLong, -1 * Borrow), 2)
                    Total = Math.Round(X * (HowLong), 2)
                    Interest = Math.Round(Total - Borrow, 2)
                Else
                    Total = hfTotalToPay.Value
                    Interest = hfTotalInterest.Value
                End If


                PopulateResultsTable(Borrow, HowLong, Interest, 0)  'Pass affordability as 0 as no need
                'litResultText.Text = "Borrowing: £" & Borrow.ToString & " + interest & fees £" & txtInterest.Text.ToString & " = Total to repay £" & txtTotal.Text.ToString
                RecordActivity(CtrlID, ChangedValue)
            End If

        Else
            'Create a new session if the SessionID already in use and this is a fresh page request and not a post back
            SessID = CType(Session("SessionUserObj"), PDLSessionUser).SSID
            If PDLAccess.IsSessionIDAlreadyUsed(SessID) Then
                For i = 1 To 10 'Loop 10 times to find a unique SessionID. Hopefully at first attempt it should find a one. Just in case not loop.
                    SessID = System.Guid.NewGuid.ToString
                    If Not PDLAccess.IsSessionIDAlreadyUsed(SessID) Then
                        Exit For
                    End If
                Next

                Dim CU As String = Request.QueryString("CU")
                SessionObj = New PDLSessionUser(SessID, CU)
                Session("SessionUserObj") = SessionObj
            End If


            Borrow = If(Session("Borrow") IsNot Nothing AndAlso Session("Borrow") > 0, Session("Borrow"), 100) 'txtHowMuch.Text
            HowLong = If(Session("Period") IsNot Nothing AndAlso Session("Period") > 0, Session("Period"), 1) 'txtHowLong.Text
            MinAfford = Pmt(interestRateAPR / 12, 3, -1 * Borrow)
            MaxAfford = Pmt(interestRateAPR / 12, 1, -1 * Borrow)
            txtAfford_SliderExtender.Minimum = MinAfford
            txtAfford_SliderExtender.Maximum = MaxAfford

            Dim X As Double = Math.Round(Pmt(interestRateAPR / 12, HowLong, -1 * Borrow), 2)
            Total = Math.Round(X * (HowLong), 2)
            Interest = Math.Round(Total - Borrow, 2)
            PopulateResultsTable(Borrow, HowLong, Interest, 0)
        End If
    End Sub

    Private Sub PopulateResultsTable(ByVal Amount As Double, ByVal Period As Integer, ByVal Interest As Double, ByVal Afford As Double)
        Dim Month1Repay As Double = 0
        Dim Month2Repay As Double = 0
        Dim Month3Repay As Double = 0
        Dim Month4Repay As Double = 0
        Dim Month5Repay As Double = 0
        Dim Month6Repay As Double = 0
        Dim Month7Repay As Double = 0
        Dim Month8Repay As Double = 0
        Dim Month9Repay As Double = 0
        Dim Month10Repay As Double = 0
        Dim Month11Repay As Double = 0
        Dim Month12Repay As Double = 0

        Dim Month1OtherCost As Double = 0
        Dim Month2OtherCost As Double = 0
        Dim Month3OtherCost As Double = 0
        Dim Month4OtherCost As Double = 0
        Dim Month5OtherCost As Double = 0
        Dim Month6OtherCost As Double = 0
        Dim Month7OtherCost As Double = 0
        Dim Month8OtherCost As Double = 0
        Dim Month9OtherCost As Double = 0
        Dim Month10OtherCost As Double = 0
        Dim Month11OtherCost As Double = 0
        Dim Month12OtherCost As Double = 0

        Dim Total As Double = 0
        Total = Amount + Interest

        lit1.Text = "Loan of £" & Amount.ToString & " over " & Period.ToString & " month(s)"

        litSummaryTotal.Text = "£" & Format(Amount, "0.00")
        litSummaryInterest.Text = "£" & Format(Interest, "0.00")
        litTotalInterestDesc.Text = "Total interest at " & Format(CType(Session("SessionUserObj"), PDLSessionUser).LoanAPR, "0.0") & "% APR"

        Dim InstantDecisionFee As Double = CType(Session("SessionUserObj"), PDLSessionUser).InstantDecisionFee
        Dim SameDayPay As Double = CType(Session("SessionUserObj"), PDLSessionUser).SamedayPaymentFee
        Dim RepeatAccFee As Double = CType(Session("SessionUserObj"), PDLSessionUser).RepeatAccFacility
        Dim TotalExpense As Double = 0
        If radInstantDecisionYes.Checked Then
            TotalExpense = SameDayPay + InstantDecisionFee + RepeatAccFee
            litInstantDecision.Text = String.Format("{0:c2}", TotalExpense)
            hfTotalExpenses.Value = TotalExpense
            Session("TotalExpenses") = TotalExpense
        Else
            TotalExpense = 0
            litInstantDecision.Text = String.Format("{0:c2}", 0)
            hfTotalExpenses.Value = 0
            Session("TotalExpenses") = 0
        End If

        litTotalCost.Text = "£" & Format(TotalExpense, "0.00")
        litCUOKCost.Text = "£" & Format(Interest + TotalExpense, "0.00")
        'litOtherLenderCost.Text = "£" & Format(Amount / 100 * 30, "0.00")
        litOtherLenderCost.Text = "£" & Format(Amount / 5 * 1.86, "0.00")   '£1.86 per every £5 loan giving
        litSave.Text = "£" & Format(CDbl(litOtherLenderCost.Text) - CDbl(litCUOKCost.Text), "0.00")

        lit3.Text = "£" & Format(Total + TotalExpense, "0.00") 'Total To Pay

        If Afford > 0 Then
            Select Case Period
                Case 1
                    Month1Repay = Total
                    Month2Repay = 0
                    Month3Repay = 0

                    Month1OtherCost = TotalExpense
                    Month2OtherCost = 0
                    Month3OtherCost = 0
                Case 2
                    Month1Repay = Afford
                    Month2Repay = Math.Round(Total - Afford, 2)
                    Month3Repay = 0

                    Month1OtherCost = (TotalExpense) / 2
                    Month2OtherCost = (TotalExpense) / 2
                    Month3OtherCost = 0
                Case 3
                    Month1Repay = Afford
                    Month2Repay = Afford
                    Month3Repay = Math.Round(Total - (Afford * 2), 2)

                    Month1OtherCost = (TotalExpense) / 3
                    Month2OtherCost = (TotalExpense) / 3
                    Month3OtherCost = (TotalExpense) / 3
            End Select
        Else
            Select Case Period
                Case 1
                    Month1Repay = Total
                    Month2Repay = 0
                    Month3Repay = 0
                    Month4Repay = 0
                    Month5Repay = 0
                    Month6Repay = 0
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = TotalExpense
                    Month2OtherCost = 0
                    Month3OtherCost = 0
                    Month4OtherCost = 0
                    Month5OtherCost = 0
                    Month6OtherCost = 0
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 2
                    Month1Repay = Total / 2
                    Month2Repay = Total / 2
                    Month3Repay = 0
                    Month4Repay = 0
                    Month5Repay = 0
                    Month6Repay = 0
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0


                    Month1OtherCost = (TotalExpense) / 2
                    Month2OtherCost = (TotalExpense) / 2
                    Month3OtherCost = 0
                    Month4OtherCost = 0
                    Month5OtherCost = 0
                    Month6OtherCost = 0
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 3
                    Month1Repay = Total / 3
                    Month2Repay = Total / 3
                    Month3Repay = Total / 3
                    Month4Repay = 0
                    Month5Repay = 0
                    Month6Repay = 0
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 3
                    Month2OtherCost = (TotalExpense) / 3
                    Month3OtherCost = (TotalExpense) / 3
                    Month4OtherCost = 0
                    Month5OtherCost = 0
                    Month6OtherCost = 0
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 4
                    Month1Repay = Total / 4
                    Month2Repay = Total / 4
                    Month3Repay = Total / 4
                    Month4Repay = Total / 4
                    Month5Repay = 0
                    Month6Repay = 0
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 4
                    Month2OtherCost = (TotalExpense) / 4
                    Month3OtherCost = (TotalExpense) / 4
                    Month4OtherCost = (TotalExpense) / 4
                    Month5OtherCost = 0
                    Month6OtherCost = 0
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 5
                    Month1Repay = Total / 5
                    Month2Repay = Total / 5
                    Month3Repay = Total / 5
                    Month4Repay = Total / 5
                    Month5Repay = Total / 5
                    Month6Repay = 0
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 5
                    Month2OtherCost = (TotalExpense) / 5
                    Month3OtherCost = (TotalExpense) / 5
                    Month4OtherCost = (TotalExpense) / 5
                    Month5OtherCost = (TotalExpense) / 5
                    Month6OtherCost = 0
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 6
                    Month1Repay = Total / 6
                    Month2Repay = Total / 6
                    Month3Repay = Total / 6
                    Month4Repay = Total / 6
                    Month5Repay = Total / 6
                    Month6Repay = Total / 6
                    Month7Repay = 0
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 6
                    Month2OtherCost = (TotalExpense) / 6
                    Month3OtherCost = (TotalExpense) / 6
                    Month4OtherCost = (TotalExpense) / 6
                    Month5OtherCost = (TotalExpense) / 6
                    Month6OtherCost = (TotalExpense) / 6
                    Month7OtherCost = 0
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 7
                    Month1Repay = Total / 7
                    Month2Repay = Total / 7
                    Month3Repay = Total / 7
                    Month4Repay = Total / 7
                    Month5Repay = Total / 7
                    Month6Repay = Total / 7
                    Month7Repay = Total / 7
                    Month8Repay = 0
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 7
                    Month2OtherCost = (TotalExpense) / 7
                    Month3OtherCost = (TotalExpense) / 7
                    Month4OtherCost = (TotalExpense) / 7
                    Month5OtherCost = (TotalExpense) / 7
                    Month6OtherCost = (TotalExpense) / 7
                    Month7OtherCost = (TotalExpense) / 7
                    Month8OtherCost = 0
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 8
                    Month1Repay = Total / 8
                    Month2Repay = Total / 8
                    Month3Repay = Total / 8
                    Month4Repay = Total / 8
                    Month5Repay = Total / 8
                    Month6Repay = Total / 8
                    Month7Repay = Total / 8
                    Month8Repay = Total / 8
                    Month9Repay = 0
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 8
                    Month2OtherCost = (TotalExpense) / 8
                    Month3OtherCost = (TotalExpense) / 8
                    Month4OtherCost = (TotalExpense) / 8
                    Month5OtherCost = (TotalExpense) / 8
                    Month6OtherCost = (TotalExpense) / 8
                    Month7OtherCost = (TotalExpense) / 8
                    Month8OtherCost = (TotalExpense) / 8
                    Month9OtherCost = 0
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 9
                    Month1Repay = Total / 9
                    Month2Repay = Total / 9
                    Month3Repay = Total / 9
                    Month4Repay = Total / 9
                    Month5Repay = Total / 9
                    Month6Repay = Total / 9
                    Month7Repay = Total / 9
                    Month8Repay = Total / 9
                    Month9Repay = Total / 9
                    Month10Repay = 0
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 9
                    Month2OtherCost = (TotalExpense) / 9
                    Month3OtherCost = (TotalExpense) / 9
                    Month4OtherCost = (TotalExpense) / 9
                    Month5OtherCost = (TotalExpense) / 9
                    Month6OtherCost = (TotalExpense) / 9
                    Month7OtherCost = (TotalExpense) / 9
                    Month8OtherCost = (TotalExpense) / 9
                    Month9OtherCost = (TotalExpense) / 9
                    Month10OtherCost = 0
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 10
                    Month1Repay = Total / 10
                    Month2Repay = Total / 10
                    Month3Repay = Total / 10
                    Month4Repay = Total / 10
                    Month5Repay = Total / 10
                    Month6Repay = Total / 10
                    Month7Repay = Total / 10
                    Month8Repay = Total / 10
                    Month9Repay = Total / 10
                    Month10Repay = Total / 10
                    Month11Repay = 0
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 10
                    Month2OtherCost = (TotalExpense) / 10
                    Month3OtherCost = (TotalExpense) / 10
                    Month4OtherCost = (TotalExpense) / 10
                    Month5OtherCost = (TotalExpense) / 10
                    Month6OtherCost = (TotalExpense) / 10
                    Month7OtherCost = (TotalExpense) / 10
                    Month8OtherCost = (TotalExpense) / 10
                    Month9OtherCost = (TotalExpense) / 10
                    Month10OtherCost = (TotalExpense) / 10
                    Month11OtherCost = 0
                    Month12OtherCost = 0
                Case 11
                    Month1Repay = Total / 11
                    Month2Repay = Total / 11
                    Month3Repay = Total / 11
                    Month4Repay = Total / 11
                    Month5Repay = Total / 11
                    Month6Repay = Total / 11
                    Month7Repay = Total / 11
                    Month8Repay = Total / 11
                    Month9Repay = Total / 11
                    Month10Repay = Total / 11
                    Month11Repay = Total / 11
                    Month12Repay = 0

                    Month1OtherCost = (TotalExpense) / 11
                    Month2OtherCost = (TotalExpense) / 11
                    Month3OtherCost = (TotalExpense) / 11
                    Month4OtherCost = (TotalExpense) / 11
                    Month5OtherCost = (TotalExpense) / 11
                    Month6OtherCost = (TotalExpense) / 11
                    Month7OtherCost = (TotalExpense) / 11
                    Month8OtherCost = (TotalExpense) / 11
                    Month9OtherCost = (TotalExpense) / 11
                    Month10OtherCost = (TotalExpense) / 11
                    Month11OtherCost = (TotalExpense) / 11
                    Month12OtherCost = 0
                Case 12
                    Month1Repay = Total / 12
                    Month2Repay = Total / 12
                    Month3Repay = Total / 12
                    Month4Repay = Total / 12
                    Month5Repay = Total / 12
                    Month6Repay = Total / 12
                    Month7Repay = Total / 12
                    Month8Repay = Total / 12
                    Month9Repay = Total / 12
                    Month10Repay = Total / 12
                    Month11Repay = Total / 12
                    Month12Repay = Total / 12

                    Month1OtherCost = (TotalExpense) / 12
                    Month2OtherCost = (TotalExpense) / 12
                    Month3OtherCost = (TotalExpense) / 12
                    Month4OtherCost = (TotalExpense) / 12
                    Month5OtherCost = (TotalExpense) / 12
                    Month6OtherCost = (TotalExpense) / 12
                    Month7OtherCost = (TotalExpense) / 12
                    Month8OtherCost = (TotalExpense) / 12
                    Month9OtherCost = (TotalExpense) / 12
                    Month10OtherCost = (TotalExpense) / 12
                    Month11OtherCost = (TotalExpense) / 12
                    Month12OtherCost = (TotalExpense) / 12
            End Select
        End If


        hfBorrow.Value = Amount
        Session("Borrow") = Amount
        hfPeriod.Value = Period
        Session("Period") = Period
        hfAfford.Value = Afford
        Session("Afford") = Afford
        hfTotalToPay.Value = Total + TotalExpense
        Session("TotalToPay") = Total + TotalExpense
        hfTotalInterest.Value = Interest
        Session("TotalInterest") = Interest
        hfMonth1Pay.Value = Month1Repay + Month1OtherCost
        Session("Month1Pay") = Month1Repay + Month1OtherCost
        hfMonth2Pay.Value = Month2Repay + Month2OtherCost
        Session("Month2Pay") = Month2Repay + Month2OtherCost
        hfMonth3Pay.Value = Month3Repay + Month3OtherCost
        Session("Month3Pay") = Month3Repay + Month3OtherCost
        hfMonth4Pay.Value = Month4Repay + Month4OtherCost
        Session("Month4Pay") = Month4Repay + Month4OtherCost
        hfMonth5Pay.Value = Month5Repay + Month5OtherCost
        Session("Month5Pay") = Month5Repay + Month5OtherCost
        hfMonth6Pay.Value = Month6Repay + Month6OtherCost
        Session("Month6Pay") = Month6Repay + Month6OtherCost
        hfMonth7Pay.Value = Month7Repay + Month7OtherCost
        Session("Month7Pay") = Month7Repay + Month7OtherCost
        hfMonth8Pay.Value = Month8Repay + Month8OtherCost
        Session("Month8Pay") = Month8Repay + Month8OtherCost
        hfMonth9Pay.Value = Month9Repay + Month9OtherCost
        Session("Month9Pay") = Month9Repay + Month9OtherCost
        hfMonth10Pay.Value = Month10Repay + Month10OtherCost
        Session("Month10Pay") = Month10Repay + Month10OtherCost
        hfMonth11Pay.Value = Month11Repay + Month11OtherCost
        Session("Month11Pay") = Month11Repay + Month11OtherCost
        hfMonth12Pay.Value = Month12Repay + Month12OtherCost
        Session("Month12Pay") = Month12Repay + Month12OtherCost


        lit4.Text = "£" & Format(Month1Repay + Month1OtherCost, "0.00")
        lit5.Text = "£" & Format(Month2Repay + Month2OtherCost, "0.00")
        lit6.Text = "£" & Format(Month3Repay + Month3OtherCost, "0.00")
        lit20.Text = "£" & Format(Month4Repay + Month4OtherCost, "0.00")
        lit21.Text = "£" & Format(Month5Repay + Month5OtherCost, "0.00")
        lit22.Text = "£" & Format(Month6Repay + Month6OtherCost, "0.00")
        lit23.Text = "£" & Format(Month7Repay + Month7OtherCost, "0.00")
        lit24.Text = "£" & Format(Month8Repay + Month8OtherCost, "0.00")
        lit25.Text = "£" & Format(Month9Repay + Month9OtherCost, "0.00")
        lit26.Text = "£" & Format(Month10Repay + Month10OtherCost, "0.00")
        lit27.Text = "£" & Format(Month11Repay + Month11OtherCost, "0.00")
        lit28.Text = "£" & Format(Month12Repay + Month12OtherCost, "0.00")


        If Month2Repay = 0 Then
            row2Month.Visible = False
        Else
            row2Month.Visible = True
        End If

        If Month3Repay = 0 Then
            row3Month.Visible = False
        Else
            row3Month.Visible = True
        End If

        If Month4Repay = 0 Then
            row4Month.Visible = False
        Else
            row4Month.Visible = True
        End If

        If Month5Repay = 0 Then
            row5Month.Visible = False
        Else
            row5Month.Visible = True
        End If

        If Month6Repay = 0 Then
            row6Month.Visible = False
        Else
            row6Month.Visible = True
        End If

        If Month7Repay = 0 Then
            row7Month.Visible = False
        Else
            row7Month.Visible = True
        End If

        If Month8Repay = 0 Then
            row8Month.Visible = False
        Else
            row8Month.Visible = True
        End If

        If Month9Repay = 0 Then
            row9Month.Visible = False
        Else
            row9Month.Visible = True
        End If

        If Month10Repay = 0 Then
            row10Month.Visible = False
        Else
            row10Month.Visible = True
        End If

        If Month11Repay = 0 Then
            row11Month.Visible = False
        Else
            row11Month.Visible = True
        End If

        If Month12Repay = 0 Then
            row12Month.Visible = False
        Else
            row12Month.Visible = True
        End If

        
    End Sub

    Public Sub RecordActivity(ByVal ChangedControl As String, ByVal ChangedValue As Double)
        Try
            Dim ActivityID As Integer = 0
            Dim ChangedSlideIndex As Int16
            If ChangedControl.Contains("txtHowMuch") Then
                ChangedSlideIndex = 1
            ElseIf ChangedControl.Contains("txtHowLong") Then
                ChangedSlideIndex = 2
            ElseIf ChangedControl.Contains("txtAfford") Then
                ChangedSlideIndex = 3
            End If
            If ChangedSlideIndex > 0 Then
                ActivityID = PDLAccess.InsertCalcActivity(Request.ServerVariables("REMOTE_ADDR"), ChangedSlideIndex, ChangedValue, CType(Session("SessionUserObj"), PDLSessionUser).SSID, CType(Session("SessionUserObj"), PDLSessionUser).CUID, 0)
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
            'Throw ex    'To Do proper error handling
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            If CType(Session("SessionUserObj"), PDLSessionUser) IsNot Nothing Then

                'PDLAccess.UpdateCreatePreConfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, CType(Session("SessionUserObj"), PDLSessionUser).CUID _
                '                                       , hfBorrow.Value _
                '                                       , hfPeriod.Value, hfAfford.Value, hfTotalInterest.Value, hfTotalToPay.Value, hfMonth1Pay.Value, hfMonth2Pay.Value, hfMonth3Pay.Value, 0, 0, 0, hfTotalExpenses.Value, 0, 0)

                PDLAccess.UpdateCreatePreConfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, CType(Session("SessionUserObj"), PDLSessionUser).CUID _
                                                       , Session("Borrow") _
                                                       , Session("Period") _
                                                       , Session("Afford") _
                                                       , Session("TotalInterest") _
                                                       , Session("TotalToPay") _
                                                       , Session("Month1Pay") _
                                                       , Session("Month2Pay") _
                                                       , Session("Month3Pay") _
                                                       , Session("Month4Pay") _
                                                       , Session("Month5Pay") _
                                                       , Session("Month6Pay") _
                                                       , Session("Month7Pay") _
                                                       , Session("Month8Pay") _
                                                       , Session("Month9Pay") _
                                                       , Session("Month10Pay") _
                                                       , Session("Month11Pay") _
                                                       , Session("Month12Pay") _
                                                       , 0, 0, 0 _
                                                       , Session("TotalExpenses") _
                                                       , 0, 0, Request.ServerVariables("REMOTE_ADDR"))


                'Populate security captcha image
                imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
                txtEnteredWord.Text = ""
                mdlPopup.Show()
                'Response.Redirect("PDLSecurity.aspx")
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnOK2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApplyWithSecurity.Click
        If txtEnteredWord.Text.ToLower = Session("CaptchaText").ToString.ToLower Then
            mdlPopup.Hide()
            Response.Redirect("PDLApplyPDL1.aspx")
        Else
            imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
            txtEnteredWord.Text = ""
            mdlPopup.Show()
        End If
    End Sub

    Protected Sub lbtnTryAnother_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnTryAnother.Click
        imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
        txtEnteredWord.Text = ""
        mdlPopup.Show()
    End Sub

End Class

