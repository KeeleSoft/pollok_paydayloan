﻿Imports PollokCU.DataAccess.Layer
Partial Class PDL_Default_Template1
    Inherits System.Web.UI.Page

    Private Shared SessionObj As PDLSessionUser = Nothing
    Private PDLAccess As PollokCU.DataAccess.Layer.clsPayDayLoan = New clsPayDayLoan

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ScriptManager1.RegisterAsyncPostBackControl(Slider1)
        'ScriptManager1.RegisterAsyncPostBackControl(Slider2)
        Dim Borrow As Double
        Dim HowLong As Int16
        Dim MinAfford As Double
        Dim MaxAfford As Double
        Dim ChangedValue As Double
        Dim Total As Double
        Dim Interest As Double
        lblMessage.Visible = False
        If Session("SessionUserObj") Is Nothing Then
            Dim CU As String = ""
            CU = Request.QueryString("CU")

            If CU Is Nothing OrElse CU.Length = 0 Then
                'Error saying not proper CU
                Response.Redirect("PDLError.aspx?C=E1")
            End If
            SessionObj = New PDLSessionUser(Session.SessionID, CU)
            Session("SessionUserObj") = SessionObj
        End If

        'If SessionObj.CUID = 0 Then
        '    Integer.TryParse(Request.QueryString("CU"), SessionObj.CUID)
        'End If
        If (Page.IsPostBack) Then
            Dim CtrlID As String = String.Empty
            If Request.Form("__EVENTTARGET") IsNot Nothing And Request.Form("__EVENTTARGET") <> String.Empty Then
                CtrlID = Request.Form("__EVENTTARGET")
            End If

            HowLong = txtHowLong.Text
            Borrow = txtHowMuch.Text
            If CtrlID.Contains("txtAfford") Then
                ChangedValue = txtAfford.Text
                Dim AffordPeriod As Double = NPer(0.24 / 12, -1 * txtAfford.Text, Borrow)
                txtHowLong.Text = Math.Ceiling(Math.Round(AffordPeriod, 2))
                HowLong = txtHowLong.Text

                Dim TotalToPay As Double = AffordPeriod * txtAfford.Text
                Interest = Math.Round(TotalToPay - Borrow, 2)

                PopulateResultsTable(Borrow, HowLong, Interest, txtAfford.Text)
                'litResultText.Text = AffordPeriod.ToString & ": " & Math.Ceiling(AffordPeriod) & " Borrowing: £" & Borrow.ToString & " + interest & fees £" & txtInterest.Text.ToString & " = Total to repay £" & txtTotal.Text.ToString
                RecordActivity(CtrlID, ChangedValue)
            ElseIf CtrlID <> "" Then
                If CtrlID.Contains("txtHowMuch") Then
                    ChangedValue = txtHowMuch.Text
                ElseIf CtrlID.Contains("txtHowLong") Then
                    ChangedValue = txtHowLong.Text
                End If

                MaxAfford = Pmt(0.24 / 12, 1, -1 * Borrow)
                MinAfford = Pmt(0.24 / 12, 3, -1 * Borrow)
                txtAfford_SliderExtender.Minimum = MinAfford
                txtAfford_SliderExtender.Maximum = MaxAfford
                If ChangedValue > 0 Then
                    txtAfford.Text = MinAfford
                    Dim X As Double = Math.Round(Pmt(0.24 / 12, HowLong, -1 * Borrow), 2)
                    Total = Math.Round(X * (HowLong), 2)
                    Interest = Math.Round(Total - Borrow, 2)
                Else
                    Total = hfTotalToPay.Value
                    Interest = hfTotalInterest.Value
                End If


                PopulateResultsTable(Borrow, HowLong, Interest, IIf(ChangedValue = 0, txtAfford.Text, 0))  'Pass affordability as 0 as no need
                'litResultText.Text = "Borrowing: £" & Borrow.ToString & " + interest & fees £" & txtInterest.Text.ToString & " = Total to repay £" & txtTotal.Text.ToString
                RecordActivity(CtrlID, ChangedValue)
            End If

        Else
            Borrow = 100 'txtHowMuch.Text
            HowLong = 1 'txtHowLong.Text
            MinAfford = Pmt(0.24 / 12, 3, -1 * Borrow)
            MaxAfford = Pmt(0.24 / 12, 1, -1 * Borrow)
            txtAfford_SliderExtender.Minimum = MinAfford
            txtAfford_SliderExtender.Maximum = MaxAfford

            Dim X As Double = Math.Round(Pmt(0.24 / 12, HowLong, -1 * Borrow), 2)
            Total = Math.Round(X * (HowLong), 2)
            Interest = Math.Round(Total - Borrow, 2)
            PopulateResultsTable(Borrow, HowLong, Interest, 0)
        End If
    End Sub

    Private Sub PopulateResultsTable(ByVal Amount As Double, ByVal Period As Integer, ByVal Interest As Double, ByVal Afford As Double)
        Dim Month1Repay As Double = 0
        Dim Month2Repay As Double = 0
        Dim Month3Repay As Double = 0
        Dim Total As Double = 0
        Total = Amount + Interest

        lit1.Text = "Loan of £" & Amount.ToString & " over " & Period.ToString & " month(s)"
        lit2.Text = "£" & Format(Interest, "0.00")
        lit3.Text = "£" & Format(Total, "0.00")

        litSummaryTotal.Text = "£" & Format(Amount, "0.00")
        litSummaryInterest.Text = "£" & Format(Interest, "0.00")

        If Afford > 0 Then
            Select Case Period
                Case 1
                    Month1Repay = Total
                    Month2Repay = 0
                    Month3Repay = 0
                Case 2
                    Month1Repay = Afford
                    Month2Repay = Math.Round(Total - Afford, 2)
                    Month3Repay = 0
                Case 3
                    Month1Repay = Afford
                    Month2Repay = Afford
                    Month3Repay = Math.Round(Total - (Afford * 2), 2)
            End Select
        Else
            Select Case Period
                Case 1
                    Month1Repay = Total
                    Month2Repay = 0
                    Month3Repay = 0
                Case 2
                    Month1Repay = Total / 2
                    Month2Repay = Total / 2
                    Month3Repay = 0
                Case 3
                    Month1Repay = Total / 3
                    Month2Repay = Total / 3
                    Month3Repay = Total / 3
            End Select
        End If


        lit4.Text = "£" & Format(Month1Repay, "0.00")
        lit5.Text = "£" & Format(Month2Repay, "0.00")
        lit6.Text = "£" & Format(Month3Repay, "0.00")

        hfBorrow.Value = Amount
        hfPeriod.Value = Period
        hfAfford.Value = Afford
        hfTotalToPay.Value = Total
        hfTotalInterest.Value = Interest
        hfMonth1Pay.Value = Month1Repay
        hfMonth2Pay.Value = Month2Repay
        hfMonth3Pay.Value = Month3Repay

        If radInstantDecisionYes.Checked Then
            txtInstantDecision.Text = String.Format("{0:c0}", CType(Session("SessionUserObj"), PDLSessionUser).InstantDecisionFee)
        Else
            txtInstantDecision.Text = String.Format("{0:c0}", 0)
        End If

        If radSameDayPayYes.Checked Then
            txtSameDay.Text = String.Format("{0:c0}", CType(Session("SessionUserObj"), PDLSessionUser).SamedayPaymentFee)
        Else
            txtSameDay.Text = String.Format("{0:c0}", 0)
        End If

        If radRepeatAccFacilityYes.Checked Then
            txtRepeatAcc.Text = String.Format("{0:c0}", CType(Session("SessionUserObj"), PDLSessionUser).RepeatAccFacility)
        Else
            txtRepeatAcc.Text = String.Format("{0:c0}", 0)
        End If
        txtMemberFee.Text = String.Format("{0:c0}", CType(Session("SessionUserObj"), PDLSessionUser).MembershipFee)
        If txtFirstSaving.Text = "" OrElse txtFirstSaving.Text < CType(Session("SessionUserObj"), PDLSessionUser).MinimumSavingRequirement Then
            txtFirstSaving.Text = String.Format("{0:c0}", CType(Session("SessionUserObj"), PDLSessionUser).MinimumSavingRequirement)
        Else
            txtFirstSaving.Text = String.Format("{0:c0}", txtFirstSaving.Text)
        End If

        litOtherCosts.Text = "£" & Format(CDbl(txtMemberFee.Text) + CDbl(txtInstantDecision.Text) + CDbl(txtSameDay.Text) + CDbl(txtRepeatAcc.Text), "0.00")
        litTotalCost.Text = "£" & Format(Interest + CDbl(txtMemberFee.Text) + CDbl(txtInstantDecision.Text) + CDbl(txtSameDay.Text) + CDbl(txtRepeatAcc.Text), "0.00")
        litOtherLenderCost.Text = "£" & Format(Amount / 100 * 30, "0.00")
        litSave.Text = "£" & Format(CDbl(litOtherLenderCost.Text) - CDbl(litTotalCost.Text), "0.00")
    End Sub

    Public Sub RecordActivity(ByVal ChangedControl As String, ByVal ChangedValue As Double)
        Try
            Dim ActivityID As Integer = 0
            Dim ChangedSlideIndex As Int16
            If ChangedControl.Contains("txtHowMuch") Then
                ChangedSlideIndex = 1
            ElseIf ChangedControl.Contains("txtHowLong") Then
                ChangedSlideIndex = 2
            ElseIf ChangedControl.Contains("txtAfford") Then
                ChangedSlideIndex = 3
            End If
            If ChangedSlideIndex > 0 Then
                ActivityID = PDLAccess.InsertCalcActivity(Request.ServerVariables("REMOTE_ADDR"), ChangedSlideIndex, ChangedValue, CType(Session("SessionUserObj"), PDLSessionUser).SSID, CType(Session("SessionUserObj"), PDLSessionUser).CUID, 0)
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
            'Throw ex    'To Do proper error handling
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            If CType(Session("SessionUserObj"), PDLSessionUser) IsNot Nothing Then

                PDLAccess.UpdateCreatePreConfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, CType(Session("SessionUserObj"), PDLSessionUser).CUID, hfBorrow.Value, hfPeriod.Value, hfAfford.Value, hfTotalInterest.Value, hfTotalToPay.Value, hfMonth1Pay.Value, hfMonth2Pay.Value, hfMonth3Pay.Value, hfMonth4Pay.Value, hfMonth5Pay.Value, hfMonth6Pay.Value, hfMonth7Pay.Value, hfMonth8Pay.Value, hfMonth9Pay.Value, hfMonth10Pay.Value, hfMonth11Pay.Value, hfMonth12Pay.Value, 0, txtFirstSaving.Text, txtMemberFee.Text, txtInstantDecision.Text, txtSameDay.Text, txtRepeatAcc.Text, Request.ServerVariables("REMOTE_ADDR"))

                'Populate security captcha image
                imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
                txtEnteredWord.Text = ""
                mdlPopup.Show()
                'Response.Redirect("PDLSecurity.aspx")
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnOK2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnApplyWithSecurity.Click
        If txtEnteredWord.Text = Session("CaptchaText") Then
            mdlPopup.Hide()
            Response.Redirect("PDLApplyPDL1.aspx")
        Else
            imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
            txtEnteredWord.Text = ""
            mdlPopup.Show()
        End If
    End Sub

    Protected Sub lbtnTryAnother_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnTryAnother.Click
        imgCaptcha.ImageUrl = "CaptchaImage.aspx" & "?" & New Random().Next()
        txtEnteredWord.Text = ""
        mdlPopup.Show()
    End Sub


End Class
