﻿<%@ Page Language="VB" MasterPageFile="~/PDL/PDLMasterPage2.master" AutoEventWireup="false" CodeFile="PDLApplyPDL1.aspx.vb" Inherits="PDL_PDLApplyPDL1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
                        <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">                            
                            <tr __designer:mapid="d9a">
                                <td __designer:mapid="d9b">
                                    <asp:Panel ID="pnlQ3Answer" runat="server">
                                        <table style="width:100%;">
                                            <tr>
                                                <td align="left">
                                                    <h2>Terms & Conditions Agreement</h2></td>                                                
                                            </tr>
                                            <tr>
                                                <td style="height: 18px">
                                                    <asp:HyperLink ID="hlTC" runat="server" 
                                                        NavigateUrl="~/PDL/Docs/TC_LCCU.pdf" Target="new">Please click here 
                                                    to view terms and conditions.</asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 50px">
                                                    <table style="width:100%;" class="BodyText">
                                                        <tr>
                                                            <td width="10%" align="left">
                                                                <asp:CheckBox ID="chkAgree1" runat="server" 
                                                                    Text="" 
                                                                    ValidationGroup="vgTerms" />
                                                            </td>
                                                            <td>
                                                               <strong>I have read and agreed to the terms and conditions.</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkAgree2" runat="server" Text="" ValidationGroup="vgTerms" />
                                                            </td>
                                                            <td>
                                                                <strong>I confirm that I live or work in the Glasgow.</strong></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="data">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td width="30%" align="left">
                                                                <asp:Button ID="btnNext" runat="server" BackColor="#00a79d" Font-Bold="True" 
                                                                    Text="Agree" ValidationGroup="vgTerms" Width="70px" />
                                                            </td>
                                                            <td width="40%">
                                                                <asp:Button ID="btnDisagree" runat="server" BackColor="#00a79d" 
                                                                    Font-Bold="True" Text="Disagree" ValidationGroup="vgTerms" Width="70px" />
                                                            </td>
                                                            <td width="30%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>                            
                        </table>
                    </asp:Content>

