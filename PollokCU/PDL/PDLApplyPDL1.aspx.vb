﻿
Partial Class PDL_PDLApplyPDL1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
        End If
    End Sub


    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click

        If chkAgree1.Checked AndAlso chkAgree2.Checked Then
            Response.Redirect("PDLApplyPDL2.aspx")
        Else
            lblMessege.Visible = True
            lblMessege.Text = "Please check these boxes to agree for terms and conditions to proceed"
        End If

    End Sub

    Protected Sub btnDisagree_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisagree.Click
        Response.Redirect("PDLError.aspx?C=E3")
    End Sub
End Class
