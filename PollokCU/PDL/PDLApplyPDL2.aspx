﻿<%@ Page Language="VB" MasterPageFile="~/PDL/PDLMasterPage2.master" AutoEventWireup="false" CodeFile="PDLApplyPDL2.aspx.vb" Inherits="PDL_PDLApplyPDL2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">

    <script type="text/javascript" language="javascript">
function noCopyMouse(e) {
        var isRight = (e.button) ? (e.button == 2) : (e.which == 3);
            if(isRight) {
                alert('You are not allowed to copy this, please type again!');
                return false;        
             }        
             return true;    
}    

function noCopyKey(e) {
        var forbiddenKeys = new Array('c','x','v');        
        var keyCode = (e.keyCode) ? e.keyCode : e.which;        
        var isCtrl;        
        if(window.event)            
            isCtrl = e.ctrlKey  
        else            
            isCtrl = (window.Event) ? ((e.modifiers & Event.CTRL_MASK) == Event.CTRL_MASK) : false;            
        if(isCtrl) {            
        for(i = 0; i < forbiddenKeys.length; i++) {                
        if(forbiddenKeys[i] == String.fromCharCode(keyCode).toLowerCase()) {                    
            alert('You are not allowed to copy and paste, please type again!');                    
            return false;                
            }            
            }        
            }        
            return true;    
}
</script>
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="26" valign="middle" class="BodyText">
              <p class="extra">This loan application form must be fully completed. Failure to disclose all required information may delay approval of your loan application.</p>
              <br /><p><strong>Please note: <span class="style82">ALL</span> fields are required except where stated, thank you.</strong></p></td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText">&nbsp;</td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText"><table width="466" border="0" cellspacing="2" cellpadding="0">
              <tr valign="middle">
                <td width="25%" height="42" bgcolor="#e20076"><div align="center" class="style37">Personal <br>
      details</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Your <br>
      employment</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Loan <br>
      details</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Declaration</div></td>
              </tr>
              <tr valign="middle">
                <td height="10" colspan="5">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
          <tr>
          &nbsp;</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText">
            <div id="loanform">
            <table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                              <td>
                      <table style="width:100%;">
                          <tr>
                              <td width="40%">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Title:<asp:RequiredFieldValidator ID="RequiredFieldValidator30" 
                                      runat="server" ControlToValidate="ddlTitle" 
                                      ErrorMessage="Title required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlTitle" runat="server" Width="208px" 
                                      DataSourceID="odsTitle" DataTextField="Title" DataValueField="SalutationID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;First name:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                      runat="server" ControlToValidate="txtFirstName" 
                                      ErrorMessage="First name required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Surname:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                      runat="server" ControlToValidate="txtSurName" ErrorMessage="Surname required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtSurName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Middle name:</td>
                              <td>
                                  <asp:TextBox ID="txtMiddleName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Gender:</td>
                              <td>
                                  <asp:DropDownList ID="ddlGender" runat="server" Width="208px">
                                      <asp:ListItem Value="M">Male</asp:ListItem>
                                      <asp:ListItem Value="F">Female</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Date of birth:<asp:CompareValidator ID="CompareValidator3" runat="server" 
                                      ControlToValidate="ddlDay" ErrorMessage="Date of birth - Day required" 
                                      Operator="GreaterThan" ValueToCompare="0">*</asp:CompareValidator>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                      ControlToValidate="ddlMonth" ErrorMessage="Date of birth - Month required" 
                                      Operator="GreaterThan" ValueToCompare="0">*</asp:CompareValidator>
                                  <asp:RangeValidator ID="RangeValidator3" runat="server" 
                                      ControlToValidate="ddlYear" ErrorMessage="Date of birth - Year required" 
                                      MaximumValue="3000" MinimumValue="1800">*</asp:RangeValidator>
                              </td>
                              <td>
                                  <table style="width:208px;" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                          <td>
                                              <asp:DropDownList ID="ddlDay" runat="server" Width="50px">
                                                  <asp:ListItem Value="0">DAY</asp:ListItem>
                                                  <asp:ListItem>1</asp:ListItem>
                                                  <asp:ListItem>2</asp:ListItem>
                                                  <asp:ListItem>3</asp:ListItem>
                                                  <asp:ListItem>4</asp:ListItem>
                                                  <asp:ListItem>5</asp:ListItem>
                                                  <asp:ListItem>6</asp:ListItem>
                                                  <asp:ListItem>7</asp:ListItem>
                                                  <asp:ListItem>8</asp:ListItem>
                                                  <asp:ListItem>9</asp:ListItem>
                                                  <asp:ListItem>10</asp:ListItem>
                                                  <asp:ListItem>11</asp:ListItem>
                                                  <asp:ListItem>12</asp:ListItem>
                                                  <asp:ListItem>13</asp:ListItem>
                                                  <asp:ListItem>14</asp:ListItem>
                                                  <asp:ListItem>15</asp:ListItem>
                                                  <asp:ListItem>16</asp:ListItem>
                                                  <asp:ListItem>17</asp:ListItem>
                                                  <asp:ListItem>18</asp:ListItem>
                                                  <asp:ListItem>19</asp:ListItem>
                                                  <asp:ListItem>20</asp:ListItem>
                                                  <asp:ListItem>21</asp:ListItem>
                                                  <asp:ListItem>22</asp:ListItem>
                                                  <asp:ListItem>23</asp:ListItem>
                                                  <asp:ListItem>24</asp:ListItem>
                                                  <asp:ListItem>25</asp:ListItem>
                                                  <asp:ListItem>26</asp:ListItem>
                                                  <asp:ListItem>27</asp:ListItem>
                                                  <asp:ListItem>28</asp:ListItem>
                                                  <asp:ListItem>29</asp:ListItem>
                                                  <asp:ListItem>30</asp:ListItem>
                                                  <asp:ListItem>31</asp:ListItem>
                                              </asp:DropDownList>
                                          </td>
                                          <td>
                                              <asp:DropDownList ID="ddlMonth" runat="server" Width="90px">
                                                  <asp:ListItem Value="0">MONTH</asp:ListItem>
                                                  <asp:ListItem Value="1">January</asp:ListItem>
                                                  <asp:ListItem Value="2">February</asp:ListItem>
                                                  <asp:ListItem Value="3">March</asp:ListItem>
                                                  <asp:ListItem Value="4">April</asp:ListItem>
                                                  <asp:ListItem Value="5">May</asp:ListItem>
                                                  <asp:ListItem Value="6">June</asp:ListItem>
                                                  <asp:ListItem Value="7">July</asp:ListItem>
                                                  <asp:ListItem Value="8">August</asp:ListItem>
                                                  <asp:ListItem Value="9">September</asp:ListItem>
                                                  <asp:ListItem Value="10">October</asp:ListItem>
                                                  <asp:ListItem Value="11">November</asp:ListItem>
                                                  <asp:ListItem Value="12">December</asp:ListItem>
                                              </asp:DropDownList>
                                          </td>
                                          <td align="right">
                                              <asp:DropDownList ID="ddlYear" runat="server" Width="55px">
                                                  <asp:ListItem Value="0">YEAR</asp:ListItem>                                                  
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                  </table>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Place of birth:<asp:RequiredFieldValidator ID="RequiredFieldValidator23" 
                                      runat="server" ControlToValidate="txtPlaceofBirth" 
                                      ErrorMessage="Place of birth required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtPlaceofBirth" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Mother&#39;s maiden name:<asp:RequiredFieldValidator ID="RequiredFieldValidator24" 
                                      runat="server" ControlToValidate="txtMotherName" 
                                      ErrorMessage="Mother's maiden name required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMotherName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                          <asp:UpdatePanel ID="updatePanelAddress1" 
    runat="server">
                                              <ContentTemplate>
                                                  <asp:ValidationSummary ID="ValidationSummary2" runat="server" 
                                                      ValidationGroup="vgSearch" />
                                                  <asp:Panel ID="pnlAddress1" runat="server">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td height="40" width="40%">
                                                                  &nbsp;<h5>&nbsp;Current address<asp:Label ID="lblAddressMessage" 
                                                                      runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                  </h5></td>
                                                              <td>
                                                                  <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                            AssociatedUpdatePanelID="updatePanelAddress1">
                                                                      <ProgressTemplate>
                                                                          <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading.gif" 
                                                                    Width="30px" />
                                                                          &nbsp;Please wait...
                                                                      </ProgressTemplate>
                                                                  </asp:UpdateProgress>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                            runat="server" ControlToValidate="txtPostCode1" ErrorMessage="Invalid postcode" 
                                                            
                                                            
                                                                      ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$">*</asp:RegularExpressionValidator>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Postcode required">*</asp:RequiredFieldValidator>
                                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Invalid postcode" 
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                            ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Postcode required" 
                                                            ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:TextBox ID="txtPostCode1" runat="server" Width="200px" AutoPostBack="True"></asp:TextBox>
                                                                  &nbsp;<asp:Button ID="btnSearch1" runat="server" Text="Search" 
                                                            ValidationGroup="vgSearch" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;Address line 1:<asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                                                            runat="server" ControlToValidate="txtAddLine1_1" 
                                                            ErrorMessage="Address line 1 required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:TextBox ID="txtAddLine1_1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;Address line 2:</td>
                                                              <td>
                                                                  <asp:TextBox ID="txtAddLine2_1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;City:<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="txtCity1" ErrorMessage="City required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:TextBox ID="txtCity1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;County:<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                            ControlToValidate="txtCounty1" ErrorMessage="County required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:TextBox ID="txtCounty1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </asp:Panel>
                                                  <asp:Panel ID="pnlAddressSearch" runat="server" Visible="False">
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td height="40" width="40%">
                                                                  &nbsp;<h5>&nbsp;Current address</h5></td>
                                                              <td>
                                                                  &nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator4" 
                                                            runat="server" ControlToValidate="txtPostCodeList" ErrorMessage="Invalid postcode" 
                                                            
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                            ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                                            ControlToValidate="txtPostCodeList" ErrorMessage="Postcode required" 
                                                            ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" 
                                                            ControlToValidate="txtPostCodeList" ErrorMessage="Postcode required">*</asp:RequiredFieldValidator>
                                                            
                                                              </td>
                                                              <td>
                                                                  <asp:TextBox ID="txtPostCodeList" runat="server" AutoPostBack="True" 
                                                            Width="200px"></asp:TextBox>
                                                                  &nbsp;<asp:Button ID="btnSearchList" runat="server" Text="Search" 
                                                            ValidationGroup="vgSearch" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  &nbsp;Address:<asp:RequiredFieldValidator ID="RequiredFieldValidator12" 
                                                            runat="server" ControlToValidate="txtAddLine1_1" 
                                                            ErrorMessage="Address required">*</asp:RequiredFieldValidator>
                                                                   <asp:RequiredFieldValidator ID="CurrentAddressValidator" 
                                                                         runat="Server" InitialValue=""
                                                                            ControlToValidate ="ddlAddressList" ErrorMessage="Select the current address">*
                                                                    </asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:DropDownList ID="ddlAddressList" runat="server" Width="300px" 
                                                                      AutoPostBack="True">
                                                                  </asp:DropDownList>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </asp:Panel>
                                              </ContentTemplate>
                                          </asp:UpdatePanel>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                      <ContentTemplate>
                                          <table style="width:100%;">
                                              <tr>
                                                  <td width="70%">
                                                      <table border="0" cellpadding="0" cellspacing="0" style="width: 85%;">
                                                          <tr>
                                                              <td width="80%">
                                                                  &nbsp;Time at the present address?
                                                                  <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                                                      ControlToValidate="ddlTimeMonths" 
                                                                      ErrorMessage="No of months at address required" Operator="GreaterThan" 
                                                                      ValueToCompare="-1">*</asp:CompareValidator>
                                                                  <asp:RequiredFieldValidator ID="NoOfYears" 
                                                                         runat="Server" InitialValue=""
                                                                            ControlToValidate ="ddlTimeYears" ErrorMessage="No of years at address required">*
                                                                    </asp:RequiredFieldValidator>
                                                              </td>
                                                              <td align="right">
                                                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 74%;">
                                                                      <tr>
                                                                          <td>
                                                                              <asp:DropDownList ID="ddlTimeYears" runat="server" AutoPostBack="True" 
                                                                                  Width="65px">
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                          <td align="right">
                                                                              <asp:DropDownList ID="ddlTimeMonths" runat="server" Width="90px">
                                                                                  <asp:ListItem Value="-1">MONTHS</asp:ListItem>
                                                                                  <asp:ListItem Value="0">0</asp:ListItem>
                                                                                  <asp:ListItem Value="1">1</asp:ListItem>
                                                                                  <asp:ListItem Value="2">2</asp:ListItem>
                                                                                  <asp:ListItem Value="3">3</asp:ListItem>
                                                                                  <asp:ListItem Value="4">4</asp:ListItem>
                                                                                  <asp:ListItem Value="5">5</asp:ListItem>
                                                                                  <asp:ListItem Value="6">6</asp:ListItem>
                                                                                  <asp:ListItem Value="7">7</asp:ListItem>
                                                                                  <asp:ListItem Value="8">8</asp:ListItem>
                                                                                  <asp:ListItem Value="9">9</asp:ListItem>
                                                                                  <asp:ListItem Value="10">10</asp:ListItem>
                                                                                  <asp:ListItem Value="11">11</asp:ListItem>                                                                                  
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td width="70%">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr>
                                                  <td width="70%">
                                                      <asp:UpdatePanel ID="updatePanelAddress2" runat="server" Visible="False">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" 
                                            ValidationGroup="vgSearch" />
                                        &nbsp;<asp:Panel ID="pnlAddress2" runat="server">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td height="40" width="40%">
                                                        &nbsp;<strong ID="Strong1">Previous address<asp:Label ID="lblAddressMessage2" 
                                                            runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                        </strong></td>
                                                    <td>
                                                        <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                                                            AssociatedUpdatePanelID="updatePanelAddress1">
                                                            <ProgressTemplate>
                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading.gif" 
                                                                    Width="30px" />
                                                                &nbsp;Please wait...
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator5" 
                                                            runat="server" ControlToValidate="txtPostCode2" ErrorMessage="Invalid previous postcode" 
                                                            
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$">*</asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                            ControlToValidate="txtPostCode2" ErrorMessage="Previous Postcode required">*</asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                                            ControlToValidate="txtPostCode2" ErrorMessage="Invalid previous postcode" 
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                            ValidationGroup="vgSearch2">*</asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                                            ControlToValidate="txtPostCode2" ErrorMessage="Previous Postcode required" 
                                                            ValidationGroup="vgSearch2">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPostCode2" runat="server" Width="200px" AutoPostBack="True"></asp:TextBox>
                                                        &nbsp;<asp:Button ID="btnSearch2" runat="server" Text="Search" 
                                                            ValidationGroup="vgSearch2" BackColor="#00a79d" Font-Bold="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;Address line 1:<asp:RequiredFieldValidator ID="RequiredFieldValidator15" 
                                                            runat="server" ControlToValidate="txtAddLine1_2" 
                                                            ErrorMessage="Previous Address line 1 required">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddLine1_2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;Address line 2:</td>
                                                    <td>
                                                        <asp:TextBox ID="txtAddLine2_2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;City:<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                                                            ControlToValidate="txtCity2" ErrorMessage="Previous City required">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCity2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;County:<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                                            ControlToValidate="txtCounty2" ErrorMessage="County required">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCounty2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlAddressSearch2" runat="server" Visible="False">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td height="40" width="40%">
                                                        &nbsp;<h5>Previous address</h5></td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator7" 
                                                            runat="server" ControlToValidate="txtPostCodeList" ErrorMessage="Invalid postcode" 
                                                            
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                            ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" 
                                                            ControlToValidate="txtPostCodeList" ErrorMessage="Postcode required" 
                                                            ValidationGroup="vgSearch2">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPostCodeList2" runat="server" AutoPostBack="True" 
                                                            Width="200px"></asp:TextBox>
                                                        &nbsp;<asp:Button ID="btnSearchList2" runat="server" Text="Search" 
                                                            ValidationGroup="vgSearch2" BackColor="#00a79d" Font-Bold="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;Address:<asp:RequiredFieldValidator ID="RequiredFieldValidator19" 
                                                            runat="server" ControlToValidate="ddlAddressList2" 
                                                            ErrorMessage="Address required" ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                        <asp:RequiredFieldValidator ID="PreviousAddressValidator" 
                                                                         runat="Server" InitialValue=""
                                                                            ControlToValidate ="ddlAddressList2" ErrorMessage="Select the previous address">*
                                                                    </asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlAddressList2" runat="server" Width="300px" 
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                                  </td>
                                              </tr>
                                          </table>
                                      </ContentTemplate>
                                  </asp:UpdatePanel>
                              </td>
                          </tr>
                          <tr>
                              <td>
                      <table style="width:100%;">
                          <tr>
                              <td width="40%">
                                  &nbsp;<h5>&nbsp;Additional &amp; Contact Details</h5></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td width="40%">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td width="40%">
                                  &nbsp;Home telephone:<asp:RegularExpressionValidator ID="RequiredFieldValidator200" runat="server" ControlToValidate="txtHomeTel" 
                                      ErrorMessage="Invalid home telephone" 
                                      ValidationExpression="^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$">*</asp:RegularExpressionValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtHomeTel" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Mobile:<asp:RegularExpressionValidator ID="RegularExpressionValidator9" 
                                      runat="server" ControlToValidate="txtMobile" ErrorMessage="Invalid mobile" 
                                      ValidationExpression="^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$">*</asp:RegularExpressionValidator>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator201" 
                                      runat="server" ControlToValidate="txtMobile" 
                                      ErrorMessage="Mobile number required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td align="left">
                                  <table style="width:85%;" cellpadding="0" cellspacing="0">
                                      <tr>
                                          <td><asp:TextBox ID="txtMobile" runat="server" Width="200px"></asp:TextBox></td>
                                          <td><span class="hotspot" onmouseover="tooltip.show('Please ensure that your mobile number is correct as we will send you a security code via sms to your mobile.');" onmouseout="tooltip.hide();"><img 
                                            alt="" src="Images/GreenInfoButton.png" height="25px" width="25px" /></span>
                                              </td>
                                      </tr>
                                  </table></td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Email:<asp:RequiredFieldValidator ID="RequiredFieldValidator9" 
                                      runat="server" ControlToValidate="txtEmail" ErrorMessage="Email required">*</asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                      ControlToValidate="txtEmail" ErrorMessage="Invalid email" 
                                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                              </td>
                              <td align="left">
                                  <table style="width:85%;">
                                      <tr>
                                          <td>
                                  <asp:TextBox ID="txtEmail" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                                          <td><span class="hotspot" onmouseover="tooltip.show('Please ensure that your email address is correct as we will send you your loan agreement and terms & conditions by email.');" onmouseout="tooltip.hide();"><img 
                                            alt="" src="Images/GreenInfoButton.png" height="25px" width="25px" /></span></td>
                                      </tr>
                                  </table>
                                                                                            </td>
                          </tr>
                          <tr><td>
                                  &nbsp;Confirm Email:<asp:RequiredFieldValidator ID="RequiredFieldValidator22" 
                                      runat="server" ControlToValidate="txtEmailConfirm" 
                                  ErrorMessage="Email confirmation required">*</asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" 
                                      ControlToValidate="txtEmailConfirm" ErrorMessage="Invalid email confirmation" 
                                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtEmailConfirm" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr><td>
        &nbsp;National Insurance:<asp:RequiredFieldValidator ID="RequiredFieldValidator10" 
                                                          runat="server" ControlToValidate="txtNI" 
                                                          ErrorMessage="National insurance number required">*</asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" 
                                  runat="server" ControlToValidate="txtNI" 
                                  ErrorMessage="Invalid NI number format" 
                                  ValidationExpression="[a-zA-Z]{1}[a-zA-Z]{1}[0-9]{6}[a-zA-Z]{0,1}">*</asp:RegularExpressionValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtNI" runat="server" Width="200px" MaxLength="9"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  
                                  &nbsp;Marital status:<asp:RequiredFieldValidator ID="RequiredFieldValidator20" 
                                      runat="server" ControlToValidate="ddlMaritalStatus" 
                                      ErrorMessage="Marital status required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlMaritalStatus" runat="server" Width="208px" 
                                      DataSourceID="odsMaritalStatus" DataTextField="LookupCodeName" 
                                      DataValueField="LookupCodeID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;No of dependents:</td>
                              <td>
                                  <asp:DropDownList ID="ddlDependents" runat="server" Width="208px">
                                      <asp:ListItem>0</asp:ListItem>
                                      <asp:ListItem>1</asp:ListItem>
                                      <asp:ListItem>2</asp:ListItem>
                                      <asp:ListItem>3</asp:ListItem>
                                      <asp:ListItem>4</asp:ListItem>
                                      <asp:ListItem>5</asp:ListItem>
                                      <asp:ListItem>6</asp:ListItem>
                                      <asp:ListItem>7</asp:ListItem>
                                      <asp:ListItem>8</asp:ListItem>
                                      <asp:ListItem>9</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Residency:<asp:RequiredFieldValidator ID="RequiredFieldValidator21" 
                                      runat="server" ControlToValidate="ddlResidency" 
                                      ErrorMessage="Residency required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlResidency" runat="server" Width="208px" 
                                      DataSourceID="odsResidency" DataTextField="LookupCodeName" 
                                      DataValueField="LookupCodeID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  &nbsp;</td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Next" Width="70px" 
                                                                      BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:ObjectDataSource ID="odsTitle" runat="server" 
                                      SelectMethod="SelectSalutations" TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsMaritalStatus" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="MARITAL_STATUS" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsResidency" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="RESIDENCY_TYPE" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
             </div>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>
