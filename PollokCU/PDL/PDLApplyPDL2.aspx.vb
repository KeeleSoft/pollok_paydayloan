﻿Imports System.Data

Partial Class PDL_PDLApplyPDL2
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
        End If

        txtEmail.Attributes.Add("onmousedown", "return noCopyMouse(event);")
        txtEmailConfirm.Attributes.Add("onmousedown", "return noCopyMouse(event);")
        txtEmail.Attributes.Add("onkeydown", "return noCopyKey(event);")
        txtEmailConfirm.Attributes.Add("onkeydown", "return noCopyKey(event);")
        If Not IsPostBack Then
            PopulateYears()
            'PopulateQuestions()
            'PopulateMonths()
        End If
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            'Fields available in this form
            Dim CompletedLevel As Int16 = 1 'Current page identifier
            Dim SalutationID As Int32 = ddlTitle.SelectedValue
            Dim FirstName As String = txtFirstName.Text
            Dim SurName As String = txtSurName.Text
            Dim MiddleName As String = txtMiddleName.Text
            Dim Gender As String = ddlGender.SelectedValue
            Dim DateOfBirth As DateTime = ddlDay.SelectedValue & "/" & ddlMonth.SelectedValue & "/" & ddlYear.SelectedValue
            Dim PlaceOfBirth As String = txtPlaceofBirth.Text
            Dim MotherName As String = txtMotherName.Text
            Dim CurrentAddressLine1 As String = txtAddLine1_1.Text
            Dim CurrentAddressLine2 As String = txtAddLine2_1.Text
            Dim CurrentCity As String = txtCity1.Text
            Dim CurrentCounty As String = txtCounty1.Text
            Dim CurrentPostCode As String = txtPostCode1.Text
            Dim CurrentAddressNoOfYears As Int32 = IIf(ddlTimeYears.SelectedIndex = 0, 0, ddlTimeYears.SelectedValue)
            Dim CurrentAddressNoOfMonths As Int32 = IIf(ddlTimeMonths.SelectedIndex = 0, 0, ddlTimeMonths.SelectedValue)
            Dim HomeTelephone As String = txtHomeTel.Text
            Dim Mobile As String = txtMobile.Text
            Dim Email As String = txtEmail.Text
            Dim NINumber As String = txtNI.Text
            Dim MaritalStatus As Integer = ddlMaritalStatus.SelectedValue
            Dim NoOfDependants As Int32 = ddlDependents.SelectedValue
            Dim PreviousAddressLine1 As String = txtAddLine1_2.Text
            Dim PreviousAddressLine2 As String = txtAddLine2_2.Text
            Dim PreviousCity As String = txtCity2.Text
            Dim PreviousCounty As String = txtCounty2.Text
            Dim PreviousPostCode As String = txtPostCode2.Text
            Dim ResidencyStatus As Integer = IIf(ddlResidency.SelectedValue = "", 0, ddlResidency.SelectedValue)
            Dim MemberID As String = 0 'IIf(Session("MemberID") > 0, Session("MemberID"), 0)
            Dim PartnerCode As String = ""

            Dim Q1Answer As Int16 = 0
            Dim Q2Answer As Int16 = 0
            Dim Q3Answer As Int16 = 0
            Dim Q4Answer As Int16 = 0
            Dim Q5Answer As Int16 = 0

            If txtEmail.Text <> txtEmailConfirm.Text Then
                lblMessege.Visible = True
                lblMessege.Text = "Email address is not matching with the confirmed email address. Please verify"
                Return
            End If

            'Try to find the member
            Dim accountStatus As String = String.Empty
            If MemberID = 0 Then
                MemberID = objPayDayLoan.ValidateMember(FirstName, SurName, DateOfBirth, CurrentPostCode, CurrentAddressLine1, CurrentCity, CurrentCounty, NINumber, accountStatus)
                If MemberID > 0 AndAlso Session("SessionUserObj") IsNot Nothing Then
                    'If Not AutoLoanFacade.Member.AllowAccountStatusForLoan(accountStatus) Then
                    '    Response.Redirect("PDLMessage.aspx?M=M4")
                    'End If
                    Session("MemberID") = MemberID
                    CType(Session("SessionUserObj"), PDLSessionUser).MemberID = MemberID
                    CType(Session("SessionUserObj"), PDLSessionUser).UpdateAllPreLoggedActivitiesWithMemberID()
                    CType(Session("SessionUserObj"), PDLSessionUser).AccountStatus = accountStatus
                End If
            End If
            If Session("SessionUserObj") IsNot Nothing Then
                CType(Session("SessionUserObj"), PDLSessionUser).MemberID = MemberID
            End If
            'If an exisitng member, check whether can give a repeat PDL
            Dim msg As String = String.Empty
            If MemberID > 0 AndAlso PollokCU.AutoLoanFacade.Member.AllowAccountStatusForLoan(accountStatus) AndAlso AllowProceedWithRepeatPDL(MemberID, msg) Then
                Dim dtApp As DataTable = objAutoLoan.GetPreviousPayDayLoanDetails(CInt(MemberID))
                If dtApp.Rows.Count > 0 Then
                    Dim amount As Double = CDbl(dtApp.Rows(0).Item("CurrentBalance"))
                    If amount = 0 Then
                        Session("MemberID") = MemberID
                        If Session("Borrow") IsNot Nothing AndAlso Session("Period") IsNot Nothing Then
                            Dim Borrow As Double = CDbl(Session("Borrow"))
                            Dim HowLong As Int16 = Convert.ToInt16(Session("Period"))
                            Response.Redirect("../AutoLoanMemberNewApplication.aspx?A=True&Amount=" & Borrow.ToString & "&Term=" & HowLong.ToString)
                        End If
                    End If
                End If
            End If

            Dim dt As DataTable = objPayDayLoan.ValidatePostCode(CurrentPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID)
            If dt.Rows.Count > 0 Then
                If CInt(dt.Rows(0).Item("ValidPostCode")) = 1 Then
                    PartnerCode = dt.Rows(0).Item("PartnerCode").ToString
                End If
            End If

            'Do the postcode validation only if not a member
            'If CType(Session("SessionUserObj"), PDLSessionUser).MemberID = 0 AndAlso Not objPayDayLoan.ValidatePostCode(CurrentPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID) Then
            '    Response.Redirect("PDLError.aspx?C=E4")
            '    Return
            'End If

            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , Q1Answer _
                                    , Q2Answer _
                                    , Q3Answer _
                                    , Q4Answer _
                                    , Q5Answer _
                                    , iUserID _
                                    , iLoanID _
                                    , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ,
, , , , , , , , , , , ,
, Request.ServerVariables("REMOTE_ADDR") _
                                    ,
, PartnerCode
                                    )
            If iLoanID > 0 Then
                Response.Redirect("PDLApplyPDL3.aspx?LoanID=" & iLoanID.ToString)
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        PopulateDOBYear()
        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If Not IsPostBack Then
                PopulateControls(Request.QueryString("LoanID"))
            End If

        Else
            hfLoanID.Value = 0
        End If

        Dim blnEnableAddressSearch As Boolean = CType(objSystemData.GetSystemKeyValue("EnableAddressSearchPDL"), Boolean)
        btnSearch1.Visible = blnEnableAddressSearch
        btnSearch2.Visible = blnEnableAddressSearch
    End Sub

    Private Sub PopulateYears()
        ddlTimeYears.Items.Clear()

        Dim li As ListItem = New ListItem
        li.Text = "YEARS"
        li.Value = ""

        ddlTimeYears.Items.Add(li)

        For i As Integer = 0 To 80
            li = New ListItem
            li.Text = i
            li.Value = i
            ddlTimeYears.Items.Add(li)
            li = Nothing
        Next
    End Sub

    Private Sub PopulateDOBYear()
        Dim iCurrentYear As Integer = DateAndTime.Year(Now())

        For i As Integer = iCurrentYear - 18 To iCurrentYear - 90 Step -1
            ddlYear.Items.Add(i)
        Next

    End Sub

    Private Sub PopulateControls(ByVal iLoanID As Integer)
        Try
            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If dtApp.Rows.Count > 0 Then
                'txtMemberNumber.Text = Session("MemberID")
                ddlTitle.SelectedValue = dtApp.Rows(0).Item("SalutationID")
                txtFirstName.Text = dtApp.Rows(0).Item("FirstName")
                txtMiddleName.Text = dtApp.Rows(0).Item("MiddleName")
                If dtApp.Rows(0).Item("Gender") IsNot System.DBNull.Value Then ddlGender.SelectedValue = dtApp.Rows(0).Item("Gender")
                txtSurName.Text = dtApp.Rows(0).Item("SurName")
                txtPlaceofBirth.Text = dtApp.Rows(0).Item("PlaceOfBirth")
                txtMotherName.Text = dtApp.Rows(0).Item("MotherName")
                ddlDay.SelectedValue = DatePart(DateInterval.Day, dtApp.Rows(0).Item("DateOfBirth"))
                ddlMonth.SelectedValue = DatePart(DateInterval.Month, dtApp.Rows(0).Item("DateOfBirth"))
                ddlYear.SelectedValue = DatePart(DateInterval.Year, dtApp.Rows(0).Item("DateOfBirth"))
                txtPostCode1.Text = dtApp.Rows(0).Item("CurrentPostCode")
                txtAddLine1_1.Text = dtApp.Rows(0).Item("CurrentAddressLine1")
                txtAddLine2_1.Text = dtApp.Rows(0).Item("CurrentAddressLine2")
                txtCity1.Text = dtApp.Rows(0).Item("CurrentCity")
                txtCounty1.Text = dtApp.Rows(0).Item("CurrentCounty")

                ddlTimeYears.SelectedValue = dtApp.Rows(0).Item("CurrentAddressNoOfYears")
                ddlTimeMonths.SelectedValue = IIf(dtApp.Rows(0).Item("CurrentAddressNoOfMonths") Is System.DBNull.Value, "0", dtApp.Rows(0).Item("CurrentAddressNoOfMonths"))

                If ddlTimeYears.SelectedValue <= 2 Then
                    updatePanelAddress1.Visible = True
                End If

                txtPostCode2.Text = dtApp.Rows(0).Item("PreviousPostCode")
                txtAddLine1_2.Text = dtApp.Rows(0).Item("PreviousAddressLine1")
                txtAddLine2_2.Text = dtApp.Rows(0).Item("PreviousAddressLine2")
                txtCity2.Text = dtApp.Rows(0).Item("PreviousCity")
                txtCounty2.Text = dtApp.Rows(0).Item("PreviousCounty")

                txtHomeTel.Text = dtApp.Rows(0).Item("HomeTelephone")
                txtMobile.Text = dtApp.Rows(0).Item("Mobile")
                txtEmail.Text = dtApp.Rows(0).Item("Email")
                txtNI.Text = dtApp.Rows(0).Item("NINumber")
                ddlMaritalStatus.SelectedValue = dtApp.Rows(0).Item("MaritalStatus")
                ddlDependents.Text = dtApp.Rows(0).Item("NoOfDependants")
                ddlResidency.Text = IIf(dtApp.Rows(0).Item("ResidencyStatus") > 0, dtApp.Rows(0).Item("ResidencyStatus"), "")
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Protected Sub Time_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTimeYears.SelectedIndexChanged, ddlTimeMonths.SelectedIndexChanged
        If ddlTimeYears.SelectedIndex = 0 OrElse ddlTimeYears.SelectedValue < 3 Then
            updatePanelAddress2.Visible = True
            txtPostCode2.Focus()
        Else
            updatePanelAddress2.Visible = False
        End If
    End Sub

    Private Function AllowProceedWithRepeatPDL(ByVal memberId As Integer, ByRef message As String) As Boolean
        Try
            Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
            Dim allow As Boolean = False
            Dim model As PollokCU.AutoLoanFacade.AutoLoanModel = New PollokCU.AutoLoanFacade.AutoLoanModel With {.MemberID = memberId, .LoanAmount = Session("Borrow")}
            model.UserID = 26   'Online User
            model.ApplyMethod = PollokCU.AutoLoanFacade.AutoLoanEnums.ApplyMethod.PDLMainApplication
            model.LoanType = PollokCU.AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay
            model.PDLLoanTerm = Session("Period")
            If Session("TotalExpenses") > 0 Then
                model.PaymentMethod = "FP"
            Else
                model.PaymentMethod = "BACS"
            End If
            Dim Engine As PollokCU.AutoLoanFacade.AutoLoanEngine = New PollokCU.AutoLoanFacade.AutoLoanEngine
            Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters
            Engine.GetMemberDetails(model)

            If model.MemberDetails Is Nothing Then
                allow = False
                message = "MemberID not found"
            ElseIf model.MemberDetails IsNot Nothing AndAlso Not model.MemberDetails.AllowProceedALPSForStatus Then
                allow = False
                message = "You are not currently set-up to use this service.  Please contact PCU"
            ElseIf Session("Borrow") > model.AutoLoanParams.MaxAllowedPDLAmount Then
                allow = False
                message = "Maximum repeat pay day loan allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedPDLAmount, 2)
            ElseIf objConfig.getConfigKey("AutoLoanPerformDuplicateCheck") = "TRUE" AndAlso Engine.AutoLoanDuplicated(memberId, model.LoanType.ToString) Then    'Duplicate check is controlled from the web.config
                allow = False
                message = "There is an existing loan applied against your member number. Could not proceed with the loan"
            Else
                Engine.PerformPayDayLoanRule(model)
                If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PayDayLoanRulePassed AndAlso model.PayDayLoanRule.PDLRiskLimit >= model.LoanAmount Then    'Passed
                    model.SaveAutoLoanModelDetails(1)
                    Dim objAutoLoanData As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
                    'objAutoLoanData.UpdateAutoLoanGrantedDate(model.AutoLoanID)
                    Session("AutoLoanModel") = model
                    allow = True
                End If
            End If
            Return allow
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function
#Region "Address Search Related"
    Protected Sub btnSearch1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch1.Click
        Try
            lblAddressMessage.Visible = False

            If Not String.IsNullOrEmpty(txtPostCode1.Text) Then
                Using craftClicks As CraftyClicksFacade.CraftyClicksService = New CraftyClicksFacade.CraftyClicksService With {.AddressLinesCount = 2}
                    Dim addresses As CraftyClicksFacade.AddressSearchResponse = craftClicks.GetPostCodeSearchResults(txtPostCode1.Text)

                    If addresses IsNot Nothing AndAlso addresses.AddressSearchData IsNot Nothing Then
                        If addresses.AddressSearchData.AddressLines.Count > 0 Then
                            AddressManyFoundCraftyClicks(addresses.AddressSearchData, txtPostCode1.Text)
                        ElseIf addresses.AddressSearchData.AddressLines.Count = 0 Then
                            lblAddressMessage.Visible = True
                            lblAddressMessage.Text = "Address not found"
                        End If
                    ElseIf addresses IsNot Nothing AndAlso Not String.IsNullOrEmpty(addresses.ErrorMsg) Then
                        lblAddressMessage.Visible = True
                        lblAddressMessage.Text = addresses.ErrorMsg
                    End If
                End Using
            End If
            
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblAddressMessage.Visible = True
            lblAddressMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub AddressManyFoundCraftyClicks(ByVal addressData As CraftyClicksFacade.AddressSearchResult, ByVal sPostCode As String)
        pnlAddressSearch.Visible = True
        pnlAddress1.Visible = False

        txtPostCodeList.Text = sPostCode
        'Populate combo
        PopulateAddressListComboCraftyClicks(ddlAddressList, addressData)
    End Sub

    Private Sub PopulateAddressListComboCraftyClicks(ByVal ddlCombo As DropDownList, ByVal addressData As CraftyClicksFacade.AddressSearchResult)
        Try
            Dim lList As ListItem = New ListItem

            lList.Text = "Please select"
            lList.Value = ""

            ddlCombo.Items.Clear()
            ddlCombo.Items.Add(lList)

            For Each add As CraftyClicksFacade.AddressItem In addressData.AddressLines
                ddlCombo.Items.Add(New ListItem(add.AddressLine1Formatted & ", " & addressData.Town, addressData.GetComboValue(add)))
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAddressList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressList.SelectedIndexChanged
        If Not String.IsNullOrEmpty(ddlAddressList.SelectedValue) Then
            ShowAddressWithSearchedCraftyClick(ddlAddressList.SelectedValue)
        End If
    End Sub

    Private Sub ShowAddressWithSearchedCraftyClick(ByVal addressSelected As String)
        pnlAddress1.Visible = True
        pnlAddressSearch.Visible = False

        If Not String.IsNullOrEmpty(addressSelected) Then
            Dim addressParts() As String = addressSelected.Split("@"c)
            If addressParts IsNot Nothing AndAlso addressParts.Count > 0 Then
                txtAddLine1_1.Text = addressParts(0)
                If addressParts.Count > 1 Then txtAddLine2_1.Text = addressParts(1)
                If addressParts.Count > 2 Then txtCity1.Text = addressParts(2)
                If addressParts.Count > 3 Then txtCounty1.Text = addressParts(3)
                If addressParts.Count > 4 Then txtPostCode1.Text = addressParts(4)
            End If
        End If
    End Sub

    Protected Sub btnSearch2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch2.Click
        Try
            lblAddressMessage.Visible = False

            If Not String.IsNullOrEmpty(txtPostCode2.Text) Then
                Using craftClicks As CraftyClicksFacade.CraftyClicksService = New CraftyClicksFacade.CraftyClicksService With {.AddressLinesCount = 2}
                    Dim addresses As CraftyClicksFacade.AddressSearchResponse = craftClicks.GetPostCodeSearchResults(txtPostCode2.Text)

                    If addresses IsNot Nothing AndAlso addresses.AddressSearchData IsNot Nothing Then
                        If addresses.AddressSearchData.AddressLines.Count > 0 Then
                            AddressManyFoundCraftyClicks2(addresses.AddressSearchData, txtPostCode2.Text)
                        ElseIf addresses.AddressSearchData.AddressLines.Count = 0 Then
                            lblAddressMessage2.Visible = True
                            lblAddressMessage2.Text = "Address not found"
                        End If
                    ElseIf addresses IsNot Nothing AndAlso Not String.IsNullOrEmpty(addresses.ErrorMsg) Then
                        lblAddressMessage2.Visible = True
                        lblAddressMessage2.Text = addresses.ErrorMsg
                    End If
                End Using
            End If
        Catch ex As Exception
            lblAddressMessage2.Visible = True
            lblAddressMessage2.Text = ex.Message
        End Try
    End Sub

    Private Sub AddressManyFoundCraftyClicks2(ByVal addressData As CraftyClicksFacade.AddressSearchResult, ByVal sPostCode As String)
        pnlAddressSearch2.Visible = True
        pnlAddress2.Visible = False

        txtPostCodeList2.Text = sPostCode
        'Populate combo
        PopulateAddressListComboCraftyClicks(ddlAddressList2, addressData)
    End Sub

    Protected Sub ddlAddressList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressList2.SelectedIndexChanged
        If Not String.IsNullOrEmpty(ddlAddressList2.SelectedValue) Then
            ShowAddressWithSearchedCraftyClick2(ddlAddressList2.SelectedValue)
        End If
    End Sub

    Private Sub ShowAddressWithSearchedCraftyClick2(ByVal addressSelected As String)
        pnlAddress2.Visible = True
        pnlAddressSearch2.Visible = False

        If Not String.IsNullOrEmpty(addressSelected) Then
            Dim addressParts() As String = addressSelected.Split("@"c)
            If addressParts IsNot Nothing AndAlso addressParts.Count > 0 Then
                txtAddLine1_2.Text = addressParts(0)
                If addressParts.Count > 1 Then txtAddLine2_2.Text = addressParts(1)
                If addressParts.Count > 2 Then txtCity2.Text = addressParts(2)
                If addressParts.Count > 3 Then txtCounty2.Text = addressParts(3)
                If addressParts.Count > 4 Then txtPostCode2.Text = addressParts(4)
            End If
        End If
    End Sub

    Protected Sub txtPostCode1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPostCode1.TextChanged
        If txtAddLine1_1.Text.Length > 0 Then
            txtAddLine1_1.Text = ""
            txtAddLine2_1.Text = ""
            txtCity1.Text = ""
            txtCounty1.Text = ""

        End If
        txtAddLine1_1.Focus()
    End Sub

    Protected Sub txtPostCode2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPostCode2.TextChanged
        If txtAddLine1_2.Text.Length > 0 Then
            txtAddLine1_2.Text = ""
            txtAddLine2_2.Text = ""
            txtCity2.Text = ""
            txtCounty2.Text = ""
        End If
        txtAddLine1_2.Focus()
    End Sub
#End Region
End Class
