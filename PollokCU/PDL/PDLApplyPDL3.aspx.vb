﻿Imports System.Data

Partial Class PDL_PDLApplyPDL3
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
        End If

        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If Not IsPostBack Then
                PopulateYearsCombos()
                PopulateControls(Request.QueryString("LoanID"))
            End If
        Else
            Response.Redirect("PDLApplyPDL1.aspx")
        End If
        Dim blnEnableAddressSearch As Boolean = CType(objSystemData.GetSystemKeyValue("EnableAddressSearchPDL"), Boolean)
        btnSearch1.Visible = blnEnableAddressSearch
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            'Fields available in this form
            Dim CompletedLevel As Int16 = 2 'Current page identifier
            Dim SalutationID As Int32 = 0
            Dim FirstName As String = String.Empty
            Dim SurName As String = String.Empty
            Dim MiddleName As String = String.Empty
            Dim Gender As String = String.Empty
            Dim DateOfBirth As DateTime = Nothing
            Dim PlaceOfBirth As String = String.Empty
            Dim MotherName As String = String.Empty
            Dim CurrentAddressLine1 As String = String.Empty
            Dim CurrentAddressLine2 As String = String.Empty
            Dim CurrentCity As String = String.Empty
            Dim CurrentCounty As String = String.Empty
            Dim CurrentPostCode As String = String.Empty
            Dim CurrentAddressNoOfYears As Int32 = 0
            Dim CurrentAddressNoOfMonths As Int32 = 0
            Dim HomeTelephone As String = String.Empty
            Dim Mobile As String = String.Empty
            Dim Email As String = String.Empty
            Dim NINumber As String = String.Empty
            Dim MaritalStatus As Integer = 0
            Dim NoOfDependants As Int32 = 0
            Dim PreviousAddressLine1 As String = String.Empty
            Dim PreviousAddressLine2 As String = String.Empty
            Dim PreviousCity As String = String.Empty
            Dim PreviousCounty As String = String.Empty
            Dim PreviousPostCode As String = String.Empty
            Dim ResidencyStatus As Integer = 0
            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)

            Dim Q1Answer As Int16 = 0
            Dim Q2Answer As Int16 = 0
            Dim Q3Answer As Int16 = 0
            Dim Q4Answer As Int16 = 0
            Dim Q5Answer As Int16 = 0

            Dim EmploymentStatus As Int32 = ddlEmpStatus.SelectedValue
            Dim CompanyID As Int16 = 0
            Dim CompanyName As String = String.Empty
            If EmploymentStatus <= 31 AndAlso ddlEmploerList.SelectedIndex > 0 AndAlso Not RowCompanyName.Visible Then
                CompanyID = ddlEmploerList.SelectedValue
                CompanyName = txtCompanyName.Text
            ElseIf EmploymentStatus <= 31 Then
                CompanyName = txtCompanyName.Text
            End If

            Dim JobTitle As String = IIf(EmploymentStatus <= 31, txtJobTitle.Text, String.Empty)
            Dim CompanyDepartment As String = IIf(EmploymentStatus <= 31, txtDepartment.Text, String.Empty)
            Dim Permanent As Boolean = IIf(EmploymentStatus <= 31, radEmpType.SelectedValue, False)
            Dim ContractDurationYears As Int16 = IIf(EmploymentStatus <= 31 And Not Permanent, ddlContractDurationYears.SelectedValue, 0)
            Dim ContractDurationMonths As Int16 = IIf(EmploymentStatus <= 31 And Not Permanent, ddlContractDurationMonths.SelectedValue, 0)
            Dim WorkAddressLine1 As String = IIf(EmploymentStatus <= 31, txtWorkAddressLine1.Text, String.Empty)
            Dim WorkAddressLine2 As String = String.Empty
            Dim WorkCity As String = IIf(EmploymentStatus <= 31, txtWorkCity.Text, String.Empty)
            Dim WorkCounty As String = IIf(EmploymentStatus <= 31, txtWorkCounty.Text, String.Empty)
            Dim WorkPostCode As String = IIf(EmploymentStatus <= 31, txtWorkPostCode.Text, String.Empty)
            Dim WithEmployerYears As Int32 = IIf(EmploymentStatus <= 31, IIf(ddlEmployerYears.SelectedValue = "YEARS", 0, ddlEmployerYears.SelectedValue), 0)
            Dim WithEmployerMonths As Int32 = IIf(EmploymentStatus <= 31, IIf(ddlEmployerYears.SelectedValue = "MONTHS", 0, ddlEmployerMonths.SelectedValue), 0)
            Dim WorkTelephone As String = IIf(EmploymentStatus <= 31, txtWorkTP.Text, String.Empty)
            Dim MonthlyIncome As Double = IIf(Not IsDBNull(EmploymentStatus), txtMonthlyIncomeAfterTax.Text, 0)
            Dim WageFrequency As String = IIf(Not IsDBNull(EmploymentStatus), ddlIncomeFrequency.Text, String.Empty)
            Dim NextPayDay As Date = Nothing 'IIf(EmploymentStatus <= 31, txtNextPayDay.Text, Nothing)
            If Not IsDBNull(EmploymentStatus) Then
                Date.TryParse(txtNextPayDay.Text, NextPayDay)
            End If
            Dim StaffNumber As String = IIf(EmploymentStatus <= 31, txtStaffNumber.Text, String.Empty)

            If Not IsDBNull(NextPayDay) AndAlso NextPayDay <= Now() Then
                lblMessege.Visible = True
                lblMessege.Text = "Next pay date must be a future date. Please input in DD/MM/YYYY format"
                Return
            End If

            If EmploymentStatus <= 31 AndAlso ddlEmploerList.SelectedIndex = 0 AndAlso String.IsNullOrEmpty(txtCompanyName.Text) Then
                lblMessege.Visible = True
                lblMessege.Text = "Please select or enter the company name"
                Return
            End If

            Dim AccountName As String = ""
            Dim AccountNumber As String = txtAccountNumber.Text
            Dim SortCode As String = txtSortCode.Text

            If AccountNumber.Length < 7 OrElse AccountNumber.Length > 9 OrElse Not IsNumeric(AccountNumber) Then
                lblMessege.Visible = True
                lblMessege.Text = "Please enter a valid account number. Only numbers are allowed and should be 8 digits"
                Return
            End If

            If SortCode.Length <> 6 OrElse Not IsNumeric(SortCode) Then
                lblMessege.Visible = True
                lblMessege.Text = "Please enter a valid sort code. Only numbers are allowed and should be 6 digits"
                Return
            End If

            Dim AccountType As String = "Current"
            Dim CustomerAccountType As String = "Personal"
            Dim AccountOwner As String = ddlAccountOwner.SelectedValue
            Dim DebitCardNumber As String = ""
            Dim BankName As String = txtBankName.Text
            Dim TimeWithBankYears As Integer = ddlTimeWithBankYears.SelectedValue   'This is the opened year
            Dim TimeWithBankMonths As Integer = ddlTimeWithBankMonths.SelectedValue 'Opened month


            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , "" _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , Q1Answer _
                                    , Q2Answer _
                                    , Q3Answer _
                                    , Q4Answer _
                                    , Q5Answer _
                                    , iUserID _
                                    , iLoanID _
                                    , _
                                    , EmploymentStatus _
                                    , CompanyID _
                                    , CompanyName _
                                    , CompanyDepartment _
                                    , Permanent _
                                    , ContractDurationYears _
                                    , ContractDurationMonths _
                                    , WorkAddressLine1 _
                                    , WorkAddressLine2 _
                                    , WorkCity _
                                    , WorkCounty _
                                    , WorkPostCode _
                                    , WithEmployerYears _
                                    , WithEmployerMonths _
                                    , WorkTelephone _
                                    , JobTitle _
                                    , MonthlyIncome _
                                    , WageFrequency _
                                    , NextPayDay _
                                    , StaffNumber _
                                    , AccountName _
                                    , AccountNumber _
                                    , SortCode _
                                    , DebitCardNumber _
                                    , BankName _
                                    , TimeWithBankYears _
                                    , TimeWithBankMonths _
                                    , , , , , , , , , , , , , , , , _
                                    , AccountType _
                                    , CustomerAccountType _
                                    , AccountOwner _
                                    )
            If iLoanID > 0 Then
                Response.Redirect("PDLApplyPDL4.aspx?LoanID=" & iLoanID.ToString)
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub


    Protected Sub ddlEmpStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpStatus.SelectedIndexChanged
        HideAndShowPanel(ddlEmpStatus.SelectedValue)
    End Sub

    Private Sub HideAndShowPanel(ByVal value As Integer)
        If value <= 31 Then
            pnlCompany.Visible = True
            pnlEmpDetails.Visible = True
            upnlEmpType.Visible = True
            pnlPayDetails.Visible = True
        Else
            pnlCompany.Visible = False
            pnlEmpDetails.Visible = False
            upnlEmpType.Visible = False
            pnlPayDetails.Visible = False

        End If
    End Sub

    Protected Sub radEmpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmpType.SelectedIndexChanged
        If radEmpType.SelectedValue = 0 Then
            ddlContractDurationYears.Visible = True
            ddlContractDurationMonths.Visible = True
        Else
            ddlContractDurationYears.Visible = False
            ddlContractDurationMonths.Visible = False
        End If
    End Sub

    Private Sub PopulateYearsCombos()
        ddlEmployerYears.Items.Clear()
        ddlEmployerYears.Items.Add("YEARS")

        For i As Integer = 0 To 50
            ddlEmployerYears.Items.Add(i)
        Next

        ddlTimeWithBankYears.Items.Clear()
        ddlTimeWithBankYears.Items.Add("YEAR")

        For i As Integer = Now.Year - 50 To Now.Year
            ddlTimeWithBankYears.Items.Add(i)
        Next

        ddlContractDurationYears.Items.Clear()
        ddlContractDurationYears.Items.Add("YEARS")

        For i As Integer = 0 To 50
            ddlContractDurationYears.Items.Add(i)
        Next
    End Sub

    Private Sub PopulateEmpMonths()
        ddlEmployerMonths.Items.Clear()

        For i As Integer = 1 To 12
            ddlEmployerMonths.Items.Add(i)
        Next
        ddlEmployerMonths.SelectedIndex = 0
    End Sub

    Protected Sub btnPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPre.Click
        Response.Redirect("PDLApplyPDL2.aspx?LoanID=" & hfLoanID.Value)
    End Sub

    Protected Sub txtPostCode1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWorkPostCode.TextChanged
        If txtWorkAddressLine1.Text.Length > 0 Then
            txtWorkAddressLine1.Text = ""
            txtWorkCity.Text = ""
            txtWorkCounty.Text = ""
        End If
    End Sub

    Private Sub PopulateControls(ByVal iLoanID As Integer)
        Try
            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If dtApp.Rows.Count > 0 Then
                ddlEmpStatus.SelectedValue = IIf(dtApp.Rows(0).Item("EmploymentStatus") = 0, "", dtApp.Rows(0).Item("EmploymentStatus"))

                HideAndShowPanel(dtApp.Rows(0).Item("EmploymentStatus"))
                txtJobTitle.Text = dtApp.Rows(0).Item("JobTitle")
                If IIf(dtApp.Rows(0).Item("CompanyID") Is System.DBNull.Value, 0, dtApp.Rows(0).Item("CompanyID")) > 0 Then
                    ddlEmploerList.SelectedValue = dtApp.Rows(0).Item("CompanyID")
                    txtCompanyName.Text = dtApp.Rows(0).Item("PayrollDeductCompanyName")
                    RowCompanyName.Visible = False
                Else
                    txtCompanyName.Text = dtApp.Rows(0).Item("CompanyName")
                    RowCompanyName.Visible = True
                End If
                txtDepartment.Text = dtApp.Rows(0).Item("CompanyDepartment")
                'radEmpType.Items(0).Selected = dtApp.Rows(0).Item("Permanent")
                'radEmpType.Items(1).Selected = Not dtApp.Rows(0).Item("Permanent")

                radEmpType.Items(0).Selected = True
                radEmpType.Items(1).Selected = False

                If radEmpType.Items(1).Selected Then
                    ddlContractDurationYears.Visible = True
                    ddlContractDurationMonths.Visible = True
                Else
                    ddlContractDurationYears.Visible = False
                    ddlContractDurationMonths.Visible = False
                End If

                ddlContractDurationYears.SelectedValue = IIf(dtApp.Rows(0).Item("ContractDuration") Is System.DBNull.Value, 0, dtApp.Rows(0).Item("ContractDuration"))
                ddlContractDurationMonths.SelectedValue = IIf(dtApp.Rows(0).Item("ContractDurationMonths") Is System.DBNull.Value, -1, dtApp.Rows(0).Item("ContractDurationMonths"))
                txtWorkAddressLine1.Text = dtApp.Rows(0).Item("WorkAddressLine1")
                txtWorkAddressLine2.Text = dtApp.Rows(0).Item("WorkAddressLine2")
                txtWorkCity.Text = dtApp.Rows(0).Item("WorkCity")
                txtWorkCounty.Text = dtApp.Rows(0).Item("WorkCounty")
                txtWorkPostCode.Text = dtApp.Rows(0).Item("WorkPostCode")
                'Trace.Warn(IIf(dtApp.Rows(0).Item("WithEmployerYears") Is System.DBNull.Value, "YEARS", dtApp.Rows(0).Item("WithEmployerYears")))
                ddlEmployerYears.SelectedValue = IIf(dtApp.Rows(0).Item("WithEmployerYears") = 0, "YEARS", dtApp.Rows(0).Item("WithEmployerYears"))
                ddlEmployerMonths.SelectedValue = IIf(dtApp.Rows(0).Item("WithEmployerMonths") = 0, "MONTHS", dtApp.Rows(0).Item("WithEmployerMonths"))
                txtWorkTP.Text = dtApp.Rows(0).Item("WorkTelephone")

                txtMonthlyIncomeAfterTax.Text = Format(dtApp.Rows(0).Item("MonthlyIncomeAfterTax"), "0")
                ddlIncomeFrequency.SelectedValue = IIf(dtApp.Rows(0).Item("WageFrequency") Is System.DBNull.Value, "", dtApp.Rows(0).Item("WageFrequency"))
                txtNextPayDay.Text = IIf(dtApp.Rows(0).Item("NextPayDay") Is System.DBNull.Value, "", dtApp.Rows(0).Item("NextPayDay"))
                txtStaffNumber.Text = dtApp.Rows(0).Item("PayrollNo")

                'txtAccountName.Text = dtApp.Rows(0).Item("AccountName")
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

#Region "Address Search Related"
    Protected Sub btnSearch1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch1.Click
        Try
            lblAddressMessage.Visible = False


            If Not String.IsNullOrEmpty(txtWorkPostCode.Text) Then
                Using craftClicks As CraftyClicksFacade.CraftyClicksService = New CraftyClicksFacade.CraftyClicksService With {.AddressLinesCount = 2}
                    Dim addresses As CraftyClicksFacade.AddressSearchResponse = craftClicks.GetPostCodeSearchResults(txtWorkPostCode.Text)

                    If addresses IsNot Nothing AndAlso addresses.AddressSearchData IsNot Nothing Then
                        If addresses.AddressSearchData.AddressLines.Count > 0 Then
                            AddressManyFoundCraftyClicks(addresses.AddressSearchData, txtWorkPostCode.Text)
                        ElseIf addresses.AddressSearchData.AddressLines.Count = 0 Then
                            lblAddressMessage.Visible = True
                            lblAddressMessage.Text = "Address not found"
                        End If
                    ElseIf addresses IsNot Nothing AndAlso Not String.IsNullOrEmpty(addresses.ErrorMsg) Then
                        lblAddressMessage.Visible = True
                        lblAddressMessage.Text = addresses.ErrorMsg
                    End If
                End Using
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblAddressMessage.Visible = True
            lblAddressMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub AddressManyFoundCraftyClicks(ByVal addressData As CraftyClicksFacade.AddressSearchResult, ByVal sPostCode As String)
        pnlAddressSearch.Visible = True
        pnlAddress1.Visible = False

        txtPostCodeList.Text = sPostCode
        'Populate combo
        PopulateAddressListComboCraftyClicks(ddlAddressList, addressData)
    End Sub

    Private Sub PopulateAddressListComboCraftyClicks(ByVal ddlCombo As DropDownList, ByVal addressData As CraftyClicksFacade.AddressSearchResult)
        Try
            Dim lList As ListItem = New ListItem

            lList.Text = "Please select"
            lList.Value = ""

            ddlCombo.Items.Clear()
            ddlCombo.Items.Add(lList)

            For Each add As CraftyClicksFacade.AddressItem In addressData.AddressLines
                ddlCombo.Items.Add(New ListItem(add.AddressLine1Formatted & ", " & addressData.Town, addressData.GetComboValue(add)))
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAddressList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressList.SelectedIndexChanged
        If Not String.IsNullOrEmpty(ddlAddressList.SelectedValue) Then
            ShowAddressWithSearchedCraftyClick(ddlAddressList.SelectedValue)
        End If
    End Sub

    Private Sub ShowAddressWithSearchedCraftyClick(ByVal addressSelected As String)
        pnlAddress1.Visible = True
        pnlAddressSearch.Visible = False

        If Not String.IsNullOrEmpty(addressSelected) Then
            Dim addressParts() As String = addressSelected.Split("@"c)
            If addressParts IsNot Nothing AndAlso addressParts.Count > 0 Then
                txtWorkAddressLine1.Text = addressParts(0)
                If addressParts.Count > 1 Then txtWorkAddressLine2.Text = addressParts(1)
                If addressParts.Count > 2 Then txtWorkCity.Text = addressParts(2)
                If addressParts.Count > 3 Then txtWorkCounty.Text = addressParts(3)
                If addressParts.Count > 4 Then txtWorkPostCode.Text = addressParts(4)
            End If
        End If
    End Sub

    Protected Sub txtWorkPostCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWorkPostCode.TextChanged
        If txtWorkAddressLine1.Text.Length > 0 Then
            txtWorkAddressLine1.Text = ""
            txtWorkAddressLine2.Text = ""
            txtWorkCity.Text = ""
            txtWorkCounty.Text = ""
        End If
        txtWorkAddressLine1.Focus()
    End Sub
#End Region

    Protected Sub ddlEmploerList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmploerList.SelectedIndexChanged
        If ddlEmploerList.SelectedIndex > 0 Then
            RowCompanyName.Visible = False
        Else
            RowCompanyName.Visible = True
        End If
    End Sub
End Class
