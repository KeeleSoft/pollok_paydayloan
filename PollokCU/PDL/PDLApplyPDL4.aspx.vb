﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PDL_PDLApplyPDL4
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
        End If

        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If Not IsPostBack Then
                PopulateControls(Request.QueryString("LoanID"))
            End If
        Else
            Response.Redirect("PDLApplyPDL1.aspx")
        End If
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            'Fields available in this form
            Dim CompletedLevel As Int16 = 3 'Current page identifier
            Dim SalutationID As Int32 = 0
            Dim FirstName As String = String.Empty
            Dim SurName As String = String.Empty
            Dim MiddleName As String = String.Empty
            Dim Gender As String = String.Empty
            Dim DateOfBirth As DateTime = Nothing
            Dim PlaceOfBirth As String = String.Empty
            Dim MotherName As String = String.Empty
            Dim CurrentAddressLine1 As String = String.Empty
            Dim CurrentAddressLine2 As String = String.Empty
            Dim CurrentCity As String = String.Empty
            Dim CurrentCounty As String = String.Empty
            Dim CurrentPostCode As String = String.Empty
            Dim CurrentAddressNoOfYears As Int32 = 0
            Dim CurrentAddressNoOfMonths As Int32 = 0
            Dim HomeTelephone As String = String.Empty
            Dim Mobile As String = String.Empty
            Dim Email As String = String.Empty
            Dim NINumber As String = String.Empty
            Dim MaritalStatus As Integer = 0
            Dim NoOfDependants As Int32 = 0
            Dim PreviousAddressLine1 As String = String.Empty
            Dim PreviousAddressLine2 As String = String.Empty
            Dim PreviousCity As String = String.Empty
            Dim PreviousCounty As String = String.Empty
            Dim PreviousPostCode As String = String.Empty
            Dim ResidencyStatus As Integer = 0
            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)

            Dim Purpose As String = ddlPurpose.SelectedValue
            Dim Password As String = txtPassword.Text
            Dim PasswordConfirm As String = txtPasswordConfirm.Text

            Dim MemorableWord As String = txtMemorableWord.Text
            Dim MemorableWordConfirm As String = txtMemorableWordConfirm.Text

            If pnlOnlineLoginDetails.Visible Then
                lblMessege.Visible = True
                If Password.Length < 6 Then
                    lblMessege.Text = "Minimum password length is 6 characters. Please try again"
                    Return
                End If

                If MemorableWord.Length < 6 Then
                    lblMessege.Text = "Minimum memorable word length is 6 characters. Please try again"
                    Return
                End If

                If Password <> PasswordConfirm Then
                    lblMessege.Text = "Password and confirmed password do not match. Please try again"
                    Return
                End If

                If MemorableWord <> MemorableWordConfirm Then
                    lblMessege.Text = "Memorable word and confirmed memorable word do not match. Please try again"
                    Return
                End If
                lblMessege.Visible = False
            End If
            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , iUserID _
                                    , iLoanID _
                                    , Purpose _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , Password _
                                    , MemorableWord _
                                    )
            If iLoanID > 0 Then
                'Generate the PDF file and save in the reports folder
                Dim sAgreementPDFName As String
                Dim dtLoan As DataTable

                dtLoan = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

                If dtLoan.Rows.Count > 0 Then
                    DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")
                End If

                sAgreementPDFName = GeneratePDF(iLoanID, DateOfBirth.ToShortDateString.Replace("/", ""))

                Response.Redirect("PDLApplyPDL5.aspx?LoanID=" & iLoanID.ToString)
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPre.Click
        Response.Redirect("PDLApplyPDL3.aspx?LoanID=" & hfLoanID.Value)
    End Sub

    Private Sub PopulateControls(ByVal iLoanID As Integer)
        Try
            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If dtApp.Rows.Count > 0 Then
                ddlPurpose.SelectedValue = IIf(IsDBNull(dtApp.Rows(0).Item("Purpose")), "", dtApp.Rows(0).Item("Purpose"))

                With dtApp.Rows(0)
                    If .Item("NextPayDay") IsNot System.DBNull.Value AndAlso .Item("NextPayDay") > Now() Then
                        txtFirstPaymentDueOn.Text = .Item("NextPayDay").ToShortDateString
                    Else
                        txtFirstPaymentDueOn.Text = DateAdd(DateInterval.Month, 1, Now()).ToShortDateString
                    End If
                    If .Item("MemberID") IsNot System.DBNull.Value AndAlso .Item("MemberID") > 0 _
                            AndAlso .Item("MemberMemorableInfo") IsNot System.DBNull.Value AndAlso .Item("MemberMemorableInfo").ToString.Length > 0 _
                            AndAlso .Item("MemberPassword") IsNot System.DBNull.Value AndAlso .Item("MemberPassword").ToString.Length > 0 Then

                        pnlOnlineLoginDetails.Visible = False
                    Else
                        pnlOnlineLoginDetails.Visible = True
                    End If
                End With
            End If

            Dim LoanDetails As DataTable = objPayDayLoan.GetPreconfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0)

            If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                With LoanDetails.Rows(0)
                    txtLoanAmount.Text = Format(.Item("LoanAmount"), "c")
                    txtRepayPeriod.Text = .Item("LoanPeriod")
                    txtTotalPayable.Text = Format(.Item("TotalToPay"), "c")
                    'txtFirstPaymentDueOn.Text = DateAdd(DateInterval.Month, 1, Now()).ToShortDateString

                    
                End With

            End If



        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Private Function GeneratePDF(ByVal iLoanID As Integer, ByVal sDOB As String) As String


        Dim CrystalReportDocument As ReportDocument
        Dim CrystalExportOptions As ExportOptions
        Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim crTable As Table

        Dim sReportPath As String = MapPath("") & "\PDLLoanAgreement.rpt"
        Trace.Warn(sReportPath)
        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


        Dim Filename, FilenameShort As String
        CrystalReportDocument = New ReportDocument()
        CrystalReportDocument.Load(sReportPath)
        'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
        Dim crTables As Tables = CrystalReportDocument.Database.Tables
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crConnectionInfo.DatabaseName = sSQLServerDB
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

        'SQL SP Parameters
        CrystalReportDocument.SetParameterValue("@ID", iLoanID)

        'Name of the o/p file
        FilenameShort = iLoanID.ToString & "_" & sDOB & ".pdf"
        Filename = MapPath("") & "\Agreements\LoanAgreement_" & FilenameShort



        CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
        CrystalDiskFileDestinationOptions.DiskFileName = Filename
        CrystalExportOptions = CrystalReportDocument.ExportOptions
        With CrystalExportOptions
            .DestinationOptions = CrystalDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With
        CrystalReportDocument.Export()

        Return FilenameShort
        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
        'Response.ContentType = "text/pdf"
        'Response.WriteFile(Filename)
        'Response.Flush()
        'Response.End()
    End Function
End Class
