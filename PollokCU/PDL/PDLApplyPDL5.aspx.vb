﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PDL_PDLApplyPDL5
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objMember As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim objExperian As PollokCU.DataAccess.Layer.clsExperian = New PollokCU.DataAccess.Layer.clsExperian

    Dim ExperianWrapper As ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient = New ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient
    Dim EquifaxFacade As EquifaxFacade.clsChecks = New EquifaxFacade.clsChecks
    'Experien authentication objects
    Dim QASService As AddressService.ProWeb = New AddressService.ProWeb
    Dim objQACanSearch As AddressService.QACanSearch = New AddressService.QACanSearch
    Dim objQASearchOk As AddressService.QASearchOk
    Dim objEngineType As AddressService.EngineType = New AddressService.EngineType
    Dim objQAConfigType As AddressService.QAConfigType = New AddressService.QAConfigType
    Dim objQASearch As AddressService.QASearch = New AddressService.QASearch
    Dim objQASearchResult As AddressService.QASearchResult = Nothing
    Dim iAddressFoundTotal As Integer = 0
    Dim objQAGetAddress As New AddressService.QAGetAddress()
    Dim sLayout As String = System.Configuration.ConfigurationManager.AppSettings("AddressService.Layout")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
            'Session("SessionUserObj") = GetSessionDetails("pv1bb5fdwdss4dnaghsod345")
        End If

        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If Not IsPostBack Then
                PopulateControls(Request.QueryString("LoanID"))
            End If
        Else
            Response.Redirect("PDLApplyPDL1.aspx")
        End If
    End Sub

    Private Function GetSessionDetails(ByVal SessionID As String) As PDLSessionUser
        Dim SessObj As PDLSessionUser = New PDLSessionUser(SessionID, "LCCU")
        Return SessObj
    End Function
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            If Not chkAgree1.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the application summary"
                Exit Sub
            End If

            If Not chkAgree2.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the declaration"
                Exit Sub
            End If

            If Not chkAgree3.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the consent"
                Exit Sub
            End If

            If Not chkAgree4.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the loan agreement"
                Exit Sub
            End If

            If Not chkAgree5.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the terms and conditions"
                Exit Sub
            End If

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            'Fields available in this form
            Dim CompletedLevel As Int16 = 4 'Current page identifier
            Dim SalutationID As Int32 = 0
            Dim FirstName As String = String.Empty
            Dim SurName As String = String.Empty
            Dim MiddleName As String = String.Empty
            Dim Gender As String = String.Empty
            Dim DateOfBirth As DateTime = Nothing
            Dim PlaceOfBirth As String = String.Empty
            Dim MotherName As String = String.Empty
            Dim CurrentAddressLine1 As String = String.Empty
            Dim CurrentAddressLine2 As String = String.Empty
            Dim CurrentCity As String = String.Empty
            Dim CurrentCounty As String = String.Empty
            Dim CurrentPostCode As String = String.Empty
            Dim CurrentAddressNoOfYears As Int32 = 0
            Dim CurrentAddressNoOfMonths As Int32 = 0
            Dim HomeTelephone As String = String.Empty
            Dim Mobile As String = String.Empty
            Dim Email As String = String.Empty
            Dim NINumber As String = String.Empty
            Dim MaritalStatus As Integer = 0
            Dim NoOfDependants As Int32 = 0
            Dim PreviousAddressLine1 As String = String.Empty
            Dim PreviousAddressLine2 As String = String.Empty
            Dim PreviousCity As String = String.Empty
            Dim PreviousCounty As String = String.Empty
            Dim PreviousPostCode As String = String.Empty
            Dim ResidencyStatus As Integer = 0
            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)

            Dim TermsAccepted As Boolean = True ' Will not reach here if not agreed yet
            Dim NormalOccupation As Boolean = IIf(radNormalOccupation.SelectedIndex = 0, True, False)
            Dim GoodHealth As Boolean = IIf(radGoodHealth.SelectedIndex = 0, True, False)

            Dim ApplicantType As Integer = 0
            Dim dtApplicantTypeDetails As DataTable = GetApplicantType(iLoanID, ApplicantType)

            Dim CUCACustomer As Boolean = False
            Dim PayrollCustomer As Boolean = False
            Dim Epic360 As Boolean = False

            If chkAgree6.Checked Then
                Epic360 = True
            End If
            btnNext.Enabled = False

            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , iUserID _
                                    , iLoanID _
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    , TermsAccepted _
                                    ,
                                    ,
                                    , GoodHealth _
                                    , NormalOccupation _
                                    ,
                                    , ApplicantType _
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    , Epic360)

            If iLoanID > 0 Then

                'Do the final checks
                Dim AllChecksPassed As Boolean = False
                Dim AuthenticationPassed As Int16 = 0
                Dim CreditCheckPassed As Int16 = 0
                Dim BankCheckPassed As Int16 = 0
                Dim AccountActiveCheckPassed As Int16 = 0
                Dim EmploymentCheckPassed As Int16 = 0
                Dim PreviousPDLCheckPassed As Int16 = 0

                Dim AuthenticatePassScore As Integer = 100
                Dim CreditPassScore As Integer = 1000
                Dim AllowFasterPaymentForApplicantType As Boolean = False
                Dim MessageCode As String = "MSG2"
                Dim memberStatusFailed As Boolean = False

                If ApplicantType > 0 AndAlso dtApplicantTypeDetails IsNot Nothing AndAlso dtApplicantTypeDetails.Rows.Count > 0 Then
                    AuthenticatePassScore = dtApplicantTypeDetails.Rows(0).Item("AuthenticatePassScore")
                    CreditPassScore = dtApplicantTypeDetails.Rows(0).Item("CreditCheckPassScore")
                    AllowFasterPaymentForApplicantType = CBool(dtApplicantTypeDetails.Rows(0).Item("AllowInstantPayment"))
                End If


                If (ApplicantType = 1 OrElse ApplicantType = 2 OrElse ApplicantType = 3 OrElse ApplicantType = 4 OrElse ApplicantType = 5 OrElse ApplicantType = 12) Then
                    Dim memberData As DataTable = objMember.SelectMemberDetail(MemberID)
                    If memberData IsNot Nothing AndAlso memberData.Rows.Count > 0 Then
                        If Not String.IsNullOrEmpty(memberData.Rows(0).Item("AccountStatus")) AndAlso Not PollokCU.AutoLoanFacade.Member.AllowAccountStatusForLoan(memberData.Rows(0).Item("AccountStatus")) Then
                            memberStatusFailed = True
                        End If
                    End If
                End If

                If Not memberStatusFailed Then 'account status is fine
                    Select Case ApplicantType
                        '1-Member and payroll deducted
                        Case 1
                            AccountActiveCheckPassed = DoAccountActiveCheck(iLoanID)
                            If AccountActiveCheckPassed = 1 Then
                                EmploymentCheckPassed = DoEmploymentCheck(iLoanID)
                                If EmploymentCheckPassed = 1 Then
                                    PreviousPDLCheckPassed = DoPreviousPDLCheck(iLoanID)
                                    If PreviousPDLCheckPassed = 1 Then
                                        CreditCheckPassed = DoCreditCheck(iLoanID, CreditPassScore)
                                        If CreditCheckPassed = 1 Then
                                            BankCheckPassed = DoBankCheck(iLoanID)
                                        End If
                                    End If
                                End If
                            End If
                            If AccountActiveCheckPassed = 1 AndAlso EmploymentCheckPassed = 1 AndAlso PreviousPDLCheckPassed = 1 AndAlso CreditCheckPassed = 1 AndAlso BankCheckPassed = 1 Then
                                AllChecksPassed = True
                            End If

                            '2-Member employed in catchment paying loan
                            '3-Member employed in catchment not paying loan
                            '4-Member employed outside catchment paying loan 
                            '5-Member employed outside catchment not paying loan
                        Case 2, 3, 4, 5
                            AccountActiveCheckPassed = DoAccountActiveCheck(iLoanID)
                            If AccountActiveCheckPassed = 1 Then
                                PreviousPDLCheckPassed = DoPreviousPDLCheck(iLoanID)
                                If PreviousPDLCheckPassed = 1 Then
                                    CreditCheckPassed = DoCreditCheck(iLoanID, CreditPassScore)
                                    If CreditCheckPassed = 1 Then
                                        BankCheckPassed = DoBankCheck(iLoanID)
                                    End If
                                End If
                            End If
                            If AccountActiveCheckPassed = 1 AndAlso PreviousPDLCheckPassed = 1 AndAlso CreditCheckPassed = 1 AndAlso BankCheckPassed = 1 Then
                                AllChecksPassed = True
                            End If

                            '6	Non member lives in catchment payroll deducted employee
                        Case 6
                            PreviousPDLCheckPassed = DoPreviousPDLCheck(iLoanID)
                            If PreviousPDLCheckPassed = 1 Then
                                'AuthenticationPassed = ExperienAuthenticate(iLoanID, AuthenticatePassScore)
                                AuthenticationPassed = DoAuthentication(iLoanID, AuthenticatePassScore)
                                If AuthenticationPassed = 1 Then
                                    CreditCheckPassed = DoCreditCheck(iLoanID, CreditPassScore)
                                    If CreditCheckPassed = 1 Then
                                        BankCheckPassed = DoBankCheck(iLoanID)
                                    End If
                                End If
                            End If

                            If PreviousPDLCheckPassed = 1 AndAlso AuthenticationPassed = 1 AndAlso CreditCheckPassed = 1 AndAlso BankCheckPassed = 1 Then
                                AllChecksPassed = True
                            End If

                            '7	Non member lives in catchment non payroll deducted employee employed in catchment
                            '8-Non member lives in catchment non payroll deducted employee employed outside catchment
                        Case 7, 8
                            PreviousPDLCheckPassed = DoPreviousPDLCheck(iLoanID)
                            If PreviousPDLCheckPassed = 1 Then
                                'AuthenticationPassed = ExperienAuthenticate(iLoanID, AuthenticatePassScore)
                                AuthenticationPassed = DoAuthentication(iLoanID, AuthenticatePassScore)
                                If AuthenticationPassed = 1 Then
                                    CreditCheckPassed = DoCreditCheck(iLoanID, CreditPassScore)
                                    If CreditCheckPassed = 1 Then
                                        BankCheckPassed = DoBankCheck(iLoanID)
                                    End If
                                End If
                            End If

                            If PreviousPDLCheckPassed = 1 AndAlso AuthenticationPassed = 1 AndAlso CreditCheckPassed = 1 AndAlso BankCheckPassed = 1 Then
                                AllChecksPassed = True
                            End If

                            '12	Member unemployed
                        Case 12
                            AccountActiveCheckPassed = DoAccountActiveCheck(iLoanID)
                            If AccountActiveCheckPassed = 1 Then
                                PreviousPDLCheckPassed = DoPreviousPDLCheck(iLoanID)
                                If PreviousPDLCheckPassed = 1 Then
                                    CreditCheckPassed = DoCreditCheck(iLoanID, CreditPassScore)
                                    If CreditCheckPassed = 1 Then
                                        BankCheckPassed = DoBankCheck(iLoanID)
                                    End If
                                End If
                            End If

                            If PreviousPDLCheckPassed = 1 AndAlso AccountActiveCheckPassed = 1 AndAlso CreditCheckPassed = 1 AndAlso BankCheckPassed = 1 Then
                                AllChecksPassed = True
                            End If

                            '9	Non member lives outside catchment payroll deducted employee
                            '10	Non member lives outside catchment non payroll deducted employed in catchment
                            '11	Non member lives outside catchment non payroll deducted employed outside catchment
                        Case 9, 10, 11
                            AllChecksPassed = False

                            '13	Non member lives in catchment unemployed
                        Case 13
                            AllChecksPassed = False
                            MessageCode = "MSG3"

                        Case Else
                            AllChecksPassed = False
                            MessageCode = "MSG3"
                    End Select
                    'End of all the checks


                End If
                'Generate the PDF file and save in the reports folder
                Dim sPDFName As String
                Dim dtLoan As DataTable

                dtLoan = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

                If dtLoan.Rows.Count > 0 Then
                    FirstName = dtLoan.Rows(0).Item("FirstName")
                    SurName = dtLoan.Rows(0).Item("Surname")
                    DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")
                    Email = dtLoan.Rows(0).Item("Email")
                    If dtLoan.Rows(0).Item("AccountSortCodeDec") IsNot System.DBNull.Value AndAlso (dtLoan.Rows(0).Item("AccountSortCodeDec") = "089401" OrElse dtLoan.Rows(0).Item("AccountSortCodeDec") = "089409") Then
                        CUCACustomer = True
                    Else
                        CUCACustomer = False
                    End If

                    If dtLoan.Rows(0).Item("CompanyID") IsNot System.DBNull.Value AndAlso dtLoan.Rows(0).Item("CompanyID") > 0 Then
                        PayrollCustomer = True
                    Else
                        PayrollCustomer = False
                    End If
                End If

                sPDFName = GeneratePDF(iLoanID, SurName, DateOfBirth.ToShortDateString.Replace("/", ""))
                Dim sPDFURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "Reports/" & sPDFName

                If AllChecksPassed AndAlso AllowFasterPaymentForApplicantType Then
                    Dim LoanDetails As DataTable = objPayDayLoan.GetPreconfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0)
                    If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                        'If LoanDetails.Rows(0).Item("InstantLoan") > 0 Then
                        If CUCACustomer Then
                            Response.Redirect("PDLApplyPDL6.aspx?LoanID=" & iLoanID.ToString)   'Got to SMS Page
                        Else
                            Response.Redirect("PDLApplyPDL7.aspx?LoanID=" & iLoanID.ToString)   'Got to Sage Page
                        End If
                        'Else
                        '    'If Email.Length > 0 Then SendAgreementEmail(iLoanID.ToString, Email, FirstName, SurName, DateOfBirth.ToShortDateString.Replace("/", ""), CUCACustomer, PayrollCustomer)
                        '    Response.Redirect("PDLApplyPDL7.aspx?LoanID=" & iLoanID.ToString)   'Got to Sage Page
                        'End If
                    End If
                ElseIf AllChecksPassed Then
                    If CUCACustomer Then
                        Response.Redirect("PDLApplyPDL6.aspx?LoanID=" & iLoanID.ToString)   'Got to SMS Page
                    Else
                        If Email.Length > 0 Then SendAgreementEmail(iLoanID.ToString, Email, FirstName, SurName, DateOfBirth.ToShortDateString.Replace("/", ""), CUCACustomer, PayrollCustomer)
                        Response.Redirect("PDLMessage.aspx?M=MSG1")
                    End If
                Else
                    If Email.Length > 0 Then SendUnsuccessfulEmail(iLoanID.ToString, Email, FirstName, SurName, MemberID, AuthenticationPassed)
                    Response.Redirect("PDLMessage.aspx?M=" & MessageCode)
                End If
                'lblMessege.Visible = True
                'lblMessege.Text = "Testing Message: No Errors. Happy. AuthenticationPassed=" & AuthenticationPassed.ToString
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            btnNext.Enabled = True
        End Try
    End Sub

    Private Function SendAgreementEmail(ByVal LoanID As String, ByVal EmailTo As String, ByVal FirstName As String, ByVal LastName As String, ByVal Dob As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As Boolean
        Try
            Dim sEmailBody As String
            Dim sEmailTo, sEmailFrom, sSubject As String
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
            Dim fileList As List(Of String) = New List(Of String)

            sEmailTo = EmailTo
            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "PCU - WeeGlasgow loan"

            Dim FilenameShort As String = LoanID & "_" & Dob & ".pdf"

            Dim sPDFURL As String = objConfig.getConfigKey("PDL.Agreement.Location") & "LoanAgreement_" & FilenameShort
            fileList.Add(sPDFURL)

            'If Not CUCACustomer AndAlso Payroll Then
            '    Dim sPayrollForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "PayrollDeductionForm.pdf"
            '    fileList.Add(sPayrollForm)
            'ElseIf Not CUCACustomer Then
            '    Dim sDDForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "DIRECT_DEBIT_FORM.pdf"
            '    fileList.Add(sDDForm)
            'End If

            sEmailBody = GetPDLCustomerEmailBody(FirstName, LastName, CUCACustomer, Payroll)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , sEmailTo _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "" _
                                      , fileList)

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Private Function GetPDLCustomerEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Congratulations, your WeeLoan application was submitted successfully. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount to your bank account within twelve hours from you receiving this message. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")

            If CUCACustomer Then
                body.AppendLine("As selected by you during the application process, a standing order will be setup from your Credit Union Current Account (PCU) to be your loan repayment, the details of which is outlined in the loan agreement attached to this email. Please ensure enough funds are available in your CUCA to meet the scheduled loan repayments standing order. Failing to make your loan repayments will lead to your personal credit record to be affected negatively.")
                body.AppendLine("<br/><br/>")
                'ElseIf Payroll Then
                '    body.AppendLine("<strong>How to repay your loan by payroll deduction:</strong>")
                '    body.AppendLine("<br/><br/>")
                '    body.AppendLine("<strong>Please find attached to this email a payroll deduction form. This must be printed out, filled in and signed. Please send the completed direct payroll deduction form back to us as soon as possible to setup your loan repayments. Alternatively you can visit any one of our branches to complete a payroll deduction form. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected.</span></strong>")
                '    body.AppendLine("<br/><br/>")
            Else
                body.AppendLine("<strong>You have successfully setup your loan repayments to be taken from your chosen account as a card payment, with the first repayment date stated within your loan agreement which is attached. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected.</span></strong>")
                body.AppendLine("<br/><br/>")
            End If

            body.AppendLine("For all contact details, branch locations with opening times and further information, please visit our website at http://weeglasgowloan.co.uk/ ")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries please contact us via email to info@pollokcu.com or call us on 0141 881 8731.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our WeeLoan loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function


    Private Function SendUnsuccessfulEmail(ByVal LoanID As String, ByVal EmailTo As String, ByVal FirstName As String, ByVal LastName As String, ByVal MemberID As Integer, ByVal AuthStatus As Integer) As Boolean
        Try
            Dim sEmailBody As String
            Dim sEmailTo, sEmailFrom, sSubject As String
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

            sEmailTo = EmailTo
            sEmailFrom = "noreply@cuOnline.org.uk"
            sSubject = "WeeLoan"

            'If MemberID = 0 AndAlso AuthStatus = 2 Then
            'sEmailBody = GetPDLUnSuccessfulNonMemberFailedAuthEmailBody(FirstName, LastName)
            'Else
            sEmailBody = GetPDLUnSuccessfulEmailBody(FirstName, LastName)
            'End If


            objEmail.SendEmailMessage(sEmailFrom _
                                      , sEmailTo _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "")

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Private Function GetPDLUnSuccessfulEmailBody(ByVal FirstName As String, ByVal LastName As String) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for applying for the Pollok Credit Union’s WeeLoan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Unfortunately, we are unable to process your application using our automated system at this time. Your application will be investigated further by our loan department team and <span style=""text-decoration: underline"">we will get in touch with you by phone or email within 3 working days</span>. We may also request further information from you such as recent payslips and bank statements to assist us in your application.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email to: theweeloan@pollokcu.com")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our WeeLoan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/>")
            body.AppendLine("Main line: 0141 881 8731")
            body.AppendLine("<br/>")
            body.AppendLine("Fax: 0141 881 8731")
            body.AppendLine("<br/>")
            body.AppendLine("Email: theweeloan@pollokcu.com")
            body.AppendLine("<br/>")
            body.AppendLine("Web: http://www.weeglasgowloan.scot/")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Private Function GetPDLUnSuccessfulNonMemberFailedAuthEmailBody(ByVal FirstName As String, ByVal LastName As String) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for applying for the Pollok Credit Union’s Wee Glasgow loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Unfortunately, we are unable to process your application using our automated system at this time, as you have failed our Online Authentication Check.  We need you to come into the office to provide a recent proof of address (such as a benefit letter or bank statement, dated within three months old) and photo identification.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Please come in to one of our branches listed below:")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<Strong>Pollok Credit Union Silverburn Branch</Strong>")
            body.AppendLine("<br/>")
            body.AppendLine("Silverburn Shopping Centre, 763 Barrhead Road, Glasgow, G53 6QR")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<Strong>Possil Branch</Strong>")
            body.AppendLine("<br/>")
            body.AppendLine("264 Saracen Street, Possil, Glasgow, G22 5LF")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<Strong>Royston Branch</Strong>")
            body.AppendLine("<br/>")
            body.AppendLine("117 Royston Road, Glasgow, G21 2QN")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<Strong>Maryhill Branch</Strong>")
            body.AppendLine("<br/>")
            body.AppendLine("Unit 2, Maryhill Shopping Centre, Glasgow, G20 9SH")
            body.AppendLine("<br/><br/>")
            body.AppendLine("You can use any of the following to prove your identity:")
            body.AppendLine("<br/>")
            body.AppendLine("1. A passport")
            body.AppendLine("<br/>")
            body.AppendLine("2. EU member State ID card")
            body.AppendLine("<br/>")
            body.AppendLine("3. Full driving licence or blue disabled driver’s pass")
            body.AppendLine("<br/>")
            body.AppendLine("4. Residence permit")
            body.AppendLine("<br/>")
            body.AppendLine("5. Original letter from a Benefits Agency (or Pensions Service, Child Benefit, etc.)")
            body.AppendLine("<br/><br/>")
            body.AppendLine("You can use any of the following to prove your address:")
            body.AppendLine("<br/>")
            body.AppendLine("1. Recent bill, such as a Council Tax demand or utility bill")
            body.AppendLine("<br/>")
            body.AppendLine("2. Council rent card or tenancy agreement")
            body.AppendLine("<br/>")
            body.AppendLine("3. Recent bank, building society or credit card statement")
            body.AppendLine("<br/>")
            body.AppendLine("4. Recent letter from a UK solicitor or Benefits Agency")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email: theweeloan@pollokcu.com")
            body.AppendLine("<br/><br/>")
            body.AppendLine("We look forward to seeing you soon and to activating your account so that you can take advantage of the benefits of membership.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our weeglasgow loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/>")
            body.AppendLine("Main line: 0141 881 8731")
            body.AppendLine("<br/>")
            body.AppendLine("Email: theweeloan@pollokcu.com")
            body.AppendLine("<br/>")
            body.AppendLine("Web: www.pollokcu.com")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Protected Sub btnPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPre.Click
        Response.Redirect("PDLApplyPDL4.aspx?LoanID=" & hfLoanID.Value)
    End Sub

    Private Sub PopulateControls(ByVal iLoanID As Integer)
        Try
            Dim LoanDetails As DataTable = objPayDayLoan.GetPreconfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0)
            If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                With LoanDetails.Rows(0)
                    litLoanAmount.Text = Format(.Item("LoanAmount"), "c")
                    litRepayPeriod.Text = .Item("LoanPeriod")
                    litInterestDesc.Text = "Total interest at " & Format(CType(Session("SessionUserObj"), PDLSessionUser).LoanAPR, "0.0") & "% APR"
                    litInterestTotal.Text = Format(.Item("TotalInterest"), "c")
                    litSameDayPay.Text = Format(.Item("InstantLoan"), "c")
                    If .Item("InstantLoan") > 0 Then
                        litHowDeposit.Text = "Faster Payment"
                    Else
                        litHowDeposit.Text = "BACS"
                    End If

                    Dim FirstInstallment As Double = .Item("Month1Payment")
                    litFirstPayment.Text = Format(FirstInstallment, "c")
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

        Try
            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If dtApp.Rows.Count > 0 Then
                Dim DOB As String = CType(dtApp.Rows(0).Item("DateOfBirth"), DateTime).ToShortDateString.Replace("/", "")
                hlAgreementLink1.NavigateUrl = "Agreements/LoanAgreement_" & hfLoanID.Value.ToString & "_" & DOB & ".pdf"
                hlLoanAgreementCheck.NavigateUrl = "Agreements/LoanAgreement_" & hfLoanID.Value.ToString & "_" & DOB & ".pdf"

                Dim nextpayDate As DateTime = DateAdd(DateInterval.Month, 1, Now)
                If dtApp.Rows(0).Item("NextPayDay") IsNot System.DBNull.Value Then Date.TryParse(dtApp.Rows(0).Item("NextPayDay"), nextpayDate)
                litFirstPaymentDesc.Text = "Your first installment will come out from your current account on " & nextpayDate.ToShortDateString & "."
            Else
                hlAgreementLink1.NavigateUrl = "#"
                hlLoanAgreementCheck.NavigateUrl = "#"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Private Function GeneratePDF(ByVal iLoanID As Integer, ByVal sSurName As String, ByVal sDOB As String) As String

        Try
            Dim CrystalReportDocument As ReportDocument
            Dim CrystalExportOptions As ExportOptions
            Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
            Dim crtableLogoninfo As New TableLogOnInfo()
            Dim crConnectionInfo As New ConnectionInfo()
            Dim crTable As Table

            Dim sReportPath As String = MapPath("") & "\PDLApplication.rpt"
            Trace.Warn(sReportPath)
            Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
            Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
            Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
            Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


            Dim Filename, FilenameShort As String
            CrystalReportDocument = New ReportDocument()
            CrystalReportDocument.Load(sReportPath)
            'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
            Dim crTables As Tables = CrystalReportDocument.Database.Tables
            For Each crTable In crTables
                crtableLogoninfo = crTable.LogOnInfo
                crConnectionInfo.ServerName = sSQLServerName
                crConnectionInfo.UserID = sSQLServerUserID
                crConnectionInfo.Password = sSQLServerPassword
                crConnectionInfo.DatabaseName = sSQLServerDB
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                crTable.ApplyLogOnInfo(crtableLogoninfo)
            Next
            CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

            'SQL SP Parameters
            CrystalReportDocument.SetParameterValue("@ID", iLoanID)

            'Name of the o/p file
            FilenameShort = iLoanID.ToString & "_" & sSurName & "_" & sDOB & ".pdf"
            Filename = MapPath("") & "\PDLApps\" & FilenameShort



            CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
            CrystalDiskFileDestinationOptions.DiskFileName = Filename
            CrystalExportOptions = CrystalReportDocument.ExportOptions
            With CrystalExportOptions
                .DestinationOptions = CrystalDiskFileDestinationOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With
            CrystalReportDocument.Export()

            Return FilenameShort
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
            'Response.ContentType = "text/pdf"
            'Response.WriteFile(Filename)
            'Response.Flush()
            'Response.End()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try

    End Function

    Private Function GetApplicantType(ByVal LoanID As Integer, ByRef ApplicantType As Integer) As DataTable
        Try
            ApplicantType = 0   'Set to unknown default
            Dim LivingPostCode As String = String.Empty
            Dim WorkPostCode As String = String.Empty
            Dim LivingInCatchment As Boolean = False
            Dim WorkingInCatchment As Boolean = False
            Dim EmploymentStatus As Integer = 0
            Dim MemberID As Integer = 0
            Dim PayrollDeductEmployee As Boolean = False
            Dim MemberLoanExists As Boolean = False
            Dim dtApplicantTypeDetails As DataTable = Nothing

            Dim dtLoan As DataTable = objPayDayLoan.SelectPayDayLoanDetail(LoanID)
            Trace.Warn("Row Count :" & dtLoan.Rows.Count.ToString)
            If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then
                With dtLoan.Rows(0)
                    MemberID = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    LivingPostCode = .Item("CurrentPostCode").ToString
                    EmploymentStatus = .Item("EmploymentStatus")
                    If EmploymentStatus <= 31 Then
                        WorkPostCode = .Item("WorkPostCode").ToString
                        'WorkingInCatchment = Convert.ToBoolean(objPayDayLoan.ValidatePostCode(WorkPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID))
                        Dim dt As DataTable = objPayDayLoan.ValidatePostCode(WorkPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID)
                        If dt.Rows.Count > 0 Then
                            'If CInt(dt.Rows(0).Item("ValidPostCode")) = 1 Then
                            WorkingInCatchment = Convert.ToBoolean(dt.Rows(0).Item("ValidPostCode"))
                            'End If
                        End If
                        If .Item("CompanyID") IsNot System.DBNull.Value AndAlso .Item("CompanyID") > 0 Then
                            PayrollDeductEmployee = True
                        Else
                            PayrollDeductEmployee = False
                        End If
                    End If

                    'LivingInCatchment = Convert.ToBoolean(objPayDayLoan.ValidatePostCode(LivingPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID))
                    Dim dt2 As DataTable = objPayDayLoan.ValidatePostCode(LivingPostCode, CType(Session("SessionUserObj"), PDLSessionUser).CUID)
                    If dt2.Rows.Count > 0 Then
                        'If CInt(dt.Rows(0).Item("ValidPostCode")) = 1 Then
                        LivingInCatchment = Convert.ToBoolean(dt2.Rows(0).Item("ValidPostCode"))
                        ' End If
                    End If
                    Trace.Warn("Catchment: " & LivingInCatchment)
                    Trace.Warn("EmpStatus: " & EmploymentStatus)

                    If MemberID > 0 Then
                        MemberLoanExists = objPayDayLoan.GetMemberLoanExistStatus(MemberID) 'Check member already paying for a loan
                    End If

                    If MemberID > 0 AndAlso PayrollDeductEmployee Then
                        ApplicantType = 1   'Member and payroll deducted
                    ElseIf MemberID > 0 AndAlso WorkingInCatchment AndAlso MemberLoanExists Then
                        ApplicantType = 2   'Member employed in catchment paying loan
                    ElseIf MemberID > 0 AndAlso WorkingInCatchment AndAlso Not MemberLoanExists Then
                        ApplicantType = 3 'Member employed in catchment not paying loan
                    ElseIf MemberID > 0 AndAlso Not WorkingInCatchment AndAlso MemberLoanExists AndAlso EmploymentStatus <= 31 Then
                        ApplicantType = 4   'Member employed outside catchment paying loan 
                    ElseIf MemberID > 0 AndAlso Not WorkingInCatchment AndAlso Not MemberLoanExists AndAlso EmploymentStatus <= 31 Then
                        ApplicantType = 5   'Member employed outside catchment not paying loan
                    ElseIf MemberID = 0 AndAlso LivingInCatchment AndAlso PayrollDeductEmployee Then
                        ApplicantType = 6   'Non member lives in catchment payroll deducted employee
                    ElseIf MemberID = 0 AndAlso LivingInCatchment AndAlso Not PayrollDeductEmployee AndAlso WorkingInCatchment Then
                        ApplicantType = 7   'Non member lives in catchment non payroll deducted employee employed in catchment
                    ElseIf MemberID = 0 AndAlso LivingInCatchment AndAlso EmploymentStatus > 31 Then
                        ApplicantType = 13  'Non member lives in catchment unemployed
                    ElseIf MemberID = 0 AndAlso LivingInCatchment AndAlso Not PayrollDeductEmployee AndAlso Not WorkingInCatchment Then
                        ApplicantType = 8   'Non member lives in catchment non payroll deducted employee employed outside catchment
                    ElseIf MemberID = 0 AndAlso Not LivingInCatchment AndAlso PayrollDeductEmployee Then
                        ApplicantType = 9   'Non member lives outside catchment payroll deducted employee
                    ElseIf MemberID = 0 AndAlso Not LivingInCatchment AndAlso Not PayrollDeductEmployee AndAlso WorkingInCatchment Then
                        ApplicantType = 10  'Non member lives outside catchment non payroll deducted employed in catchment
                    ElseIf MemberID = 0 AndAlso Not LivingInCatchment AndAlso Not PayrollDeductEmployee AndAlso Not WorkingInCatchment Then
                        ApplicantType = 11  'Non member lives outside catchment non payroll deducted employed outside catchment
                    ElseIf MemberID > 0 AndAlso EmploymentStatus > 31 Then
                        ApplicantType = 12  'Member unemployed
                    Else
                        ApplicantType = 0   'Unknown
                    End If

                End With
            End If

            If ApplicantType > 0 Then
                dtApplicantTypeDetails = objPayDayLoan.GetApplicantTypeDetails(ApplicantType)
            End If
            Return dtApplicantTypeDetails
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            Return Nothing
        End Try
    End Function

#Region "Final Checking Methods"
    Private Function DoAuthentication(ByVal iLoanID As Integer, ByVal AuthenticationPassScore As Integer) As Int16
        Try
            Dim PassedAuthentication As Int16 = 0

            Dim dtTXN As DataTable
            dtTXN = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            Dim id As Integer = dtTXN.Rows(0).Item("ID")
            Dim mid As Int32 = Convert.ToInt32(IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), 0))
            Dim fname As String = dtTXN.Rows(0).Item("FirstName")
            Dim sname As String = dtTXN.Rows(0).Item("SurName")
            Dim apptype As String = dtTXN.Rows(0).Item("AppType")
            Dim dob As DateTime = dtTXN.Rows(0).Item("DateOfBirth")

            Dim CurrentAddressLine1 As String = dtTXN.Rows(0).Item("CurrentAddressLine1")
            Dim CurrentAddressLine2 As String = dtTXN.Rows(0).Item("CurrentAddressLine2")
            Dim CurrentCity As String = dtTXN.Rows(0).Item("CurrentCity")
            Dim CurrentCounty As String = dtTXN.Rows(0).Item("CurrentCounty")
            Dim CurrentPostCode As String = dtTXN.Rows(0).Item("CurrentPostCode")

            Dim PreviousAddressLine1 As String = dtTXN.Rows(0).Item("PreviousAddressLine1")
            Dim PreviousAddressLine2 As String = dtTXN.Rows(0).Item("PreviousAddressLine2")
            Dim PreviousCity As String = dtTXN.Rows(0).Item("PreviousCity")
            Dim PreviousCounty As String = dtTXN.Rows(0).Item("PreviousCounty")
            Dim PreviousPostcode As String = dtTXN.Rows(0).Item("PreviousPostCode")
            Dim midname As String = dtTXN.Rows(0).Item("MiddleName")
            Dim Years As Integer = CInt(dtTXN.Rows(0).Item("CurrentAddressNoOfYears"))

            PreviousCounty = PreviousCounty.Replace(" ", "")
            CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
            CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
            PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
            PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

            Dim status As Integer = EquifaxFacade.amlCheck(midname, sname, fname, dob, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, id, apptype, mid)

            If status = 1 Then
                PassedAuthentication = 1    'Pass
            ElseIf status = 2 Then
                PassedAuthentication = 2    'Fail
            Else
                PassedAuthentication = 0    'Technical Fail
            End If

            objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, PassedAuthentication)

            Return PassedAuthentication
        Catch ex As Exception
            'Throw ex
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0    'Not Done
        End Try
    End Function

    Private Function DoCreditCheck(ByVal iLoanID As Integer, ByVal CreditScorePassValue As Integer) As Int16
        Try
            Dim PassedCreditCheck As Int16 = 0

            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

            If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                With dtApp.Rows(0)

                    'Dim MemberID As String = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    'Dim id As Integer = CInt(.Item("ID"))
                    Dim mid As Int32 = Convert.ToInt32(IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0))
                    Dim FirstName As String = .Item("FirstName")
                    Dim SurName As String = .Item("SurName")
                    Dim DOBYear As String = .Item("DOB_YYYY")
                    Dim DOBMonth As String = .Item("DOB_MM")
                    Dim DOBDay As String = .Item("DOB_DD")
                    'Dim MidName As String = .Item("MiddleName")
                    Dim apptype As String = .Item("AppType")
                    Dim a As String = DOBYear + DOBMonth + DOBDay
                    Dim dob As DateTime
                    DateTime.TryParseExact(a, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, dob)

                    Dim CurrentAddressLine1 As String = .Item("CurrentAddressLine1")
                    Dim CurrentAddressLine2 As String = .Item("CurrentAddressLine2")
                    Dim CurrentCity As String = .Item("CurrentCity")
                    Dim CurrentCounty As String = .Item("CurrentCounty")
                    Dim CurrentPostCode As String = .Item("CurrentPostCode")

                    Dim PreviousAddressLine1 As String = .Item("PreviousAddressLine1")
                    Dim PreviousAddressLine2 As String = .Item("PreviousAddressLine2")
                    Dim PreviousCity As String = .Item("PreviousCity")
                    Dim PreviousCounty As String = .Item("PreviousCounty")
                    Dim PreviousPostcode As String = .Item("PreviousPostCode")

                    PreviousCounty = PreviousCounty.Replace(" ", "")
                    CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
                    CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
                    PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
                    PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

                    Dim midname As String = .Item("MiddleName")
                    Dim Years As Integer = CInt(.Item("CurrentAddressNoOfYears"))

                    Dim passScore As Integer
                    Dim dtApplicantTypeDetails = objPayDayLoan.GetApplicantTypeDetails(.Item("ApplicantTypeID"))
                    If dtApplicantTypeDetails IsNot Nothing AndAlso dtApplicantTypeDetails.Rows.Count > 0 Then
                        passScore = CInt(dtApplicantTypeDetails.Rows(0).Item("CreditCheckPassScore"))
                    Else
                        passScore = 0
                    End If

                    If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                        With dtApp.Rows(0)
                            Dim CurrentAL1 As String = .Item("CurrentAddressLine1")
                            Dim CurrentHouseNumber As String = ""
                            Dim CurrentStreetName As String = ""

                            Dim PreAL1 As String = .Item("PreviousAddressLine1")
                            Dim PreHouseNumber As String = ""
                            Dim PreStreetName As String = ""

                            If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                                CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                                CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
                            ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                                CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                                CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
                            End If

                            If PreAL1.Length > 0 AndAlso PreAL1.IndexOf(",") > 0 Then
                                PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(",") - 1)
                                PreStreetName = PreAL1.Substring(PreAL1.IndexOf(",") + 1, PreAL1.Length - PreAL1.IndexOf(",") - 1).TrimStart
                            ElseIf PreAL1.Length > 0 AndAlso PreAL1.IndexOf(" ") > 0 Then
                                PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(" ") - 1)
                                PreStreetName = PreAL1.Substring(PreAL1.IndexOf(" ") + 1, PreAL1.Length - PreAL1.IndexOf(" ") - 1)
                            End If

                            'ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
                            'If ExperianWrapper.DoConsumerCreditSearch(.Item("Title"), .Item("FirstName"), .Item("MiddleName"), .Item("SurName"), .Item("Gender") _
                            ', .Item("DOB_YYYY"), .Item("DOB_MM"), .Item("DOB_DD") _
                            ', CurrentHouseNumber, CurrentStreetName, .Item("CurrentCity"), .Item("CurrentCounty"), .Item("CurrentPostCode") _
                            ' , .Item("CurrentAddressNoOfYears"), .Item("CurrentAddressNoOfMonths") _
                            ', PreHouseNumber, PreStreetName, .Item("PreviousCity"), .Item("PreviousCounty"), .Item("PreviousPostCode") _
                            ', IIf(.Item("PreviousAddressNoOfYears") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfYears"), 0) _
                            ' , IIf(.Item("PreviousAddressNoOfMonths") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfMonths"), 0) _
                            ', CInt(.Item("LoanAmount")), CInt(.Item("LoanPeriod")), .Item("Purpose") _
                            ', iLoanID, Request.ServerVariables("REMOTE_ADDR") _
                            ', CreditScorePassValue) Then
                            'Dim addressid As String = "58150004386"

                            Dim status As Integer = EquifaxFacade.CreditSearch(midname, SurName, FirstName, dob, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, iLoanID, apptype, mid, passScore)

                            If status = 1 Then
                                PassedCreditCheck = 1    'Pass
                            ElseIf status = 2 Then
                                PassedCreditCheck = 2    'Fail
                            Else
                                PassedCreditCheck = 0    'Technical Fail
                            End If
                        End With
                    End If

                    objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, , PassedCreditCheck)
                End With
            End If
            Return PassedCreditCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Private Function DoBankCheck(ByVal iLoanID As Integer) As Int16
        Try
            Dim PassedBankCheck As Int16 = 0

            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

            If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                With dtApp.Rows(0)

                    'Dim MemberID As String = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    Dim mid As Int32 = Convert.ToInt32(IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0))
                    Dim SortCode As String = .Item("AccountSortCodeDec")
                    Dim AccountNumber As String = .Item("AccountNumberDec")
                    Dim AccountSetupDateYear As String = .Item("TimeWithBankYear")
                    Dim AccountSetupDateMonth As String = .Item("TimeWithBankMonth")
                    Dim AccountType As String = .Item("AccountType")
                    Dim CustomerAccountType As String = .Item("CustomerAccountType")
                    Dim FirstName As String = .Item("FirstName")
                    Dim SurName As String = .Item("SurName")
                    Dim DOBYear As String = .Item("DOB_YYYY")
                    Dim DOBMonth As String = .Item("DOB_MM")
                    Dim DOBDay As String = .Item("DOB_DD")
                    Dim MidName As String = .Item("MiddleName")
                    Dim apptype As String = .Item("AppType")

                    Dim AddressYear As String = .Item("CurrentAddressNoOfYears")
                    Dim AddressMonth As String = .Item("CurrentAddressNoOfMonths")
                    Dim BankYear As String = .Item("TimeWithBankYear")
                    Dim BankMonth As Integer = CInt(.Item("TimeWithBankMonth"))

                    Dim CurrentAddressLine1 As String = .Item("CurrentAddressLine1")
                    Dim CurrentAddressLine2 As String = .Item("CurrentAddressLine2")
                    Dim CurrentCity As String = .Item("CurrentCity")
                    Dim CurrentCounty As String = .Item("CurrentCounty")
                    Dim CurrentPostCode As String = .Item("CurrentPostCode")

                    Dim PreviousAddressLine1 As String = .Item("PreviousAddressLine1")
                    Dim PreviousAddressLine2 As String = .Item("PreviousAddressLine2")
                    Dim PreviousCity As String = .Item("PreviousCity")
                    Dim PreviousCounty As String = .Item("PreviousCounty")
                    Dim PreviousPostcode As String = .Item("PreviousPostCode")
                    Dim Years As Integer = CInt(.Item("CurrentAddressNoOfYears"))

                    PreviousCounty = PreviousCounty.Replace(" ", "")
                    CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
                    CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
                    PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
                    PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

                    Dim CurrentAL1 As String = .Item("CurrentAddressLine1")
                    Dim HouseNumber As String = ""
                    Dim FlatNumber As String = ""
                    Dim Street As String = ""

                    If SortCode = "089401" OrElse SortCode = "089409" Then 'Do the internal bank check as LMCU sort code
                        Dim dtBankCheck As DataTable = objPayDayLoan.ValidateCurrentAccount(mid, AccountNumber, iLoanID)
                        If dtBankCheck IsNot Nothing AndAlso dtBankCheck.Rows.Count > 0 AndAlso dtBankCheck.Rows(0).Item("ValidAccount") = True AndAlso dtBankCheck.Rows(0).Item("ValidDeposits") = True Then
                            PassedBankCheck = 1
                        Else
                            PassedBankCheck = 2
                        End If
                    Else
                        If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                            HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                            If HouseNumber.ToLower.Contains("flat") Then
                                FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                                HouseNumber = ""
                            End If
                            Street = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
                        ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                            HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                            If HouseNumber.ToLower.Contains("flat") Then
                                FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                                HouseNumber = ""
                            End If
                            Street = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
                        End If

                        Dim PostCode As String = .Item("CurrentPostCode")
                        Dim OwnerType As String = .Item("AccountOwner")

                        'ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
                        'If ExperianWrapper.DoBankWizard_Verify(SortCode, AccountNumber, AccountSetupDateYear, AccountSetupDateMonth, AccountType, CustomerAccountType _
                        ', FirstName, SurName, DOBYear, DOBMonth, DOBDay, HouseNumber, FlatNumber, Street, PostCode, OwnerType _
                        ' , iLoanID, Request.ServerVariables("REMOTE_ADDR"), "PDL") Then
                        'Dim addressid As String = "58150004386"
                        'Dim timebank As String = "P11Y5M"
                        'Dim timeaddress As String = "P9Y11M"

                        Dim BYear As Integer = DateTime.Now.Year - BankYear
                        Dim BMonth As Integer = CInt(DateTime.Now.Month)

                        If BankMonth > BMonth Then
                            BMonth = BMonth + 12
                            BMonth = BMonth - BankMonth
                            BYear = BYear - 1
                        Else
                            BMonth = BMonth - BankMonth
                        End If

                        Dim timebank As String = "P" & BYear & "Y" & BMonth & "M"
                        Dim timeaddress As String = "P" & AddressYear & "Y" & AddressMonth & "M"

                        Dim substring1 As String = SortCode.Substring(0, 2)
                        Dim substring2 As String = SortCode.Substring(2, 2)
                        Dim substring3 As String = SortCode.Substring(4)

                        Dim srtcd As String = substring1 & "-" & substring2 & "-" & substring3

                        Dim status As Integer = EquifaxFacade.BankCheck(AccountNumber, srtcd, timebank, SurName, FirstName, MidName, timeaddress, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, iLoanID, apptype, mid)

                        If status = 1 Then
                            PassedBankCheck = 1    'Pass
                        ElseIf status = 2 Then
                            PassedBankCheck = 2    'Fail
                        Else
                            PassedBankCheck = 0    'Technical Fail
                        End If

                            objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, , , PassedBankCheck)
                    End If
                End With
            End If
            Return PassedBankCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Private Function DoAccountActiveCheck(ByVal iLoanID As Integer) As Int16
        Try
            Dim PassedAccountActiveCheck As Int16 = 0
            Dim MemberID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 0)
            Dim AccountStatus As String = String.Empty

            If MemberID > 0 Then
                Dim MemberData As DataTable = objMember.SelectMemberDetail(MemberID)
                If MemberData IsNot Nothing AndAlso MemberData.Rows.Count > 0 Then
                    AccountStatus = MemberData.Rows(0).Item("AccountStatus")

                    If AccountStatus = "A" OrElse AccountStatus = "E" OrElse AccountStatus = "M" OrElse AccountStatus = "H" OrElse AccountStatus = "I" OrElse AccountStatus = "K" OrElse AccountStatus = "L" Then
                        PassedAccountActiveCheck = 1
                    Else
                        PassedAccountActiveCheck = 2
                    End If
                End If
            End If
            objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, , , , PassedAccountActiveCheck)
            Return PassedAccountActiveCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Private Function DoEmploymentCheck(ByVal iLoanID As Integer) As Int16
        Try
            Dim PassedEmploymentCheck As Int16 = 0  'Set as passed for now

            Dim MemberID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 0)
            Dim AccountStatus As String = String.Empty

            If MemberID > 0 Then
                Dim StatusOK As Boolean = objPayDayLoan.GetMemberPayrollStatus(MemberID)
                If StatusOK Then
                    PassedEmploymentCheck = 1
                Else
                    PassedEmploymentCheck = 2
                End If
            End If

            objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, , , , , PassedEmploymentCheck)
            Return PassedEmploymentCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Private Function DoPreviousPDLCheck(ByVal iLoanID As Integer) As Int16
        Try
            Dim PassedPDLCheck As Int16 = 0  'Set as not performed

            Dim StatusOK As Boolean = objPayDayLoan.GetPreviousPayDayLoanExistStatus(iLoanID)
            If StatusOK Then    'Loan Exists
                PassedPDLCheck = 2
            Else
                PassedPDLCheck = 1  'No loan exists, So OK to proceed
            End If
            Return PassedPDLCheck

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function
#End Region


#Region "Experien Authentication"
    Protected Function ExperienAuthenticate(ByVal iLoanID As Integer, ByVal AuthenticationPassScore As Integer) As Int16


        Try
            Dim PassedAuthentication As Int16 = 0
            Dim ExperienErrorMessage As String = ""
            Dim sDecisionCode As String = ""
            Dim sExperianRef As String = ""
            Dim sAuthIndexCode As String = ""
            Dim sAuthIndexText As String = ""
            Dim sAuthDecisionCode As String = ""
            Dim sAuthDecisionText As String = ""
            Dim sPolicyCode As String = ""
            Dim sPolicyRuleText As String = ""

            SetupEngineTypeAuth(objEngineType)
            objQACanSearch.Country = "GBR"
            objQACanSearch.Engine = objEngineType
            objQACanSearch.Layout = sLayout   '"< Default >"
            objQACanSearch.QAConfig = objQAConfigType


            objQASearchOk = QASService.DoCanSearch(objQACanSearch)

            objQASearch.Country = "GBR"
            objQASearch.Engine = objEngineType
            objQASearch.Engine.PromptSet = AddressService.PromptSetType.Default
            objQASearch.Engine.PromptSetSpecified = True
            objQASearch.Layout = sLayout
            objQASearch.QAConfig = objQAConfigType

            Dim objSearchTerm(25) As AddressService.SearchTerm

            PopulateSearchTerm(objSearchTerm, iLoanID)   'Populate search criterias byref

            objQASearch.SearchSpec = objSearchTerm

            objQASearchResult = QASService.DoSearch(objQASearch)

            'Search return is in objQASearchResult object now
            Dim picklist As AddressService.QAPicklistType = objQASearchResult.QAPicklist
            Dim address As AddressService.QAAddressType = objQASearchResult.QAAddress

            If picklist IsNot Nothing Then
                ExperienErrorMessage = generateExperienErrorMessage(picklist)
            Else
                ExperienErrorMessage = ""
            End If

            'Populate experian data only if availabe. Otherwise they will be balnk by default
            If address IsNot Nothing Then
                generateExperienSearchResult(address, sDecisionCode, sExperianRef, sAuthIndexCode, sAuthIndexText, sAuthDecisionCode, sAuthDecisionText, sPolicyCode, sPolicyRuleText)

                'Save the details against the application
                objExperian.UpdateAuthenticationOldData(iLoanID, ExperienErrorMessage, sDecisionCode, sExperianRef, sAuthIndexCode, sAuthIndexText, sAuthDecisionCode, sAuthDecisionText, sPolicyCode, sPolicyRuleText)

                If sAuthIndexCode IsNot Nothing AndAlso sAuthIndexCode >= AuthenticationPassScore Then
                    PassedAuthentication = 1    'Passed
                Else
                    PassedAuthentication = 2    'Failed
                End If
            End If

            objPayDayLoan.UpdateApplicationCheckStatus(iLoanID, PassedAuthentication)
            Return PassedAuthentication
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0    'Not Done
        End Try
    End Function

    Private Sub SetupEngineTypeAuth(ByRef et As AddressService.EngineType)
        et.Flatten = True
        'et.flattenField = True
        'et.flattenFieldSpecified = True
        et.FlattenSpecified = True
        et.Intensity = AddressService.EngineIntensityType.Exact
        'et.intensityField = AddressService.EngineIntensityType.Exact
        'et.intensityFieldSpecified = False
        et.IntensitySpecified = False
        et.PromptSet = AddressService.PromptSetType.Default
        'et.promptSetField= AddressService.PromptSetType.Default
        'et.promptSetFieldSpecified = True
        et.PromptSetSpecified = True
        et.Threshold = Nothing
        'et.thresholdField = Nothing
        et.Timeout = Nothing
        'et.timeoutField = Nothing
        et.Value = AddressService.EngineEnumType.Authenticate
        'et.valueField = AddressService.EngineEnumType.Singleline
    End Sub

    Private Sub PopulateSearchTerm(ByRef objSearchTerm() As AddressService.SearchTerm, ByVal iLoanID As Integer)
        Try
            Dim dtLoanDet As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

            If dtLoanDet.Rows.Count > 0 Then
                objSearchTerm(0) = New AddressService.SearchTerm
                objSearchTerm(0).Key = "CTRL_SEARCHCONSENT"
                objSearchTerm(0).Value = "Yes"

                objSearchTerm(1) = New AddressService.SearchTerm
                objSearchTerm(1).Key = "NAME_TITLE"
                objSearchTerm(1).Value = IIf(dtLoanDet.Rows(0).Item("Title") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Title"), "")

                objSearchTerm(2) = New AddressService.SearchTerm
                objSearchTerm(2).Key = "NAME_FORENAME"
                objSearchTerm(2).Value = IIf(dtLoanDet.Rows(0).Item("FirstName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("FirstName"), "")

                objSearchTerm(3) = New AddressService.SearchTerm
                objSearchTerm(3).Key = "NAME_INITIALS"
                objSearchTerm(3).Value = IIf(dtLoanDet.Rows(0).Item("MiddleName") IsNot System.DBNull.Value, Left(dtLoanDet.Rows(0).Item("MiddleName"), 1), "")

                objSearchTerm(4) = New AddressService.SearchTerm
                objSearchTerm(4).Key = "NAME_SURNAME"
                objSearchTerm(4).Value = IIf(dtLoanDet.Rows(0).Item("SurName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("SurName"), "")

                objSearchTerm(5) = New AddressService.SearchTerm
                objSearchTerm(5).Key = "NAME_DATEOFBIRTH"
                objSearchTerm(5).Value = IIf(dtLoanDet.Rows(0).Item("DateOfBirth") IsNot System.DBNull.Value, Format(dtLoanDet.Rows(0).Item("DateOfBirth"), "dd/MM/yyyy"), "")

                objSearchTerm(6) = New AddressService.SearchTerm
                objSearchTerm(6).Key = "NAME_SEX"
                objSearchTerm(6).Value = IIf(dtLoanDet.Rows(0).Item("Gender") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Gender"), "") 'getGender(IIf(dtLoanDet.Rows(0).Item("Title") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Title"), "")) 'M or F, We dont capture this

                Dim sFlat As String = ""
                Dim sHouseName As String = ""
                Dim sHouseNo As String = ""
                Dim sStreet1 As String = ""

                If dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value AndAlso dtLoanDet.Rows(0).Item("CurrentAddressLine1").ToString.Contains(",") Then
                    Dim sAddL1 As String = dtLoanDet.Rows(0).Item("CurrentAddressLine1").ToString
                    sFlat = Left(sAddL1, sAddL1.IndexOf(","))
                    sHouseName = Right(sAddL1, (sAddL1.Length - sAddL1.ToString.IndexOf(",") - 2))
                    sStreet1 = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine2") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine2"), "")
                ElseIf dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value Then
                    sHouseNo = dtLoanDet.Rows(0).Item("CurrentAddressLine1")
                    sHouseNo = Left(sHouseNo, sHouseNo.ToString.IndexOf(" "))
                    sStreet1 = dtLoanDet.Rows(0).Item("CurrentAddressLine1")
                    sStreet1 = Right(sStreet1, (sStreet1.Length - sStreet1.ToString.IndexOf(" ")))
                End If

                objSearchTerm(7) = New AddressService.SearchTerm
                objSearchTerm(7).Key = "ADDR_FLAT"
                objSearchTerm(7).Value = sFlat

                objSearchTerm(8) = New AddressService.SearchTerm
                objSearchTerm(8).Key = "ADDR_HOUSENAME"
                objSearchTerm(8).Value = sHouseName

                'Dim sHouseNo As String = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine1"), "")
                'sHouseNo = Left(sHouseNo, sHouseNo.ToString.IndexOf(" "))

                objSearchTerm(9) = New AddressService.SearchTerm
                objSearchTerm(9).Key = "ADDR_HOUSENUMBER"
                objSearchTerm(9).Value = sHouseNo

                'Dim sStreet1 As String = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine1"), "")
                'sStreet1 = Right(sStreet1, (sStreet1.Length - sStreet1.ToString.IndexOf(" ")))

                objSearchTerm(10) = New AddressService.SearchTerm
                objSearchTerm(10).Key = "ADDR_STREET"
                objSearchTerm(10).Value = sStreet1

                objSearchTerm(11) = New AddressService.SearchTerm
                objSearchTerm(11).Key = "ADDR_DISTRICT"
                objSearchTerm(11).Value = ""

                objSearchTerm(12) = New AddressService.SearchTerm
                objSearchTerm(12).Key = "ADDR_TOWN"
                objSearchTerm(12).Value = IIf(dtLoanDet.Rows(0).Item("CurrentCity") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentCity"), "")

                objSearchTerm(13) = New AddressService.SearchTerm
                objSearchTerm(13).Key = "ADDR_COUNTY"
                objSearchTerm(13).Value = IIf(dtLoanDet.Rows(0).Item("CurrentCounty") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentCounty"), "")

                objSearchTerm(14) = New AddressService.SearchTerm
                objSearchTerm(14).Key = "ADDR_POSTCODE"
                objSearchTerm(14).Value = IIf(dtLoanDet.Rows(0).Item("CurrentPostCode") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentPostCode"), "")

                objSearchTerm(15) = New AddressService.SearchTerm
                objSearchTerm(15).Key = "AUT1_MOTHERMAIDENNAME"
                objSearchTerm(15).Value = IIf(dtLoanDet.Rows(0).Item("MotherName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("MotherName"), "")

                objSearchTerm(16) = New AddressService.SearchTerm
                objSearchTerm(16).Key = "AUT1_PLACEOFBIRTH"
                objSearchTerm(16).Value = IIf(dtLoanDet.Rows(0).Item("PlaceOfBirth") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PlaceOfBirth"), "")

                Dim sCurrentAddressNoOfYears = IIf(dtLoanDet.Rows(0).Item("CurrentAddressNoOfYears") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressNoOfYears"), 0)
                Dim sCurrentAddressNoOfMonths = IIf(dtLoanDet.Rows(0).Item("CurrentAddressNoOfMonths") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressNoOfMonths"), 0)
                Dim sDateFrom As Date = DateAdd(DateInterval.Month, sCurrentAddressNoOfMonths * -1, DateAdd(DateInterval.Year, sCurrentAddressNoOfYears * -1, Now()))

                objSearchTerm(17) = New AddressService.SearchTerm
                objSearchTerm(17).Key = "RESY_DATEFROM#1"
                objSearchTerm(17).Value = ""    'Its screws if you pass this , String.Format(sDateFrom.ToShortDateString, "dd/MM/yyyy")

                objSearchTerm(18) = New AddressService.SearchTerm
                objSearchTerm(18).Key = "ADDR_FLAT#1"
                objSearchTerm(18).Value = ""

                objSearchTerm(19) = New AddressService.SearchTerm
                objSearchTerm(19).Key = "ADDR_HOUSENAME#1"
                objSearchTerm(19).Value = ""

                Dim sHouseNo2 As String = IIf(dtLoanDet.Rows(0).Item("PreviousAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousAddressLine1"), "")
                If sHouseNo2.Contains(" ") Then sHouseNo2 = Left(sHouseNo2, sHouseNo2.ToString.IndexOf(" "))

                objSearchTerm(20) = New AddressService.SearchTerm
                objSearchTerm(20).Key = "ADDR_HOUSENUMBER#1"
                objSearchTerm(20).Value = sHouseNo2

                Dim sStreet2 As String = IIf(dtLoanDet.Rows(0).Item("PreviousAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousAddressLine1"), "")
                If sStreet2.Contains(" ") Then sStreet2 = Right(sStreet2, (sStreet2.Length - sStreet2.ToString.IndexOf(" ")))

                objSearchTerm(21) = New AddressService.SearchTerm
                objSearchTerm(21).Key = "ADDR_STREET#1"
                objSearchTerm(21).Value = sStreet2

                objSearchTerm(22) = New AddressService.SearchTerm
                objSearchTerm(22).Key = "ADDR_DISTRICT#1"
                objSearchTerm(22).Value = ""

                objSearchTerm(23) = New AddressService.SearchTerm
                objSearchTerm(23).Key = "ADDR_TOWN#1"
                objSearchTerm(23).Value = IIf(dtLoanDet.Rows(0).Item("PreviousCity") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousCity"), "")

                objSearchTerm(24) = New AddressService.SearchTerm
                objSearchTerm(24).Key = "ADDR_COUNTY#1"
                objSearchTerm(24).Value = IIf(dtLoanDet.Rows(0).Item("PreviousCounty") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousCounty"), "")

                objSearchTerm(25) = New AddressService.SearchTerm
                objSearchTerm(25).Key = "ADDR_POSTCODE#1"
                objSearchTerm(25).Value = IIf(dtLoanDet.Rows(0).Item("PreviousPostCode") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousPostCode"), "")
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        End Try
    End Sub

    Private Function generateExperienErrorMessage(ByVal picklist As AddressService.QAPicklistType) As String
        Dim sError As String = ""
        For i = 0 To picklist.PicklistEntry.Length - 1
            sError = sError & picklist.PicklistEntry(i).Picklist & vbCrLf
        Next
        Return sError
    End Function

    Private Sub generateExperienSearchResult(ByVal address As AddressService.QAAddressType _
                                             , ByRef sDecisionCode As String _
                                               , ByRef sExperianRef As String _
                                               , ByRef sAuthIndexCode As String _
                                               , ByRef sAuthIndexText As String _
                                               , ByRef sAuthDecisionCode As String _
                                               , ByRef sAuthDecisionText As String _
                                               , ByRef sPolicyCode As String _
                                               , ByRef sPolicyRuleText As String)

        For i = 0 To address.AddressLine.Length - 1
            Select Case address.AddressLine(i).Label.Trim
                Case "Authenticate Decision text"
                    sAuthDecisionText = address.AddressLine(i).Line
                Case "Authenticate Decision"
                    sAuthDecisionCode = address.AddressLine(i).Line
                Case "Authenticate Authentication index explanation"
                    sAuthIndexText = address.AddressLine(i).Line
                Case "Authenticate Authentication index"
                    sAuthIndexCode = address.AddressLine(i).Line
                Case "Authenticate HR Policy rule"
                    sPolicyCode = address.AddressLine(i).Line
                Case "Authenticate HR Policy rule text"
                    sPolicyRuleText = address.AddressLine(i).Line
                Case "Authenticate Experian customer reference"
                    sExperianRef = address.AddressLine(i).Line
            End Select
        Next

    End Sub

    Private Function getGender(ByVal sSalutation As String) As String
        If sSalutation = "Mr" OrElse sSalutation = "Sir" OrElse sSalutation = "Professor" Then
            Return "M"
        ElseIf sSalutation = "Mrs" OrElse sSalutation = "Ms" OrElse sSalutation = "Miss" Then
            Return "F"
        Else
            Return ""
        End If
    End Function
#End Region
End Class
