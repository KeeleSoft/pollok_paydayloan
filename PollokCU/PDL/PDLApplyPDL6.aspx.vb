﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PDL_PDLApplyPDL6
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            If Request.QueryString("VendorTxCode") IsNot Nothing Then
                Dim PDLData As DataTable = objPayDayLoan.SelectPayDayLoanDetail(Request.QueryString("LoanID"))
                If PDLData IsNot Nothing AndAlso PDLData.Rows.Count > 0 Then
                    Session("SessionUserObj") = GetSessionDetails(PDLData.Rows(0).Item("SessionID"))
                Else
                    Response.Redirect("PDLError.aspx?C=E2")
                End If
            Else
                Response.Redirect("PDLError.aspx?C=E2")
            End If

        End If

        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If objPayDayLoan.GetPDLPaymentSentStatus(Request.QueryString("LoanID")) Then    'When there is a payment recorded against this ID do not proceed
                Response.Redirect("PDLError.aspx?C=E7")
            End If

            If Not IsPostBack Then
                GenerateCode(Request.QueryString("LoanID"))
            End If
        Else
            Response.Redirect("PDLApplyPDL1.aspx")
        End If
    End Sub

    Private Function GetSessionDetails(ByVal SessionID As String) As PDLSessionUser
        Dim SessObj As PDLSessionUser = New PDLSessionUser(SessionID, "LCCU")
        Return SessObj
    End Function
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            If objPayDayLoan.GetPDLPaymentSentStatus(iLoanID) Then    'When there is a payment recorded against this ID do not proceed
                Response.Redirect("PDLError.aspx?C=E7")
            End If

            'Fields available in this form
            Dim CompletedLevel As Int16 = 5 'Current page identifier
            Dim SalutationID As Int32 = 0
            Dim FirstName As String = String.Empty
            Dim SurName As String = String.Empty
            Dim MiddleName As String = String.Empty
            Dim Gender As String = String.Empty
            Dim DateOfBirth As DateTime = Nothing
            Dim PlaceOfBirth As String = String.Empty
            Dim MotherName As String = String.Empty
            Dim CurrentAddressLine1 As String = String.Empty
            Dim CurrentAddressLine2 As String = String.Empty
            Dim CurrentCity As String = String.Empty
            Dim CurrentCounty As String = String.Empty
            Dim CurrentPostCode As String = String.Empty
            Dim CurrentAddressNoOfYears As Int32 = 0
            Dim CurrentAddressNoOfMonths As Int32 = 0
            Dim HomeTelephone As String = String.Empty
            Dim Mobile As String = String.Empty
            Dim Email As String = String.Empty
            Dim NINumber As String = String.Empty
            Dim MaritalStatus As Integer = 0
            Dim NoOfDependants As Int32 = 0
            Dim PreviousAddressLine1 As String = String.Empty
            Dim PreviousAddressLine2 As String = String.Empty
            Dim PreviousCity As String = String.Empty
            Dim PreviousCounty As String = String.Empty
            Dim PreviousPostCode As String = String.Empty
            Dim ResidencyStatus As Integer = 0
            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)

            lblMessege.Visible = True
            If txtSecurityCode.Text <> txtSecurityCodeConfirm.Text Then
                lblMessege.Text = "Please verify the security code and the confirmed security code are same."
                Return
            End If

            If hfAttempt.Value >= 3 Then
                ''Refer to staff with the status
                Try
                    Dim objPDLStaff As PollokCU.DataAccess.Layer.clsStaffPayDayLoan = New PollokCU.DataAccess.Layer.clsStaffPayDayLoan
                    objPDLStaff.UpdateApplicationByStaff(iLoanID, 15, 26, iUserID)  'Security check failed
                Catch ex As Exception
                    objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
                End Try
                
                Response.Redirect("PDLMessage.aspx?M=MSG2")
            End If

            If txtSecurityCode.Text <> CType(Session("SessionUserObj"), PDLSessionUser).SecurityCode Then
                hfAttempt.Value = CInt(hfAttempt.Value) + 1
                lblMessege.Text = "Security code does not match. Please re-try. (Attempt " & hfAttempt.Value & ")"
                Return
            End If



            lblMessege.Visible = False
            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , iUserID _
                                    , iLoanID _
                                    )
            If iLoanID > 0 Then
                
                'Generetae FasterPayment
                GenerateCSV(iLoanID)
                'End of FasterPayment

                SendAgreementEmail(iLoanID) 'Send the agreement email
                Response.Redirect("PDLMessage.aspx?M=MSG1")
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPre.Click
        Response.Redirect("PDLApplyPDL5.aspx?LoanID=" & hfLoanID.Value)
    End Sub

    Private Function SendAgreementEmail(ByVal LoanID As String) As Boolean
        Try
            Dim sEmailBody As String
            Dim Email, FirstName, LastName, sEmailFrom, sSubject As String
            Dim Dob As Date
            Dim CUCACustomer As Boolean = False
            Dim PayrollCustomer As Boolean = False
            Dim fileList As List(Of String) = New List(Of String)

            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

            Dim dtLoan As DataTable = objPayDayLoan.SelectPayDayLoanDetail(LoanID)

            If dtLoan.Rows.Count > 0 Then
                FirstName = dtLoan.Rows(0).Item("FirstName")
                LastName = dtLoan.Rows(0).Item("Surname")
                Dob = dtLoan.Rows(0).Item("DateOfBirth")
                Email = dtLoan.Rows(0).Item("Email")

                If dtLoan.Rows(0).Item("AccountSortCodeDec") IsNot System.DBNull.Value AndAlso (dtLoan.Rows(0).Item("AccountSortCodeDec") = "089401" OrElse dtLoan.Rows(0).Item("AccountSortCodeDec") = "089409") Then
                    CUCACustomer = True
                Else
                    CUCACustomer = False
                End If

                If dtLoan.Rows(0).Item("CompanyID") IsNot System.DBNull.Value AndAlso dtLoan.Rows(0).Item("CompanyID") > 0 Then
                    PayrollCustomer = True
                Else
                    PayrollCustomer = False
                End If

                sEmailFrom = "noreply@cuonline.org.uk"
                sSubject = "WeeLoan"

                Dim FilenameShort As String = LoanID & "_" & Dob.ToShortDateString.Replace("/", "") & ".pdf"

                Dim sPDFURL As String = objConfig.getConfigKey("PDL.Agreement.Location") & "LoanAgreement_" & FilenameShort
                fileList.Add(sPDFURL)

                'If Not CUCACustomer AndAlso PayrollCustomer Then
                '    Dim sPayrollForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "PayrollDeductionForm.pdf"
                '    fileList.Add(sPayrollForm)
                'ElseIf Not CUCACustomer Then
                '    Dim sDDForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "DIRECT_DEBIT_FORM.pdf"
                '    fileList.Add(sDDForm)
                'End If

                sEmailBody = GetPDLCustomerEmailBody(FirstName, LastName, CUCACustomer, PayrollCustomer)

                objEmail.SendEmailMessage(sEmailFrom _
                                          , Email _
                                          , sSubject _
                                          , sEmailBody _
                                          , "" _
                                          , "" _
                                          , fileList)


                If CUCACustomer Then    'Send to CUCA if a CUCA applicant
                    Dim CUCAFileList As List(Of String) = New List(Of String)
                    CUCAFileList.Add(sPDFURL)   'Attach agreement
                    Dim PDFApp As String = LoanID.ToString & "_" & LastName & "_" & Dob.ToShortDateString.Replace("/", "") & ".pdf"
                    Dim PDFURLLong As String = MapPath("") & "\PDLApps\" & PDFApp
                    CUCAFileList.Add(PDFURLLong)

                    objEmail.SendEmailMessage(sEmailFrom _
                                          , "gayeshan@yahoo.coms" _
                                          , "PDL Standing Order Setup - AUTO EMAIL" _
                                          , "PDL application and agreement are attached" _
                                          , "" _
                                          , "" _
                                          , fileList)
                End If
            End If
            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Private Function GetPDLCustomerEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Congratulations, your WeeeLoan application was submitted successfully. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount to your bank account within one working day from you receiving this message. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")

            If CUCACustomer Then
                body.AppendLine("As selected by you during the application process, a standing order will be setup from your Credit Union Current Account (CUCA) to be your loan repayment, the details of which is outlined in the loan agreement attached to this email. Please ensure enough funds are available in your CUCA to meet the scheduled loan repayments standing order. Failing to make your loan repayments will lead to your personal credit record to be affected negatively.")
                body.AppendLine("<br/><br/>")
                'ElseIf Payroll Then
                '    body.AppendLine("<strong>How to repay your loan by payroll deduction:</strong>")
                '    body.AppendLine("<br/><br/>")
                '    body.AppendLine("<strong>Please find attached to this email a payroll deduction form. This must be printed out, filled in and signed. Please send the completed direct payroll deduction form back to us as soon as possible to setup your loan repayments. Alternatively you can visit any one of our branches to complete a payroll deduction form. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected. </span></strong>")
                '    body.AppendLine("<br/><br/>")
            Else
                body.AppendLine("<strong>You have successfully setup your loan repayments to be taken from your chosen account as a card payment, with the first repayment date stated within your loan agreement which is attached. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected.</span></strong>")
                body.AppendLine("<br/><br/>")
            End If

            body.AppendLine("For all contact details, branch locations with opening times and further information, please visit our website at http://www.pollokcu.com/ ")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries please contact us via email to theweeloan@pollokcu.com or call us on 0141 881 8731.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our Wee Glasgow loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Private Sub GenerateCode(ByVal iLoanID As Integer)
        Try
            Dim objRandom As New System.Random(CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))

            Dim SecurityCode As Integer = objRandom.Next(100000, 999999)
            CType(Session("SessionUserObj"), PDLSessionUser).SecurityCode = SecurityCode.ToString

            lblMessege.Visible = False

            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)
            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)

            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , 0 _
                                    , 0 _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , Date.MinValue _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , 0 _
                                    , 0 _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , 0 _
                                    , 0 _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , "" _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , iUserID _
                                    , iLoanID _
                                    , "" _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , , _
                                    , SecurityCode _
                                    )

            SMSSecurityCode(iLoanID)   'Send the SMS to the applicant

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Private Function GeneratePDF(ByVal iLoanID As Integer, ByVal sDOB As String) As String


        Dim CrystalReportDocument As ReportDocument
        Dim CrystalExportOptions As ExportOptions
        Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim crTable As Table

        Dim sReportPath As String = MapPath("") & "\PDLLoanAgreement.rpt"
        Trace.Warn(sReportPath)
        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


        Dim Filename, FilenameShort As String
        CrystalReportDocument = New ReportDocument()
        CrystalReportDocument.Load(sReportPath)
        'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
        Dim crTables As Tables = CrystalReportDocument.Database.Tables
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crConnectionInfo.DatabaseName = sSQLServerDB
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

        'SQL SP Parameters
        CrystalReportDocument.SetParameterValue("@ID", iLoanID)

        'Name of the o/p file
        FilenameShort = iLoanID.ToString & "_" & sDOB & ".pdf"
        Filename = MapPath("") & "\Agreements\LoanAgreement_" & FilenameShort



        CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
        CrystalDiskFileDestinationOptions.DiskFileName = Filename
        CrystalExportOptions = CrystalReportDocument.ExportOptions
        With CrystalExportOptions
            .DestinationOptions = CrystalDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With
        CrystalReportDocument.Export()

        Return FilenameShort
        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
        'Response.ContentType = "text/pdf"
        'Response.WriteFile(Filename)
        'Response.Flush()
        'Response.End()
    End Function

    Protected Sub btnReSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReSend.Click
        Try
            GenerateCode(Request.QueryString("LoanID"))
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Private Sub SMSSecurityCode(ByVal iLoanID As Integer)

        Dim Username As String
        Dim Password As String
        Dim Account As String
        Dim Recipient As String = String.Empty
        Dim Body As String
        Dim objConfig As New System.Configuration.AppSettingsReader

        lblMessege.Visible = False
        Try
            Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
            Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
            Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
            Body = CType(Session("SessionUserObj"), PDLSessionUser).SecurityCode.ToString

            Dim LoanDetails As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                With LoanDetails.Rows(0)
                    Recipient = .Item("Mobile")
                End With
            End If

            If Recipient.Length <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "No mobile number found to send the security code"
                Return
            End If

            Dim header As esendex.MessengerHeader

            header = New esendex.MessengerHeader
            header.Username = Username
            header.Password = Password
            header.Account = Account

            Dim service As esendex.SendService

            service = New esendex.SendService
            service.MessengerHeaderValue = header
            service.SendMessage(Recipient, Body, esendex.MessageType.Text)
            objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, 0)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    'Do not generate the CSV automatically and let the scheduled service pich up the record and process
    Private Sub GenerateCSV(ByVal iLoanID As Integer)
        Try
            lblMessege.Visible = False
            Dim MembersCount As Integer = 0
            Dim dtLoan As DataTable

            dtLoan = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

            If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then
                Dim objConfig As New System.Configuration.AppSettingsReader
                'Dim FilePath As String = CType(objConfig.GetValue("CUOKPaymentsCSVFolder", GetType(System.String)), System.String) & "\CUOKPay_" & iLoanID.ToString & ".csv"
                'Dim InputLine As String = ""

                'Using New Impersonator("httpweb", "CUONLINEWEB", "Sadmin*4321")
                'Dim w As IO.StreamWriter
                'w = IO.File.CreateText(FilePath)

                Dim MemberID As Integer = 0
                Dim dr As DataRow = dtLoan.Rows(0)
                Dim PayRef As String = CStr(dr("ID"))
                If Not IsDBNull(dr("MemberID")) AndAlso CInt(dr("MemberID")) > 0 Then
                    PayRef = PayRef & "-" & dr("MemberID")
                    MemberID = dr("MemberID")
                End If

                Dim SortCode As String = dr("AccountSortCodeDec")
                Dim AccNumber As String = dr("AccountNumberDec")
                Dim AccName As String = dr("AccountNameDec")
                Dim LoanAmountInPence As Integer = CInt(dr("LoanAmount")) * 100
                Dim TxnType As Integer = 99
                Dim InstantLoan As Boolean = (dr("InstantLoan") > 0)
                'InputLine = SortCode & "," & _
                '            AccNumber & "," & _
                '            AccName & "," & _
                '            PayRef & "," & _
                '            LoanAmountInPence.ToString & "," & _
                '            TxnType.ToString
                'w.WriteLine(InputLine)


                'w.Flush()
                'w.Close()


                'Insert into the table
                objPayDayLoan.UpdatePreConfirmedLoanReApplyEmailSent(iLoanID, CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0, True)
                objPayDayLoan.InsertPaymentInstruction(iLoanID, MemberID, SortCode, AccNumber, AccName, PayRef, LoanAmountInPence, TxnType, 26, InstantLoan) '26=Online.User Staff Account
            

            'End Using
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try


    End Sub
End Class
