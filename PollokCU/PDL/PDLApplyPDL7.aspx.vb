﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports SageIncludesServer
Imports System.IO
Imports System.Net

Partial Class PDL_PDLApplyPDL7
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Shared objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            'Session("SessionUserObj") = GetSessionDetails("pv1bb5fdwdss4dnaghsod345")
            Response.Redirect("PDLError.aspx?C=E2")
        End If

        If Request.QueryString("LoanID") > 0 Then
            hfLoanID.Value = Request.QueryString("LoanID")

            If objPayDayLoan.GetPDLPaymentSentStatus(Request.QueryString("LoanID")) Then    'When there is a payment recorded against this ID do not proceed
                Response.Redirect("PDLError.aspx?C=E7")
            End If
        Else
            Response.Redirect("PDLApplyPDL1.aspx")
        End If
    End Sub
    Private Function GetSessionDetails(ByVal SessionID As String) As PDLSessionUser
        Dim SessObj As PDLSessionUser = New PDLSessionUser(SessionID, "PCU")
        Return SessObj
    End Function
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click

        Try
            lblMessege.Visible = False

            'Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time
            Dim iUserID As Integer
            Dim normalAmount As String = ""
            Dim noOfPayments As String = ""
            Dim startDate As String = ""
            Dim stDate As DateTime
            Dim nextpayDate As DateTime = DateAdd(DateInterval.Month, 1, Now)

            Dim AppData As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

            If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
                With AppData.Rows(0)
                    iUserID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    normalAmount = .Item("Month1Payment")
                    noOfPayments = .Item("LoanPeriod")
                    If .Item("NextPayDay") IsNot System.DBNull.Value Then Date.TryParse(.Item("NextPayDay"), nextpayDate)
                    'stDate = .Item("NextPayDay")
                End With
                startDate = Format(nextpayDate, "yyyy-MM-dd")

                Dim cartid As String = "PDL-" & iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & iUserID.ToString

                'SendToSageServer(iLoanID)
                'sendToWorldServer(iLoanID)
                RedirectAndPOST(Me.Page, cartid, noOfPayments, normalAmount, normalAmount, startDate)
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnPre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPre.Click
        Response.Redirect("PDLApplyPDL5.aspx?LoanID=" & hfLoanID.Value)
    End Sub

    Protected Sub SendToSageServer(ByVal iLoanID As Integer)
        Dim objSageData As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay

        Dim strVendorTxCode As String

        Dim strPost As String
        Dim strMemberID As String
        Dim strFirstName As String
        Dim strSurName As String
        Dim strEmail As String
        Dim strBillingAddressLine1 As String
        Dim strBillingAddressLine2 As String
        Dim strBillingCity As String
        Dim strBillingPostCode As String
        Dim strBillingCountry As String = "GB"
        Dim strReason As String = "WeeLoan 1p Payement Verification"
        Dim strAmount As String = "0.01"

        Dim AppData As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

        If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
            With AppData.Rows(0)
                strMemberID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                strFirstName = .Item("FirstName")
                strSurName = .Item("Surname")
                strEmail = .Item("Email")
                strBillingAddressLine1 = .Item("CurrentAddressLine1")
                strBillingAddressLine2 = .Item("CurrentAddressLine2")
                strBillingCity = .Item("CurrentCity")
                strBillingPostCode = .Item("CurrentPostCode")
            End With
            Randomize()
            strVendorTxCode = iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & strMemberID

            strPost = "VPSProtocol=" & strProtocol
            strPost = strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
            strPost = strPost & "&Vendor=" & strVendorName
            strPost = strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
            strPost = strPost & "&Amount=" & strAmount '** Formatted to 2 decimal places with leading digit **
            strPost = strPost & "&Currency=" & strCurrency
            strPost = strPost & "&Description=" & strReason
            strPost = strPost & "&NotificationURL=" & strYourSiteFQDN & "/PDL/PDLSageNotificationPageServer.aspx"

            '** Billing Details **
            strPost = strPost & "&BillingSurname=" & URLEncode(strSurName)
            strPost = strPost & "&BillingFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&BillingAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&BillingAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&BillingCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&BillingCountry=" & URLEncode(strBillingCountry)

            '** Delivery Details **
            strPost = strPost & "&DeliverySurname=" & URLEncode(strSurName)
            strPost = strPost & "&DeliveryFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&DeliveryAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&DeliveryAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&DeliveryCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&DeliveryCountry=" & URLEncode(strBillingCountry)

            strPost = strPost & "&CustomerEMail=" & strEmail
            If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"
            strPost = strPost & "&Apply3DSecure=0"
            strPost = strPost & "&Profile=NORMAL"

            Dim objUTFEncode As New UTF8Encoding
            Dim arrRequest As Byte()
            Dim objStreamReq As Stream
            Dim objStreamRes As StreamReader
            Dim objHttpRequest As HttpWebRequest
            Dim objHttpResponse As HttpWebResponse
            Dim objUri As New Uri(SystemURL(strConnectTo, "purchase"))
            Dim strResponse As String
            Dim strPageError As String
            Dim strStatus As String
            Dim strStatusDetail As String
            Dim strVPSTxId As String
            Dim strSecurityKey As String
            Dim strNextURL As String

            objHttpRequest = HttpWebRequest.Create(objUri)
            objHttpRequest.KeepAlive = False
            objHttpRequest.Method = "POST"

            objHttpRequest.ContentType = "application/x-www-form-urlencoded"
            arrRequest = objUTFEncode.GetBytes(strPost)
            objHttpRequest.ContentLength = arrRequest.Length
            objStreamReq = objHttpRequest.GetRequestStream()
            objStreamReq.Write(arrRequest, 0, arrRequest.Length)
            objStreamReq.Close()

            'Get response
            objHttpResponse = objHttpRequest.GetResponse()
            objStreamRes = New StreamReader(objHttpResponse.GetResponseStream(), Encoding.ASCII)

            strResponse = objStreamRes.ReadToEnd()
            objStreamRes.Close()

            If Err.Number <> 0 Then
                '** An non zero Err.number indicates an error of some kind **
                '** Check for the most common error... unable to reach the purchase URL **  
                If Err.Number = -2147012889 Then
                    strPageError = "Your server was unable to register this transaction with Sage Pay." & _
                    "  Check that you do not have a firewall restricting the POST and " & _
                    "that your server can correctly resolve the address " & SystemURL(strConnectTo, "puchase")
                Else
                    strPageError = "An Error has occurred whilst trying to register this transaction.<BR>" & _
                    "The Error Number is: " & Err.Number & "<BR>" & _
                    "The Description given is: " & Err.Description
                End If
                lblMessege.Visible = True
                lblMessege.Text = strPageError

            Else
                strStatus = findField("Status", strResponse)
                strStatusDetail = findField("StatusDetail", strResponse)

                If Left(strStatus, 2) = "OK" Then
                    '** An OK status mean that the transaction has been successfully registered **
                    '** Your code needs to extract the VPSTxId (Sage Pay's unique reference for this transaction) **
                    '** and the SecurityKey (used to validate the call back from Sage Pay later) and the NextURL **
                    '** (the URL to which the customer's browser must be redirected to enable them to pay) **
                    strVPSTxId = findField("VPSTxId", strResponse)
                    strSecurityKey = findField("SecurityKey", strResponse)
                    strNextURL = findField("NextURL", strResponse)

                    'Store details in the database
                    objSageData.CreateSagePayResponseServerRegistration(True, strMemberID, strFirstName, strSurName, strEmail, strBillingAddressLine1, strBillingAddressLine2 _
                                                                        , strBillingCity, strBillingPostCode, strReason _
                                                                        , strVendorTxCode, strStatus, strStatusDetail, strAmount, strVPSTxId, strSecurityKey, strNextURL, iLoanID)

                    '** Finally, if we're not in Simulator Mode, redirect the page to the NextURL **
                    '** In Simulator mode, we allow this page to display and ask for Proceed to be clicked **
                    If strConnectTo <> "SIMULATOR" Then
                        Response.Clear()
                        Response.Redirect(strNextURL)
                        Response.End()
                    Else

                    End If

                ElseIf strStatus = "MALFORMED" Then
                    '** A MALFORMED status occurs when the POST sent above is not correctly formatted **
                    '** or is missing compulsory fields.  You will normally only see these during **
                    '** development and early testing **
                    strPageError = "Sage Pay returned an MALFORMED status. " & _
                    "The POST was Malformed because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError

                ElseIf strStatus = "INVALID" Then
                    '** An INVALID status occurs when the structure of the POST was correct, but **
                    '** one of the fields contains incorrect or invalid data.  These may happen when live **
                    '** but you should modify your code to format all data correctly before sending **
                    '** the POST to Server **
                    strPageError = "Sage Pay returned an INVALID status. " & _
                    "The data sent was Invalid because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                Else
                    '** The only remaining status is ERROR **
                    '** This occurs extremely rarely when there is a system level error at Sage Pay **
                    '** If you receive this status the payment systems may be unavailable **<br>
                    '** You could redirect your customer to a page offering alternative methods of payment here **
                    strPageError = "Sage Pay returned an ERROR status. " & _
                    "The description of the error was """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                End If
            End If
        End If

        
    End Sub

    Protected Sub SendToWorldServer(ByVal iLoanID As Integer)
        Dim objSageData As PollokCU.DataAccess.Layer.clsWorldPay = New PollokCU.DataAccess.Layer.clsWorldPay

        Dim strVendorTxCode As String

        Dim strPost As String
        Dim strMemberID As String
        Dim strFirstName As String
        Dim strSurName As String
        Dim strEmail As String
        Dim strBillingAddressLine1 As String
        Dim strBillingAddressLine2 As String
        Dim strBillingCity As String
        Dim strBillingPostCode As String
        Dim strBillingCountry As String = "GB"
        Dim strReason As String = "handiloan 1p Payement Verification"
        Dim strAmount As String = "0.01"

        Dim AppData As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

        If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
            With AppData.Rows(0)
                strMemberID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                strFirstName = .Item("FirstName")
                strSurName = .Item("Surname")
                strEmail = .Item("Email")
                strBillingAddressLine1 = .Item("CurrentAddressLine1")
                strBillingAddressLine2 = .Item("CurrentAddressLine2")
                strBillingCity = .Item("CurrentCity")
                strBillingPostCode = .Item("CurrentPostCode")
            End With
            Randomize()
            strVendorTxCode = iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & strMemberID

            strPost = "VPSProtocol=" & strProtocol
            strPost = strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
            strPost = strPost & "&Vendor=" & strVendorName
            strPost = strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
            strPost = strPost & "&Amount=" & strAmount '** Formatted to 2 decimal places with leading digit **
            strPost = strPost & "&Currency=" & strCurrency
            strPost = strPost & "&Description=" & strReason
            strPost = strPost & "&NotificationURL=" & strYourSiteFQDN & "WorldPayTest.aspx"

            '** Billing Details **
            strPost = strPost & "&BillingSurname=" & URLEncode(strSurName)
            strPost = strPost & "&BillingFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&BillingAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&BillingAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&BillingCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&BillingCountry=" & URLEncode(strBillingCountry)

            '** Delivery Details **
            strPost = strPost & "&DeliverySurname=" & URLEncode(strSurName)
            strPost = strPost & "&DeliveryFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&DeliveryAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&DeliveryAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&DeliveryCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&DeliveryCountry=" & URLEncode(strBillingCountry)

            strPost = strPost & "&CustomerEMail=" & strEmail
            If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"
            strPost = strPost & "&Apply3DSecure=0"
            strPost = strPost & "&Profile=NORMAL"

            Dim objUTFEncode As New UTF8Encoding
            Dim arrRequest As Byte()
            Dim objStreamReq As Stream
            Dim objStreamRes As StreamReader
            Dim objHttpRequest As HttpWebRequest
            Dim objHttpResponse As HttpWebResponse
            Dim objUri As New Uri(SystemURL(strConnectTo, "purchase"))
            Dim strResponse As String
            Dim strPageError As String
            Dim strStatus As String
            Dim strStatusDetail As String
            Dim strVPSTxId As String
            Dim strSecurityKey As String
            Dim strNextURL As String

            objHttpRequest = HttpWebRequest.Create(objUri)
            objHttpRequest.KeepAlive = False
            objHttpRequest.Method = "POST"

            objHttpRequest.ContentType = "application/x-www-form-urlencoded"
            arrRequest = objUTFEncode.GetBytes(strPost)
            objHttpRequest.ContentLength = arrRequest.Length
            objStreamReq = objHttpRequest.GetRequestStream()
            objStreamReq.Write(arrRequest, 0, arrRequest.Length)
            objStreamReq.Close()

            'Get response
            objHttpResponse = objHttpRequest.GetResponse()
            objStreamRes = New StreamReader(objHttpResponse.GetResponseStream(), Encoding.ASCII)

            strResponse = objStreamRes.ReadToEnd()
            objStreamRes.Close()

            If Err.Number <> 0 Then
                '** An non zero Err.number indicates an error of some kind **
                '** Check for the most common error... unable to reach the purchase URL **  
                If Err.Number = -2147012889 Then
                    strPageError = "Your server was unable to register this transaction with Sage Pay." & _
                    "  Check that you do not have a firewall restricting the POST and " & _
                    "that your server can correctly resolve the address " & SystemURL(strConnectTo, "puchase")
                Else
                    strPageError = "An Error has occurred whilst trying to register this transaction.<BR>" & _
                    "The Error Number is: " & Err.Number & "<BR>" & _
                    "The Description given is: " & Err.Description
                End If
                lblMessege.Visible = True
                lblMessege.Text = strPageError

            Else
                strStatus = findField("Status", strResponse)
                strStatusDetail = findField("StatusDetail", strResponse)

                If Left(strStatus, 2) = "OK" Then
                    '** An OK status mean that the transaction has been successfully registered **
                    '** Your code needs to extract the VPSTxId (Sage Pay's unique reference for this transaction) **
                    '** and the SecurityKey (used to validate the call back from Sage Pay later) and the NextURL **
                    '** (the URL to which the customer's browser must be redirected to enable them to pay) **
                    strVPSTxId = findField("VPSTxId", strResponse)
                    strSecurityKey = findField("SecurityKey", strResponse)
                    strNextURL = findField("NextURL", strResponse)

                    'Store details in the database
                    ' objSageData.CreateSagePayResponseServerRegistration(True, strMemberID, strFirstName, strSurName, strEmail, strBillingAddressLine1, strBillingAddressLine2 _
                    ' , strBillingCity, strBillingPostCode, strReason _
                    ' , strVendorTxCode, strStatus, strStatusDetail, strAmount, strVPSTxId, strSecurityKey, strNextURL, iLoanID)

                    '** Finally, if we're not in Simulator Mode, redirect the page to the NextURL **
                    '** In Simulator mode, we allow this page to display and ask for Proceed to be clicked **
                    If strConnectTo <> "SIMULATOR" Then
                        Response.Clear()
                        Response.Redirect(strNextURL)
                        Response.End()
                    Else

                    End If

                ElseIf strStatus = "MALFORMED" Then
                    '** A MALFORMED status occurs when the POST sent above is not correctly formatted **
                    '** or is missing compulsory fields.  You will normally only see these during **
                    '** development and early testing **
                    strPageError = "Sage Pay returned an MALFORMED status. " & _
                    "The POST was Malformed because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError

                ElseIf strStatus = "INVALID" Then
                    '** An INVALID status occurs when the structure of the POST was correct, but **
                    '** one of the fields contains incorrect or invalid data.  These may happen when live **
                    '** but you should modify your code to format all data correctly before sending **
                    '** the POST to Server **
                    strPageError = "Sage Pay returned an INVALID status. " & _
                    "The data sent was Invalid because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                Else
                    '** The only remaining status is ERROR **
                    '** This occurs extremely rarely when there is a system level error at Sage Pay **
                    '** If you receive this status the payment systems may be unavailable **<br>
                    '** You could redirect your customer to a page offering alternative methods of payment here **
                    strPageError = "Sage Pay returned an ERROR status. " & _
                    "The description of the error was """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                End If
            End If
        End If


    End Sub





    Public Shared Sub RedirectAndPOST(page As Page, cartid As String, noOfPayments As String, normalAmount As String, FirstAmount As String, startDate As String)
        Dim data As New NameValueCollection()
        Dim count As Integer = CInt(noOfPayments)

        data.Add("instId", "1140028")
        data.Add("cartId", cartid)
        data.Add("amount", "0.01")
        data.Add("currency", "GBP")
        data.Add("futurePayType", "regular")
        data.Add("option", "1")
        If noOfPayments > 1 Then
            data.Add("intervalMult", "1")
            data.Add("intervalUnit", "3")
            data.Add("initialAmount", FirstAmount)
        End If
        If noOfPayments >= 1 Then data.Add("noOfPayments", noOfPayments)
        'If (count > 1) Then
        data.Add("normalAmount", normalAmount)
        'Else
        'data.Add("normalAmount", "0.00")
        'End If
        data.Add("startDate", startDate)


        Dim postUrl As String = String.Empty
        'Dim testMode As Boolean = True
        'Set this from Web.Config WorldTestmode
        Dim strConnectTo As String = objConfig.getConfigKey("SagePayEnv")
        If strConnectTo = "LIVE" Then
            postUrl = "https://secure.worldpay.com/wcc/purchase"
            data.Add("testMode", "0")
        Else
            postUrl = "https://secure-test.worldpay.com/wcc/purchase"
            data.Add("testMode", "100")
        End If

        'Prepare the Posting form
        Dim strForm As String = PreparePOSTForm(postUrl, data)


        'Add a literal control the specified page holding the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(New LiteralControl(strForm))
    End Sub

    Private Shared Function PreparePOSTForm(url As String, data As NameValueCollection) As [String]
        'Set a name for the form
        Dim formID As String = "PostForm"

        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append((Convert.ToString((Convert.ToString((Convert.ToString("<form id=""") & formID) + """ name=""") & formID) + """ action=""") & url) + """ method=""POST"">")
        For Each key As String In data
            strForm.Append((Convert.ToString("<input type=""hidden"" name=""") & key) + """ value=""" + data(key) + """>")
        Next
        strForm.Append("</form>")

        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")
        strScript.Append((Convert.ToString((Convert.ToString("var v") & formID) + " = document.") & formID) + ";")
        strScript.Append((Convert.ToString("v") & formID) + ".submit();")
        strScript.Append("</script>")

        'Return the form and the script concatenated. (The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function




End Class
