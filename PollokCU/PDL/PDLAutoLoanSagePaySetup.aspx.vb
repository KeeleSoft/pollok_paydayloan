﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports SageIncludesServer
Imports System.IO
Imports System.Net

Partial Class PDL_PDLAutoLoanSagePaySetup
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objMember As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim LoanID As Integer = Request.QueryString("L")
        hfLoanID.Value = LoanID
        Dim VerifyStatus As Boolean = False
        Dim ForceSagePayAllow As Boolean = False

        If LoanID > 0 Then
            hfLoanID.Value = LoanID
            Dim dtLoan As DataTable = objAutoLoan.GetAutoLoanDetail(LoanID)
            If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then
                If dtLoan.Rows(0).Item("ForceSagePayAllow") IsNot System.DBNull.Value AndAlso Boolean.Parse(dtLoan.Rows(0).Item("ForceSagePayAllow")) Then
                    ForceSagePayAllow = True
                Else
                    ForceSagePayAllow = False
                End If
            End If


            If ForceSagePayAllow Then
                Session("LoanID") = LoanID
                'OK to proceed
            Else
                Response.Redirect("PDLError.aspx?C=E6") 'Bad URL
            End If
        Else
            Response.Redirect("PDLError.aspx?C=E5") 'Bad URL
        End If
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            SendToSageServer(iLoanID)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            btnNext.Enabled = True
        End Try
    End Sub

    Protected Sub SendToSageServer(ByVal iLoanID As Integer)
        Dim objSageData As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay

        Dim strVendorTxCode As String

        Dim strPost As String
        Dim strMemberID As String
        Dim strFirstName As String
        Dim strSurName As String
        Dim strEmail As String
        Dim strBillingAddressLine1 As String
        Dim strBillingAddressLine2 As String
        Dim strBillingCity As String
        Dim strBillingPostCode As String
        Dim strBillingCountry As String = "GB"
        Dim strReason As String = "handiloan ALPS 1p Payement Verification"
        Dim strAmount As String = "0.01"

        Dim AppData As DataTable = objAutoLoan.GetAutoLoanDetail(iLoanID)

        If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
            With AppData.Rows(0)
                strMemberID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                strFirstName = .Item("FirstName")
                strSurName = .Item("Surname")
                strEmail = .Item("Email")
                strBillingAddressLine1 = .Item("CurrentAddressLine1")
                strBillingAddressLine2 = ""
                strBillingCity = .Item("CurrentCity")
                strBillingPostCode = .Item("CurrentPostCode")
            End With
            Randomize()
            strVendorTxCode = iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & strMemberID

            strPost = "VPSProtocol=" & strProtocol
            strPost = strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
            strPost = strPost & "&Vendor=" & strVendorName
            strPost = strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
            strPost = strPost & "&Amount=" & strAmount '** Formatted to 2 decimal places with leading digit **
            strPost = strPost & "&Currency=" & strCurrency
            strPost = strPost & "&Description=" & strReason
            strPost = strPost & "&NotificationURL=" & strYourSiteFQDN & "/AutoLoan/PDLALPSSageNotificationPageServer.aspx"

            '** Billing Details **
            strPost = strPost & "&BillingSurname=" & URLEncode(strSurName)
            strPost = strPost & "&BillingFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&BillingAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&BillingAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&BillingCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&BillingCountry=" & URLEncode(strBillingCountry)

            '** Delivery Details **
            strPost = strPost & "&DeliverySurname=" & URLEncode(strSurName)
            strPost = strPost & "&DeliveryFirstnames=" & URLEncode(strFirstName)
            strPost = strPost & "&DeliveryAddress1=" & URLEncode(strBillingAddressLine1)
            If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&DeliveryAddress2=" & URLEncode(strBillingAddressLine2)
            strPost = strPost & "&DeliveryCity=" & URLEncode(strBillingCity)
            strPost = strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
            strPost = strPost & "&DeliveryCountry=" & URLEncode(strBillingCountry)

            strPost = strPost & "&CustomerEMail=" & strEmail
            If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"
            strPost = strPost & "&Apply3DSecure=0"
            strPost = strPost & "&Profile=NORMAL"

            Dim objUTFEncode As New UTF8Encoding
            Dim arrRequest As Byte()
            Dim objStreamReq As Stream
            Dim objStreamRes As StreamReader
            Dim objHttpRequest As HttpWebRequest
            Dim objHttpResponse As HttpWebResponse
            Dim objUri As New Uri(SystemURL(strConnectTo, "purchase"))
            Dim strResponse As String
            Dim strPageError As String
            Dim strStatus As String
            Dim strStatusDetail As String
            Dim strVPSTxId As String
            Dim strSecurityKey As String
            Dim strNextURL As String

            objHttpRequest = HttpWebRequest.Create(objUri)
            objHttpRequest.KeepAlive = False
            objHttpRequest.Method = "POST"

            objHttpRequest.ContentType = "application/x-www-form-urlencoded"
            arrRequest = objUTFEncode.GetBytes(strPost)
            objHttpRequest.ContentLength = arrRequest.Length
            objStreamReq = objHttpRequest.GetRequestStream()
            objStreamReq.Write(arrRequest, 0, arrRequest.Length)
            objStreamReq.Close()

            'Get response
            objHttpResponse = objHttpRequest.GetResponse()
            objStreamRes = New StreamReader(objHttpResponse.GetResponseStream(), Encoding.ASCII)

            strResponse = objStreamRes.ReadToEnd()
            objStreamRes.Close()

            If Err.Number <> 0 Then
                '** An non zero Err.number indicates an error of some kind **
                '** Check for the most common error... unable to reach the purchase URL **  
                If Err.Number = -2147012889 Then
                    strPageError = "Your server was unable to register this transaction with Sage Pay." & _
                    "  Check that you do not have a firewall restricting the POST and " & _
                    "that your server can correctly resolve the address " & SystemURL(strConnectTo, "puchase")
                Else
                    strPageError = "An Error has occurred whilst trying to register this transaction.<BR>" & _
                    "The Error Number is: " & Err.Number & "<BR>" & _
                    "The Description given is: " & Err.Description
                End If
                lblMessege.Visible = True
                lblMessege.Text = strPageError

            Else
                strStatus = findField("Status", strResponse)
                strStatusDetail = findField("StatusDetail", strResponse)

                If Left(strStatus, 2) = "OK" Then
                    '** An OK status mean that the transaction has been successfully registered **
                    '** Your code needs to extract the VPSTxId (Sage Pay's unique reference for this transaction) **
                    '** and the SecurityKey (used to validate the call back from Sage Pay later) and the NextURL **
                    '** (the URL to which the customer's browser must be redirected to enable them to pay) **
                    strVPSTxId = findField("VPSTxId", strResponse)
                    strSecurityKey = findField("SecurityKey", strResponse)
                    strNextURL = findField("NextURL", strResponse)

                    'Store details in the database
                    objSageData.CreateSagePayResponseServerRegistration(True, strMemberID, strFirstName, strSurName, strEmail, strBillingAddressLine1, strBillingAddressLine2 _
                                                                        , strBillingCity, strBillingPostCode, strReason _
                                                                        , strVendorTxCode, strStatus, strStatusDetail, strAmount, strVPSTxId, strSecurityKey, strNextURL, iLoanID, "PDLALPS")

                    '** Finally, if we're not in Simulator Mode, redirect the page to the NextURL **
                    '** In Simulator mode, we allow this page to display and ask for Proceed to be clicked **
                    If strConnectTo <> "SIMULATOR" Then
                        Response.Clear()
                        Response.Redirect(strNextURL)
                        Response.End()
                    Else

                    End If

                ElseIf strStatus = "MALFORMED" Then
                    '** A MALFORMED status occurs when the POST sent above is not correctly formatted **
                    '** or is missing compulsory fields.  You will normally only see these during **
                    '** development and early testing **
                    strPageError = "Sage Pay returned an MALFORMED status. " & _
                    "The POST was Malformed because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError

                ElseIf strStatus = "INVALID" Then
                    '** An INVALID status occurs when the structure of the POST was correct, but **
                    '** one of the fields contains incorrect or invalid data.  These may happen when live **
                    '** but you should modify your code to format all data correctly before sending **
                    '** the POST to Server **
                    strPageError = "Sage Pay returned an INVALID status. " & _
                    "The data sent was Invalid because """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                Else
                    '** The only remaining status is ERROR **
                    '** This occurs extremely rarely when there is a system level error at Sage Pay **
                    '** If you receive this status the payment systems may be unavailable **<br>
                    '** You could redirect your customer to a page offering alternative methods of payment here **
                    strPageError = "Sage Pay returned an ERROR status. " & _
                    "The description of the error was """ & findField("StatusDetail", strResponse) & """"
                    lblMessege.Visible = True
                    lblMessege.Text = strPageError
                End If
            End If
        End If


    End Sub

End Class
