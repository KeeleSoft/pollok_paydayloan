﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports SageIncludesServer
Imports System.IO
Imports System.Net

Partial Class PDL_PDLAutoLoanWorldPaySetup
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objMember As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember
    Shared objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim LoanID As Integer = Request.QueryString("L")
        hfLoanID.Value = LoanID
        Dim VerifyStatus As Boolean = False
        Dim ForceSagePayAllow As Boolean = False

        If LoanID > 0 Then
            hfLoanID.Value = LoanID
            Dim dtLoan As DataTable = objAutoLoan.GetAutoLoanDetail(LoanID)
            If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then
                If dtLoan.Rows(0).Item("ForceSagePayAllow") IsNot System.DBNull.Value AndAlso Boolean.Parse(dtLoan.Rows(0).Item("ForceSagePayAllow")) Then
                    ForceSagePayAllow = True
                Else
                    ForceSagePayAllow = False
                End If
            End If


            If ForceSagePayAllow Then
                Session("LoanID") = LoanID
                'OK to proceed
            Else
                Response.Redirect("PDLError.aspx?C=E6") 'Bad URL
            End If
        Else
            Response.Redirect("PDLError.aspx?C=E5") 'Bad URL
        End If
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            Dim iUserID As Integer
            Dim normalAmount As String = ""
            Dim noOfPayments As String = ""
            Dim startDate As String = ""
            Dim stDate As DateTime
            Dim nextpayDate As DateTime = DateAdd(DateInterval.Month, 1, Now)

            Dim AppData As DataTable = objAutoLoan.GetAutoLoanDetail(iLoanID)

            If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then
                With AppData.Rows(0)
                    iUserID = If(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    normalAmount = .Item("FinalMonthlyPayment")
                    noOfPayments = .Item("FinalLoanTerm")
                    If .Item("NextPayDay") IsNot System.DBNull.Value Then Date.TryParse(.Item("NextPayDay"), nextpayDate)
                    'stDate = .Item("NextPayDay")
                End With
                startDate = Format(nextpayDate, "yyyy-MM-dd")

                Dim cartid As String = "PDLALPS-" & iLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & iUserID.ToString

                RedirectAndPOST(Me.Page, cartid, noOfPayments, normalAmount, normalAmount, startDate)
            End If
            'SendToSageServer(iLoanID)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            btnNext.Enabled = True
        End Try
    End Sub

    Public Shared Sub RedirectAndPOST(page As Page, cartid As String, noOfPayments As String, normalAmount As String, FirstAmount As String, startDate As String)
        Dim data As New NameValueCollection()
        Dim count As Integer = CInt(noOfPayments)

        data.Add("instId", "1140028")
        data.Add("cartId", cartid)
        data.Add("amount", "0.01")
        data.Add("currency", "GBP")
        data.Add("futurePayType", "regular")
        data.Add("option", "1")
        data.Add("intervalMult", "1")
        data.Add("intervalUnit", "3")
        If noOfPayments > 1 Then data.Add("noOfPayments", noOfPayments)
        'If (count > 1) Then
        data.Add("normalAmount", normalAmount)
        'Else
        'data.Add("normalAmount", "0.00")
        'End If
        data.Add("startDate", startDate)
        data.Add("initialAmount", FirstAmount)

        Dim postUrl As String = String.Empty
        'Dim testMode As Boolean = True 
        'Set this from Web.Config SageTestmode

        Dim strConnectTo As String = objConfig.getConfigKey("SagePayEnv")
        If strConnectTo = "LIVE" Then
            postUrl = "https://secure.worldpay.com/wcc/purchase"
            data.Add("testMode", "0")
        Else
            postUrl = "https://secure-test.worldpay.com/wcc/purchase"
            data.Add("testMode", "100")
        End If

        'Prepare the Posting form
        Dim strForm As String = PreparePOSTForm(postUrl, data)


        'Add a literal control the specified page holding the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(New LiteralControl(strForm))
    End Sub

    Private Shared Function PreparePOSTForm(url As String, data As NameValueCollection) As [String]
        'Set a name for the form
        Dim formID As String = "PostForm"

        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append((Convert.ToString((Convert.ToString((Convert.ToString("<form id=""") & formID) + """ name=""") & formID) + """ action=""") & url) + """ method=""POST"">")
        For Each key As String In data
            strForm.Append((Convert.ToString("<input type=""hidden"" name=""") & key) + """ value=""" + data(key) + """>")
        Next
        strForm.Append("</form>")

        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")
        strScript.Append((Convert.ToString((Convert.ToString("var v") & formID) + " = document.") & formID) + ";")
        strScript.Append((Convert.ToString("v") & formID) + ".submit();")
        strScript.Append("</script>")

        'Return the form and the script concatenated. (The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function
End Class
