﻿<%@ Page Language="VB" MasterPageFile="~/PDL/PDLMasterPage2.master" AutoEventWireup="false" CodeFile="PDLError.aspx.vb" Inherits="PDL_PDLError" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlE1" runat="server">
                                Unknown Credit Union. Please visit the main Credit Union website and click on 
                                the appropriate link to access this facility.
                            </asp:Panel>
                            <asp:Panel ID="pnlE2" runat="server">
                                Session expired. Please visit the main web site to re-apply.
                            </asp:Panel>
                             <asp:Panel ID="PnlE9" runat="server">
                                Transaction already processed.
                            </asp:Panel>
                            <asp:Panel ID="pnlE3" runat="server">
                                Thank you for your time and you may close your browser now.
                            </asp:Panel>
                            <asp:Panel ID="pnlE4" runat="server">
                                Your post code is not within our coverage area. Therefore we cannot proceed with the loan application
                            </asp:Panel>
                            <asp:Panel ID="pnlE5" runat="server">
                                Invalid link. Please check your link or contact PCU.
                            </asp:Panel>
                            <asp:Panel ID="pnlE6" runat="server">
                                Your link is expired or invalid link. Please contact PCU.
                            </asp:Panel>
                            <asp:Panel ID="pnlE7" runat="server">
                                Could not make any payments for this application. Please contact PCU.
                            </asp:Panel>
                            <asp:Panel ID="PnlE8" runat="server">
                                Could not process the payment verification with WorldPay.
                            </asp:Panel>
                            <asp:Panel ID="pnlUnknown" runat="server">
                                Unknown error occured.
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

