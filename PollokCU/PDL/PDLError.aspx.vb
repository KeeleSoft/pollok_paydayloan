﻿
Partial Class PDL_PDLError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ErrorCode As String = Request.QueryString("C")

        pnlE1.Visible = False
        pnlE2.Visible = False
        pnlE3.Visible = False
        pnlE4.Visible = False
        pnlE5.Visible = False
        pnlE6.Visible = False
        pnlE7.Visible = False
        PnlE8.Visible = False
        PnlE9.Visible = False
        pnlUnknown.Visible = False

        Select Case ErrorCode
            Case "E1"
                pnlE1.Visible = True
            Case "E2"
                pnlE2.Visible = True
            Case "E3"
                pnlE3.Visible = True
            Case "E4"
                pnlE4.Visible = True
            Case "E5"
                pnlE5.Visible = True
            Case "E6"
                pnlE6.Visible = True
            Case "E7"
                pnlE7.Visible = True
            Case "E8"
                PnlE8.Visible = True
            Case "E9"
                PnlE9.Visible = True
            Case Else
                pnlUnknown.Visible = True
        End Select
    End Sub
End Class

