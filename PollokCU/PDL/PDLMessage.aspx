﻿<%@ Page Language="VB" MasterPageFile="~/PDL/PDLMasterPage2.master" AutoEventWireup="false" CodeFile="PDLMessage.aspx.vb" Inherits="PDL_PDLMessage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table style="width:100%;">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                        <asp:Panel ID="pnlMsg1" runat="server">
                                Thank you for applying for the Pollok Credit Union’s pay day loan. <br /><br />
                                Congratulations, your pay day loan application was submitted successfully. If 
                                the Faster Payment facility was selected in your application, we will action the 
                                transfer of your loan amount to your bank account within twelve hours from you 
                                receiving this message. If you did not opt for the Faster Payment facility, the 
                                transfer of your loan amount may take up to 3 working days to reach your 
                                account.<br /><br />
                                Your loan agreement with the terms and conditions will be emailed to you immediately. If you have any queries please contact us via email to <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com<</a><br /><br />
                                Once again we thank you for applying for our pay day loan loan, an ethical and 
                                truly low cost payday loan.
                        </asp:Panel>
                        <asp:Panel ID="pnlMsg2" runat="server">
                                Thank you for applying for the Pollok City Credit Union’s pay day Wee Glasgow loan. <br /><br />
                                Unfortunately, we are unable to process your application at this time. This may be due to one or more verification failures from the information you have provided to us. Your application will be investigated further by our loan department team and we will get in touch with you by phone or email within 3 working days. We may request further information from you such as recent payslips and bank statements to assist us in your application.<br /><br />
                                If you have any queries please contact us via email to <a href="#">theweeloan@pollokcu.com</a><br /><br />
                                We thank you for applying for our WeeLoan, an ethical and truly low cost payday loan.
                        </asp:Panel>
                        <asp:Panel ID="pnlMsg3" runat="server">
                                Thank you for applying for the Pollok Credit Union’s WeeLoan. <br /><br />
                                Unfortunately, we regret to inform you that we are unable to process your application at this time. Using the information that you have provided to us on your application, it was found that you do not qualify for our weeglasgow loan.<br /><br /> For more information on Credit Unions and to find your local Credit Union, please visit the website for Association of British Credit Unions (ABCUL) on: <a href="http://www.abcul.org/home">http://www.abcul.org/home </a>
                                <br />
                                <br />
                                We thank you for applying for our weeglasgow loan, an ethical and truly low cost payday loan.
                        </asp:Panel>
                        <asp:Panel ID="pnlM1" runat="server">
                                Thank you for applying for Pollok Credit Union payday loan. 
                                Congratulations, you have succeeded your application. If you have requested a 
                                Faster Payment facility we will credit your account on our next payment cycle. 
                                We will also email all the legal papers. If you have any queries please email us 
                                on <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com<</a>
                            </asp:Panel>     
                            <asp:Panel ID="pnlM2" runat="server">
                                Thank you for your WeeLoan application. However your security code did not match within the 3 attempts given. Please contact us.
                            </asp:Panel>
                            <asp:Panel ID="pnlM3" runat="server">
                                Thank you for setting up WorldPay payments.
                            </asp:Panel> 
                            <asp:Panel ID="pnlM4" runat="server">
                                You are not currently set-up to use this service.  Please contact PCU.
                            </asp:Panel>                         
                            <asp:Panel ID="pnlUnknown" runat="server">
                                Unknown message.
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

