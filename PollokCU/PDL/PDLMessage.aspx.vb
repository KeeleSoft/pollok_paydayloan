﻿
Partial Class PDL_PDLMessage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MessageCode As String = Request.QueryString("M")

        pnlMsg1.Visible = False
        pnlMsg2.Visible = False
        pnlMsg3.Visible = False
        pnlM1.Visible = False
        pnlM2.Visible = False
        pnlM3.Visible = False
        pnlM4.Visible = False
        pnlUnknown.Visible = False

        Select Case MessageCode
            Case "MSG1"
                pnlMsg1.Visible = True
                Session("MemberID") = Nothing
                Session("SessionUserObj") = Nothing

            Case "MSG2"
                pnlMsg2.Visible = True
            Case "MSG3"
                pnlMsg3.Visible = True
            Case "M1"
                pnlM1.Visible = True
            Case "M2"
                pnlM2.Visible = True
            Case "M3"
                pnlM3.Visible = True
            Case "M4"
                pnlM4.Visible = True
            Case Else
                pnlUnknown.Visible = True

        End Select

        Session.Abandon()
    End Sub
End Class
