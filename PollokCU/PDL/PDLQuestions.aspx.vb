﻿
Partial Class PDL_PDLQuestions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("SessionUserObj") Is Nothing Then
            Response.Redirect("PDLError.aspx?C=E2")
        End If
    End Sub

    Protected Sub btnQ0Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQ0Next.Click
        Try
            lblMessege.Visible = False

            If radQ0.SelectedValue = "Yes" Then
                pnlQ0.Visible = False
                pnlQ0Yes.Visible = True
            Else
                pnlQ0.Visible = False
                pnlQ2.Visible = True
            End If

        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub lbLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbLogin.Click
        Try
            Response.Redirect("../Login.aspx?From=PDL")
        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnQ1Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQ1Next.Click
        Try
            lblMessege.Visible = False

            If radQ1.SelectedValue = "Yes" Then
                pnlQ1.Visible = False
                pnlQ3.Visible = True
            Else
                pnlQ1.Visible = False
                pnlQ1No.Visible = True
            End If

        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnQ2Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQ2Next.Click
        Try
            lblMessege.Visible = False

            pnlQ1.Visible = False

            If radQ2.SelectedValue = "Yes" Then
                Response.Redirect("PDLApplyPDL2.aspx")
            Else
                pnlQ2.Visible = False
                pnlQ1.Visible = True
            End If

        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnQ3Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQ3Next.Click
        Try
            lblMessege.Visible = False

            pnlQ1.Visible = False
            pnlQ2.Visible = False
            pnlQ3.Visible = False
            pnlQ3Answer.Visible = True
            If radQ3.SelectedValue = "Yes" Then
                litQ3Answer.Text = "Once you have submitted your on-line loan application, please forward original of your most recent payslip, last two month bank statements and proof of address to Pollok Credit Union, Loan Department, 79 Denmark Hill, Camberwell, London SE5 8RS, once we have received all the required documentation, you will be contacted with a loan decision within 10 working days"
            Else
                litQ3Answer.Text = "Once you have submitted your on-line loan application, please forward the original of a proof of address, (no older than 3 months) to Pollok Credit Union, Loan Department, 79 Denmark Hill, Camberwell, London SE5 8RS. A proof of address can be a benefit letter or utility bill, once you have provided the required documentation; you will be contacted with a loan decision within 10 working days"
            End If



        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnQ3AnswerNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQ3AnswerNext.Click
        Try
            lblMessege.Visible = False

            pnlQ1.Visible = False
            pnlQ2.Visible = False
            pnlQ3.Visible = False

            If Not chkAgree.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please agree to send necessary documents in order to proceed with the loan"
            Else
                Response.Redirect("PDLApplyPDL2.aspx")
            End If
        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub


End Class
