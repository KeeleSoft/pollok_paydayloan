﻿Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Partial Class PDL_PDLReConfirm
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objMember As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SessionID As String = Request.QueryString("S")
        Dim LoanID As Integer = Request.QueryString("L")
        hfLoanID.Value = LoanID
        Dim VerifyStatus As Boolean = False
        Dim CUCAButPayingFromDiffAcc As Boolean = False

        If SessionID IsNot Nothing AndAlso SessionID.Length > 0 AndAlso LoanID > 0 Then
            VerifyStatus = objPayDayLoan.GetReConfirmPDLVerifyStatus(SessionID, LoanID)
            If Not VerifyStatus Then
                Dim dtLoan As DataTable = objPayDayLoan.SelectPayDayLoanDetail(LoanID)
                If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then
                    If dtLoan.Rows(0).Item("CUCAButPayingDiffAccount") IsNot System.DBNull.Value AndAlso Boolean.Parse(dtLoan.Rows(0).Item("CUCAButPayingDiffAccount")) Then
                        CUCAButPayingFromDiffAcc = True
                    Else
                        CUCAButPayingFromDiffAcc = False
                    End If
                End If
            End If

            If VerifyStatus OrElse CUCAButPayingFromDiffAcc Then
                Session("LoanID") = LoanID
                'OK to proceed
                Session("SessionUserObj") = GetSessionDetails(SessionID)
                If Not IsPostBack Then
                    PopulateControls(LoanID)
                End If
            Else
                Response.Redirect("PDLError.aspx?C=E6") 'Bad URL
            End If
        Else
            Response.Redirect("PDLError.aspx?C=E5") 'Bad URL
        End If
    End Sub

    Private Function GetSessionDetails(ByVal SessionID As String) As PDLSessionUser
        Dim SessObj As PDLSessionUser = New PDLSessionUser(SessionID, "LCCU")
        Return SessObj
    End Function
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            If Session("LoanID") IsNot Nothing AndAlso Session("LoanID") > 0 Then
                If objPayDayLoan.GetPDLPaymentSentStatus(Session("LoanID")) Then    'When there is a payment recorded against this ID do not proceed
                    Response.Redirect("PDLError.aspx?C=E7")
                End If
            Else
                Response.Redirect("PDLError.aspx?C=E6")
            End If

            If Not chkAgree1.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the application summary"
                Exit Sub
            End If

            If Not chkAgree2.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the declaration"
                Exit Sub
            End If

            If Not chkAgree3.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the consent"
                Exit Sub
            End If

            If Not chkAgree4.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the loan agreement"
                Exit Sub
            End If

            If Not chkAgree5.Checked Then
                lblMessege.Visible = True
                lblMessege.Text = "Please read and agree to the terms and conditions"
                Exit Sub
            End If

            Dim iUserID As Integer = IIf(Session("MemberID") > 0, Session("MemberID"), 1)
            Dim iLoanID As Integer = hfLoanID.Value 'This will be 0 for the 1st time

            'Fields available in this form
            Dim CompletedLevel As Int16 = 4 'Current page identifier
            Dim SalutationID As Int32 = 0
            Dim FirstName As String = String.Empty
            Dim SurName As String = String.Empty
            Dim MiddleName As String = String.Empty
            Dim Gender As String = String.Empty
            Dim DateOfBirth As DateTime = Nothing
            Dim PlaceOfBirth As String = String.Empty
            Dim MotherName As String = String.Empty
            Dim CurrentAddressLine1 As String = String.Empty
            Dim CurrentAddressLine2 As String = String.Empty
            Dim CurrentCity As String = String.Empty
            Dim CurrentCounty As String = String.Empty
            Dim CurrentPostCode As String = String.Empty
            Dim CurrentAddressNoOfYears As Int32 = 0
            Dim CurrentAddressNoOfMonths As Int32 = 0
            Dim HomeTelephone As String = String.Empty
            Dim Mobile As String = String.Empty
            Dim Email As String = String.Empty
            Dim NINumber As String = String.Empty
            Dim MaritalStatus As Integer = 0
            Dim NoOfDependants As Int32 = 0
            Dim PreviousAddressLine1 As String = String.Empty
            Dim PreviousAddressLine2 As String = String.Empty
            Dim PreviousCity As String = String.Empty
            Dim PreviousCounty As String = String.Empty
            Dim PreviousPostCode As String = String.Empty
            Dim ResidencyStatus As Integer = 0
            Dim MemberID As String = IIf(Session("MemberID") > 0, Session("MemberID"), 0)

            Dim TermsAccepted As Boolean = True ' Will not reach here if not agreed yet
            Dim NormalOccupation As Boolean = IIf(radNormalOccupation.SelectedIndex = 0, True, False)
            Dim GoodHealth As Boolean = IIf(radGoodHealth.SelectedIndex = 0, True, False)

            Dim ApplicantType As Integer = Session("ApplicantTypeID")
            Dim dtApplicantTypeDetails As DataTable = GetApplicantTypeDetails(Session("ApplicantTypeID"))
            Trace.Warn(ApplicantType.ToString)

            Dim CUCACustomer As Boolean = False
            Dim PayrollCustomer As Boolean = False
            Dim CUCAButPayingFromDiffAcc As Boolean = False
            Dim Epic360 As Boolean = False

            If chkAgree6.Checked Then
                Epic360 = True
            End If
            btnNext.Enabled = False

            objPayDayLoan.InsertUpdateApplication("PayDayLoan" _
                                    , CType(Session("SessionUserObj"), PDLSessionUser).SSID _
                                    , MemberID _
                                    , CompletedLevel _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , 0 _
                                    , iUserID _
                                    , iLoanID _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , _
                                    , TermsAccepted _
                                    , _
                                    , _
                                    , GoodHealth _
                                    , NormalOccupation _
                                    , _
                                    , ApplicantType _
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    ,
                                    , Epic360)
            If iLoanID > 0 Then

                'Do the final checks
                Dim AllChecksPassed As Boolean = False
                Dim AuthenticationPassed As Int16 = 0
                Dim CreditCheckPassed As Int16 = 0
                Dim BankCheckPassed As Int16 = 0
                Dim AccountActiveCheckPassed As Int16 = 0
                Dim EmploymentCheckPassed As Int16 = 0
                Dim PreviousPDLCheckPassed As Int16 = 0

                Dim AllowFasterPaymentForApplicantType As Boolean = False
                Dim MessageCode As String = "MSG2"


                If ApplicantType > 0 AndAlso dtApplicantTypeDetails IsNot Nothing AndAlso dtApplicantTypeDetails.Rows.Count > 0 Then
                    AllowFasterPaymentForApplicantType = CBool(dtApplicantTypeDetails.Rows(0).Item("AllowInstantPayment"))
                End If

                'End of all the checks

                'Generate the PDF file and save in the reports folder
                Dim sPDFName As String
                Dim dtLoan As DataTable

                dtLoan = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)

                If dtLoan.Rows.Count > 0 Then
                    FirstName = dtLoan.Rows(0).Item("FirstName")
                    SurName = dtLoan.Rows(0).Item("Surname")
                    DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")
                    Email = dtLoan.Rows(0).Item("Email")
                    If dtLoan.Rows(0).Item("AccountSortCodeDec") IsNot System.DBNull.Value AndAlso (dtLoan.Rows(0).Item("AccountSortCodeDec") = "089401" OrElse dtLoan.Rows(0).Item("AccountSortCodeDec") = "089409") Then
                        CUCACustomer = True
                    Else
                        CUCACustomer = False
                    End If

                    If dtLoan.Rows(0).Item("CompanyID") IsNot System.DBNull.Value AndAlso dtLoan.Rows(0).Item("CompanyID") > 0 Then
                        PayrollCustomer = True
                    Else
                        PayrollCustomer = False
                    End If

                    If dtLoan.Rows(0).Item("CUCAButPayingDiffAccount") IsNot System.DBNull.Value AndAlso Boolean.Parse(dtLoan.Rows(0).Item("CUCAButPayingDiffAccount")) Then
                        CUCAButPayingFromDiffAcc = True
                    Else
                        CUCAButPayingFromDiffAcc = False
                    End If
                End If

                sPDFName = GeneratePDF(iLoanID, SurName, DateOfBirth.ToShortDateString.Replace("/", ""))
                Dim sPDFURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "Reports/" & sPDFName

                'If AllowFasterPaymentForApplicantType Then
                Dim LoanDetails As DataTable = objPayDayLoan.GetPreconfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0)
                If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                    'If LoanDetails.Rows(0).Item("InstantLoan") > 0 Then
                    If CUCACustomer AndAlso Not CUCAButPayingFromDiffAcc Then
                        Response.Redirect("PDLApplyPDL6.aspx?LoanID=" & iLoanID.ToString)   'Got to SMS Page
                    Else
                        Response.Redirect("PDLApplyPDL7.aspx?LoanID=" & iLoanID.ToString)   'Got to Sage Page
                    End If
                    'Else
                    '    If Email.Length > 0 Then SendAgreementEmail(iLoanID.ToString, Email, FirstName, SurName, DateOfBirth.ToShortDateString.Replace("/", ""), CUCACustomer, PayrollCustomer)
                    '    Response.Redirect("PDLMessage.aspx?M=MSG1")
                    'End If
                End If
                'Else
                '    If Email.Length > 0 Then SendAgreementEmail(iLoanID.ToString, Email, FirstName, SurName, DateOfBirth.ToShortDateString.Replace("/", ""), CUCACustomer, PayrollCustomer)
                '    Response.Redirect("PDLMessage.aspx?M=MSG1")
                'End If
                'lblMessege.Visible = True
                'lblMessege.Text = "Testing Message: No Errors. Happy. AuthenticationPassed=" & AuthenticationPassed.ToString
            Else
                lblMessege.Visible = True
                lblMessege.Text = "Cannot proceed with the loan, Technical failure."
            End If


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            btnNext.Enabled = True
        End Try
    End Sub

    Private Function SendAgreementEmail(ByVal LoanID As String, ByVal EmailTo As String, ByVal FirstName As String, ByVal LastName As String, ByVal Dob As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As Boolean
        Try
            Dim sEmailBody As String
            Dim sEmailTo, sEmailFrom, sSubject As String
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
            Dim fileList As List(Of String) = New List(Of String)

            sEmailTo = EmailTo
            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "WeeLoans"

            Dim FilenameShort As String = LoanID & "_" & Dob & ".pdf"

            Dim sPDFURL As String = objConfig.getConfigKey("PDL.Agreement.Location") & "LoanAgreement_" & FilenameShort
            fileList.Add(sPDFURL)

            'If Not CUCACustomer AndAlso Payroll Then
            '    Dim sPayrollForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "PayrollDeductionForm.pdf"
            '    fileList.Add(sPayrollForm)
            'ElseIf Not CUCACustomer Then
            '    Dim sDDForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "DIRECT_DEBIT_FORM.pdf"
            '    fileList.Add(sDDForm)
            'End If

            sEmailBody = GetPDLCustomerEmailBody(FirstName, LastName, CUCACustomer, Payroll)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , sEmailTo _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "" _
                                      , fileList)

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Private Function GetPDLCustomerEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Congratulations, your WeeGlasgowLoan application was submitted successfully. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount to your bank account within twelve hours from you receiving this message. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")

            If CUCACustomer Then
                body.AppendLine("As selected by you during the application process, a standing order will be setup from your Credit Union Current Account (CUCA) to be your loan repayment, the details of which is outlined in the loan agreement attached to this email. Please ensure enough funds are available in your CUCA to meet the scheduled loan repayments standing order. Failing to make your loan repayments will lead to your personal credit record to be affected negatively.")
                body.AppendLine("<br/><br/>")
                'ElseIf Payroll Then
                '    body.AppendLine("<strong>How to repay your loan by payroll deduction:</strong>")
                '    body.AppendLine("<br/><br/>")
                '    body.AppendLine("<strong>Please find attached to this email a payroll deduction form. This must be printed out, filled in and signed. Please send the completed direct payroll deduction form back to us as soon as possible to setup your loan repayments. Alternatively you can visit any one of our branches to complete a payroll deduction form. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected.</span></strong>")
                '    body.AppendLine("<br/><br/>")
            Else
                body.AppendLine("<strong>You have successfully setup your loan repayments to be taken from your chosen account as a card payment, with the first repayment date stated within your loan agreement which is attached. <span style=""color: #FF3300"">Please ensure that enough funds is available in your bank account when your loan repayment is due. Failing to meet your loan repayments will lead to your personal credit record to be negatively affected.</span></strong>")
                body.AppendLine("<br/><br/>")
            End If

            body.AppendLine("For all contact details, branch locations with opening times and further information, please visit our website at www.pollokcu.com ")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries please contact us via email to theweeloan@pollokcu.com or call us on 0141 881 8731.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our weeglasgow loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function


    Private Function SendUnsuccessfulEmail(ByVal LoanID As String, ByVal EmailTo As String, ByVal FirstName As String, ByVal LastName As String) As Boolean
        Try
            Dim sEmailBody As String
            Dim sEmailTo, sEmailFrom, sSubject As String
            Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

            sEmailTo = EmailTo
            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "PCU - WeeGlasgow Loan"

            sEmailBody = GetPDLUnSuccessfulEmailBody(FirstName, LastName)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , sEmailTo _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "")

            Return True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return False
        End Try
    End Function

    Private Function GetPDLUnSuccessfulEmailBody(ByVal FirstName As String, ByVal LastName As String) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for applying for the Pollok City Credit Union’s weeglasgow loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Unfortunately, we are unable to process your application using our automated system at this time. Your application will be investigated further by our loan department team and <span style=""text-decoration: underline"">we will get in touch with you by phone or email within 3 working days</span>. We may also request further information from you such as recent payslips and bank statements to assist us in your application.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email to: theweeloan@pollokcu.com")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our weeglasgow loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/>")
            body.AppendLine("Main line: 0141 881 8731")
            body.AppendLine("<br/>")
            body.AppendLine("Email: theweeloan@pollokcu.com")
            body.AppendLine("<br/>")
            body.AppendLine("Web: www.pollokcu.com")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Private Sub PopulateControls(ByVal iLoanID As Integer)
        Try
            Dim LoanDetails As DataTable = objPayDayLoan.GetPreconfirmedLoan(CType(Session("SessionUserObj"), PDLSessionUser).SSID, 0)
            If LoanDetails IsNot Nothing AndAlso LoanDetails.Rows.Count > 0 Then
                With LoanDetails.Rows(0)
                    litLoanAmount.Text = Format(.Item("LoanAmount"), "c")
                    litRepayPeriod.Text = .Item("LoanPeriod")
                    litInterestDesc.Text = "Total interest at " & Format(CType(Session("SessionUserObj"), PDLSessionUser).LoanAPR, "0.0") & "% APR"
                    litInterestTotal.Text = Format(.Item("TotalInterest"), "c")
                    litSameDayPay.Text = Format(.Item("InstantLoan"), "c")
                    If .Item("InstantLoan") > 0 Then
                        litHowDeposit.Text = "Faster Payment"
                    Else
                        litHowDeposit.Text = "BACS"
                    End If
                    Dim nextpayDate As DateTime = Now
                    Date.TryParse(.Item("NextPayDay"), nextpayDate)
                    litFirstPaymentDesc.Text = "Your first installment will come out from your current account on " & nextpayDate.ToShortDateString & "."

                    Dim FirstInstallment As Double = .Item("Month1Payment")
                    litFirstPayment.Text = Format(FirstInstallment, "c")
                End With

            End If

            Dim dtApp As DataTable = objPayDayLoan.SelectPayDayLoanDetail(iLoanID)
            If dtApp.Rows.Count > 0 Then
                Dim DOB As String = CType(dtApp.Rows(0).Item("DateOfBirth"), DateTime).ToShortDateString.Replace("/", "")
                hlAgreementLink1.NavigateUrl = "Agreements/LoanAgreement_" & hfLoanID.Value.ToString & "_" & DOB & ".pdf"
                hlLoanAgreementCheck.NavigateUrl = "Agreements/LoanAgreement_" & hfLoanID.Value.ToString & "_" & DOB & ".pdf"
                If dtApp.Rows(0).Item("MemberID") IsNot System.DBNull.Value Then
                    Session("MemberID") = dtApp.Rows(0).Item("MemberID")
                End If

                Session("ApplicantTypeID") = dtApp.Rows(0).Item("ApplicantTypeID")

                Dim CUCACustomer As Boolean
                If dtApp.Rows(0).Item("AccountSortCodeDec") IsNot System.DBNull.Value AndAlso (dtApp.Rows(0).Item("AccountSortCodeDec") = "089401" OrElse dtApp.Rows(0).Item("AccountSortCodeDec") = "089409") Then
                    CUCACustomer = True
                    SagePayDescRow.Visible = False
                Else
                    CUCACustomer = False
                    SagePayDescRow.Visible = True
                End If
            Else
                hlAgreementLink1.NavigateUrl = "#"
                hlLoanAgreementCheck.NavigateUrl = "#"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try

    End Sub

    Private Function GeneratePDF(ByVal iLoanID As Integer, ByVal sSurName As String, ByVal sDOB As String) As String

        Try
            Dim CrystalReportDocument As ReportDocument
            Dim CrystalExportOptions As ExportOptions
            Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
            Dim crtableLogoninfo As New TableLogOnInfo()
            Dim crConnectionInfo As New ConnectionInfo()
            Dim crTable As Table

            Dim sReportPath As String = MapPath("") & "\PDLApplication.rpt"
            Trace.Warn(sReportPath)
            Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
            Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
            Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
            Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


            Dim Filename, FilenameShort As String
            CrystalReportDocument = New ReportDocument()
            CrystalReportDocument.Load(sReportPath)
            'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
            Dim crTables As Tables = CrystalReportDocument.Database.Tables
            For Each crTable In crTables
                crtableLogoninfo = crTable.LogOnInfo
                crConnectionInfo.ServerName = sSQLServerName
                crConnectionInfo.UserID = sSQLServerUserID
                crConnectionInfo.Password = sSQLServerPassword
                crConnectionInfo.DatabaseName = sSQLServerDB
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                crTable.ApplyLogOnInfo(crtableLogoninfo)
            Next
            CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

            'SQL SP Parameters
            CrystalReportDocument.SetParameterValue("@ID", iLoanID)

            'Name of the o/p file
            FilenameShort = iLoanID.ToString & "_" & sSurName & "_" & sDOB & ".pdf"
            Filename = MapPath("") & "\PDLApps\" & FilenameShort



            CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
            CrystalDiskFileDestinationOptions.DiskFileName = Filename
            CrystalExportOptions = CrystalReportDocument.ExportOptions
            With CrystalExportOptions
                .DestinationOptions = CrystalDiskFileDestinationOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With
            CrystalReportDocument.Export()

            Return FilenameShort
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
            'Response.ContentType = "text/pdf"
            'Response.WriteFile(Filename)
            'Response.Flush()
            'Response.End()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try

    End Function

    Private Function GetApplicantTypeDetails(ByVal ApplicantType As Integer) As DataTable
        Try
           
            Dim dtApplicantTypeDetails As DataTable = Nothing

            If ApplicantType > 0 Then
                dtApplicantTypeDetails = objPayDayLoan.GetApplicantTypeDetails(ApplicantType)
            End If
            Return dtApplicantTypeDetails
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
            Return Nothing
        End Try
    End Function

End Class
