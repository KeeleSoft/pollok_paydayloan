Imports SageIncludesServer
Imports System.Web.Security.FormsAuthentication
Partial Class PDLSageNotificationPageServer
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    '**************************************************************************************************
    ' Description
    ' ===========
    '
    ' This page handles the notification POSTs from Server.  It should be made externally visible
    ' so that Server can send messages to over either HTTP or HTTPS.
    ' The code validates the Server POST using MD5 hashing, updates the database accordingly,
    ' and replies with a RedirectURL to which Server will send your customer.  This is normally your
    ' order completion page, or a page to handle failures or cancellations.
    '**************************************************************************************************
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strStatus As String
        Dim strVendorTxCode As String
        Dim strVPSTxId As String
        Dim strSecurityKey As String
        Dim strSQL As String
        Dim strStatusDetail As String
        Dim strTxAuthNo As String
        Dim strAVSCV2 As String
        Dim strAddressResult As String
        Dim strPostCodeResult As String
        Dim strCV2Result As String
        Dim strGiftAid As String
        Dim str3DSecureStatus As String
        Dim strCAVV As String
        Dim strAddressStatus As String
        Dim strPayerStatus As String
        Dim strCardType As String
        Dim strLast4Digits As String
        Dim strMySignature As String
        Dim strVPSSignature As String
        Dim strMessage As String
        Dim strDBStatus As String
        Dim strRedirectPage As String

        Try


            strStatus = cleanInput(Request.Form("Status"), "Text")
            strVendorTxCode = cleanInput(Request.Form("VendorTxCode"), "VendorTxCode")
            strVPSTxId = cleanInput(Request.Form("VPSTxId"), "Text")

            Dim strLoanID As String = ""
            If Not String.IsNullOrEmpty(strVendorTxCode) Then
                Dim vendorCodeParts() As String = strVendorTxCode.Split("-")
                If vendorCodeParts.Count > 0 Then
                    strLoanID = vendorCodeParts(0)
                End If
            End If
            '** We've found the order in the database, so now we can validate the message **
            '** First blank out our result variables **
            strStatusDetail = ""
            strTxAuthNo = ""
            strAVSCV2 = ""
            strAddressResult = ""
            strPostCodeResult = ""
            strCV2Result = ""
            strGiftAid = ""
            str3DSecureStatus = ""
            strCAVV = ""
            strAddressStatus = ""
            strPayerStatus = ""
            strCardType = ""
            strLast4Digits = ""
            strMySignature = ""

            '** Now get the VPSSignature value from the POST, and the StatusDetail in case we need it **
            strVPSSignature = cleanInput(Request.Form("VPSSignature"), "Text")
            strStatusDetail = cleanInput(Request.Form("StatusDetail"), "Text")

            '** Retrieve the other fields, from the POST if they are present **
            If Len(Request.Form("TxAuthNo")) > 0 Then strTxAuthNo = cleanInput(Request.Form("TxAuthNo"), "Number")
            If Len(Request.Form("AVSCV2")) > 0 Then strAVSCV2 = cleanInput(Request.Form("AVSCV2"), "Text")
            If Len(Request.Form("AddressResult")) > 0 Then strAddressResult = cleanInput(Request.Form("AddressResult"), "Text")
            If Len(Request.Form("PostCodeResult")) > 0 Then strPostCodeResult = cleanInput(Request.Form("PostCodeResult"), "Text")
            If Len(Request.Form("CV2Result")) > 0 Then strCV2Result = cleanInput(Request.Form("CV2Result"), "Text")
            If Len(Request.Form("GiftAid")) > 0 Then strGiftAid = cleanInput(Request.Form("GiftAid"), "Number")
            If Len(Request.Form("3DSecureStatus")) > 0 Then str3DSecureStatus = cleanInput(Request.Form("3DSecureStatus"), "Text")
            If Len(Request.Form("CAVV")) > 0 Then strCAVV = cleanInput(Request.Form("CAVV"), "Text")
            If Len(Request.Form("AddressStatus")) > 0 Then strAddressStatus = cleanInput(Request.Form("AddressStatus"), "Text")
            If Len(Request.Form("PayerStatus")) > 0 Then strPayerStatus = cleanInput(Request.Form("PayerStatus"), "Text")
            If Len(Request.Form("CardType")) > 0 Then strCardType = cleanInput(Request.Form("CardType"), "Text")
            If Len(Request.Form("Last4Digits")) > 0 Then strLast4Digits = cleanInput(Request.Form("Last4Digits"), "Number")

            '** Now we rebuilt the POST message, including our security key, and use the MD5 Hash **
            '** component that ships with the kit to create our own signature to compare with **
            '** the contents of the VPSSignature field in the POST.  Check the Server protocol **
            '** if you need clarification on this process **
            strMessage = strVPSTxId & strVendorTxCode & strStatus & strTxAuthNo & strVendorName & strAVSCV2 & strSecurityKey & _
               strAddressResult & strPostCodeResult & strCV2Result & strGiftAid & str3DSecureStatus & strCAVV & _
               strAddressStatus & strPayerStatus & strCardType & strLast4Digits

            strMySignature = HashPasswordForStoringInConfigFile(strMessage, "MD5")

            Dim blnSuccess As Boolean = False
            '** Great, the signatures DO match, so we can update the database and redirect the user appropriately **
            If strStatus = "OK" Then
                blnSuccess = True
                strDBStatus = "AUTHORISED - The transaction was successfully authorised with the bank."
            ElseIf strStatus = "NOTAUTHED" Then
                strDBStatus = "DECLINED - The transaction was not authorised by the bank."
            ElseIf strStatus = "ABORT" Then
                strDBStatus = "ABORTED - The customer clicked Cancel on the payment pages, or the transaction was timed out due to customer inactivity."
            ElseIf strStatus = "REJECTED" Then
                strDBStatus = "REJECTED - The transaction was failed by your 3D-Secure or AVS/CV2 rule-bases."
            ElseIf strStatus = "AUTHENTICATED" Then
                strDBStatus = "AUTHENTICATED - The transaction was successfully 3D-Secure Authenticated and can now be Authorised."
            ElseIf strStatus = "REGISTERED" Then
                strDBStatus = "REGISTERED - The transaction was could not be 3D-Secure Authenticated, but has been registered to be Authorised."
            ElseIf strStatus = "ERROR" Then
                strDBStatus = "ERROR - There was an error during the payment process.  The error details are: " & SQLSafe(strStatusDetail)
            Else
                strDBStatus = "UNKNOWN - An unknown status was returned from Sage Pay.  The Status was: " & SQLSafe(strStatus) & _
                ", with StatusDetail:" & SQLSafe(strStatusDetail)
            End If

            '** Update our database with the results from the Notification POST **
            objSage.UpdateSagePayResponse(blnSuccess _
                                                 , strVendorTxCode, strStatus, strTxAuthNo, strAVSCV2, strAddressResult _
                                                 , strPostCodeResult, strCV2Result, strGiftAid, str3DSecureStatus, strCAVV, strAddressStatus _
                                                 , strPayerStatus, strCardType, strLast4Digits)

            '** New reply to Server to let the system know we've received the Notification POST **
            Response.Clear()
            Response.ContentType = "text/plain"

            '** Always send a Status of OK if we've read everything correctly.  Only INVALID for messages with a Status of ERROR **
            If strStatus = "ERROR" Then
                Response.Write("Status=INVALID" & vbCrLf)
            Else
                Response.Write("Status=OK" & vbCrLf)
            End If

            '** Now decide where to redirect the customer **
            If (strStatus = "OK") Or (strStatus = "AUTHENTICATED") Or (strStatus = "REGISTERED") Then
                '** If a transaction status is OK, AUTHENTICATED or REGISTERED, we should send the customer to the success page **
                strRedirectPage = "PDL/PDLApplyPDL6.aspx?LoanID=" & strLoanID & "&VendorTxCode=" & strVendorTxCode
            Else
                '** The status indicates a failure of one state or another, so send the customer to orderFailed instead **
                strRedirectPage = "PDL/PDLError.aspx?C=E8"
            End If

            '** Only use the Internal FQDN value during development.  In LIVE systems, always use the actual FQDN **
            If strConnectTo = "LIVE" Then
                Response.Write("RedirectURL=" & strYourSiteFQDN & strRedirectPage & vbCrLf)
            Else
                Response.Write("RedirectURL=" & strYourSiteInternalFQDN & strRedirectPage & vbCrLf)
            End If

            '** No need to send a StatusDetail, since we're happy with the POST **
            Response.End()

            'End If


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, "")
        End Try
    End Sub
End Class
