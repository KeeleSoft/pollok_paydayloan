﻿<%@ Page Language="VB" Trace="False" MasterPageFile="~/PDL/PDLMasterPage2.master" AutoEventWireup="false" CodeFile="PDLSagePaySetup.aspx.vb" Inherits="PDL_PDLSagePaySetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText"><table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          &nbsp;</td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                            <td><strong>We will pass your application to Sage pay to make a 1 pence payment to verify your card details. Please note we will not store your card details and Sage Pay will process this transaction.</strong>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                          &nbsp;</td>
                          </tr>                          
                          <tr><td>
                                          &nbsp;</td></tr>
                          <tr><td>
                              <img alt="" src="../Images/SagePay_Logo.png" /></td></tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  &nbsp;</td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Proceed" Width="70px" 
                                                                      BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>


