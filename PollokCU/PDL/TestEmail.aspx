﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TestEmail.aspx.vb" Inherits="PDL_TestEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width:70%;">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    To</td>
                <td>
                    <asp:TextBox ID="txtTo" runat="server" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Message</td>
                <td>
                    <asp:TextBox ID="txtBody" runat="server" Rows="10" TextMode="MultiLine" 
                        Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSend" runat="server" Text="Send" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
