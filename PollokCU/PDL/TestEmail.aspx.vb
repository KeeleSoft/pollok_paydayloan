﻿Imports System.Data

Partial Class PDL_TestEmail
    Inherits System.Web.UI.Page

    Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            lblMessage.Text = ""

            SendEmail()

            lblMessage.Text = "Sent Fine"
        Catch ex As Exception
            lblMessage.Text = "Failed, " & ex.Message
        End Try
    End Sub

    Private Sub SendEmail()
        Dim sEmailBody As String

        Dim sEmailTo, sEmailFrom, sSubject As String
        Dim fileList As List(Of String) = New List(Of String)

        sEmailTo = txtTo.Text
        sEmailFrom = "noreply@cuonline.org.uk"

        sSubject = "handiloan"

        Dim FilenameShort As String = "100086" & "_" & "06071965" & ".pdf"

        Dim sPDFURL As String = objConfig.getConfigKey("PDL.Agreement.Location") & "LoanAgreement_" & FilenameShort
        fileList.Add(sPDFURL)

        Dim sPayrollForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "PayrollDeductionForm.pdf"
        fileList.Add(sPayrollForm)
        Dim sDDForm As String = objConfig.getConfigKey("PDL.Agreement.Location") & "DIRECT_DEBIT_FORM.pdf"
        fileList.Add(sDDForm)


        sEmailBody = GetPDLCustomerEmailBody("Shan", "Mithige", True, True)



        objEmail.SendEmailMessage(sEmailFrom _
                                  , sEmailTo _
                                  , sSubject _
                                  , sEmailBody _
                                  , "" _
                                  , "" _
                                  , fileList)

    End Sub


    Private Function GetPDLCustomerEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal CUCACustomer As Boolean, ByVal Payroll As Boolean) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Congratulations, your handiloan application was submitted successfully. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount to your bank account within twelve hours from you receiving this message. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")

            If CUCACustomer Then
                body.AppendLine("As selected by you during the application process, a standing order will be setup from your Credit Union Current Account (CUCA) to be your loan repayment, the details of which is outlined in the loan agreement attached to this email. Please ensure enough funds are available in your CUCA to meet the scheduled loan repayments standing order. Failing to make your loan repayments will lead to your personal credit record to be affected negatively.")
                body.AppendLine("<br/><br/>")
            ElseIf Payroll Then
                body.AppendLine("How to repay your loan by payroll deduction:")
                body.AppendLine("<br/><br/>")
                body.AppendLine("Please find attached to this email a payroll deduction form. This must be printed out, filled in and signed. Please send the completed direct payroll deduction form back to us as soon as possible to setup your loan repayments. Alternatively you can visit any one of our branches to complete a payroll deduction form. Failing to setup your loan repayments will lead to your personal credit record to be affected negatively. ")
                body.AppendLine("<br/><br/>")
            Else
                body.AppendLine("How to repay your loan by direct debit:")
                body.AppendLine("<br/><br/>")
                body.AppendLine("Please find attached to this email a direct debit form. This must be printed out, filled in and signed. Please send the completed direct debit form back to us as soon as possible to setup your loan repayments. Alternatively you can visit any one of our branches to complete a direct debit form. Failing to setup your loan repayments will lead to your personal credit record to be affected negatively. ")
                body.AppendLine("<br/><br/>")
            End If

            body.AppendLine("For all contact details, branch locations with opening times and further information, please visit our website at http://leedscitycreditunion.co.uk/ ")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries please contact us via email to theweeloan@pollokcu.com or call us on 0113 203 1613.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our handiloan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            lblMessage.Text = "Failed, " & ex.Message
            Return ""
        End Try
    End Function
End Class
