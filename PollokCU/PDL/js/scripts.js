$(document).ready(function(){

	//Home Slider
	$(".hero-slider").owlCarousel({
 		items:1,
 		loop:true,
 		autoplay:true,
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    margin:0,
	    nav: false,
	    dots: false,
	    responsiveClass:true
	});

	// Mobile Navigation Slicknav
 	$('#mmenu').slicknav({
		prependTo:"#mobile-menu",
		allowParentLinks: true, // Allow clickable links as parent elements.
		nestedParentLinks: true // If false, parent links will be separated from the sub-menu toggle.
	});

	// TOGGLE DIVS
	
	$('.expand div').hide();
	$('.expand a.toggle').click(function(){
		$(this).parent().siblings('div').slideToggle();
		$(this).parent().find('a').toggle();
		return false;
	});
	
	// RIGHT ALIGN FB-LIKE
	
	var iframe = $('iframe');
	console.log(iframe);
	
});