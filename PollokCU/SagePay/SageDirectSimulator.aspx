﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SageDirectSimulator.aspx.vb" Inherits="SagePay_SageDirectSimulator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width: 70%;">
            <tr>
                <td>
                    Amount</td>
                <td>
                    <asp:TextBox ID="Amount" runat="server" Width="200px">30</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Billing First Name 
                    (7 Characters)</td>
                <td>
                    <asp:TextBox ID="FirstName" runat="server" Width="200px">G</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Billing Surname 
                    (20 Characters)</td>
                <td>
                    <asp:TextBox ID="SurName" runat="server" Width="200px">Hewa Mithige</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Email</td>
                <td>
                    <asp:TextBox ID="Email" runat="server" Width="200px">gayeshan@gmail.com</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address Line 1</td>
                <td>
                    <asp:TextBox ID="AddressLine1" runat="server" Width="200px">17 Greenwood Avenue</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Address Line 2</td>
                <td>
                    <asp:TextBox ID="AddressLine2" runat="server" Width="200px">Trent Vale</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    City</td>
                <td>
                    <asp:TextBox ID="City" runat="server" Width="200px">Stoke-On-Trent</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Post Code</td>
                <td>
                    <asp:TextBox ID="PostCode" runat="server" Width="200px">ST4 6NF</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:HiddenField ID="VPSProtocol" runat="server" Value="2.23" />
                    <asp:HiddenField ID="TxType" runat="server" Value="PAYMENT" />
                    <asp:HiddenField ID="Vendor" runat="server" Value="londonmutualcre" />
                    <asp:HiddenField ID="VendorTxCode" runat="server" Value="10000" />
                    <asp:HiddenField ID="Currency" runat="server" Value="GBP" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
