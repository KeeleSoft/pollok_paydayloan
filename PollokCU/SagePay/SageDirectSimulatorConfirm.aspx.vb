﻿Imports SageIncludes

Partial Class SagePay_SageDirectSimulatorConfirm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strVendorTxCode As String
        Dim strCrypt As String
        Dim strPost As String

        Dim strBillingAddressLine1 As String = Session("AddressLine1")
        Dim strBillingAddressLine2 As String = Session("AddressLine2")
        Dim strBillingCity As String = Session("City")
        Dim strBillingPostCode As String = Session("PostCode")
        Dim strBillingCountry As String = "GB"


        litAmount.Text = Session("Amount")


        '** Okay, build the crypt field for Form using the information in our session **
        '** First we need to generate a unique VendorTxCode for this transaction **
        '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
        '** but the VendorTxCode MUST be unique for each transaction you send to Server **
        Randomize()
        strVendorTxCode = strVendorName & "-" & Right(DatePart("yyyy", Now()), 2) & _
                            Right("00" & DatePart("m", Now()), 2) & Right("00" & DatePart("d", Now()), 2) & _
                            Right("00" & DatePart("h", Now()), 2) & Right("00" & DatePart("n", Now()), 2) & _
                            Right("00" & DatePart("s", Now()), 2) & "-" & CStr(Math.Round(Rnd() * 100000))


        strPost = "VendorTxCode=" & strVendorTxCode '** As generated above **
        strPost = strPost & "&Amount=" & FormatNumber(Session("Amount"), 2, -1, 0, 0) '** Formatted to 2 decimal places with leading digit **
        strPost = strPost & "&Currency=" & strCurrency
        strPost = strPost & "&Description=Pay Day Loan Payment " & strVendorName
        '** The SuccessURL is the page to which Form returns the customer if the transaction is successful **
        '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
        strPost = strPost & "&SuccessURL=" & strYourSiteFQDN & strVirtualDir & "/SagePaySuccessful.aspx"

        '** The FailureURL is the page to which Form returns the customer if the transaction is unsuccessful **
        '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
        strPost = strPost & "&FailureURL=" & strYourSiteFQDN & strVirtualDir & "/SagePayFailed.aspx"

        '** Pass the Customer's name for use within confirmation emails and the Sage Pay Admin area.
        strPost = strPost & "&CustomerName=" & Session("FirstName") & " " & Session("SurName")
        strPost = strPost & "&CustomerEMail=" & Session("Email")
        If strVendorEMail <> "[your e-mail address]" Then
            strPost = strPost & "&VendorEMail=" & strVendorEMail
        End If

        strPost = strPost & "&SendEMail=" & iSendEMail
        '** You can specify any custom message to send to your customers in their confirmation e-mail here **
        '** The field can contain HTML if you wish, and be different for each order.  The field is optional **
        strPost = strPost & "&eMailMessage=Thank you for your payment."

        '** Populate Customer Details for crypt string
        '** Billing Details
        strPost = strPost & "&BillingSurname=" & Session("SurName")
        strPost = strPost & "&BillingFirstnames=" & Session("FirstName")
        strPost = strPost & "&BillingAddress1=" & strBillingAddressLine1
        If Not String.IsNullOrEmpty(strBillingAddressLine2) Then
            strPost = strPost & "&BillingAddress2=" & strBillingAddressLine2
        End If
        strPost = strPost & "&BillingCity=" & strBillingCity
        strPost = strPost & "&BillingPostCode=" & strBillingPostCode
        strPost = strPost & "&BillingCountry=" & strBillingCountry

        strPost = strPost & "&DeliverySurname=" & Session("SurName")
        strPost = strPost & "&DeliveryFirstnames=" & Session("FirstName")
        strPost = strPost & "&DeliveryAddress1=" & strBillingAddressLine1
        If Not String.IsNullOrEmpty(strBillingAddressLine2) Then
            strPost = strPost & "&DeliveryAddress2=" & strBillingAddressLine2
        End If
        strPost = strPost & "&DeliveryCity=" & strBillingCity
        strPost = strPost & "&DeliveryPostCode=" & strBillingPostCode
        strPost = strPost & "&DeliveryCountry=" & strBillingCountry

        '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
        '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
        If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"

        '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
        '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
        strPost = strPost & "&Apply3DSecure=0"

        ' ** Encrypt the plaintext string for inclusion in the hidden field **
        strCrypt = EncryptAndEncode(strPost)

        Crypt.Value = strCrypt
        btnSubmit.PostBackUrl = SystemURL(strConnectTo)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

    End Sub
End Class
