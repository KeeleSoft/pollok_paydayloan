﻿Imports SageIncludes
Partial Class SagePay_SagePaySuccessfulSimulator
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strDecoded As String
        Dim strCrypt As String
        Dim strGiftAid As String = "No"
        Dim strVendorTxCode As String
        '** Now check we have a Crypt field passed to this page **
        strCrypt = Request.QueryString("Crypt")
        If Len(strCrypt) = 0 Then
            Response.Clear()
            'Server.Transfer("welcome.aspx")
            Response.Write("No Crypt")
            Response.End()
        End If

        '** Now decode the Crypt field and extract the results **
        strDecoded = DecodeAndDecrypt(strCrypt)

        If getToken(strDecoded, "GiftAid") = "1" Then
            strGiftAid = "Yes"
        End If

        strVendorTxCode = getToken(strDecoded, "VendorTxCode")
        lblVendorTxCode.Text = Server.HtmlEncode(strVendorTxCode)
        lblVendorTxCodeReference.Text = Server.HtmlEncode(strVendorTxCode)
        lblStatus.Text = Server.HtmlEncode(getToken(strDecoded, "Status"))
        lblStatusDetail.Text = Server.HtmlEncode(getToken(strDecoded, "StatusDetail"))
        lblAmount.Text = Server.HtmlEncode(getToken(strDecoded, "Amount") & " " & strCurrency)
        lblVPSTxId.Text = Server.HtmlEncode(getToken(strDecoded, "VPSTxId"))
        lblVPSAuthCode.Text = Server.HtmlEncode(getToken(strDecoded, "TxAuthNo"))
        lblAVSCV2Result.Text = Server.HtmlEncode("- Address: " & getToken(strDecoded, "AddressResult") & ", Post Code: " & getToken(strDecoded, "PostCodeResult") & ", CV2: " & getToken(strDecoded, "CV2Result"))
        lblGiftAid.Text = Server.HtmlEncode(strGiftAid)
        lbl3DSecure.Text = Server.HtmlEncode(getToken(strDecoded, "3DSecureStatus"))
        lblCAVV.Text = Server.HtmlEncode(getToken(strDecoded, "CAVV"))
        lblCardType.Text = Server.HtmlEncode(getToken(strDecoded, "CardType"))
        lblLast4Digits.Text = Server.HtmlEncode(getToken(strDecoded, "Last4Digits"))
        lblPPAddressStatus.Text = Server.HtmlEncode(getToken(strDecoded, "AddressStatus"))
        lblPPPayerStatus.Text = Server.HtmlEncode(getToken(strDecoded, "PayerStatus"))

    End Sub
End Class
