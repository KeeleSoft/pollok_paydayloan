﻿
Partial Class SagePay_SageServerSimulator
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Session("Amount") = Amount.Text
        Session("FirstName") = FirstName.Text
        Session("SurName") = SurName.Text
        Session("Email") = Email.Text
        Session("AddressLine1") = AddressLine1.Text
        Session("AddressLine2") = AddressLine2.Text
        Session("City") = City.Text
        Session("PostCode") = PostCode.Text
        Response.Redirect("SageServerSimulatorConfirm.aspx")
    End Sub
End Class
