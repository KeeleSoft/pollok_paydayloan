﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SageServerSimulatorConfirm.aspx.vb" Inherits="SagePay_SageServerSimulatorConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width: 70%;">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Amount</td>
                <td>
                    <asp:Literal ID="litAmount" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    Name</td>
                <td>
                    <asp:Literal ID="litName" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    Email</td>
                <td>
                    <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:HiddenField ID="VPSProtocol" runat="server" Value="2.23" />
                    <asp:HiddenField ID="TxType" runat="server" Value="PAYMENT" />
                    <asp:HiddenField ID="Vendor" runat="server" Value="londonmutualcre" />
                    <asp:HiddenField ID="VendorTxCode" runat="server" Value="10000" />
                    <asp:HiddenField ID="Currency" runat="server" Value="GBP" />
                    <asp:HiddenField ID="Crypt" runat="server" Value="GBP" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" 
                        PostBackUrl="https://test.sagepay.com/Simulator/VSPFormGateway.asp" 
                        Text="Button" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
