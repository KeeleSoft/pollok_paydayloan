﻿Imports SageIncludesServer
Imports System.IO
Imports System.Net

Partial Class SagePay_SageServerSimulatorConfirm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strVendorTxCode As String
        
        Dim strPost As String

        Dim strBillingAddressLine1 As String = Session("AddressLine1")
        Dim strBillingAddressLine2 As String = Session("AddressLine2")
        Dim strBillingCity As String = Session("City")
        Dim strBillingPostCode As String = Session("PostCode")
        Dim strBillingCountry As String = "GB"


        litAmount.Text = Session("Amount")


        '** Okay, build the crypt field for Form using the information in our session **
        '** First we need to generate a unique VendorTxCode for this transaction **
        '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
        '** but the VendorTxCode MUST be unique for each transaction you send to Server **
        Randomize()
        strVendorTxCode = strVendorName & "-" & Right(DatePart("yyyy", Now()), 2) & _
                            Right("00" & DatePart("m", Now()), 2) & Right("00" & DatePart("d", Now()), 2) & _
                            Right("00" & DatePart("h", Now()), 2) & Right("00" & DatePart("n", Now()), 2) & _
                            Right("00" & DatePart("s", Now()), 2) & "-" & CStr(Math.Round(Rnd() * 100000))


        strPost = "VPSProtocol=" & strProtocol
        strPost = strPost & "&TxType=" & strTransactionType '** PAYMENT by default.  You can change this in the includes file **
        strPost = strPost & "&Vendor=" & strVendorName
        strPost = strPost & "&VendorTxCode=" & strVendorTxCode '** As generated above **
        strPost = strPost & "&Amount=" & FormatNumber(Session("Amount"), 2, -1, 0, 0) '** Formatted to 2 decimal places with leading digit **
        strPost = strPost & "&Currency=" & strCurrency
        strPost = strPost & "&Description=Pay Day Loan Payment " & strVendorName
        '** The SuccessURL is the page to which Form returns the customer if the transaction is successful **
        '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
        strPost = strPost & "&NotificationURL=" & strYourSiteFQDN & strVirtualDir & "/notificationPage.aspx"
        '** The SuccessURL is the page to which Form returns the customer if the transaction is successful **
        '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
        'strPost = strPost & "&SuccessURL=" & strYourSiteFQDN & strVirtualDir & "/SagePaySuccessful.aspx"

        '** The FailureURL is the page to which Form returns the customer if the transaction is unsuccessful **
        '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
        'strPost = strPost & "&FailureURL=" & strYourSiteFQDN & strVirtualDir & "/SagePayFailed.aspx"

        '** Billing Details **
        strPost = strPost & "&BillingSurname=" & URLEncode(Session("SurName"))
        strPost = strPost & "&BillingFirstnames=" & URLEncode(Session("FirstName"))
        strPost = strPost & "&BillingAddress1=" & URLEncode(strBillingAddressLine1)
        If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&BillingAddress2=" & URLEncode(strBillingAddressLine2)
        strPost = strPost & "&BillingCity=" & URLEncode(strBillingCity)
        strPost = strPost & "&BillingPostCode=" & URLEncode(strBillingPostCode)
        strPost = strPost & "&BillingCountry=" & URLEncode(strBillingCountry)

        '** Delivery Details **
        strPost = strPost & "&DeliverySurname=" & URLEncode(Session("SurName"))
        strPost = strPost & "&DeliveryFirstnames=" & URLEncode(Session("FirstName"))
        strPost = strPost & "&DeliveryAddress1=" & URLEncode(strBillingAddressLine1)
        If Len(strBillingAddressLine2) > 0 Then strPost = strPost & "&DeliveryAddress2=" & URLEncode(strBillingAddressLine2)
        strPost = strPost & "&DeliveryCity=" & URLEncode(strBillingCity)
        strPost = strPost & "&DeliveryPostCode=" & URLEncode(strBillingPostCode)
        strPost = strPost & "&DeliveryCountry=" & URLEncode(strBillingCountry)

        '** Pass the Customer's name for use within confirmation emails and the Sage Pay Admin area.
        strPost = strPost & "&CustomerEMail=" & Session("Email")

        '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
        '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
        If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"

        '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
        '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
        strPost = strPost & "&Apply3DSecure=0"
        '** Optional setting for Profile can be used to set a simpler payment page. See protocol guide for more info. **
        strPost = strPost & "&Profile=NORMAL" 'NORMAL is default setting. Can also be set to LOW for the simpler payment page version.

        Dim objUTFEncode As New UTF8Encoding
        Dim arrRequest As Byte()
        Dim objStreamReq As Stream
        Dim objStreamRes As StreamReader
        Dim objHttpRequest As HttpWebRequest
        Dim objHttpResponse As HttpWebResponse
        Dim objUri As New Uri(SystemURL(strConnectTo, "purchase"))
        Dim strResponse As String
        Dim strPageError As String
        Dim strStatus As String
        Dim strStatusDetail As String
        Dim strVPSTxId As String
        Dim strSecurityKey As String
        Dim strNextURL As String

        objHttpRequest = HttpWebRequest.Create(objUri)
        objHttpRequest.KeepAlive = False
        objHttpRequest.Method = "POST"

        objHttpRequest.ContentType = "application/x-www-form-urlencoded"
        arrRequest = objUTFEncode.GetBytes(strPost)
        objHttpRequest.ContentLength = arrRequest.Length
        objStreamReq = objHttpRequest.GetRequestStream()
        objStreamReq.Write(arrRequest, 0, arrRequest.Length)
        objStreamReq.Close()

        'Get response
        objHttpResponse = objHttpRequest.GetResponse()
        objStreamRes = New StreamReader(objHttpResponse.GetResponseStream(), Encoding.ASCII)

        strResponse = objStreamRes.ReadToEnd()
        objStreamRes.Close()

        If Err.Number <> 0 Then
            '** An non zero Err.number indicates an error of some kind **
            '** Check for the most common error... unable to reach the purchase URL **  
            If Err.Number = -2147012889 Then
                strPageError = "Your server was unable to register this transaction with Sage Pay." & _
                "  Check that you do not have a firewall restricting the POST and " & _
                "that your server can correctly resolve the address " & SystemURL(strConnectTo, "puchase")
            Else
                strPageError = "An Error has occurred whilst trying to register this transaction.<BR>" & _
                "The Error Number is: " & Err.Number & "<BR>" & _
                "The Description given is: " & Err.Description
            End If
            lblMessage.Visible = True
            lblMessage.Text = strPageError
            
        Else
            '** No transport level errors, so the message got the Sage Pay **
            '** Analyse the response from Server to check that everything is okay **
            '** Registration results come back in the Status and StatusDetail fields **
            strStatus = findField("Status", strResponse)
            strStatusDetail = findField("StatusDetail", strResponse)

            If Left(strStatus, 2) = "OK" Then
                '** An OK status mean that the transaction has been successfully registered **
                '** Your code needs to extract the VPSTxId (Sage Pay's unique reference for this transaction) **
                '** and the SecurityKey (used to validate the call back from Sage Pay later) and the NextURL **
                '** (the URL to which the customer's browser must be redirected to enable them to pay) **
                strVPSTxId = findField("VPSTxId", strResponse)
                strSecurityKey = findField("SecurityKey", strResponse)
                strNextURL = findField("NextURL", strResponse)

                '** Finally, if we're not in Simulator Mode, redirect the page to the NextURL **
                '** In Simulator mode, we allow this page to display and ask for Proceed to be clicked **
                If strConnectTo <> "SIMULATOR" Then
                    Response.Clear()
                    Response.Redirect(strNextURL)
                    Response.End()
                Else
                    
                End If

            ElseIf strStatus = "MALFORMED" Then
                '** A MALFORMED status occurs when the POST sent above is not correctly formatted **
                '** or is missing compulsory fields.  You will normally only see these during **
                '** development and early testing **
                strPageError = "Sage Pay returned an MALFORMED status. " & _
                "The POST was Malformed because """ & findField("StatusDetail", strResponse) & """"
                lblMessage.Visible = True
                lblMessage.Text = strPageError

            ElseIf strStatus = "INVALID" Then
                '** An INVALID status occurs when the structure of the POST was correct, but **
                '** one of the fields contains incorrect or invalid data.  These may happen when live **
                '** but you should modify your code to format all data correctly before sending **
                '** the POST to Server **
                strPageError = "Sage Pay returned an INVALID status. " & _
                "The data sent was Invalid because """ & findField("StatusDetail", strResponse) & """"
                lblMessage.Visible = True
                lblMessage.Text = strPageError
            Else
                '** The only remaining status is ERROR **
                '** This occurs extremely rarely when there is a system level error at Sage Pay **
                '** If you receive this status the payment systems may be unavailable **<br>
                '** You could redirect your customer to a page offering alternative methods of payment here **
                strPageError = "Sage Pay returned an ERROR status. " & _
                "The description of the error was """ & findField("StatusDetail", strResponse) & """"
                lblMessage.Visible = True
                lblMessage.Text = strPageError
            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

    End Sub
End Class
