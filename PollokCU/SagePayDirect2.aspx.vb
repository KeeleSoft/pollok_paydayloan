﻿Imports SageIncludes
Partial Class SagePayDirect2
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;")

        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
        End If

        PopulatePage()

    End Sub

    Private Sub PopulatePage()
        Try
            Dim strVendorTxCode As String
            Dim strCrypt As String
            Dim strPost As String

            Dim strAmount As String = FormatNumber(Session("Amount"), 2, -1, 0, 0) '** Formatted to 2 decimal places with leading digit **
            Dim strMemberID As String = Session("MemberIDPayment")
            Dim strFirstName As String = Session("FirstName")
            Dim strSurName As String = Session("SurName")
            Dim strEmail As String = Session("Email")
            Dim strBillingAddressLine1 As String = Session("AddressLine1")
            Dim strBillingAddressLine2 As String = Session("AddressLine2")
            Dim strBillingCity As String = Session("City")
            Dim strBillingPostCode As String = Session("PostCode")
            Dim strBillingCountry As String = "GB"
            Dim strReason As String = Session("Reason")

            litMemberNumber.Text = strMemberID
            litAmount.Text = strAmount
            litReason.Text = strReason
            litFirstName.Text = strFirstName
            litSurname.Text = strSurName
            litEmail.Text = strEmail
            litAddressLine1.Text = strBillingAddressLine1
            litAddressLine2.Text = strBillingAddressLine2
            litCity.Text = strBillingCity
            litPostCode.Text = strBillingPostCode

            '** Okay, build the crypt field for Form using the information in our session **
            '** First we need to generate a unique VendorTxCode for this transaction **
            '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
            '** but the VendorTxCode MUST be unique for each transaction you send to Server **
            Randomize()
            'strVendorTxCode = strVendorName & "-" & Right(DatePart("yyyy", Now()), 2) & _
            '                    Right("00" & DatePart("m", Now()), 2) & Right("00" & DatePart("d", Now()), 2) & _
            '                    Right("00" & DatePart("h", Now()), 2) & Right("00" & DatePart("n", Now()), 2) & _
            '                    Right("00" & DatePart("s", Now()), 2) & "-" & CStr(Math.Round(Rnd() * 100000))

            strVendorTxCode = strVendorName & "-" & strMemberID & "-" & CStr(Math.Round(Rnd() * 100000))

            strPost = "VendorTxCode=" & strVendorTxCode '** As generated above **
            strPost = strPost & "&Amount=" & strAmount '** Formatted to 2 decimal places with leading digit **
            strPost = strPost & "&Currency=" & strCurrency
            strPost = strPost & "&Description=" & strReason & "-" & strVendorName
            '** The SuccessURL is the page to which Form returns the customer if the transaction is successful **
            '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
            strPost = strPost & "&SuccessURL=" & strYourSiteFQDN & "SagePaySuccessful.aspx"

            '** The FailureURL is the page to which Form returns the customer if the transaction is unsuccessful **
            '** You can change this for each transaction, perhaps passing a session ID or state flag if you wish **
            strPost = strPost & "&FailureURL=" & strYourSiteFQDN & "SagePayFailed.aspx"

            '** Pass the Customer's name for use within confirmation emails and the Sage Pay Admin area.
            strPost = strPost & "&CustomerName=" & strFirstName & " " & strSurName
            strPost = strPost & "&CustomerEMail=" & strEmail
            If strVendorEMail <> "[your e-mail address]" Then
                strPost = strPost & "&VendorEMail=" & strVendorEMail
            End If

            strPost = strPost & "&SendEMail=" & iSendEMail
            '** You can specify any custom message to send to your customers in their confirmation e-mail here **
            '** The field can contain HTML if you wish, and be different for each order.  The field is optional **
            strPost = strPost & "&eMailMessage=Thank you for your payment."

            '** Populate Customer Details for crypt string
            '** Billing Details
            strPost = strPost & "&BillingSurname=" & strSurName
            strPost = strPost & "&BillingFirstnames=" & strFirstName
            strPost = strPost & "&BillingAddress1=" & strBillingAddressLine1
            If Not String.IsNullOrEmpty(strBillingAddressLine2) Then
                strPost = strPost & "&BillingAddress2=" & strBillingAddressLine2
            End If
            strPost = strPost & "&BillingCity=" & strBillingCity
            strPost = strPost & "&BillingPostCode=" & strBillingPostCode
            strPost = strPost & "&BillingCountry=" & strBillingCountry

            strPost = strPost & "&DeliverySurname=" & strSurName
            strPost = strPost & "&DeliveryFirstnames=" & strFirstName
            strPost = strPost & "&DeliveryAddress1=" & strBillingAddressLine1
            If Not String.IsNullOrEmpty(strBillingAddressLine2) Then
                strPost = strPost & "&DeliveryAddress2=" & strBillingAddressLine2
            End If
            strPost = strPost & "&DeliveryCity=" & strBillingCity
            strPost = strPost & "&DeliveryPostCode=" & strBillingPostCode
            strPost = strPost & "&DeliveryCountry=" & strBillingCountry

            '** Allow fine control over AVS/CV2 checks and rules by changing this value. 0 is Default **
            '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
            If strTransactionType <> "AUTHENTICATE" Then strPost = strPost & "&ApplyAVSCV2=0"

            '** Allow fine control over 3D-Secure checks and rules by changing this value. 0 is Default **
            '** It can be changed dynamically, per transaction, if you wish.  See the Server Protocol document **
            strPost = strPost & "&Apply3DSecure=0"

            ' ** Encrypt the plaintext string for inclusion in the hidden field **
            strCrypt = EncryptAndEncode(strPost)

            Crypt.Value = strCrypt
            TxType.Value = strTransactionType
            VPSProtocol.Value = strProtocol
            Vendor.Value = strVendorName
            Currency.Value = strCurrency
            VendorTxCode.Value = strVendorTxCode
            btnSubmit.PostBackUrl = SystemURL(strConnectTo)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSubmit0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click

    End Sub
End Class
