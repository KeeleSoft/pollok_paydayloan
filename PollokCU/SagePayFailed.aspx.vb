﻿Imports SageIncludes
Partial Class SagePayFailed
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
        End If

        Try
            Dim strDecoded As String
            Dim strCrypt As String
            Dim strGiftAid As String = "No"
            Dim strVendorTxCode As String

            '** Now check we have a Crypt field passed to this page **
            strCrypt = Request.QueryString("Crypt")
            If Len(strCrypt) = 0 Then
                pnlNotOK.Visible = True
                pnlOK.Visible = False
            Else
                pnlNotOK.Visible = False
                pnlOK.Visible = True

                '** Now decode the Crypt field and extract the results **
                strDecoded = DecodeAndDecrypt(strCrypt)

                If getToken(strDecoded, "GiftAid") = "1" Then
                    strGiftAid = "Yes"
                End If

                strVendorTxCode = getToken(strDecoded, "VendorTxCode")


                Dim strStatus As String = getToken(strDecoded, "Status")
                Dim strReason As String
                '** Determine the reason this transaction was unsuccessful **
                If strStatus = "NOTAUTHED" Then
                    strReason = "You payment was declined by the bank.  This could be due to insufficient funds, or incorrect card details."
                ElseIf strStatus = "ABORT" Then
                    strReason = "You chose to Cancel your payment on the payment pages.  If you wish to change your payment and resubmit it you " & _
                    "can do so here. If you have questions or concerns about paying online, please contact us at 0113 203 1613."
                ElseIf strStatus = "REJECTED" Then
                    strReason = "Your payment did not meet our minimum fraud screening requirements." & _
                    " If you have questions about our fraud screening rules, or wish to contact us to discuss this, please call 0113 203 1613."
                ElseIf strStatus = "INVALID" Or strStatus = "MALFORMED" Then
                    strReason = "We could not process your payment because we have been unable to register your transaction with our Payment Gateway." & _
                    " You can place the payment over the telephone instead by calling 0113 203 1613."
                ElseIf strStatus = "ERROR" Then
                    strReason = "We could not process your payment because our Payment Gateway service was experiencing difficulties." & _
                    " You can place the payment over the telephone instead by calling 0113 203 1613."
                Else
                    strReason = "The transaction process failed.  We please contact us with the date and time of your payment and we will investigate."
                End If

                litReason.Text = strReason

                Dim strStatusDetail As String = Server.HtmlEncode(getToken(strDecoded, "StatusDetail"))
                Dim strAmount As String = Server.HtmlEncode(getToken(strDecoded, "Amount") & " " & strCurrency)
                Dim strVPSTxId As String = Server.HtmlEncode(getToken(strDecoded, "VPSTxId"))
                Dim strVPSAuthCode As String = Server.HtmlEncode(getToken(strDecoded, "TxAuthNo"))
                Dim strAVSCV2Result As String = Server.HtmlEncode("- Address: " & getToken(strDecoded, "AddressResult") & ", Post Code: " & getToken(strDecoded, "PostCodeResult") & ", CV2: " & getToken(strDecoded, "CV2Result"))

                Dim str3DSecure As String = Server.HtmlEncode(getToken(strDecoded, "3DSecureStatus"))
                Dim strCAVV As String = Server.HtmlEncode(getToken(strDecoded, "CAVV"))
                Dim strCardType As String = Server.HtmlEncode(getToken(strDecoded, "CardType"))
                Dim strLast4Digits As String = Server.HtmlEncode(getToken(strDecoded, "Last4Digits"))
                Dim strPPAddressStatsus As String = Server.HtmlEncode(getToken(strDecoded, "AddressStatus"))
                Dim strPPPayerStatus As String = Server.HtmlEncode(getToken(strDecoded, "PayerStatus"))

                'objSage.CreateSagePayResponse(False _
                '                              , Session("MemberIDPayment"), Session("FirstName"), Session("SurName"), Session("Email") _
                '                              , Session("AddressLine1"), Session("AddressLine2"), Session("City"), Session("PostCode") _
                '                              , Session("Reason") _
                '                              , strVendorTxCode, strStatus, strStatusDetail, strAmount _
                '                              , strVPSTxId, strVPSAuthCode, strAVSCV2Result, strGiftAid _
                '                              , str3DSecure, strCAVV, strCardType, strLast4Digits, strPPAddressStatsus, strPPPayerStatus)
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

End Class
