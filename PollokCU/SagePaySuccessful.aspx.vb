﻿Imports SageIncludes
Partial Class SagePaySuccessful
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
        End If

        Try
            Dim strDecoded As String
            Dim strCrypt As String
            Dim strGiftAid As String = "No"
            Dim strVendorTxCode As String

            '** Now check we have a Crypt field passed to this page **
            strCrypt = Request.QueryString("Crypt")
            If Len(strCrypt) = 0 Then
                pnlNotOK.Visible = True
                pnlOK.Visible = False
            Else
                pnlNotOK.Visible = False
                pnlOK.Visible = True

                '** Now decode the Crypt field and extract the results **
                strDecoded = DecodeAndDecrypt(strCrypt)

                If getToken(strDecoded, "GiftAid") = "1" Then
                    strGiftAid = "Yes"
                End If

                strVendorTxCode = getToken(strDecoded, "VendorTxCode")
                litRef.Text = Server.HtmlEncode(strVendorTxCode)

                Dim strStatus As String = Server.HtmlEncode(getToken(strDecoded, "Status"))
                Dim strStatusDetail As String = Server.HtmlEncode(getToken(strDecoded, "StatusDetail"))
                Dim strAmount As String = Server.HtmlEncode(getToken(strDecoded, "Amount") & " " & strCurrency)
                Dim strVPSTxId As String = Server.HtmlEncode(getToken(strDecoded, "VPSTxId"))
                Dim strVPSAuthCode As String = Server.HtmlEncode(getToken(strDecoded, "TxAuthNo"))
                Dim strAVSCV2Result As String = Server.HtmlEncode("- Address: " & getToken(strDecoded, "AddressResult") & ", Post Code: " & getToken(strDecoded, "PostCodeResult") & ", CV2: " & getToken(strDecoded, "CV2Result"))

                Dim str3DSecure As String = Server.HtmlEncode(getToken(strDecoded, "3DSecureStatus"))
                Dim strCAVV As String = Server.HtmlEncode(getToken(strDecoded, "CAVV"))
                Dim strCardType As String = Server.HtmlEncode(getToken(strDecoded, "CardType"))
                Dim strLast4Digits As String = Server.HtmlEncode(getToken(strDecoded, "Last4Digits"))
                Dim strPPAddressStatsus As String = Server.HtmlEncode(getToken(strDecoded, "AddressStatus"))
                Dim strPPPayerStatus As String = Server.HtmlEncode(getToken(strDecoded, "PayerStatus"))

                'objSage.CreateSagePayResponse(True _
                '                              , Session("MemberIDPayment"), Session("FirstName"), Session("SurName"), Session("Email") _
                '                              , Session("AddressLine1"), Session("AddressLine2"), Session("City"), Session("PostCode") _
                '                              , Session("Reason") _
                '                              , strVendorTxCode, strStatus, strStatusDetail, strAmount _
                '                              , strVPSTxId, strVPSAuthCode, strAVSCV2Result, strGiftAid _
                '                              , str3DSecure, strCAVV, strCardType, strLast4Digits, strPPAddressStatsus, strPPPayerStatus)
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

End Class
