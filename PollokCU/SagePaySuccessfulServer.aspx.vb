﻿Imports SageIncludesServer
Partial Class SagePaySuccessfulServer
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("MemberID") Is Nothing Then
            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
        End If

        Try
            Dim strVendorTxCode As String = cleanInput(Request.QueryString("VendorTxCode"), "VendorTxCode")
            If strVendorTxCode.Length = 0 Then
                pnlNotOK.Visible = True
                pnlOK.Visible = False
            Else
                pnlNotOK.Visible = False
                pnlOK.Visible = True

                litRef.Text = Server.HtmlEncode(strVendorTxCode)
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub

End Class
