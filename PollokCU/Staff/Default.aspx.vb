﻿
Partial Class Staff_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        Else
            Response.Redirect("StaffHome.aspx")
        End If

    End Sub
End Class
