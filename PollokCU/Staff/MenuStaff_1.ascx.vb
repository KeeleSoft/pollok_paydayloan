﻿
Partial Class MenuStaff_1
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("StaffData") IsNot Nothing Then
            Dim StaffData As StaffDetails = CType(Session("StaffData"), StaffDetails)

            menuCUOKLoans.Visible = (StaffData.PDLAllowApprove OrElse StaffData.PDLAllowPayment OrElse StaffData.AllowViewCUOK)
            menuALPS.Visible = StaffData.AllowViewALPS
            menuFiles.Visible = StaffData.AllowFileDownload
            menuSagePays.Visible = StaffData.AllowViewSage
            menuSageColl.Visible = StaffData.AllowViewSageCollection
            menuCreditLimits.Visible = StaffData.AllowViewCreditLimits
            menuReports.Visible = (StaffData.StaffLevelEnum = StaffDetails.StaffLevelName.Admin)
            menuPassword.Visible = (StaffData.StaffName.ToLower <> "lmcuhelpdesk")
            menuCUStats.Visible = StaffData.AllowViewCUStats
        End If
    End Sub
End Class
