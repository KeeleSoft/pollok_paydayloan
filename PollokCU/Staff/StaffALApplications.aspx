﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffALApplications.aspx.vb" Inherits="StaffALApplications" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;ALPS </span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table class="BodyText" style="width:100%;">
                    <tr>
                        <td>
                            Last faster payment time:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litLastFPTime" runat="server"></asp:Literal>
                        </td>
                        <td>
                            Next faster payment time:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litNextFPTime" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table style="width:100%;">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table 
                    style="width:100%;" class="BodyText">
                <tr>
                    <td>
                        <strong>Search...</strong></td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        Member ID</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtMemberID" runat="server" Width="150px" 
                                                                            ValidationGroup="vgSearch"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        First name</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName" runat="server" Width="150px" 
                                                                            ValidationGroup="vgSearch"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Surname</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSurName" runat="server" Width="150px" 
                                                                            ValidationGroup="vgSearch"></asp:TextBox>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Completed Level</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlLevel" runat="server" Width="154px" 
                                                                            ValidationGroup="vgSearch">
                                                                            <asp:ListItem></asp:ListItem>
                                                                            <asp:ListItem>1</asp:ListItem>
                                                                            <asp:ListItem>2</asp:ListItem>
                                                                            <asp:ListItem>3</asp:ListItem>
                                                                            <asp:ListItem>4</asp:ListItem>
                                                                            <asp:ListItem>5</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Status</td>
                                                                    <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" DataSourceID="odsStatus" 
                                DataTextField="AppStatusDesc" DataValueField="AutoLoanAppStatusID" 
                                Width="154px" ValidationGroup="vgSearch">
                            </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                            <asp:ObjectDataSource ID="odsStatus" runat="server" 
                                SelectMethod="GetAllAutoLoanApplicationStatuses" 
                                TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                            </asp:ObjectDataSource>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                                                            ValidationGroup="vgSearch" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsPDLLoans" runat="server" 
                    SelectMethod="GetAllAutoloanApplications" 
                     TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                    <SelectParameters>
                    <asp:Parameter Name="FirstName" DefaultValue="" Type="String" />
                    <asp:Parameter Name="SurName" DefaultValue="" Type="String" />
                    <asp:Parameter DefaultValue="0" Name="Level" Type="Int16" />
                    <asp:Parameter DefaultValue="0" Name="ApplicationStatus" Type="Int32" />
                    <asp:Parameter DefaultValue="0" Name="MemberID" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsPDLLoans" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True" 
                    OnRowDataBound="gvBatches_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="MemberID" HeaderText="Member#" 
                            SortExpression="MemberID" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AppType" HeaderText="Type" 
                            SortExpression="AppType" />
                        <asp:BoundField DataField="ApplyMethod" HeaderText="Apply Method" 
                            SortExpression="ApplyMethod" />
                        <asp:BoundField DataField="AppliedDate" HeaderText="Applied On" 
                            SortExpression="AppliedDate" />
                        <asp:TemplateField HeaderText="Name" SortExpression="FullName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FullName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AppStatusDescription" 
                            HeaderText="Status" 
                            SortExpression="AppStatusDescription" />
                        <asp:BoundField DataField="CompletedLevel" HeaderText="Level" 
                            SortExpression="CompletedLevel">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditALPS" runat="server" Target="_blank"
                                    ImageUrl="~/images/icon_edit.png" 
                                    NavigateUrl='<%# "StaffEditAutoLoan.aspx?ID=" & Eval("AutoLoanID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                                <asp:HiddenField ID="hfDuplicated" runat="server" 
                                    Value='<%# Eval("DuplicatedApplication") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

