﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffALApplications
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Dim FPTime = objSystemData.GetSystemKeyValue("LastFasterPaymentProcessed")
        litLastFPTime.Text = FPTime

        If Not String.IsNullOrEmpty(FPTime) Then
            Dim FPTimeConverted As DateTime = Nothing
            If DateTime.TryParse(FPTime, FPTimeConverted) Then
                litNextFPTime.Text = DateAdd(DateInterval.Hour, 2, FPTimeConverted).ToString
            End If

        End If
    End Sub



    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            lblMessage.Visible = False

            odsPDLLoans.SelectParameters.Item("FirstName").DefaultValue = txtFirstName.Text
            odsPDLLoans.SelectParameters.Item("SurName").DefaultValue = txtSurName.Text
            odsPDLLoans.SelectParameters.Item("Level").DefaultValue = ddlLevel.SelectedValue
            odsPDLLoans.SelectParameters.Item("ApplicationStatus").DefaultValue = ddlStatus.SelectedValue
            odsPDLLoans.SelectParameters.Item("MemberID").DefaultValue = txtMemberID.Text

            odsPDLLoans.Select()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Sub gvBatches_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hfDuplicated As HiddenField = CType(e.Row.FindControl("hfDuplicated"), HiddenField)
            Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)
            Dim hlEditALPS As HyperLink = CType(e.Row.FindControl("hlEditALPS"), HyperLink)
            If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                If CBool(hfDuplicated.Value) Then
                    lblName.ForeColor = Drawing.Color.Red
                End If
                If Session("AllowManageALPS") OrElse Session("StaffLevel") = 1 Then
                    hlEditALPS.Enabled = True
                Else
                    hlEditALPS.Enabled = False
                End If
            End If
        End If
    End Sub
End Class
