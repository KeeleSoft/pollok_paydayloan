﻿Imports PollokCU
Partial Class Staff_StaffALNewApplication
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If
        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

#Region "Newloan control handlers"
    Protected Sub AutoLoanNewApplicationControl_ShowAvailableOptions(ByVal model As AutoLoanFacade.AutoLoanModel) Handles AutoLoanNewApplicationControl.ShowAvailableOptions
        Try
            Session("AutoLoanModel") = model
            HideAllControls()
            AutoLoanOptionsControl.LoadOption()
            AutoLoanOptionsControl.Visible = True

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub AutoLoanOptionsControl_LoanAborted() Handles AutoLoanOptionsControl.LoanAborted
        Try
            HideAllControls()
            AutoLoanMessagesControl.ShowMessage("M4")
            AutoLoanMessagesControl.Visible = True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub AutoLoanNewApplicationControl_PayDayLoanFailed(ByVal model As AutoLoanFacade.AutoLoanModel) Handles AutoLoanNewApplicationControl.PayDayLoanFailed, AutoLoanNewApplicationControl.RevLoanFailed, AutoLoanOptionsControl.RevLoanFailed
        Try
            Session("AutoLoanModel") = model
            HideAllControls()
            If (model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso model.RiskLimit = 0) _
                OrElse (model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PDLRiskLimit = 0) Then
                AutoLoanMessagesControl.ShowMessage("M5")   'No risk limit setup
                AutoLoanMessagesControl.Visible = True
            ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso model.IgnoreOneThirdFailure AndAlso model.OneThirdRule IsNot Nothing AndAlso Not model.OneThirdRule.OneThirdRulePassed Then   '1/3rd rule failed and staff selected to ignore
                AutoLoanMessagesControl.ShowMessage("M7")   '1/3rd failed and staff selected to proceed
                AutoLoanMessagesControl.Visible = True
            ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso IsNumeric(model.AccountNumber) AndAlso CInt(model.AccountNumber) <= 0 Then
                Dim msg As String = "Could not found a valid LCCU current account. Please contact LMCU."
                AutoLoanMessagesControl.ShowMessage("custom", msg)   'invalid cuca
                AutoLoanMessagesControl.Visible = True
            ElseIf model.OneThirdRule IsNot Nothing AndAlso Not model.OneThirdRule.OneThirdRulePassed AndAlso model.OneThirdRule.MonthsLeftToSettle > 0 Then
                Dim msg As String = "You need to pay one third of your loan before you take a top up loan. We calculate that you could apply for a loan after " & DateAdd(DateInterval.Month, model.OneThirdRule.MonthsLeftToSettle, Now()).ToShortDateString & "."
                AutoLoanMessagesControl.ShowMessage("custom", msg)   'One third rule failed
                AutoLoanMessagesControl.Visible = True
            Else
                AutoLoanAppealControl.LoadAppealControl()
                AutoLoanAppealControl.Visible = True
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub AutoLoanNewApplicationControl_PayDayLoanPassed(ByVal model As AutoLoanFacade.AutoLoanModel) Handles AutoLoanNewApplicationControl.PayDayLoanPassed, AutoLoanNewApplicationControl.RevLoanPassed, AutoLoanOptionsControl.RevLoanPassed
        Try
            Session("AutoLoanModel") = model
            HideAllControls()
            AutoLoanPaymentDetailsControl.LoadPaymentOptions()
            AutoLoanPaymentDetailsControl.Visible = True

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
#End Region

#Region "Options control handlers"
    Protected Sub AutoLoanOptionsControl_ActionAppeal(ByVal model As AutoLoanFacade.AutoLoanModel) Handles AutoLoanOptionsControl.ActionAppeal
        Try
            Session("AutoLoanModel") = model
            HideAllControls()
            AutoLoanAppealControl.LoadAppealControl()
            AutoLoanAppealControl.Visible = True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub AutoLoanOptionsControl_SessionExpired() Handles AutoLoanOptionsControl.SessionExpired, AutoLoanPaymentDetailsControl.SessionExpired, AutoLoanAppealControl.SessionExpired
        AutoLoanOptionsControl.Visible = False
        AutoLoanMessagesControl.ShowMessage("M3")
        AutoLoanMessagesControl.Visible = True
    End Sub
#End Region


#Region "Appeal Control Handlers"
    Protected Sub AutoLoanAppealControl_AppealSent() Handles AutoLoanAppealControl.AppealSent
        HideAllControls()
        AutoLoanMessagesControl.ShowMessage("M1")
        AutoLoanMessagesControl.Visible = True
    End Sub

    Protected Sub AutoLoanAppealControl_RefuseSent() Handles AutoLoanAppealControl.RefuseSent
        HideAllControls()
        AutoLoanMessagesControl.ShowMessage("M4")
        AutoLoanMessagesControl.Visible = True
    End Sub
#End Region

#Region "Payment control handlers"
    Protected Sub AutoLoanPaymentDetailsControl_PaymentInformationCaptured(manualVerifyRequired As Boolean) Handles AutoLoanPaymentDetailsControl.PaymentInformationCaptured
        Try
            HideAllControls()
            Dim msgCode As String = "M2"
            If manualVerifyRequired Then
                msgCode = "M6"
            End If
            AutoLoanMessagesControl.ShowMessage(msgCode)
            AutoLoanMessagesControl.Visible = True
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
#End Region




    Private Sub HideAllControls()
        AutoLoanNewApplicationControl.Visible = False
        AutoLoanAppealControl.Visible = False
        AutoLoanOptionsControl.Visible = False
        AutoLoanMessagesControl.Visible = False
        AutoLoanPaymentDetailsControl.Visible = False
    End Sub

    

End Class
