﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffAutoLoanBACSFiles.aspx.vb" Inherits="StaffAutoLoanBACSFiles" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;ALPS BACS File Download - Pending BACS Entries To Process</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td>
                <asp:GridView ID="gvBACSEntries" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsBACSEntries" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="MemberID" HeaderText="MemberID" 
                            SortExpression="MemberID" >
                        </asp:BoundField>
                        <asp:BoundField DataField="MemberName" HeaderText="Member Name" 
                            SortExpression="MemberName">
                        <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Amount" HeaderText="Amount" 
                            SortExpression="Amount" />
                        <asp:BoundField DataField="CurrentAccNumber" HeaderText="Current A\C" 
                            SortExpression="CurrentAccNumber" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditCollection" runat="server" 
                                    ImageUrl="~/images/icon_delete.png" 
                                    OnClick="return confirm('Are you sure you want to delete this entry?');" 
                                    NavigateUrl='<%# "StaffAutoLoanBACSFiles.aspx?Action=Delete&ID="& Eval("AutoLoanID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No pending ALPS BACS entries to process
                    </EmptyDataTemplate>
                </asp:GridView>
                                    
                        </td>
                    </tr>
                    <tr>
                        <td>
                                    
                            <asp:Button ID="btnProcessPending" runat="server" 
                                Text="Process Pending BACS Entries" />
                                    
                    <asp:ObjectDataSource ID="odsBACSEntries" runat="server" 
                    SelectMethod="GetAutoLoanPendingBACSEntries" 
                    TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;ALPS BACS File Download - Generated BACS Files</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsNewMemberFileList" runat="server" 
                    SelectMethod="SelectBACSFileDownload" 
                    TypeName="PollokCU.DataAccess.Layer.clsStaffAdmin">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="ALPS" Name="AppType" Type="String" />
                        </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="gvFileList" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsNewMemberFileList" PageSize="20" SkinID="NormalGrid" OnRowDataBound="gvFileList_RowDataBound"
                    Width="100%" AllowSorting="True" 
                    style="margin-top: 0px">
                    <Columns>
                        <asp:BoundField DataField="FileDate" HeaderText="File Date" 
                            SortExpression="FileDate" >
                            <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="StaffName" HeaderText="Created By" 
                            SortExpression="StaffName" />
                        <asp:TemplateField HeaderText="Record Count" SortExpression="BacsCount">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MembersCount") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMembersCount" runat="server" 
                                    Text='<%# Bind("BacsCount") %>'></asp:Label>
                                <asp:HiddenField ID="hfSMSBatchSent" runat="server" Value='<%# Bind("SMSBatchSent") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDownload" runat="server" 
                                    ImageUrl="~/Images/btn_view.png" 
                                    NavigateUrl='<%# "..\BacsFiles\" & Eval("FileName") %>' 
                                    Text="Download BACS File"></asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Send SMS">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlSMS" runat="server" ImageUrl="~/Images/sms_yes.png" 
                                    NavigateUrl='<%# "StaffAutoLoanBACSFiles.aspx?Action=SendSMS&FileID="& Eval("FileID") %>' Text="Send Batch SMS"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No BACS files to download
                    </EmptyDataTemplate>
                </asp:GridView>
                                    
                    
                
                </td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;ALPS Loan File Download - Generated Loan Files</span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                                    
                    <asp:ObjectDataSource ID="odsNewMemberFileListALPS" runat="server" 
                    SelectMethod="SelectNewMemberFileDownload" 
                    TypeName="PollokCU.DataAccess.Layer.clsStaffAdmin">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="ALPS_APPS" Name="AppType" Type="String" />
                        </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="gvFileListLoanFiles" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsNewMemberFileListALPS" PageSize="20" SkinID="NormalGrid" OnRowDataBound="gvFileListLoanFiles_RowDataBound"
                    Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="FileDateLong" HeaderText="File Date" 
                            SortExpression="FileDateLong" >
                            <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Members Count" SortExpression="MembersCount">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MembersCount") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMembersCount" runat="server" 
                                    Text='<%# Bind("MembersCount") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDownload" runat="server" 
                                    ImageUrl="~/Images/btn_view.png" 
                                    NavigateUrl='<%# "..\MemberCSVs\" & Eval("FileName") %>' 
                                    Text="Download Member CSV File"></asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </td>
          </tr>
          <tr>
            <td height="25" valign="middle">For security reasons, when you have finished using staff services always select LOG OUT.</td>
          </tr>          
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

