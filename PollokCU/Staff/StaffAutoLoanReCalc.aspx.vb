﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports PollokCU
Partial Class StaffAutoLoanReCalc
    Inherits System.Web.UI.Page

    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Me.btnOptionPDL.Attributes.Add("onclick", "return confirm('Are you sure you want to select this option and confirm?');")
        Me.btnNoSTLRequestedAmount.Attributes.Add("onclick", "return confirm('Are you sure you want to select this option and confirm?');")
        Me.btnOptionNoSTL.Attributes.Add("onclick", "return confirm('Are you sure you want to select this option and confirm?');")
        Me.btnOptionYesSTL.Attributes.Add("onclick", "return confirm('Are you sure you want to select this option and confirm?');")
        Me.btnOptionYesSTLReqAmount.Attributes.Add("onclick", "return confirm('Are you sure you want to select this option and confirm?');")
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Private Sub HideOptions()
        AllOptions.Visible = False
        OptionNoSTLRequestedAmount.Visible = False
        OptionNoSTL.Visible = False
        OptionYesSTL.Visible = False
        OptionYesSTLRequestedAmount.Visible = False

    End Sub

    Protected Sub btnRecalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalc.Click
        Try
            HideOptions()
            lblMessage.Visible = True
            Dim LoanAmount As Double = 0
            Dim MemberID As Integer = 0

            Double.TryParse(txtLoanAmount.Text, LoanAmount)
            If LoanAmount <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid loan amount"
                Return
            End If

            Dim dtTXN As DataTable
            dtTXN = objAutoLoan.GetAutoLoanDetail(Request.QueryString("ID"))
            If dtTXN.Rows.Count > 0 Then
                MemberID = dtTXN.Rows(0).Item("MemberID")
                Dim model As AutoLoanFacade.AutoLoanModel = New AutoLoanFacade.AutoLoanModel With {.MemberID = MemberID, .LoanAmount = LoanAmount}
                model.AutoLoanID = Request.QueryString("ID")
                model.UserID = Session("StaffID")
                If dtTXN.Rows(0).Item("AppType") = "RevLoan" Then
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan
                Else
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay
                End If
                Dim Engine As AutoLoanFacade.AutoLoanEngine = New AutoLoanFacade.AutoLoanEngine
                Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters
                Engine.GetMemberDetails(model)
                Engine.GetInitialMemberDetails(model)
                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay AndAlso LoanAmount > model.AutoLoanParams.MaxAllowedPDLAmount Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Maximum repeat pay day loan allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedPDLAmount, 2)
                ElseIf model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan AndAlso LoanAmount > model.AutoLoanParams.MaxAllowedRevLoanAmount Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Maximum repeat loan amount allowed is £" & FormatNumber(model.AutoLoanParams.MaxAllowedRevLoanAmount, 2)
                Else
                    If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay Then
                        Engine.PerformPayDayLoanRule(model)
                        If model.PayDayLoanRule IsNot Nothing AndAlso model.PayDayLoanRule.PayDayLoanRulePassed Then    'Passed
                            AllOptions.Visible = True
                            OptionPDL.Visible = True
                            Session("AutoLoanModel") = model
                        Else
                            lblMessage.Visible = True
                            lblMessage.Text = "Requested pay day loan amount cannot be granted. " & model.PayDayLoanRule.Comment
                        End If
                    Else    'Rev Loan
                        Engine.PerformOneThirdRule(model)
                        If model.OneThirdRule IsNot Nothing AndAlso model.OneThirdRule.OneThirdRulePassed AndAlso model.RiskLimit > 0 Then
                            Engine.PerformShareToLoanRule(model)    'Do the Share To Loan
                            Engine.PerformDecisionCalculations(model)   'Calculate all possible combinations
                            Session("AutoLoanModel") = model
                            ShowOptions(model)
                        Else
                            lblMessage.Visible = True
                            If model.RiskLimit <= 0 Then
                                lblMessage.Text = "Requested loan amount cannot be granted. No risk limit defined"
                            ElseIf Not model.OneThirdRule.OneThirdRulePassed Then
                                lblMessage.Text = "Requested loan amount cannot be granted. One third rule failed"
                            End If
                        End If
                    End If
                End If
            End If


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub ShowOptions(ByVal model As AutoLoanFacade.AutoLoanModel)
        HideOptions()
        AllOptions.Visible = True
        If model.DecisionCalcs.NoSTLLoanEntitlement >= model.LoanAmount Then
            OptionNoSTLRequestedAmount.Visible = True
            Dim noSTLTextRequested As String = String.Format("Amount to borrow {0} for the period of {1} months? Total loan balance will be {2} and monthly repayment is {3}", FormatNumber(model.LoanAmount, 2), FormatNumber(model.DecisionCalcs.NoSTLRequestedAmountFinalTerm, 0), FormatNumber(model.DecisionCalcs.NoSTLRequestedAmountTotalLoanBalance, 2), FormatNumber(model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment, 2))
            litNoSTLRequestedAmountText.Text = noSTLTextRequested
        ElseIf model.DecisionCalcs.NoSTLLoanEntitlement > 0 Then
            OptionNoSTL.Visible = True
            Dim noSTLText As String = String.Format("Amount to borrow {0} for the period of {1} months? Total loan balance will be {2} and monthly repayment is {3}", FormatNumber(model.DecisionCalcs.NoSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.NoSTLNewLoanTerm, 0), FormatNumber(model.DecisionCalcs.NoSTLTotalLoanBalance, 2), FormatNumber(model.DecisionCalcs.NoSTLRepaymentAmount, 2))
            litNoSTLText.Text = noSTLText
        End If

        If model.ShareToLoanRule.ShareToLoanRulePassed Then
            If model.LoanAmount < model.DecisionCalcs.YesSTLLoanEntitlement Then    'When the requested amount is less than after STL entitlemenet then give the option for requested amount
                OptionYesSTLRequestedAmount.Visible = True
                Dim yesSTLTextReqText As String = String.Format("Share to loan and borrow {0} for the period of {1} months? Total loan balance will be {2} and monthly repayment is {3}. Share To Loan transfer amount is {4}", FormatNumber(model.LoanAmount, 2), FormatNumber(model.DecisionCalcs.YesSTLRequestedAmountFinalTerm, 0), FormatNumber(model.DecisionCalcs.YesSTLRequestedAmountTotalLoanBalance, 2), FormatNumber(model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment, 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2))
                litYesSTLRequestedAmountText.Text = yesSTLTextReqText
            ElseIf model.DecisionCalcs.YesSTLLoanEntitlement > 0 Then
                OptionYesSTL.Visible = True
                Dim yesSTLText As String = String.Format("Share to loan and borrow {0} for the period of {1} months? Total loan balance will be {2} and monthly repayment is {3}. Share To Loan transfer amount is {4}", FormatNumber(model.DecisionCalcs.YesSTLLoanEntitlement, 2), FormatNumber(model.DecisionCalcs.YesSTLNewLoanTerm, 0), FormatNumber(model.DecisionCalcs.YesSTLTotalLoanBalance, 2), FormatNumber(model.DecisionCalcs.YesSTLRepaymentAmount, 2), FormatNumber(model.DecisionCalcs.STLReductionAmount, 2))
                litYesSTLText.Text = yesSTLText
            End If
        End If
        OptionCancel.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
    End Sub

    Private Sub UpdateFinalAmounts(ByVal LoanID As Integer, ByVal amount As Double, ByVal Term As Integer, ByVal Repayment As Double)
        'objAutoLoan.UpdateAutoLoanFinalFigures(LoanID, amount, Term, Repayment, 0, Session("StaffID"))
        objAutoLoan.UpdateAutoLoanSendToBACS(LoanID, Session("StaffID"))
        objAutoLoan.UpdateAutoLoanGrantedDate(LoanID)
    End Sub

    Protected Sub btnNoSTLRequestedAmount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNoSTLRequestedAmount.Click
        Try
            If Session("AutoLoanModel") IsNot Nothing Then
                Dim model As AutoLoanFacade.AutoLoanModel = Session("AutoLoanModel")
                UpdateFinalAmounts(model.AutoLoanID, model.LoanAmount, model.DecisionCalcs.NoSTLRequestedAmountFinalTerm, model.DecisionCalcs.NoSTLRequestedAmountFinalRepayment)
                Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnOptionNoSTL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionNoSTL.Click
        Try
            If Session("AutoLoanModel") IsNot Nothing Then
                Dim model As AutoLoanFacade.AutoLoanModel = Session("AutoLoanModel")
                UpdateFinalAmounts(model.AutoLoanID, model.DecisionCalcs.NoSTLLoanEntitlement, model.DecisionCalcs.NoSTLNewLoanTerm, model.DecisionCalcs.NoSTLRepaymentAmount)
                Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnOptionYesSTL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionYesSTL.Click
        Try
            If Session("AutoLoanModel") IsNot Nothing Then
                Dim model As AutoLoanFacade.AutoLoanModel = Session("AutoLoanModel")
                UpdateFinalAmounts(model.AutoLoanID, model.DecisionCalcs.YesSTLLoanEntitlement, model.DecisionCalcs.YesSTLNewLoanTerm, model.DecisionCalcs.YesSTLRepaymentAmount)
                Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnOptionYesSTLReqAmount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptionYesSTLReqAmount.Click
        Try
            If Session("AutoLoanModel") IsNot Nothing Then
                Dim model As AutoLoanFacade.AutoLoanModel = Session("AutoLoanModel")
                UpdateFinalAmounts(model.AutoLoanID, model.LoanAmount, model.DecisionCalcs.YesSTLRequestedAmountFinalTerm, model.DecisionCalcs.YesSTLRequestedAmountFinalRepayment)
                Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
