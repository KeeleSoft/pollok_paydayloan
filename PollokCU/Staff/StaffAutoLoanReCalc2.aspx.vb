﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports PollokCU

Partial Class StaffAutoLoanReCalc2
    Inherits System.Web.UI.Page

    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Me.btnConfirm.Attributes.Add("onclick", "return confirm('Are you sure you want to confirm selected figures? Click Yes if you want to approve and send to bacs.');")
        ddlTerm.Items.Add("")
        For i As Integer = 1 To 24
            ddlTerm.Items.Add(i)
        Next
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

   

    Protected Sub btnRecalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecalc.Click
        Try
            AllOptions.Visible = False
            lblMessage.Visible = True
            Dim LoanAmount As Double = 0
            Dim Term As Integer = 0
            Dim Repayment As Double = 0
            Dim RepaymentNewLoan As Double = 0
            Dim RepaymentOldLoan As Double = 0
            Dim S2Loan As Double = 0
            Dim MemberID As Integer = 0
            Dim model As AutoLoanFacade.AutoLoanModel

            Double.TryParse(txtLoanAmount.Text, LoanAmount)
            If LoanAmount <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid loan amount"
                Return
            End If

            Integer.TryParse(ddlTerm.SelectedValue, Term)
            Double.TryParse(txtRepayment.Text, Repayment)
            Double.TryParse(txtShareToLoan.Text, S2Loan)

            If Term <= 0 AndAlso Repayment <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Please select either term or repayment"
                Return
            End If

            Dim dtTXN As DataTable
            dtTXN = objAutoLoan.GetAutoLoanDetail(Request.QueryString("ID"))
            If dtTXN.Rows.Count > 0 Then
                MemberID = dtTXN.Rows(0).Item("MemberID")
                model = New AutoLoanFacade.AutoLoanModel With {.MemberID = MemberID, .LoanAmount = LoanAmount}
                model.AutoLoanID = Request.QueryString("ID")
                model.UserID = Session("StaffID")
                If dtTXN.Rows(0).Item("AppType") = "RevLoan" Then
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan
                Else
                    model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.PayDay
                End If
                Dim Engine As AutoLoanFacade.AutoLoanEngine = New AutoLoanFacade.AutoLoanEngine
                Engine.GetAutoLoanParameters(model) 'Load auto loan related parameters
                Engine.GetMemberDetails(model)
                Engine.GetInitialMemberDetails(model)

                If model.LoanType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan Then
                    If Term > 0 Then
                        RepaymentNewLoan = Pmt(model.AutoLoanParams.InterestRate, Term, -1 * LoanAmount)
                        RepaymentOldLoan = Pmt(model.AutoLoanParams.InterestRateOld, Term, -1 * (model.TotalLoansOutstanding - S2Loan))
                        Repayment = RepaymentNewLoan + RepaymentOldLoan
                        'Repayment = Pmt(model.AutoLoanParams.InterestRate, Term, -1 * (LoanAmount + (model.TotalLoansOutstanding - S2Loan)))
                        'ElseIf Repayment > 0 Then
                        'Term = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRate, Repayment, -1 * (LoanAmount + (model.TotalLoansOutstanding - S2Loan)))))
                    End If
                Else    'PayDay,OD Loan
                    If Term > 0 Then
                        Repayment = Pmt(model.AutoLoanParams.InterestRate, Term, -1 * LoanAmount)
                    ElseIf Repayment > 0 Then
                        Term = CInt(Math.Ceiling(NPer(model.AutoLoanParams.InterestRate, Repayment, -1 * LoanAmount)))
                    End If
                End If

                ShowCalculatedValues(model.AutoLoanID, LoanAmount, Term, Repayment, model.TotalLoansOutstanding, model.MemberID, model.OneThirdRule.CurrentRepayAmount, S2Loan, model.AutoLoanParams.CUCADefaultSaving, model.LoanType, model.MemberDetails.FirstName & " " & model.MemberDetails.Surname, RepaymentNewLoan, RepaymentOldLoan)

            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub ShowCalculatedValues(ByVal ID As Integer, ByVal loanAmount As Double, ByVal term As Integer, ByVal repayment As Double, ByVal currentLoanBalance As Double, ByVal memberid As Integer, ByVal currentRepayment As Double, ByVal S2Loan As Double, ByVal CUCASaving As Double, ByVal appType As AutoLoanFacade.AutoLoanEnums.AutoLoanType, ByVal memberName As String, ByVal repaymentNewLoan As Double, ByVal repaymentOldLoan As Double)
        AllOptions.Visible = True
        litAmount.Text = loanAmount
        If appType = AutoLoanFacade.AutoLoanEnums.AutoLoanType.RevLoan Then
            litBalance.Text = currentLoanBalance
            litShare2Loan.Text = S2Loan
        Else
            litBalance.Text = "N/A"
            litShare2Loan.Text = "N/A"
        End If
        litTerm.Text = term
        litNewRepayment.Text = Math.Ceiling(repaymentNewLoan)
        litCurrentRepayment.Text = Math.Ceiling(repaymentOldLoan)
        litRepayment.Text = Math.Ceiling(repayment)
        litCUCASavings.Text = CUCASaving
        hfLoanID2.Value = ID
        hfMemberID.Value = memberid
        hfMemberName.Value = memberName
        hfCurrentRepayment.Value = currentRepayment
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
    End Sub

    Private Sub UpdateFinalAmounts(ByVal LoanID As Integer, ByVal amountNewLoan As Double, ByVal TermNewLoan As Integer, ByVal RepaymentNewLoan As Double, ByVal S2Loan As Double, ByVal amountOldLoan As Double, ByVal TermOldLoan As Integer, ByVal RepaymentOldLoan As Double)
        objAutoLoan.UpdateAutoLoanFinalFigures(LoanID, amountNewLoan, TermNewLoan, RepaymentNewLoan, S2Loan, Session("StaffID"), amountOldLoan, TermOldLoan, RepaymentOldLoan)
        objAutoLoan.UpdateAutoLoanSendToBACS(LoanID, Session("StaffID"))
        objAutoLoan.UpdateAutoLoanGrantedDate(LoanID)
    End Sub

    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Try
            Dim s1Amount As Double = 0
            Double.TryParse(litShare2Loan.Text, s1Amount)
            Dim oldLoanBalance As Double = litBalance.Text - s1Amount
            UpdateFinalAmounts(hfLoanID2.Value, litAmount.Text, litTerm.Text, litNewRepayment.Text, litShare2Loan.Text, oldLoanBalance, litTerm.Text, litCurrentRepayment.Text)

            If oldLoanBalance > 0 Then
                SendRescheduleEmail(oldLoanBalance, "", litCurrentRepayment.Text, litTerm.Text)
            End If
            If hfCurrentRepayment.Value <> litRepayment.Text Then
                'Send an email to CUCA for the new repayment
                SendCUCAEmail()
            End If
            If Not String.IsNullOrEmpty(litShare2Loan.Text) AndAlso Double.Parse(litShare2Loan.Text) > 0 Then
                'Send an email to Loans for the new shareto loan
                SendS2LoanEmail()
            End If
            Response.Redirect("StaffEditAutoLoan.aspx?ID=" & Request.QueryString("ID"))
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendRescheduleEmail(ByVal loanAmount As Double, ByVal loanProduct As String, ByVal loanRepay As Double, ByVal loanTerm As Integer)
        Dim msg As String = "Member Number: " & hfMemberID.Value & " has rescheduled their " & loanAmount & " " & loanProduct & " to " & loanRepay & " over " & loanTerm

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS Current Loan Reschedule" _
                                          , msg, "")

        objAutoLoan.InsertAutoLoanNote(hfLoanID2.Value, "Current loan reschedule email sent " & msg, Session("StaffID"))

    End Sub

    Private Sub SendCUCAEmail()
        Dim msg As String = "Member Number: " & hfMemberID.Value & ", " & vbCrLf
        msg &= "Current repayment: " & hfCurrentRepayment.Value & ", " & vbCrLf
        msg &= "New repayment: " & (Double.Parse(litRepayment.Text) + Double.Parse(litCUCASavings.Text)).ToString & ", " & vbCrLf
        msg &= "New term (months): " & litTerm.Text & ", " & vbCrLf

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , "theweeloan@pollokcu.com" _
                                          , "ALPS New Repayment Confirmation" _
                                          , msg, "")
        objAutoLoan.InsertAutoLoanNote(hfLoanID2.Value, "Email Sent To CUCA: " & msg, Session("StaffID"))
    End Sub

    Private Sub SendS2LoanEmail()
        Dim msg As String = "Member Number: " & hfMemberID.Value & ", " & vbCrLf
        msg &= "Member Name: " & hfMemberName.Value & ", " & vbCrLf
        msg &= "Share To Loan Amount: " & litShare2Loan.Text & ", " & vbCrLf

        Dim email As String = "theweeloan@pollokcu.com"
        'Dim email() As String = {"gayeshan@yahoo.com", "handiloan@leedscitycreditunion.co.uk", "lucky@creditunion.co.uk", "gayeshan@gmail.com"}

        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail
        objEmail.SendEmailMessage("noreply@cuonline.org.uk" _
                                          , email _
                                          , "ALPS New S2L Confirmation" _
                                          , msg, "")
        objAutoLoan.InsertAutoLoanNote(hfLoanID2.Value, "Email Sent To Loans: " & msg, Session("StaffID"))
    End Sub
End Class
