﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffCUStats.aspx.vb" Inherits="StaffCUStats" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Pay Day Loan Credit Union Usage Stats </span></td>
          </tr>
          <tr>
            <td valign="middle" >                                    
                    <table style="width:100%;">
                        <tr>
                            <td>
                                Credit union:</td>
                            <td>
                                <asp:DropDownList ID="ddlCU" runat="server" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Month:</td>
                            <td>
                                <asp:DropDownList ID="ddlMonth" runat="server" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                Year:</td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" Width="80px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnView" runat="server" Text="View" />
                            </td>
                        </tr>                       
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>                       
                        </table>   
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" >                                    
                    <asp:ObjectDataSource 
                                                    ID="odsStats" runat="server" 
                                                    SelectMethod="GetUsageStatsByCU" 
                                                    
                        TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="CUID" Type="Int32" />
                                                        <asp:Parameter Name="ReportMonth" Type="Int32" />
                                                        <asp:Parameter Name="ReportYear" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                </td>
          </tr>
          <tr>
            <td valign="middle" >                                    
                                                <asp:GridView ID="gvStats" runat="server" AllowSorting="True" 
                                                    AutoGenerateColumns="False" 
                        DataSourceID="odsStats" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="TrackingService" HeaderText="Service" 
                                                            SortExpression="TrackingService" />
                                                        <asp:BoundField DataField="StatCount" HeaderText="Count" 
                                                            SortExpression="StatCount" />
                                                        <asp:BoundField DataField="StatCost" HeaderText="Cost" 
                                                            SortExpression="StatCost" DataFormatString="{0:N}" />
                                                    </Columns>                                                    
                                                </asp:GridView>
                </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

