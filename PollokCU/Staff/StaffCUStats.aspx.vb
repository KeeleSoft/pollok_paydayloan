﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffCUStats
    Inherits System.Web.UI.Page

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadDropDowns()
        End If
    End Sub

    Private Sub LoadDropDowns()
        ddlCU.Items.Clear()
        ddlCU.Items.Add(New ListItem("LMCU", 1))
        ddlCU.Items.Add(New ListItem("Pollok Credit Union", 3))
        ddlCU.SelectedIndex = 1

        ddlMonth.Items.Clear()
        ddlMonth.Items.Add(New ListItem("Jan", 1))
        ddlMonth.Items.Add(New ListItem("Feb", 2))
        ddlMonth.Items.Add(New ListItem("Mar", 3))
        ddlMonth.Items.Add(New ListItem("Apr", 4))
        ddlMonth.Items.Add(New ListItem("May", 5))
        ddlMonth.Items.Add(New ListItem("Jun", 6))
        ddlMonth.Items.Add(New ListItem("Jul", 7))
        ddlMonth.Items.Add(New ListItem("Aug", 8))
        ddlMonth.Items.Add(New ListItem("Sep", 9))
        ddlMonth.Items.Add(New ListItem("Oct", 10))
        ddlMonth.Items.Add(New ListItem("Nov", 11))
        ddlMonth.Items.Add(New ListItem("Dec", 12))
        ddlMonth.SelectedValue = Month(Now())

        ddlYear.Items.Clear()
        For i As Integer = Year(Now()) - 2 To Year(Now())
            ddlYear.Items.Add(i)
        Next
        ddlYear.SelectedIndex = 2
    End Sub

    Protected Sub btnView_Click(sender As Object, e As System.EventArgs) Handles btnView.Click
        odsStats.SelectParameters.Item("CUID").DefaultValue = ddlCU.SelectedValue
        odsStats.SelectParameters.Item("ReportMonth").DefaultValue = ddlMonth.SelectedValue
        odsStats.SelectParameters.Item("ReportYear").DefaultValue = ddlYear.SelectedValue

        odsStats.Select()
    End Sub
End Class
