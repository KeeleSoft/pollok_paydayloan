﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffChangePassword.aspx.vb" Inherits="StaffChangePassword" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10" valign="top">
        &nbsp;
        </td>
        <td>&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130" valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td valign="top">
            &nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Change Password </span></td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    CssClass="errormsg" />
            </td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                    
                <table style="width:100%;">
                    <tr>
                        <td width="40%">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Current password:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                runat="server" ControlToValidate="txtCurrentPW" 
                                ErrorMessage="Current password required">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCurrentPW" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            New password:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                runat="server" ControlToValidate="txtNewPW" 
                                ErrorMessage="New password required">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewPW" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Confirm new password<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                runat="server" ControlToValidate="txtConfirmNewPW" 
                                ErrorMessage="Confirm new password required">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtConfirmNewPW" runat="server" TextMode="Password" 
                                Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnChange" runat="server" Text="Change Password" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                    
            </td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                    <p>For security reasons, when you have finished using staff services always select LOG OUT.</p>
            </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

