﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffChangePassword
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnChange_Click(sender As Object, e As System.EventArgs) Handles btnChange.Click
        Try
            lblMessage.Visible = False

            If txtNewPW.Text.Length < 10 Then
                lblMessage.Visible = True
                lblMessage.Text = "New password should be atleast 10 characters"
                Return
            End If

            If txtNewPW.Text <> txtConfirmNewPW.Text Then
                lblMessage.Visible = True
                lblMessage.Text = "New password and confirmed password not matched"
                Return
            End If

            Dim dtStaff As DataTable = objStaff.SelectStaffDetail(Session("StaffID"))
            If dtStaff IsNot Nothing AndAlso dtStaff.Rows.Count > 0 Then
                If txtCurrentPW.Text <> dtStaff.Rows(0).Item("PasswordDec") Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Invalid current password"
                    Return
                End If
            End If

            objStaff.UpdateStaffPassword(Session("StaffID"), txtNewPW.Text, Session("StaffID"))
            lblMessage.Visible = True
            lblMessage.Text = "Password changed"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
