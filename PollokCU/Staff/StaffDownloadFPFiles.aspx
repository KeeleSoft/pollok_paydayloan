﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Staff/MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffDownloadFPFiles.aspx.vb" Inherits="StaffDownloadFPFiles" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Faster Payment Files </span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <asp:Label ID="lblInfoMessage" runat="server" style="font-weight: 700"></asp:Label>
              </td>
          </tr>
            </tr>
      
        <tr>
        <td>
            <asp:GridView ID="gvPendingFPayments" runat="server" AutoGenerateColumns="False" SkinID="NormalGrid" Width="100%" DataSourceID="odsPendingFPFiles">
                                    <Columns>
                                        <asp:BoundField DataField="ApplicationID" HeaderText="Application ID">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MemberID" HeaderText="Member ID">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Amount">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("AmountInPence") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("RealAmount")  %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
           <asp:ObjectDataSource ID="odsPendingFPFiles" runat="server" 
                    SelectMethod="GetAllPendingPaymentFiles" 
                    TypeName="PollokCU.DataAccess.Layer.clsFasterPaymentBatch">
                </asp:ObjectDataSource>
            </td>
          </tr>
        <tr>
        <td width="10" valign="top">
            &nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle" align="left" >
               <asp:Button ID="btnAuthorise" runat="server" Text="Mark Processed"  Width="156px" /> 
                                                            </td>
          </tr>
         
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsNewMemberFileListALPS" runat="server" 
                    SelectMethod="GetAllFasterPaymentFiles" 
                    TypeName="PollokCU.DataAccess.Layer.clsFasterPaymentBatch">
                </asp:ObjectDataSource>
                <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsNewMemberFileListALPS" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True" 
                    OnRowDataBound="gvBatches_RowDataBound">
                    <Columns>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelectBacsLoan" runat="server" />
                                <asp:HiddenField ID="hfAllowAuth" runat="server"  Value='<%# Bind("Processed")%>'/>
                                <asp:HiddenField ID="hfBacsLoanID" runat="server" Value='<%# Bind("FasterPaymentBatchID")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FasterPaymentBatchID" HeaderText="Batch ID" 
                            SortExpression="FasterPaymentBatchID" />
                         <asp:BoundField DataField="PaymentCount" HeaderText="Payment Count" 
                            SortExpression="PaymentCount" />
                        <asp:TemplateField HeaderText="Created Date" 
                            SortExpression="ApplicationReceivedDate">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" 
                                    Text='<%# Bind("CreatedDateTime")%>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblReceivedDate" runat="server" 
                                    Text='<%# Bind("CreatedDateTime", "{0:d}")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditALPS" runat="server" 
                                    ImageUrl="~/images/btn_view.png" 
                                    NavigateUrl='<%# "..\FasterPayments\Files\" & Eval("FileName")%>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    
                              
                    
                </td>
          </tr>
          <tr>
             
            <td  >
                </td>
               
            </tr>
          <tr>
            <td>
                <p>For security reasons, when you have finished using staff services always select LOG OUT.</p></td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

