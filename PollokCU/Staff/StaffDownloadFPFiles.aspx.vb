﻿
Partial Class StaffDownloadFPFiles
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Session("StaffData") IsNot Nothing Then
            btnAuthorise.Visible = CType(Session("StaffData"), StaffDetails).AllowBacsLoanAuth
        End If
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub


    Sub gvBatches_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim hfAllowAuth As HiddenField = CType(e.Row.FindControl("hfAllowAuth"), HiddenField)
            Dim chkSelectBacsLoan As CheckBox = CType(e.Row.FindControl("chkSelectBacsLoan"), CheckBox)
            Dim downloadImg As HyperLink = CType(e.Row.FindControl("hlEditALPS"), HyperLink)

            Dim hfValidAccount As HiddenField = CType(e.Row.FindControl("hfValidAccount"), HiddenField)
            Dim lblAccDetails As Label = CType(e.Row.FindControl("lblAccDetails"), Label)
            Dim lblReceivedDate As Label = CType(e.Row.FindControl("lblReceivedDate"), Label)

            If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                If CBool(hfAllowAuth.Value) Then
                    chkSelectBacsLoan.Visible = False
                    'downloadImg.Visible = False
                Else
                    chkSelectBacsLoan.Visible = True
                    downloadImg.Visible = True
                End If
                'If Not CBool(hfValidAccount.Value) Then
                'lblAccDetails.ForeColor = Drawing.Color.Red
                'End If

                'Dim recDate As Date = Nothing
                'Date.TryParse(lblReceivedDate.Text, recDate)
                'If recDate > Date.MinValue AndAlso DateDiff(DateInterval.Day, recDate, Now()) > 10 Then
                'lblReceivedDate.ForeColor = Drawing.Color.Red
            End If
            'End If
        End If
    End Sub

    Protected Sub btnAuthorise_Click(sender As Object, e As System.EventArgs) Handles btnAuthorise.Click
        Try
            Dim objFasterPaymentData As PollokCU.DataAccess.Layer.clsFasterPaymentBatch = New PollokCU.DataAccess.Layer.clsFasterPaymentBatch

            'lblInfoMessage.Visible = False
            lblInfoMessage.Visible = False

            For Each gr As GridViewRow In gvBatches.Rows
                Dim chkBox As CheckBox = Nothing
                chkBox = TryCast(gr.Cells(0).Controls(1), CheckBox)

                Dim hfId As HiddenField = Nothing

                If chkBox IsNot Nothing AndAlso chkBox.Checked Then
                    hfId = TryCast(gr.Cells(0).Controls(5), HiddenField)
                    If hfId IsNot Nothing AndAlso hfId.Value > 0 Then
                        objFasterPaymentData.UpdateFasterPaymentFiles(CInt(hfId.Value), CInt(Session("StaffID")))
                    Else
                        lblInfoMessage.Visible = True
                        lblInfoMessage.Text = "Select File(s) to process"
                    End If

                End If
            Next
            Response.Redirect("StaffDownloadFPFiles.aspx")
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblInfoMessage.Visible = True
            lblInfoMessage.Text = ex.Message
        End Try
    End Sub
End Class
