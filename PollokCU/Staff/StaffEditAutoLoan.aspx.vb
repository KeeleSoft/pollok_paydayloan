﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class StaffEditAutoLoan
    Inherits System.Web.UI.Page

    Dim objAutoLoan As PollokCU.DataAccess.Layer.clsAutoLoan = New PollokCU.DataAccess.Layer.clsAutoLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        PopulateControls()
        Me.btnFastPay.Attributes.Add("onclick", "return confirm('Are you sure you want to send a faster payment?');")
        Me.btnSendToBacsFile.Attributes.Add("onclick", "return confirm('Are you sure you want to send the application to BACS file?');")
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            Dim objConfig As New System.Configuration.AppSettingsReader
            Dim StaffIDApproved As Integer = 0
            Dim StaffIDPaymentSent As Integer = 0
            Dim WorldpayStatus As Int32 = -1
            lblMessage.Visible = True

            Dim dtTXN As DataTable
            dtTXN = objAutoLoan.GetAutoLoanDetail(Request.QueryString("ID"))

            If dtTXN.Rows.Count > 0 Then
                litID.Text = dtTXN.Rows(0).Item("AutoLoanID")
                If dtTXN.Rows(0).Item("AppType") = "PayDay" Then
                    hlCollectSchedule.Visible = True
                    hlCollectSchedule.NavigateUrl = "StaffEditPDLPaymentCollections.aspx?AppType=PDLALPS&ID=" & dtTXN.Rows(0).Item("AutoLoanID")
                    btnSendReConfirmEmailForCUCA.Enabled = True
                Else
                    hlCollectSchedule.Visible = False
                    btnSendReConfirmEmailForCUCA.Enabled = False
                End If

                'hlEditLoan.NavigateUrl = "StaffEditPDLPreConfirmedLoan.aspx?SessionID=" & dtTXN.Rows(0).Item("SessionID")
                litMemberID.Text = IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), "")
                litLoanAmount.Text = FormatNumber(dtTXN.Rows(0).Item("LoanAmount"), 2)
                litFinalLoanAmount.Text = FormatNumber(dtTXN.Rows(0).Item("FinalLoanAmount"), 2)
                litFinalTerm.Text = FormatNumber(dtTXN.Rows(0).Item("FinalLoanTerm"), 0)
                litFinalPayment.Text = FormatNumber(dtTXN.Rows(0).Item("FinalMonthlyPayment"), 2)
                litRequestedDate.Text = dtTXN.Rows(0).Item("CreatedDate")
                litCompletedLevel.Text = dtTXN.Rows(0).Item("CompletedLevel")
                litApplicantName.Text = dtTXN.Rows(0).Item("FullName")
                litApplicantTypeDesc.Text = dtTXN.Rows(0).Item("AppType")
                litApplyMethod.Text = dtTXN.Rows(0).Item("ApplyMethod")
                litEmail.Text = dtTXN.Rows(0).Item("Email")
                litMobile.Text = dtTXN.Rows(0).Item("MobileNumber")
                If dtTXN.Rows(0).Item("PaymentMethod") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("PaymentMethod") = "FP" Then
                    litFasterPayment.Text = "YES"
                Else
                    litFasterPayment.Text = "NO"
                End If
                WorldpayStatus = Convert.ToInt32(dtTXN.Rows(0).Item("FuturePay"))
                If (WorldpayStatus = -1) Then
                    litWPDesc.Text = "Not completed"
                    litWorldPayment.Text = "0"
                ElseIf (WorldpayStatus > 0) Then
                    litWPDesc.Text = "Completed"
                    litWorldPayment.Text = WorldpayStatus
                ElseIf (WorldpayStatus = 0) Then
                    litWPDesc.Text = "Failed"
                    litWorldPayment.Text = WorldpayStatus
                End If
                Dim outcome As Integer = dtTXN.Rows(0).Item("LoanOutcomeFlag")
                Select Case outcome
                    Case 0
                        If dtTXN.Rows(0).Item("AppType") <> "PayDay" Then
                            litLoanOutcome.Text = "Not Completed"
                        Else
                            litLoanOutcome.Text = "Not Applicable"
                        End If
                    Case 1
                        litLoanOutcome.Text = "Loan agreed, no conditions"
                    Case 2
                        litLoanOutcome.Text = "No STL Option Selected"
                    Case 3
                        litLoanOutcome.Text = "Yes STL Option Selected"
                    Case 4
                        litLoanOutcome.Text = "Appealed Selected"
                    Case Else
                        litLoanOutcome.Text = "Unknown"
                End Select

                litOneThirdDesc.Text = dtTXN.Rows(0).Item("OneThirdRuleStatusDesc")
                litShareToLoanDesc.Text = dtTXN.Rows(0).Item("ShareToLoanRuleStatusDesc")
                litPDLDesc.Text = dtTXN.Rows(0).Item("PayDayLoanRuleStatusDesc")

                Try
                    'litInputCurrRepayment.Text = dtTXN.Rows(0).Item("InputCurrentRepayments")
                    'litInputShareBalance.Text = dtTXN.Rows(0).Item("InputShareBalance")
                    'litInputExistingLoan.Text = dtTXN.Rows(0).Item("InputExistingLoanBalance")
                    'litInputRiskLimit.Text = dtTXN.Rows(0).Item("InputRiskLimit")
                    'litInputTotalIncome.Text = dtTXN.Rows(0).Item("InputTotalIncome")
                Catch ex As Exception
                    lblMessage.Visible = True
                    lblMessage.Text = "Error populating input fields"
                End Try

                Try
                    'litMaxLoan1.Text = dtTXN.Rows(0).Item("TotalMaxLoanLimitGivenRepayments")
                    'litMaxLoan2.Text = dtTXN.Rows(0).Item("TotalMaxLoanLimitGivenRepaymentsSTL")
                    'litCurrentRisk.Text = dtTXN.Rows(0).Item("CurrentRisk")
                    'litMaxLoan.Text = dtTXN.Rows(0).Item("MaxLoan")
                    'litMaxRepay1.Text = dtTXN.Rows(0).Item("MaxRepaymentForLoanFixed")
                    'litMaxRepay2.Text = dtTXN.Rows(0).Item("MaxRepaymentForLoanFixedSTL")
                    'litIncomeRatio.Text = dtTXN.Rows(0).Item("TotalIncomeAfterRatio")
                    'litHighIncome1.Text = dtTXN.Rows(0).Item("HighestIncomeSelected")
                    'litHighIncome2.Text = dtTXN.Rows(0).Item("HighestIncomeSelectedSTL")
                    'litShareBalAfter.Text = dtTXN.Rows(0).Item("ShareBalanceAfterSTL")
                    'litCurrLoanBalAfter.Text = dtTXN.Rows(0).Item("CurrentLoanBalanceAfterSTL")
                    'litSTLReduction.Text = dtTXN.Rows(0).Item("STLReductionAmount")

                    'litNSTLEntitlement.Text = dtTXN.Rows(0).Item("NoSTLLoanEntitlement")
                    'litNSTLLoanBal.Text = dtTXN.Rows(0).Item("NoSTLTotalLoanBalance")
                    'litNSTLRepay.Text = dtTXN.Rows(0).Item("NoSTLRepaymentAmount")
                    'litNSTLTerm.Text = dtTXN.Rows(0).Item("NoSTLNewLoanTerm")
                    'litNSTLEntitlementPreRisk.Text = dtTXN.Rows(0).Item("NoSTLLoanEntitlementPreRisk")
                    'litNSTLPreRiskBal.Text = dtTXN.Rows(0).Item("NoSTLPreRiskLoanBalance")
                    'litNSTLHigherMax.Text = dtTXN.Rows(0).Item("NoSTLHigherThanMaxReduceFigure")

                    'litYSTLEntitlement.Text = dtTXN.Rows(0).Item("YesSTLLoanEntitlement")
                    'litYSTLLoanBal.Text = dtTXN.Rows(0).Item("YesSTLTotalLoanBalance")
                    'litYSTLRepay.Text = dtTXN.Rows(0).Item("YesSTLRepaymentAmount")
                    'litYSTLTerm.Text = dtTXN.Rows(0).Item("YesSTLNewLoanTerm")
                    'litYSTLEntitlementPreRisk.Text = dtTXN.Rows(0).Item("YesSTLLoanEntitlementPreRisk")
                    'litYSTLPreRiskBal.Text = dtTXN.Rows(0).Item("YesSTLPreRiskLoanBalance")
                    'litYSTLHigherMax.Text = dtTXN.Rows(0).Item("YesSTLHigherThanMaxReduceFigure")

                Catch ex As Exception
                    lblMessage.Visible = True
                    lblMessage.Text = "Error populating calculated fields"
                End Try

                'litOneThirdResult.Text = dtTXN.Rows(0).Item("OneThirdRuleResultString")
                'litDecisionCalcsData.Text = If(dtTXN.Rows(0).Item("DecisionCalcsDataString") Is System.DBNull.Value, "", dtTXN.Rows(0).Item("DecisionCalcsDataString"))
                'litShareToLoanResult.Text = dtTXN.Rows(0).Item("ShareToLoanRuleResultString")
                litPDLResult.Text = dtTXN.Rows(0).Item("PayDayLoanRuleResultString")

                If dtTXN.Rows(0).Item("AppealReason") IsNot System.DBNull.Value AndAlso Not String.IsNullOrEmpty(dtTXN.Rows(0).Item("AppealReason")) Then
                    litAppealReason.Text = "Appeal reason: " & dtTXN.Rows(0).Item("AppealReason")
                    litAppealReason.Visible = True
                Else
                    litAppealReason.Visible = False
                End If

                StaffIDApproved = IIf(dtTXN.Rows(0).Item("StaffID") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("StaffID") > 0, dtTXN.Rows(0).Item("StaffID"), 0)
                StaffIDPaymentSent = IIf(dtTXN.Rows(0).Item("StaffIDPaymentSent") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("StaffIDPaymentSent") > 0, dtTXN.Rows(0).Item("StaffIDPaymentSent"), 0)
                litCreatedByName.Text = dtTXN.Rows(0).Item("StaffNameCreated")
                litApprovedByName.Text = dtTXN.Rows(0).Item("StaffNameAproved")
                If dtTXN.Rows(0).Item("AppType") = "PayDay" AndAlso CBool(Session("PDLAllowPayment")) AndAlso Not CBool(dtTXN.Rows(0).Item("FasterPaymentSent")) Then
                    btnFastPay.Enabled = True
                Else
                    btnFastPay.Enabled = False
                End If
                If (Session("StaffLevel") = 1 OrElse Session("StaffDepartment") = "Accounts") _
                 AndAlso dtTXN.Rows(0).Item("PaymentMethod") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("PaymentMethod") <> "FP" _
                 AndAlso (dtTXN.Rows(0).Item("CUCABACSCSVAllow") Is System.DBNull.Value OrElse Not CBool(dtTXN.Rows(0).Item("CUCABACSCSVAllow"))) Then

                    btnSendToBacsFile.Enabled = True
                Else
                    btnSendToBacsFile.Enabled = False
                End If
                btnReCalc.Enabled = dtTXN.Rows(0).Item("LoanGrantedDate") Is System.DBNull.Value
                litPaidByName.Text = dtTXN.Rows(0).Item("StaffNamePaymentSent")

                ddlStatus.SelectedValue = IIf(dtTXN.Rows(0).Item("ApplicationStatus") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("ApplicationStatus") > 0, dtTXN.Rows(0).Item("ApplicationStatus"), 1)
                If dtTXN.Rows(0).Item("ApplyMethod") = "TextSystem" Then
                    SMSHistoryRow.Visible = True
                Else
                    SMSHistoryRow.Visible = False
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAddNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNote.Click
        Try
            lblMessage.Visible = False

            If txtNotes.Text.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Note required."
                Return
            End If

            objAutoLoan.InsertAutoLoanNote(litID.Text, txtNotes.Text, Session("StaffID"))

            txtNotes.Text = ""
            PopulateNotesGrid()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = "Failed to create note " & vbCrLf & ex.Message
        End Try
    End Sub

    Private Sub PopulateNotesGrid()
        Try
            lblMessage.Visible = False


            odsNotes.Select()
            gvNotes.DataBind()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = "Failed to populate notes grid, " & ex.Message
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblMessage.Visible = False

            Dim iUserID As Integer = Session("StaffID")
            Dim AppStatus As Integer = ddlStatus.SelectedValue


            objAutoLoan.UpdateAutoLoanApplicationByStaff(litID.Text, AppStatus, iUserID)

            Response.Redirect("StaffALApplications.aspx")

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnFastPay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFastPay.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            GenerateFasterPaymentFile(litID.Text)
            objAutoLoan.UpdateAutoLoanGrantedDate(litID.Text)

            lblMessage.Text = "Faster Payment Instructions Generated"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    'Do not generate the CSV and just insert into the table so the scheduled task will pick it up
    Private Sub GenerateFasterPaymentFile(ByVal iLoanID As Integer)
        Dim objPayDayLoan As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
        Dim dtTXN As DataTable
        dtTXN = objAutoLoan.GetAutoLoanDetail(iLoanID)
        If dtTXN IsNot Nothing AndAlso dtTXN.Rows.Count > 0 Then
            Dim dr As DataRow = dtTXN.Rows(0)
            Dim MemberID As Integer = dr("MemberID")
            Dim PayRef As String = "PDLALPS" & iLoanID.ToString & "-" & MemberID.ToString
            Dim SortCode As String = ""
            Dim AccountNumber As String = ""
            Dim AccName As String = ""
            If dr.Item("PaymentSortCodeDec") IsNot System.DBNull.Value Then SortCode = dr.Item("PaymentSortCodeDec")
            If dr.Item("PaymentAccountNumberDec") IsNot System.DBNull.Value Then AccountNumber = dr.Item("PaymentAccountNumberDec")
            If dr.Item("FullName") IsNot System.DBNull.Value Then AccName = dr.Item("FullName")

            Dim LoanAmountInPence As Integer = CInt(dr("FinalLoanAmount")) * 100
            Dim TxnType As Integer = 99
            Dim InstantLoan As Boolean = True

            If LoanAmountInPence > 0 AndAlso Not String.IsNullOrEmpty(SortCode) AndAlso Not String.IsNullOrEmpty(AccountNumber) Then
                objPayDayLoan.InsertPaymentInstruction(iLoanID, MemberID, SortCode _
                                                           , AccountNumber, AccName _
                                                           , PayRef, LoanAmountInPence, 99, Session("StaffID"), InstantLoan, "PDLALPS") '26=Online.User Staff Account

                lblMessage.Visible = True
                lblMessage.Text = "Faster payment entry created"
            Else
                lblMessage.Visible = True
                lblMessage.Text = "Loan amount or Account number and sort code not setup. Please setup account details and loan amount to generate faster payment file"
            End If
        End If
    End Sub

    Protected Sub btnSendToBacsFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendToBacsFile.Click
        Try
            objAutoLoan.UpdateAutoLoanSendToBACS(litID.Text, Session("StaffID"))
            'objAutoLoan.UpdateAutoLoanGrantedDate(litID.Text)
            lblMessage.Text = "BACS payment details are flaged to be included in the next batch file."
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    
    ''' <summary>
    ''' Approval is no longer needed as we have to use a different approach for appealed loans. So leave this method for nows
    ''' </summary>
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblMessage.Visible = True

            Dim StaffID As Integer = Session("StaffID")

            objAutoLoan.UpdateAutoLoanApprovedByStaffID(litID.Text, StaffID)

            lblMessage.Text = "Application approved by staff name is updated."

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnReCalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReCalc.Click
        Response.Redirect("StaffAutoLoanReCalc2.aspx?ID=" & litID.Text)
    End Sub

    Protected Sub btnSendText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendText.Click
        rowSendText.Visible = True
        Dim objMem As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember
        Dim dt As DataTable = objAutoLoan.GetAutoLoanDetail(CInt(litID.Text))
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            If dt.Rows(0).Item("MobileNumber") IsNot System.DBNull.Value Then txtMobile.Text = dt.Rows(0).Item("MobileNumber")
        End If

        Dim msg As String = "Your loan amount is " & litFinalLoanAmount.Text & " and monthly repayment will be " & (Double.Parse(litFinalPayment.Text)).ToString & " for " & litFinalTerm.Text & " months"
        txtTextMessage.Text = msg
    End Sub

    Protected Sub btnSendSMS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendSMS.Click
        Try
            lblMessage.Visible = True

            If txtMobile.Text <> "" AndAlso txtTextMessage.Text <> "" Then

                Dim Username As String
                Dim Password As String
                Dim Account As String
                Dim Recipient As String = String.Empty
                Dim Body As String
                Dim objConfig As New System.Configuration.AppSettingsReader

                Username = CType(objConfig.GetValue("EsendexUserName", GetType(System.String)), System.String)
                Password = CType(objConfig.GetValue("EsendexPassword", GetType(System.String)), System.String) 'set password here
                Account = CType(objConfig.GetValue("EsendexAccountRef", GetType(System.String)), System.String) 'set account reference here
                Body = txtTextMessage.Text
                Recipient = txtMobile.Text

                Dim header As esendex.MessengerHeader

                header = New esendex.MessengerHeader
                header.Username = Username
                header.Password = Password
                header.Account = Account

                Dim service As esendex.SendService

                service = New esendex.SendService
                service.MessengerHeaderValue = header
                service.SendMessage(Recipient, Body, esendex.MessageType.Text)
                objSystemData.InsertTrackingServiceStat(4, 0, Request.ServerVariables("REMOTE_ADDR"), Body, Session("StaffID"))
                objAutoLoan.InsertAutoLoanNote(litID.Text, "SMS Sent: " & txtTextMessage.Text, Session("StaffID"))
                PopulateNotesGrid()
                lblMessage.Text = "SMS Message sent."
                rowSendText.Visible = False
            Else
                lblMessage.Text = "Please enter mobile number and text message to send an SMS"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSendReConfirmEmailForCUCA_Click(sender As Object, e As System.EventArgs) Handles btnSendReConfirmEmailForCUCA.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            objAutoLoan.UpdateForceSagePayAllow(litID.Text, True, Session("StaffID"))
            SendForceSagePayEmail(litID.Text)
            objAutoLoan.InsertAutoLoanNote(litID.Text, "Force WorldPay setup Email Sent", Session("StaffID"))
            lblMessage.Text = "Force WorldPay setup Email Sent"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function SendForceSagePayEmail(ByVal iLoanID As Integer) As Boolean
        Dim sEmailBody As String
        Dim Email, FullName, sEmailFrom, sSubject As String


        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

        Dim dtLoan As DataTable = objAutoLoan.GetAutoLoanDetail(iLoanID)

        If dtLoan.Rows.Count > 0 Then
            FullName = dtLoan.Rows(0).Item("FullName")
            Email = dtLoan.Rows(0).Item("Email")

            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "PCU - WeeLoan WorldPay Setup"

            Dim sClickURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "PDL/PDLAutoLoanWorldPaySetup.aspx?L=" & iLoanID.ToString

            sEmailBody = GetForceWorldPayEmailBody(FullName, sClickURL)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , Email _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "")

            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetForceWorldPayEmailBody(ByVal FullName As String, ByVal URL As String) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FullName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for your recent application for the Pollok Credit Union’s Wee Glasgow Loan. Simply click and follow this link to setup WorldPay payments.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<a href=""" & URL & """>" & URL & "</a>")
            body.AppendLine("<br/>")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email to: <a href="""">theweeloan@pollokcu.com</a>")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our Wee Glasgow Loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Kindest regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function
End Class
