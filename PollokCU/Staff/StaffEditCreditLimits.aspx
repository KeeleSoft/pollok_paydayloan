﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffEditCreditLimits.aspx.vb" Inherits="StaffEditCreditLimits" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Edit Credit Limits 
                &nbsp;<asp:Literal ID="litMemberID" runat="server"></asp:Literal></span></td>
          </tr> 
          <tr>
             <td valign="middle" class="BodyText">
                 <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" />
              </td>
          </tr>         
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsCreditLimits" runat="server" 
                    SelectMethod="GetCreditLimitProductsByMember" 
                     TypeName="PollokCU.DataAccess.Layer.clsCreditLimits">
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="MemberID" 
                            QueryStringField="MemberID" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvMemberList" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsCreditLimits" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" 
                    OnRowDataBound="gvBatches_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="ProductName" HeaderText="Product" 
                            SortExpression="ProductName" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Credit Limit" 
                            SortExpression="ProductAmountLimit">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" 
                                    Text='<%# Bind("ProductAmountLimit") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Hiddenfield ID="hfProductAmountLimit" runat="server" Value='<%# Bind("ProductAmountLimit") %>'></asp:Hiddenfield>
                                <asp:TextBox ID="txtProductAmountLimit" runat="server" 
                                    Text='<%# Bind("ProductAmountLimit","{0:N0}") %>' Width="65px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Annual Limit" SortExpression="ProductCountLimit">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" 
                                    Text='<%# Bind("ProductCountLimit","{0:N0}") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:hiddenfield ID="hfProductCountLimit" runat="server" Value='<%# Bind("ProductCountLimit") %>'></asp:hiddenfield>
                                <asp:TextBox ID="txtProductCountLimit" runat="server" 
                                    Text='<%# Bind("ProductCountLimit") %>' Width="65px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notes">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNote" runat="server" Width="200px"></asp:TextBox>
                                <asp:Label ID="lblNoteRequired" runat="server" CssClass="errormsg" Text="*" 
                                    Visible="False"></asp:Label>
                                <asp:HiddenField ID="hfCreditLimitProductID" runat="server" Value='<%# Bind("CreditLimitProductID") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server" 
                                    ImageUrl="~/images/icon_edit.png" 
                                    NavigateUrl='<%# "StaffEditCreditLimits.aspx?MemberID=" & Request.QueryString("MemberID") & "&LimitID=" & Eval("LimitID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>                    
                </td>
          </tr>
          <tr>
             <td valign="middle" class="BodyText">
                 <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" />
              </td>
          </tr> 
          <!-- TemplateEndEditable -->
          <tr>
             <td valign="middle" class="BodyText">
                 &nbsp;</td>
          </tr> 
          <tr>
             <td valign="middle" class="BodyText">
                 <strong>Change history<asp:ObjectDataSource ID="odsCreditLimitHistory" runat="server" 
                    SelectMethod="GetCreditLimitProductChangeHistory" 
                     TypeName="PollokCU.DataAccess.Layer.clsCreditLimits">
                    <SelectParameters>
                        <asp:QueryStringParameter DefaultValue="0" Name="limitID" 
                            QueryStringField="LimitID" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                 </strong></td>
          </tr> 
          <tr>
             <td valign="middle" class="BodyText">
                <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsCreditLimitHistory" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" 
                    OnRowDataBound="gvBatches_RowDataBound" 
                     PageSize="100">
                    <Columns>
                        <asp:BoundField DataField="ProductName" HeaderText="Product" 
                            SortExpression="ProductName" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="StaffName" HeaderText="Created By" 
                            SortExpression="StaffName" />
                        <asp:BoundField DataField="ProductAmountLimitFrom" DataFormatString="{0:N0}" 
                            HeaderText="Limit From" SortExpression="ProductAmountLimitFrom" />
                        <asp:BoundField DataField="ProductAmountLimitTo" DataFormatString="{0:N0}" 
                            HeaderText="Limit To" SortExpression="ProductAmountLimitTo" />
                        <asp:BoundField DataField="ProductCountLimitFrom" 
                            HeaderText="Annual Limit From" SortExpression="ProductCountLimitFrom" />
                        <asp:BoundField DataField="ProductCountLimitTo" HeaderText="Annual Limit To" 
                            SortExpression="ProductCountLimitTo" />
                        <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
                    </Columns>
                </asp:GridView>                    
                </td>
          </tr> 
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

