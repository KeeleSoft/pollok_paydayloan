﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffEditCreditLimits
    Inherits System.Web.UI.Page

    Dim objCreditLimits As PollokCU.DataAccess.Layer.clsCreditLimits = New PollokCU.DataAccess.Layer.clsCreditLimits
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete
        litMemberID.Text = " - " & Request.QueryString("MemberID")
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            ' Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Sub gvBatches_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim hfDuplicated As HiddenField = CType(e.Row.FindControl("hfDuplicated"), HiddenField)
        '    Dim hfSuccessfulBacs As HiddenField = CType(e.Row.FindControl("hfSuccessfulBacs"), HiddenField)
        '    Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

        '    If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
        '        If CBool(hfDuplicated.Value) Then
        '            lblName.ForeColor = Drawing.Color.Red
        '        ElseIf CBool(hfSuccessfulBacs.Value) Then
        '            lblName.ForeColor = Drawing.Color.Green
        '        End If
        '    End If
        'End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            Dim memberID As Integer = Request.QueryString("MemberID")
            Dim UserID As Integer = Session("StaffID")
            Dim txtProductAmountLimit As TextBox = Nothing
            Dim hfProductAmountLimit As HiddenField = Nothing
            Dim txtProductCountLimit As TextBox = Nothing
            Dim hfProductCountLimit As HiddenField = Nothing
            Dim txtNote As TextBox = Nothing
            Dim lblNoteRequired As Label = Nothing
            Dim hfCreditLimitProductID As HiddenField = Nothing

            Dim creditLimit As Double = 0
            Dim annualLimit As Double = 0
            Dim saveChanges As Boolean = False
            Dim noErrors As Boolean = True
            lblMessage.Text = ""
            For Each gridRow As GridViewRow In gvMemberList.Rows
                txtProductAmountLimit = CType(gridRow.Cells(0).FindControl("txtProductAmountLimit"), TextBox)
                hfProductAmountLimit = CType(gridRow.Cells(0).FindControl("hfProductAmountLimit"), HiddenField)

                txtProductCountLimit = CType(gridRow.Cells(0).FindControl("txtProductCountLimit"), TextBox)
                hfProductCountLimit = CType(gridRow.Cells(0).FindControl("hfProductCountLimit"), HiddenField)

                hfCreditLimitProductID = CType(gridRow.Cells(0).FindControl("hfCreditLimitProductID"), HiddenField)
                txtNote = CType(gridRow.Cells(0).FindControl("txtNote"), TextBox)
                lblNoteRequired = CType(gridRow.Cells(0).FindControl("lblNoteRequired"), Label)
                Double.TryParse(txtProductAmountLimit.Text, creditLimit)
                Integer.TryParse(txtProductCountLimit.Text, annualLimit)
                If txtNote.Text.Length = 0 AndAlso (creditLimit <> CDbl(hfProductAmountLimit.Value) OrElse annualLimit <> CInt(hfProductCountLimit.Value)) Then
                    lblNoteRequired.Visible = True
                    noErrors = False
                    lblMessage.Text = "Please enter a note for each credit limit you would like to amend"
                Else
                    lblNoteRequired.Visible = False
                End If

                If txtNote.Text.Length > 0 AndAlso (creditLimit <> CDbl(hfProductAmountLimit.Value) OrElse annualLimit <> CInt(hfProductCountLimit.Value)) Then
                    'Save changes
                    objCreditLimits.InsertUpdateCreditLimitProduct(memberID, hfCreditLimitProductID.Value, creditLimit, annualLimit, txtNote.Text, UserID)
                    saveChanges = True
                End If
            Next

            If saveChanges AndAlso noErrors Then
                odsCreditLimits.Select()
                gvMemberList.DataBind()
            End If



        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
