﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Staff_StaffEditPDLLoanDetails
    Inherits System.Web.UI.Page

    Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            PopulateDOBYear()
            PopulateYears()
            PopulateControls()
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            Dim dtTXN As DataTable
            dtTXN = objPDL.SelectPayDayLoanDetail(Request.QueryString("ID"))

            If dtTXN.Rows.Count > 0 Then
                With dtTXN.Rows(0)
                    litID.Text = .Item("ID")
                    hfID.Value = .Item("ID")
                    litMemberID.Text = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), "")
                    ddlTitle.SelectedValue = .Item("SalutationID")
                    txtFirstName.Text = .Item("FirstName")
                    txtMiddleName.Text = .Item("MiddleName")
                    If .Item("Gender") IsNot System.DBNull.Value Then ddlGender.SelectedValue = .Item("Gender")
                    txtSurName.Text = .Item("SurName")
                    txtPlaceofBirth.Text = .Item("PlaceOfBirth")
                    txtMotherName.Text = .Item("MotherName")
                    ddlDay.SelectedValue = DatePart(DateInterval.Day, .Item("DateOfBirth"))
                    ddlMonth.SelectedValue = DatePart(DateInterval.Month, .Item("DateOfBirth"))
                    ddlYear.SelectedValue = DatePart(DateInterval.Year, .Item("DateOfBirth"))
                    txtPostCode1.Text = .Item("CurrentPostCode")
                    txtAddLine1_1.Text = .Item("CurrentAddressLine1")
                    txtAddLine2_1.Text = .Item("CurrentAddressLine2")
                    txtCity1.Text = .Item("CurrentCity")
                    txtCounty1.Text = .Item("CurrentCounty")
                    ddlTimeYears.SelectedValue = .Item("CurrentAddressNoOfYears")
                    ddlTimeMonths.SelectedValue = IIf(.Item("CurrentAddressNoOfMonths") Is System.DBNull.Value, "0", .Item("CurrentAddressNoOfMonths"))

                    txtPostCode2.Text = .Item("PreviousPostCode")
                    txtAddLine1_2.Text = .Item("PreviousAddressLine1")
                    txtAddLine2_2.Text = .Item("PreviousAddressLine2")
                    txtCity2.Text = .Item("PreviousCity")
                    txtCounty2.Text = .Item("PreviousCounty")

                    txtHomeTel.Text = .Item("HomeTelephone")
                    txtMobile.Text = .Item("Mobile")
                    txtEmail.Text = .Item("Email")
                    txtNI.Text = .Item("NINumber")
                    ddlMaritalStatus.SelectedValue = .Item("MaritalStatus")
                    ddlDependents.Text = .Item("NoOfDependants")
                    ddlResidency.Text = IIf(.Item("ResidencyStatus") > 0, .Item("ResidencyStatus"), "")

                    txtAccountNumber.Text = .Item("AccountNumberDec")
                    txtSortCode.Text = .Item("AccountSortCodeDec")
                    txtBankName.Text = .Item("BankName")
                    If .Item("AccountOwner") IsNot System.DBNull.Value Then ddlAccountOwner.SelectedValue = .Item("AccountOwner")
                    If .Item("TimeWithBankYear") IsNot System.DBNull.Value Then ddlTimeWithBankYears.SelectedValue = .Item("TimeWithBankYear")
                    If .Item("TimeWithBankMonth") IsNot System.DBNull.Value Then ddlTimeWithBankMonths.SelectedValue = .Item("TimeWithBankMonth")
                    If .Item("PaymentFrequencyText") IsNot System.DBNull.Value Then ddlPayFrequency.SelectedValue = .Item("PaymentFrequencyText")

                    txtNextPayDay.Text = If(.Item("NextPayDay") Is System.DBNull.Value, "", .Item("NextPayDay"))
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub PopulateDOBYear()
        Dim iCurrentYear As Integer = DateAndTime.Year(Now())

        For i As Integer = iCurrentYear - 18 To iCurrentYear - 90 Step -1
            ddlYear.Items.Add(i)
        Next

    End Sub

    Private Sub PopulateYears()
        ddlTimeYears.Items.Clear()

        Dim li As ListItem = New ListItem
        li.Text = "YEARS"
        li.Value = ""

        ddlTimeYears.Items.Add(li)

        For i As Integer = 0 To 80
            li = New ListItem
            li.Text = i
            li.Value = i
            ddlTimeYears.Items.Add(li)
            li = Nothing
        Next

        ddlTimeWithBankYears.Items.Clear()
        ddlTimeWithBankYears.Items.Add("YEAR")

        For i As Integer = Now.Year - 50 To Now.Year
            ddlTimeWithBankYears.Items.Add(i)
        Next
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            Dim iLoanID As Integer = hfID.Value
            Dim SalutationID As Int32 = ddlTitle.SelectedValue
            Dim FirstName As String = txtFirstName.Text
            Dim SurName As String = txtSurName.Text
            Dim MiddleName As String = txtMiddleName.Text
            Dim Gender As String = ddlGender.SelectedValue
            Dim DateOfBirth As DateTime = ddlDay.SelectedValue & "/" & ddlMonth.SelectedValue & "/" & ddlYear.SelectedValue
            Dim PlaceOfBirth As String = txtPlaceofBirth.Text
            Dim MotherName As String = txtMotherName.Text
            Dim CurrentAddressLine1 As String = txtAddLine1_1.Text
            Dim CurrentAddressLine2 As String = txtAddLine2_1.Text
            Dim CurrentCity As String = txtCity1.Text
            Dim CurrentCounty As String = txtCounty1.Text
            Dim CurrentPostCode As String = txtPostCode1.Text
            Dim CurrentAddressNoOfYears As Int32 = IIf(ddlTimeYears.SelectedIndex = 0, 0, ddlTimeYears.SelectedValue)
            Dim CurrentAddressNoOfMonths As Int32 = IIf(ddlTimeMonths.SelectedIndex = 0, 0, ddlTimeMonths.SelectedValue)
            Dim HomeTelephone As String = txtHomeTel.Text
            Dim Mobile As String = txtMobile.Text
            Dim Email As String = txtEmail.Text
            Dim NINumber As String = txtNI.Text
            Dim MaritalStatus As Integer = ddlMaritalStatus.SelectedValue
            Dim NoOfDependants As Int32 = ddlDependents.SelectedValue
            Dim PreviousAddressLine1 As String = txtAddLine1_2.Text
            Dim PreviousAddressLine2 As String = txtAddLine2_2.Text
            Dim PreviousCity As String = txtCity2.Text
            Dim PreviousCounty As String = txtCounty2.Text
            Dim PreviousPostCode As String = txtPostCode2.Text
            Dim ResidencyStatus As Integer = IIf(ddlResidency.SelectedValue = "", 0, ddlResidency.SelectedValue)

            Dim AccountNumber As String = txtAccountNumber.Text
            Dim SortCode As String = txtSortCode.Text
            Dim AccountType As String = "Current"
            Dim CustomerAccountType As String = "Personal"
            Dim AccountOwner As String = ddlAccountOwner.SelectedValue
            Dim BankName As String = txtBankName.Text
            Dim TimeWithBankYears As Integer = ddlTimeWithBankYears.SelectedValue   'This is the opened year
            Dim TimeWithBankMonths As Integer = ddlTimeWithBankMonths.SelectedValue 'Opened month
            Dim PaymentFrequencyText As String = ddlPayFrequency.SelectedValue

            Dim NextPayDay As Date = txtNextPayDay.Text

            objPDL.InsertUpdateApplication("PayDayLoan" _
                                    , "" _
                                    , 0 _
                                    , 0 _
                                    , SalutationID _
                                    , FirstName _
                                    , SurName _
                                    , MiddleName _
                                    , Gender _
                                    , DateOfBirth _
                                    , PlaceOfBirth _
                                    , MotherName _
                                    , CurrentAddressLine1 _
                                    , CurrentAddressLine2 _
                                    , CurrentCity _
                                    , CurrentCounty _
                                    , CurrentPostCode _
                                    , CurrentAddressNoOfYears _
                                    , CurrentAddressNoOfMonths _
                                    , HomeTelephone _
                                    , Mobile _
                                    , Email _
                                    , NINumber _
                                    , MaritalStatus _
                                    , NoOfDependants _
                                    , PreviousAddressLine1 _
                                    , PreviousAddressLine2 _
                                    , PreviousCity _
                                    , PreviousCounty _
                                    , PreviousPostCode _
                                    , ResidencyStatus _
                                    , 0, 0, 0, 0, 0 _
                                    , Session("StaffID") _
                                    , iLoanID _
                                    , , , , , , , , , , , , , , , , , , , , NextPayDay, , _
                                    , AccountNumber _
                                    , SortCode _
                                    , _
                                    , BankName _
                                    , TimeWithBankYears _
                                    , TimeWithBankMonths _
                                    , , , , , , , , , , , , , , , , _
                                    , AccountType _
                                    , CustomerAccountType _
                                    , AccountOwner _
                                    , PaymentFrequencyText _
                            )

            lblMessage.Visible = True
            lblMessage.Text = "Details updated..."

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
