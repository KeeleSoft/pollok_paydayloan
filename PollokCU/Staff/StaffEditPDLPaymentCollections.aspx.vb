﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Staff_StaffEditPDLPaymentCollections
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            If Request.QueryString("CollectionID") > 0 Then
                pnlEditCollection.Visible = True
                PopulateEditCollection(Request.QueryString("CollectionID"))
            Else
                pnlEditCollection.Visible = False
            End If

            If Request.QueryString("Action") IsNot Nothing AndAlso Request.QueryString("Action") = "Add" Then
                pnlAddCollection.Visible = True
            Else
                pnlAddCollection.Visible = False
            End If

        End If
    End Sub

    Private Sub PopulateEditCollection(ByVal CollectionID As Integer)
        Try
            Me.btnDeleteCollection.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this payment schedule entry?');")

            Dim collectionData As DataTable = objSage.GetSageCollectionsScheduleByCollectionID(CollectionID)
            If collectionData IsNot Nothing AndAlso collectionData.Rows.Count > 0 Then
                With collectionData.Rows(0)
                    hfCollectionID.Value = .Item("SagePayCollectionID")
                    litSequence.Text = .Item("SequenceID")
                    txtAmount.Text = .Item("Amount")
                    txtScheduleTime.Text = .Item("ScheduleDateTime")
                    txtNote.Text = If(.Item("Note") IsNot System.DBNull.Value, .Item("Note"), "")
                    ddlPaymentTake.Text = .Item("PaymentTakenDesc")

                    If CBool(.Item("PaymentTaken")) Then
                        btnDeleteCollection.Enabled = False
                    Else
                        btnDeleteCollection.Enabled = True
                    End If

                    If .Item("BatchID") IsNot System.DBNull.Value AndAlso .Item("BatchID") > 0 Then
                        ProcessedInfoRow.Visible = True
                        Dim info As String = "BatchID: " & .Item("BatchID").ToString
                        info = info & ", Status: " & If(.Item("ResponseStatus") IsNot System.DBNull.Value, .Item("ResponseStatus").ToString, "")
                        info = info & ", Status Details: " & If(.Item("ResponseStatusDetail") IsNot System.DBNull.Value, .Item("ResponseStatusDetail").ToString, "")
                        info = info & ", VPSTxId: " & If(.Item("ResponseVPSTxId") IsNot System.DBNull.Value, .Item("ResponseVPSTxId").ToString, "")
                        info = info & ", ResponseSecurityKey: " & If(.Item("ResponseSecurityKey") IsNot System.DBNull.Value, .Item("ResponseSecurityKey").ToString, "")
                        info = info & ", ResponseTxAuthNo: " & If(.Item("ResponseTxAuthNo") IsNot System.DBNull.Value, .Item("ResponseTxAuthNo").ToString, "")
                        litCollectionDetails.Text = info
                    Else
                        ProcessedInfoRow.Visible = False
                    End If
                End With
            Else
                lblMessage.Visible = True
                lblMessage.Text = "Invalid Payment Collection ID and no data found to edit"
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    
    Protected Sub btnSaveCollection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCollection.Click
        Try
            lblMessage.Visible = False

            Dim AppID As Integer = Request.QueryString("ID")
            Dim CollectionID As Integer = hfCollectionID.Value
            Dim ScheduleDateTime As DateTime = Nothing
            DateTime.TryParse(txtScheduleTime.Text, ScheduleDateTime)
            If IsNothing(ScheduleDateTime) Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid scheduled date time Entered"
                Return
            End If

            Dim Amount As Double = 0
            Double.TryParse(txtAmount.Text, Amount)
            If IsNothing(Amount) OrElse Amount = 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid amount Entered"
                Return
            End If

            Dim PaymentTaken As Boolean = False
            If ddlPaymentTake.SelectedIndex = 0 Then
                PaymentTaken = 0
            Else
                PaymentTaken = 1
            End If
            Dim Note As String = txtNote.Text

            objSage.UpdateSagePayCollectionEntry(CollectionID, Amount, ScheduleDateTime, PaymentTaken, Note)
            Response.Redirect("StaffEditPDLPaymentCollections.aspx?AppType=" & Request.QueryString("AppType") & "&ID=" & AppID.ToString)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnDeleteCollection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteCollection.Click
        Try
            Dim AppID As Integer = Request.QueryString("ID")
            objSage.DeleteSagePayCollectionEntry(hfCollectionID.Value)
            Response.Redirect("StaffEditPDLPaymentCollections.aspx?AppType=" & Request.QueryString("AppType") & "&ID=" & AppID.ToString)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveNewCollection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNewCollection.Click
        Try
            Dim AppID As Integer = Request.QueryString("ID")
            Dim AppType As String = Request.QueryString("AppType")

            Dim ScheduleDateTime As DateTime = Nothing
            DateTime.TryParse(txtScheduleTimeNew.Text, ScheduleDateTime)
            If IsNothing(ScheduleDateTime) OrElse ScheduleDateTime = Date.MinValue Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid scheduled date time Entered"
                Return
            End If

            Dim Amount As Double = 0
            Double.TryParse(txtAmountNew.Text, Amount)
            If IsNothing(Amount) OrElse Amount = 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Invalid amount Entered"
                Return
            End If

            Dim PaymentTaken As Boolean = False
            If ddlPaymentTakeNew.SelectedIndex = 0 Then
                PaymentTaken = 0
            Else
                PaymentTaken = 1
            End If
            Dim Note As String = txtNoteNew.Text

            objSage.InsertSagePayCollectionEntry(AppID, Amount, ScheduleDateTime, PaymentTaken, Note, AppType)

            Response.Redirect("StaffEditPDLPaymentCollections.aspx?AppType=" & Request.QueryString("AppType") & "&ID=" & AppID.ToString)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAddNewCollection0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewCollection.Click
        Try
            Dim AppID As Integer = Request.QueryString("ID")

            Response.Redirect("StaffEditPDLPaymentCollections.aspx?Action=Add&AppType=" & Request.QueryString("AppType") & "&ID=" & AppID.ToString)
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
