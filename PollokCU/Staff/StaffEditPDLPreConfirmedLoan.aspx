﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffEditPDLPreConfirmedLoan.aspx.vb" Inherits="Staff_StaffEditPDLPreConfirmedLoan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 80%;" class="BodyText">
        <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Amend Pay Day Loan Details</span></td>
          </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                </td>
        </tr>
        <tr>
            <td>
                                  &nbsp;</td>
        </tr>
        <tr>
            <td>
                      <table style="width:100%;">
                          <tr>
                              <td width="40%">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td width="40%">
                                  &nbsp;Session Reference:</td>
                              <td>
                                  <asp:Literal ID="litSessionID" runat="server"></asp:Literal>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Member ID:</td>
                              <td>
                                  <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Loan Amount:<asp:RequiredFieldValidator ID="RequiredFieldValidator30" 
                                      runat="server" ControlToValidate="txtLoanAmount" 
                                      ErrorMessage="Amount required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtLoanAmount" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Period:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                      runat="server" ControlToValidate="ddlPeriod" 
                                      ErrorMessage="Period required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlPeriod" runat="server" Width="208px">
                                      <asp:ListItem Value="1">1 Month</asp:ListItem>
                                      <asp:ListItem Value="2">2 Months</asp:ListItem>
                                      <asp:ListItem Value="3">3 Months</asp:ListItem>
                                      <asp:ListItem Value="4">4 Months</asp:ListItem>
                                      <asp:ListItem Value="5">5 Months</asp:ListItem>
                                      <asp:ListItem Value="6">6 Months</asp:ListItem>
                                      <asp:ListItem Value="7">7 Months</asp:ListItem>
                                      <asp:ListItem Value="8">8 Months</asp:ListItem>
                                      <asp:ListItem Value="9">9 Months</asp:ListItem>
                                      <asp:ListItem Value="10">10 Months</asp:ListItem>
                                      <asp:ListItem Value="11">11 Months</asp:ListItem>
                                      <asp:ListItem Value="12">12 Months</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Total 
                                  Interest:<asp:RequiredFieldValidator ID="RequiredFieldValidator204" 
                                      runat="server" ControlToValidate="txtTotalInterest" 
                                      ErrorMessage="Total interest required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtTotalInterest" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Total To Pay:<asp:RequiredFieldValidator ID="RequiredFieldValidator203" 
                                      runat="server" ControlToValidate="txtTotalToPay" 
                                      ErrorMessage="Total to pay required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtTotalToPay" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 1 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator23" 
                                      runat="server" ControlToValidate="txtMonth1Payment" 
                                      ErrorMessage="Month 1 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth1Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 2 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                      runat="server" ControlToValidate="txtMonth2Payment" 
                                      ErrorMessage="Month 2 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth2Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 3 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                      runat="server" ControlToValidate="txtMonth3Payment" 
                                      ErrorMessage="Month 3 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth3Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 4 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                                      runat="server" ControlToValidate="txtMonth4Payment" 
                                      ErrorMessage="Month 4 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth4Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 5 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                                      runat="server" ControlToValidate="txtMonth5Payment" 
                                      ErrorMessage="Month 5 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth5Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 6 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator6" 
                                      runat="server" ControlToValidate="txtMonth6Payment" 
                                      ErrorMessage="Month 6 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth6Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 7 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                                      runat="server" ControlToValidate="txtMonth7Payment" 
                                      ErrorMessage="Month 7 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth7Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 8 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator8" 
                                      runat="server" ControlToValidate="txtMonth8Payment" 
                                      ErrorMessage="Month 8 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth8Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 9 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator9" 
                                      runat="server" ControlToValidate="txtMonth9Payment" 
                                      ErrorMessage="Month 9 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth9Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 10 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator10" 
                                      runat="server" ControlToValidate="txtMonth10Payment" 
                                      ErrorMessage="Month 10 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth10Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 11 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator11" 
                                      runat="server" ControlToValidate="txtMonth11Payment" 
                                      ErrorMessage="Month 11 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth11Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Month 12 Payment:<asp:RequiredFieldValidator ID="RequiredFieldValidator12" 
                                      runat="server" ControlToValidate="txtMonth12Payment" 
                                      ErrorMessage="Month 12 payment required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMonth12Payment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Faster Payment Fee:<asp:RequiredFieldValidator ID="RequiredFieldValidator24" 
                                      runat="server" ControlToValidate="txtInstantPayment" 
                                      
                                      ErrorMessage="Faster payment amount required. Enter 0 if no faster payment">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtInstantPayment" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfSessionID" runat="server" />
                                  <asp:HiddenField ID="hfCUID" runat="server" />
                                  <asp:HiddenField ID="hfInterest" runat="server" />
                                  <asp:HiddenField ID="hfAffordability" runat="server" />
                              </td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                                                  <asp:Button ID="btnNext" runat="server" Text="Submit" Width="100px" BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          </table>
                              </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

