﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Staff_StaffEditPDLPreConfirmedLoan
    Inherits System.Web.UI.Page

    Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            PopulateControls()
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            Dim dtTXN As DataTable
            dtTXN = objPDL.GetPreconfirmedLoan(Request.QueryString("SessionID"), 0)

            If dtTXN.Rows.Count > 0 Then
                With dtTXN.Rows(0)
                    litSessionID.Text = .Item("SessionID")
                    hfSessionID.Value = .Item("SessionID")
                    hfCUID.Value = .Item("CUID")
                    litMemberID.Text = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), "")
                    txtLoanAmount.Text = CDbl(.Item("LoanAmount"))
                    ddlPeriod.SelectedValue = CDbl(.Item("LoanPeriod"))
                    hfAffordability.Value = CDbl(.Item("Affordability"))
                    txtTotalInterest.Text = CDbl(.Item("TotalInterest"))
                    txtTotalToPay.Text = CDbl(.Item("TotalToPay"))
                    txtMonth1Payment.Text = CDbl(.Item("Month1Payment"))
                    txtMonth2Payment.Text = CDbl(.Item("Month2Payment"))
                    txtMonth3Payment.Text = CDbl(.Item("Month3Payment"))
                    txtMonth4Payment.Text = CDbl(.Item("Month4Payment"))
                    txtMonth5Payment.Text = CDbl(.Item("Month5Payment"))
                    txtMonth6Payment.Text = CDbl(.Item("Month6Payment"))
                    txtMonth7Payment.Text = CDbl(.Item("Month7Payment"))
                    txtMonth8Payment.Text = CDbl(.Item("Month8Payment"))
                    txtMonth9Payment.Text = CDbl(.Item("Month9Payment"))
                    txtMonth10Payment.Text = CDbl(.Item("Month10Payment"))
                    txtMonth11Payment.Text = CDbl(.Item("Month11Payment"))
                    txtMonth12Payment.Text = CDbl(.Item("Month12Payment"))
                    txtInstantPayment.Text = CDbl(.Item("InstantLoan"))

                    End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            Dim SessionID As String = hfSessionID.Value
            Dim CUID As Integer = hfCUID.Value
            Dim MemberID As Integer = CInt(IIf(litMemberID.Text.Length > 0, litMemberID.Text, 0))
            Dim LoanAmount As Double = txtLoanAmount.Text
            Dim LoanPeriod As Integer = ddlPeriod.SelectedValue
            Dim Affordability As Double = hfAffordability.Value
            Dim TotalInterest As Double = txtTotalInterest.Text
            Dim TotalToPay As Double = txtTotalToPay.Text
            Dim Month1Pay As Double = txtMonth1Payment.Text
            Dim Month2Pay As Double = txtMonth2Payment.Text
            Dim Month3Pay As Double = txtMonth3Payment.Text
            Dim Month4Pay As Double = txtMonth4Payment.Text
            Dim Month5Pay As Double = txtMonth5Payment.Text
            Dim Month6Pay As Double = txtMonth6Payment.Text
            Dim Month7Pay As Double = txtMonth7Payment.Text
            Dim Month8Pay As Double = txtMonth8Payment.Text
            Dim Month9Pay As Double = txtMonth9Payment.Text
            Dim Month10Pay As Double = txtMonth10Payment.Text
            Dim Month11Pay As Double = txtMonth11Payment.Text
            Dim Month12Pay As Double = txtMonth12Payment.Text
            Dim InstantLoanFee As Double = txtInstantPayment.Text
            Dim FirstSaving As Double = 0
            Dim MembershipFee As Double = 0
            Dim SameDayPay As Double = 0
            Dim RepeatAccFacility As Double = 0
            

            objPDL.UpdateCreatePreConfirmedLoan(SessionID _
                                                , CUID _
                                                , LoanAmount _
                                                , LoanPeriod _
                                                , Affordability _
                                                , TotalInterest _
                                                , TotalToPay _
                                                , Month1Pay _
                                                , Month2Pay _
                                                , Month3Pay _
                                                , Month4Pay _
                                                , Month5Pay _
                                                , Month6Pay _
                                                , Month7Pay _
                                                , Month8Pay _
                                                , Month9Pay _
                                                , Month10Pay _
                                                , Month11Pay _
                                                , Month12Pay _
                                                , MemberID _
                                                , FirstSaving _
                                                , MembershipFee _
                                                , InstantLoanFee _
                                                , SameDayPay _
                                                , RepeatAccFacility, "")

                                   
            lblMessage.Visible = True
            lblMessage.Text = "Loan Details updated..."

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
