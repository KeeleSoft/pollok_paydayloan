﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffEditPDLoan.aspx.vb" Inherits="StaffEditPDLoan" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Pay Day Loan </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width: 100%">
                    <tr>
                        <td width="20%">
                            &nbsp;Weeglasgow Reference:</td>
                        <td width="30%">
                            <asp:Literal ID="litID" runat="server"></asp:Literal>
                        </td>
                        <td width="25%">
                            &nbsp;Member Number:</td>
                        <td width="25%">
                            <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Applicant Type:</td>
                        <td>
                            <asp:Literal ID="litApplicantTypeDesc" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            <asp:Literal ID="litApplicantTypeID" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Applied Date/Time:</td>
                        <td>
                            <asp:Literal ID="litRequestedDate" runat="server"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;Completed Level:</td>
                        <td>
                            <asp:Literal ID="litCompletedLevel" runat="server"></asp:Literal>/5
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Applicant Name:</td>
                        <td>
                            <asp:Literal ID="litApplicantName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            </td>
                        <td colspan="3">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:HyperLink ID="hlPDF" runat="server" Target="_blank">Download PDF</asp:HyperLink>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Approved By:</td>
                        <td>
                            <asp:DropDownList ID="ddlStaff" runat="server" DataSourceID="odsStaff" 
                                DataTextField="StaffName" DataValueField="StaffID" Width="150px">
                            </asp:DropDownList>
                                                                    </td>
                        <td>
                            &nbsp;Application Status:<asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlStatus" 
                                ErrorMessage="Application status required" ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" DataSourceID="odsStatus" 
                                DataTextField="AppStatusDescription" DataValueField="ApplicationStatusID" 
                                Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Paid By:</td>
                        <td>
                            <asp:Literal ID="litPaidByName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;Re-Confirm Email Sent By:</td>
                        <td>
                            <asp:Literal ID="litReConfSentBy" runat="server"></asp:Literal>
                                                                    </td>
                         <td colspan="4" >
                          WorldPay AgreementID:&nbsp; <asp:Literal ID="Literalwpay" runat="server"></asp:Literal>&nbsp;&nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                     <tr>
                        <td>
                            &nbsp;Monthly Income:</td>
                        <td>
                            <asp:Literal ID="litMonthlyIncome" runat="server"></asp:Literal>
                                                                    </td>
                         <td colspan="4" >
                          Partner code:&nbsp; <asp:Literal ID="LiteralPartnerID" runat="server"></asp:Literal>&nbsp;&nbsp;</td> 
                        <td>
                            &nbsp;</td>
                        </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;
                                                                    </td>
                         <td colspan="4" >
                          Epic 360 Status:&nbsp;  <asp:Literal ID="litEpic" runat="server"></asp:Literal>&nbsp;&nbsp;</td> 
                        <td>
                            &nbsp;</td>
                        </tr>
                    <tr>
                        <td>
                            <asp:ObjectDataSource ID="odsStaff" runat="server" 
                                SelectMethod="SelectAllStaff" TypeName="PollokCU.DataAccess.Layer.clsStaffAdmin">
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            <asp:ObjectDataSource ID="odsStatus" runat="server" 
                                SelectMethod="GetAllPDLApplicationStatuses" 
                                TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="vgSave" />
                        </td>
                    </tr>
                     <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;<strong>Application Links</strong></td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <table style="width: 100%;">
                                <tr>
                                    <td width="20%">
                            <asp:HyperLink ID="hlEditDetails" runat="server" Font-Bold="True" 
                                Font-Size="Small" Target="_EditLoan">Edit Application Details</asp:HyperLink>
                                    </td>
                                    <td width="20%" style="margin-left: 40px">
                            <asp:HyperLink ID="hlEditLoan" runat="server" Font-Bold="True" 
                                Font-Size="Small" Target="_EditLoan">Edit Loan Details</asp:HyperLink>
                                    </td>
                                    <td width="20%">
                            <asp:HyperLink ID="hlCollectSchedule" runat="server" Font-Bold="True" 
                                Font-Size="Small" Target="_SageCollect">Collection Schedule</asp:HyperLink>
                                    </td>
                                    <td width="20%">
                            <asp:HyperLink ID="hlExperianReport" runat="server" Target="_blank" Font-Bold="True" 
                                            Font-Size="Small">View Equifax Report</asp:HyperLink>
                                                                    </td>
                                    <td width="20%">
                            <asp:HyperLink ID="hlAgreement" runat="server" Target="_blank" Font-Bold="True" 
                                            Font-Size="Small">Download Agreement</asp:HyperLink>
                                                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td align="center" valign="top" width="15%">
                                        <strong>Account Active Check</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>Employment\Payroll Check</strong></td>
                                    <td align="center" valign="top" width="15%">
                                       <strong>Previous PDL Check</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>Authenticate Check</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>Credit Check</strong></td>
                                    <td align="center" valign="top" width="10%">
                                        <strong>Bank Check</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>WorldPay Status</strong></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="liAccActiveCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litEmpCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litPDLCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litAuthCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litCreditCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litBankCheckDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litInternalBankCheck" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                                       
                    <tr>
                        <td colspan="4">
                            &nbsp;<strong>Manual Overrides:</strong></td>
                        
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnDoAuthenticate" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Authenticate" Width="100px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDoCreditSearch" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Credit Search" Width="100px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnDoBankCheck" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Bank Check" Width="120px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCreatePDF" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Re-Create PDF" Width="100px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCreateAgreement" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Create Agreement" Width="130px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnFastPay" runat="server" BackColor="Red" 
                                            Height="50px" Text="Send To Faster Payment" Width="150px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSendToCUCA" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Send To CUCA" Width="100px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSendToCSV" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Send To CSV" Width="100px" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Button ID="btnSendReConfirmEmailForCUCA" runat="server" BackColor="Red" 
                                            Height="50px" Text="Force WorldPay Setup" Width="130px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSendReConfirmEmail" runat="server" BackColor="Red" 
                                            Height="50px" Text="Send Re-Confirm Email" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table style="width:100%;">
                                        <tr>
                                            <td bgcolor="#0068B6" class="Button style5">
                                                &nbsp;<span class="style37 style80">&nbsp;&nbsp;Notes:</span><asp:ObjectDataSource 
                                                    ID="odsNotes" runat="server" 
                                                    SelectMethod="GetPDLNotes" 
                                                    TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter DefaultValue="0" Name="PDLApplicationID" 
                                                            QueryStringField="ID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvNotes" runat="server" AllowSorting="True" 
                                                    AutoGenerateColumns="False" DataSourceID="odsNotes" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
                                                        <asp:BoundField DataField="StaffName" HeaderText="Created By" 
                                                            SortExpression="StaffName" />
                                                        <asp:BoundField DataField="CreatedDate" HeaderText="Created" 
                                                            SortExpression="CreatedDate" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No notes available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td width="200">
                                                            Notes:<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                                                ControlToValidate="txtNotes" CssClass="errormsg" ErrorMessage="note required" 
                                                                ValidationGroup="vgNote">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNotes" runat="server" Height="40px" MaxLength="400" 
                                                                style="margin-left: 0px" TabIndex="65" ValidationGroup="vgNote" 
                                                                Width="500px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="btnAddNote" runat="server" Text="Add Note" 
                                                                ValidationGroup="vgNote" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                            <td bgcolor="#0068B6" class="Button style5">
                                                &nbsp;<span class="style37 style80">&nbsp;&nbsp;Calculator Activity:</span><asp:ObjectDataSource 
                                                    ID="odsCalcActivity" runat="server" 
                                                    SelectMethod="GetCalcActivityByAppID" 
                                                    TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter DefaultValue="0" Name="PDLApplicationID" 
                                                            QueryStringField="ID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </td>
                                        </tr>
                                <tr>
                                    <td>
                                                <asp:GridView ID="gvCalcActivity" runat="server" AllowSorting="True" 
                                                    AutoGenerateColumns="False" DataSourceID="odsCalcActivity" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="IPAddress" HeaderText="IPAddress" 
                                                            SortExpression="IPAddress" />
                                                        <asp:BoundField DataField="ChangedBarDesc" HeaderText="Changed Bar" 
                                                            SortExpression="ChangedBarDesc" />
                                                        <asp:BoundField DataField="ValueChanged" DataFormatString="{0:F0}" 
                                                            HeaderText="Value" SortExpression="ValueChanged" />
                                                        <asp:BoundField DataField="CreatedDate" HeaderText="Created" 
                                                            SortExpression="CreatedDate" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No notes available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:HiddenField ID="hfAuthenticatePassScore" runat="server" />
                <asp:HiddenField ID="hfCreditPassScore" runat="server" />
                <asp:HiddenField ID="hfSessionID" runat="server" />
              </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

