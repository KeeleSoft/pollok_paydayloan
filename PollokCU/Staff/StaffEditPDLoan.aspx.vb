﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Imports EquifaxFacade

Partial Class StaffEditPDLoan
    Inherits System.Web.UI.Page

    Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objPDLStaff As PollokCU.DataAccess.Layer.clsStaffPayDayLoan = New PollokCU.DataAccess.Layer.clsStaffPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim ExperianWrapper As ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient = New ExperianServiceWrapper.LMCU.ExperianAuthentication.LMCUWSClient
    Dim objExperian As PollokCU.DataAccess.Layer.clsExperian = New PollokCU.DataAccess.Layer.clsExperian

    'Experien authentication objects
    Dim QASService As AddressService.ProWeb = New AddressService.ProWeb
    Dim objQACanSearch As AddressService.QACanSearch = New AddressService.QACanSearch
    Dim objQASearchOk As AddressService.QASearchOk
    Dim objEngineType As AddressService.EngineType = New AddressService.EngineType
    Dim objQAConfigType As AddressService.QAConfigType = New AddressService.QAConfigType
    Dim objQASearch As AddressService.QASearch = New AddressService.QASearch
    Dim objQASearchResult As AddressService.QASearchResult = Nothing
    Dim iAddressFoundTotal As Integer = 0
    Dim objQAGetAddress As New AddressService.QAGetAddress()
    Dim sLayout As String = System.Configuration.ConfigurationManager.AppSettings("AddressService.Layout")


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        PopulateControls()
        Me.btnDoAuthenticate.Attributes.Add("onclick", "return confirm('Are you sure you want to perform an authentication check?');")
        Me.btnDoCreditSearch.Attributes.Add("onclick", "return confirm('Are you sure you want to perform a credit search?');")
        Me.btnDoBankCheck.Attributes.Add("onclick", "return confirm('Are you sure you want to perform a bank check?');")
        Me.btnCreatePDF.Attributes.Add("onclick", "return confirm('Are you sure you want to re-generate the PDF?');")
        Me.btnCreateAgreement.Attributes.Add("onclick", "return confirm('Are you sure you want to re-generate the agreement?');")
        Me.btnFastPay.Attributes.Add("onclick", "return confirm('Are you sure you want to send a faster payment?');")
        Me.btnSendReConfirmEmail.Attributes.Add("onclick", "return confirm('Are you sure you want to send a re-confirm email and send the payment upon confirmation?');")
        Me.btnSendReConfirmEmailForCUCA.Attributes.Add("onclick", "return confirm('Are you sure you want to send a force sage pay setup email?');")
        Me.btnSendToCUCA.Attributes.Add("onclick", "return confirm('Are you sure you want to send the application PDF to CUCA?');")
        Me.btnSendToCSV.Attributes.Add("onclick", "return confirm('Are you sure you want to send the application to CSV?');")

    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            Dim objConfig As New System.Configuration.AppSettingsReader
            Dim StaffIDApproved As Integer = 0
            Dim StaffIDPaymentSent As Integer = 0
            Dim WorldpayStatus As Int32 = -1

            lblMessage.Visible = True

            Dim dtTXN As DataTable
            dtTXN = objPDL.SelectPayDayLoanDetail(Request.QueryString("ID"))

            If dtTXN.Rows.Count > 0 Then
                litID.Text = dtTXN.Rows(0).Item("ID")
                hfSessionID.Value = dtTXN.Rows(0).Item("SessionID")
                hlEditDetails.NavigateUrl = "StaffEditPDLLoanDetails.aspx?ID=" & dtTXN.Rows(0).Item("ID")
                hlEditLoan.NavigateUrl = "StaffEditPDLPreConfirmedLoan.aspx?SessionID=" & dtTXN.Rows(0).Item("SessionID")
                hlCollectSchedule.NavigateUrl = "StaffEditPDLPaymentCollections.aspx?AppType=PDL&ID=" & dtTXN.Rows(0).Item("ID")
                litMemberID.Text = IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), "")
                litRequestedDate.Text = dtTXN.Rows(0).Item("CreatedDate")
                litCompletedLevel.Text = dtTXN.Rows(0).Item("CompletedLevel")
                litApplicantName.Text = dtTXN.Rows(0).Item("Title") & " " & dtTXN.Rows(0).Item("FirstName") & " " & dtTXN.Rows(0).Item("SurName")
                litApplicantTypeDesc.Text = dtTXN.Rows(0).Item("ApplicantTypeDesc")
                litApplicantTypeID.Text = dtTXN.Rows(0).Item("ApplicantTypeID")

                liAccActiveCheckDesc.Text = dtTXN.Rows(0).Item("AccountActiveStatusDesc").ToString()
                litEmpCheckDesc.Text = dtTXN.Rows(0).Item("EmploymentCheckStatusDesc").ToString()
                litAuthCheckDesc.Text = dtTXN.Rows(0).Item("AuthenticationStatusDesc").ToString()
                litCreditCheckDesc.Text = dtTXN.Rows(0).Item("CreditCheckStatusDesc").ToString()
                litBankCheckDesc.Text = dtTXN.Rows(0).Item("BankCheckStatusDesc").ToString()

                WorldpayStatus = Convert.ToInt32(dtTXN.Rows(0).Item("FuturePay"))
                If (WorldpayStatus = -1) Then
                    litInternalBankCheck.Text = "Not completed"
                    Literalwpay.Text = "0"
                ElseIf (WorldpayStatus > 0) Then
                    litInternalBankCheck.Text = "Completed"
                    Literalwpay.Text = WorldpayStatus
                ElseIf (WorldpayStatus = 0) Then
                    litInternalBankCheck.Text = "Failed"
                    Literalwpay.Text = WorldpayStatus
                End If
                litEpic.Text = dtTXN.Rows(0).Item("Epic360").ToString()
                LiteralPartnerID.Text = dtTXN.Rows(0).Item("PartnerCodeDesc").ToString()
                litPDLCheckDesc.Text = dtTXN.Rows(0).Item("PreviousPDLAppliedStatusDesc").ToString()
                litMonthlyIncome.Text = Math.Round(dtTXN.Rows(0).Item("MonthlyIncomeAfterTax"), 2).ToString()

                btnSendToCUCA.Visible = False
                btnSendToCSV.Visible = False

                StaffIDApproved = IIf(dtTXN.Rows(0).Item("StaffID") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("StaffID") > 0, dtTXN.Rows(0).Item("StaffID"), 0)
                    StaffIDPaymentSent = IIf(dtTXN.Rows(0).Item("StaffIDPaymentSent") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("StaffIDPaymentSent") > 0, dtTXN.Rows(0).Item("StaffIDPaymentSent"), 0)

                    ddlStaff.SelectedValue = StaffIDApproved
                    If StaffIDApproved > 0 AndAlso CBool(Session("PDLAllowPayment")) AndAlso Not CBool(dtTXN.Rows(0).Item("FasterPaymentSent")) Then
                        btnFastPay.Enabled = True
                    Else
                        btnFastPay.Enabled = False
                    End If

                    Dim ReConfirmOver2Hours As Boolean = True


                    If CBool(Session("PDLAllowPayment")) AndAlso (Not CBool(dtTXN.Rows(0).Item("ReConfirmEmailSent")) OrElse CBool(dtTXN.Rows(0).Item("ReConfirmEmailSentOver2Hours"))) AndAlso
                    (dtTXN.Rows(0).Item("BankCheckStatus") = 1 OrElse dtTXN.Rows(0).Item("InternalBankCheckStatus") = 1) Then   'Only when one of the bank check is passed
                        btnSendReConfirmEmail.Enabled = True
                    Else
                        btnSendReConfirmEmail.Enabled = False
                    End If

                    btnSendToCSV.Enabled = Not CBool(dtTXN.Rows(0).Item("NewMemberCSVAllow"))

                    litPaidByName.Text = dtTXN.Rows(0).Item("StaffNamePaymentSent")
                    litReConfSentBy.Text = dtTXN.Rows(0).Item("StaffNameReConfirmEmailSent")

                    ddlStatus.SelectedValue = IIf(dtTXN.Rows(0).Item("ApplicationStatus") IsNot System.DBNull.Value AndAlso dtTXN.Rows(0).Item("ApplicationStatus") > 0, dtTXN.Rows(0).Item("ApplicationStatus"), 1)

                    Dim FilenameShort As String = dtTXN.Rows(0).Item("ID").ToString & "_" & dtTXN.Rows(0).Item("SurName").ToString & "_" & dtTXN.Rows(0).Item("DateOfBirth").ToShortDateString.Replace("/", "") & ".pdf"
                    Dim PDFName As String = "~\PDL\PDLApps\" & FilenameShort
                    hlPDF.NavigateUrl = PDFName
                    hlExperianReport.NavigateUrl = "StaffViewEquifaxReport.aspx?LoanID=" & dtTXN.Rows(0).Item("ID").ToString

                    Dim AgreementNameShort As String = dtTXN.Rows(0).Item("ID").ToString & "_" & dtTXN.Rows(0).Item("DateOfBirth").ToShortDateString.Replace("/", "") & ".pdf"
                    Dim AgreementNameFull As String = "~\PDL\Agreements\LoanAgreement_" & AgreementNameShort
                    hlAgreement.NavigateUrl = AgreementNameFull

                    Dim dtApplicantTypeDetails = objPDL.GetApplicantTypeDetails(dtTXN.Rows(0).Item("ApplicantTypeID"))
                    If dtApplicantTypeDetails IsNot Nothing AndAlso dtApplicantTypeDetails.Rows.Count > 0 Then
                        hfAuthenticatePassScore.Value = dtApplicantTypeDetails.Rows(0).Item("AuthenticatePassScore")
                        hfCreditPassScore.Value = dtApplicantTypeDetails.Rows(0).Item("CreditCheckPassScore")
                    Else
                        hfAuthenticatePassScore.Value = 50
                        hfCreditPassScore.Value = 2000
                    End If

                    Dim SortCode As String = dtTXN.Rows(0).Item("AccountSortCodeDec")
                    If SortCode = "089401" OrElse SortCode = "089409" Then
                        btnDoBankCheck.Text = "Internal Bank Check"
                        btnSendToCUCA.Enabled = True
                        'btnSendReConfirmEmailForCUCA.Enabled = True
                    Else
                        btnDoBankCheck.Text = "Bank Check"
                        btnSendToCUCA.Enabled = False
                        'btnSendReConfirmEmailForCUCA.Enabled = False
                    End If
                    btnSendReConfirmEmailForCUCA.Enabled = True
                End If

                If Session("StaffData") IsNot Nothing Then
                Dim StaffData As StaffDetails = CType(Session("StaffData"), StaffDetails)
                If StaffData IsNot Nothing AndAlso StaffData.AllowViewCUOK Then 'CUOK view only
                    btnSave.Enabled = False
                    btnDoAuthenticate.Enabled = False
                    btnDoCreditSearch.Enabled = False
                    btnDoBankCheck.Enabled = False
                    btnCreatePDF.Enabled = False
                    btnCreateAgreement.Enabled = False
                    btnFastPay.Enabled = False
                    btnSendToCUCA.Enabled = False
                    btnSendToCSV.Enabled = False
                    btnSendReConfirmEmailForCUCA.Enabled = False
                    btnSendReConfirmEmail.Enabled = False
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAddNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNote.Click
        Try
            lblMessage.Visible = False

            If txtNotes.Text.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "Note required."
                Return
            End If

            objPDLStaff.InsertPDLNote(litID.Text, txtNotes.Text, Session("StaffID"))

            txtNotes.Text = ""
            PopulateNotesGrid()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = "Failed to create note " & vbCrLf & ex.Message
        End Try
    End Sub

    Private Sub PopulateNotesGrid()
        Try
            lblMessage.Visible = False


            odsNotes.Select()
            gvNotes.DataBind()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = "Failed to populate notes grid, " & ex.Message
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblMessage.Visible = False

            Dim iStaffID As Integer = ddlStaff.SelectedValue
            Dim iUserID As Integer = Session("StaffID")
            Dim AppStatus As Integer = ddlStatus.SelectedValue


            objPDLStaff.UpdateApplicationByStaff(litID.Text, AppStatus, iStaffID, iUserID)

            Response.Redirect("StaffPDLoans.aspx")

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnDoBankCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDoBankCheck.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            'DoBankCheck(litID.Text)

            Dim dtTXN As DataTable
            dtTXN = objPDL.SelectPayDayLoanDetail(Request.QueryString("ID"))
            With dtTXN.Rows(0)
                Dim id As Integer = dtTXN.Rows(0).Item("ID")
                Dim mid As Integer = CInt(IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), 0))
                Dim fname As String = dtTXN.Rows(0).Item("FirstName")
                Dim sname As String = dtTXN.Rows(0).Item("SurName")
                Dim apptype As String = dtTXN.Rows(0).Item("AppType")
                Dim midname As String = dtTXN.Rows(0).Item("MiddleName")
                Dim SortCode As String = dtTXN.Rows(0).Item("AccountSortCodeDec")
                Dim AccountNumber As String = dtTXN.Rows(0).Item("AccountNumberDec")

                Dim AddressYear As String = dtTXN.Rows(0).Item("CurrentAddressNoOfYears")
                Dim AddressMonth As String = dtTXN.Rows(0).Item("CurrentAddressNoOfMonths")
                Dim BankYear As String = dtTXN.Rows(0).Item("TimeWithBankYear")
                Dim BankMonth As Integer = dtTXN.Rows(0).Item("TimeWithBankMonth")

                Dim CurrentAddressLine1 As String = dtTXN.Rows(0).Item("CurrentAddressLine1")
                Dim CurrentAddressLine2 As String = dtTXN.Rows(0).Item("CurrentAddressLine2")
                Dim CurrentCity As String = dtTXN.Rows(0).Item("CurrentCity")
                Dim CurrentCounty As String = dtTXN.Rows(0).Item("CurrentCounty")
                Dim CurrentPostCode As String = dtTXN.Rows(0).Item("CurrentPostCode")

                Dim PreviousAddressLine1 As String = dtTXN.Rows(0).Item("PreviousAddressLine1")
                Dim PreviousAddressLine2 As String = dtTXN.Rows(0).Item("PreviousAddressLine2")
                Dim PreviousCity As String = dtTXN.Rows(0).Item("PreviousCity")
                Dim PreviousCounty As String = dtTXN.Rows(0).Item("PreviousCounty")
                Dim PreviousPostcode As String = dtTXN.Rows(0).Item("PreviousPostCode")
                Dim Years As Integer = CInt(dtTXN.Rows(0).Item("CurrentAddressNoOfYears"))

                PreviousCounty = PreviousCounty.Replace(" ", "")
                CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
                CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
                PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
                PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

                Dim substring1 As String = SortCode.Substring(0, 2)
                Dim substring2 As String = SortCode.Substring(2, 2)
                Dim substring3 As String = SortCode.Substring(4)

                Dim srtcd As String = substring1 & "-" & substring2 & "-" & substring3

                Dim BYear As Integer = DateTime.Now.Year - BankYear
                Dim BMonth As Integer = CInt(DateTime.Now.Month)

                If BankMonth > BMonth Then
                    BMonth = BMonth + 12
                    BMonth = BMonth - BankMonth
                    BYear = BYear - 1
                Else
                    BMonth = BMonth - BankMonth
                End If

                Dim timebank As String = "P" & BYear & "Y" & BMonth & "M"
                Dim timeaddress As String = "P" & AddressYear & "Y" & AddressMonth & "M"
                'Dim timebank As String = "P11Y5M"
                'Dim timeaddress As String = "P9Y11M"
                'Dim addressid As String = "58150004386"

                Dim V As clsChecks = New clsChecks()
                Dim status As Integer = V.BankCheck(AccountNumber, srtcd, timebank, sname, fname, midname, timeaddress, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, id, apptype, mid)

                objPDL.UpdateApplicationCheckStatus(id, , , status)

                If status = 1 Then
                    lblMessage.Text = "Bank Check Passed"
                ElseIf status = 2 Then
                    lblMessage.Text = "Bank Check Done but Failed"
                Else
                    lblMessage.Text = "Bank Check technical failed"
                End If
            End With
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function DoBankCheck(ByVal iLoanID As Integer) As Int16
        Try
            Dim PassedBankCheck As Int16 = 0

            Dim dtApp As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

            If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                With dtApp.Rows(0)

                    Dim MemberID As String = IIf(.Item("MemberID") IsNot System.DBNull.Value, .Item("MemberID"), 0)
                    Dim SortCode As String = .Item("AccountSortCodeDec")
                    Dim AccountNumber As String = .Item("AccountNumberDec")
                    Dim AccountSetupDateYear As String = .Item("TimeWithBankYear")
                    Dim AccountSetupDateMonth As String = .Item("TimeWithBankMonth")
                    Dim AccountType As String = .Item("AccountType")
                    Dim CustomerAccountType As String = .Item("CustomerAccountType")
                    Dim FirstName As String = .Item("FirstName")
                    Dim SurName As String = .Item("SurName")
                    Dim DOBYear As String = .Item("DOB_YYYY")
                    Dim DOBMonth As String = .Item("DOB_MM")
                    Dim DOBDay As String = .Item("DOB_DD")

                    Dim CurrentAL1 As String = .Item("CurrentAddressLine1")
                    Dim HouseNumber As String = ""
                    Dim FlatNumber As String = ""
                    Dim Street As String = ""

                    If SortCode = "089401" OrElse SortCode = "089409" Then 'Do the internal bank check as LMCU sort code
                        Dim dtBankCheck As DataTable = objPDL.ValidateCurrentAccount(MemberID, AccountNumber, iLoanID)
                        If dtBankCheck IsNot Nothing AndAlso dtBankCheck.Rows.Count > 0 AndAlso dtBankCheck.Rows(0).Item("ValidAccount") = True AndAlso dtBankCheck.Rows(0).Item("ValidDeposits") = True Then
                            PassedBankCheck = 1
                        Else
                            PassedBankCheck = 2
                        End If
                    Else
                        If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                            HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                            If HouseNumber.ToLower.Contains("flat") Then
                                FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                                HouseNumber = ""
                            End If
                            Street = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
                        ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                            HouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                            If HouseNumber.ToLower.Contains("flat") Then
                                FlatNumber = HouseNumber.ToLower.Replace("flat", "").Trim
                                HouseNumber = ""
                            End If
                            Street = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
                        End If

                        Dim PostCode As String = .Item("CurrentPostCode")
                        Dim OwnerType As String = .Item("AccountOwner")

                        ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
                        If ExperianWrapper.DoBankWizard_Verify(SortCode, AccountNumber, AccountSetupDateYear, AccountSetupDateMonth, AccountType, CustomerAccountType _
                                                               , FirstName, SurName, DOBYear, DOBMonth, DOBDay, HouseNumber, FlatNumber, Street, PostCode, OwnerType _
                                                               , iLoanID, Request.ServerVariables("REMOTE_ADDR")) Then
                            PassedBankCheck = 1
                        Else
                            PassedBankCheck = 2
                        End If
                        objPDL.UpdateApplicationCheckStatus(iLoanID, , , PassedBankCheck)
                    End If
                End With
            End If
            Return PassedBankCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Protected Sub btnDoAuthenticate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDoAuthenticate.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            'Dim AuthPassScore As Integer = CInt(IIf(hfAuthenticatePassScore.Value > 0, hfAuthenticatePassScore.Value, 100))
            'Dim status As Integer = ExperienAuthenticate(litID.Text, AuthPassScore) 'DoAuthentication(litID.Text, AuthPassScore)
            Dim dtTXN As DataTable
            dtTXN = objPDL.SelectPayDayLoanDetail(Request.QueryString("ID"))
            Dim id As Integer = dtTXN.Rows(0).Item("ID")
            Dim mid As Integer = CInt(IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), 0))
            Dim fname As String = dtTXN.Rows(0).Item("FirstName")
            Dim sname As String = dtTXN.Rows(0).Item("SurName")
            Dim apptype As String = dtTXN.Rows(0).Item("AppType")
            Dim dob As DateTime = dtTXN.Rows(0).Item("DateOfBirth")

            Dim CurrentAddressLine1 As String = dtTXN.Rows(0).Item("CurrentAddressLine1")
            Dim CurrentAddressLine2 As String = dtTXN.Rows(0).Item("CurrentAddressLine2")
            Dim CurrentCity As String = dtTXN.Rows(0).Item("CurrentCity")
            Dim CurrentCounty As String = dtTXN.Rows(0).Item("CurrentCounty")
            Dim CurrentPostCode As String = dtTXN.Rows(0).Item("CurrentPostCode")

            Dim PreviousAddressLine1 As String = dtTXN.Rows(0).Item("PreviousAddressLine1")
            Dim PreviousAddressLine2 As String = dtTXN.Rows(0).Item("PreviousAddressLine2")
            Dim PreviousCity As String = dtTXN.Rows(0).Item("PreviousCity")
            Dim PreviousCounty As String = dtTXN.Rows(0).Item("PreviousCounty")
            Dim PreviousPostcode As String = dtTXN.Rows(0).Item("PreviousPostCode")
            Dim midname As String = dtTXN.Rows(0).Item("MiddleName")
            Dim Years As Integer = CInt(dtTXN.Rows(0).Item("CurrentAddressNoOfYears"))

            PreviousCounty = PreviousCounty.Replace(" ", "")
            CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
            CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
            PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
            PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

            ' Dim addressid As String = "58150004386"
            Dim V As clsChecks = New clsChecks()
            Dim status As Integer = V.amlCheck(midname, sname, fname, dob, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, id, apptype, mid)

            objPDL.UpdateApplicationCheckStatus(id, status)

            If status = 1 Then
                lblMessage.Text = "Authenticate Check Passed"
            ElseIf status = 2 Then
                lblMessage.Text = "Authenticate Done but Failed"
            Else
                lblMessage.Text = "Authenticate technical failed"
            End If
        Catch ex As Exception

        End Try

        'objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        'lblMessage.Visible = True
        'lblMessage.Text = ex.Message
        'End Try
    End Sub

    Private Function DoAuthentication(ByVal iLoanID As Integer, ByVal AuthenticationPassScore As Integer) As Int16
        Try
            Dim PassedAuthentication As Int16 = 0

            Dim dtApp As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

            If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                With dtApp.Rows(0)
                    Dim CurrentAL1 As String = .Item("CurrentAddressLine1")
                    Dim CurrentHouseNumber As String = ""
                    Dim CurrentStreetName As String = ""

                    Dim PreAL1 As String = .Item("PreviousAddressLine1")
                    Dim PreHouseNumber As String = ""
                    Dim PreStreetName As String = ""

                    If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                        CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                        CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
                    ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                        CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                        CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
                    End If

                    If PreAL1.Length > 0 AndAlso PreAL1.IndexOf(",") > 0 Then
                        PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(",") - 1)
                        PreStreetName = PreAL1.Substring(PreAL1.IndexOf(",") + 1, PreAL1.Length - PreAL1.IndexOf(",") - 1).TrimStart
                    ElseIf PreAL1.Length > 0 AndAlso PreAL1.IndexOf(" ") > 0 Then
                        PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(" ") - 1)
                        PreStreetName = PreAL1.Substring(PreAL1.IndexOf(" ") + 1, PreAL1.Length - PreAL1.IndexOf(" ") - 1)
                    End If

                    ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
                    If ExperianWrapper.DoAuthenticationPlus(.Item("Title"), .Item("FirstName"), .Item("MiddleName"), .Item("SurName"), .Item("Gender") _
                                                            , .Item("DOB_YYYY"), .Item("DOB_MM"), .Item("DOB_DD") _
                                                            , CurrentHouseNumber, CurrentStreetName, .Item("CurrentCity"), .Item("CurrentCounty"), .Item("CurrentPostCode") _
                                                            , .Item("CurrentAddressNoOfYears"), .Item("CurrentAddressNoOfMonths") _
                                                            , PreHouseNumber, PreStreetName, .Item("PreviousCity"), .Item("PreviousCounty"), .Item("PreviousPostCode") _
                                                            , IIf(.Item("PreviousAddressNoOfYears") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfYears"), 0) _
                                                            , IIf(.Item("PreviousAddressNoOfMonths") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfMonths"), 0) _
                                                            , CInt(.Item("LoanAmount")), CInt(.Item("LoanPeriod")) _
                                                            , IIf(.Item("Purpose") IsNot System.DBNull.Value, .Item("Purpose"), "97") _
                                                            , iLoanID, Request.ServerVariables("REMOTE_ADDR") _
                                                            , AuthenticationPassScore) Then
                        PassedAuthentication = 1    'Pass
                    Else
                        PassedAuthentication = 2    'Fail
                    End If
                End With
            End If

            objPDL.UpdateApplicationCheckStatus(iLoanID, PassedAuthentication)

            Return PassedAuthentication
        Catch ex As Exception
            Throw ex
            'objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0    'Not Done
        End Try
    End Function

    Protected Sub btnDoCreditSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDoCreditSearch.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            'Dim CreditPassScore As Integer = CInt(IIf(hfCreditPassScore.Value > 0, hfCreditPassScore.Value, 1000))
            'Dim status As Integer = DoCreditCheck(litID.Text, CreditPassScore)
            Dim passScore As Integer = CInt(hfCreditPassScore.Value)
            Dim dtTXN As DataTable
            dtTXN = objPDL.SelectPayDayLoanDetail(Request.QueryString("ID"))
            Dim id As Integer = dtTXN.Rows(0).Item("ID")
            Dim mid As Int32 = Convert.ToInt32(IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), 0))
            Dim fname As String = dtTXN.Rows(0).Item("FirstName")
            Dim sname As String = dtTXN.Rows(0).Item("SurName")
            Dim apptype As String = dtTXN.Rows(0).Item("AppType")
            Dim dob As DateTime = dtTXN.Rows(0).Item("DateOfBirth")

            Dim CurrentAddressLine1 As String = dtTXN.Rows(0).Item("CurrentAddressLine1")
            Dim CurrentAddressLine2 As String = dtTXN.Rows(0).Item("CurrentAddressLine2")
            Dim CurrentCity As String = dtTXN.Rows(0).Item("CurrentCity")
            Dim CurrentCounty As String = dtTXN.Rows(0).Item("CurrentCounty")
            Dim CurrentPostCode As String = dtTXN.Rows(0).Item("CurrentPostCode")

            Dim PreviousAddressLine1 As String = dtTXN.Rows(0).Item("PreviousAddressLine1")
            Dim PreviousAddressLine2 As String = dtTXN.Rows(0).Item("PreviousAddressLine2")
            Dim PreviousCity As String = dtTXN.Rows(0).Item("PreviousCity")
            Dim PreviousCounty As String = dtTXN.Rows(0).Item("PreviousCounty")
            Dim PreviousPostcode As String = dtTXN.Rows(0).Item("PreviousPostCode")

            PreviousCounty = PreviousCounty.Replace(" ", "")
            CurrentAddressLine1 = CurrentAddressLine1.ToLower.Replace("flat", "")
            CurrentAddressLine2 = CurrentAddressLine2.ToLower.Replace("flat", "")
            PreviousAddressLine1 = PreviousAddressLine1.ToLower.Replace("flat", "")
            PreviousAddressLine2 = PreviousAddressLine2.ToLower.Replace("flat", "")

            Dim midname As String = dtTXN.Rows(0).Item("MiddleName")
            Dim Years As Integer = CInt(dtTXN.Rows(0).Item("CurrentAddressNoOfYears"))

            'Dim addressid As String = "58150004386"
            Dim V As clsChecks = New clsChecks()
            Dim status As Integer = V.CreditSearch(midname, sname, fname, dob, CurrentCounty, CurrentCity, CurrentPostCode, CurrentAddressLine1, CurrentAddressLine2, PreviousCounty, PreviousCity, PreviousPostcode, PreviousAddressLine1, PreviousAddressLine2, Years, id, apptype, mid, passScore)

            objPDL.UpdateApplicationCheckStatus(id, , status)

            If status = 1 Then
                lblMessage.Text = "CreditCheck Passed"
            ElseIf status = 2 Then
                lblMessage.Text = "CreditCheck Done but Failed"
            Else
                lblMessage.Text = "CreditCheck technical failed"
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function DoCreditCheck(ByVal iLoanID As Integer, ByVal CreditScorePassValue As Integer) As Int16
        Try
            Dim PassedCreditCheck As Int16 = 0

            Dim dtApp As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

            If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                With dtApp.Rows(0)
                    Dim CurrentAL1 As String = .Item("CurrentAddressLine1")
                    Dim CurrentHouseNumber As String = ""
                    Dim CurrentStreetName As String = ""

                    Dim PreAL1 As String = .Item("PreviousAddressLine1")
                    Dim PreHouseNumber As String = ""
                    Dim PreStreetName As String = ""

                    If CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(",") > 0 Then
                        CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(","))
                        CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(",") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(",") - 1).TrimStart
                    ElseIf CurrentAL1.Length > 0 AndAlso CurrentAL1.IndexOf(" ") > 0 Then
                        CurrentHouseNumber = CurrentAL1.Substring(0, CurrentAL1.IndexOf(" "))
                        CurrentStreetName = CurrentAL1.Substring(CurrentAL1.IndexOf(" ") + 1, CurrentAL1.Length - CurrentAL1.IndexOf(" ") - 1)
                    End If

                    If PreAL1.Length > 0 AndAlso PreAL1.IndexOf(",") > 0 Then
                        PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(",") - 1)
                        PreStreetName = PreAL1.Substring(PreAL1.IndexOf(",") + 1, PreAL1.Length - PreAL1.IndexOf(",") - 1).TrimStart
                    ElseIf PreAL1.Length > 0 AndAlso PreAL1.IndexOf(" ") > 0 Then
                        PreHouseNumber = PreAL1.Substring(0, PreAL1.IndexOf(" ") - 1)
                        PreStreetName = PreAL1.Substring(PreAL1.IndexOf(" ") + 1, PreAL1.Length - PreAL1.IndexOf(" ") - 1)
                    End If

                    ExperianWrapper.ExperianEnv = objConfig.getConfigKey("ExperianEnv")
                    If ExperianWrapper.DoConsumerCreditSearch(.Item("Title"), .Item("FirstName"), .Item("MiddleName"), .Item("SurName"), .Item("Gender") _
                                                            , .Item("DOB_YYYY"), .Item("DOB_MM"), .Item("DOB_DD") _
                                                            , CurrentHouseNumber, CurrentStreetName, .Item("CurrentCity"), .Item("CurrentCounty"), .Item("CurrentPostCode") _
                                                            , .Item("CurrentAddressNoOfYears"), .Item("CurrentAddressNoOfMonths") _
                                                            , PreHouseNumber, PreStreetName, .Item("PreviousCity"), .Item("PreviousCounty"), .Item("PreviousPostCode") _
                                                            , IIf(.Item("PreviousAddressNoOfYears") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfYears"), 0) _
                                                            , IIf(.Item("PreviousAddressNoOfMonths") IsNot System.DBNull.Value, .Item("PreviousAddressNoOfMonths"), 0) _
                                                            , CInt(.Item("LoanAmount")), CInt(.Item("LoanPeriod")), .Item("Purpose") _
                                                            , iLoanID, Request.ServerVariables("REMOTE_ADDR") _
                                                            , CreditScorePassValue) Then
                        PassedCreditCheck = 1    'Pass
                    Else
                        PassedCreditCheck = 2    'Fail
                    End If
                End With
            End If

            objPDL.UpdateApplicationCheckStatus(iLoanID, , PassedCreditCheck)

            Return PassedCreditCheck
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0
        End Try
    End Function

    Protected Sub btnCreatePDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreatePDF.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            GeneratePDF(litID.Text)

            lblMessage.Text = "PDF Generated"


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function GeneratePDF(ByVal iLoanID As Integer) As String


        Dim CrystalReportDocument As ReportDocument
        Dim CrystalExportOptions As ExportOptions
        Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim crTable As Table

        Dim sReportPath As String = MapPath("..\PDL") & "\PDLApplication.rpt"
        Trace.Warn(sReportPath)
        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


        Dim dtLoan As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)
        'Generate the PDF file and save in the reports folder
        Dim SurName As String = String.Empty
        Dim DateOfBirth As DateTime = Nothing
        If dtLoan.Rows.Count > 0 Then
            SurName = dtLoan.Rows(0).Item("Surname")
            DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")
        End If


        Dim Filename, FilenameShort As String
        CrystalReportDocument = New ReportDocument()
        CrystalReportDocument.Load(sReportPath)
        'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
        Dim crTables As Tables = CrystalReportDocument.Database.Tables
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crConnectionInfo.DatabaseName = sSQLServerDB
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

        'SQL SP Parameters
        CrystalReportDocument.SetParameterValue("@ID", iLoanID)

        'Name of the o/p file
        FilenameShort = iLoanID.ToString & "_" & SurName & "_" & DateOfBirth.ToShortDateString.Replace("/", "") & ".pdf"
        Filename = MapPath("..\PDL") & "\PDLApps\" & FilenameShort



        CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
        CrystalDiskFileDestinationOptions.DiskFileName = Filename
        CrystalExportOptions = CrystalReportDocument.ExportOptions
        With CrystalExportOptions
            .DestinationOptions = CrystalDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With
        CrystalReportDocument.Export()

        Return FilenameShort
        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
        'Response.ContentType = "text/pdf"
        'Response.WriteFile(Filename)
        'Response.Flush()
        'Response.End()
    End Function

#Region "Experien Authentication"
    Protected Function ExperienAuthenticate(ByVal iLoanID As Integer, ByVal AuthenticationPassScore As Integer) As Int16


        Try
            Dim PassedAuthentication As Int16 = 0
            Dim ExperienErrorMessage As String = ""
            Dim sDecisionCode As String = ""
            Dim sExperianRef As String = ""
            Dim sAuthIndexCode As String = ""
            Dim sAuthIndexText As String = ""
            Dim sAuthDecisionCode As String = ""
            Dim sAuthDecisionText As String = ""
            Dim sPolicyCode As String = ""
            Dim sPolicyRuleText As String = ""

            SetupEngineTypeAuth(objEngineType)
            objQACanSearch.Country = "GBR"
            objQACanSearch.Engine = objEngineType
            objQACanSearch.Layout = sLayout   '"< Default >"
            objQACanSearch.QAConfig = objQAConfigType


            objQASearchOk = QASService.DoCanSearch(objQACanSearch)

            objQASearch.Country = "GBR"
            objQASearch.Engine = objEngineType
            objQASearch.Engine.PromptSet = AddressService.PromptSetType.Default
            objQASearch.Engine.PromptSetSpecified = True
            objQASearch.Layout = sLayout
            objQASearch.QAConfig = objQAConfigType

            Dim objSearchTerm(25) As AddressService.SearchTerm

            PopulateSearchTerm(objSearchTerm, iLoanID)   'Populate search criterias byref

            objQASearch.SearchSpec = objSearchTerm

            objQASearchResult = QASService.DoSearch(objQASearch)

            'Search return is in objQASearchResult object now
            Dim picklist As AddressService.QAPicklistType = objQASearchResult.QAPicklist
            Dim address As AddressService.QAAddressType = objQASearchResult.QAAddress

            If picklist IsNot Nothing Then
                ExperienErrorMessage = generateExperienErrorMessage(picklist)
            Else
                ExperienErrorMessage = ""
            End If

            'Populate experian data only if availabe. Otherwise they will be balnk by default
            If address IsNot Nothing Then
                generateExperienSearchResult(address, sDecisionCode, sExperianRef, sAuthIndexCode, sAuthIndexText, sAuthDecisionCode, sAuthDecisionText, sPolicyCode, sPolicyRuleText)

                'Save the details against the application
                objExperian.UpdateAuthenticationOldData(iLoanID, ExperienErrorMessage, sDecisionCode, sExperianRef, sAuthIndexCode, sAuthIndexText, sAuthDecisionCode, sAuthDecisionText, sPolicyCode, sPolicyRuleText)

                If sAuthIndexCode IsNot Nothing AndAlso sAuthIndexCode >= AuthenticationPassScore Then
                    PassedAuthentication = 1    'Passed
                Else
                    PassedAuthentication = 2    'Failed
                End If
            End If

            objPDL.UpdateApplicationCheckStatus(iLoanID, PassedAuthentication)
            Return PassedAuthentication
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return 0    'Not Done
        End Try
    End Function

    Private Sub SetupEngineTypeAuth(ByRef et As AddressService.EngineType)
        et.Flatten = True
        'et.flattenField = True
        'et.flattenFieldSpecified = True
        et.FlattenSpecified = True
        et.Intensity = AddressService.EngineIntensityType.Exact
        'et.intensityField = AddressService.EngineIntensityType.Exact
        'et.intensityFieldSpecified = False
        et.IntensitySpecified = False
        et.PromptSet = AddressService.PromptSetType.Default
        'et.promptSetField= AddressService.PromptSetType.Default
        'et.promptSetFieldSpecified = True
        et.PromptSetSpecified = True
        et.Threshold = Nothing
        'et.thresholdField = Nothing
        et.Timeout = Nothing
        'et.timeoutField = Nothing
        et.Value = AddressService.EngineEnumType.Authenticate
        'et.valueField = AddressService.EngineEnumType.Singleline
    End Sub

    Private Sub PopulateSearchTerm(ByRef objSearchTerm() As AddressService.SearchTerm, ByVal iLoanID As Integer)
        Try
            Dim dtLoanDet As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

            If dtLoanDet.Rows.Count > 0 Then
                objSearchTerm(0) = New AddressService.SearchTerm
                objSearchTerm(0).Key = "CTRL_SEARCHCONSENT"
                objSearchTerm(0).Value = "Yes"

                objSearchTerm(1) = New AddressService.SearchTerm
                objSearchTerm(1).Key = "NAME_TITLE"
                objSearchTerm(1).Value = IIf(dtLoanDet.Rows(0).Item("Title") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Title"), "")

                objSearchTerm(2) = New AddressService.SearchTerm
                objSearchTerm(2).Key = "NAME_FORENAME"
                objSearchTerm(2).Value = IIf(dtLoanDet.Rows(0).Item("FirstName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("FirstName"), "")

                objSearchTerm(3) = New AddressService.SearchTerm
                objSearchTerm(3).Key = "NAME_INITIALS"
                objSearchTerm(3).Value = IIf(dtLoanDet.Rows(0).Item("MiddleName") IsNot System.DBNull.Value, Left(dtLoanDet.Rows(0).Item("MiddleName"), 1), "")

                objSearchTerm(4) = New AddressService.SearchTerm
                objSearchTerm(4).Key = "NAME_SURNAME"
                objSearchTerm(4).Value = IIf(dtLoanDet.Rows(0).Item("SurName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("SurName"), "")

                objSearchTerm(5) = New AddressService.SearchTerm
                objSearchTerm(5).Key = "NAME_DATEOFBIRTH"
                objSearchTerm(5).Value = IIf(dtLoanDet.Rows(0).Item("DateOfBirth") IsNot System.DBNull.Value, Format(dtLoanDet.Rows(0).Item("DateOfBirth"), "dd/MM/yyyy"), "")

                objSearchTerm(6) = New AddressService.SearchTerm
                objSearchTerm(6).Key = "NAME_SEX"
                objSearchTerm(6).Value = IIf(dtLoanDet.Rows(0).Item("Gender") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Gender"), "") 'getGender(IIf(dtLoanDet.Rows(0).Item("Title") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("Title"), "")) 'M or F, We dont capture this

                Dim sFlat As String = ""
                Dim sHouseName As String = ""
                Dim sHouseNo As String = ""
                Dim sStreet1 As String = ""

                If dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value AndAlso dtLoanDet.Rows(0).Item("CurrentAddressLine1").ToString.Contains(",") Then
                    Dim sAddL1 As String = dtLoanDet.Rows(0).Item("CurrentAddressLine1").ToString
                    sFlat = Left(sAddL1, sAddL1.IndexOf(","))
                    sHouseName = Right(sAddL1, (sAddL1.Length - sAddL1.ToString.IndexOf(",") - 2))
                    sStreet1 = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine2") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine2"), "")
                ElseIf dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value Then
                    sHouseNo = dtLoanDet.Rows(0).Item("CurrentAddressLine1")
                    If sHouseNo.ToString.IndexOf(" ") > 0 Then sHouseNo = Left(sHouseNo, sHouseNo.ToString.IndexOf(" "))
                    sStreet1 = dtLoanDet.Rows(0).Item("CurrentAddressLine1")
                    sStreet1 = Right(sStreet1, (sStreet1.Length - sStreet1.ToString.IndexOf(" ")))
                End If

                objSearchTerm(7) = New AddressService.SearchTerm
                objSearchTerm(7).Key = "ADDR_FLAT"
                objSearchTerm(7).Value = sFlat

                objSearchTerm(8) = New AddressService.SearchTerm
                objSearchTerm(8).Key = "ADDR_HOUSENAME"
                objSearchTerm(8).Value = sHouseName

                'Dim sHouseNo As String = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine1"), "")
                'sHouseNo = Left(sHouseNo, sHouseNo.ToString.IndexOf(" "))

                objSearchTerm(9) = New AddressService.SearchTerm
                objSearchTerm(9).Key = "ADDR_HOUSENUMBER"
                objSearchTerm(9).Value = sHouseNo

                'Dim sStreet1 As String = IIf(dtLoanDet.Rows(0).Item("CurrentAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressLine1"), "")
                'sStreet1 = Right(sStreet1, (sStreet1.Length - sStreet1.ToString.IndexOf(" ")))

                objSearchTerm(10) = New AddressService.SearchTerm
                objSearchTerm(10).Key = "ADDR_STREET"
                objSearchTerm(10).Value = sStreet1

                objSearchTerm(11) = New AddressService.SearchTerm
                objSearchTerm(11).Key = "ADDR_DISTRICT"
                objSearchTerm(11).Value = ""

                objSearchTerm(12) = New AddressService.SearchTerm
                objSearchTerm(12).Key = "ADDR_TOWN"
                objSearchTerm(12).Value = IIf(dtLoanDet.Rows(0).Item("CurrentCity") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentCity"), "")

                objSearchTerm(13) = New AddressService.SearchTerm
                objSearchTerm(13).Key = "ADDR_COUNTY"
                objSearchTerm(13).Value = IIf(dtLoanDet.Rows(0).Item("CurrentCounty") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentCounty"), "")

                objSearchTerm(14) = New AddressService.SearchTerm
                objSearchTerm(14).Key = "ADDR_POSTCODE"
                objSearchTerm(14).Value = IIf(dtLoanDet.Rows(0).Item("CurrentPostCode") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentPostCode"), "")

                objSearchTerm(15) = New AddressService.SearchTerm
                objSearchTerm(15).Key = "AUT1_MOTHERMAIDENNAME"
                objSearchTerm(15).Value = IIf(dtLoanDet.Rows(0).Item("MotherName") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("MotherName"), "")

                objSearchTerm(16) = New AddressService.SearchTerm
                objSearchTerm(16).Key = "AUT1_PLACEOFBIRTH"
                objSearchTerm(16).Value = IIf(dtLoanDet.Rows(0).Item("PlaceOfBirth") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PlaceOfBirth"), "")

                Dim sCurrentAddressNoOfYears = IIf(dtLoanDet.Rows(0).Item("CurrentAddressNoOfYears") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressNoOfYears"), 0)
                Dim sCurrentAddressNoOfMonths = IIf(dtLoanDet.Rows(0).Item("CurrentAddressNoOfMonths") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("CurrentAddressNoOfMonths"), 0)
                Dim sDateFrom As Date = DateAdd(DateInterval.Month, sCurrentAddressNoOfMonths * -1, DateAdd(DateInterval.Year, sCurrentAddressNoOfYears * -1, Now()))

                objSearchTerm(17) = New AddressService.SearchTerm
                objSearchTerm(17).Key = "RESY_DATEFROM#1"
                objSearchTerm(17).Value = ""    'Its screws if you pass this , String.Format(sDateFrom.ToShortDateString, "dd/MM/yyyy")

                objSearchTerm(18) = New AddressService.SearchTerm
                objSearchTerm(18).Key = "ADDR_FLAT#1"
                objSearchTerm(18).Value = ""

                objSearchTerm(19) = New AddressService.SearchTerm
                objSearchTerm(19).Key = "ADDR_HOUSENAME#1"
                objSearchTerm(19).Value = ""

                Dim sHouseNo2 As String = IIf(dtLoanDet.Rows(0).Item("PreviousAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousAddressLine1"), "")
                If sHouseNo2.Contains(" ") Then sHouseNo2 = Left(sHouseNo2, sHouseNo2.ToString.IndexOf(" "))

                objSearchTerm(20) = New AddressService.SearchTerm
                objSearchTerm(20).Key = "ADDR_HOUSENUMBER#1"
                objSearchTerm(20).Value = sHouseNo2

                Dim sStreet2 As String = IIf(dtLoanDet.Rows(0).Item("PreviousAddressLine1") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousAddressLine1"), "")
                If sStreet2.Contains(" ") Then sStreet2 = Right(sStreet2, (sStreet2.Length - sStreet2.ToString.IndexOf(" ")))

                objSearchTerm(21) = New AddressService.SearchTerm
                objSearchTerm(21).Key = "ADDR_STREET#1"
                objSearchTerm(21).Value = sStreet2

                objSearchTerm(22) = New AddressService.SearchTerm
                objSearchTerm(22).Key = "ADDR_DISTRICT#1"
                objSearchTerm(22).Value = ""

                objSearchTerm(23) = New AddressService.SearchTerm
                objSearchTerm(23).Key = "ADDR_TOWN#1"
                objSearchTerm(23).Value = IIf(dtLoanDet.Rows(0).Item("PreviousCity") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousCity"), "")

                objSearchTerm(24) = New AddressService.SearchTerm
                objSearchTerm(24).Key = "ADDR_COUNTY#1"
                objSearchTerm(24).Value = IIf(dtLoanDet.Rows(0).Item("PreviousCounty") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousCounty"), "")

                objSearchTerm(25) = New AddressService.SearchTerm
                objSearchTerm(25).Key = "ADDR_POSTCODE#1"
                objSearchTerm(25).Value = IIf(dtLoanDet.Rows(0).Item("PreviousPostCode") IsNot System.DBNull.Value, dtLoanDet.Rows(0).Item("PreviousPostCode"), "")
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
        End Try
    End Sub

    Private Function generateExperienErrorMessage(ByVal picklist As AddressService.QAPicklistType) As String
        Dim sError As String = ""
        For i = 0 To picklist.PicklistEntry.Length - 1
            sError = sError & picklist.PicklistEntry(i).Picklist & vbCrLf
        Next
        Return sError
    End Function

    Private Sub generateExperienSearchResult(ByVal address As AddressService.QAAddressType _
                                             , ByRef sDecisionCode As String _
                                               , ByRef sExperianRef As String _
                                               , ByRef sAuthIndexCode As String _
                                               , ByRef sAuthIndexText As String _
                                               , ByRef sAuthDecisionCode As String _
                                               , ByRef sAuthDecisionText As String _
                                               , ByRef sPolicyCode As String _
                                               , ByRef sPolicyRuleText As String)

        For i = 0 To address.AddressLine.Length - 1
            Select Case address.AddressLine(i).Label.Trim
                Case "Authenticate Decision text"
                    sAuthDecisionText = address.AddressLine(i).Line
                Case "Authenticate Decision"
                    sAuthDecisionCode = address.AddressLine(i).Line
                Case "Authenticate Authentication index explanation"
                    sAuthIndexText = address.AddressLine(i).Line
                Case "Authenticate Authentication index"
                    sAuthIndexCode = address.AddressLine(i).Line
                Case "Authenticate HR Policy rule"
                    sPolicyCode = address.AddressLine(i).Line
                Case "Authenticate HR Policy rule text"
                    sPolicyRuleText = address.AddressLine(i).Line
                Case "Authenticate Experian customer reference"
                    sExperianRef = address.AddressLine(i).Line
            End Select
        Next

    End Sub

    Private Function getGender(ByVal sSalutation As String) As String
        If sSalutation = "Mr" OrElse sSalutation = "Sir" OrElse sSalutation = "Professor" Then
            Return "M"
        ElseIf sSalutation = "Mrs" OrElse sSalutation = "Ms" OrElse sSalutation = "Miss" Then
            Return "F"
        Else
            Return ""
        End If
    End Function
#End Region

    Protected Sub btnFastPay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFastPay.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            GenerateFasterPaymentFile(litID.Text)

            lblMessage.Text = "Faster Payment Instructions Generated"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    'Do not generate the CSV and just insert into the table so the scheduled task will pick it up
    Private Sub GenerateFasterPaymentFile(ByVal iLoanID As Integer)
        Dim MembersCount As Integer = 0
        Dim dtLoan As DataTable

        dtLoan = objPDL.SelectPayDayLoanDetail(iLoanID)

        If dtLoan IsNot Nothing AndAlso dtLoan.Rows.Count > 0 Then

            Dim objConfig As New System.Configuration.AppSettingsReader
            'Dim FilePath As String = CType(objConfig.GetValue("CUOKPaymentsCSVFolder", GetType(System.String)), System.String) & "\CUOKPay_" & iLoanID.ToString & ".csv"
            'Dim InputLine As String = ""

            'Using New Impersonator("httpweb", "CUONLINEWEB", "Sadmin*4321")


            'Dim w As IO.StreamWriter
            'w = IO.File.CreateText(FilePath)

            Dim MemberID As Integer = 0
            Dim dr As DataRow = dtLoan.Rows(0)
            Dim PayRef As String = CStr(dr("ID"))
            If Not IsDBNull(dr("MemberID")) AndAlso CInt(dr("MemberID")) > 0 Then
                PayRef = PayRef & "-" & dr("MemberID")
                MemberID = dr("MemberID")
            End If

            Dim SortCode As String = dr("AccountSortCodeDec")
            Dim AccNumber As String = dr("AccountNumberDec")
            Dim AccName As String = dr("AccountNameDec")
            Dim LoanAmountInPence As Integer = CInt(dr("LoanAmount")) * 100
            Dim TxnType As Integer = 99
            Dim InstantLoan As Boolean = (dr("InstantLoan") > 0)


            'InputLine = SortCode & "," & _
            '            AccNumber & "," & _
            '            AccName & "," & _
            '            PayRef & "," & _
            '            LoanAmountInPence.ToString & "," & _
            '            TxnType.ToString
            'w.WriteLine(InputLine)


            'w.Flush()
            'w.Close()

            'Insert into the table
            objPDL.InsertPaymentInstruction(iLoanID, MemberID, SortCode, AccNumber, AccName, PayRef, LoanAmountInPence, TxnType, Session("StaffID"), InstantLoan)
            'End Using
        End If
    End Sub

    Protected Sub btnCreateAgreement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateAgreement.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            GenerateAgreementPDF(litID.Text)

            lblMessage.Text = "Agreement Re-generated"


        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub GenerateAgreementPDF(ByVal iLoanID As Integer)
        Dim dtLoan As DataTable
        Dim DateOfBirth As DateTime = Nothing
        Dim DOB As String = ""

        dtLoan = objPDL.SelectPayDayLoanDetail(iLoanID)

        If dtLoan.Rows.Count > 0 Then
            DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")
            DOB = DateOfBirth.ToShortDateString.Replace("/", "")
        End If

        Dim CrystalReportDocument As ReportDocument
        Dim CrystalExportOptions As ExportOptions
        Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim crTable As Table

        Dim sReportPath As String = MapPath("..\PDL") & "\PDLLoanAgreement.rpt"
        Trace.Warn(sReportPath)
        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


        Dim Filename, FilenameShort As String
        CrystalReportDocument = New ReportDocument()
        CrystalReportDocument.Load(sReportPath)
        'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
        Dim crTables As Tables = CrystalReportDocument.Database.Tables
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crConnectionInfo.DatabaseName = sSQLServerDB
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False

        'SQL SP Parameters
        CrystalReportDocument.SetParameterValue("@ID", iLoanID)

        'Name of the o/p file
        FilenameShort = iLoanID.ToString & "_" & DOB & ".pdf"
        Filename = MapPath("..\PDL") & "\Agreements\LoanAgreement_" & FilenameShort



        CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
        CrystalDiskFileDestinationOptions.DiskFileName = Filename
        CrystalExportOptions = CrystalReportDocument.ExportOptions
        With CrystalExportOptions
            .DestinationOptions = CrystalDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With
        CrystalReportDocument.Export()

    End Sub

    Protected Sub btnSendReConfirmEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendReConfirmEmail.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            objPDL.UpdatePreConfirmedLoanReApplyEmailSent(litID.Text, hfSessionID.Value, Session("StaffID"))
            SendReConfirmEmail(litID.Text)
            objPDLStaff.InsertPDLNote(litID.Text, "Re-Confirm Email Sent", Session("StaffID"))
            lblMessage.Text = "Re-confirmation email sent"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Function SendReConfirmEmail(ByVal iLoanID As Integer, Optional ByVal Reconfirm As Boolean = True) As Boolean
        Dim sEmailBody As String
        Dim Email, FirstName, LastName, sEmailFrom, sSubject, SessionID As String


        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

        Dim dtLoan As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

        If dtLoan.Rows.Count > 0 Then
            FirstName = dtLoan.Rows(0).Item("FirstName")
            LastName = dtLoan.Rows(0).Item("Surname")
            Email = dtLoan.Rows(0).Item("Email")
            SessionID = dtLoan.Rows(0).Item("SessionID")

            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "PCU - Wee Glasgow Loan"

            Dim sClickURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "PDL/PDLReConfirm.aspx?S=" & SessionID & "&L=" & iLoanID.ToString

            sEmailBody = GetReConfirmEmailBody(FirstName, LastName, sClickURL, Reconfirm)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , Email _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "")

            Return True
        Else
            Return False
        End If
    End Function

    Private Function SendForceSagePayEmail(ByVal iLoanID As Integer) As Boolean
        Dim sEmailBody As String
        Dim Email, FirstName, LastName, sEmailFrom, sSubject, SessionID As String


        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

        Dim dtLoan As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)

        If dtLoan.Rows.Count > 0 Then
            FirstName = dtLoan.Rows(0).Item("FirstName")
            LastName = dtLoan.Rows(0).Item("Surname")
            Email = dtLoan.Rows(0).Item("Email")
            SessionID = dtLoan.Rows(0).Item("SessionID")

            sEmailFrom = "noreply@cuonline.org.uk"
            sSubject = "PCU - WeeLoan WorldPay Setup"

            Dim sClickURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "PDL/PDLworldPaySetup.aspx?S=" & SessionID & "&L=" & iLoanID.ToString

            sEmailBody = GetForceSageEmailBody(FirstName, LastName, sClickURL)

            objEmail.SendEmailMessage(sEmailFrom _
                                      , Email _
                                      , sSubject _
                                      , sEmailBody _
                                      , "" _
                                      , "")

            Return True
        Else
            Return False
        End If
    End Function

    Private Function GetReConfirmEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal URL As String, Optional ByVal Reconfirm As Boolean = True) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for your recent application for the Pollok Credit Union’s Pay Day loan online. Congratulations, after further assessment, we are pleased to inform you that your pay day loan application is successfully approved and you are now just one step away from getting your loan. Simply click and follow this link to view and to accept your loan agreement and terms and conditions. Please have your mobile phone to hand as we will be sending you a security code to verify you:")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<a href=""" & URL & """>" & URL & "</a>")
            body.AppendLine("<br/>")
            If Reconfirm Then
                body.AppendLine("<strong><span style=""color: #FF3300"">*Please note that this link is only valid for 2 hours from the time this email was sent.</span></strong>")
            End If
            body.AppendLine("<br/><br/>")
            body.AppendLine("After successfully completing this final stage, you will receive a final email confirming your completion with a copy of the loan agreement and repayment details. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount to your bank account within 24 hours from you replying to this email. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email to: info@pollokcu.com")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our pay day loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Best regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Private Function GetForceSageEmailBody(ByVal FirstName As String, ByVal LastName As String, ByVal URL As String) As String
        Try
            Dim body As StringBuilder = New StringBuilder

            body.AppendLine("Dear " & FirstName & " " & LastName)
            body.AppendLine("<br/><br/>")
            body.AppendLine("Thank you for your recent online application for Pollok Credit Union’s Wee Glasgow Loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Congratulations, we are pleased to inform you that your loan application has been successfully approved and you are now just one step away from getting your loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Simply click and follow this link to view and to accept your loan agreement and terms and conditions. Please have your mobile phone to hand as we will be sending you a security code to verify you:")
            body.AppendLine("<br/><br/>")
            body.AppendLine("<a href=""" & URL & """>" & URL & "</a>")
            body.AppendLine("<br/>")
            body.AppendLine("*Please note that this link is only valid for 2 hours from the time this email was sent.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("After successfully completing this final stage, you will receive a final email confirming your completion with a copy of the loan agreement and repayment details. If you selected the Faster Payment facility in your application, we will action the transfer of your loan amount to your bank account within 24 hours from you replying to this email. If you did not opt for the Faster Payment facility, the transfer of your loan amount may take up to 3 working days to reach your account.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("If you have any queries regarding your loan, please contact us via email to: <a href="">theweeloan@pollokcu.com</a>")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Once again we thank you for applying for our Wee Glasgow Loan, an ethical and truly low cost payday loan.")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Kindest regards")
            body.AppendLine("<br/><br/>")
            body.AppendLine("Pollok Credit Union")
            body.AppendLine("<br/><br/>")

            Return body.ToString
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            Return ""
        End Try
    End Function

    Protected Sub btnSendToCUCA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendToCUCA.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""

            SendApplicationToCUCA(litID.Text)

            objPDLStaff.InsertPDLNote(litID.Text, "Application PDF is sent to CUCA", Session("StaffID"))
            lblMessage.Text = "Application PDF is sent to CUCA"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub SendApplicationToCUCA(ByVal iLoanID As Integer)
        Dim dtLoan As DataTable = objPDL.SelectPayDayLoanDetail(iLoanID)
        'Generate the PDF file and save in the reports folder
        Dim SurName As String = String.Empty
        Dim DateOfBirth As DateTime = Nothing
        Dim Filename, FilenameShort As String

        Dim sEmailFrom As String = "noreply@cuonline.org.uk"
        Dim sSubject As String = "PDL Standing Order Setup"
        Dim objEmail As Nettune.Emailer.clsSMTPEmail = New Nettune.Emailer.clsSMTPEmail

        If dtLoan.Rows.Count > 0 Then
            SurName = dtLoan.Rows(0).Item("Surname")
            DateOfBirth = dtLoan.Rows(0).Item("DateOfBirth")

            'FilenameShort = iLoanID.ToString & "_" & SurName & "_" & DateOfBirth.ToShortDateString.Replace("/", "") & ".pdf"
            FilenameShort = "LoanAgreement_" & iLoanID.ToString & "_" & DateOfBirth.ToShortDateString.Replace("/", "") & ".pdf"
            Filename = MapPath("..\PDL") & "\Agreements\" & FilenameShort

            objEmail.SendEmailMessage(sEmailFrom _
                                      , "gayeshan@yahoo.com" _
                                      , sSubject _
                                      , "PDL Standing Order Setup, please see attachment." _
                                      , Filename _
                                      , "")
        End If


    End Sub

    Protected Sub btnSendToCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendToCSV.Click
        Try
            objPDL.UpdateApplicationSendToNewMember(litID.Text)
            lblMessage.Text = "Member details are sent to CSV and will be included on the next CSV file."
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSendReConfirmEmailForCUCA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendReConfirmEmailForCUCA.Click
        Try
            lblMessage.Visible = True
            lblMessage.Text = ""
            objPDL.UpdateCUCAButPayingFromDifferentAccount(litID.Text, True, Session("StaffID"))
            SendForceSagePayEmail(litID.Text)
            objPDLStaff.InsertPDLNote(litID.Text, "Force WorldPay setup Email Sent", Session("StaffID"))
            lblMessage.Text = "Force WorldPay setup Email Sent"

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
