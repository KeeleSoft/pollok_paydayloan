﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffEditYSA.aspx.vb" Inherits="StaffEditYSA" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" Visible="False" />
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;YSA Deposit</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width: 100%">
                    <tr>
                        <td width="20%">
                            Deposit Reference:</td>
                        <td width="30%">
                            <asp:Literal ID="litID" runat="server"></asp:Literal>
                        </td>
                        <td width="25%">
                            Member Number:</td>
                        <td width="25%">
                            <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            School:</td>
                        <td>
                            <asp:Literal ID="litSchool" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Member:</td>
                        <td>
                            <asp:Literal ID="litMemberName" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Amount:</td>
                        <td>
                            <asp:TextBox ID="txtAmount" runat="server" ValidationGroup="vgSave"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtAmount" ErrorMessage="Amount required" 
                                ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Request Date:</td>
                        <td>
                            <asp:Literal ID="litRequestDate" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSendToCSV" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Send To CSV File" Width="120px" />
                                        <asp:Button ID="btnSave" runat="server" BackColor="Red" 
                                            Height="50px" Text="Save" Width="120px" ValidationGroup="vgSave" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="margin-left: 40px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="text-align: right">
                                        <asp:Button ID="btnClose" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Close" Width="120px" />
                                        </td>
                                </tr>                                
                                </table>
                        </td>
                    </tr>
                    <tr id="rowSendText" runat="server" visible="false">
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    
                    </table>
                </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                &nbsp;</td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

