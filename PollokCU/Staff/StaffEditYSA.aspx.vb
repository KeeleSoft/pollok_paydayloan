﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class StaffEditYSA
    Inherits System.Web.UI.Page

    Dim objSchool As PollokCU.DataAccess.Layer.clsSchoolsSavings = New PollokCU.DataAccess.Layer.clsSchoolsSavings
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        PopulateControls()
        Me.btnSave.Attributes.Add("onclick", "return confirm('Are you sure you want to save?');")
        Me.btnSendToCSV.Attributes.Add("onclick", "return confirm('Are you sure you want to send the deposit to CSV file?');")
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            lblMessage.Visible = True

            Dim dtTXN As DataTable
            dtTXN = objSchool.SelectYSADepositByID(Request.QueryString("ID"))

            If dtTXN.Rows.Count > 0 Then
                litID.Text = dtTXN.Rows(0).Item("DepositID")
                litMemberID.Text = IIf(dtTXN.Rows(0).Item("MemberID") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("MemberID"), "")
                litSchool.Text = dtTXN.Rows(0).Item("SchoolName")
                litMemberName.Text = dtTXN.Rows(0).Item("MemberName")
                litRequestDate.Text = dtTXN.Rows(0).Item("TransactionDate")
                txtAmount.Text = dtTXN.Rows(0).Item("Amount")
                If CBool(dtTXN.Rows(0).Item("CSVGenerated")) = True Then
                    btnSendToCSV.Enabled = True
                Else
                    btnSendToCSV.Enabled = False
                    btnSendToCSV.BackColor = Drawing.Color.Gray
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblMessage.Visible = False

            Dim iUserID As Integer = Session("StaffID")
            Dim amount As Double = txtAmount.Text


            objSchool.UpdateSchoolDeposit(litID.Text, amount, iUserID)

            Response.Redirect("StaffYSA.aspx")

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


    Protected Sub btnSendToBacsFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendToCSV.Click
        Try
            objSchool.UpdateSchoolDepositArchive(litID.Text, False, 0)
            lblMessage.Text = "Deposit details are flaged to be included in the next batch. Please process via file downloads"
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub



    Protected Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("StaffYSA.aspx")
    End Sub
End Class
