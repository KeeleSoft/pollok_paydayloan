﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffEditYSASchoolLogin.aspx.vb" Inherits="StaffEditYSASchoolLogin" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" Visible="False" />
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Update School Login Info</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width: 100%">
                    <tr>
                        <td>
                            School:</td>
                        <td>
                            <asp:Literal ID="litSchool" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Pay run number:</td>
                        <td>
                            <asp:Literal ID="litPayrunNo" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:</td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="vgSave" 
                                Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtEmail" ErrorMessage="Email required" 
                                ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:</td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" ValidationGroup="vgSave" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtEmail" ErrorMessage="Password required" 
                                ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Confirm Password:</td>
                        <td>
                            <asp:TextBox ID="txtConfirmPassword" runat="server" ValidationGroup="vgSave" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ControlToValidate="txtConfirmPassword" ErrorMessage="Password confirmation required" 
                                ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" BackColor="Red" 
                                            Height="50px" Text="Save" Width="120px" ValidationGroup="vgSave" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="margin-left: 40px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="text-align: right">
                                        <asp:Button ID="btnClose" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Close" Width="120px" />
                                        </td>
                                </tr>                                
                                </table>
                        </td>
                    </tr>
                    <tr id="rowSendText" runat="server" visible="false">
                        <td colspan="2">
                            &nbsp;</td>
                    </tr>
                    
                    </table>
                </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                &nbsp;</td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

