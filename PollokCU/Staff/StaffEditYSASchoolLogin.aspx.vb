﻿Imports System.Data.SqlClient
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class StaffEditYSASchoolLogin
    Inherits System.Web.UI.Page

    Dim objSchool As PollokCU.DataAccess.Layer.clsSchoolsSavings = New PollokCU.DataAccess.Layer.clsSchoolsSavings
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        PopulateControls()
        'Me.btnSave.Attributes.Add("onclick", "return confirm('Are you sure you want to save login details?');")

    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Private Sub PopulateControls()
        Try
            lblMessage.Visible = True

            Dim dtTXN As DataTable
            dtTXN = objSchool.SelectSchoolDetails(Request.QueryString("PayrunNo"))

            If dtTXN.Rows.Count > 0 Then
                litSchool.Text = dtTXN.Rows(0).Item("SchoolName")
                litPayrunNo.Text = dtTXN.Rows(0).Item("Payrunno")
                txtEmail.Text = If(dtTXN.Rows(0).Item("Email") IsNot System.DBNull.Value, dtTXN.Rows(0).Item("Email"), "")
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblMessage.Visible = False

            Dim iUserID As Integer = Session("StaffID")

            If txtPassword.Text = txtConfirmPassword.Text Then
                objSchool.UpdateSchoolDetails(litPayrunNo.Text, txtEmail.Text, txtPassword.Text, iUserID)
                lblMessage.Visible = True
                lblMessage.Text = "Login details updated"
            Else
                lblMessage.Visible = True
                lblMessage.Text = "Password and the confirmed password do not match. Please try again"
            End If

            

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


    Protected Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("StaffYSA.aspx")
    End Sub
End Class
