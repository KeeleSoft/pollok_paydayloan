﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffLogin
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim bAllowLogin As Boolean = False
        Dim iStaffID As Integer = 0

        Try
            lblMessage.Visible = False
            objStaff.AuthenticateStaffLogin(txtLoginName.Text _
                                              , txtPassword.Text _
                                              , bAllowLogin _
                                              , iStaffID)

            If bAllowLogin AndAlso iStaffID > 0 Then
                SetSessionForStaff(iStaffID)
                Response.Redirect("StaffHome.aspx")
            Else    'Staff ID and/or password failed
                lblMessage.Visible = True
                lblMessage.Text = "Invalid login information. Please try again"
            End If

        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try

    End Sub

    Private Sub SetSessionForStaff(ByVal iStaffID As Integer)
        Try
            Dim dtMember As DataTable
            dtMember = objStaff.SelectStaffDetail(iStaffID)

            Session("StaffID") = iStaffID
            If dtMember.Rows.Count > 0 Then
                Dim StaffData As StaffDetails = New StaffDetails(dtMember.Rows(0))
                Session("StaffData") = StaffData

                Session("StaffName") = dtMember.Rows(0).Item("StaffName")
                Session("StaffLevel") = dtMember.Rows(0).Item("StaffLevel")
                Session("StaffDepartment") = dtMember.Rows(0).Item("Department")
                Session("PDLAllowApprove") = dtMember.Rows(0).Item("PDLAllowApprove")
                Session("PDLAllowPayment") = dtMember.Rows(0).Item("PDLAllowPayment")
                Session("AllowViewSage") = dtMember.Rows(0).Item("AllowViewSage")
                Session("AllowViewSageCollection") = dtMember.Rows(0).Item("AllowViewSageCollection")
                Session("AllowViewALPS") = dtMember.Rows(0).Item("AllowViewALPS")
                Session("AllowManageALPS") = dtMember.Rows(0).Item("AllowManageALPS")
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = "Failed to setup staff session." & vbCrLf & ex.Message
        End Try
    End Sub

End Class
