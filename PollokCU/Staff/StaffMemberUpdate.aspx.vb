﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffMemberUpdate
    Inherits System.Web.UI.Page

    Dim objMember As PollokCU.DataAccess.Layer.clsMember = New PollokCU.DataAccess.Layer.clsMember

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        'If Session("StaffID") Is Nothing Then
        '    Response.Redirect("StaffLogin.aspx")
        'End If
    End Sub

    Protected Sub btnChange_Click(sender As Object, e As System.EventArgs) Handles btnChange.Click
        Try
            lblMessage.Visible = False

            objMember.UpdateMemberDetails(txtMemberID.Text, txtMobile.Text)

            lblMessage.Visible = True
            lblMessage.Text = "Member details changed"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
