﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffMemberVerify.aspx.vb" Inherits="StaffMemberVerify" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top">
            <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Member Verify </span></td>
          </tr>          
          <tr>
            <td height="25" valign="middle">
                <table 
                    style="width:100%;" class="BodyText">
                <tr>
                    <td>
                        <strong>Search...</strong></td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        NI number</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtNI" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Surname</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSurName" runat="server" Width="150px"></asp:TextBox>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        DOB</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDOB" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Post code</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPostCode" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Address line 1</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtAddress1" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
          </tr>
          <tr>
            <td>
                &nbsp;</td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsMembers" runat="server" 
                    SelectMethod="GetELMSMemberListForVerify" 
                     TypeName="PollokCU.DataAccess.Layer.clsELMS">
                    <SelectParameters>
                        <asp:Parameter Name="NINumber" Type="String" />
                        <asp:Parameter Name="AddressLine1" Type="String" />
                        <asp:Parameter Name="PostCode" Type="String" />
                        <asp:Parameter Name="SurName" DefaultValue="" Type="String" />
                        <asp:Parameter Name="DOB" Type="DateTime" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvMemberList" runat="server" AutoGenerateColumns="False" 
                    PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" 
                    EnableModelValidation="True">
                    <Columns>
                        <asp:BoundField DataField="MemberID" HeaderText="Member#" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FullName" HeaderText="Name" />
                        <asp:BoundField DataField="NINumber" 
                            HeaderText="NI#" />
                        <asp:BoundField DataField="AddressLine1" HeaderText="Address" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server" 
                                    ImageUrl="~/images/btn_view.png" 
                                    NavigateUrl='<%# "StaffMemberVerifyShowDetails.aspx?MemberID=" & Eval("MemberID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No members found
                    </EmptyDataTemplate>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

