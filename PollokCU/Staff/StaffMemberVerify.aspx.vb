﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffMemberVerify
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Try
            lblMessage.Visible = False

            If Not String.IsNullOrEmpty(txtNI.Text) OrElse Not String.IsNullOrEmpty(txtAddress1.Text) OrElse Not String.IsNullOrEmpty(txtPostCode.Text) OrElse Not String.IsNullOrEmpty(txtSurName.Text) OrElse Not String.IsNullOrEmpty(txtDOB.Text) Then
                odsMembers.SelectParameters.Item("NINumber").DefaultValue = txtNI.Text
                odsMembers.SelectParameters.Item("AddressLine1").DefaultValue = txtAddress1.Text
                odsMembers.SelectParameters.Item("PostCode").DefaultValue = txtPostCode.Text.Replace(" ", "")
                odsMembers.SelectParameters.Item("SurName").DefaultValue = txtSurName.Text
                lblMessage.Visible = True

                Dim dob As Date = Date.MinValue
                If Not String.IsNullOrEmpty(txtDOB.Text) Then Date.TryParse(txtDOB.Text, dob)

                If dob > Date.MinValue Then
                    odsMembers.SelectParameters.Item("DOB").DefaultValue = dob
                Else
                    odsMembers.SelectParameters.Item("DOB").DefaultValue = Date.MinValue
                End If
                odsMembers.Select()
                gvMemberList.DataSource = odsMembers
                gvMemberList.DataBind()
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
