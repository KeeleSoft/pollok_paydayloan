﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffMemberVerifyShowDetails.aspx.vb" Inherits="StaffMemberVerifyShowDetails" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top">
            <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;Member Verification Details </span></td>
          </tr>          
          <tr>
            <td>
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
          </tr>
          <tr>
            <td>
                <table style="width:100%;">
                    <tr>
                        <td>
                            Member name:</td>
                        <td>
                            <asp:Literal ID="litMemberName" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Account status:</td>
                        <td>
                            <asp:Literal ID="litAccStatus" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total loans taken:</td>
                        <td>
                            <asp:Literal ID="litTotalLoans" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total loans outstanding:</td>
                        <td>
                            <asp:Literal ID="litTotalOutstanding" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last loan repayment value:</td>
                        <td>
                            <asp:Literal ID="litRepayment" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last loan taken date:</td>
                        <td>
                            <asp:Literal ID="litLOoanDate" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

