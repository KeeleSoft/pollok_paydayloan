﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffMemberVerifyShowDetails
    Inherits System.Web.UI.Page

    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Dim elmsData As PollokCU.DataAccess.Layer.clsELMS = New PollokCU.DataAccess.Layer.clsELMS
            Dim memberData As DataTable = elmsData.GetELMSMemberVerifyDetails(Request.QueryString("MemberID"))
            If memberData IsNot Nothing AndAlso memberData.Rows.Count > 0 Then
                With memberData.Rows(0)
                    litMemberName.Text = .Item("MemberName")
                    litAccStatus.Text = .Item("AccountStatusDesc")
                    litTotalLoans.Text = FormatNumber(.Item("TotalLoansTaken"), 2)
                    litTotalOutstanding.Text = FormatNumber(.Item("TotalLoansOutstanding"), 2)
                    litRepayment.Text = FormatNumber(.Item("LastRepayAmount"), 2)
                    litLOoanDate.Text = .Item("LastLoanDate")
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
