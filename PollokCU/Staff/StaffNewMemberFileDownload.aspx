﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffNewMemberFileDownload.aspx.vb" Inherits="StaffNewMemberFileDownload" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;New Member File Downloads </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsNewMemberFileList" runat="server" 
                    SelectMethod="SelectNewMemberFileDownload" 
                    TypeName="PollokCU.DataAccess.Layer.clsStaffAdmin">
                    <SelectParameters>
                            <asp:Parameter DefaultValue="Membership" Name="AppType" Type="String" />
                        </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="gvFileList" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsNewMemberFileList" PageSize="100" SkinID="NormalGrid" OnRowDataBound="gvFileList_RowDataBound"
                    Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="FileDate" HeaderText="File Date" 
                            SortExpression="FileDate" >
                            <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Members Count" SortExpression="MembersCount">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MembersCount") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMembersCount" runat="server" 
                                    Text='<%# Bind("MembersCount") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDownload" runat="server" 
                                    ImageUrl="~/Images/btn_view.png" 
                                    NavigateUrl='<%# "..\MemberCSVs\" & Eval("FileName") %>' 
                                    Text="Download Member CSV File"></asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

