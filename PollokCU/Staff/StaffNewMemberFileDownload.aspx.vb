﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffNewMemberFileDownload
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dtGenerateVariables As DataTable = objStaff.SelectNewMemberFileGenerateVariables
        If dtGenerateVariables IsNot Nothing AndAlso dtGenerateVariables.Rows.Count > 0 Then
            If CBool(dtGenerateVariables.Rows(0).Item("GenerateRequired")) Then
                Dim FileCounter As Integer = dtGenerateVariables.Rows(0).Item("PendingFileCount")
                Dim FileDate As Date = DateAdd(DateInterval.Day, 1, CDate(dtGenerateVariables.Rows(0).Item("LastGeneratedDate")))
                Dim FileName As String
                Dim MemberCount As Integer = 0

                For i As Integer = 1 To FileCounter
                    If DateDiff(DateInterval.Day, FileDate, Now()) = 0 Then Exit For
                    FileName = "NewMembers_" & Replace(FileDate.ToShortDateString, "/", "") & ".csv"
                    MemberCount = GenerateCSV(FileName, FileDate)
                    objStaff.InsertNewMemberFileDownload(FileDate, FileName, MemberCount, Session("StaffID"))
                    FileDate = DateAdd(DateInterval.Day, 1, FileDate)
                Next
            End If
        End If
        
    End Sub

    Private Function GenerateCSV(ByVal FileName As String, ByVal FileDate As Date) As Integer

        Dim MembersCount As Integer = 0
        Dim dsNewMembers As DataTable = objStaff.SelectNewMembersToGeneratCSV(FileDate)

        If dsNewMembers IsNot Nothing AndAlso dsNewMembers.Rows.Count > 0 Then
            MembersCount = dsNewMembers.Rows.Count

            Dim AppPath As String = Request.PhysicalApplicationPath
            Dim FilePath As String = AppPath & "MemberCSVs\" & FileName
            Dim InputLine As String = ""
            Dim w As IO.StreamWriter
            w = IO.File.CreateText(FilePath)

            For Each dr As DataRow In dsNewMembers.Rows
                InputLine = dr("MemberID") & "," & _
                            dr("Title") & "," & _
                            dr("SurName") & "," & _
                            dr("FirstName") & "," & _
                            "," & _
                            Replace(dr("CurrentAddressLine1"), ",", " ") & "," & _
                            Replace(dr("CurrentAddressLine2"), ",", " ") & "," & _
                            dr("CurrentCity") & "," & _
                            dr("CurrentPostCode") & "," & _
                            dr("HomeTelephone") & "," & _
                            dr("DateOfBirth") & "," & _
                            FileDate.ToShortDateString & "," & _
                            "," & _
                            "," & _
                            dr("NINumber") & "," & _
                            dr("Email") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "A," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            dr("CompanyName") & "," & _
                            Replace(dr("WorkAddressLine1"), ",", " ") & "," & _
                            Replace(dr("WorkAddressLine2"), ",", " ") & " " & dr("WorkCity") & "," & _
                            dr("WorkPostCode") & "," & _
                            dr("WorkTelephone") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            dr("BeneficiaryTitleDesc") & " " & dr("BeneficiaryFirstName") & " " & dr("BeneficiarySurname") & "," & _
                            Replace(dr("BeneficiaryAddressLine1"), ",", " ") & "," & _
                            Replace(dr("BeneficiaryAddressLine2"), ",", " ") & " " & dr("BeneficiaryCity") & "," & _
                            dr("BeneficiaryPostCode") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            dr("BankName") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            dr("AccountNo") & "," & _
                            "," & _
                            dr("SortCode") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            dr("BeneficiaryRelationship") & "," & _
                            "," & _
                            "," & _
                            "," & _
                            "," & _
                            "final field"
                w.WriteLine(InputLine)
            Next

            w.Flush()
            w.Close()

        End If

        Return MembersCount
    End Function

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Sub gvFileList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hlDownload As HyperLink = CType(e.Row.FindControl("hlDownload"), HyperLink)
            Dim lblMembersCount As Label = CType(e.Row.FindControl("lblMembersCount"), Label)

            If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                If lblMembersCount.Text > 0 Then
                    hlDownload.Visible = True
                Else
                    hlDownload.Visible = False
                End If
            End If
        End If
    End Sub
End Class
