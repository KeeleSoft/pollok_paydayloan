﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffPostCodeSearch
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then


                lblMessage.Visible = False

                Dim sEnableAddressSearchRegister As String = objSystemData.GetSystemKeyValue("EnableAddressSearchRegister")
                ddlRegister.SelectedValue = sEnableAddressSearchRegister

                Dim sEnableAddressSearchJoin As String = objSystemData.GetSystemKeyValue("EnableAddressSearchJoin")
                ddlMember.SelectedValue = sEnableAddressSearchJoin

                Dim sEnableAddressSearchLoan As String = objSystemData.GetSystemKeyValue("EnableAddressSearchLoan")
                ddlLoan.SelectedValue = sEnableAddressSearchLoan

                Dim sEnableAddressSearchCurrent As String = objSystemData.GetSystemKeyValue("EnableAddressSearchCurrent")
                ddlCurrent.SelectedValue = sEnableAddressSearchCurrent

                Dim sEnableAddressSearchRevLoan As String = objSystemData.GetSystemKeyValue("EnableAddressSearchRevLoan")
                ddlRevLoan.SelectedValue = sEnableAddressSearchRevLoan

                Dim sEnableAddressSearchAddress As String = objSystemData.GetSystemKeyValue("EnableAddressSearchAddress")
                ddlAddress.SelectedValue = sEnableAddressSearchAddress
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSaveRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveRegister.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchRegister", ddlRegister.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Join CU Online is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveMember_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMember.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchJoin", ddlMember.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Become a member is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveLoan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveLoan.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchLoan", ddlLoan.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Loan address search is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveCurrent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCurrent.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchCurrent", ddlCurrent.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Current account address search is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveRevLoan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveRevLoan.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchRevLoan", ddlRevLoan.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Rev loan address search is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSaveAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAddress.Click
        Try
            lblMessage.Visible = False

            objSystemData.UpdateSystemKey("EnableAddressSearchAddress", ddlAddress.SelectedValue.ToString)

            lblMessage.Visible = True
            lblMessage.Text = "Address search is updated"
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
