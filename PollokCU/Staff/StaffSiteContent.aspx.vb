﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffSiteContent
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then


                lblMessage.Visible = False

                Dim dtMsg1 As DataTable = objSystemData.SelectSiteContentDetail("LoginPageRightMessage")

                If dtMsg1 IsNot Nothing AndAlso dtMsg1.Rows.Count > 0 Then
                    txtMessage1.Text = dtMsg1.Rows(0).Item("SiteContent")
                    ddlShowMsg1.SelectedValue = dtMsg1.Rows(0).Item("Display")
                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "No LoginPageRightMessage found"
                End If
            End If
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblMessage.Visible = False

            If ddlShowMsg1.SelectedValue = "True" AndAlso txtMessage1.Text.Length = 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "LoginPageRightMessage required when the display status is selected as show."
                Return
            End If

            'All validation fine
            objSystemData.UpdateSiteContent("LoginPageRightMessage", txtMessage1.Text, Boolean.Parse(ddlShowMsg1.SelectedValue), Session("StaffID"))

            lblMessage.Visible = True
            lblMessage.Text = "All messages updated."
        Catch ex As Exception
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
