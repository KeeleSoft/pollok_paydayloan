﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffViewCreditLimits
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            lblMessage.Visible = False

            odsCreditLimits.SelectParameters.Item("FirstName").DefaultValue = txtFirstName.Text
            odsCreditLimits.SelectParameters.Item("SurName").DefaultValue = txtSurName.Text
            If txtMemberID.Text.Length > 0 AndAlso IsNumeric(txtMemberID.Text) Then
                odsCreditLimits.SelectParameters.Item("MemberID").DefaultValue = txtMemberID.Text
            Else
                odsCreditLimits.SelectParameters.Item("MemberID").DefaultValue = 0
            End If
            Select Case ddlProductStatus.SelectedValue
                Case 0
                    odsCreditLimits.SelectParameters.Item("blankRevLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankPDLLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankODLoan").DefaultValue = False
                Case 1
                    odsCreditLimits.SelectParameters.Item("blankRevLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankPDLLoan").DefaultValue = True
                    odsCreditLimits.SelectParameters.Item("blankODLoan").DefaultValue = False
                Case 2
                    odsCreditLimits.SelectParameters.Item("blankRevLoan").DefaultValue = True
                    odsCreditLimits.SelectParameters.Item("blankPDLLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankODLoan").DefaultValue = False
                Case 3
                    odsCreditLimits.SelectParameters.Item("blankRevLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankPDLLoan").DefaultValue = False
                    odsCreditLimits.SelectParameters.Item("blankODLoan").DefaultValue = True
            End Select
            odsCreditLimits.Select()
            gvMemberList.DataSource = odsCreditLimits
            gvMemberList.DataBind()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Sub gvBatches_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    Dim hfDuplicated As HiddenField = CType(e.Row.FindControl("hfDuplicated"), HiddenField)
        '    Dim hfSuccessfulBacs As HiddenField = CType(e.Row.FindControl("hfSuccessfulBacs"), HiddenField)
        '    Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

        '    If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
        '        If CBool(hfDuplicated.Value) Then
        '            lblName.ForeColor = Drawing.Color.Red
        '        ElseIf CBool(hfSuccessfulBacs.Value) Then
        '            lblName.ForeColor = Drawing.Color.Green
        '        End If
        '    End If
        'End If
    End Sub
End Class
