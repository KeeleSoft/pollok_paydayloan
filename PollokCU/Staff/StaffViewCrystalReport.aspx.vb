﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class Staff_StaffViewCrystalReport
    Inherits System.Web.UI.Page

    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            'Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        LoadReport(GetReportName(Request.QueryString("RID")))
    End Sub

    Private Function GetReportName(reportID As Integer) As String
        Select Case reportID
            Case 1
                Return "PDL- Calculator activity.rpt"
            Case 2
                Return "credit score & Q score.rpt"
            Case 3
                Return "PDL- monthly income.rpt"
            Case 4
                Return "PDL- job title.rpt"
            Case 5
                Return "Q1 to 5.rpt"
            Case 6
                Return "PDL-genda.rpt"
            Case 7
                Return "Number of loan applications received by LMCU.rpt"
            Case 8
                Return "repayment period & payday loans applied.rpt"
            Case 9
                Return "PDL-age.rpt"
            Case 10
                Return "PDL- No of dependents.rpt"
            Case 11
                Return "Summary by Marital Status.rpt"
            Case 12
                Return "Summary by Employment Status.rpt"
            Case Else
                Return "TestReport.rpt"
        End Select
    End Function

    Private Sub LoadReport(reportName As String)
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()

        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")

        Dim rDoc As ReportDocument = New ReportDocument
        rDoc.Load(Server.MapPath("CrystalFiles\" & reportName))
        Dim crTables As Tables = rDoc.Database.Tables
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.DatabaseName = sSQLServerDB
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        rDoc.ReportOptions.EnableSaveDataWithReport = False

        CrystalReportViewer1.ReportSource = rDoc
    End Sub
End Class
