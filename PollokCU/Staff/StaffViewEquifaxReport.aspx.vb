﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Staff_StaffViewEquifaxReport
    Inherits System.Web.UI.Page

    Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objExperianData As PollokCU.DataAccess.Layer.clsEquifax = New PollokCU.DataAccess.Layer.clsEquifax
    Dim LoanID As Integer = 0

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Visible = False

            LoanID = Request.QueryString("LoanID")
            If LoanID <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No loan id found"
            End If
            PopulateClientDetails()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateClientDetails()
        Try
            Dim LoanData As DataTable = objPDL.SelectPayDayLoanDetail(LoanID)
            If LoanData IsNot Nothing AndAlso LoanData.Rows.Count > 0 Then
                With LoanData.Rows(0)
                    litName.Text = .Item("FirstName") & " " & .Item("Surname")
                    litAuthStatus.Text = .Item("AuthenticationStatusDesc")
                    litBankWizardStatus.Text = .Item("BankCheckStatusDesc")
                    Dim AuthStatus As Integer = CInt(.Item("AuthenticationStatus"))
                    Dim BankStatus As Integer = CInt(.Item("BankCheckStatus"))
                    Dim CreditStatus As Integer = CInt(.Item("CreditCheckStatus"))

                    LiteralCredit.Text = .Item("CreditCheckStatusDesc")
                    If .Item("AuthenticationStatus") <> 0 Then
                        'pnlAuth.Visible = True
                        PopulateAuthenticationPanel(LoanData.Rows(0), AuthStatus)
                    Else
                        pnlAuth.Visible = False
                        Panel1.Visible = False
                    End If

                    If .Item("BankCheckStatus") <> 0 Then
                        pnlBankCheck.Visible = True
                        PopulateBankCheckPanel(BankStatus)
                    Else
                        pnlBankCheck.Visible = False
                        Panel2.Visible = False
                    End If



                    If .Item("CreditCheckStatus") <> 0 Then
                        'CreditReport.Visible = True
                        PopulateCreditSearch(CreditStatus)
                    Else
                        CreditReport.Visible = False
                        CD_Scoring_Row.Visible = False
                        CD_PubInfo_Row.Visible = False
                        CD_IMPAIRED_CH.Visible = False
                        Panel3.Visible = False
                    End If
                End With
            End If
            'PopulateCreditSearch()  '''TODO:Remove
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateAuthenticationPanel(ByVal dr As DataRow, ByVal AuthStatus As Integer)
        Try
            Dim AdrStatus As String = ""
            Dim AMLData As DataTable = objExperianData.GetAMLData(LoanID)
            If AMLData IsNot Nothing AndAlso AMLData.Rows.Count > 0 Then
                AdrStatus = AMLData.Rows(0).Item("AddressMatchStatus")
                If AdrStatus.Equals("noMatch") Or AdrStatus.Equals("multipleMatch") Then
                    pnlAuth.Visible = False
                    Panel1.Visible = True
                    Literal2.Text = AdrStatus
                Else
                    pnlAuth.Visible = True
                    With AMLData.Rows(0)

                        AuthErrorTableRow.Visible = True
                        Panel1.Visible = False

                        litVRC101.Text = .Item("VRC101")
                        litESC941.Text = .Item("ESC941")
                        litRSC8.Text = .Item("RSC8")
                        litQSC001.Text = .Item("QSC001")
                        litCSC903.Text = .Item("CSC903")
                        litFSC12.Text = .Item("FSC12")
                        litTSC1.Text = .Item("TSC1")
                        litFSC3.Text = .Item("FSC3")
                        litNSC1.Text = .Item("NSC1")
                        litNSC2.Text = .Item("NSC2")
                        litHSC1.Text = .Item("HSC1")
                        litDSC1.Text = .Item("DSC1")
                        litDSC3.Text = .Item("DSC3")
                        litDSC4.Text = .Item("DSC4")
                        litDSC5.Text = .Item("DSC5")
                        litASC8.Text = .Item("ASC8")
                        litVRC18.Text = .Item("VRC18")
                        litVIC3.Text = .Item("VIC3")
                        litVNC3.Text = .Item("VNC3")
                        litVXC3.Text = .Item("VXC3")
                        litOptions.Text = .Item("Options")

                        litNotice.Text = .Item("NoticeOfCorrectionOrDisputePresent")
                        litCounty.Text = .Item("County")
                        litNumber.Text = .Item("Number")
                        litPostCode.Text = .Item("PostCode")
                        litPostTown.Text = .Item("PostTown")
                        litStreet1.Text = .Item("Street1")
                        litAddressID.Text = .Item("AddressID")

                    End With
                End If
            Else
                pnlAuth.Visible = False
                Panel1.Visible = False
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateBankCheckPanel(ByVal BankStatus As Integer)
        Try
            Dim AdrStatus As String = ""
            Dim BankData As DataTable = objExperianData.GetBankCheckData(LoanID)
            If BankData IsNot Nothing AndAlso BankData.Rows.Count > 0 Then
                AdrStatus = BankData.Rows(0).Item("AddressMatchStatus")
                If AdrStatus.Equals("noMatch") Or AdrStatus.Equals("multipleMatch") Then
                    pnlBankCheck.Visible = False
                    Panel2.Visible = True
                    Literal4.Text = AdrStatus
                Else
                    pnlBankCheck.Visible = True
                    Panel2.Visible = False
                    With BankData.Rows(0)

                        litQSC032.Text = .Item("QSC032")
                        litQSC034.Text = .Item("QSC034")
                        litQSP032.Text = .Item("QSP032")
                        litQSP034.Text = .Item("QSP034")
                        litQSE032.Text = .Item("QSE032")
                        litQSE034.Text = .Item("QSE034")
                        litQSN032.Text = .Item("QSN032")
                        litQSN032.Text = .Item("QSN034")
                        litQSC031.Text = .Item("QSC031")
                        litQSC033.Text = .Item("QSC033")
                        litQSC035.Text = .Item("QSC035")
                        litQSP030.Text = .Item("QSP030")
                        litQSP031.Text = .Item("QSP031")
                        litQSP033.Text = .Item("QSP033")
                        litQSP035.Text = .Item("QSP035")
                        litQSE030.Text = .Item("QSE030")
                        litQSE031.Text = .Item("QSE031")
                        litQSE033.Text = .Item("QSE033")
                        litQSE035.Text = .Item("QSE035")
                        litQSN030.Text = .Item("QSN030")
                        litQSN031.Text = .Item("QSN031")
                        litQSN033.Text = .Item("QSN033")
                        litQSN035.Text = .Item("QSN035")
                        litQSC030.Text = .Item("QSC030")
                        litFBC1.Text = .Item("FBC1")
                        litFBC2.Text = .Item("FBC2")
                        litFBC5.Text = .Item("FBC5")



                        'If .Item("ConditionCount") > 0 Then
                        'BankCheckConditionsHeaderRow.Visible = True
                        'BankCheckConditionsDataRow.Visible = True

                        'If BankData.Tables.Count > 1 Then
                        'gvBankconditions.DataSource = BankData.Tables(1)
                        'gvBankconditions.DataBind()
                        'End If
                        'Else
                        BankCheckConditionsHeaderRow.Visible = False
                        BankCheckConditionsDataRow.Visible = False
                        'End If
                    End With
                End If
            Else
                pnlBankCheck.Visible = False
                Panel2.Visible = False
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateCreditSearch(ByVal CreditStatus As Integer)
        Try
            Dim dtMain As DataTable = Nothing

            Dim CreditSearchDS As DataTable = objExperianData.GetAllConsumerDataSerachTables(LoanID)
            'If CreditSearchDS IsNot Nothing AndAlso CreditSearchDS.Rows.Count > 0 Then
            If CreditSearchDS.Rows.Count > 0 Then
                'CD_Scoring_Row.Visible = True
                dtMain = CreditSearchDS
                Populate_CONSUMERDATA_MAIN(dtMain, CreditStatus)
                Populate_CreditSearch_Electoral(dtMain, CreditStatus)
                Populate_CreditSearch_MatchAddress(dtMain, CreditStatus)
            Else
                CD_Scoring_Row.Visible = False
                CreditReport.Visible = False
                CD_PubInfo_Row.Visible = False
                CD_IMPAIRED_CH.Visible = False
            End If
            'End If



        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


#Region "Populating each credit search table"
    Private Sub Populate_CONSUMERDATA_MAIN(ByVal dt As DataTable, ByVal CreditStatus As Integer)
        Try
            Dim AdrStatus As String = ""
            Dim CreditData As DataTable = objExperianData.GetAllConsumerDataSerachTables(LoanID)
            Dim dtApp As DataTable = objExperianData.GetCreditSearchMatchAddressData(LoanID)
            If CreditData IsNot Nothing AndAlso CreditData.Rows.Count > 0 Then
                If dtApp IsNot Nothing AndAlso dtApp.Rows.Count > 0 Then
                    AdrStatus = dtApp.Rows(0).Item("AddressMatchStatus")
                End If
                If AdrStatus.Equals("noMatch") Or AdrStatus.Equals("multipleMatch") Then
                        Panel3.Visible = True
                        CD_Scoring_Row.Visible = False
                        CreditReport.Visible = False
                        Literal6.Text = AdrStatus
                    Else
                        CD_Scoring_Row.Visible = True
                        CreditReport.Visible = True
                        Panel3.Visible = False
                        With CreditData.Rows(0)

                            Scoring_E5S01.Text = If(.Item("FTOLF04_Value") IsNot DBNull.Value, .Item("FTOLF04_Value"), "")
                            Scoring_E5S02.Text = If(.Item("RNOLF04_Value") IsNot DBNull.Value, .Item("RNOLF04_Value"), "")
                            Scoring_E5S041.Text = CInt(Scoring_E5S01.Text) + CInt(Scoring_E5S02.Text)
                            QSC032.Text = If(.Item("QSC032") IsNot DBNull.Value, .Item("QSC032"), "")
                            QSC034.Text = If(.Item("QSC034") IsNot DBNull.Value, .Item("QSC034"), "")
                            QSP032.Text = If(.Item("QSP032") IsNot DBNull.Value, .Item("QSP032"), "")
                            QSP034.Text = If(.Item("QSP034") IsNot DBNull.Value, .Item("QSP034"), "")
                            QSE032.Text = If(.Item("QSE032") IsNot DBNull.Value, .Item("QSE032"), "")
                            QSE034.Text = If(.Item("QSE034") IsNot DBNull.Value, .Item("QSE034"), "")
                            QSN032.Text = If(.Item("QSN032") IsNot DBNull.Value, .Item("QSN032"), "")
                            QSN034.Text = If(.Item("QSN034") IsNot DBNull.Value, .Item("QSN034"), "")
                            QSC031.Text = If(.Item("QSC031") IsNot DBNull.Value, .Item("QSC031"), "")
                            QSC033.Text = If(.Item("QSC033") IsNot DBNull.Value, .Item("QSC033"), "")
                            QSC035.Text = If(.Item("QSC035") IsNot DBNull.Value, .Item("QSC035"), "")
                            QSP030.Text = If(.Item("QSP030") IsNot DBNull.Value, .Item("QSP030"), "")
                            QSP031.Text = If(.Item("QSP031") IsNot DBNull.Value, .Item("QSP031"), "")
                            QSP033.Text = If(.Item("QSP033") IsNot DBNull.Value, .Item("QSP033"), "")
                            QSP035.Text = If(.Item("QSP035") IsNot DBNull.Value, .Item("QSP035"), "")
                            QSE030.Text = If(.Item("QSE030") IsNot DBNull.Value, .Item("QSE030"), "")
                            QSE031.Text = If(.Item("QSE031") IsNot DBNull.Value, .Item("QSE031"), "")
                            QSE033.Text = If(.Item("QSE033") IsNot DBNull.Value, .Item("QSE033"), "")
                            QSE035.Text = If(.Item("QSE035") IsNot DBNull.Value, .Item("QSE035"), "")
                            QSN030.Text = If(.Item("QSN030") IsNot DBNull.Value, .Item("QSN030"), "")
                            QSN031.Text = If(.Item("QSN031") IsNot DBNull.Value, .Item("QSN031"), "")
                            QSN033.Text = If(.Item("QSN033") IsNot DBNull.Value, .Item("QSN033"), "")
                            QSN035.Text = If(.Item("QSN035") IsNot DBNull.Value, .Item("QSN035"), "")
                            QSC030.Text = If(.Item("QSC030") IsNot DBNull.Value, .Item("QSC030"), "")
                            FBC1.Text = If(.Item("FBC1") IsNot DBNull.Value, .Item("FBC1"), "")
                            FBC2.Text = If(.Item("FBC2") IsNot DBNull.Value, .Item("FBC2"), "")
                            FBC5.Text = If(.Item("FBC5") IsNot DBNull.Value, .Item("FBC5"), "")
                            Options.Text = If(.Item("Option") IsNot DBNull.Value, .Item("Option"), "")

                        End With
                    End If
                Else
                    CD_Scoring_Row.Visible = False
                CreditReport.Visible = False
                Panel3.Visible = False
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CreditSearch_Electoral(ByVal dt As DataTable, ByVal CreditStatus As Integer)
        Try
            Dim Electoral As DataTable = objExperianData.GetCreditSearchElectroRollData(LoanID)
            If Electoral.Rows.Count > 0 Then
                CD_PubInfo_Row.Visible = True
                ElectroRoll.DataSource = Electoral
                ElectroRoll.DataBind()
            Else
                CD_PubInfo_Row.Visible = False
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CreditSearch_MatchAddress(ByVal dt As DataTable, ByVal CreditStatus As Integer)
        Try
            Dim AdrStatus As String = ""
            Dim dtApp As DataTable = objExperianData.GetCreditSearchMatchAddressData(LoanID)
            If dtApp.Rows.Count > 0 Then
                AdrStatus = dtApp.Rows(0).Item("AddressMatchStatus")
                If AdrStatus.Equals("noMatch") Or AdrStatus.Equals("multipleMatch") Then
                    CD_IMPAIRED_CH.Visible = False
                    'Panel3.Visible = True
                    'Literal6.Text = AdrStatus
                Else
                    Panel3.Visible = False
                    CD_IMPAIRED_CH.Visible = True
                    AddressMatch.DataSource = dtApp
                    AddressMatch.DataBind()
                End If
            Else
                CD_IMPAIRED_CH.Visible = False
                Panel3.Visible = False
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

#End Region

End Class
