﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffViewExperianReport.aspx.vb" Inherits="Staff_StaffViewExperianReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width:100%;" class="BodyText">
        <tr>
        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Experian Report - 
            <asp:Literal ID="litName" runat="server"></asp:Literal></span>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
        </tr>
        <tr>
            <td class="BodyText"><strong>Authentication: </strong><asp:Literal ID="litAuthStatus" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlAuth" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table class="BodyText" style="width:100%;">
                                    <tr ID="AuthErrorTableRow" runat="server">
                                        <td>
                                            Error Message:</td>
                                        <td>
                                            <asp:Literal ID="litAuthErrorMessage" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Experian Reference:</td>
                                        <td>
                                            <asp:Literal ID="litAuthExperianReference" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            DecCode:</td>
                                        <td>
                                            <asp:Literal ID="litAuthDecCode" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            DecText:</td>
                                        <td>
                                            <asp:Literal ID="litAuthDecText" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            AuthIndex:</td>
                                        <td>
                                            <asp:Literal ID="litAuthAuthIndex" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            AuthText:</td>
                                        <td>
                                            <asp:Literal ID="litAuthAuthText" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Policy Code:</td>
                                        <td>
                                            <asp:Literal ID="litAuthPolicyCode" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Policy Text:</td>
                                        <td>
                                            <asp:Literal ID="litAuthPolicyText" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="BodyText"><strong>Internal Bank Check: </strong>
                <asp:Literal ID="litInternalBankCheckStatus" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlInternalBankCheck" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                Current Account Valid?</td>
                            <td>
                                <asp:Literal ID="litInternalBankValidAcc" runat="server"></asp:Literal>
                            </td>
                            <td>
                                Deposits within last 60 days?</td>
                            <td>
                                <asp:Literal ID="litInternalBankDeposits" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="BodyText"><strong>Bank Wizard: </strong>
                <asp:Literal ID="litBankWizardStatus" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlBankCheck" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            Verification Status:</td>
                                        <td>
                                            <asp:Literal ID="litBankVerificationStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Condition Count:</td>
                                        <td>
                                            <asp:Literal ID="litBankConditionCount" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Bacs Code:</td>
                                        <td>
                                            <asp:Literal ID="litBankBacsCode" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data Access Key:</td>
                                        <td>
                                            <asp:Literal ID="litBankDataAccessKey" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Personal Details Score:</td>
                                        <td>
                                            <asp:Literal ID="litBankPersonalDetailsScore" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Address Score:</td>
                                        <td>
                                            <asp:Literal ID="litBankAddressScore" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Account Type Match:</td>
                                        <td>
                                            <asp:Literal ID="litBankAccountTypeMatch" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Account Setup Date Score:</td>
                                        <td>
                                            <asp:Literal ID="litBankAccountSetupDateScore" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Account Setup Date Match</td>
                                        <td>
                                            <asp:Literal ID="litBankAccountSetupDateMatch" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Account Owner Match:</td>
                                        <td>
                                            <asp:Literal ID="litBankAccountOwnerMatch" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr runat="server" id="BankCheckConditionsHeaderRow">
                            <td><strong>
                                Bank Wizard: Conditions</strong></td>
                        </tr>
                        <tr runat="server" id="BankCheckConditionsDataRow">
                            <td>
                                <asp:GridView ID="gvBankconditions" runat="server" AutoGenerateColumns="False" 
                                    EnableModelValidation="True" SkinID="NormalGrid" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ErrorCode" HeaderText="ErrorCode">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Sevirity" HeaderText="Sevirity">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Value" HeaderText="Value">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #3366FF">
                <strong style="color: #C0C0C0">Consumer Search Report</strong></td>
        </tr>        
        <tr runat="server" id = "CreditReport">
            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr id="CD_Scoring_Row" runat="server">
            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="background-color: #C0C0C0">
                            <strong>SCORING</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Literal ID="liDescScoring_E5S01" runat="server" 
                                            Text="Population indicator "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litDescScoring_E5S02" runat="server" 
                                            Text="Sub population indicator "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S02" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="litDescScoring_E5S041" runat="server" 
                                            Text="Scorecard identifier 1(1)"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S041" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="descScoring_E5S042" runat="server" 
                                            Text="Scorecard identifier 1(2)"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S042" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server" Text="Scorecard identifier 1(3)"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S043" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal3" runat="server" Text="Opt in Score"></asp:Literal>
                                    </td>
                                    <td style="font-weight: 700">
                                        <asp:Literal ID="Scoring_E5S051" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal5" runat="server" Text="Opt Out Score"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S052" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal7" runat="server" Text="Alert For Associates Score"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_E5S053" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal8" runat="server" Text="Household override score "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_NDHHOSCORE" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal10" runat="server" Text="Scorecard identifier 2 (1)"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_NDSI21" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal12" runat="server" Text="Score identifier 2 (2)  "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_NDSI22" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal14" runat="server" Text="Score identifier 2 (3)  "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Scoring_NDSI23" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color: #C0C0C0">
                            <strong>ELECTORAL ROLL/ADDRESS CONFIRMATION</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2">
                                        <b>First consumer / current address</b></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal2" runat="server" 
                                            Text="Reference for SS "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal6" runat="server" 
                                            Text="Reference for SP "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q02" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal11" runat="server" 
                                            Text="Years on ER for SS"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q03" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal15" runat="server" 
                                            Text="Years on ER for SP"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q04" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>First consumer / previous address</strong></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal17" runat="server" Text="Reference for SS "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q05" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal19" runat="server" Text="Reference for SP "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q06" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal21" runat="server" Text="Years on ER for SS"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q07" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal23" runat="server" Text="Years on ER for SP"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q08" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Second consumer current address</strong></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal25" runat="server" Text="Reference for SS "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q09" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal27" runat="server" Text="Reference for SP "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q10" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal29" runat="server" Text="Years on ER for SS"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q11" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal31" runat="server" Text="Years on ER for SP"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q12" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px" colspan="2">
                                        <strong>Second consumer previous address</strong></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal32" runat="server" Text="Reference for SS "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q13" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal33" runat="server" Text="Reference for SP "></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q14" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal34" runat="server" Text="Years on ER for SS"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q15" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal35" runat="server" Text="Years on ER for SP"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q16" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px" colspan="3">
                                        <strong>Name confirmed via electoral roll and cais</strong></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        SP</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q17" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        SPA</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4Q18" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px" colspan="3">
                                        <strong>Address variables </strong>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        Type of address (first consumers main address)</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4R01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Postcode of first consumer
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4R02" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Postcode of second consumer
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_E4R03" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        First app. Previous addr</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA4R01PM" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        Second app. Current addr.
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA4R01CJ" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Second app. Previous addr.
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA4R01PJ" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Main Consumer Electoral roll/PAF (forename match)</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_NDERL01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Main Consumer Electoral roll/PAF (surname match)</td>
                                    <td style="margin-left: 80px">
                                        <asp:Literal ID="ElectoralRoll_NDERL02" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px" colspan="2">
                                        <strong>First consumer / current address</strong></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="margin-left: 80px">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        Reference for SF</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA2Q01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Years on ER for SF</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA2Q02" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        VR/PAF No Trace (MA/CA)</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_NDERLMACA" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        VR/PAF No Trace (MA/PA)</td>
                                    <td style="margin-left: 80px">
                                        <asp:Literal ID="ElectoralRoll_NDERLMAPA" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        VR/PAF No Trace (JA/CA)</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_NDERLJACA" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        VR/PAF No Trace (JA/PA)</td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_NDERLJAPA" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Electoral Roll Current addr surname count
                                    </td>
                                    <td>
                                        <asp:Literal ID="ElectoralRoll_EA5U01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        Electoral Roll Previous addr surname count</td>
                                    <td style="margin-left: 80px">
                                        <asp:Literal ID="ElectoralRoll_EA5U02" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr id="CD_PubInfo_Row" runat="server">
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width:100%;">
                                <tr>
                                    <td style="background-color: #C0C0C0">
                                        <strong>PUBLIC INFORMATION</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Public information – same person</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="12%">
                                                    # CCJ&#39;s</td>
                                                <td width="13%">
                                        <asp:Literal ID="E1A01" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    Total value (class 1) of CCJ&#39;s</td>
                                                <td width="13%">
                                        <asp:Literal ID="E1A02" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    Age of most recent CCJ</td>
                                                <td width="13%">
                                        <asp:Literal ID="E1A03" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    Bankruptcy detected (SP)</td>
                                                <td width="13%">
                                        <asp:Literal ID="EA1C01" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Outstanding CCJ information </strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="12%">
                                                    # O/S CCJ&#39;s (SP)</td>
                                                <td width="13%">
                                        <asp:Literal ID="EA1D01" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    Total value (class 1) of O/S CCJ&#39;s (SP)</td>
                                                <td width="13%">
                                        <asp:Literal ID="EA1D02" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    Age of most recent O/S CCJ (SP)</td>
                                                <td width="13%">
                                        <asp:Literal ID="EA1D03" runat="server"></asp:Literal>
                                                </td>
                                                <td width="12%">
                                                    &nbsp;</td>
                                                <td width="13%">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                        <td>
                            <strong>Public information – same person associate</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        # CCJ&#39;s</td>
                                    <td width="13%">
                                        <asp:Literal ID="E2G01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Total value (class 1) of CCJ&#39;s</td>
                                    <td width="13%">
                                        <asp:Literal ID="E2G02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Age of most recent CCJ</td>
                                    <td width="13%">
                                        <asp:Literal ID="E2G03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy detected (SP)</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA2I01" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>O/s CCJ information </strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        # O/S CCJ&#39;s (SPA)</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA2J01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Total value (class 1) of O/S CCJ&#39;s (SPA)</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA2J02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Age of most recent O/S CCJ (SPA)</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA2J03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Public information – all</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        Satisfied CCJ detected (ALL)</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4Q06" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy Restriction Order/Undertaking present (SP)</td>
                                    <td width="13%">
                                        <asp:Literal ID="SPABRPRESENT" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy Restriction Order/Undertaking present (SPA)</td>
                                    <td width="13%">
                                        <asp:Literal ID="SPBRPRESENT" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                            </table>
                        </td>
                    </tr>
                    
                    
                </table>
            </td>
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr id="CD_IMPAIRED_CH" runat="server">
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="background-color: #C0C0C0"><strong>IMPAIRED CREDIT HISTORY</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        Impaired Credit History</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDICH" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Secured Arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDSECARR" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Unsecured Arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDUNSECARR" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        CCJ Amount</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDCCJ" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Individual Voluntary Arrangement</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDIVA" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDBANKRUPT" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Impaired Credit History</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMAICH" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Secured Arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMASECARR" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Unsecured arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMAUNSECARR" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        CCJ Amount</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMACCJ" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Individual Voluntary Arrangement</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMAIVA" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDMABANKRUPT" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Impaired Credit History</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJAICH" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Secured Arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJASECARR" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Unsecured arrears</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJAUNSECARR" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        CCJ Amount</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJACCJ" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Individual Voluntary Arrangement</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJAIVA" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Bankruptcy</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDJABANKRUPT" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="CD_MOSAIC" runat="server">
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="background-color: #C0C0C0"><strong>MOSAIC/GEO-DELPHI</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>MOSAIC percentages (same person current address postcode)</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        Addresses with families </td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Addresses with singles</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Addresses with mixed households </td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Addresses with unsatisfied CCJ&#39;s</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M04" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        No of CCJ&#39;s as a % of electors</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M05" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        No directorships as a % of electors 2 d.p.</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M06" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Unsatisfied CCJ&#39;s &gt; £500 </td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M07" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Building Society searches - unavailable in MOSAIC 97 </td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M08" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Electric sale/rent searches – unavailable in MOSAIC 97 </td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M09" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        FLA company type searches – unavailable in MOSAIC 97</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M10" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Personal loan searches – unavailable in MOSAIC 97</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M11" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Public utility searches – unavailable in MOSAIC 97</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4M12" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        MOSAIC 97 code</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4T01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        N.I. Mosaic Code</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA5T01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Financial Mosaic Code</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA5T02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Geo-Delphi Index</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG01" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Factor scores</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="12%">
                                        Wealth</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4N01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Children</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4N02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Stability</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4N03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Rurability</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4N04" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Education</td>
                                    <td width="13%">
                                        <asp:Literal ID="EA4N05" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Geo-Delphi Score</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode – Ave number of CCJs per h/h</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with CCJ last 36m</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG04" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Postcode – Ave number of CAIS 8/9s per h/h</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG05" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with 3+ CAIS 8/9s</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG06" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with CAIS 8/9 L 24m</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG07" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with delinquent CAIS</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG08" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        Postcode - % h/hs with worst current status 3+</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG09" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with worst status last 6m 4+</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG10" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with worst curr stat rev 3+</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG11" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        Postcode - % h/hs with limit &gt; £10k</td>
                                    <td width="13%">
                                        <asp:Literal ID="NDG12" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="12%">
                                        UK Mosaic Code</td>
                                    <td width="13%">
                                        <asp:Literal ID="UKMOSAIC" runat="server"></asp:Literal>
                                    </td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                    <td width="12%">
                                        &nbsp;</td>
                                    <td width="13%">
                                        &nbsp;</td>
                                </tr>
                                </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="CD_UTILISATION_BLOCK_ROW" runat="server">
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="background-color: #C0C0C0"><strong>UTILISATION BLOCK</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td width="20%">
                                        SP Balance of active CAIS opened in last 6 months (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of all active CAIS (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA02" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of active revolving CAIS opened in last 6 months </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of active revolving CAIS </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA04" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Balance of active non-revolving CAIS opened in last 6 months (excluding 
                                        mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA05" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of all active non-revolving CAIS (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA06" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of all active CAIS with current status of zero (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA07" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of all active CAIS with current status of 1 (excluding mortgages) with unsatisfied CCJ&#39;s</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA08" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Balance of all active CAIS with current status of 2 (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA09" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance of all active CAIS with current status of 3 to 6 (excluding 
                                        mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPA10" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active CAIS with balance &gt; £0 (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB111" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active CAIS with balance &gt; £500 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB112" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Number of active CAIS with balance &gt; £1000 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB113" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active revolving CAIS with balance &gt; £500 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB114" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active non-revolving CAIS with balance &gt; £1000 (excluding 
                                        mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB115" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active CAIS open longer than 12 months with current status of zero 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB116" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Number of active CAIS open longer than 36 months with current status of zero 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB117" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance trend of balance of active revolving CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB218" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance trend of balance of active non-revolving CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB219" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Balance trend of balance of active CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB220" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Balance trend of balance of active CAIS (mortgages only)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB221" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total monthly payments on active non-revolving CAIS with balance &gt; £0 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB322" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total monthly payments on active non-revolving CAIS with balance &gt; £0 
                                        (including mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPB323" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP CIFAS latest reason code </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPC24" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Number of previous searches in last 3 months (Telecoms)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPD25" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of settled CAIS with status 8</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPE126" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of settled CAIS with status 8 in last 12 months</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPE127" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total balance of settled CAIS with status 8</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPE128" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Number of active revolving CAIS with CLU &gt; 50%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF129" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active revolving CAIS with CLU &gt; 75%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF130" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Number of active revolving CAIS with CLU &gt; 100%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF131" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total credit limit on active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF232" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Highest credit limit on active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF233" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Highest CLU of active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF334" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total CLU of active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF335" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Total CLU of active revolving CAIS opened in last 12 months</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPF336" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Age in months of oldest of all CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPG37" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Average age in months of all CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPG38" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Monthly Credit Commitments (revolving)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPH39" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP Monthly Credit commitments (non-revolving)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPH40" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SP Monthly Mortgage Payments</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPH41" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SP-CII-Check digit</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPCIICHECKDIGIT" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of active CAIS opened in last 3 months (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA01" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of all active CAIS (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA02" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Balance of active revolving CAIS opened in last 6 months </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA03" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of active revolving CAIS </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA04" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of active non-revolving CAIS opened in last 6 months (excluding 
                                        mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA05" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of all active non-revolving CAIS (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA06" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Balance of all active CAIS with current status of zero (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA07" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of all active CAIS with current status of 1 (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA08" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of all active CAIS with current status of 2 (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA09" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance of all active CAIS with current status of 3 to 6 (excluding 
                                        mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAA10" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Number of active CAIS with balance &gt; £0 (excluding mortgages) </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB111" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active CAIS with balance &gt; £500 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB112" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active CAIS with balance &gt; £1000 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB113" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active revolving CAIS with balance &gt; £500 (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB114" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Number of active non-revolving CAIS with balance &gt; £1000 (excluding 
                                        mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB115" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active CAIS open longer than 12 months with current status of zero 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB116" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active CAIS open longer than 36 months with current status of zero 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB117" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance trend of balance of active revolving CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB218" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Balance trend of balance of active non-revolving CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB219" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance trend of balance of active CAIS (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB220" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Balance trend of balance of active CAIS (mortgages only)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB221" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Total monthly payments on active non-revolving CAIS with balance &gt; £0 
                                        (excluding mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB322" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Total monthly payments on active non-revolving CAIS with balance &gt; £0 
                                        (including mortgages)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAB323" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA CIFAS latest reason code </td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAC24" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of previous searches in last 3 months (Telecoms)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAD25" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of settled CAIS with status 8</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAE126" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Number of settled CAIS with status 8 in last 12 months</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAE127" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Total balance of settled CAIS with status 8</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAE128" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active revolving CAIS with CLU &gt; 50%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF129" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Number of active revolving CAIS with CLU &gt; 75%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF130" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Number of active revolving CAIS with CLU &gt; 100%</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF131" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Total credit limit on active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF232" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Highest credit limit on active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF233" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Highest CLU of active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF334" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Total CLU of active revolving CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF335" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Total CLU of active revolving CAIS opened in last 12 months</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAF336" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Age in months of oldest of all CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAG37" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Average age in months of all CAIS</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAG38" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        SPA Monthly Credit Commitments (revolving)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAH39" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Monthly Credit commitments (non-revolving)</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAH40" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA Monthly Mortgage Payments</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPAH41" runat="server"></asp:Literal>
                                    </td>
                                    <td width="20%">
                                        SPA-CII-Check Digit</td>
                                    <td width="5%">
                                        <asp:Literal ID="SPACIICHECKDIGIT" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        &nbsp;</td>
                                    <td width="5%">
                                        &nbsp;</td>
                                    <td width="20%">
                                        &nbsp;</td>
                                    <td width="5%">
                                        &nbsp;</td>
                                    <td width="20%">
                                        &nbsp;</td>
                                    <td width="5%">
                                        &nbsp;</td>
                                    <td width="20%">
                                        &nbsp;</td>
                                    <td width="5%">
                                        &nbsp;</td>
                                </tr>
                                </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

