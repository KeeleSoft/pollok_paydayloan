﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Staff_StaffViewExperianReport
    Inherits System.Web.UI.Page

    Dim objPDL As PollokCU.DataAccess.Layer.clsPayDayLoan = New PollokCU.DataAccess.Layer.clsPayDayLoan
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objExperianData As PollokCU.DataAccess.Layer.clsExperian = New PollokCU.DataAccess.Layer.clsExperian
    Dim LoanID As Integer = 0

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Visible = False

            LoanID = Request.QueryString("LoanID")
            If LoanID <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No loan id found"
            End If
            PopulateClientDetails()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateClientDetails()
        Try
            Dim LoanData As DataTable = objPDL.SelectPayDayLoanDetail(LoanID)
            If LoanData IsNot Nothing AndAlso LoanData.Rows.Count > 0 Then
                With LoanData.Rows(0)
                    litName.Text = .Item("FirstName") & " " & .Item("Surname")
                    litAuthStatus.Text = .Item("AuthenticationStatusDesc")
                    litBankWizardStatus.Text = .Item("BankCheckStatusDesc")
                    litInternalBankCheckStatus.Text = .Item("InternalBankCheckStatusDesc")
                    If .Item("AuthenticationStatus") <> 0 Then
                        pnlAuth.Visible = True
                        PopulateAuthenticationPanel(LoanData.Rows(0))
                    Else
                        pnlAuth.Visible = False
                    End If

                    If .Item("BankCheckStatus") <> 0 Then
                        pnlBankCheck.Visible = True
                        PopulateBankCheckPanel()
                    Else
                        pnlBankCheck.Visible = False
                    End If

                    If .Item("InternalBankCheckStatus") <> 0 Then
                        pnlInternalBankCheck.Visible = True
                        litInternalBankValidAcc.Text = IIf(.Item("InternalBankCheckValidAccount") = True, "Yes", "No")
                        litInternalBankDeposits.Text = IIf(.Item("InternalBankCheckValidDeposits") = True, "Yes", "No")
                    Else
                        pnlInternalBankCheck.Visible = False
                    End If

                    If .Item("CreditCheckStatus") <> 0 Then
                        CreditReport.Visible = True
                        PopulateCreditSearch()
                    Else
                        CreditReport.Visible = False
                    End If
                End With
            End If
            PopulateCreditSearch()  '''TODO:Remove
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateAuthenticationPanel(ByVal dr As DataRow)
        Try

            With dr
                If .Item("ExperianFailedMessage").ToString.Length > 0 Then
                    AuthErrorTableRow.Visible = True
                    litAuthErrorMessage.Text = .Item("ExperianFailedMessage")
                Else
                    AuthErrorTableRow.Visible = False
                End If

                litAuthExperianReference.Text = .Item("ExperianRef")
                litAuthDecCode.Text = .Item("ExperianAuthDecisionCode")
                litAuthDecText.Text = .Item("ExperianAuthDecisionText")
                litAuthAuthIndex.Text = .Item("ExperianAuthIndexCode")
                litAuthAuthText.Text = .Item("ExperianAuthIndexText")
                litAuthPolicyCode.Text = .Item("ExperianPolicyCode")
                litAuthPolicyText.Text = .Item("ExperianPolicyRuleText")

            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateBankCheckPanel()
        Try
            Dim BankData As DataSet = objExperianData.GetBankCheckData(LoanID)
            If BankData IsNot Nothing AndAlso BankData.Tables.Count > 0 Then
                With BankData.Tables(0).Rows(0)
                    litBankVerificationStatus.Text = .Item("VerificationStatus")
                    litBankConditionCount.Text = .Item("ConditionCount")
                    litBankBacsCode.Text = .Item("BacsCode")
                    litBankDataAccessKey.Text = .Item("DataAccessKey")
                    litBankPersonalDetailsScore.Text = .Item("PersonalDetailsScore")
                    litBankAddressScore.Text = .Item("AddressScore")
                    litBankAccountTypeMatch.Text = .Item("AccountTypeMatch")
                    litBankAccountSetupDateScore.Text = .Item("AccountSetupDateScore")
                    litBankAccountSetupDateMatch.Text = .Item("AccountSetupDateMatch")
                    litBankAccountOwnerMatch.Text = .Item("AccountOwnerMatch")

                    If .Item("ConditionCount") > 0 Then
                        BankCheckConditionsHeaderRow.Visible = True
                        BankCheckConditionsDataRow.Visible = True

                        If BankData.Tables.Count > 1 Then
                            gvBankconditions.DataSource = BankData.Tables(1)
                            gvBankconditions.DataBind()
                        End If
                    Else
                        BankCheckConditionsHeaderRow.Visible = False
                        BankCheckConditionsDataRow.Visible = False
                    End If
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateCreditSearch()
        Try
            Dim dtMain As DataTable = Nothing

            Dim CreditSearchDS As DataSet = objExperianData.GetAllConsumerDataSerachTables(LoanID)
            If CreditSearchDS IsNot Nothing AndAlso CreditSearchDS.Tables.Count > 0 Then
                If CreditSearchDS.Tables.Count > 0 AndAlso CreditSearchDS.Tables(0).Rows.Count > 0 Then
                    CD_Scoring_Row.Visible = True
                    dtMain = CreditSearchDS.Tables(0)
                    Populate_CONSUMERDATA_MAIN(dtMain)
                Else
                    CD_Scoring_Row.Visible = False
                End If

                If CreditSearchDS.Tables.Count > 1 AndAlso CreditSearchDS.Tables(1).Rows.Count > 0 Then
                    CD_PubInfo_Row.Visible = True
                    Dim dtPublicinfo As DataTable = CreditSearchDS.Tables(1)
                    Populate_CONSUMERDATA_PublicInfo(dtPublicinfo)
                Else
                    CD_PubInfo_Row.Visible = False
                End If

                If CreditSearchDS.Tables.Count > 2 AndAlso CreditSearchDS.Tables(2).Rows.Count > 0 Then
                    CD_IMPAIRED_CH.Visible = True
                    Dim dtImparedCH As DataTable = CreditSearchDS.Tables(2)
                    Populate_CONSUMERDATA_ImparedCH(dtImparedCH)
                Else
                    CD_IMPAIRED_CH.Visible = False
                End If

                If CreditSearchDS.Tables.Count > 3 AndAlso CreditSearchDS.Tables(3).Rows.Count > 0 Then
                    CD_MOSAIC.Visible = True
                    Dim dtMosaic As DataTable = CreditSearchDS.Tables(3)
                    Populate_CONSUMERDATA_MOSAIC(dtMosaic)
                Else
                    CD_MOSAIC.Visible = False
                End If

                If CreditSearchDS.Tables.Count > 4 AndAlso CreditSearchDS.Tables(4).Rows.Count > 0 Then
                    CD_UTILISATION_BLOCK_ROW.Visible = True
                    Dim dtUti As DataTable = CreditSearchDS.Tables(4)
                    Populate_CONSUMERDATA_UtilisationBlock(dtUti)
                Else
                    CD_UTILISATION_BLOCK_ROW.Visible = False
                End If
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub


#Region "Populating each credit search table"
    Private Sub Populate_CONSUMERDATA_MAIN(ByVal dt As DataTable)
        Try
            With dt.Rows(0)
                Scoring_E5S01.Text = If(.Item("Scoring_E5S01") IsNot DBNull.Value, .Item("Scoring_E5S01"), "")
                Scoring_E5S02.Text = If(.Item("Scoring_E5S02") IsNot DBNull.Value, .Item("Scoring_E5S02"), "")
                Scoring_E5S041.Text = If(.Item("Scoring_E5S041") IsNot DBNull.Value, .Item("Scoring_E5S041"), "")
                Scoring_E5S042.Text = If(.Item("Scoring_E5S042") IsNot DBNull.Value, .Item("Scoring_E5S042"), "")
                Scoring_E5S043.Text = If(.Item("Scoring_E5S043") IsNot DBNull.Value, .Item("Scoring_E5S043"), "")
                Scoring_E5S051.Text = If(.Item("Scoring_E5S051") IsNot DBNull.Value, .Item("Scoring_E5S051"), "")
                Scoring_E5S052.Text = If(.Item("Scoring_E5S052") IsNot DBNull.Value, .Item("Scoring_E5S052"), "")
                Scoring_E5S053.Text = If(.Item("Scoring_E5S053") IsNot DBNull.Value, .Item("Scoring_E5S053"), "")
                Scoring_NDHHOSCORE.Text = If(.Item("Scoring_NDHHOSCORE") IsNot DBNull.Value, .Item("Scoring_NDHHOSCORE"), "")
                Scoring_NDSI21.Text = If(.Item("Scoring_NDSI21") IsNot DBNull.Value, .Item("Scoring_NDSI21"), "")
                Scoring_NDSI22.Text = If(.Item("Scoring_NDSI22") IsNot DBNull.Value, .Item("Scoring_NDSI22"), "")
                Scoring_NDSI23.Text = If(.Item("Scoring_NDSI23") IsNot DBNull.Value, .Item("Scoring_NDSI23"), "")

                ElectoralRoll_E4Q01.Text = If(.Item("ElectoralRoll_E4Q01") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q01"), "")
                ElectoralRoll_E4Q02.Text = If(.Item("ElectoralRoll_E4Q02") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q02"), "")
                ElectoralRoll_E4Q03.Text = If(.Item("ElectoralRoll_E4Q03") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q03"), "")
                ElectoralRoll_E4Q04.Text = If(.Item("ElectoralRoll_E4Q04") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q04"), "")
                ElectoralRoll_E4Q05.Text = If(.Item("ElectoralRoll_E4Q05") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q05"), "")
                ElectoralRoll_E4Q06.Text = If(.Item("ElectoralRoll_E4Q06") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q06"), "")
                ElectoralRoll_E4Q07.Text = If(.Item("ElectoralRoll_E4Q07") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q07"), "")
                ElectoralRoll_E4Q08.Text = If(.Item("ElectoralRoll_E4Q08") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q08"), "")
                ElectoralRoll_E4Q09.Text = If(.Item("ElectoralRoll_E4Q09") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q09"), "")
                ElectoralRoll_E4Q10.Text = If(.Item("ElectoralRoll_E4Q10") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q10"), "")
                ElectoralRoll_E4Q11.Text = If(.Item("ElectoralRoll_E4Q11") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q11"), "")
                ElectoralRoll_E4Q12.Text = If(.Item("ElectoralRoll_E4Q12") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q12"), "")
                ElectoralRoll_E4Q13.Text = If(.Item("ElectoralRoll_E4Q13") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q13"), "")
                ElectoralRoll_E4Q14.Text = If(.Item("ElectoralRoll_E4Q14") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q14"), "")
                ElectoralRoll_E4Q15.Text = If(.Item("ElectoralRoll_E4Q15") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q15"), "")
                ElectoralRoll_E4Q16.Text = If(.Item("ElectoralRoll_E4Q16") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q16"), "")
                ElectoralRoll_E4Q17.Text = If(.Item("ElectoralRoll_E4Q17") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q17"), "")
                ElectoralRoll_E4Q18.Text = If(.Item("ElectoralRoll_E4Q18") IsNot DBNull.Value, .Item("ElectoralRoll_E4Q18"), "")
                ElectoralRoll_E4R01.Text = If(.Item("ElectoralRoll_E4R01") IsNot DBNull.Value, .Item("ElectoralRoll_E4R01"), "")
                ElectoralRoll_E4R02.Text = If(.Item("ElectoralRoll_E4R02") IsNot DBNull.Value, .Item("ElectoralRoll_E4R02"), "")
                ElectoralRoll_E4R03.Text = If(.Item("ElectoralRoll_E4R03") IsNot DBNull.Value, .Item("ElectoralRoll_E4R03"), "")
                ElectoralRoll_EA4R01PM.Text = If(.Item("ElectoralRoll_EA4R01PM") IsNot DBNull.Value, .Item("ElectoralRoll_EA4R01PM"), "")
                ElectoralRoll_EA4R01CJ.Text = If(.Item("ElectoralRoll_EA4R01CJ") IsNot DBNull.Value, .Item("ElectoralRoll_EA4R01CJ"), "")
                ElectoralRoll_EA4R01PJ.Text = If(.Item("ElectoralRoll_EA4R01PJ") IsNot DBNull.Value, .Item("ElectoralRoll_EA4R01PJ"), "")
                ElectoralRoll_NDERL01.Text = If(.Item("ElectoralRoll_NDERL01") IsNot DBNull.Value, .Item("ElectoralRoll_NDERL01"), "")
                ElectoralRoll_NDERL02.Text = If(.Item("ElectoralRoll_NDERL02") IsNot DBNull.Value, .Item("ElectoralRoll_NDERL02"), "")
                ElectoralRoll_EA2Q01.Text = If(.Item("ElectoralRoll_EA2Q01") IsNot DBNull.Value, .Item("ElectoralRoll_EA2Q01"), "")
                ElectoralRoll_EA2Q02.Text = If(.Item("ElectoralRoll_EA2Q02") IsNot DBNull.Value, .Item("ElectoralRoll_EA2Q02"), "")
                ElectoralRoll_NDERLMACA.Text = If(.Item("ElectoralRoll_NDERLMACA") IsNot DBNull.Value, .Item("ElectoralRoll_NDERLMACA"), "")
                ElectoralRoll_NDERLMAPA.Text = If(.Item("ElectoralRoll_NDERLMAPA") IsNot DBNull.Value, .Item("ElectoralRoll_NDERLMAPA"), "")
                ElectoralRoll_NDERLJACA.Text = If(.Item("ElectoralRoll_NDERLJACA") IsNot DBNull.Value, .Item("ElectoralRoll_NDERLJACA"), "")
                ElectoralRoll_NDERLJAPA.Text = If(.Item("ElectoralRoll_NDERLJAPA") IsNot DBNull.Value, .Item("ElectoralRoll_NDERLJAPA"), "")
                ElectoralRoll_EA5U01.Text = If(.Item("ElectoralRoll_EA5U01") IsNot DBNull.Value, .Item("ElectoralRoll_EA5U01"), "")
                ElectoralRoll_EA5U02.Text = If(.Item("ElectoralRoll_EA5U02") IsNot DBNull.Value, .Item("ElectoralRoll_EA5U02"), "")

            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CONSUMERDATA_PublicInfo(ByVal dt As DataTable)
        Try
            With dt.Rows(0)
                E1A01.Text = If(.Item("E1A01") IsNot DBNull.Value, .Item("E1A01"), "")
                E1A02.Text = If(.Item("E1A02") IsNot DBNull.Value, .Item("E1A02"), "")
                E1A03.Text = If(.Item("E1A03") IsNot DBNull.Value, .Item("E1A03"), "")
                EA1C01.Text = If(.Item("EA1C01") IsNot DBNull.Value, .Item("EA1C01"), "")
                EA1D01.Text = If(.Item("EA1D01") IsNot DBNull.Value, .Item("EA1D01"), "")
                EA1D02.Text = If(.Item("EA1D02") IsNot DBNull.Value, .Item("EA1D02"), "")
                EA1D03.Text = If(.Item("EA1D03") IsNot DBNull.Value, .Item("EA1D03"), "")
                E2G01.Text = If(.Item("E2G01") IsNot DBNull.Value, .Item("E2G01"), "")
                E2G02.Text = If(.Item("E2G02") IsNot DBNull.Value, .Item("E2G02"), "")
                E2G03.Text = If(.Item("E2G03") IsNot DBNull.Value, .Item("E2G03"), "")
                EA2I01.Text = If(.Item("EA2I01") IsNot DBNull.Value, .Item("EA2I01"), "")
                EA4Q06.Text = If(.Item("EA4Q06") IsNot DBNull.Value, .Item("EA4Q06"), "")
                SPABRPRESENT.Text = If(.Item("SPABRPRESENT") IsNot DBNull.Value, .Item("SPABRPRESENT"), "")
                SPBRPRESENT.Text = If(.Item("SPBRPRESENT") IsNot DBNull.Value, .Item("SPBRPRESENT"), "")

            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CONSUMERDATA_ImparedCH(ByVal dt As DataTable)
        Try
            With dt.Rows(0)
                NDICH.Text = If(.Item("NDICH") IsNot DBNull.Value, .Item("NDICH"), "")
                NDSECARR.Text = If(.Item("NDSECARR") IsNot DBNull.Value, .Item("NDSECARR"), "")
                NDUNSECARR.Text = If(.Item("NDUNSECARR") IsNot DBNull.Value, .Item("NDUNSECARR"), "")
                NDCCJ.Text = If(.Item("NDCCJ") IsNot DBNull.Value, .Item("NDCCJ"), "")
                NDIVA.Text = If(.Item("NDIVA") IsNot DBNull.Value, .Item("NDIVA"), "")
                NDBANKRUPT.Text = If(.Item("NDBANKRUPT") IsNot DBNull.Value, .Item("NDBANKRUPT"), "")
                NDMAICH.Text = If(.Item("NDMAICH") IsNot DBNull.Value, .Item("NDMAICH"), "")
                NDMASECARR.Text = If(.Item("NDMASECARR") IsNot DBNull.Value, .Item("NDMASECARR"), "")
                NDMAUNSECARR.Text = If(.Item("NDMAUNSECARR") IsNot DBNull.Value, .Item("NDMAUNSECARR"), "")
                NDMACCJ.Text = If(.Item("NDMACCJ") IsNot DBNull.Value, .Item("NDMACCJ"), "")
                NDMAIVA.Text = If(.Item("NDMAIVA") IsNot DBNull.Value, .Item("NDMAIVA"), "")
                NDMABANKRUPT.Text = If(.Item("NDMABANKRUPT") IsNot DBNull.Value, .Item("NDMABANKRUPT"), "")
                NDJAICH.Text = If(.Item("NDJAICH") IsNot DBNull.Value, .Item("NDJAICH"), "")
                NDJASECARR.Text = If(.Item("NDJASECARR") IsNot DBNull.Value, .Item("NDJASECARR"), "")
                NDJAUNSECARR.Text = If(.Item("NDJAUNSECARR") IsNot DBNull.Value, .Item("NDJAUNSECARR"), "")
                NDJACCJ.Text = If(.Item("NDJACCJ") IsNot DBNull.Value, .Item("NDJACCJ"), "")
                NDJAIVA.Text = If(.Item("NDJAIVA") IsNot DBNull.Value, .Item("NDJAIVA"), "")
                NDJABANKRUPT.Text = If(.Item("NDJABANKRUPT") IsNot DBNull.Value, .Item("NDJABANKRUPT"), "")

            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CONSUMERDATA_MOSAIC(ByVal dt As DataTable)
        Try
            With dt.Rows(0)
                EA4M01.Text = If(.Item("EA4M01") IsNot DBNull.Value, .Item("EA4M01"), "")
                EA4M02.Text = If(.Item("EA4M02") IsNot DBNull.Value, .Item("EA4M02"), "")
                EA4M03.Text = If(.Item("EA4M03") IsNot DBNull.Value, .Item("EA4M03"), "")
                EA4M04.Text = If(.Item("EA4M04") IsNot DBNull.Value, .Item("EA4M04"), "")
                EA4M05.Text = If(.Item("EA4M05") IsNot DBNull.Value, .Item("EA4M05"), "")
                EA4M06.Text = If(.Item("EA4M06") IsNot DBNull.Value, .Item("EA4M06"), "")
                EA4M07.Text = If(.Item("EA4M07") IsNot DBNull.Value, .Item("EA4M07"), "")
                EA4M08.Text = If(.Item("EA4M08") IsNot DBNull.Value, .Item("EA4M08"), "")
                EA4M09.Text = If(.Item("EA4M09") IsNot DBNull.Value, .Item("EA4M09"), "")
                EA4M10.Text = If(.Item("EA4M10") IsNot DBNull.Value, .Item("EA4M10"), "")
                EA4M11.Text = If(.Item("EA4M11") IsNot DBNull.Value, .Item("EA4M11"), "")
                EA4M12.Text = If(.Item("EA4M12") IsNot DBNull.Value, .Item("EA4M12"), "")
                EA4T01.Text = If(.Item("EA4T01") IsNot DBNull.Value, .Item("EA4T01"), "")
                EA5T01.Text = If(.Item("EA5T01") IsNot DBNull.Value, .Item("EA5T01"), "")
                EA5T02.Text = If(.Item("EA5T02") IsNot DBNull.Value, .Item("EA5T02"), "")
                NDG01.Text = If(.Item("NDG01") IsNot DBNull.Value, .Item("NDG01"), "")
                EA4N01.Text = If(.Item("EA4N01") IsNot DBNull.Value, .Item("EA4N01"), "")
                EA4N02.Text = If(.Item("EA4N02") IsNot DBNull.Value, .Item("EA4N02"), "")

                EA4N03.Text = If(.Item("EA4N03") IsNot DBNull.Value, .Item("EA4N03"), "")
                EA4N04.Text = If(.Item("EA4N04") IsNot DBNull.Value, .Item("EA4N04"), "")
                EA4N05.Text = If(.Item("EA4N05") IsNot DBNull.Value, .Item("EA4N05"), "")
                NDG02.Text = If(.Item("NDG02") IsNot DBNull.Value, .Item("NDG02"), "")
                NDG03.Text = If(.Item("NDG03") IsNot DBNull.Value, .Item("NDG03"), "")
                NDG04.Text = If(.Item("NDG04") IsNot DBNull.Value, .Item("NDG04"), "")
                NDG05.Text = If(.Item("NDG05") IsNot DBNull.Value, .Item("NDG05"), "")
                NDG06.Text = If(.Item("NDG06") IsNot DBNull.Value, .Item("NDG06"), "")
                NDG07.Text = If(.Item("NDG07") IsNot DBNull.Value, .Item("NDG07"), "")
                NDG08.Text = If(.Item("NDG08") IsNot DBNull.Value, .Item("NDG08"), "")
                NDG09.Text = If(.Item("NDG09") IsNot DBNull.Value, .Item("NDG09"), "")
                NDG10.Text = If(.Item("NDG10") IsNot DBNull.Value, .Item("NDG10"), "")
                NDG11.Text = If(.Item("NDG11") IsNot DBNull.Value, .Item("NDG11"), "")
                NDG12.Text = If(.Item("NDG12") IsNot DBNull.Value, .Item("NDG12"), "")
                UKMOSAIC.Text = If(.Item("UKMOSAIC") IsNot DBNull.Value, .Item("UKMOSAIC"), "")

            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub Populate_CONSUMERDATA_UtilisationBlock(ByVal dt As DataTable)
        Try
            With dt.Rows(0)
                SPA01.Text = If(.Item("SPA01") IsNot DBNull.Value, .Item("SPA01"), "")
                SPA02.Text = If(.Item("SPA02") IsNot DBNull.Value, .Item("SPA02"), "")
                SPA03.Text = If(.Item("SPA03") IsNot DBNull.Value, .Item("SPA03"), "")
                SPA04.Text = If(.Item("SPA04") IsNot DBNull.Value, .Item("SPA04"), "")
                SPA05.Text = If(.Item("SPA05") IsNot DBNull.Value, .Item("SPA05"), "")
                SPA06.Text = If(.Item("SPA06") IsNot DBNull.Value, .Item("SPA06"), "")
                SPA07.Text = If(.Item("SPA07") IsNot DBNull.Value, .Item("SPA07"), "")
                SPA08.Text = If(.Item("SPA08") IsNot DBNull.Value, .Item("SPA08"), "")
                SPA09.Text = If(.Item("SPA09") IsNot DBNull.Value, .Item("SPA09"), "")
                SPA10.Text = If(.Item("SPA10") IsNot DBNull.Value, .Item("SPA10"), "")
                SPB111.Text = If(.Item("SPB111") IsNot DBNull.Value, .Item("SPB111"), "")
                SPB112.Text = If(.Item("SPB112") IsNot DBNull.Value, .Item("SPB112"), "")
                SPB113.Text = If(.Item("SPB113") IsNot DBNull.Value, .Item("SPB113"), "")
                SPB114.Text = If(.Item("SPB114") IsNot DBNull.Value, .Item("SPB114"), "")
                SPB115.Text = If(.Item("SPB115") IsNot DBNull.Value, .Item("SPB115"), "")
                SPB116.Text = If(.Item("SPB116") IsNot DBNull.Value, .Item("SPB116"), "")
                SPB117.Text = If(.Item("SPB117") IsNot DBNull.Value, .Item("SPB117"), "")
                SPB218.Text = If(.Item("SPB218") IsNot DBNull.Value, .Item("SPB218"), "")
                SPB219.Text = If(.Item("SPB219") IsNot DBNull.Value, .Item("SPB219"), "")
                SPB220.Text = If(.Item("SPB220") IsNot DBNull.Value, .Item("SPB220"), "")
                SPB221.Text = If(.Item("SPB221") IsNot DBNull.Value, .Item("SPB221"), "")
                SPB322.Text = If(.Item("SPB322") IsNot DBNull.Value, .Item("SPB322"), "")
                SPB323.Text = If(.Item("SPB323") IsNot DBNull.Value, .Item("SPB323"), "")
                SPC24.Text = If(.Item("SPC24") IsNot DBNull.Value, .Item("SPC24"), "")
                SPD25.Text = If(.Item("SPD25") IsNot DBNull.Value, .Item("SPD25"), "")
                SPE126.Text = If(.Item("SPE126") IsNot DBNull.Value, .Item("SPE126"), "")
                SPE127.Text = If(.Item("SPE127") IsNot DBNull.Value, .Item("SPE127"), "")
                SPE128.Text = If(.Item("SPE128") IsNot DBNull.Value, .Item("SPE128"), "")
                SPF129.Text = If(.Item("SPF129") IsNot DBNull.Value, .Item("SPF129"), "")
                SPF130.Text = If(.Item("SPF130") IsNot DBNull.Value, .Item("SPF130"), "")
                SPF131.Text = If(.Item("SPF131") IsNot DBNull.Value, .Item("SPF131"), "")
                SPF232.Text = If(.Item("SPF232") IsNot DBNull.Value, .Item("SPF232"), "")
                SPF233.Text = If(.Item("SPF233") IsNot DBNull.Value, .Item("SPF233"), "")
                SPF334.Text = If(.Item("SPF334") IsNot DBNull.Value, .Item("SPF334"), "")
                SPF335.Text = If(.Item("SPF335") IsNot DBNull.Value, .Item("SPF335"), "")
                SPF336.Text = If(.Item("SPF336") IsNot DBNull.Value, .Item("SPF336"), "")
                SPG37.Text = If(.Item("SPG37") IsNot DBNull.Value, .Item("SPG37"), "")
                SPG38.Text = If(.Item("SPG38") IsNot DBNull.Value, .Item("SPG38"), "")
                SPH39.Text = If(.Item("SPH39") IsNot DBNull.Value, .Item("SPH39"), "")
                SPH40.Text = If(.Item("SPH40") IsNot DBNull.Value, .Item("SPH40"), "")
                SPH41.Text = If(.Item("SPH41") IsNot DBNull.Value, .Item("SPH41"), "")
                SPCIICHECKDIGIT.Text = If(.Item("SPCIICHECKDIGIT") IsNot DBNull.Value, .Item("SPCIICHECKDIGIT"), "")
                SPAA01.Text = If(.Item("SPAA01") IsNot DBNull.Value, .Item("SPAA01"), "")
                SPAA02.Text = If(.Item("SPAA02") IsNot DBNull.Value, .Item("SPAA02"), "")
                SPAA03.Text = If(.Item("SPAA03") IsNot DBNull.Value, .Item("SPAA03"), "")
                SPAA04.Text = If(.Item("SPAA04") IsNot DBNull.Value, .Item("SPAA04"), "")
                SPAA05.Text = If(.Item("SPAA05") IsNot DBNull.Value, .Item("SPAA05"), "")
                SPAA06.Text = If(.Item("SPAA06") IsNot DBNull.Value, .Item("SPAA06"), "")
                SPAA07.Text = If(.Item("SPAA07") IsNot DBNull.Value, .Item("SPAA07"), "")
                SPAA08.Text = If(.Item("SPAA08") IsNot DBNull.Value, .Item("SPAA08"), "")
                SPAA09.Text = If(.Item("SPAA09") IsNot DBNull.Value, .Item("SPAA09"), "")
                SPAA10.Text = If(.Item("SPAA10") IsNot DBNull.Value, .Item("SPAA10"), "")
                SPAB111.Text = If(.Item("SPAB111") IsNot DBNull.Value, .Item("SPAB111"), "")
                SPAB112.Text = If(.Item("SPAB112") IsNot DBNull.Value, .Item("SPAB112"), "")
                SPAB113.Text = If(.Item("SPAB113") IsNot DBNull.Value, .Item("SPAB113"), "")
                SPAB114.Text = If(.Item("SPAB114") IsNot DBNull.Value, .Item("SPAB114"), "")
                SPAB115.Text = If(.Item("SPAB115") IsNot DBNull.Value, .Item("SPAB115"), "")
                SPAB116.Text = If(.Item("SPAB116") IsNot DBNull.Value, .Item("SPAB116"), "")
                SPAB117.Text = If(.Item("SPAB117") IsNot DBNull.Value, .Item("SPAB117"), "")
                SPAB218.Text = If(.Item("SPAB218") IsNot DBNull.Value, .Item("SPAB218"), "")
                SPAB219.Text = If(.Item("SPAB219") IsNot DBNull.Value, .Item("SPAB219"), "")
                SPAB220.Text = If(.Item("SPAB220") IsNot DBNull.Value, .Item("SPAB220"), "")
                SPAB221.Text = If(.Item("SPAB221") IsNot DBNull.Value, .Item("SPAB221"), "")
                SPAB322.Text = If(.Item("SPAB322") IsNot DBNull.Value, .Item("SPAB322"), "")
                SPAB323.Text = If(.Item("SPAB323") IsNot DBNull.Value, .Item("SPAB323"), "")
                SPAC24.Text = If(.Item("SPAC24") IsNot DBNull.Value, .Item("SPAC24"), "")
                SPAD25.Text = If(.Item("SPAD25") IsNot DBNull.Value, .Item("SPAD25"), "")
                SPAE126.Text = If(.Item("SPAE126") IsNot DBNull.Value, .Item("SPAE126"), "")
                SPAE127.Text = If(.Item("SPAE127") IsNot DBNull.Value, .Item("SPAE127"), "")
                SPAE128.Text = If(.Item("SPAE128") IsNot DBNull.Value, .Item("SPAE128"), "")
                SPAF129.Text = If(.Item("SPAF129") IsNot DBNull.Value, .Item("SPAF129"), "")
                SPAF130.Text = If(.Item("SPAF130") IsNot DBNull.Value, .Item("SPAF130"), "")
                SPAF131.Text = If(.Item("SPAF131") IsNot DBNull.Value, .Item("SPAF131"), "")
                SPAF232.Text = If(.Item("SPAF232") IsNot DBNull.Value, .Item("SPAF232"), "")
                SPAF233.Text = If(.Item("SPAF233") IsNot DBNull.Value, .Item("SPAF233"), "")
                SPAF334.Text = If(.Item("SPAF334") IsNot DBNull.Value, .Item("SPAF334"), "")
                SPAF335.Text = If(.Item("SPAF335") IsNot DBNull.Value, .Item("SPAF335"), "")
                SPAF336.Text = If(.Item("SPAF336") IsNot DBNull.Value, .Item("SPAF336"), "")
                SPAG37.Text = If(.Item("SPAG37") IsNot DBNull.Value, .Item("SPAG37"), "")
                SPAG38.Text = If(.Item("SPAG38") IsNot DBNull.Value, .Item("SPAG38"), "")
                SPAH39.Text = If(.Item("SPAH39") IsNot DBNull.Value, .Item("SPAH39"), "")
                SPAH40.Text = If(.Item("SPAH40") IsNot DBNull.Value, .Item("SPAH40"), "")
                SPAH41.Text = If(.Item("SPAH41") IsNot DBNull.Value, .Item("SPAH41"), "")
                SPACIICHECKDIGIT.Text = If(.Item("SPACIICHECKDIGIT") IsNot DBNull.Value, .Item("SPACIICHECKDIGIT"), "")
            End With

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
#End Region

End Class
