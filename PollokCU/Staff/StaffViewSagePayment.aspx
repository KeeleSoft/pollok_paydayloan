﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffViewSagePayment.aspx.vb" Inherits="Staff_StaffViewSagePayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width:100%;" class="BodyText">
        <tr>
        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Sage Payment Details - 
            <asp:Literal ID="litName" runat="server"></asp:Literal></span>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
        </tr>
        <tr>
            <td class="BodyText">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlDetails" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table class="BodyText" style="width:100%;">
                                    <tr ID="AuthErrorTableRow" runat="server">
                                        <td>
                                            Payment Status:</td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litSuccessStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Reason:</td>
                                        <td>
                                            <asp:Literal ID="litReason" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Tx code</td>
                                        <td>
                                            <asp:Literal ID="litTxCode" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Amount:</td>
                                        <td>
                                            <asp:Literal ID="litAmount" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Notification Status:</td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litNotificationStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            VPSTxID</td>
                                        <td>
                                            <asp:Literal ID="litVPSTxID" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Member ID:</td>
                                        <td>
                                            <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Name:</td>
                                        <td>
                                            <asp:Literal ID="litFullName" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Email:</td>
                                        <td>
                                            <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address Line 1:</td>
                                        <td>
                                            <asp:Literal ID="litAddress1" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Address Line 2:</td>
                                        <td>
                                            <asp:Literal ID="litAddress2" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            City:</td>
                                        <td>
                                            <asp:Literal ID="litCity" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Postcode:</td>
                                        <td>
                                            <asp:Literal ID="litPostCode" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Payment Date:</td>
                                        <td>
                                            <asp:Literal ID="litDatePaid" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Registration Status:</td>
                                        <td>
                                            <asp:Literal ID="litStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Status Details:</td>
                                        <td>
                                            <asp:Literal ID="litStatusDetails" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Security Key</td>
                                        <td>
                                            <asp:Literal ID="litSecurityKey" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Card Type:</td>
                                        <td>
                                            <asp:Literal ID="litCardType" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            TxAuthNo:</td>
                                        <td>
                                            <asp:Literal ID="litTxAuthNo" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            AVSCV2:</td>
                                        <td>
                                            <asp:Literal ID="litAVSCV2" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address Result:</td>
                                        <td>
                                            <asp:Literal ID="litAddressResult" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Postcode Result:</td>
                                        <td>
                                            <asp:Literal ID="litPostCodeResult" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            CV2 Result:</td>
                                        <td>
                                            <asp:Literal ID="litCV2Result" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3D Secure Status:</td>
                                        <td>
                                            <asp:Literal ID="lit3DSecure" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            CAVV:</td>
                                        <td>
                                            <asp:Literal ID="litCAVV" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Last 4 Digits:</td>
                                        <td>
                                            <asp:Literal ID="litLast4Digits" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Address Status:</td>
                                        <td>
                                            <asp:Literal ID="litAddressStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Payer Status:</td>
                                        <td>
                                            <asp:Literal ID="litPayerStatus" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            Next URL:</td>
                                        <td>
                                            <asp:Literal ID="litNextURL" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        
    </table>
</asp:Content>

