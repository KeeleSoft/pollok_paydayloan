﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Staff_StaffViewSagePayment
    Inherits System.Web.UI.Page

    Dim objSageData As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim TxCode As String = ""

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Visible = False

            TxCode = Request.QueryString("TxCode")
            If TxCode.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No Vendor TxCode found"
            End If
            PopulateDetails()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateDetails()
        Try
            Dim PaymentData As DataTable = objSageData.GetSagePaymentByVendorTxnCode(TxCode)
            If PaymentData IsNot Nothing AndAlso PaymentData.Rows.Count > 0 Then
                With PaymentData.Rows(0)
                    litName.Text = .Item("FullName")
                    litSuccessStatus.Text = .Item("SuccessStatusDesc")
                    litReason.Text = .Item("PaymentReason")
                    litTxCode.Text = .Item("VendorTxCode")
                    litAmount.Text = .Item("Amount")
                    litNotificationStatus.Text = .Item("NotificationStatus")
                    litVPSTxID.Text = .Item("VPSTxId")
                    litMemberID.Text = .Item("MemberID")
                    litFullName.Text = .Item("FullName")
                    litEmail.Text = .Item("Email")
                    litAddress1.Text = .Item("AddressLine1")
                    litAddress2.Text = .Item("AddressLine2")
                    litCity.Text = .Item("City")
                    litPostCode.Text = .Item("PostCode")
                    litStatus.Text = .Item("Status")
                    litStatusDetails.Text = .Item("StatusDetail")
                    litSecurityKey.Text = .Item("SecurityKey")
                    litNextURL.Text = .Item("NextURL")
                    litTxAuthNo.Text = .Item("TxAuthNo")
                    litAVSCV2.Text = .Item("AVSCV2")
                    litAddressResult.Text = .Item("AddressResult")
                    litPostCodeResult.Text = .Item("PostCodeResult")
                    litCV2Result.Text = .Item("CV2Result")
                    lit3DSecure.Text = .Item("SecureStatus3D")
                    litCAVV.Text = .Item("CAVV")
                    litAddressStatus.Text = .Item("AddressStatus")
                    litPayerStatus.Text = .Item("PayerStatus")
                    litCardType.Text = .Item("CardType")
                    litLast4Digits.Text = .Item("Last4Digits")
                    litDatePaid.Text = .Item("DatePaid")
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

End Class
