﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Staff_StaffViewWorldCollections
    Inherits System.Web.UI.Page

    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Const NEXT_AVAILABLE As String = "Next available collections to process"
    Const ALL_SCHEDULED As String = "All scheduled un processed sage collections"
    Const ALL_FAILED As String = "All failed sage collections"
    Const ALL_SUCCESS As String = "All successful sage collections"

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            LoadNextAvailableSchedule() 'Defaut load all next available collections to process
        End If
    End Sub

    Private Sub LoadNextAvailableSchedule()
        Try
            ReportDesc.Text = NEXT_AVAILABLE
            Dim dt As DataTable = objSage.GetNextAvailableSageCollectionsScheduled(False)
            If dt IsNot Nothing Then
                gvSageCollections.DataSource = dt
                gvSageCollections.DataBind()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAllSchedules_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllSchedules.Click
        Try
            ReportDesc.Text = ALL_SCHEDULED
            Dim dt As DataTable = objSage.GetAllSageCollectionsScheduledUnProcessed
            If dt IsNot Nothing Then
                gvSageCollections.DataSource = dt
                gvSageCollections.DataBind()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnAllFailed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllFailed.Click
        Try
            ReportDesc.Text = ALL_FAILED
            Dim FailedDate As DateTime = Date.MinValue
            If txtFailedDate.Text.Length > 0 Then
                FailedDate = txtFailedDate.Text
            End If
            Dim dt As DataTable = objSage.GetAllSageCollectionsFailed(FailedDate)
            If dt IsNot Nothing Then
                gvSageCollections.DataSource = dt
                gvSageCollections.DataBind()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub llSuccess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles llSuccess.Click
        Try
            ReportDesc.Text = ALL_SUCCESS
            Dim dt As DataTable = objSage.GetAllSageCollectionsSuccess
            If dt IsNot Nothing Then
                gvSageCollections.DataSource = dt
                gvSageCollections.DataBind()
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnNextToProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNextToProcess.Click
        LoadNextAvailableSchedule()
    End Sub
End Class
