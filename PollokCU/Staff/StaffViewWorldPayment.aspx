﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Staff/MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffViewWorldPayment.aspx.vb" Inherits="Staff_StaffViewWorldPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width:100%;" class="BodyText">
        <tr>
        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;WorldPay Payment Details - 
            <asp:Literal ID="litName" runat="server"></asp:Literal></span>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
        </tr>
        <tr>
            <td class="BodyText">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlDetails" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table class="BodyText" style="width:100%;">
                                    <tr ID="AuthErrorTableRow" runat="server">
                                        <td>
                                            Payment Status:</td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litSuccessStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Reason:</td>
                                        <td>
                                            <asp:Literal ID="litReason" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Transaction ID:</td>
                                        <td>
                                            <asp:Literal ID="litTxCode" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Amount:</td>
                                        <td>
                                            <asp:Literal ID="litAmount" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Card Type::</td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litNotificationStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Cart ID:</td>
                                        <td>
                                            <asp:Literal ID="litVPSTxID" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Member ID:</td>
                                        <td>
                                            <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Name:</td>
                                        <td>
                                            <asp:Literal ID="litFullName" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Email:</td>
                                        <td>
                                            <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Address Line 1:</td>
                                        <td>
                                            <asp:Literal ID="litAddress1" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Address Line 2:</td>
                                        <td>
                                            <asp:Literal ID="litAddress2" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Address Line 3:</td>
                                        <td>
                                            <asp:Literal ID="litAddress3" runat="server"></asp:Literal>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Town:</td>
                                        <td>
                                            <asp:Literal ID="litCity" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Postcode:</td>
                                        <td>
                                            <asp:Literal ID="litPostCode" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Payment Date:</td>
                                        <td>
                                            <asp:Literal ID="litDatePaid" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Country:</td>
                                        <td>
                                            <asp:Literal ID="litStatus" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Country Match:</td>
                                        <td>
                                            <asp:Literal ID="litStatusDetails" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Route Key</td>
                                        <td>
                                            <asp:Literal ID="litSecurityKey" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Currency:</td>
                                        <td>
                                            <asp:Literal ID="litCardType" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            RawAuthNo:</td>
                                        <td>
                                            <asp:Literal ID="litTxAuthNo" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Trans Status:</td>
                                        <td>
                                            <asp:Literal ID="litAVSCV2" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tel:</td>
                                        <td>
                                            <asp:Literal ID="litAddressResult" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Fax:</td>
                                        <td>
                                            <asp:Literal ID="litPostCodeResult" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Trans Time:</td>
                                        <td>
                                            <asp:Literal ID="litCV2Result" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            IP Address:</td>
                                        <td>
                                            <asp:Literal ID="lit3DSecure" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            Auth Mode:</td>
                                        <td>
                                            <asp:Literal ID="litCAVV" runat="server"></asp:Literal>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px">
                                            </td>
                                        <td style="height: 20px">
                                        </td>
                                        <td style="height: 20px">
                                            </td>
                                        <td style="height: 20px">
                                        </td>
                                        <td style="height: 20px">
                                            </td>
                                        <td style="height: 20px">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        
    </table>
</asp:Content>


