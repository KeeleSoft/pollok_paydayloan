﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Staff_StaffViewWorldPayment
    Inherits System.Web.UI.Page

    Dim objWorldData As PollokCU.DataAccess.Layer.clsWorldPay = New PollokCU.DataAccess.Layer.clsWorldPay
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim TransID As String = ""

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Visible = False

            TransID = Request.QueryString("TID")
            If TransID.Length <= 0 Then
                lblMessage.Visible = True
                lblMessage.Text = "No Transaction ID found"
            End If
            PopulateDetails()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub PopulateDetails()
        Try
            Dim PaymentData As DataTable = objWorldData.GetWorldPaymentByTransID(TransID)
            If PaymentData IsNot Nothing AndAlso PaymentData.Rows.Count > 0 Then
                With PaymentData.Rows(0)
                    litName.Text = .Item("Name")
                    litSuccessStatus.Text = .Item("SuccessStatusDesc")
                    litReason.Text = .Item("ApplicationType")
                    litTxCode.Text = .Item("TransId")
                    litAmount.Text = .Item("Amount")
                    litNotificationStatus.Text = .Item("CardType")
                    litVPSTxID.Text = .Item("CartID")
                    litMemberID.Text = .Item("MemberID")
                    litFullName.Text = .Item("Name")
                    litEmail.Text = .Item("Email")
                    litAddress1.Text = .Item("Address1")
                    litAddress2.Text = .Item("Address2")
                    litAddress3.Text = .Item("Address3")
                    litCity.Text = .Item("Town")
                    litPostCode.Text = .Item("PostCode")

                    litStatus.Text = .Item("CountryString")
                    litStatusDetails.Text = .Item("CountryMatch")
                    litSecurityKey.Text = .Item("RouteKey")
                    'litNextURL.Text = .Item("NextURL")
                    litTxAuthNo.Text = .Item("RawAuthCode")
                    litAVSCV2.Text = .Item("TransStatus")
                    litAddressResult.Text = .Item("Tel")
                    litPostCodeResult.Text = .Item("Fax")
                    litCV2Result.Text = .Item("TransTime")
                    lit3DSecure.Text = .Item("IpAddress")
                    litCAVV.Text = .Item("AuthMode")
                    'litAddressStatus.Text = .Item("AddressStatus")
                    'litPayerStatus.Text = .Item("PayerStatus")
                    litCardType.Text = .Item("Currency")
                    'litLast4Digits.Text = .Item("Last4Digits")
                    litDatePaid.Text = .Item("DatePaid")
                End With
            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

End Class
