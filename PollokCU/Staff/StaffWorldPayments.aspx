﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Staff/MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffWorldPayments.aspx.vb" Inherits="Staff_StaffWorldPayments" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;WorldPay Payments </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsPendingTransactions" runat="server" 
                    SelectMethod="GetWorldPayments" 
                    TypeName="PollokCU.DataAccess.Layer.clsWorldPay">
                </asp:ObjectDataSource>
                <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsPendingTransactions" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="SuccessStatusDesc" HeaderText="Status" 
                            SortExpression="SuccessStatusDesc" >
                            <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TransId" HeaderText="TransId" 
                            SortExpression="TransId" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ApplicationType" HeaderText="Reason" 
                            SortExpression="ApplicationType" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                       <asp:BoundField DataField="Amount" HeaderText="Amount" 
                           SortExpression="Amount">
                        <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MemberID" HeaderText="MemberID" 
                            SortExpression="MemberID" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Name" HeaderText="Name" 
                            SortExpression="Name" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CartID" 
                            HeaderText="CartID" 
                            SortExpression="CartID" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DatePaid" HeaderText="Date" 
                            SortExpression="DatePaid" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server"
                                    ImageUrl="~/images/icon_edit.png"
                                    NavigateUrl='<%# "StaffViewWorldPayment.aspx?TID=" & Eval("TransId") %>'
                                    Target="_SagePayView" Text="Edit Request"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

