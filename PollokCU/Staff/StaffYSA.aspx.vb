﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffYSA
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            lblMessage.Visible = False

            odsYSADeposits.SelectParameters.Item("payRunNo").DefaultValue = ddlSchool.SelectedValue
            odsYSADeposits.SelectParameters.Item("memberID").DefaultValue = txtMemberNo.Text

            odsYSADeposits.SelectParameters.Item("depositDate").DefaultValue = "01/01/1999"

            odsYSADeposits.Select()
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Sub gvBatches_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim hfDuplicated As HiddenField = CType(e.Row.FindControl("hfDuplicated"), HiddenField)
            'Dim hfSuccessfulBacs As HiddenField = CType(e.Row.FindControl("hfSuccessfulBacs"), HiddenField)
            'Dim lblName As Label = CType(e.Row.FindControl("lblName"), Label)

            'If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
            '    If CBool(hfDuplicated.Value) Then
            '        lblName.ForeColor = Drawing.Color.Red
            '    ElseIf CBool(hfSuccessfulBacs.Value) Then
            '        lblName.ForeColor = Drawing.Color.Green
            '    End If
            'End If
        End If
    End Sub

    Protected Sub btnUpdateLogin_Click(sender As Object, e As System.EventArgs) Handles btnUpdateLogin.Click
        Try
            lblMessage.Visible = False
            Dim payRunNo As Integer = ddlSchool.SelectedValue
            If payRunNo > 0 Then
                Response.Redirect("StaffEditYSASchoolLogin.aspx?PayrunNo=" & payRunNo.ToString)
            Else
                lblMessage.Visible = True
                lblMessage.Text = "Please select a school to update login details"
            End If

        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
