﻿<%@ Page Language="VB" MasterPageFile="MasterPageHeaderOnly.master" AutoEventWireup="false" CodeFile="StaffYSAFiles.aspx.vb" Inherits="StaffYSAFiles" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;Young Saver Deposits File Download - Pending Deposit Entries To Process</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td>
                <asp:GridView ID="gvBACSEntries" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsBACSEntries" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="SchoolName" HeaderText="School" 
                            SortExpression="SchoolName" />
                        <asp:BoundField DataField="MemberID" HeaderText="MemberID" 
                            SortExpression="MemberID" >
                        </asp:BoundField>
                        <asp:BoundField DataField="MemberName" HeaderText="Member Name" 
                            SortExpression="MemberName">
                        <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Amount" HeaderText="Amount" 
                            SortExpression="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransactionDate" HeaderText="Date" 
                            SortExpression="TransactionDate" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditCollection" runat="server" 
                                    ImageUrl="~/images/icon_delete.png" 
                                    OnClick="return confirm('Are you sure you want to delete this entry?');" 
                                    NavigateUrl='<%# "StaffYSAFiles.aspx?Action=Delete&ID="& Eval("DepositID") %>' 
                                    Text="Delete Deposit "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No pending YSA deposit entries to process
                    </EmptyDataTemplate>
                </asp:GridView>
                                    
                        </td>
                    </tr>
                    <tr>
                        <td>
                                    
                            <asp:Button ID="btnProcessPending" runat="server" 
                                Text="Process Pending Deposit Entries" />
                                    
                    <asp:ObjectDataSource ID="odsBACSEntries" runat="server" 
                    SelectMethod="SelectYSADepositsForCSV" 
                    TypeName="PollokCU.DataAccess.Layer.clsSchoolsSavings">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="0" Name="payRunNumber" Type="Int32" />
                        </SelectParameters>
                </asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;YSA Deposits File Download - Generated Deposit Files</span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                                    
                    <asp:ObjectDataSource ID="odsNewMemberFileListALPS" runat="server" 
                    SelectMethod="SelectNewMemberFileDownload" 
                    TypeName="PollokCU.DataAccess.Layer.clsStaffAdmin">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="YSA_DEPS" Name="AppType" Type="String" />
                        </SelectParameters>
                </asp:ObjectDataSource>
                <asp:GridView ID="gvFileListLoanFiles" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsNewMemberFileListALPS" PageSize="20" SkinID="NormalGrid" OnRowDataBound="gvFileListLoanFiles_RowDataBound"
                    Width="100%" AllowSorting="True">
                    <Columns>
                        <asp:BoundField DataField="FileDateLong" HeaderText="File Date" 
                            SortExpression="FileDateLong" >
                            <HeaderStyle BackColor="White" HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Deposit Count" SortExpression="MembersCount">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MembersCount") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMembersCount" runat="server" 
                                    Text='<%# Bind("MembersCount") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlDownload" runat="server" 
                                    ImageUrl="~/Images/btn_view.png" 
                                    NavigateUrl='<%# "..\MemberCSVs\" & Eval("FileName") %>' 
                                    Text="Download Deposit File"></asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </td>
          </tr>
          <tr>
            <td height="25" valign="middle">For security reasons, when you have finished using staff services always select LOG OUT.</td>
          </tr>          
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

