﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class StaffYSAFiles
    Inherits System.Web.UI.Page

    Dim objStaff As PollokCU.DataAccess.Layer.clsStaffAdmin = New PollokCU.DataAccess.Layer.clsStaffAdmin
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Dim objSchoolSavings As PollokCU.DataAccess.Layer.clsSchoolsSavings = New PollokCU.DataAccess.Layer.clsSchoolsSavings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("Action") = "Delete" Then
            objSchoolSavings.UpdateSchoolDepositArchive(Request.QueryString("ID"), True, 0)
            Response.Redirect("StaffYSAFiles.aspx")
        End If
        Me.btnProcessPending.Attributes.Add("onclick", "return confirm('Are you sure you want to process all pending deposit entries?');")

    End Sub

    Private Sub GenerateCSV(ByVal FileName As String, ByVal bacsEntries As DataTable)

        Const Line1 As String = "100 02"
        Const Line2 As String = ":32A:<<DATE>>GBP<<AMOUNT>>"
        Const Line3 As String = ":50:London Mutual Credit Union Ltd"
        Const Line4 As String = ":53D:/0890296706740000"
        Const Line5 As String = ":57D:/089407"  'CUCA Sort Code
        Const Line6 As String = "The Cooperative bank"
        Const Line7 As String = "Skelmersdale"
        Const Line8 As String = "Lancs"
        Const Line9 As String = ":59:/089407<<CURRENTACCNUMBER>>"
        Const Line10 As String = "<<MEMBER_NAME>>"
        Const Line11 As String = "<<ADDRESS_LINE1>>"
        Const Line12 As String = "<<CITY>>"
        Const Line13 As String = ":70:<<MEMBERID>>"
        Const Line14 As String = "-"

        Dim datePart As String = Right(Year(Now()), 2) & Month(Now()).ToString.PadLeft(2, "0") & Day(Now()).ToString.PadLeft(2, "0")

        If bacsEntries IsNot Nothing AndAlso bacsEntries.Rows.Count > 0 Then
            Dim AppPath As String = Request.PhysicalApplicationPath
            Dim FilePath As String = AppPath & "BacsFiles\" & FileName
            Dim InputLine As String = ""
            Dim w As IO.StreamWriter
            w = IO.File.CreateText(FilePath)

            For Each dr As DataRow In bacsEntries.Rows
                w.WriteLine(Line1)
                w.WriteLine(Line2.Replace("<<AMOUNT>>", CStr(dr("Amount")).Replace(".", ",")).Replace("<<DATE>>", datePart))
                w.WriteLine(Line3)
                w.WriteLine(Line4)
                w.WriteLine(Line5)
                w.WriteLine(Line6)
                w.WriteLine(Line7)
                w.WriteLine(Line8)
                w.WriteLine(Line9.Replace("<<CURRENTACCNUMBER>>", dr("CurrentAccNumber").ToString.PadLeft(8, "0")))
                w.WriteLine(Line10.Replace("<<MEMBER_NAME>>", dr("MemberName")))
                w.WriteLine(Line11.Replace("<<ADDRESS_LINE1>>", dr("AddressLine1")))
                w.WriteLine(Line12.Replace("<<CITY>>", dr("City")))
                w.WriteLine(Line13.Replace("<<MEMBERID>>", dr("MemberID")))
                w.WriteLine(Line14)

            Next

            w.Flush()
            w.Close()

        End If
    End Sub

    Private Function GenerateCSVDepositFile(ByVal FileName As String, ByVal dtDeposits As DataTable) As Integer

        Dim MembersCount As Integer = 0

        If dtDeposits IsNot Nothing AndAlso dtDeposits.Rows.Count > 0 Then
            MembersCount = dtDeposits.Rows.Count

            Dim AppPath As String = Request.PhysicalApplicationPath
            Dim FilePath As String = AppPath & "MemberCSVs\" & FileName
            Dim InputLine As String = ""
            Dim w As IO.StreamWriter
            w = IO.File.CreateText(FilePath)

            For Each dr As DataRow In dtDeposits.Rows
                InputLine = dr("MemberID") & "," & _
                            dr("FirstName") & "," & _
                            dr("Surname") & "," & _
                            dr("ProductCode") & "," & _
                            dr("Amount")
                w.WriteLine(InputLine)
            Next

            w.Flush()
            w.Close()

        End If

        Return MembersCount
    End Function

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        btnProcessPending.Visible = (gvBACSEntries.Rows.Count > 0)
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Request.QueryString("a") = "logout" Then
            Session.Abandon()
            Response.Redirect("StaffLogin.aspx")
        End If

        If Session("StaffID") Is Nothing Then
            Response.Redirect("StaffLogin.aspx")
        End If
    End Sub

    Sub gvFileList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hlDownload As HyperLink = CType(e.Row.FindControl("hlDownload"), HyperLink)
            Dim lblMembersCount As Label = CType(e.Row.FindControl("lblMembersCount"), Label)
            Dim hlSMS As HyperLink = CType(e.Row.FindControl("hlSMS"), HyperLink)
            Dim hfSMSBatchSent As HiddenField = CType(e.Row.FindControl("hfSMSBatchSent"), HiddenField)

            If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                If lblMembersCount.Text > 0 Then
                    hlDownload.Visible = True
                Else
                    hlDownload.Visible = False
                End If
                If CBool(hfSMSBatchSent.Value) Then
                    hlSMS.Enabled = False
                    hlSMS.ImageUrl = "~/Images/icon-tick.png"
                Else
                    hlSMS.Enabled = True
                    hlSMS.ImageUrl = "~/Images/sms_yes.png"
                End If
            End If
        End If
    End Sub

    Sub gvFileListLoanFiles_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hlDownload As HyperLink = CType(e.Row.FindControl("hlDownload"), HyperLink)
            Dim lblMembersCount As Label = CType(e.Row.FindControl("lblMembersCount"), Label)

            If e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                If lblMembersCount.Text > 0 Then
                    hlDownload.Visible = True
                Else
                    hlDownload.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnProcessPending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessPending.Click
        Try
            lblMessage.Visible = False
            Dim dtDeposits As DataTable = objSchoolSavings.SelectYSADepositsForCSV(0)
            If dtDeposits IsNot Nothing AndAlso dtDeposits.Rows.Count > 0 Then
                Dim fileDate As Date = Now

                Dim fileNameLoan As String = "YSADeposits_" & Replace(fileDate.ToShortDateString, "/", "") & Replace(fileDate.ToShortTimeString.Trim, ":", "") & ".csv"
                Dim MemberCount As Integer = 0
                MemberCount = GenerateCSVDepositFile(fileNameLoan, dtDeposits)  'Generate loan file
                objStaff.InsertNewMemberFileDownload(fileDate, fileNameLoan, MemberCount, Session("StaffID"), "YSA_DEPS")
                objSchoolSavings.UpdateSchoolDepositArchive(0, True, 0) 'Archive all pending deposits

                lblMessage.Visible = True
                lblMessage.Text = "All pending deposit entries are processed"

                odsBACSEntries.Select()
                gvBACSEntries.DataBind()

                odsNewMemberFileListALPS.Select()
                gvFileListLoanFiles.DataBind()  'Reloan loan files

            End If
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessage.Visible = True
            lblMessage.Text = ex.Message
        End Try
    End Sub
End Class
