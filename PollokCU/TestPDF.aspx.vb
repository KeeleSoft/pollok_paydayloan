﻿
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web
Partial Class TestPDF
    Inherits System.Web.UI.Page

    Dim objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            lblMessege.Visible = False

            Dim sPDFName As String
            'Generate the PDF file and save in the reports folder
            sPDFName = GeneratePDF(1, "Test", Now.ToShortDateString.Replace("/", ""))
            Dim sPDFURL As String = objConfig.getConfigKey("SCU.Domain.Name") & "Reports/" & sPDFName

            lblMessege.Visible = True
            lblMessege.Text = sPDFName
        Catch ex As Exception
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub
    Private Function GeneratePDF(ByVal iLoanID As Integer, ByVal sSurName As String, ByVal sDOB As String) As String


        Dim CrystalReportDocument As ReportDocument
        Dim CrystalExportOptions As ExportOptions
        Dim CrystalDiskFileDestinationOptions As DiskFileDestinationOptions

        Dim sReportPath As String = MapPath("") & "\RegistrationApplication2.rpt"

        Dim sSQLServerName As String = objConfig.getConfigKey("SQLServerName")
        Dim sSQLServerUserID As String = objConfig.getConfigKey("SQLServerUserID")
        Dim sSQLServerPassword As String = objConfig.getConfigKey("SQLServerPassword")
        Dim sSQLServerDB As String = objConfig.getConfigKey("SQLServerDB")


        Dim Filename, FilenameShort As String
        CrystalReportDocument = New ReportDocument()
        CrystalReportDocument.Load(sReportPath)
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()

        Dim crTables As Tables = CrystalReportDocument.Database.Tables
        Dim crTable As Table
        For Each crTable In crTables
            crtableLogoninfo = crTable.LogOnInfo
            crConnectionInfo.ServerName = sSQLServerName
            crConnectionInfo.UserID = sSQLServerUserID
            crConnectionInfo.Password = sSQLServerPassword
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            crTable.ApplyLogOnInfo(crtableLogoninfo)
        Next
        CrystalReportDocument.ReportOptions.EnableSaveDataWithReport = False


        'CrystalReportDocument.SetDatabaseLogon(sSQLServerUserID, sSQLServerPassword, sSQLServerName, sSQLServerDB)
        'SQL SP Parameters
        CrystalReportDocument.SetParameterValue("@LoanID", iLoanID)

        'Name of the o/p file
        FilenameShort = iLoanID.ToString & "_" & sSurName & "_" & sDOB & ".pdf"
        Filename = MapPath("") & "\Reports\" & FilenameShort



        CrystalDiskFileDestinationOptions = New DiskFileDestinationOptions()
        CrystalDiskFileDestinationOptions.DiskFileName = Filename
        CrystalExportOptions = CrystalReportDocument.ExportOptions
        With CrystalExportOptions
            .DestinationOptions = CrystalDiskFileDestinationOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With
        CrystalReportDocument.Export()

        Return FilenameShort
        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment; filename=" & FilenameShort)
        'Response.ContentType = "text/pdf"
        'Response.WriteFile(Filename)
        'Response.Flush()
        'Response.End()
    End Function

    
End Class
