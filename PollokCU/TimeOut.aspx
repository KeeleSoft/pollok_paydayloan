﻿<%@ Page Language="VB" MasterPageFile="~/MasterPageHeaderOnly2.master" AutoEventWireup="false" CodeFile="TimeOut.aspx.vb" Inherits="TimeOut" %>

<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc1:MenuPreLogged ID="MenuPreLogged1" runat="server" />
          </td>
        <td width="25">&nbsp;</td>
        <td width="705" valign="top">
            <table style="width:100%;">
                <tr>
                    <td style="font-size: xx-large; color: #CC0000;" height="60" valign="top">
                        Automatic Timeout</td>
                </tr>
                <tr>
                    <td class="BodyText">As you have not entered any details or navigated for some time you have been automatically logged out from the online services. If you were part way through an action, and had not reached the confirmation screen, it is likely that the action you were undertaking will not have been processed.</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="BodyText">
                        <strong>Further Options</strong></td>
                </tr>
                <tr>
                    <td><a href="Login.aspx">Log in to CU Online again</a></td>
                </tr>
                <tr>
                    <td>
                        <a href="http://www.creditunion.co.uk/">Return to Credit Union homepage</a> </td>
                </tr>
            </table>
          </td>
        
      </tr>
    </table>
</asp:Content>

