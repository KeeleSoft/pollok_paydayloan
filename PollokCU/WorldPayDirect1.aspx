﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageHeaderOnly2.master" AutoEventWireup="false" CodeFile="WorldPayDirect1.aspx.vb" Inherits="WorldPayDirect1" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="130" valign="top">
                <uc1:MenuLogged ID="MenuLogged1" runat="server" />
               <uc4:MenuPreLogged ID="MenuPreLogged1" runat="server" />
            </td>
            <td width="23">
                &nbsp;</td>
            <td valign="top">
                <table border="0" cellspacing="0" cellpadding="0" style="width: 488px">
          <!-- TemplateBeginEditable name="Main body" -->          
                    <tr>
                        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5">
                            <span class="style37 style80">&nbsp;Make a payment to Pollok</span></td>
                    </tr>
                    <tr>
                        <td height="25" valign="middle" class="Button style5">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                        </td>
                    </tr>
                    <tr>
                        <td height="10" valign="middle" class="BodyText">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="BodyText" style="width: 100%;">
                                            <tr>
                                                <td width="30%">
                                                    Member Number:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                                        runat="server" ControlToValidate="txtMemberNumber" 
                                                        ErrorMessage="Member number required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtMemberNumber" runat="server" Width="150px" MaxLength="15"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Reason for payment:</td>
                                                <td>
                                                    <asp:DropDownList ID="ddlReason" runat="server" Width="152px">
                                                        <asp:ListItem>Pay Day Loan Repayment</asp:ListItem>
                                                        <asp:ListItem>Other</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Amount to pay (GBP):<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                                        runat="server" ControlToValidate="txtAmount" ErrorMessage="Amount required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtAmount" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Loan ID:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                                        runat="server" ControlToValidate="txtLoanID" 
                                                        ErrorMessage="Loan ID required">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                    <asp:TextBox ID="txtLoanID" runat="server" Width="150px" MaxLength="7"></asp:TextBox>
                                                </td>
                                                <td>
                                                   </td>
                                            </tr>
                                           
                                            
                                           
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" style="height: 23px" />
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <br />
                            <asp:Image ID="Image1" runat="server" Height="30px" ImageUrl="~/Images/VISA.gif" Width="51px" />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/visa_electron.gif" Width="44px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image5" runat="server" Height="29px" ImageUrl="~/Images/amex-logo2.gif" Width="58px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image6" runat="server" Height="30px" ImageUrl="~/Images/JCB.gif" Width="59px" />
                            <br />

                            <br>Pollok Credit Union will never ask you for your memorable data or Password in an e-mail. Never disclose this information to anyone.<br />
                            <h3>Terms and conditions :</h3> 
                             <asp:HyperLink ID="hlTC" runat="server" 
                                                        NavigateUrl="~/PDL/Docs/TC_LCCU.pdf" Target="new">Please click here 
                                                    to view Terms and conditions.</asp:HyperLink> <br /> <br />
                            Refund policy: Refunds are by discrete management final decision. <br />
                            <h3>Contact details :</h3>
                            Pollok Credit Union <br />
                            Main line: 0141 881 8731 <br />
                            Fax: 0141 881 8731 <br />
                            Address: 140 Woodhead road, Glasgow, G53 7NN <br />
                            Email: info@pollokcu.com <br /> 
                            Web: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.pcu.org.uk" Target="new">www.pcu.org.uk</asp:HyperLink> <br />
                            <br />
                            <br /> For security reasons, when you have finished using Internet Banking always select LOG OUT.
                        </td>
                    </tr>
          <!-- TemplateEndEditable -->
                </table>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

