﻿Imports SageIncludes
Partial Class WorldPayDirect1
    Inherits System.Web.UI.Page
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
            txtMemberNumber.Text = Session("MemberID")
            txtMemberNumber.Enabled = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            lblMessege.Visible = False

            'MemberID Validation
            Dim MemberID As Integer = 0
            Integer.TryParse(txtMemberNumber.Text, MemberID)
            If MemberID <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid member number"
                Return
            End If

            'Amount Validation
            Dim Amount As Double = 0
            Double.TryParse(txtAmount.Text, Amount)
            If Amount <= 0 Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid amount to pays"
                Return
            End If

            'LoanID Validation
            Dim iLoanID As String = txtLoanID.Text
            If isValidNameField(iLoanID) Then
                lblMessege.Visible = True
                lblMessege.Text = "Invalid Loan ID"
                Return
            End If

            Dim Reason As String = ddlReason.SelectedValue

            'When all entered are OK, goto confirm page
            Session("MemberIDPayment") = MemberID
            Session("Amount") = Amount
            Session("iLoanID") = iLoanID
            Session("Reason") = Reason
            Response.Redirect("WorldPayDirect2.aspx")
        Catch ex As Exception
            objSystemData.InsertErrorLog(ex, Request.ServerVariables("REMOTE_ADDR"))
            lblMessege.Visible = True
            lblMessege.Text = ex.Message
        End Try
    End Sub
End Class
