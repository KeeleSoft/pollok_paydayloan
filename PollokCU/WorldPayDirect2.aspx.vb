﻿
Partial Class WorldPayDirect2
    Inherits System.Web.UI.Page
    Dim objSystemData As PollokCU.DataAccess.Layer.clsSystemData = New PollokCU.DataAccess.Layer.clsSystemData
    Shared objConfig As PollokCU.DataAccess.Layer.clsConnection = New PollokCU.DataAccess.Layer.clsConnection
    Dim strAmount As String = ""
    Dim strMemberID As String = ""
    Dim strLoanID As String = ""
    Dim strReason As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;")

        If Session("MemberID") Is Nothing Then
            'AccountsControl1.Visible = False
            'QuickLinks1.Visible = True

            MenuLogged1.Visible = False
            MenuPreLogged1.Visible = True
        Else
            MenuLogged1.Visible = True
            MenuPreLogged1.Visible = False
        End If

        PopulatePage()

    End Sub

    Private Sub PopulatePage()
        Try
           
            strAmount = FormatNumber(Session("Amount"), 2, -1, 0, 0) '** Formatted to 2 decimal places with leading digit **
            strMemberID = Session("MemberIDPayment")
            strLoanID = Session("iLoanID")
            strReason = Session("Reason")

            litMemberNumber.Text = strMemberID
            litAmount.Text = strAmount
            litReason.Text = strReason
            litLoanID.Text = strLoanID
            
        Catch ex As Exception
            
            '** Okay, build the crypt field for Form using the information in our session **
            '** First we need to generate a unique VendorTxCode for this transaction **
            '** We're using VendorName, time stamp and a random element.  You can use different mehtods if you wish **
            '** but the VendorTxCode MUST be unique for each transaction you send to Server **
            
        End Try
    End Sub

    

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        Dim cartid As String = "DIR-" & strLoanID.ToString & "-001-" & CStr(Math.Round(Rnd() * 100)) & "XX" & strMemberID.ToString
        RedirectAndPOST(Me.Page, cartid, strAmount)

    End Sub

    Public Shared Sub RedirectAndPOST(page As Page, cartid As String, amount As String)
        Dim data As New NameValueCollection()

        data.Add("instId", "1140028")
        data.Add("cartId", cartid)
        data.Add("amount", amount)
        data.Add("currency", "GBP")

        Dim postUrl As String = String.Empty
        'Dim testMode As Boolean = True 
        'Set this from Web.Config SageTestmode
        Dim strConnectTo As String = objConfig.getConfigKey("SagePayEnv")
        If strConnectTo = "LIVE" Then
            postUrl = "https://secure.worldpay.com/wcc/purchase"
            data.Add("testMode", "0")
        Else
            postUrl = "https://secure-test.worldpay.com/wcc/purchase"
            data.Add("testMode", "100")
        End If

        'Prepare the Posting form
        Dim strForm As String = PreparePOSTForm(postUrl, data)


        'Add a literal control the specified page holding the Post Form, this is to submit the Posting form with the request.
        page.Controls.Add(New LiteralControl(strForm))
    End Sub

    Private Shared Function PreparePOSTForm(url As String, data As NameValueCollection) As [String]
        'Set a name for the form
        Dim formID As String = "PostForm"

        'Build the form using the specified data to be posted.
        Dim strForm As New StringBuilder()
        strForm.Append((Convert.ToString((Convert.ToString((Convert.ToString("<form id=""") & formID) + """ name=""") & formID) + """ action=""") & url) + """ method=""POST"">")
        For Each key As String In data
            strForm.Append((Convert.ToString("<input type=""hidden"" name=""") & key) + """ value=""" + data(key) + """>")
        Next
        strForm.Append("</form>")

        'Build the JavaScript which will do the Posting operation.
        Dim strScript As New StringBuilder()
        strScript.Append("<script language='javascript'>")
        strScript.Append((Convert.ToString((Convert.ToString("var v") & formID) + " = document.") & formID) + ";")
        strScript.Append((Convert.ToString("v") & formID) + ".submit();")
        strScript.Append("</script>")

        'Return the form and the script concatenated. (The order is important, Form then JavaScript)
        Return strForm.ToString() + strScript.ToString()
    End Function

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("WorldPayDirect1.aspx")
    End Sub
End Class
