﻿Imports System.Data

Partial Class WorldPayNotification
    Inherits System.Web.UI.Page

    Public objdata As PollokCU.DataAccess.Layer.clsWorldPay = New PollokCU.DataAccess.Layer.clsWorldPay

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim Processed As Boolean
        Dim transid As Int64 = 0
        Dim crtid As String = ""
        Dim iLoanID As Integer = 0
        Dim ApplicationType As String = ""
        transid = Convert.ToInt64(Request.QueryString("tid"))
        Dim AppData As DataTable = objdata.SelectWorldPayDetail(transid)
        If AppData IsNot Nothing AndAlso AppData.Rows.Count > 0 Then


            With AppData.Rows(0)
                Processed = .Item("TransactionProcessed")
                iLoanID = .Item("ApplicationID")
                crtid = .Item("CartID")

            End With
            If Processed Then
                Response.Redirect("PDL/PDLError.aspx?C=E9")

            Else
                Dim cartParts() As String = crtid.Split("-"c)
                If cartParts.Any() Then
                    ApplicationType = cartParts(0)
                End If

                If String.Equals(ApplicationType, "PDL") Then
                    objdata.UpdatetWorldPayDetail(transid)
                    Response.Redirect("PDL/PDLApplyPDL6.aspx?LoanID=" & iLoanID.ToString)

                ElseIf String.Equals(ApplicationType, "PDLALPS") Then
                    objdata.UpdatetWorldPayDetail(transid)
                    Response.Redirect("AutoLoanPDLWorldPaySuccess.aspx?AutoLoanID=" & iLoanID.ToString)
                Else
                    objdata.UpdatetWorldPayDetail(transid)
                    Response.Redirect("WorldPayDirectSuccessfull.aspx")
                End If
            End If
        End If
    End Sub

End Class
