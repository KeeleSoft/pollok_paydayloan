﻿
Partial Class WorldPayRedirectNotification
    Inherits System.Web.UI.Page

    Public objdata As PollokCU.DataAccess.Layer.clsWorldPay = New PollokCU.DataAccess.Layer.clsWorldPay
    Dim objSage As PollokCU.DataAccess.Layer.clsSagePay = New PollokCU.DataAccess.Layer.clsSagePay

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        getWorldPayData()
    End Sub

    Private Sub getWorldPayData()

        Dim country As String = ""
        Dim authcost As Double = 0
        Dim msgType As String = ""
        Dim routeKey As String = ""
        Dim transId As Int64 = 0
        Dim countryMatch As String = ""
        Dim rawAuthMessage As String = ""
        Dim authCurrency As String = ""
        Dim charenc As String = ""
        Dim compName As String = ""
        Dim rawAuthCode As String = ""
        Dim amountString As String = ""
        Dim installation As Integer = 0
        Dim currency As String = ""
        Dim tel As String = ""
        Dim fax As String = ""
        Dim lang As String = ""
        Dim countryString As String = ""
        Dim email As String = ""
        Dim transStatus As String = ""
        Dim spcharEnc As String = ""
        Dim amount As Double = 0
        Dim address As String = ""
        Dim transTime As String = ""
        Dim cost As Double = 0
        Dim Town As String = ""
        Dim address3 As String = ""
        Dim address2 As String = ""
        Dim address1 As String = ""
        Dim cartId As String = ""
        Dim postcode As String = ""
        Dim ipAddress As String = ""
        Dim cardType As String = ""
        Dim authAmount As Double = 0
        Dim authMode As String = ""
        Dim FuturePayID As Int64 = 0
        Dim instId As Integer = 0
        Dim name As String = ""
        Dim callbackPw As String = ""
        Dim region As String = ""
        Dim avs As Integer = 0
        Dim desc As String = ""
        Dim authAmountString As String = ""
        Dim ApplicationID As Integer = 0
        Dim MemberID As Integer = 0
        Dim ApplicationType As String = ""


        If Not String.IsNullOrEmpty(Request.Form("country")) Then country = Request.Form("country")
        If Not String.IsNullOrEmpty(Request.Form("authcost")) Then authcost = CDbl(Request.Form("authcost"))
        If Not String.IsNullOrEmpty(Request.Form("msgType")) Then msgType = Request.Form("msgType")
        If Not String.IsNullOrEmpty(Request.Form("routeKey")) Then routeKey = Request.Form("routeKey")
        If Not String.IsNullOrEmpty(Request.Form("transId")) Then transId = Convert.ToInt64(Request.Form("transId"))
        If Not String.IsNullOrEmpty(Request.Form("countryMatch")) Then countryMatch = Request.Form("countryMatch")
        If Not String.IsNullOrEmpty(Request.Form("rawAuthMessage")) Then rawAuthMessage = Request.Form("rawAuthMessage")
        If Not String.IsNullOrEmpty(Request.Form("authCurrency")) Then authCurrency = Request.Form("authCurrency")
        If Not String.IsNullOrEmpty(Request.Form("charenc")) Then charenc = Request.Form("charenc")
        If Not String.IsNullOrEmpty(Request.Form("compName")) Then compName = Request.Form("compName")
        If Not String.IsNullOrEmpty(Request.Form("rawAuthCode")) Then rawAuthCode = Request.Form("rawAuthCode")
        If Not String.IsNullOrEmpty(Request.Form("amountString")) Then amountString = Request.Form("amountString")
        If Not String.IsNullOrEmpty(Request.Form("installation")) Then installation = CInt(Request.Form("installation"))
        If Not String.IsNullOrEmpty(Request.Form("currency")) Then currency = Request.Form("currency")
        If Not String.IsNullOrEmpty(Request.Form("tel")) Then tel = Request.Form("tel")
        If Not String.IsNullOrEmpty(Request.Form("fax")) Then fax = Request.Form("fax")
        If Not String.IsNullOrEmpty(Request.Form("lang")) Then lang = Request.Form("lang")
        If Not String.IsNullOrEmpty(Request.Form("countryString")) Then countryString = Request.Form("countryString")
        If Not String.IsNullOrEmpty(Request.Form("email")) Then email = Request.Form("email")
        If Not String.IsNullOrEmpty(Request.Form("transStatus")) Then transStatus = Request.Form("transStatus")
        If Not String.IsNullOrEmpty(Request.Form("spcharEnc")) Then spcharEnc = Request.Form("spcharEnc")
        If Not String.IsNullOrEmpty(Request.Form("amount")) Then amount = CDbl(Request.Form("amount"))
        If Not String.IsNullOrEmpty(Request.Form("address")) Then address = Request.Form("address")
        If Not String.IsNullOrEmpty(Request.Form("transTime")) Then transTime = Request.Form("transTime")
        If Not String.IsNullOrEmpty(Request.Form("cost")) Then cost = CDbl(Request.Form("cost"))
        If Not String.IsNullOrEmpty(Request.Form("Town")) Then Town = Request.Form("Town")
        If Not String.IsNullOrEmpty(Request.Form("address3")) Then address3 = Request.Form("address3")
        If Not String.IsNullOrEmpty(Request.Form("address2")) Then address2 = Request.Form("address2")
        If Not String.IsNullOrEmpty(Request.Form("address1")) Then address1 = Request.Form("address1")
        If Not String.IsNullOrEmpty(Request.Form("cartId")) Then cartId = Request.Form("cartId")
        If Not String.IsNullOrEmpty(Request.Form("postcode")) Then postcode = Request.Form("postcode")
        If Not String.IsNullOrEmpty(Request.Form("ipAddress")) Then ipAddress = Request.Form("ipAddress")
        If Not String.IsNullOrEmpty(Request.Form("cardType")) Then cardType = Request.Form("cardType")
        If Not String.IsNullOrEmpty(Request.Form("authAmount")) Then authAmount = CDbl(Request.Form("authAmount"))
        If Not String.IsNullOrEmpty(Request.Form("authMode")) Then authMode = Request.Form("authMode")
        If Not String.IsNullOrEmpty(Request.Form("futurePayId")) Then FuturePayID = Convert.ToInt64(Request.Form("futurePayId"))
        If Not String.IsNullOrEmpty(Request.Form("instId")) Then instId = CInt(Request.Form("instId"))
        If Not String.IsNullOrEmpty(Request.Form("name")) Then name = Request.Form("name")
        If Not String.IsNullOrEmpty(Request.Form("callbackPw")) Then callbackPw = Request.Form("callbackPw")
        If Not String.IsNullOrEmpty(Request.Form("region")) Then region = Request.Form("region")
        If Not String.IsNullOrEmpty(Request.Form("avs")) Then avs = CInt(Request.Form("avs"))
        If Not String.IsNullOrEmpty(Request.Form("desc")) Then desc = Request.Form("desc")
        If Not String.IsNullOrEmpty(Request.Form("authAmountString")) Then authAmountString = Request.Form("authAmountString")

        Dim cartParts() As String = cartId.Split("-"c)
        If cartParts.Any() Then
            ApplicationType = cartParts(0)
        End If
        If cartParts.Count() > 1 Then
            ApplicationID = cartParts(1)
        End If
        If cartParts.Count() > 3 Then
            MemberID = cartParts(3).Split("XX")(2)
        End If

        objdata.InsertworldPayData(ApplicationID, ApplicationType, MemberID, country, authcost, msgType, routeKey, transId, countryMatch, rawAuthMessage, authCurrency, charenc, compName, rawAuthCode, amountString, installation, currency,
                                   tel, fax, lang, countryString, email, transStatus, spcharEnc, amount, address, transTime, cost, Town, address3, address2, address1, cartId, postcode, ipAddress,
                                   cardType, authAmount, authMode, FuturePayID, instId, name, callbackPw, region, avs, desc, authAmountString)

        If String.Equals(ApplicationType, "PDL") Then
            objSage.InsertSagePayCollection(ApplicationID)
        End If
    End Sub
End Class
