﻿<%@ control language="VB" autoeventwireup="false" inherits="AutoLoanPDLChecks, App_Web_cuiqqipq" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="AutoLoanNewApplication.ascx" tagname="AutoLoanNewApplication3" tagprefix="uc10" %>

<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;ALPS Pay Day Loan Checks </span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
    </td>
</tr>
<tr>
    <td valign="middle">
        <table class="BodyText" style="width:100%;">
            <tr>
                <td>
                    Member ID:</td>
                <td>
                                <strong><asp:Literal ID="litMemberID" runat="server"></asp:Literal></strong>
                            </td>
                <td>
                    Member name:</td>
                <td>
                                <strong><asp:Literal ID="litMemberName" runat="server"></asp:Literal></strong>
                            </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                                &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat ="server" id="ManualVerifyPanel" visible="false">
    <td valign="middle" class="BodyText">
        <strong><asp:Literal ID="litPaymentText" runat="server"></asp:Literal>
        </strong>
    </td>
</tr>
<tr runat ="server" id="paymentOptionsPanel">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <strong>Please selected payment option</strong></td>
                <td>
                   
                    <asp:RadioButton id=Radio1 Text="Faster Payment (£5.00)" Checked="True" GroupName="RadioGroup1" runat="server" AutoPostBack="True" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton id=Radio2 Text="BACS" GroupName="RadioGroup1" runat="server" AutoPostBack="True"/>
               
                </td>
            </tr>
            <tr>
                <td>
                    Please enter the last 4 digits of your bank account number previously used to 
                    recieve money:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                        runat="server" ControlToValidate="txtBankAcc4Digits" 
                        ErrorMessage="Bank account last 4 digits required">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:TextBox ID="txtBankAcc4Digits" runat="server" MaxLength="4" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Next pay day:</td>
                <td>
                    <asp:TextBox ID="txtNextPayDay" runat="server" MaxLength="20" Width="200px"></asp:TextBox><asp:CalendarExtender
                                                      ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                      PopupPosition="Right" TargetControlID="txtNextPayDay" 
                                                      TodaysDateFormat="dd/MM/yyyy">
                                                  </asp:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Monthly repayment:</td>
                <td>
                    <asp:TextBox ID="txtMonthlyPayment" runat="server" MaxLength="20" Width="200px" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                    </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnBack" runat="server" Text="Previous" Width="100px" BackColor="#00a79d" ValidationGroup="grp2" Visible="False"/>
                    &nbsp;&nbsp;<asp:Button ID="btnConfirm" runat="server" Text="Confirm" Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
           
            </table>
    </td>
</tr>
    <tr id="row1" runat="server">
        <td>
            <uc10:autoloannewapplication3 ID="AutoLoanNewApplicationControl" runat="server" />
            </td>
    </tr>
<tr id="rowBankDetails" runat="server">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Sort code:</td>
                <td>
                    <asp:TextBox ID="txtSortCode" runat="server" MaxLength="6" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Account number:</td>
                <td>
                    <asp:TextBox ID="txtAccountNumber" runat="server" MaxLength="8" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Account opened year:</td>
                <td>
                    <asp:DropDownList ID="ddlAccYear" runat="server" Width="203px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Account opened month:</td>
                <td>
                    <asp:DropDownList ID="ddlAccMonth" runat="server" Width="203px">
                        <asp:ListItem Value="1">Jan</asp:ListItem>
                        <asp:ListItem Value="2">Feb</asp:ListItem>
                        <asp:ListItem Value="3">Mar</asp:ListItem>
                        <asp:ListItem Value="4">Apr</asp:ListItem>
                        <asp:ListItem Value="5">May</asp:ListItem>
                        <asp:ListItem Value="6">Jun</asp:ListItem>
                        <asp:ListItem Value="7">Jul</asp:ListItem>
                        <asp:ListItem Value="8">Aug</asp:ListItem>
                        <asp:ListItem Value="9">Sep</asp:ListItem>
                        <asp:ListItem Value="10">Oct</asp:ListItem>
                        <asp:ListItem Value="11">Nov</asp:ListItem>
                        <asp:ListItem Value="12">Dec</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td>
                    &nbsp;
                    </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnConfirmBankDetails" runat="server" Text="Confirm" 
                        Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
            </table>
    </td>
</tr>
<tr id="rowCardDetails" runat="server">
    <td valign="middle" class="BodyText" id="rowBankCardDetails">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Please enter the last 4 digits of 
                    your debit card number previously used to 
                    pay:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                        runat="server" ControlToValidate="txtBankCard4Digits" 
                        ErrorMessage="Bank card last 4 digits required">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:TextBox ID="txtBankCard4Digits" runat="server" MaxLength="4" Width="200px"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>
                    &nbsp;
                    </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnConfirmCardDigits" runat="server" Text="Confirm" 
                        Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
            </table>
    </td>
</tr>
<tr id="rowSMSCode"  runat="server">
    <td valign="middle" class="BodyText" id="Td1">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Please enter the SMS security code you just recieved:</td>
                <td>
                    <asp:TextBox ID="txtSMSCode" runat="server" MaxLength="6" Width="200px"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>
                    &nbsp;
                     </td>
                <td>
                     &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnSMSCodeConfirm" runat="server" Text="Confirm" 
                        Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
            </table>
    </td>
</tr>
<tr id="rowSage"  runat="server">
    <td valign="middle" class="BodyText" id="Td2">
        <table style="width:100%;">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <strong>We will pass your application to WorldPay to make a 1 pence payment to 
                    verify your card details. Please note we will not store your card details and 
                    WorldPay will process this transaction.</strong>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img alt="" src="../Images/Worldpay.png" /></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td width="50%">
                                &nbsp;</td>
                            <td align="left">
                                <asp:Button ID="btnSageProceed" runat="server" BackColor="#00a79d" 
                                    Font-Bold="True" Text="Proceed" Width="100px" />
                            </td>
                        </tr>
                    </table>
                    <br />
                            <asp:Image ID="Image1" runat="server" Height="30px" ImageUrl="~/Images/VISA.gif" Width="51px" />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/visa_electron.gif" Width="44px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image5" runat="server" Height="29px" ImageUrl="~/Images/amex-logo2.gif" Width="58px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image6" runat="server" Height="30px" ImageUrl="~/Images/JCB.gif" Width="59px" />
                            <br />
                               <br />
                               <h3>Terms and conditions :        <h3>Terms and conditions :   <h3>Terms and conditions :</h3> 
                             <asp:HyperLink ID="hlTC" runat="server" 
                                                        NavigateUrl="~/PDL/Docs/TC_LCCU.pdf" Target="new">Please click here 
                                                    to view Terms and conditions.</asp:HyperLink> <br /> <br />
                            Refund policy: Refunds are by discrete management final decision. <br /> <br />
                            <h3>Contact details :</h3>
                            Pollok Credit Union <br />
                            Main line: 0141 881 8731 <br />
                            Fax: 0141 881 8731 <br />
                            Address: 140 Woodhead road, Glasgow, G53 7NN <br />
                            Email: info@pollokcu.com <br /> 
                            Web: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.pcu.org.uk" Target="new">www.pcu.org.uk</asp:HyperLink> <br />
                            <br /> 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hfLoanID" runat="server" />
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
            
