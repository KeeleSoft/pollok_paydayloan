﻿<%@ control language="VB" autoeventwireup="false" inherits="AutoLoanAutoLoanPaymentDetails, App_Web_uyevbyxq" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<table width="100%" cellspacing="0" cellpadding="0">
<tr><td height="25" valign="middle" bgcolor="#00a79d" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Payment Account Details </span></td></tr>
<tr>
    <td valign="middle" class="BodyText">
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
    </td>
</tr>
<tr>
    <td valign="middle">
        <table class="BodyText" style="width:100%;">
            <tr>
                <td>
                    Member ID:</td>
                <td>
                                <strong><asp:Literal ID="litMemberID" runat="server"></asp:Literal></strong>
                            </td>
                <td>
                    Member name:</td>
                <td>
                                <strong><asp:Literal ID="litMemberName" runat="server"></asp:Literal></strong>
                            </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                                &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        &nbsp;<strong><asp:Literal ID="litPaymentText" runat="server"></asp:Literal>
        </strong></td>
</tr>
<tr runat ="server" id="SMSCodePanel" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    Please enter the SMS security code recieved:</td>
                <td>
                    <asp:TextBox ID="txtSecurityCode" runat="server" MaxLength="6" Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr runat ="server" id="ManualVerifyPanel" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    Or verify manually:</td>
                <td>
                    <asp:CheckBox ID="chkManualVerify" runat="server" />
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr runat ="server" id="paymentOptionsPanel">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <strong>Please selected payment option</strong></td>
                <td>
                    <asp:RadioButtonList ID="radPaymentOption" runat="server">
                        <asp:ListItem Selected="True" Value="FP">Faster Payment (£5.00)</asp:ListItem>
                        <asp:ListItem>BACS</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    Sort code:</td>
                <td>
                    <asp:TextBox ID="txtAccNumber" runat="server" MaxLength="6" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Account number:</td>
                <td>
                    <asp:TextBox ID="txtSortCode" runat="server" MaxLength="8" Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Next pay day:</td>
                <td>
                    <asp:TextBox ID="txtNextPayDay" runat="server" MaxLength="20" Width="200px"></asp:TextBox><asp:CalendarExtender
                                                      ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                      PopupPosition="Right" TargetControlID="txtNextPayDay" 
                                                      TodaysDateFormat="dd/MM/yyyy">
                                                  </asp:CalendarExtender>
                </td>
            </tr>
            </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td width="50%">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="btnConfirm" runat="server" Text="Confirm" Width="100px" BackColor="#00a79d"/>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td valign="middle" class="BodyText">&nbsp;</td>
</tr>
</table>
            
