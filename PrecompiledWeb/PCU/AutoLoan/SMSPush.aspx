﻿<%@ page language="VB" autoeventwireup="false" inherits="AutoLoan_SMSPush, App_Web_uyevbyxq" stylesheettheme="Grey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table style="width:100%;">
            <tr>
                <td>
                    Testing Harness</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server" 
                        style="color: #FF3300; font-weight: 700"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td>
                                Mobile number:</td>
                            <td>
                                <asp:TextBox ID="txtMobile" runat="server" Width="150px">447786202903</asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SMS Message:</td>
                            <td>
                                <asp:TextBox ID="txtMessage" runat="server" Width="300px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
