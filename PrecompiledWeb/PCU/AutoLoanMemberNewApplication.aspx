﻿<%@ page language="VB" masterpagefile="~/PDLMasterPage2.master" autoeventwireup="false" inherits="AutoLoanMemberNewApplication, App_Web_qcnk5q40" stylesheettheme="Grey" %>

<%@ Register src="QuickLinksLoggedIn.ascx" tagname="QuickLinksLoggedIn" tagprefix="uc3" %>

<%@ Register src="AutoLoan/AutoLoanNewApplication.ascx" tagname="AutoLoanNewApplication3" tagprefix="uc10" %>
<%@ Register src="AutoLoan/AutoLoanAppeal.ascx" tagname="AutoLoanAppeal2" tagprefix="uc2" %>
<%@ Register src="AutoLoan/AutoLoanMessages.ascx" tagname="AutoLoanMessages2" tagprefix="uc6" %>
<%@ Register src="AutoLoan/AutoLoanPaymentDetails.ascx" tagname="AutoLoanPaymentDetails2" tagprefix="uc7" %>
<%@ Register src="AutoLoan/AutoLoanOptions.ascx" tagname="AutoLoanOptions2" tagprefix="uc8" %>
<%@ Register src="AutoLoan/AutoLoanPDLChecks.ascx" tagname="AutoLoanPDLChecks" tagprefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="130" valign="top"> &nbsp;               
            </td>
            <td width="23">
                                &nbsp;</td>
            <td valign="top">
            <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <uc10:autoloannewapplication3 ID="AutoLoanNewApplicationControl" runat="server" />
                <uc2:autoloanappeal2 ID="AutoLoanAppealControl" runat="server" Visible="False" />
                <uc6:autoloanmessages2 ID="AutoLoanMessagesControl" runat="server" 
                    Visible="False" />
                <uc7:autoloanpaymentdetails2 ID="AutoLoanPaymentDetailsControl" runat="server" 
                    Visible="False" />
                <uc8:autoloanoptions2 ID="AutoLoanOptionsControl" runat="server" 
                    Visible="False" />      
                
                <br />
                <uc5:AutoLoanPDLChecks ID="AutoLoanPDLChecksControl" runat="server" 
                    Visible="False" />
                
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

