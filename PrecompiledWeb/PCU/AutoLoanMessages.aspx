﻿<%@ page language="VB" masterpagefile="~/PDLMasterPage2.master" autoeventwireup="false" inherits="AutoLoanMessages, App_Web_qcnk5q40" stylesheettheme="Grey" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="QuickLinksLoggedIn.ascx" tagname="QuickLinksLoggedIn" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            
            <td width="23">
                                &nbsp;</td>
                                            <td valign="top">
                                                
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          <tr>
            <td height="25" valign="middle" bgcolor="#00a79d" class="Button style5">
                <span class="style37 style80">&nbsp;Auto Loan Processing System (ALPS)</span></td>
          </tr>
          <tr>
            <td height="10" valign="middle" class="BodyText">
                                          &nbsp;</td>
          </tr>
          <tr>
            <td height="10" valign="middle" class="BodyText">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                        <asp:Panel ID="pnlMsg1" runat="server" Width="814px">
                                Thank you for applying for the  Pollok Credit Union’s <asp:Literal ID="litLoanName1" runat="server"></asp:Literal> loan. <br /><br />
                                Congratulations, your loan has been processed successfully. If the Faster Payment facility was selected in your application, we will action the transfer of your loan amount into your bank account within one working day. If you have selected the BACS Payment facility, we need to process your request manually. Please note that it will take 3 working days for the money to reach into your account, after the PCU has process your request.<br /><br />
                                Please note thay you have already signed our revolving loan agreement  with us.  If you have any queries please contact us via email writing to: <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com</a><br /><br />
                                Thank you for being a loyal member of the Pollok Credit Union & selecting us for your financial services.
                               
                               <br /><br />
                         </asp:Panel>
                        <asp:Panel ID="pnlMsg2" runat="server">
                                Thank you for applying for the Pollok Credit Union’s loan. <br /><br />
                                Unfortunately, we are unable to process your application at this time. This may be due to one or more verification failures from the information you have provided to us. Your application will be investigated further by our loan department team and we will get in touch with you by phone or email within 3 working days. We may request further information from you such as recent payslips and bank statements to assist us in your application.<br /><br />
                                If you have any queries please contact us via email to <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com</a><br /><br />
                                We thank you for applying for our PCU loan, an ethical and truly low cost payday loan.
                        </asp:Panel>
                        <asp:Panel ID="pnlMsg3" runat="server">
                                Thank you for applying for the Pollok Credit Union’s loan. <br /><br />
                                Unfortunately, we regret to inform you that we are unable to process your application at this time. Using the information that you have provided to us on your application, it was found that you do not qualify for our weeglasgow loan. This because the services and products offered by the Pollok Credit Union are exclusively for people who live or work within the Glasgow.<br /><br />
                                For more information on Credit Unions and to find your local Credit Union, please visit the website for Association of British Credit Unions (ABCUL) on: <a href="http://www.abcul.org/home"> http://www.abcul.org/home </a><br /><br />
                                We thank you for applying for our PCU loan, an ethical and truly low cost payday loan.
                        </asp:Panel>
                        <asp:Panel ID="pnlM1" runat="server">
                                Thank you for applying for Pollok Credit Union payday loan. 
                                Congratulations, you have succeeded your application. If you have requested a 
                                Faster Payment facility we will credit your account on our next payment cycle. 
                                We will also email all the legal papers. If you have any queries please email us 
                                on <a href="mailto:theweeloan@pollokcu.com">theweeloan@pollokcu.com</a>
                            </asp:Panel>     
                            <asp:Panel ID="pnlM2" runat="server">
                                Thank you for your PCU loan application. However your security code did not match within the 3 attempts given. Please contact us.
                            </asp:Panel>
                            <asp:Panel ID="pnlM3" runat="server">
                                Thank you for setting up WorldPay payments.
                            </asp:Panel>                         
                             <asp:Panel ID="pnlM4" runat="server">
                                 Unfortunately we are unable to process your request at this time as you have 
                                 failed one or more of our verification checks, to reapply please click the link 
                                 to <a href="PDL/Default.aspx?CU=PCU">Apply for PCU loan</a>.
                            </asp:Panel>  
                            <asp:Panel ID="pnlUnknown" runat="server">
                                Unknown message.
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td height="10" valign="middle" class="BodyText">
                &nbsp;</td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                                
            </td>
           
        </tr>
    </table>
</asp:Content>

