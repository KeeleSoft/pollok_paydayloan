﻿<%@ page title="" language="VB" masterpagefile="~/PDLMasterPage2.master" autoeventwireup="false" inherits="AutoLoanPDLWorldPaySuccess, App_Web_hw0trknz" stylesheettheme="Grey" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="QuickLinksLoggedIn.ascx" tagname="QuickLinksLoggedIn" tagprefix="uc3" %>

<%@ Register src="AutoLoan/AutoLoanNewApplication.ascx" tagname="AutoLoanNewApplication3" tagprefix="uc10" %>
<%@ Register src="AutoLoan/AutoLoanAppeal.ascx" tagname="AutoLoanAppeal2" tagprefix="uc2" %>
<%@ Register src="AutoLoan/AutoLoanMessages.ascx" tagname="AutoLoanMessages2" tagprefix="uc6" %>
<%@ Register src="AutoLoan/AutoLoanPaymentDetails.ascx" tagname="AutoLoanPaymentDetails2" tagprefix="uc7" %>
<%@ Register src="AutoLoan/AutoLoanOptions.ascx" tagname="AutoLoanOptions2" tagprefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            
            <td width="23">
                                &nbsp;</td>
            <td valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                            <td><strong>Please enter the security code sent to your mobile.</strong>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                          <table style="width:100%;" __designer:mapid="276">
                                              <tr __designer:mapid="277">
                                                  <td width="40%" __designer:mapid="278">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="279">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr __designer:mapid="27a">                                                  
                                                  <td __designer:mapid="27d">
  &nbsp;Security Code:<asp:CompareValidator ID="CompareValidator4" runat="server" 
                                                                      ControlToValidate="txtSecurityCode" 
                                                                      ErrorMessage="Invalid security code" Operator="GreaterThan" 
                                                                      ValueToCompare="100000">*</asp:CompareValidator>
                                                              </td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtSecurityCode" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;Confirm Security Code:<asp:CompareValidator ID="CompareValidator5" runat="server" 
                                                                      ControlToValidate="txtSecurityCodeConfirm" 
                                                                      ErrorMessage="Invalid confirmed security code" Operator="GreaterThan" 
                                                                      ValueToCompare="100000">*</asp:CompareValidator>
                                                              </td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtSecurityCodeConfirm" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="27d">
                                                      &nbsp;</td>
                                              </tr>
                                              </table>
                              </td>
                          </tr>                          
                          <tr><td>
                                          <asp:LinkButton ID="btnReSend" runat="server">Did not recieve the code? Click 
                                          here to re-send.</asp:LinkButton>
                              </td></tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  &nbsp;</td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Next" Width="70px" BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                                  <asp:HiddenField ID="hfAttempt" runat="server" Value="0" />
                              </td>
                          </tr>
                      </table>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

