﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_Default, App_Web_oc53n32s" stylesheettheme="Grey" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


<%-- Add content controls here --%>
<asp:Content ID="Content1" runat="server" contentplaceholderid="ContentMainBody">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div id="content">
			<div id="contentheader">
				<h2>Apply for a Wee Glasgow Loan </h2> 
				<div class="columnleft">
					<h3>Cash when you need it quick</h3> 
					<p class="extra">
						Wee Glasgow Loans are short term loan facility from Pollok Credit Union designed to make borrowing more affordable.
					</p>
					<p>
						By being a member of the credit union and using our Wee Glasgow Loan service you will be able to make considerable savings on interest payments compared to using other payday lenders and high cost lenders. Wee Glasgow Loans also gives you the opportunity to pay back your loan over 1 to 12 months. It’s a flexible product that gives you control over how much you borrow and how much you want to pay back each month. 
					</p>
				</div>
				<div class="columnright">
					<h4>Typical example</h4> 
					<div class="data grey">
						<table>
							<tr>
								<th colspan="2">
									<asp:Literal ID="lit7" runat="server" Text="Loan of £400 over 1 month"></asp:Literal> 
								</th>
							</tr>
							<tr>
								<td>
									Total interest 
								</td>
								<td>
									<asp:Literal ID="lit8" runat="server" Text="£8.00"></asp:Literal> 
								</td>
							</tr>
							<tr>
								<th>
									Total to pay 
								</th>
								<th>
									<asp:Literal ID="lit9" runat="server" Text="£408.00"></asp:Literal>
								</th>
							</tr>
							<tr>
								<td>
									Month 1 repayment 
								</td>
								<td>
									<asp:Literal ID="lit10" runat="server" Text="£408.00"></asp:Literal> 
								</td>
							</tr>
							
							
						</table>
					</div>
				</div>
				<hr />
			</div>
			<div id="contentmain">
				<div class="columnleft">
					<div id="loanform">	
															
							                    
					    <table style="width: 100%">
                            <tr>
                                <td><asp:HiddenField ID="hfBorrow" runat="server" />
                                <asp:HiddenField ID="hfPeriod" runat="server" />
                                <asp:HiddenField ID="hfAfford" runat="server" />
                                <asp:HiddenField ID="hfTotalInterest" runat="server" />
                                <asp:HiddenField ID="hfTotalToPay" runat="server" />
                                <asp:HiddenField ID="hfMonth1Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth2Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth3Pay" runat="server" />	
                                <asp:HiddenField ID="hfMonth4Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth5Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth6Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth7Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth8Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth9Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth10Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth11Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth12Pay" runat="server" />
                                <table class="sliderquestion" style="width:100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="20px" width="80%"><h4>How much cash would you like to borrow?</h4></td>
                                <td height="20px" width="20%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtHowMuch" runat="server" AutoPostBack="True"></asp:TextBox>
                                     <asp:SliderExtender ID="txtHowMuch_SliderExtender" runat="server" 
        BoundControlID="txtHowMuchBound" TargetControlID="txtHowMuch" Minimum="100" Maximum="400" 
                             RailCssClass="slider_rail" HandleCssClass="slider_handle" 
                                        HandleImageUrl="Images/scroll.png" Steps="0"/>                                           
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHowMuchBound" runat="server" AutoPostBack="True" CssClass="style-1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">&nbsp;</td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><h4>How many months would you like the loan for?</h4></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtHowLong" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:SliderExtender ID="txtHowLong_SliderExtender" runat="server" 
                                        BoundControlID="txtHowLongBound" 
                                        TargetControlID="txtHowLong" 
                                        Minimum="1" Maximum="12" RailCssClass="slider_rail" HandleCssClass="slider_handle" HandleImageUrl="Images/scroll.png"/>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtHowLongBound" runat="server" AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">&nbsp;</td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr style="visibility: hidden">
                                <td height="20px"><h4>How much would you like to payback each month?</h4></td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr style="visibility: hidden">
                                <td>
                                    <asp:TextBox ID="txtAfford" runat="server" AutoPostBack="True"></asp:TextBox>
                                    <asp:SliderExtender ID="txtAfford_SliderExtender" runat="server" 
                                        BoundControlID="txtAffordBound" HandleCssClass="slider_handle" Maximum="400" HandleImageUrl="Images/scroller.gif"
                                        Minimum="100" RailCssClass="slider_rail" TargetControlID="txtAfford" />
                                </td>
                                <td>
                                                                        <asp:TextBox ID="txtAffordBound" runat="server" AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>    
                            <tr>
                                <td></td>
                                <td>&nbsp;</td>
                            </tr>                       
                            <tr>
                                <td><h4>Do you want same day payment?</h4></td>
                                <td>&nbsp;</td>
                            </tr>
                            </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="formtable">
								<tr>
									<th>
										Same day payment facility 
									</th>
									<td>
                    <asp:RadioButton ID="radInstantDecisionYes" runat="server" AutoPostBack="True" 
                        Text="Yes" ValidationGroup="vgInstantDecision" Checked="True" GroupName="InstantDecision" />
                                            &nbsp;</td>
									<td>
                    <asp:RadioButton ID="radInstantDecisionNo" runat="server" AutoPostBack="True" 
                        Text="No" ValidationGroup="vgInstantDecision" GroupName="InstantDecision" />
                                            &nbsp;</td>
									<td>&nbsp;<strong><asp:Literal ID="litInstantDecision" runat="server"></asp:Literal></strong></td>
									<td>
										&nbsp;<span class="hotspot" onmouseover="tooltip.show('If you dont want a same day payment, we could pay it free using BACS which will take up to 3 working days to reach money in your current account.');" onmouseout="tooltip.hide();"><img 
                                            alt="" src="Images/GreenInfoButton.png" height="30px" width="30px" /></span></td>
								</tr>
								
							</table></td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label></td>
                            </tr>
                        </table>
															
								                    
					</div>
				</div>
				<div class="columnright">
					<h4>Loan summary</h4>
					<div class="data green">
						<table>
							<tr>
								<th>
									<asp:Literal ID="lit1" runat="server"></asp:Literal> 
								</th>
								<th>
									&nbsp; 
								</th>
								<th>
									<asp:Literal ID="litSummaryTotal" runat="server"></asp:Literal> 
								</th>
							</tr>
							<tr>
								<td>
									<asp:Literal ID="litTotalInterestDesc" runat="server"></asp:Literal> 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="litSummaryInterest" runat="server"></asp:Literal> 
								</td>
							</tr>
							<tr>
								<td>
									Same day payment facility: 
								</td>
								<td><asp:HiddenField ID="hfTotalExpenses" runat="server" />									
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="litTotalCost" runat="server"></asp:Literal> 
									<!--span class="hotspot" onmouseover="tooltip.show('These are the optional services that you’ve elected on the left-hand side of the page');" onmouseout="tooltip.hide();"><img 
                                            alt="" src="Images/GreenInfoButton.jpg" height="20px" width="20px" /></span-->
								</td>
							</tr>																					
							<tr>
								<th>
									Total to pay 
								</th>
								<th>
									&nbsp; 
								</th>
								<th>
									<asp:Literal ID="lit3" runat="server"></asp:Literal>
								</th>
							</tr>
							<tr>
								<td>
									Month 1 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit4" runat="server"></asp:Literal> 
								</td>
							</tr>
							<tr id="row2Month" runat="server">
								<td>
									Month 2 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit5" runat="server"></asp:Literal>
								</td>
							</tr>
							<tr id="row3Month" runat="server">
								<td>
									Month 3 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit6" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row4Month" runat="server">
								<td>
									Month 4 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit20" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row5Month" runat="server">
								<td>
									Month 5 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit21" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row6Month" runat="server">
								<td>
									Month 6 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit22" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row7Month" runat="server">
								<td>
									Month 7 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit23" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row8Month" runat="server">
								<td>
									Month 8 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit24" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row9Month" runat="server">
								<td>
									Month 9 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit25" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row10Month" runat="server">
								<td>
									Month 10 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit26" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row11Month" runat="server">
								<td>
									Month 11 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit27" runat="server"></asp:Literal>
								</td>
							</tr>
                            <tr id="row12Month" runat="server">
								<td>
									Month 12 repayment 
								</td>
								<td>
									&nbsp; 
								</td>
								<td>
									<asp:Literal ID="lit28" runat="server"></asp:Literal>
								</td>
							</tr>
						</table>
					</div>
					<div class="data yellow">
						<table>
						    <tr>
								<td>
									Total cost of our loan
								</td>
								<td>
									<asp:Literal ID="litCUOKCost" runat="server"></asp:Literal>
								</td>
							</tr>
							<tr>
								<td>
									Same loan from a typical payday lender 
								</td>
								<td>
									<asp:Literal ID="litOtherLenderCost" runat="server"></asp:Literal>
								</td>
							</tr>
							<tr>
								<th>
									Our loan will save you 
								</th>
								<th>
									<asp:Literal ID="litSave" runat="server"></asp:Literal>
								</th>
							</tr>
							
						</table>
					</div>
					<div>
					<asp:Button id="btnShowPopup" runat="server" style="display:none" />
                        <asp:ImageButton ID="ImageButton1" runat="server" 
                                    ImageUrl="~/PDL/Images/btnApplyLoan.png" />
                                    <asp:ModalPopupExtender ID="mdlPopup" runat="server" 
                CancelControlID="Button1" PopupControlID="pnlModelPopUp" 
                TargetControlID="btnShowPopup" BackgroundCssClass="modalBackground" 
                OkControlID="btnOK" onokscript="onOk()" DropShadow="false">
            </asp:ModalPopupExtender>   
					</div>
				</div>
			</div>
		</div>
<asp:Panel ID="pnlModelPopUp" runat="server" Width="250px" BorderColor="#33CC33" 
            BorderStyle="Solid" BorderWidth="1px" Height="300px" BackColor="White">
          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                  <table style="width:100%;">
                      <tr>
                          <td align="right">
                              <asp:ImageButton ID="Button1" runat="server" ImageUrl="images/icon_delete.png" 
                                  Text="Close" />
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <h4>&nbsp;Security check</h4></td>
                      </tr>
                      <tr>
                          <td><p>&nbsp;Enter the word below.</p></td>
                      </tr>
                      <tr>
                          <td><p>
                              &nbsp;Cant&#39;t read this?
                              <asp:LinkButton ID="lbtnTryAnother" runat="server">try another</asp:LinkButton></p>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td align="center">
                              <asp:Image ID="imgCaptcha" runat="server" />
                          </td>
                      </tr>                      
                      <tr>
                          <td valign="middle"><p>
                              &nbsp;Enter the above text in the box and click the button</p>&nbsp;&nbsp;<asp:TextBox ID="txtEnteredWord" runat="server"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td align="center">
                              <asp:ImageButton ID="btnApplyWithSecurity" runat="server" 
                                  ImageUrl="~/PDL/Images/btnApplyLoan.png" Text="OK" />
                              <asp:Button ID="btnOK" runat="server" style="display:none" Text="Button" />
                          </td>
                      </tr>
                  </table>
              </ContentTemplate>
          </asp:UpdatePanel>
        </asp:Panel>
 </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

