﻿<%@ page language="VB" autoeventwireup="false" inherits="PDL_Default2, App_Web_imf01pu1" stylesheettheme="Grey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table bgcolor="#F3F3F3" border="1" bordercolor="#CCCCCC" cellpadding="5" cellspacing="0" width="320" webkit-border-radius:10px>
        <strong>
        <asp:Literal ID="lit1" runat="server"></asp:Literal>
        </strong>
        <tr>
            <td bgcolor="#F3F3F3" width="65">
                &nbsp;</td>
            <td bgcolor="#F3F3F3" >
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#F3F3F3">
                Total interest</td>
            <td bgcolor="#F3F3F3">
                <div align="right">
                    <asp:Literal ID="lit2" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td bgcolor="#F3F3F3">
                <strong>Total to pay</strong></td>
            <td bgcolor="#F3F3F3">
                <div align="right">
                    <strong>
                    <asp:Literal ID="lit3" runat="server"></asp:Literal>
                    </strong>
                </div>
            </td>
        </tr>
        <tr>
            <td bgcolor="#F3F3F3">
                Month 1 repayment</td>
            <td bgcolor="#F3F3F3">
                <div align="right">
                    <asp:Literal ID="lit4" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td bgcolor="#F3F3F3">
                Month 2 repayment</td>
            <td bgcolor="#F3F3F3">
                <div align="right">
                    <asp:Literal ID="lit5" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
        <tr>
            <td bgcolor="#F3F3F3">
                Month 3 repayment</td>
            <td bgcolor="#F3F3F3">
                <div align="right">
                    <asp:Literal ID="lit6" runat="server"></asp:Literal>
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
