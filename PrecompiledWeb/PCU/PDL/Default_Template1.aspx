﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage.master" autoeventwireup="false" inherits="PDL_Default_Template1, App_Web_oc53n32s" title="Untitled Page" stylesheettheme="Grey" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .slider_rail
        {
            position: relative;
            height: 20px;
            width: 320px;
            background: #FFFFFF;
            border: 1px solid gray #67AE3E;
        }
        .slider_handle
        {
            position: absolute;
            height: 20px;
            width: 63px;
        }
        .modalBackground {
	        background-color:Gray;
	        filter:alpha(opacity=70);
	        opacity:0.7;
        }
    </style> 
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder1">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <table style="width:100%;">

<tr>
    <td>
        <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="443" valign="top"><strong><span class="style58">Cash when you need it quick</span><br />
                                        CUOK is a short term loan facility from Leeds City Credit Union designed to see you through until your next payday. It is exclusively for people who live or work in Southwark or Lambeth.</strong><br />
                                        <br />
                                        By being a member of the credit union and using our CUOK loan service you will be able to make considerable savings on interest payments compared to using other payday loan lenders. CUOK also gives you the opportunity to pay back your loan over 3 months. It&rsquo;s a flexible product that gives you control over how much you borrow and how much you want to pay back each month.<br />
                        <br />
            </td>
            <td width="40">&nbsp;</td>
            <td valign="top">
                <table style="width:100%;">
                    <tr>
                        <td><strong><span class="style58">Typical example</span></strong></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border-style: solid; width:100%;" border-color="#CCCCCC" bgcolor="#EBEBEB" 
                                cellpadding="0" cellspacing="0" align="center" border="1">
                                <tr>
                                    <td align="center">
                                        <table style="width:94%;" cellpadding="0" cellspacing="0">
                                            <tr style="height: 20px">
                                                <td align="left" valign="bottom">
                                                    <strong><asp:Literal ID="lit7" runat="server" 
                              Text="Loan of £400 over 3 months"></asp:Literal></strong></td>
                                            </tr>
                                            <tr style="height: 2px">
                                                <td><div align="center"><hr /></div></td>
                                            </tr>
                                            <tr style="height: 5px">
                                                <td valign="top">
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="70%" align="left">
                                                                Total interest</td>
                                                            <td width="30%" align="right">
                                                                <asp:Literal ID="lit8" runat="server" Text="£16.10"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><hr /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="70%" align="left"><strong>Total to pay</strong></td>
                                                            <td width="30%" align="right">
                                                                <strong>
                                                                <asp:Literal ID="lit9" runat="server" Text="£416.10"></asp:Literal>
                                                                </strong></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><hr /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="70%" align="left">
                                                                Month 1 repayment</td>
                                                            <td width="30%" align="right">
                                                                <asp:Literal ID="lit10" runat="server" Text="£138.70"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><hr /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="70%" align="left">
                                                                Month 2 repayment</td>
                                                            <td width="30%" align="right">
                                                                <asp:Literal ID="lit11" runat="server" Text="£138.70"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><hr /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                        <tr style="height: 20px" valign="top">
                                                            <td width="70%" align="left">
                                                                Month 3 repayment</td>
                                                            <td width="30%" align="right">
                                                                <asp:Literal ID="lit12" runat="server" Text="£138.70"></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        </table>
    </td>
</tr>
<tr bgcolor="#FFFFFF">
    <td height="20"><div align="center"><img src="Images/DottedLine.jpg" width="860" /></div></td>
</tr>
<tr bgcolor="#FFFFFF">
            <td>&nbsp;<asp:HiddenField ID="hfBorrow" runat="server" />
                                <asp:HiddenField ID="hfPeriod" runat="server" />
                                <asp:HiddenField ID="hfAfford" runat="server" />
                                <asp:HiddenField ID="hfTotalInterest" runat="server" />
                                <asp:HiddenField ID="hfTotalToPay" runat="server" />
                                <asp:HiddenField ID="hfMonth1Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth2Pay" runat="server" />
                                <asp:HiddenField ID="hfMonth3Pay" runat="server" />
                <asp:HiddenField ID="hfMonth4Pay" runat="server" />
                <asp:HiddenField ID="hfMonth5Pay" runat="server" />
                <asp:HiddenField ID="hfMonth6Pay" runat="server" />
                <asp:HiddenField ID="hfMonth7Pay" runat="server" />
                <asp:HiddenField ID="hfMonth8Pay" runat="server" />
                <asp:HiddenField ID="hfMonth9Pay" runat="server" />
                <asp:HiddenField ID="hfMonth10Pay" runat="server" />
                <asp:HiddenField ID="hfMonth11Pay" runat="server" />
                <asp:HiddenField ID="hfMonth12Pay" runat="server" />
                                </td>
</tr>
<tr>
    <td valign="top">
        <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>            
            <td width="443" valign="top">
            <table width="441" height="355" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td valign="top">
                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="70%" valign="top">
                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="20px"><strong>How much cash would you like to borrow?</strong></td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <asp:TextBox ID="txtHowMuch" runat="server" AutoPostBack="True" Height="18px"></asp:TextBox>
                                    <asp:SliderExtender ID="txtHowMuch_SliderExtender" runat="server" 
        BoundControlID="txtHowMuchBound" TargetControlID="txtHowMuch" Minimum="100" Maximum="400" 
                             RailCssClass="slider_rail" HandleCssClass="slider_handle" HandleImageUrl="Images/btnSliderHandle.jpg"/>                                            
                                </td>
                                <td height="20px">
                                    <asp:TextBox ID="txtHowMuchBound" runat="server" Width="50px" 
                                        AutoPostBack="True" Height="20px" BorderColor="#67AE3E" 
                                    BorderStyle="Solid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">&nbsp;</td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px"><strong>How many months would you like the loan for?</strong></td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <asp:TextBox ID="txtHowLong" runat="server" AutoPostBack="True" Height="18px"></asp:TextBox>
                                    <asp:SliderExtender ID="txtHowLong_SliderExtender" runat="server" 
                                        BoundControlID="txtHowLongBound" 
                                        TargetControlID="txtHowLong" 
                                        Minimum="1" Maximum="3" RailCssClass="slider_rail" HandleCssClass="slider_handle" HandleImageUrl="Images/btnSliderHandle.jpg"   
                                         />
                                </td>
                                <td height="20px">
                                    <asp:TextBox ID="txtHowLongBound" runat="server" Width="50px" 
                                        AutoPostBack="True" Height="20px" BorderColor="#67AE3E" 
                                    BorderStyle="Solid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">&nbsp;</td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px"><strong>How much would you like to payback each month?</strong></td>
                                <td height="20px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <asp:TextBox ID="txtAfford" runat="server" AutoPostBack="True" Height="18px"></asp:TextBox>
                                    <asp:SliderExtender ID="txtAfford_SliderExtender" runat="server" 
                                        BoundControlID="txtAffordBound" HandleCssClass="slider_handle" Maximum="400" HandleImageUrl="Images/btnSliderHandle.jpg"
                                        Minimum="100" RailCssClass="slider_rail" TargetControlID="txtAfford" />
                                </td>
                                <td height="20px">
                                                                        <asp:TextBox ID="txtAffordBound" runat="server" AutoPostBack="True" 
                                        Width="50px" Height="20px" BorderColor="#67AE3E" 
                                    BorderStyle="Solid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    &nbsp;</td>
                                <td height="20px">
                                                                        &nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <strong>Optional add on costs</strong></td>
                                <td height="20px">
                                                                        &nbsp;</td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    &nbsp;</td>
                                <td height="20px">
                                                                        &nbsp;</td>
                            </tr>
                            <tr valign="middle">
                                <td height="20px">
                                    Membership fee</td>
                                <td height="20px"><asp:TextBox ID="txtMemberFee" runat="server" 
                                        Width="50px" ReadOnly="True" BorderColor="#67AE3E" BorderStyle="Solid" 
                                        Height="20px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    Your first savings</td>
                                <td height="20px"><asp:TextBox ID="txtFirstSaving" runat="server" AutoPostBack="True" 
                                        Width="50px" BorderColor="#67AE3E" BorderStyle="Solid" 
                                        Height="20px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <table style="width:100%;">
                                        <tr>
                                            <td width="60%">
                                                Instant Loan decision making</td>
                                            <td width="20%">
                    <asp:RadioButton ID="radInstantDecisionYes" runat="server" AutoPostBack="True" 
                        Text="Yes" ValidationGroup="vgInstantDecision" Checked="True" GroupName="InstantDecision" />
                                            </td>
                                            <td width="20%">
                    <asp:RadioButton ID="radInstantDecisionNo" runat="server" AutoPostBack="True" 
                        Text="No" ValidationGroup="vgInstantDecision" GroupName="InstantDecision" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="20px"><asp:TextBox ID="txtInstantDecision" runat="server" AutoPostBack="True" 
                                        Width="50px" ReadOnly="True" BorderColor="#67AE3E" BorderStyle="Solid" 
                                        Height="20px"></asp:TextBox>
                                    <span class="hotspot" onmouseover="tooltip.show('If you say No - we will be manually processing your loan during our office hours - Monday to Friday, which could take over 3 days.');" onmouseout="tooltip.hide();"><img alt="" src="Images/GreenInfoButton.jpg" height="20px" width="20px" /></span></td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <table style="width:100%;">
                                        <tr>
                                            <td width="60%">
                                                Same day payment</td>
                                            <td width="20%">
                    <asp:RadioButton ID="radSameDayPayYes" runat="server" AutoPostBack="True" 
                        Text="Yes" ValidationGroup="vgSameDayPay" Checked="True" GroupName="SameDayPay" />
                                            </td>
                                            <td width="20%">
                    <asp:RadioButton ID="radSameDayPayNo" runat="server" AutoPostBack="True" 
                        Text="No" ValidationGroup="vgSameDayPay" GroupName="SameDayPay" />
                                                                                    </td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="20px"><asp:TextBox ID="txtSameDay" runat="server" AutoPostBack="True" 
                                        Width="50px" ReadOnly="True" BorderColor="#67AE3E" BorderStyle="Solid" 
                                        Height="20px"></asp:TextBox>
                                                                                        <span class="hotspot" onmouseover="tooltip.show('If you dont want a same day payment, we could pay it free using BACS which will take up to 3 days to reach money in your current account.');" onmouseout="tooltip.hide();"><img alt="" src="Images/GreenInfoButton.jpg" height="20px" width="20px" /></span></td>
                            </tr>
                            <tr>
                                <td height="20px">
                                    <table style="width:100%;">
                                        <tr>
                                            <td width="60%">
                                                Repeat Account facility</td>
                                            <td width="20%">
                                                <asp:RadioButton ID="radRepeatAccFacilityYes" runat="server" 
                        AutoPostBack="True" Text="Yes" ValidationGroup="vgRepeatAccFacility" Checked="True" 
                                                    GroupName="RepeatAccFacility" />
                                            </td>
                                            <td width="20%">
                                                <asp:RadioButton ID="radRepeatAccFacilityNo" runat="server" AutoPostBack="True" 
                            Text="No" ValidationGroup="vgRepeatAccFacility" GroupName="RepeatAccFacility" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td height="20px"><asp:TextBox ID="txtRepeatAcc" runat="server" AutoPostBack="True" 
                                        Width="50px" ReadOnly="True" BorderColor="#67AE3E" BorderStyle="Solid" 
                                        Height="20px"></asp:TextBox>
                                                                                        <span class="hotspot" onmouseover="tooltip.show('If you think that you might want to use our payday loan services in the future, this facility could fast track your request next time.');" onmouseout="tooltip.hide();"><img alt="" src="Images/GreenInfoButton.jpg" height="20px" width="20px" /></span></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                 </td>
            </tr>
            <tr>
                <td valign="top">
                
                    <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                
                </td>
            </tr>
            </table>
            <br />
            <br />
            </td>
            <td width="40">&nbsp;</td>
            <td valign="top"><strong><span class="style58">Loan summary </span></strong><br /><br />
                <table align="center" bgcolor="#EAF7DD" border-color="#CCCCCC" cellpadding="0" 
                    cellspacing="0" style="border-style: solid; width:100%;" border="1">
                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" style="width:94%;">
                                <tr style="height: 20px">
                                    <td align="left" valign="bottom">
                                        <strong><asp:Literal ID="lit1" runat="server"></asp:Literal></strong>
                                    </td>
                                </tr>
                                <tr style="height: 2px">
                                    <td>
                                        <div align="center">
                                            <hr />
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 5px">
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    Total interest</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="lit2" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    <strong>Total to pay</strong></td>
                                                <td align="right" width="30%">
                                                    <strong><asp:Literal ID="lit3" runat="server"></asp:Literal></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    Month 1 repayment</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="lit4" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    Month 2 repayment</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="lit5" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr style="height: 20px" valign="top">
                                                <td align="left" width="70%">
                                                    Month 3 repayment</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="lit6" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table align="center" bgcolor="#EAF7DD" border-color="#CCCCCC" cellpadding="0" 
                    cellspacing="0" style="border-style: solid; width:100%;" border="1">
                    <tr>
                        <td align="center">
                            <table cellpadding="0" cellspacing="0" style="width:94%;">       
                                <tr style="height: 20px">
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%"><strong>Total CUOK Loan</strong></td>
                                                <td align="right" width="30%">
                                                    <strong>
                                                    <asp:Literal ID="litSummaryTotal" runat="server"></asp:Literal>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    Interest</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="litSummaryInterest" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    Total other costs</td>
                                                <td align="right" width="30%">
                                                    <asp:Literal ID="litOtherCosts" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr>
                                                <td align="left" width="70%">
                                                    <strong>Total cost </strong></td>
                                                <td align="right" width="30%">
                                                    <strong>
                                                    <asp:Literal ID="litTotalCost" runat="server"></asp:Literal>
                                                    </strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr valign="top">
                                                <td align="left" width="80%">
                                                    Cost of same loan from a typical payday lender</td>
                                                <td align="right" width="20%">
                                                    <asp:Literal ID="litOtherLenderCost" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr style="height: 20px" valign="top">
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width:100%;">
                                            <tr valign="top">
                                                <td align="left" width="70%">
                                                    <strong>Our CUOK loan will still save you</strong></td>
                                                <td align="right" width="30%">
                                                    <strong><asp:Literal ID="litSave" runat="server"></asp:Literal></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                
                    <br />
                <table width="150" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="150" height="29">
                        <asp:Button id="btnShowPopup" runat="server" style="display:none" />
                        <asp:ImageButton ID="ImageButton1" runat="server" 
                                    ImageUrl="~/PDL/Images/btnApplyLoan.jpg" />
                                    <asp:ModalPopupExtender ID="mdlPopup" runat="server" 
                CancelControlID="Button1" PopupControlID="pnlModelPopUp" 
                TargetControlID="btnShowPopup" BackgroundCssClass="modalBackground" 
                OkControlID="btnOK" onokscript="onOk()" DropShadow="true">
            </asp:ModalPopupExtender>    
                                <div align="center" class="style76">Apply for this loan </div>
                    </td>
                </tr>
                </table>
                <br />
             </td>
        </tr>
        </table>
    </td>
</tr>
</table>
      <asp:Panel ID="pnlModelPopUp" runat="server" Width="250px" BorderColor="#33CC33" 
            BorderStyle="Solid" BorderWidth="1px" Height="300px" BackColor="White">
          <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
              <ContentTemplate>
                  <table style="width:100%;">
                      <tr>
                          <td align="right">
                              <asp:ImageButton ID="Button1" runat="server" ImageUrl="images/icon_delete.png" 
                                  Text="Close" />
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <strong>Security check</strong></td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;Enter the word below.</td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;Cant&#39;t read this?
                              <asp:LinkButton ID="lbtnTryAnother" runat="server">try another</asp:LinkButton>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td align="center">
                              <asp:Image ID="imgCaptcha" runat="server" />
                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td valign="middle">
                              &nbsp;Text in the box&nbsp;&nbsp;<asp:TextBox ID="txtEnteredWord" runat="server"></asp:TextBox>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td align="center">
                              <asp:ImageButton ID="btnApplyWithSecurity" runat="server" 
                                  ImageUrl="~/PDL/Images/btnApplyLoan.jpg" Text="OK" />
                              <asp:Button ID="btnOK" runat="server" style="display:none" Text="Button" />
                          </td>
                      </tr>
                  </table>
              </ContentTemplate>
          </asp:UpdatePanel>
        </asp:Panel>  
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>


<asp:Content ID="Content3" runat="server" 
    contentplaceholderid="HeadingContentPlaceHolder">Pay Day Loans
</asp:Content>



