﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_PDLApplyPDL3, App_Web_oc53n32s" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="26" valign="middle" class="BodyText">
              <p>This loan application form must be fully completed. Failure to disclose all required information may delay approval of your loan application.</p>
              <br /><p><strong>Please note: <span class="style82">ALL</span> fields are required except where stated, thank you.</strong></p></td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText">&nbsp;</td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText"><table width="466" border="0" cellspacing="2" cellpadding="0">
              <tr valign="middle">
                <td width="25%" height="42" bgcolor="#00a79d"><div align="center" class="style37">Personal <br>
      details</div></td>
                <td width="25%" bgcolor="#e20076"><div align="center" class="style37">Your <br>
      employment</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Loan <br>
      details</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Declaration</div></td>
              </tr>
              <tr valign="middle">
                <td height="10" colspan="5">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText"><table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                              <td>
                                          <table style="width:100%;" __designer:mapid="276">
                                              <tr __designer:mapid="277">
                                                  <td width="40%" __designer:mapid="278">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="279">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;Employment status:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                      runat="server" ControlToValidate="ddlEmpStatus" 
                                      ErrorMessage="Employment status required">*</asp:RequiredFieldValidator>
                                                  </td>
                                                  <td __designer:mapid="27d">
                                  <asp:DropDownList ID="ddlEmpStatus" runat="server" Width="208px" 
                                      DataSourceID="odsEmpStatus" DataTextField="LookupCodeName" DataValueField="LookupCodeID" 
                                                          AutoPostBack="True" Font-Bold="True">
                                  </asp:DropDownList>
                                                                                            </td>
                                              </tr>
                                              </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                      <ContentTemplate>
                                          <asp:Panel ID="pnlCompany" runat="server">
                                              <table style="width:100%;">
                                                  <tr>
                                                      <td>
                                                          <table style="width:100%;">
                                                              <tr>
                                                                  <td width="40%">
                                                                      &nbsp;</td>
                                                                  <td>
                                                                      &nbsp;</td>
                                                              </tr>
                                                              <tr>
                                                                  <td>
                                                                      &nbsp;Job title:<asp:RequiredFieldValidator ID="RequiredFieldValidator24" 
                                                                          runat="server" ControlToValidate="txtJobTitle" 
                                                                          ErrorMessage="Job title required">*</asp:RequiredFieldValidator>
                                                                  </td>
                                                                  <td>
                                                                      <asp:TextBox ID="txtJobTitle" runat="server" Width="200px"></asp:TextBox>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td>
                                                                      &nbsp;Select Company:</td>
                                                                  <td align="left" valign="middle">
                                                                      <table style="width:85%;">
                                                                          <tr>
                                                                              <td valign="middle">
                                                                                  <asp:DropDownList ID="ddlEmploerList" runat="server" AutoPostBack="True" 
                                                                                      DataSourceID="odsEmployers" DataTextField="CompanyName" 
                                                                                      DataValueField="EmployerID" Font-Bold="True" Width="208px">
                                                                                  </asp:DropDownList>
                                                                              </td>
                                                                              <td>
                                                                                  <span class="hotspot" onmouseover="tooltip.show('If not listed, please leave blank and type company name below');" onmouseout="tooltip.hide();"><img 
                                            alt="" src="Images/GreenInfoButton.png" height="25px" width="25px" /></span></td>
                                                                          </tr>
                                                                      </table>
                                                                  </td>
                                                              </tr>
                                                              <tr id="RowCompanyName" runat="server">
                                                                  <td>
                                                                      &nbsp;<span style="color: #FF3300"><strong>Or</strong></span> enter the company name:</td>
                                                                  <td>
                                                                      <asp:TextBox ID="txtCompanyName" runat="server" Width="200px"></asp:TextBox>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td>
                                                                      &nbsp;Department:</td>
                                                                  <td>
                                                                      <asp:TextBox ID="txtDepartment" runat="server" Width="200px"></asp:TextBox>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td>
                                                                      &nbsp;Time with current employer:<asp:RangeValidator ID="RangeValidator1" 
                                                                          runat="server" ControlToValidate="ddlEmployerYears" 
                                                                          ErrorMessage="Invalid no of years with employer" MaximumValue="50" 
                                                                          MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                                                      <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                                                          ControlToValidate="ddlEmployerMonths" 
                                                                          ErrorMessage="Invalid no of months with employer" Operator="GreaterThan" 
                                                                          Type="Integer" ValueToCompare="-1">*</asp:CompareValidator>
                                                                  </td>
                                                                  <td>
                                                                      <table border="0" cellpadding="0" cellspacing="0" style="width: 74%;">
                                                                          <tr>
                                                                              <td>
                                                                                  <asp:DropDownList ID="ddlEmployerYears" runat="server" Width="65px" 
                                                                                      Font-Bold="True">
                                                                                  </asp:DropDownList>
                                                                              </td>
                                                                              <td width="60%" align="right">
                                                                                  <asp:DropDownList ID="ddlEmployerMonths" runat="server" Width="90px" 
                                                                                      Font-Bold="True">
                                                                                      <asp:ListItem Value="-1">MONTHS</asp:ListItem>
                                                                                      <asp:ListItem Value="0">0</asp:ListItem>
                                                                                      <asp:ListItem Value="1">1</asp:ListItem>
                                                                                      <asp:ListItem Value="2">2</asp:ListItem>
                                                                                      <asp:ListItem Value="3">3</asp:ListItem>
                                                                                      <asp:ListItem Value="4">4</asp:ListItem>
                                                                                      <asp:ListItem Value="5">5</asp:ListItem>
                                                                                      <asp:ListItem Value="6">6</asp:ListItem>
                                                                                      <asp:ListItem Value="7">7</asp:ListItem>
                                                                                      <asp:ListItem Value="8">8</asp:ListItem>
                                                                                      <asp:ListItem Value="9">9</asp:ListItem>
                                                                                      <asp:ListItem Value="10">10</asp:ListItem>
                                                                                      <asp:ListItem Value="11">11</asp:ListItem>
                                                                                      <asp:ListItem Value="12">12</asp:ListItem>
                                                                                  </asp:DropDownList>
                                                                              </td>
                                                                          </tr>
                                                                      </table>
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                  <td>
                                                                      &nbsp;Work telephone:</td>
                                                                  <td>
                                                                      <asp:TextBox ID="txtWorkTP" runat="server" Width="200px"></asp:TextBox>
                                                                  </td>
                                                              </tr>
                                                          </table>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </asp:Panel>
                                      </ContentTemplate>
                                  </asp:UpdatePanel>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <table style="width:100%;">
                                      <tr>
                                          <td>
                                              <asp:UpdatePanel ID="upnlEmpType" runat="server">
                                                  <ContentTemplate>
                                                      <table style="width:100%;">
                                                          <tr>
                                                              <td width="40%" valign="top">
                                                                  &nbsp;What is your work status?<asp:RequiredFieldValidator 
                                                                      ID="RequiredFieldValidator23" runat="server" ControlToValidate="radEmpType" 
                                                                      ErrorMessage="Work status required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td>
                                                                  <asp:RadioButtonList ID="radEmpType" runat="server" AutoPostBack="True" 
                                                                      RepeatDirection="Horizontal">
                                                                      <asp:ListItem Value="1">Permanent</asp:ListItem>
                                                                      <asp:ListItem Value="0">Contract</asp:ListItem>
                                                                  </asp:RadioButtonList>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td valign="top" width="40%">
                                                                  <asp:RangeValidator ID="RangeValidator2" runat="server" 
                                                                      ControlToValidate="ddlContractDurationYears" 
                                                                      ErrorMessage="Invalid no of years for contract duration" MaximumValue="50" 
                                                                      MinimumValue="0" Type="Integer">*</asp:RangeValidator>
                                                                  <asp:CompareValidator ID="CompareValidator5" runat="server" 
                                                                      ControlToValidate="ddlContractDurationMonths" 
                                                                      ErrorMessage="Invalid no of months for contract period" Operator="GreaterThan" 
                                                                      Type="Integer" ValueToCompare="-1">*</asp:CompareValidator>
                                                              </td>
                                                              <td>
                                                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 70%;">
                                                                      <tr>
                                                                          <td>
                                                                              <asp:DropDownList ID="ddlContractDurationYears" runat="server" Visible="False" 
                                                                                  Width="65px">
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                          <td>
                                                                              <asp:DropDownList ID="ddlContractDurationMonths" runat="server" Visible="False" 
                                                                                  Width="90px">
                                                                                  <asp:ListItem Value="-1">MONTHS</asp:ListItem>
                                                                                  <asp:ListItem Value="0">0</asp:ListItem>
                                                                                  <asp:ListItem Value="1">1</asp:ListItem>
                                                                                  <asp:ListItem Value="2">2</asp:ListItem>
                                                                                  <asp:ListItem Value="3">3</asp:ListItem>
                                                                                  <asp:ListItem Value="4">4</asp:ListItem>
                                                                                  <asp:ListItem Value="5">5</asp:ListItem>
                                                                                  <asp:ListItem Value="6">6</asp:ListItem>
                                                                                  <asp:ListItem Value="7">7</asp:ListItem>
                                                                                  <asp:ListItem Value="8">8</asp:ListItem>
                                                                                  <asp:ListItem Value="9">9</asp:ListItem>
                                                                                  <asp:ListItem Value="10">10</asp:ListItem>
                                                                                  <asp:ListItem Value="11">11</asp:ListItem>
                                                                                  <asp:ListItem Value="12">12</asp:ListItem>
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </ContentTemplate>
                                              </asp:UpdatePanel>
                                                                                            </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:Panel ID="pnlEmpDetails" runat="server">
                                      <table style="width:100%;">
                                          <tr>
                                              <td width="40%">
                                                  <asp:UpdatePanel ID="updatePanelAddress1" runat="server">
                                                      <ContentTemplate>
                                                          <asp:ValidationSummary ID="ValidationSummary2" runat="server" 
                                                              ValidationGroup="vgSearch" />
                                                          &nbsp;<asp:Panel ID="pnlAddress1" runat="server">
                                                              <table style="width:100%;">
                                                                  <tr>
                                                                      <td height="40" width="40%">
                                                                          &nbsp;<strong ID="40">Work address<asp:Label ID="lblAddressMessage" 
                                                                              runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                          </strong>
                                                                      </td>
                                                                      <td>
                                                                          <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                                              AssociatedUpdatePanelID="updatePanelAddress1">
                                                                              <ProgressTemplate>
                                                                                  <asp:Image ID="Image1" runat="server" ImageUrl="~/PDL/Images/loading.gif" 
                                                                                      Width="30px" />
                                                                                  &nbsp;Please wait...
                                                                              </ProgressTemplate>
                                                                          </asp:UpdateProgress>
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                                              runat="server" ControlToValidate="txtWorkPostCode" ErrorMessage="Invalid postcode" 
                                                                              
                                                                              ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$">*</asp:RegularExpressionValidator>
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                                              ControlToValidate="txtWorkPostCode" ErrorMessage="Postcode required">*</asp:RequiredFieldValidator>
                                                                          <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                                              ControlToValidate="txtWorkPostCode" ErrorMessage="Invalid postcode" 
                                                                              ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                                              ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                                                              ControlToValidate="txtWorkPostCode" ErrorMessage="Work Postcode required" 
                                                                              ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtWorkPostCode" runat="server" AutoPostBack="True" 
                                                                              Width="140px"></asp:TextBox>
                                                                          &nbsp;<asp:Button ID="btnSearch1" runat="server" Text="Search" 
                                                                              ValidationGroup="vgSearch" BackColor="#00a79d" Font-Bold="True" />
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;Address line 1:<asp:RequiredFieldValidator ID="RequiredFieldValidator14" 
                                                                              runat="server" ControlToValidate="txtWorkAddressLine1" 
                                                                              ErrorMessage="Work address line 1 required">*</asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtWorkAddressLine1" runat="server" Width="200px" TabIndex="1"></asp:TextBox>
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;Address line 2:</td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtWorkAddressLine2" runat="server" Width="200px"></asp:TextBox>
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;City:<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                                                              ControlToValidate="txtWorkCity" ErrorMessage="Work city required">*</asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtWorkCity" runat="server" Width="200px"></asp:TextBox>
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;County:<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                                              ControlToValidate="txtWorkCounty" ErrorMessage="Work county required">*</asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtWorkCounty" runat="server" Width="200px"></asp:TextBox>
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </asp:Panel>
                                                          <asp:Panel ID="pnlAddressSearch" runat="server" Visible="False">
                                                              <table style="width:100%;">
                                                                  <tr>
                                                                      <td height="40" width="40%">
                                                                          &nbsp;<strong ID="41">Work address</strong></td>
                                                                      <td>
                                                                          &nbsp;</td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator4" 
                                                                              runat="server" ControlToValidate="txtPostCodeList" 
                                                                              ErrorMessage="Invalid postcode" 
                                                                              ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                                              ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                                          <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                                                              ControlToValidate="txtPostCodeList" ErrorMessage="Postcode required" 
                                                                              ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:TextBox ID="txtPostCodeList" runat="server" AutoPostBack="True" 
                                                                              Width="200px"></asp:TextBox>
                                                                          &nbsp;<asp:Button ID="btnSearchList" runat="server" Text="Search" 
                                                                              ValidationGroup="vgSearch" BackColor="#00a79d" Font-Bold="True" />
                                                                      </td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td>
                                                                          &nbsp;Address:<asp:RequiredFieldValidator ID="RequiredFieldValidator12" 
                                                                              runat="server" ControlToValidate="ddlAddressList" 
                                                                              ErrorMessage="Address required">*</asp:RequiredFieldValidator>
                                                                          <asp:RequiredFieldValidator ID="WorkAddressValidator" 
                                                                         runat="Server" InitialValue=""
                                                                            ControlToValidate ="ddlAddressList" ErrorMessage="Select the Work address">*
                                                                    </asp:RequiredFieldValidator>
                                                                      </td>
                                                                      <td>
                                                                          <asp:DropDownList ID="ddlAddressList" runat="server" AutoPostBack="True" 
                                                                              Width="300px">
                                                                          </asp:DropDownList>
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </asp:Panel>
                                                      </ContentTemplate>
                                                  </asp:UpdatePanel>
                                              </td>
                                          </tr>
                                      </table>
                                  </asp:Panel>
                              </td>
                          </tr>
                          <tr>
                                <td>
                                                               
                                      <table style="width:100%;">
                                          <tr>
                                              <td width="40%">
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Monthly income (After Tax):<asp:RequiredFieldValidator ID="RequiredFieldValidator16" 
                                      runat="server" ControlToValidate="txtMonthlyIncomeAfterTax" 
                                      ErrorMessage="Monthly income required">*</asp:RequiredFieldValidator>
                                                  <asp:CompareValidator ID="CompareValidator7" runat="server" 
                                                      ControlToValidate="txtMonthlyIncomeAfterTax" 
                                                      ErrorMessage="Invalid monthly income" 
                                                      Operator="GreaterThan" Type="Double" ValueToCompare="0">*</asp:CompareValidator>
                                              </td>
                                              <td>
                                                  <asp:TextBox ID="txtMonthlyIncomeAfterTax" runat="server" Width="200px"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Income/Wage frequency<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator17" runat="server" 
                                                      ControlToValidate="ddlIncomeFrequency" 
                                                      ErrorMessage="Income frequency required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top">
                                                  <asp:DropDownList ID="ddlIncomeFrequency" runat="server" 
                                                      Width="208px" Font-Bold="True">
                                                      <asp:ListItem></asp:ListItem>
                                                      <asp:ListItem>Monthly</asp:ListItem>
                                                      <asp:ListItem>Weekly</asp:ListItem>
                                                      <asp:ListItem>Four Weekly</asp:ListItem>
                                                  </asp:DropDownList>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Next payday: (dd/mm/yyyy)<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator18" runat="server" 
                                                      ControlToValidate="txtNextPayDay" 
                                                      ErrorMessage="Next pay day required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top">
                                                  <asp:TextBox ID="txtNextPayDay" runat="server" Width="200px"></asp:TextBox><asp:CalendarExtender
                                                      ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                      PopupPosition="Right" TargetControlID="txtNextPayDay" 
                                                      TodaysDateFormat="dd/MM/yyyy">
                                                  </asp:CalendarExtender>
                                              </td>
                                          </tr> 
                                          </table>
                                          <tr>
                                <td>
                                <asp:Panel ID="pnlPayDetails" runat="server">                                  
                                      <table style="width:100%;">

                                          <tr>
                                              <td style="width: 174px">
                                                  Payroll/Staff number:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator19" runat="server" 
                                                      ControlToValidate="txtStaffNumber" 
                                                      ErrorMessage="Payroll/Staff number required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top">
                                                  &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtStaffNumber" runat="server" Width="200px"></asp:TextBox>
                                              </td>
                                          </tr>
                                           </table>     
                                </asp:Panel>  
                                          </td>
                          </tr>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                              <td valign="top">
                                                  &nbsp;</td>
                                          </tr>
                                           
                                                           
                              </td>
                          </tr>
                          <tr><td height="40"><strong>Bank Account your income is paid into.</strong></td></tr>
                          <tr><td>
                          <asp:Panel ID="pnlBankAcc" runat="server">
                          <table style="width:100%;">
                                          <tr>
                                              <td width="40%">
                                                  &nbsp;</td>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td style="height: 16px">
                                                  Current Account number:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator3" runat="server" 
                                                      ControlToValidate="txtAccountNumber" 
                                                      ErrorMessage="Account number required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top" style="height: 16px">
                                                  <asp:TextBox ID="txtAccountNumber" runat="server" Width="200px" MaxLength="9"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                 Sort code:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator4" runat="server" 
                                                      ControlToValidate="txtSortCode" 
                                                      ErrorMessage="Sort code required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top">
                                                  <asp:TextBox ID="txtSortCode" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Bank name:</td>
                                              <td valign="top">
                                                  <asp:TextBox ID="txtBankName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Account Owner:</td>
                                              <td valign="top">
                                                  <asp:DropDownList ID="ddlAccountOwner" runat="server" Font-Bold="True" 
                                                      Width="208px">
                                                      <asp:ListItem>Personal</asp:ListItem>
                                                      <asp:ListItem>Joint</asp:ListItem>                                                      
                                                  </asp:DropDownList>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Account Opened:<asp:RangeValidator ID="RangeValidator3" runat="server" 
                                                      ControlToValidate="ddlTimeWithBankYears" 
                                                      ErrorMessage="Invalid account opened year" MaximumValue="2050" MinimumValue="1900" 
                                                      Type="Integer">*</asp:RangeValidator>
                                                  <asp:CompareValidator ID="CompareValidator6" runat="server" 
                                                                          ControlToValidate="ddlTimeWithBankMonths" 
                                                                          ErrorMessage="Invalid account opened month" Operator="GreaterThan" 
                                                                          Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                                                                  </td>
                                              <td valign="top">
                                                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 74%;">
                                                                      <tr>
                                                                          <td>
                                                                              <asp:DropDownList ID="ddlTimeWithBankYears" runat="server" 
                                                                                  Width="65px" Font-Bold="True">
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                          <td align="right">
                                                                              <asp:DropDownList ID="ddlTimeWithBankMonths" runat="server" 
                                                                                  Width="90px" Font-Bold="True">
                                                                                  <asp:ListItem Value="-1">MONTH</asp:ListItem>
                                                                                  <asp:ListItem Value="1">Jan</asp:ListItem>
                                                                                  <asp:ListItem Value="2">Feb</asp:ListItem>
                                                                                  <asp:ListItem Value="3">Mar</asp:ListItem>
                                                                                  <asp:ListItem Value="4">Apr</asp:ListItem>
                                                                                  <asp:ListItem Value="5">May</asp:ListItem>
                                                                                  <asp:ListItem Value="6">Jun</asp:ListItem>
                                                                                  <asp:ListItem Value="7">Jul</asp:ListItem>
                                                                                  <asp:ListItem Value="8">Aug</asp:ListItem>
                                                                                  <asp:ListItem Value="9">Sep</asp:ListItem>
                                                                                  <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                                  <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                                  <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                              <td valign="top">
                                                  &nbsp;</td>
                                          </tr>
                                      </table>
                           </asp:Panel>
                           </td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  <asp:Button ID="btnPre" runat="server" Text="Previous" Width="70px" 
                                                                      ValidationGroup="vgPre" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Next" Width="70px" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                                  <asp:ObjectDataSource ID="odsEmpStatus" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="EMPLOYMENT_STATUS" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsEmployers" runat="server" 
                                      SelectMethod="SelectEmployers" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                  </asp:ObjectDataSource>
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>



