﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_PDLApplyPDL4, App_Web_oc53n32s" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="26" valign="middle" class="BodyText">
              <p>This loan application form must be fully completed. Failure to disclose all required information may delay approval of your loan application.</p>
              <br /><p><strong>Please note: <span class="style82">ALL</span> fields are required except where stated, thank you.</strong></p></td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText">&nbsp;</td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText"><table width="466" border="0" cellspacing="2" cellpadding="0">
              <tr valign="middle">
                <td width="25%" height="42" bgcolor="#00a79d"><div align="center" class="style37">Personal <br>
      details</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Your <br>
      employment</div></td>
                <td width="25%" bgcolor="#e20076"><div align="center" class="style37">Loan <br>
      details</div></td>
                <td width="25%" bgcolor="#00a79d"><div align="center" class="style37">Declaration</div></td>
              </tr>
              <tr valign="middle">
                <td height="10" colspan="5">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText"><table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                              <td>
                                          <table style="width:100%;" __designer:mapid="276">
                                              <tr __designer:mapid="277">
                                                  <td width="50%" __designer:mapid="278">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="279">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr __designer:mapid="27a">                                                  
                                                  <td __designer:mapid="27d">
  &nbsp;Loan amount:</td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtLoanAmount" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;Repayment period (months):</td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtRepayPeriod" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;Total amount payable (inc. interest &amp; fees)</td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtTotalPayable" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;<strong>First loan repayment due from:<br />&nbsp;(or within 30 days from this date)</strong></td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtFirstPaymentDueOn" runat="server" Width="200px" ReadOnly="True"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;How you intend to use your loan?<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                                            ControlToValidate="ddlPurpose" ErrorMessage="Purpose required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                                                  <td __designer:mapid="27d">
                                  <asp:DropDownList ID="ddlPurpose" runat="server" Width="208px" >
                                      <asp:ListItem Value="98">Select</asp:ListItem>                                      
                                      <asp:ListItem Value="03">Electrical Goods</asp:ListItem>
                                      <asp:ListItem Value="04">Furniture</asp:ListItem>
                                      <asp:ListItem Value="05">Home Improvements</asp:ListItem>
                                      <asp:ListItem Value="08">Holiday</asp:ListItem>
                                      <asp:ListItem Value="09">Debt Consolidation</asp:ListItem>
                                      <asp:ListItem Value="1000">Children's Gifts</asp:ListItem>
                                      <asp:ListItem Value="1001">Festivities</asp:ListItem>
                                      <asp:ListItem Value="1002">Car Insurance</asp:ListItem>
                                      <asp:ListItem Value="1003">Kitchen Appliance</asp:ListItem>
                                      <asp:ListItem Value="1004">Education Fee's</asp:ListItem>
                                      <asp:ListItem Value="1005">Public Transport Costs</asp:ListItem>
                                      <asp:ListItem Value="1006">Rent</asp:ListItem>
                                      <asp:ListItem Value="1007">Council Tax</asp:ListItem>
                                      <asp:ListItem Value="1008">Utility Bills</asp:ListItem>
                                      <asp:ListItem Value="97">Other</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="27d">
                                                      &nbsp;</td>
                                              </tr>
                                              </table>
                              </td>
                          </tr>                          
                          <tr>
                                <td>
                                    &nbsp;</td>
                          </tr>
                          <tr><td>
                              <asp:Panel ID="pnlOnlineLoginDetails" runat="server">
                                  <table style="width:100%;">
                                      <tr>
                                          
                                          <td>
                                              <strong>Online Login Account Setup</strong></td>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  <table style="width:100%;">
                                                      <tr>
                                                          <td width="50%">
                                                              &nbsp;</td>
                                                          <td>
                                                              &nbsp;</td>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;Password:<asp:RequiredFieldValidator ID="RequiredFieldValidator16" 
                                                                  runat="server" ControlToValidate="txtPassword" ErrorMessage="Password required">*</asp:RequiredFieldValidator>
                                                          </td>
                                                          <td>
                                                              <asp:TextBox ID="txtPassword" runat="server" MaxLength="15" TextMode="Password" 
                                                                  Width="200px"></asp:TextBox>
                                                          </td>
                                                          <td>
                                                              <span class="hotspot" onmouseout="tooltip.hide();" 
                                                                  onmouseover="tooltip.show('Minimum password length is 6 characters and maximum should not exceed 15 characters.');">
                                                              <img 
                                            alt="" src="Images/GreenInfoButton.png" height="30px" width="30px" />
                                                              </span>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;Confirm password:<asp:RequiredFieldValidator ID="RequiredFieldValidator17" 
                                                                  runat="server" ControlToValidate="txtPasswordConfirm" 
                                                                  ErrorMessage="Password confirmation required">*</asp:RequiredFieldValidator>
                                                          </td>
                                                          <td>
                                                              <asp:TextBox ID="txtPasswordConfirm" runat="server" TextMode="Password" 
                                                                  Width="200px"></asp:TextBox>
                                                          </td>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;Memorable word:<asp:RequiredFieldValidator ID="RequiredFieldValidator18" 
                                                                  runat="server" ControlToValidate="txtMemorableWord" 
                                                                  ErrorMessage="Memorable word required">*</asp:RequiredFieldValidator>
                                                          </td>
                                                          <td>
                                                              <asp:TextBox ID="txtMemorableWord" runat="server" MaxLength="15" 
                                                                  TextMode="Password" Width="200px"></asp:TextBox>
                                                          </td>
                                                          <td>
                                                              <span class="hotspot" onmouseout="tooltip.hide();" 
                                                                  onmouseover="tooltip.show('Minimum memorable word length is 6 characters and maximum should not exceed 15 characters.');">
                                                              <img 
                                            alt="" src="Images/GreenInfoButton.png" height="30px" width="30px" />
                                                              </span>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              &nbsp;Confirm memorable word:<asp:RequiredFieldValidator 
                                                                  ID="RequiredFieldValidator19" runat="server" 
                                                                  ControlToValidate="txtMemorableWordConfirm" 
                                                                  ErrorMessage="Memorable word confirmation required">*</asp:RequiredFieldValidator>
                                                          </td>
                                                          <td>
                                                              <asp:TextBox ID="txtMemorableWordConfirm" runat="server" TextMode="Password" 
                                                                  Width="200px"></asp:TextBox>
                                                          </td>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                          </td>
                                                          <td>
                                                          </td>
                                                          <td>
                                                              &nbsp;</td>
                                                      </tr>
                                                  </table>
                                              </td>
                                          </tr>
                                          
                                      </tr>
                                  </table>
                              </asp:Panel>                          
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  <asp:Button ID="btnPre" runat="server" Text="Previous" Width="70px" 
                                                                      ValidationGroup="vgPre" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Next" Width="70px" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                                  <asp:ObjectDataSource ID="odsEmpStatus" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="LeedsCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="EMPLOYMENT_STATUS" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsEmployers" runat="server" 
                                      SelectMethod="SelectEmployers" 
                                      TypeName="LeedsCU.DataAccess.Layer.clsSystemData">
                                  </asp:ObjectDataSource>
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>



