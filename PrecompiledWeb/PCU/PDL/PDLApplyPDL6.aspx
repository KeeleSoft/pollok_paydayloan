﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_PDLApplyPDL6, App_Web_oc53n32s" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="26" valign="middle" class="BodyText">
              <p>This loan application form must be fully completed. Failure to disclose all required information may delay approval of your loan application.</p>
              <br /><p><strong>Please note: <span class="style82">ALL</span> fields are required except where stated, thank you.</strong></p></td>
          </tr>
          <tr>
            <td height="26" valign="middle" class="BodyText">&nbsp;</td>
          </tr>
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText"><table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                            <td><strong>Please enter the security code sent to your mobile.</strong>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                          <table style="width:100%;" __designer:mapid="276">
                                              <tr __designer:mapid="277">
                                                  <td width="40%" __designer:mapid="278">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="279">
                                                      &nbsp;</td>
                                              </tr>
                                              <tr __designer:mapid="27a">                                                  
                                                  <td __designer:mapid="27d">
  &nbsp;Security Code:<asp:CompareValidator ID="CompareValidator4" runat="server" 
                                                                      ControlToValidate="txtSecurityCode" 
                                                                      ErrorMessage="Invalid security code" Operator="GreaterThan" 
                                                                      ValueToCompare="100000">*</asp:CompareValidator>
                                                              </td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtSecurityCode" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;Confirm Security Code:<asp:CompareValidator ID="CompareValidator5" runat="server" 
                                                                      ControlToValidate="txtSecurityCodeConfirm" 
                                                                      ErrorMessage="Invalid confirmed security code" Operator="GreaterThan" 
                                                                      ValueToCompare="100000">*</asp:CompareValidator>
                                                              </td>
                                                  <td __designer:mapid="27d">
                                  <asp:TextBox ID="txtSecurityCodeConfirm" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                                                                            </td>
                                              </tr>
                                              <tr __designer:mapid="27a">
                                                  <td __designer:mapid="27b">
                                                      &nbsp;</td>
                                                  <td __designer:mapid="27d">
                                                      &nbsp;</td>
                                              </tr>
                                              </table>
                              </td>
                          </tr>                          
                          <tr><td>
                                          <asp:LinkButton ID="btnReSend" runat="server">Did not recieve the code? Click 
                                          here to re-send.</asp:LinkButton>
                              </td></tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  <asp:Button ID="btnPre" runat="server" Text="Previous" Width="70px" 
                                                                      ValidationGroup="vgPre" BackColor="#00a79d" Font-Bold="True" 
                                                                      Enabled="False" />
                                                              </td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Next" Width="70px" BackColor="#00a79d" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                                  <asp:HiddenField ID="hfAttempt" runat="server" Value="0" />
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>



