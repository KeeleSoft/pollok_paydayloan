﻿<%@ page title="" language="VB" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_PDLAutoLoanWorldPaySetup, App_Web_oc53n32s" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText"><table width="466" height="159" border="0" cellpadding="0" cellspacing="0" bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                      <table style="width:100%;">
                          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          &nbsp;</td>
                                  </tr>
                              </table>
                              </td></tr>
                          <tr>
                            <td><strong>We will pass your application to WorldPay to make a 1 pence payment to verify your card details. Please note we will not store your card details and WorldPay will process this transaction.</strong>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                          &nbsp;</td>
                          </tr>                          
                          <tr><td>
                                          &nbsp;</td></tr>
                          <tr><td>
                              <img alt="" src="../Images/Worldpay.png" /></td></tr>
                          <tr><td>&nbsp;</td></tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  &nbsp;</td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Proceed" Width="70px" 
                                                                      BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                   <br />
                            <asp:Image ID="Image1" runat="server" Height="30px" ImageUrl="~/Images/VISA.gif" Width="51px" />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/visa_electron.gif" Width="44px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image5" runat="server" Height="29px" ImageUrl="~/Images/amex-logo2.gif" Width="58px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image6" runat="server" Height="30px" ImageUrl="~/Images/JCB.gif" Width="59px" />
                            <br />
                               <br />
                               <h3>Terms and conditions :</h3> 
                             <asp:HyperLink ID="hlTC" runat="server" 
                                                        NavigateUrl="~/PDL/Docs/TC_LCCU.pdf" Target="new">Please click here 
                                                    to view Terms and conditions.</asp:HyperLink> <br /> <br />
                            Refund policy: Refunds are by discrete management final decision. <br /> <br />
                            <h3>Contact details :</h3>
                            Pollok Credit Union <br />
                            Main line: 0141 881 8731 <br />
                            Fax: 0141 881 8731 <br />
                            Address: 140 Woodhead road, Glasgow, G53 7NN <br />
                            Email: info@pollokcu.com <br /> 
                            Web: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.pcu.org.uk" Target="new">www.pcu.org.uk</asp:HyperLink> <br />
                            <br />           

                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                              </td>
                          </tr>
                          
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>

</asp:Content>

