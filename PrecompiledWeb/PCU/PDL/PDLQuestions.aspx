﻿<%@ page language="VB" masterpagefile="~/PDL/PDLMasterPage.master" autoeventwireup="false" inherits="PDL_PDLQuestions, App_Web_oc53n32s" title="Untitled Page" stylesheettheme="Grey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                        <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr __designer:mapid="d96">
                                <td __designer:mapid="d97">
                                    <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                        CssClass="summaryerrors" />
                                </td>
                            </tr>
                            <tr __designer:mapid="d9a">
                                <td __designer:mapid="d9b">
                                    <asp:Panel ID="pnlQ0" runat="server">
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="70%">
                                                    &nbsp;Have you had a CU payday loan before?<asp:RequiredFieldValidator 
                                                        ID="RequiredFieldValidator4" runat="server" ControlToValidate="radQ0" 
                                                        ErrorMessage="Please answer the question">*</asp:RequiredFieldValidator>
                                                </td>                                                <td>
                                                    <asp:RadioButtonList ID="radQ0" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnQ0Next" runat="server" Text="Next" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ0Yes" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td>
                                                    This means you already have got the user login details and we could fast track your request. <br /> Please get the logging information ready and <asp:LinkButton ID="lbLogin" runat="server">Click Here.</asp:LinkButton></td>                                                
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ1" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="70%">
                                                    &nbsp;Do you live or work in Southwark, Lambeth or Westminster?<asp:RequiredFieldValidator 
                                                        ID="RequiredFieldValidator1" runat="server" ControlToValidate="radQ1" 
                                                        ErrorMessage="Please answer the question">*</asp:RequiredFieldValidator>
                                                </td>                                                <td>
                                                    <asp:RadioButtonList ID="radQ1" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnQ1Next" runat="server" Text="Next" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ1No" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td>
                                                    Our payday loan facility is only available to those who live or work in Southwark, Lambeth or Westminster. If you want to know your nearest credit union please <a href="http://www.abcul.org" target="_blank">click here</a> - Sorry that we cannot help you in this occasion</td>                                                
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ2" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="70%">
                                                    &nbsp;Are you a member of the Pollok City Credit Union?<asp:RequiredFieldValidator 
                                                        ID="RequiredFieldValidator2" runat="server" ControlToValidate="radQ2" 
                                                        ErrorMessage="Please answer the question">*</asp:RequiredFieldValidator>
                                                </td>                                                <td>
                                                    <asp:RadioButtonList ID="radQ2" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnQ2Next" runat="server" Text="Next" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ2No" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td>
                                                    To apply for a loan you must be a member and must live or work in the London Borough of Southwark, Lambeth or Westminster, if you do live in Southwark or Lambeth please complete a membership form before continuing with this loan application if not we are very sorry but as you do not live or work in the London Borough of Southwark or Lambeth you cannot join the Pollok City Credit Union. If you would like to join a Credit Union in your area, please call the Association of British Credit Unions on 0161-832-3694 who will be able to help you; alternatively please see their website <a href="http://www.abcul.org" target="_blank">www.abcul.org</a>  for further information</td>                                                
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ3" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="70%">
                                                    &nbsp;Are you employed?<asp:RequiredFieldValidator 
                                                        ID="RequiredFieldValidator3" runat="server" ControlToValidate="radQ3" 
                                                        ErrorMessage="Please answer the question">*</asp:RequiredFieldValidator>
                                                </td>                                                <td>
                                                    <asp:RadioButtonList ID="radQ3" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem>Yes</asp:ListItem>
                                                        <asp:ListItem>No</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnQ3Next" runat="server" Text="Next" Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlQ3Answer" runat="server" Visible="False">
                                        <table style="width:100%;">
                                            <tr>
                                                <td>
                                                    <asp:Literal ID="litQ3Answer" runat="server"></asp:Literal>
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkAgree" runat="server" 
                                                        Text="I agree to send the above documents to the credit union" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td width="70%">
                                                                &nbsp;</td>
                                                            <td>
                                                                <asp:Button ID="btnQ3AnswerNext" runat="server" Text="Next" Width="70px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr __designer:mapid="da8">
                                <td __designer:mapid="da9">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:Content>

