﻿<%@ page language="VB" trace="False" masterpagefile="~/PDL/PDLMasterPage2.master" autoeventwireup="false" inherits="PDL_PDLReConfirm, App_Web_oc53n32s" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMainBody" Runat="Server">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                                <td align="left">
                                                    <h2>Your Loan Application</h2></td>                                                
                                            </tr>
                            <tr>
                                <td>
                                            <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->                    
          <tr>
            <td height="10" valign="middle" bordercolor="#BABBBD" class="BodyText">
                <table width="500" border="0" cellpadding="0" cellspacing="0" 
                    bordercolor="#BBBCBE">
                <tr>
                  <td width="506" height="300" valign="top">
                  <table width="466" border="0" cellpadding="0" cellspacing="0">
          <!-- TemplateBeginEditable name="Main body" -->
          <tr><td>
                              <table style="width: 100%;">
                                  <tr>
                                      <td>
                                          <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                                      </td>
                                  </tr>
                              </table>
                              </td></tr>
          <tr>
            <td height="27" bordercolor="#00CCFF" bgcolor="#0091CA"><span class="Button"><span class="style2"><span class="SectionHeader style37 style61"><strong><a name="Deposit"></a>&nbsp;&nbsp;A) Your declaration</strong></span></span></span></td>
          </tr>
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td height="15"><strong>By selecting &ldquo;Submit&rdquo; below you   are giving the following declaration and consent:</strong><br>
              <br>
              I DECLARE, TO THE BEST OF MY KNOWLEDGE AND BELIEF, THAT: <br>
  <br>
              1. I am not indebted to any other Credit Union, Bank or Loan Agency either as a borrower or a Guarantor, except as stated on this application form. <br /><br />
              2. To the best of my knowledge and belief<br /><br />
              <table width="462" border="0">
                  <tr>
                    <td width="150" valign="middle">
                                          <asp:RadioButtonList ID="radGoodHealth" runat="server" 
                                              RepeatDirection="Horizontal" Font-Bold="True" Font-Size="Small" 
                                              ForeColor="Black">
                                              <asp:ListItem Selected="True">I am</asp:ListItem>
                                              <asp:ListItem>I am not</asp:ListItem>
                                          </asp:RadioButtonList></td>
                    <td width="285" valign="middle"><strong>- in good health</strong></td>
                    </tr>
                  <tr valign="middle">
                    <td> 
                                          <asp:RadioButtonList ID="radNormalOccupation" runat="server" 
                                              RepeatDirection="Horizontal" Font-Bold="True" Font-Size="Small">
                                              <asp:ListItem Selected="True" Value="1">I am fit</asp:ListItem>
                                              <asp:ListItem Value="0">I am not fit</asp:ListItem>
                                          </asp:RadioButtonList> </td>
                    <td><strong>-</strong> <strong>to follow my normal occupation</strong></td>
                    </tr>
                </table>
              3. I authorise the credit union to obtain any further information as   required from my employer or credit reference agency.<br>
                <br>
                4. ALL STATEMENTS CONTAINED IN THIS APPLICATION ARE TRUE, COMPLETE AND   ACCURATE&nbsp; <br>
                5. I acknowledge that if any information given in this application   proves incomplete or inaccurate, any loan to me as a result of this   application may become immediately repayable to the Credit Union.<br>
  <br>
                6. I understand that the provision of false information is fraud and that the   credit union may take appropriate action if I am found to have deliberately   provided false or misleading information.<br>
  <br>
                7. I understand that in the event of loss of my income / employment due to an   accident, redundancy or sickness, I will still be required to pay my agreed   loan repayments.<br>
                8. I understand that if Pollok Credit Union do not obtain a   satisfactory result from their credit information searches then they will not   advance the loan to me and Pollok Credit Union will have no further   obligation to me in relation to the loan applied for.<br>
                <br>
              <br>
            </td>
          </tr>
          <tr>
            <td height="27" bordercolor="#00CCFF" bgcolor="#0091CA"><span class="Button"><span class="style2"> <span class="style30 style67">&nbsp;&nbsp;</span><span class="SectionHeader style37 style61"><strong><a name="Deposit"></a>B) Your information and credit reference &amp; fraud prevention agencies consent</strong></span></span></span></td>
          </tr>
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td height="15"><strong>By selecting &ldquo;Submit&rdquo; below you   are giving us your consent to use your personal and financial information in   the ways set out in full in our <a href="Docs/TC_LCCU.pdf" target="_blank">Terms and Conditions.</a>&nbsp; </strong><br /><br />
              (You will   need Adobe Acrobat Reader to view the documents. You can get Acrobat <a href="http://get.adobe.com/reader/" target="_blank">here</a>. )<br /><br />
              We would   recommend that you save and print a copy of both the Loan Agreement and the Terms   and Conditions for your reference.<br /><br />
              <strong>Please read our Terms and   Conditions carefully.</strong><br /><br />
              In summary:<br /><br />
              1) When you apply to us to open an   account or apply for Credit, the Pollok Credit Union will check the   following records about you and others (see 2 below)<br>
                a) Our own;<br>
                b) Those at   credit reference agencies (CRAs). When CRAs receive a search from us they   will place a search footprint on your credit file that may be seen by other   lenders. They supply to us both public (including the electoral register) and   shared credit and fraud prevention information.<br>
                c) Those at   fraud prevention agencies (FPAs).
                We will make   checks such as; assessing this application for credit and verifying   identities to prevent and detect crime and money laundering. We may also make   periodic searches at CRAs and FPAs to manage your account with us.<br /><br />
              2) If you are   making a joint application or tell us that you have a spouse or financial   associate, we will link your records together so you must be sure that you   have their agreement to disclose information about them. CRAs also link your   records together and these links will remain on your and their files until   such time as you or your partner successfully files for a disassociation with   the CRAs to break that link.<br /><br />
              3) Information   on applications will be sent to CRAs and will be recorded by them. Where you   borrow from us, we will give details of your accounts and how you manage   it/them to CRAs. If you borrow and do not repay in full and on time, CRAs   will record the outstanding debt. This information may be supplied to other   organisations by CRAs and FPAs to perform similar checks and to trace your   whereabouts and recover debts that you owe. Records remain on file for 6   years after they are closed, whether settled by you or defaulted.<br /><br />
              4) If you give   us false or inaccurate information and we suspect or identify fraud we will   record this and may also pass this information to FPAs and other   organisations involved in crime and fraud prevention.<br /><br />
              5) If you have   borrowed from us and do not make payments that you owe us, we will trace your   whereabouts and recover debts.<br /><br />
              6) We and   other organisations may access and use from other countries the information   recorded by fraud prevention agencies.<br /><br />
              7) Your data   may also be used for other purposes for which you give your specific   permission or, in very limited circumstances, when required by law or where permitted under the terms of the Data Protection Act 1998.<br /><br />
              </td>
          </tr>
          <tr>
            <td height="27" bordercolor="#00CCFF" bgcolor="#0091CA"><span class="Button"><span class="style2"> <span class="style30 style67">&nbsp;&nbsp;</span><span class="SectionHeader style37 style61"><strong><a name="Deposit"></a>C) Agreement to terms and conditions of loan</strong></span></span></span></td>
          </tr>
          <tr>
            <td height="15">&nbsp;</td>
          </tr>
          <tr>
            <td height="15"><strong>Your application to Pollok Credit Union consists   of the application information (summarised below), the Declaration (A) and   Consent (B) (set out above ) together with our Loan Agreement and&nbsp; standard Terms and Conditions form the   agreement between us.&nbsp; </strong><br /><br />
              <strong>Before submitting your Application you must read and agree to our Loan Agreement and Terms and Conditions. </strong> <br /><br />
                <asp:HyperLink ID="hlAgreementLink1" runat="server" NavigateUrl="#" Target="_blank">You can read our Loan Agreement here</asp:HyperLink> <br /><br />
              <a href="Docs/TC_LCCU.pdf" target="_blank">You can read our Terms & Conditions here. (PDF 322K)</a><br /><br />
              (You will   need Adobe Acrobat Reader to view the documents. You can get Acrobat <a href="http://get.adobe.com/reader/" target="_blank">here</a>. )<br /><br />
              We would   recommend that you save and print a copy of both the Loan Agreement and the Terms   and Conditions for your reference.<br /><br />
              <strong>Application Summary</strong><br /><br />
              <table width="465" border="0" cellpadding="4" cellspacing="2">
                <tr>
                  <td width="261" valign="top" bgcolor="#F3F3F3">Loan Amount</td>
                  <td width="188" valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litLoanAmount" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litInterestDesc" runat="server"></asp:Literal></td>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litInterestTotal" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                  <td valign="top" bgcolor="#F3F3F3">Repayment period</td>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litRepayPeriod" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                  <td valign="top" bgcolor="#F3F3F3">Optional services<br>
                    Same day payment <br>                    
                  <td valign="top" bgcolor="#F3F3F3"><br>
                    <asp:Literal ID="litSameDayPay" runat="server"></asp:Literal><br>                    
                </tr>
                <tr>
                  <td valign="top" bgcolor="#F3F3F3">How we are going to deposit money into you current     account, if the loan is approved</td>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litHowDeposit" runat="server"></asp:Literal></td>
                </tr>
                <tr>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litFirstPaymentDesc" runat="server"></asp:Literal></td>
                  <td valign="top" bgcolor="#F3F3F3"><asp:Literal ID="litFirstPayment" runat="server"></asp:Literal></td>
                </tr>
            </table>
              The     charges for any optional services that you have selected as part of the     application process will be added to your loan repayment instalment(s).&nbsp; These charges include     those for faster application processing, faster payment service<strong>, </strong>repeat account facility<strong> and     one off payments of your membership and first savings </strong>to advance the loan to you [and are set out in your     loan application summary documentation]. <br>
              <br>
                <strong>Free, confidential  financial coaching is available to you via Epic 360. Epic 360 is an independent service across Glasgow to help you manage your money. If you live in Glasgow and your are 16yrs plus and would like Epic 360 to contact you, please tick the box.</strong>
              <br>
                <br>
                <table width="465" border="0">
                     <tr>
                  <td width="222"><strong><a href="http://www.epic360.co.uk" target="_blank">I want to be contacted by Epic360</a></strong></td>
                  <td width="233"><strong><asp:CheckBox ID="chkAgree6" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" Checked="True" /></strong></td>
                </tr>
                    </table>
                <br>
              <strong>If you do not &ldquo;tick&rdquo; the boxes below confirming your agreement to all   parts of the agreement between us then we cannot consider your application   for a loan.</strong><strong> </strong><br /><br />
              
              <table width="465" border="0">
                <tr>
                  <td><strong>I HAVE READ AND AGREE TO:</strong></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="222"><strong>the APPLICATION SUMMARY </strong></td>
                  <td width="233"><strong><asp:CheckBox ID="chkAgree1" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" />
                  </strong></td>
                </tr>
                <tr>
                  <td><strong>the DECLARATION (A) </strong></td>
                  <td><strong><asp:CheckBox ID="chkAgree2" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" /></strong></td>
                </tr>
                <tr>
                  <td><strong>the CONSENT (B)</strong></td>
                  <td><strong><asp:CheckBox ID="chkAgree3" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" /></strong></td>
                </tr>
                <tr>
                  <td><strong><asp:HyperLink ID="hlLoanAgreementCheck" runat="server" Target="_blank">the LOAN 
                      AGREEMENT</asp:HyperLink>
                      </strong></td>
                  <td><strong><asp:CheckBox ID="chkAgree4" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" /></strong></td>
                </tr>
                <tr>
                  <td><strong><a href="Docs/TC_LCCU.pdf" target="_blank">the TERMS AND CONDITIONS </a></strong></td>
                  <td><strong><asp:CheckBox ID="chkAgree5" runat="server" Text="I AGREE" 
                          TextAlign="Left" Font-Bold="True" Font-Size="Small" /></strong></td>
                </tr>
                 
              </table></td>
          </tr>
          <tr id="SagePayDescRow" runat="server">
            <td height="15"><strong>select the submit button to progress onwards to setup your loan repayments with your chosen bank account. Please have your bank card to hand</strong></td>
          </tr>
          <tr>
            <td height="25" valign="middle" class="BodyText">
              <p>&nbsp;</p>
<p>&nbsp;</p>
              <br>
                <a href="#Deposit">back to top</a><br>
                <br>
                <br>
                
              </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                      <table style="width:100%;">
                          
                          
                          <tr>
                              <td>&nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                                      <table __designer:mapid="164" border="0" cellpadding="0" cellspacing="0" 
                                                          style="width: 81%;">
                                                          <tr __designer:mapid="165">
                                                              <td __designer:mapid="166" width="80%">
                                                                  &nbsp;</td>
                                                              <td __designer:mapid="167" align="right">
                                                                  <asp:Button ID="btnNext" runat="server" Text="Submit" Width="100px" BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfLoanID" runat="server" />
                              </td>
                          </tr>
                      </table>
                    </td>
                </tr>
              </table>
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table>
                                </td>
                            </tr>
                            </table>
                    </asp:Content>


