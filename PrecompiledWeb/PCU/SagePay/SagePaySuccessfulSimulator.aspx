﻿<%@ page language="VB" autoeventwireup="false" inherits="SagePay_SagePaySuccessfulSimulator, App_Web_k5gc5osw" stylesheettheme="Grey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


.formTable
{
    width: 100%;
    border: 0px none;
	border-collapse: collapse;
}

.formTable td
{
	border-top: 0px none;
	border-right: 0px none;
	border-left: 0px none;
	border-bottom: 1px solid #d3d3d3;
    padding: 3px 10px 6px 3px;
	vertical-align: top;
}

td 
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color:  #666666;
}

.subheader 
{
    background-image: url('http://localhost:63715/images/yellowBlob.png');
    background-repeat: no-repeat;
    background-position: 0px 22px;
    padding: 20px 0px 8px 30px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 18px;
	color: #E98300;
}

.fieldLabel 
{
	font-family: Arial, Helvetica, sans-serif;
	color:#575C53;
	font-size:14px;
	font-weight:normal;
	text-align: right;
	width: 33%;
}	

.fieldData 
{
	width: 70%;
}

.warning 
{
	list-style-image:url('http://localhost:63715/images/form_bullet.gif');
	font-size: 12px;
	color: #CC0000;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="formTable" width="60%">
            <tr>
                <td colspan="2">
                    <div class="subheader">
                        Details sent back by Form</div>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Reference is:</td>
                <td class="fieldData">
&nbsp;<strong><asp:Label ID="lblVendorTxCodeReference" runat="server" Text="Label"></asp:Label>
                    </strong>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    VendorTxCode:</td>
                <td class="fieldData">
                    <asp:Label ID="lblVendorTxCode" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Status:</td>
                <td class="fieldData">
                    <asp:Label ID="lblStatus" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Status Detail:</td>
                <td class="fieldData">
                    <asp:Label ID="lblStatusDetail" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Amount:</td>
                <td class="fieldData">
                    <asp:Label ID="lblAmount" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    VPSTxId:</td>
                <td class="fieldData">
                    <asp:Label ID="lblVPSTxId" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    VPSAuthCode (TxAuthNo):</td>
                <td class="fieldData">
                    <asp:Label ID="lblVPSAuthCode" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    AVSCV2 Result:</td>
                <td class="fieldData">
                    <asp:Label ID="lblAVSCV2Result" runat="server" CssClass="smalltext" 
                        Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Gift Aid Transaction?:</td>
                <td class="fieldData">
                    <asp:Label ID="lblGiftAid" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    3D-Secure Status:</td>
                <td class="fieldData">
                    <asp:Label ID="lbl3DSecure" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    CAVV:</td>
                <td class="fieldData">
                    <asp:Label ID="lblCAVV" runat="server" Text="Label"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    CardType:</td>
                <td class="fieldData">
                    <asp:Label ID="lblCardType" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    Last4Digits:</td>
                <td class="fieldData">
                    <asp:Label ID="lblLast4Digits" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    AddressStatus:</td>
                <td class="fieldData">
                    <span style="float:right; font-size: smaller;">&nbsp;*PayPal transactions only</span><asp:Label 
                        ID="lblPPAddressStatus" runat="server" text="" />
                </td>
            </tr>
            <tr>
                <td class="fieldLabel">
                    PayerStatus:</td>
                <td class="fieldData">
                    <span style="float:right; font-size: smaller;">&nbsp;*PayPal transactions only</span><asp:Label 
                        ID="lblPPPayerStatus" runat="server" text="" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
