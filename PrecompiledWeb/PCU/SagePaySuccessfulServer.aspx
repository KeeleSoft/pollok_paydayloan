﻿<%@ page title="" language="VB" autoeventwireup="false" inherits="SagePaySuccessfulServer, App_Web_qcnk5q40" stylesheettheme="Grey" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc4" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Leeds City Credit Union - Online Services</title>   
<link href="scu2.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/JavaScript">
<!--
    function MM_preloadImages() { //v3.0
        var d = document; if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
        }
    }
//-->
</script>
</head>
<body>
<form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    </div>
    <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="240">&nbsp;</td>
        <td width="177">&nbsp;</td>
        <td width="443" valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="Images/scu logo2.jpg" width="200" height="100"></td>
        <td colspan="2"><div align="right" class="Button style15">
            <table width="400" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="400" height="60" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td width="390"><div align="right" class="Button"><a href="http://www.creditunion.co.uk/About%20us.htm" class="Button">About us </a> | <a href="http://www.creditunion.co.uk/Contact.htm" class="Button">Contact</a> | <a href="http://www.creditunion.co.uk/Downloads.htm" class="Button">Downloads</a> | <a href="http://www.creditunion.co.uk/News.htm" class="Button">News | </a><a href="#" class="Button"></a><span class="style21"><a href="#" class="Button"><span class="style13"></span></a><span class="style13"><a href="http://www.creditunion.co.uk/Governance.htm" class="Button">Governance</a> | <a href="http://www.creditunion.co.uk/CSR.htm" class="Button">CSR</a> | <a href="http://www.creditunion.co.uk/Legal.htm" class="Button">Legal</a> </span></span></div></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td width="400">&nbsp;</td>
              </tr>
              <tr>
                <td width="400"><div align="right" class="style34 style35">
                    <div align="right" class="style58 style59 style85 style86 style71">Altogether better </div>
                </div></td>
              </tr>
            </table>
        </div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30" valign="bottom"><table width="879" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="21">&nbsp;</td>
        <td width="858"><br>
          .............................................................................................................................................................................................................................................................................................<br>
          <br>
          <strong>Online Services<br>
          </strong>.............................................................................................................................................................................................................................................................................................<strong><br>
          <br>
          </strong></td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td height="15">&nbsp;</td>
  </tr>
  
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="130" valign="top">
                <uc1:MenuLogged ID="MenuLogged1" runat="server" />
                <uc4:MenuPreLogged ID="MenuPreLogged1" runat="server" />
            </td>
            <td width="23">
                &nbsp;</td>
            <td valign="top">
                <table width="466" border="0" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
                    <tr>
                        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5">
                            <span class="style37 style80">&nbsp;Thank you for your payment</span></td>
                    </tr>
                    <tr>
                        <td height="25" valign="middle" class="Button style5">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td height="10" valign="middle" class="BodyText">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlOK" runat="server">
                                            <strong>Thank you for your payment and your payment has been successful. The 
                                            unique reference for this transaction is:
                                            <asp:Literal ID="litRef" runat="server"></asp:Literal>
                                            </strong>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="pnlNotOK" runat="server" Visible="False">
                                            <strong>There was an error processing your payment. Please contact LCCU.</strong></asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br>Leeds City Credit Union will never ask you for your memorable data or Password in an e-mail. Never disclose this information to anyone.<br />
                            <br />For security reasons, when you have finished using Internet Banking always select LOG OUT.
                        </td>
                    </tr>
          <!-- TemplateEndEditable -->
                </table>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>

    </td>        
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="10" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30"><div align="center">.............................................................................................................................................................................................................................................................................................</div></td>
      </tr>
      <tr>
        <td height="30" valign="top" class="BodyText1 style23 style44 style13 style66 style56">
            <table width="95%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="60%"><span class="style84">Leeds City Credit Union is authorised and regulated by the Financial Services Authority (FSA) </span></td>
              <td align="right">
                <div align="right"><span class="style61 style83"><strong> Providing ethical financial services since 1982 </strong></span> </div></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>


