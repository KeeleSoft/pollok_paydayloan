﻿<%@ page title="" language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="Staff_StaffALNewApplication, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<%@ Register src="../AutoLoan/AutoLoanNewApplication.ascx" tagname="AutoLoanNewApplication" tagprefix="uc1" %>
<%@ Register src="../AutoLoan/AutoLoanAppeal.ascx" tagname="AutoLoanAppeal" tagprefix="uc2" %>

<%@ Register src="../AutoLoan/AutoLoanMessages.ascx" tagname="AutoLoanMessages" tagprefix="uc6" %>

<%@ Register src="../AutoLoan/AutoLoanPaymentDetails.ascx" tagname="AutoLoanPaymentDetails" tagprefix="uc7" %>

<%@ Register src="../AutoLoan/AutoLoanOptions.ascx" tagname="AutoLoanOptions" tagprefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10" valign="top">
            &nbsp;</td>
            <td width="130">
                &nbsp;</td>
            <td width="15">
                &nbsp;</td>
            <td valign="top">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="10" valign="top">
            &nbsp;</td>
        </tr>
        <tr>
            <td width="10" valign="top">
            &nbsp;</td>
            <td valign="top">
                <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />                
            </td>
            <td>
                &nbsp;</td>
            <td valign="top">
                <uc1:AutoLoanNewApplication ID="AutoLoanNewApplicationControl" runat="server" />
                <uc2:AutoLoanAppeal ID="AutoLoanAppealControl" runat="server" Visible="False" />
                <uc6:AutoLoanMessages ID="AutoLoanMessagesControl" runat="server" Visible="False" />
                <uc7:AutoLoanPaymentDetails ID="AutoLoanPaymentDetailsControl" runat="server" Visible="False" />
                <uc8:AutoLoanOptions ID="AutoLoanOptionsControl" runat="server" Visible="False" />
                
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="10" valign="top">
            &nbsp;</td>
        </tr>
    </table>
</asp:Content>

