﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffAutoLoanReCalc, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" Visible="False" />            
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Auto Loan </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td width="40%">
                            Enter required loan amount:<asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLoanAmount" 
                                ErrorMessage="Loan amount required">*</asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfMemberID" runat="server" />
                            <asp:HiddenField ID="hfLoanID" runat="server" />
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtLoanAmount" runat="server" Width="120px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnRecalc" runat="server" Text="Re Calculate" />
                        </td>
                    </tr>
                </table>
                </td>
          </tr>
          <tr id="AllOptions" runat="Server" visible="false">
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Select Option</strong></td>
                    </tr>
                    <tr runat="server" id="OptionPDL" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litPDLText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionPDL" runat="server" Text="Select" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionNoSTLRequestedAmount" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litNoSTLRequestedAmountText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnNoSTLRequestedAmount" runat="server" Text="Select" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
                    <tr runat="server" id="OptionNoSTL" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litNoSTLText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionNoSTL" runat="server" Text="Select" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
                    <tr runat="server" id="OptionYesSTL" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litYesSTLText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionYesSTL" runat="server" Text="Select" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
                    <tr runat="server" id="OptionYesSTLRequestedAmount" visible="false">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litYesSTLRequestedAmountText" runat="server"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnOptionYesSTLReqAmount" runat="server" Text="Select" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
<tr runat="server" id="OptionCancel">
    <td valign="middle" class="BodyText">
        <table style="width:100%;">
            <tr>
                <td>
                    <table style="width:100%;">
                        <tr>
                            <td style="margin-left: 40px" width="70%">
                                <strong><asp:Literal ID="litCancelText" runat="server" 
                                    Text="Could not select any option. Cancel"></asp:Literal></strong>
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </td>
</tr>
          
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
              </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

