﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffAutoLoanReCalc2, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" Visible="False" />            
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Auto Loan </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td width="40%">
                            Enter required loan amount:<asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLoanAmount" 
                                ErrorMessage="Loan amount required">*</asp:RequiredFieldValidator>
                            <asp:HiddenField ID="hfLoanID" runat="server" />
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtLoanAmount" runat="server" Width="120px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="40%">
                            Select the term (months)</td>
                        <td width="30%">
                            <asp:DropDownList ID="ddlTerm" runat="server" Width="122px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="40%">
                            Or</td>
                        <td width="30%">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="40%">
                            Enter monthly repayment</td>
                        <td width="30%">
                            <asp:TextBox ID="txtRepayment" runat="server" Width="120px" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="40%">
                            Share to loan amount:</td>
                        <td width="30%">
                            <asp:TextBox ID="txtShareToLoan" runat="server" Width="120px">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnRecalc" runat="server" Text="Re Calculate" Width="110px" />
                        </td>
                    </tr>
                </table>
                </td>
          </tr>
          <tr id="AllOptions" runat="Server" visible="false">
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td width="40%">
                            &nbsp;</td>
                        <td width="30%">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            New loan amount:</td>
                        <td>
                            <asp:Literal ID="litAmount" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:HiddenField ID="hfLoanID2" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Existing balance:</td>
                        <td>
                            <asp:Literal ID="litBalance" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:HiddenField ID="hfCurrentRepayment" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Share to loan amount:</td>
                        <td>
                            <asp:Literal ID="litShare2Loan" runat="server"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Term:</td>
                        <td>
                            <asp:Literal ID="litTerm" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:HiddenField ID="hfMemberID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Current loan monthly payment:</td>
                        <td>
                            <asp:Literal ID="litCurrentRepayment" runat="server"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            New loan monthly payment:</td>
                        <td>
                            <asp:Literal ID="litNewRepayment" runat="server"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Total
                            monthly repayment:</td>
                        <td>
                            <asp:Literal ID="litRepayment" runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:HiddenField ID="hfMemberName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CUCA savings:</td>
                        <td>
                            <asp:Literal ID="litCUCASavings" runat="server"></asp:Literal>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="110px" />
                        </td>
                        <td>
                            <asp:Button ID="btnConfirm" runat="server" Text="Confirm" Width="110px" />
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

