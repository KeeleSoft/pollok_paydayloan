﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffEditAutoLoan, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            <uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" Visible="False" />
          </td>
        <td width="23">&nbsp;</td>
        <td valign="top">
                                    
                    
                <table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Auto Loan </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" 
                    ValidationGroup="vgSave" />
                <br/>
                                    
                </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width: 100%">
                    <tr>
                        <td width="20%">
                            Auto 
                            Loan Reference:</td>
                        <td width="30%">
                            <asp:Literal ID="litID" runat="server"></asp:Literal>
                        </td>
                        <td width="25%">
                            Member Number:</td>
                        <td width="25%">
                            <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Applicantion Type:</td>
                        <td>
                            <asp:Literal ID="litApplicantTypeDesc" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Loan Amount Granted:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litFinalLoanAmount" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Applied Date/Time:</td>
                        <td>
                            <asp:Literal ID="litRequestedDate" runat="server"></asp:Literal>
                        </td>
                        <td>
                            Term:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litFinalTerm" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Member Name:</td>
                        <td>
                            <asp:Literal ID="litApplicantName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Monthly payment:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litFinalPayment" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Loan outcome:</td>
                        <td>
                            <asp:Literal ID="litLoanOutcome" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Completed Level:</td>
                        <td>
                            <asp:Literal ID="litCompletedLevel" runat="server"></asp:Literal>/X</td>
                    </tr>
                    <tr>
                        <td>
                            Apply method:</td>
                        <td>
                            <asp:Literal ID="litApplyMethod" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Amount Requested:</td>
                        <td>
                            <asp:Literal ID="litLoanAmount" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            Email:</td>
                        <td>
                            <asp:Literal ID="litEmail" runat="server"></asp:Literal>

                        </td>
                        <td>
                            Mobile Number:</td>
                        <td>
                            <asp:Literal ID="litMobile" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            WorldPay AgreementID:</td>
                        <td>
                            <asp:Literal ID="litWorldPayment" runat="server"></asp:Literal>

                        </td>
                        <td>
                            Faster payment:</td>
                        <td>
                            <asp:Literal ID="litFasterPayment" runat="server"></asp:Literal>
                                                                    </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><strong>
                            <asp:Literal ID="litAppealReason" runat="server"></asp:Literal></strong>
                            </td>                        
                    </tr>
                    <tr>
                        <td>
                            Created By:</td>
                        <td>
                            <asp:Literal ID="litCreatedByName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Approved By:</td>
                        <td>
                            <asp:Literal ID="litApprovedByName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            Application Status:<asp:RequiredFieldValidator 
                                ID="RequiredFieldValidator16" runat="server" ControlToValidate="ddlStatus" 
                                ErrorMessage="Application status required" ValidationGroup="vgSave">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" DataSourceID="odsStatus" 
                                DataTextField="AppStatusDesc" DataValueField="AutoLoanAppStatusID" 
                                Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Paid By:</td>
                        <td>
                            <asp:Literal ID="litPaidByName" runat="server"></asp:Literal>
                                                                    </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>                    
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:ObjectDataSource ID="odsStatus" runat="server" 
                                SelectMethod="GetAllAutoLoanApplicationStatuses" 
                                TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                            </asp:ObjectDataSource>
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="vgSave" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;<strong>Application Links</strong></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width: 100%;">
                                <tr>
                                    <td width="20%">
                                        <asp:HyperLink ID="hlCollectSchedule" runat="server" Font-Bold="True" 
                                            Font-Size="Small" Target="_SageCollect">Collection Schedule</asp:HyperLink>
                                    </td>
                                    <td width="20%">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td align="center" valign="top" width="15%">
                                        <strong>One Third Status</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>Share To Loan Status</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>PayDay Loan Status</strong></td>
                                    <td align="center" valign="top" width="15%">
                                        <strong>WorldPay Status</strong></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Literal ID="litOneThirdDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litShareToLoanDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litPDLDesc" runat="server"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:Literal ID="litWPDesc" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;<strong>Manual Overrides:</strong></td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnReCalc" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Recalculate Loan" Width="120px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSendText" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Send a Text" Width="100px" />
                                    </td>
                                    <td style="margin-left: 40px">
                                        <asp:Button ID="btnFastPay" runat="server" BackColor="Red" 
                                            Height="50px" Text="Send To Faster Payment" Width="150px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSendReConfirmEmailForCUCA" runat="server" BackColor="Red" 
                                            Height="50px" Text="Force WorldPay Setup" Width="150px" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>                                
                                <tr>
                                    <td>
                                       <asp:Button ID="btnSendToBacsFile" runat="server" BackColor="#FF6600" 
                                            Height="50px" Text="Send To BACS File" Width="120px" visible="false"/></td>
                                    <td>
                                        &nbsp;</td>
                                    <td style="margin-left: 40px">
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>                                
                            </table>
                        </td>
                    </tr>
                    <tr id="rowSendText" runat="server" visible="false">
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="40%">
                                                    Mobile number:</td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" runat="server" Width="178px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtTextMessage" runat="server" Rows="3" TextMode="MultiLine" 
                                            Width="440px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width:100%;">
                                            <tr>
                                                <td width="40%">
                                                    &nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btnSendSMS" runat="server" Text="Send SMS" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                                                                                              
                    <tr>
                        <td colspan="4">
                                        <strong>PayDay Loan Rule Result</strong></td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                                        <asp:Literal ID="litPDLResult" runat="server"></asp:Literal>
                                    </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    
                    
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table style="width:100%;">
                                        <tr>
                                            <td bgcolor="#0068B6" class="Button style5">
                                                &nbsp;<span class="style37 style80">&nbsp;&nbsp;Notes:</span><asp:ObjectDataSource 
                                                    ID="odsNotes" runat="server" 
                                                    SelectMethod="GetAutoLoanNotes" 
                                                    TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter DefaultValue="0" Name="AutoLoanID" 
                                                            QueryStringField="ID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="gvNotes" runat="server" AllowSorting="True" 
                                                    AutoGenerateColumns="False" DataSourceID="odsNotes" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
                                                        <asp:BoundField DataField="StaffName" HeaderText="Created By" 
                                                            SortExpression="StaffName" />
                                                        <asp:BoundField DataField="CreatedDate" HeaderText="Created" 
                                                            SortExpression="CreatedDate" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No notes available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td width="200">
                                                            Notes:<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                                                ControlToValidate="txtNotes" CssClass="errormsg" ErrorMessage="note required" 
                                                                ValidationGroup="vgNote">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNotes" runat="server" Height="40px" MaxLength="400" 
                                                                style="margin-left: 0px" TabIndex="65" ValidationGroup="vgNote" 
                                                                Width="500px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            <asp:Button ID="btnAddNote" runat="server" Text="Add Note" 
                                                                ValidationGroup="vgNote" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;</td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr runat="server" id="SMSHistoryRow">
                        <td colspan="4">
                                    <table style="width:100%;" __designer:mapid="1">
                                        <tr __designer:mapid="2">
                                            <td bgcolor="#0068B6" class="Button style5" __designer:mapid="3">
                                                &nbsp;<span class="style37 style80" __designer:mapid="4">&nbsp;&nbsp;SMS 
                                                Communication History:</span><asp:ObjectDataSource 
                                                    ID="odsSMSHistory" runat="server" 
                                                    SelectMethod="GetAutoLoanSMSCommunicationHistory" 
                                                    TypeName="PollokCU.DataAccess.Layer.clsAutoLoan">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter DefaultValue="0" Name="AutoLoanID" 
                                                            QueryStringField="ID" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </td>
                                        </tr>
                                        <tr __designer:mapid="8">
                                            <td __designer:mapid="9">
                                                <asp:GridView ID="gvSMSHistory" runat="server" 
                                                    AutoGenerateColumns="False" DataSourceID="odsSMSHistory" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="InOutStatus" HeaderText="In\Out" />
                                                        <asp:BoundField DataField="MobileNumber" HeaderText="Mobile" />
                                                        <asp:BoundField DataField="TextMessage" HeaderText="Text Message" />
                                                        <asp:BoundField DataField="DateTimeStamp" HeaderText="Time Stamp" />
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No sms communication history available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr __designer:mapid="10">
                                            <td __designer:mapid="11">
                                                &nbsp;</td>
                                        </tr>
                                        </table>
                                </td>
                    </tr>
                </table>
                </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                &nbsp;</td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

