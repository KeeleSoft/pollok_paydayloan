﻿<%@ page title="" language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="Staff_StaffEditPDLLoanDetails, App_Web_kgsx03tj" stylesheettheme="Grey" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 80%;" class="BodyText">
        <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Pay Day Loan </span></td>
          </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                </td>
        </tr>
        <tr>
            <td>
                                  <asp:ObjectDataSource ID="odsTitle" runat="server" 
                                      SelectMethod="SelectSalutations" TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsMaritalStatus" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="MARITAL_STATUS" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  <asp:ObjectDataSource ID="odsResidency" runat="server" 
                                      SelectMethod="GetLookupFromCatCode" 
                                      TypeName="PollokCU.DataAccess.Layer.clsSystemData">
                                      <SelectParameters>
                                          <asp:Parameter DefaultValue="RESIDENCY_TYPE" Name="sCatCode" Type="String" />
                                      </SelectParameters>
                                  </asp:ObjectDataSource>
                                  </td>
        </tr>
        <tr>
            <td>
                      <table style="width:100%;">
                          <tr>
                              <td width="40%">
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td width="40%">
                                  &nbsp;WeeLoan Reference (ID):</td>
                              <td>
                                  <asp:Literal ID="litID" runat="server"></asp:Literal>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Member ID:</td>
                              <td>
                                  <asp:Literal ID="litMemberID" runat="server"></asp:Literal>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Title:<asp:RequiredFieldValidator ID="RequiredFieldValidator30" 
                                      runat="server" ControlToValidate="ddlTitle" 
                                      ErrorMessage="Title required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlTitle" runat="server" Width="208px" 
                                      DataSourceID="odsTitle" DataTextField="Title" DataValueField="SalutationID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;First name:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                      runat="server" ControlToValidate="txtFirstName" 
                                      ErrorMessage="First name required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Surname:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                                      runat="server" ControlToValidate="txtSurName" ErrorMessage="Surname required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtSurName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Middle name:</td>
                              <td>
                                  <asp:TextBox ID="txtMiddleName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Gender:</td>
                              <td>
                                  <asp:DropDownList ID="ddlGender" runat="server" Width="208px">
                                      <asp:ListItem Value="M">Male</asp:ListItem>
                                      <asp:ListItem Value="F">Female</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Date of birth:<asp:CompareValidator ID="CompareValidator3" runat="server" 
                                      ControlToValidate="ddlDay" ErrorMessage="Date of birth - Day required" 
                                      Operator="GreaterThan" ValueToCompare="0">*</asp:CompareValidator>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                      ControlToValidate="ddlMonth" ErrorMessage="Date of birth - Month required" 
                                      Operator="GreaterThan" ValueToCompare="0">*</asp:CompareValidator>
                                  <asp:RangeValidator ID="RangeValidator3" runat="server" 
                                      ControlToValidate="ddlYear" ErrorMessage="Date of birth - Year required" 
                                      MaximumValue="3000" MinimumValue="1800">*</asp:RangeValidator>
                              </td>
                              <td align="left">
                                  <table style="width:208px;" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                          <td>
                                              <asp:DropDownList ID="ddlDay" runat="server" Width="50px">
                                                  <asp:ListItem Value="0">DAY</asp:ListItem>
                                                  <asp:ListItem>1</asp:ListItem>
                                                  <asp:ListItem>2</asp:ListItem>
                                                  <asp:ListItem>3</asp:ListItem>
                                                  <asp:ListItem>4</asp:ListItem>
                                                  <asp:ListItem>5</asp:ListItem>
                                                  <asp:ListItem>6</asp:ListItem>
                                                  <asp:ListItem>7</asp:ListItem>
                                                  <asp:ListItem>8</asp:ListItem>
                                                  <asp:ListItem>9</asp:ListItem>
                                                  <asp:ListItem>10</asp:ListItem>
                                                  <asp:ListItem>11</asp:ListItem>
                                                  <asp:ListItem>12</asp:ListItem>
                                                  <asp:ListItem>13</asp:ListItem>
                                                  <asp:ListItem>14</asp:ListItem>
                                                  <asp:ListItem>15</asp:ListItem>
                                                  <asp:ListItem>16</asp:ListItem>
                                                  <asp:ListItem>17</asp:ListItem>
                                                  <asp:ListItem>18</asp:ListItem>
                                                  <asp:ListItem>19</asp:ListItem>
                                                  <asp:ListItem>20</asp:ListItem>
                                                  <asp:ListItem>21</asp:ListItem>
                                                  <asp:ListItem>22</asp:ListItem>
                                                  <asp:ListItem>23</asp:ListItem>
                                                  <asp:ListItem>24</asp:ListItem>
                                                  <asp:ListItem>25</asp:ListItem>
                                                  <asp:ListItem>26</asp:ListItem>
                                                  <asp:ListItem>27</asp:ListItem>
                                                  <asp:ListItem>28</asp:ListItem>
                                                  <asp:ListItem>29</asp:ListItem>
                                                  <asp:ListItem>30</asp:ListItem>
                                                  <asp:ListItem>31</asp:ListItem>
                                              </asp:DropDownList>
                                          </td>
                                          <td>
                                              <asp:DropDownList ID="ddlMonth" runat="server" Width="90px">
                                                  <asp:ListItem Value="0">MONTH</asp:ListItem>
                                                  <asp:ListItem Value="1">January</asp:ListItem>
                                                  <asp:ListItem Value="2">February</asp:ListItem>
                                                  <asp:ListItem Value="3">March</asp:ListItem>
                                                  <asp:ListItem Value="4">April</asp:ListItem>
                                                  <asp:ListItem Value="5">May</asp:ListItem>
                                                  <asp:ListItem Value="6">June</asp:ListItem>
                                                  <asp:ListItem Value="7">July</asp:ListItem>
                                                  <asp:ListItem Value="8">August</asp:ListItem>
                                                  <asp:ListItem Value="9">September</asp:ListItem>
                                                  <asp:ListItem Value="10">October</asp:ListItem>
                                                  <asp:ListItem Value="11">November</asp:ListItem>
                                                  <asp:ListItem Value="12">December</asp:ListItem>
                                              </asp:DropDownList>
                                          </td>
                                          <td align="right">
                                              <asp:DropDownList ID="ddlYear" runat="server" Width="55px">
                                                  <asp:ListItem Value="0">YEAR</asp:ListItem>                                                  
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                  </table>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Place of birth:<asp:RequiredFieldValidator ID="RequiredFieldValidator23" 
                                      runat="server" ControlToValidate="txtPlaceofBirth" 
                                      ErrorMessage="Place of birth required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtPlaceofBirth" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Mother&#39;s maiden name:<asp:RequiredFieldValidator ID="RequiredFieldValidator24" 
                                      runat="server" ControlToValidate="txtMotherName" 
                                      ErrorMessage="Mother's maiden name required">*</asp:RequiredFieldValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtMotherName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;<strong>Current address</strong></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                                                              <td __designer:mapid="d0b">
                                                                  &nbsp;Postcode:<asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                                                            runat="server" ControlToValidate="txtPostCode1" ErrorMessage="Invalid postcode" 
                                                            
                                                            
                                                                      
                                                                      
                                                                      ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$">*</asp:RegularExpressionValidator>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Postcode required">*</asp:RequiredFieldValidator>
                                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Invalid postcode" 
                                                            ValidationExpression="^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$" 
                                                            ValidationGroup="vgSearch">*</asp:RegularExpressionValidator>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                                            ControlToValidate="txtPostCode1" ErrorMessage="Postcode required" 
                                                            ValidationGroup="vgSearch">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td __designer:mapid="d10">
                                                                  <asp:TextBox ID="txtPostCode1" runat="server" Width="200px" AutoPostBack="True"></asp:TextBox>
                                                                  &nbsp;</td>
                                                          </tr>
                          <tr>
                                                              <td __designer:mapid="d14">
                                                                  &nbsp;Address line 1:<asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                                                            runat="server" ControlToValidate="txtAddLine1_1" 
                                                            ErrorMessage="Address line 1 required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td __designer:mapid="d16">
                                                                  <asp:TextBox ID="txtAddLine1_1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                          <tr>
                                                              <td __designer:mapid="d19">
                                                                  &nbsp;Address line 2:</td>
                                                              <td __designer:mapid="d1a">
                                                                  <asp:TextBox ID="txtAddLine2_1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                          <tr>
                                                              <td __designer:mapid="d1d">
                                                                  &nbsp;City:<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="txtCity1" ErrorMessage="City required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td __designer:mapid="d1f">
                                                                  <asp:TextBox ID="txtCity1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                          <tr>
                                                              <td __designer:mapid="d22">
                                                                  &nbsp;County:<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                            ControlToValidate="txtCounty1" ErrorMessage="County required">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td __designer:mapid="d24">
                                                                  <asp:TextBox ID="txtCounty1" runat="server" Width="200px"></asp:TextBox>
                                                              </td>
                                                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Time at the present address?<asp:CompareValidator 
                                                                      ID="CompareValidator5" runat="server" 
                                                                      ControlToValidate="ddlTimeYears" 
                                                                      ErrorMessage="No of years at address required" Operator="GreaterThanEqual" 
                                                                      ValueToCompare="0">*</asp:CompareValidator>
                                                                  <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                                                      ControlToValidate="ddlTimeMonths" 
                                                                      ErrorMessage="No of months at address required" Operator="GreaterThanEqual" 
                                                                      ValueToCompare="0">*</asp:CompareValidator></td>
                              <td align="left">
                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 50%;">
                                      <tr>
                                          <td>
                                              <asp:DropDownList ID="ddlTimeYears" runat="server" AutoPostBack="True" 
                                                  Width="65px">
                                              </asp:DropDownList>
                                          </td>
                                          <td align="right">
                                              <asp:DropDownList ID="ddlTimeMonths" runat="server" Width="90px">
                                                  <asp:ListItem Value="-1">MONTHS</asp:ListItem>
                                                  <asp:ListItem Value="0">0</asp:ListItem>
                                                  <asp:ListItem Value="1">1</asp:ListItem>
                                                  <asp:ListItem Value="2">2</asp:ListItem>
                                                  <asp:ListItem Value="3">3</asp:ListItem>
                                                  <asp:ListItem Value="4">4</asp:ListItem>
                                                  <asp:ListItem Value="5">5</asp:ListItem>
                                                  <asp:ListItem Value="6">6</asp:ListItem>
                                                  <asp:ListItem Value="7">7</asp:ListItem>
                                                  <asp:ListItem Value="8">8</asp:ListItem>
                                                  <asp:ListItem Value="9">9</asp:ListItem>
                                                  <asp:ListItem Value="10">10</asp:ListItem>
                                                  <asp:ListItem Value="11">11</asp:ListItem>
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;<strong>Previous address</strong></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                                                    <td __designer:mapid="d67">
                                                        &nbsp;Postcode:</td>
                                                    <td __designer:mapid="d6c">
                                                        <asp:TextBox ID="txtPostCode2" runat="server" Width="200px" AutoPostBack="True"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                          <tr>
                                                    <td __designer:mapid="d70">
                                                        &nbsp;Address line 1:</td>
                                                    <td __designer:mapid="d72">
                                                        <asp:TextBox ID="txtAddLine1_2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                          <tr>
                                                    <td __designer:mapid="d75">
                                                        &nbsp;Address line 2:</td>
                                                    <td __designer:mapid="d76">
                                                        <asp:TextBox ID="txtAddLine2_2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                          <tr>
                                                    <td __designer:mapid="d79">
                                                        &nbsp;City:</td>
                                                    <td __designer:mapid="d7b">
                                                        <asp:TextBox ID="txtCity2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                          <tr>
                                                    <td __designer:mapid="d7e">
                                                        &nbsp;County:</td>
                                                    <td __designer:mapid="d80">
                                                        <asp:TextBox ID="txtCounty2" runat="server" Width="200px"></asp:TextBox>
                                                    </td>
                                                </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  <strong>&nbsp;Additional &amp; Contact Details</strong></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td width="40%">
                                  &nbsp;Home telephone:<asp:RegularExpressionValidator ID="RequiredFieldValidator200" runat="server" ControlToValidate="txtHomeTel" 
                                      ErrorMessage="Invalid home telephone" 
                                      ValidationExpression="^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$">*</asp:RegularExpressionValidator>
                              </td>
                              <td>
                                  <asp:TextBox ID="txtHomeTel" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Mobile:<asp:RegularExpressionValidator ID="RegularExpressionValidator9" 
                                      runat="server" ControlToValidate="txtMobile" ErrorMessage="Invalid mobile" 
                                      ValidationExpression="^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$">*</asp:RegularExpressionValidator>
                                                                                            </td>
                              <td align="left">
                                  <asp:TextBox ID="txtMobile" runat="server" Width="200px"></asp:TextBox></td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Email:<asp:RequiredFieldValidator ID="RequiredFieldValidator9" 
                                      runat="server" ControlToValidate="txtEmail" ErrorMessage="Email required">*</asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                      ControlToValidate="txtEmail" ErrorMessage="Invalid email" 
                                      ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                              </td>
                              <td align="left">
                                  <asp:TextBox ID="txtEmail" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
        &nbsp;National Insurance:<asp:RequiredFieldValidator ID="RequiredFieldValidator10" 
                                                          runat="server" ControlToValidate="txtNI" 
                                                          ErrorMessage="National insurance number required">*</asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" 
                                  runat="server" ControlToValidate="txtNI" 
                                  ErrorMessage="Invalid NI number format" 
                                  ValidationExpression="[a-zA-Z]{1}[a-zA-Z]{1}[0-9]{6}[a-zA-Z]{0,1}">*</asp:RegularExpressionValidator>
                                                                                            </td>
                              <td>
                                  <asp:TextBox ID="txtNI" runat="server" Width="200px" MaxLength="9"></asp:TextBox>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  
                                  &nbsp;Marital status:<asp:RequiredFieldValidator ID="RequiredFieldValidator20" 
                                      runat="server" ControlToValidate="ddlMaritalStatus" 
                                      ErrorMessage="Marital status required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlMaritalStatus" runat="server" Width="208px" 
                                      DataSourceID="odsMaritalStatus" DataTextField="LookupCodeName" 
                                      DataValueField="LookupCodeID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;No of dependents:</td>
                              <td>
                                  <asp:DropDownList ID="ddlDependents" runat="server" Width="208px">
                                      <asp:ListItem>0</asp:ListItem>
                                      <asp:ListItem>1</asp:ListItem>
                                      <asp:ListItem>2</asp:ListItem>
                                      <asp:ListItem>3</asp:ListItem>
                                      <asp:ListItem>4</asp:ListItem>
                                      <asp:ListItem>5</asp:ListItem>
                                      <asp:ListItem>6</asp:ListItem>
                                      <asp:ListItem>7</asp:ListItem>
                                      <asp:ListItem>8</asp:ListItem>
                                      <asp:ListItem>9</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;Residency:<asp:RequiredFieldValidator ID="RequiredFieldValidator21" 
                                      runat="server" ControlToValidate="ddlResidency" 
                                      ErrorMessage="Residency required">*</asp:RequiredFieldValidator>
                              </td>
                              <td>
                                  <asp:DropDownList ID="ddlResidency" runat="server" Width="208px" 
                                      DataSourceID="odsResidency" DataTextField="LookupCodeName" 
                                      DataValueField="LookupCodeID">
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  <strong>Bank Account your income is paid into.</strong></td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                                              <td style="height: 16px" __designer:mapid="1055">
                                                  Current Account number:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator201" runat="server" 
                                                      ControlToValidate="txtAccountNumber" 
                                                      ErrorMessage="Account number required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top" style="height: 16px" __designer:mapid="1057">
                                                  <asp:TextBox ID="txtAccountNumber" runat="server" Width="200px" MaxLength="9"></asp:TextBox>
                                              </td>
                                          </tr>
                          <tr>
                                              <td __designer:mapid="105a">
                                                 Sort code:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator202" runat="server" 
                                                      ControlToValidate="txtSortCode" 
                                                      ErrorMessage="Sort code required">*</asp:RequiredFieldValidator>
                                              </td>
                                              <td valign="top" __designer:mapid="105c">
                                                  <asp:TextBox ID="txtSortCode" runat="server" Width="200px" MaxLength="6"></asp:TextBox>
                                              </td>
                                          </tr>
                          <tr>
                                              <td __designer:mapid="105f">
                                                  Bank name:</td>
                                              <td valign="top" __designer:mapid="1060">
                                                  <asp:TextBox ID="txtBankName" runat="server" Width="200px"></asp:TextBox>
                                                                                            </td>
                                          </tr>
                          <tr>
                                              <td __designer:mapid="1063">
                                                  Account Owner:</td>
                                              <td valign="top" __designer:mapid="1064">
                                                  <asp:DropDownList ID="ddlAccountOwner" runat="server" Font-Bold="True" 
                                                      Width="208px">
                                                      <asp:ListItem>Personal</asp:ListItem>
                                                      <asp:ListItem>Joint</asp:ListItem>                                                      
                                                  </asp:DropDownList>
                                              </td>
                                          </tr>
                          <tr>
                                              <td __designer:mapid="1069">
                                                  Account Opened:<asp:RangeValidator ID="RangeValidator4" runat="server" 
                                                      ControlToValidate="ddlTimeWithBankYears" 
                                                      ErrorMessage="Invalid no of years with bank" MaximumValue="2050" MinimumValue="1900" 
                                                      Type="Integer">*</asp:RangeValidator>
                                                  <asp:CompareValidator ID="CompareValidator6" runat="server" 
                                                                          ControlToValidate="ddlTimeWithBankMonths" 
                                                                          ErrorMessage="Invalid no of months time with bank" Operator="GreaterThan" 
                                                                          Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                                                                  </td>
                                              <td valign="top" __designer:mapid="106c" align="left">
                                                                  <table border="0" cellpadding="0" cellspacing="0" style="width: 50%;" 
                                                                      __designer:mapid="106d">
                                                                      <tr __designer:mapid="106e">
                                                                          <td __designer:mapid="106f">
                                                                              <asp:DropDownList ID="ddlTimeWithBankYears" runat="server" 
                                                                                  Width="65px" Font-Bold="True">
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                          <td align="right" __designer:mapid="1071">
                                                                              <asp:DropDownList ID="ddlTimeWithBankMonths" runat="server" 
                                                                                  Width="90px" Font-Bold="True">
                                                                                  <asp:ListItem Value="-1">MONTH</asp:ListItem>
                                                                                  <asp:ListItem Value="1">Jan</asp:ListItem>
                                                                                  <asp:ListItem Value="2">Feb</asp:ListItem>
                                                                                  <asp:ListItem Value="3">Mar</asp:ListItem>
                                                                                  <asp:ListItem Value="4">Apr</asp:ListItem>
                                                                                  <asp:ListItem Value="5">May</asp:ListItem>
                                                                                  <asp:ListItem Value="6">Jun</asp:ListItem>
                                                                                  <asp:ListItem Value="7">Jul</asp:ListItem>
                                                                                  <asp:ListItem Value="8">Aug</asp:ListItem>
                                                                                  <asp:ListItem Value="9">Sep</asp:ListItem>
                                                                                  <asp:ListItem Value="10">Oct</asp:ListItem>
                                                                                  <asp:ListItem Value="11">Nov</asp:ListItem>
                                                                                  <asp:ListItem Value="12">Dec</asp:ListItem>
                                                                              </asp:DropDownList>
                                                                          </td>
                                                                      </tr>
                                                                  </table>
                                                              </td>
                                          </tr>
                          <tr>
                              <td>
                                  Payment frequency: (To show in agreement)</td>
                              <td>
                                  <asp:DropDownList ID="ddlPayFrequency" runat="server" Width="208px">
                                      <asp:ListItem>MONTH</asp:ListItem>
                                      <asp:ListItem>WEEK</asp:ListItem>
                                      <asp:ListItem>FORTNIGHT</asp:ListItem>
                                      <asp:ListItem>4 WEEK</asp:ListItem>
                                  </asp:DropDownList>
                                                                                            </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  First repayment date:<asp:RequiredFieldValidator 
                                                      ID="RequiredFieldValidator18" runat="server" 
                                                      ControlToValidate="txtNextPayDay" 
                                                      ErrorMessage="Next pay day required">*</asp:RequiredFieldValidator>
                                              </td>
                              <td>
                                  <asp:TextBox ID="txtNextPayDay" runat="server" Width="200px"></asp:TextBox><asp:CalendarExtender
                                                      ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                      PopupPosition="Right" TargetControlID="txtNextPayDay" 
                                                      TodaysDateFormat="dd/MM/yyyy">
                                                  </asp:CalendarExtender></td>
                          </tr>
                          <tr>
                              <td>
                                  <asp:HiddenField ID="hfID" runat="server" />
                              </td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                                                  <asp:Button ID="btnNext" runat="server" Text="Submit" Width="100px" BackColor="#67AE3E" Font-Bold="True" />
                                                              </td>
                          </tr>
                          <tr>
                              <td>
                                  &nbsp;</td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          </table>
                              </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

