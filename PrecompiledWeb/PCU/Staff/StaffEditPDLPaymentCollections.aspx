﻿<%@ page title="" language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="Staff_StaffEditPDLPaymentCollections, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 80%;" class="BodyText">
        <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;WorldPay Payment Collection Schedule</span></td>
          </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                                              CssClass="summaryerrors" />
                </td>
        </tr>
        <tr>
            <td>
                                  <asp:Panel ID="pnlEditCollection" runat="server">
                                      <table style="width:100%;">
                                          <tr>
                                              <td>
                                                  <strong>Edit WorldPay Collection Schedule</strong></td>
                                          </tr>
                                          <tr>
                                              <td align="left">
                                                  <table style="width: 80%;">
                                                      <tr>
                                                          <td width="40%">
                                                              Sequence</td>
                                                          <td>
                                                              <asp:Literal ID="litSequence" runat="server"></asp:Literal>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Amount</td>
                                                          <td>
                                                              <asp:TextBox ID="txtAmount" runat="server" Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Schedule Date &amp; Time</td>
                                                          <td>
                                                              <asp:TextBox ID="txtScheduleTime" runat="server" Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Payment Taken?</td>
                                                          <td>
                                                              <asp:DropDownList ID="ddlPaymentTake" runat="server" Width="204px">
                                                                  <asp:ListItem>NO</asp:ListItem>
                                                                  <asp:ListItem>YES</asp:ListItem>
                                                              </asp:DropDownList>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Note</td>
                                                          <td>
                                                              <asp:TextBox ID="txtNote" runat="server" Rows="5" TextMode="MultiLine" 
                                                                  Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr id="ProcessedInfoRow" runat="server">
                                                          <td>
                                                              <strong>Collection Details</strong></td>
                                                          <td>
                                                              <asp:Literal ID="litCollectionDetails" runat="server"></asp:Literal>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              <asp:HiddenField ID="hfCollectionID" runat="server" />
                                                          </td>
                                                          <td>
                                                              <table style="width: 60%;">
                                                                  <tr>
                                                                      <td>
                                                                          <asp:Button ID="btnSaveCollection" runat="server" Text="Save" />
                                                                      </td>
                                                                      <td>
                                                                          <asp:Button ID="btnDeleteCollection" runat="server" BackColor="#CC3300" 
                                                                              Text="Delete" />
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </td>
                                                      </tr>
                                                  </table>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                      </table>
                                  </asp:Panel>
                                            </td>
        </tr>
        <tr>
            <td>
                                  <asp:Panel ID="pnlAddCollection" runat="server">
                                      <table style="width:100%;">
                                          <tr>
                                              <td>
                                                  <strong>Add WorldPay Collection Schedule Entry</strong></td>
                                          </tr>
                                          <tr>
                                              <td align="left">
                                                  <table style="width: 60%;">
                                                      <tr>
                                                          <td>
                                                              Amount</td>
                                                          <td>
                                                              <asp:TextBox ID="txtAmountNew" runat="server" Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Schedule Date &amp; Time</td>
                                                          <td>
                                                              <asp:TextBox ID="txtScheduleTimeNew" runat="server" Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Payment Taken?</td>
                                                          <td>
                                                              <asp:DropDownList ID="ddlPaymentTakeNew" runat="server" Width="204px">
                                                                  <asp:ListItem>NO</asp:ListItem>
                                                                  <asp:ListItem>YES</asp:ListItem>
                                                              </asp:DropDownList>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              Note</td>
                                                          <td>
                                                              <asp:TextBox ID="txtNoteNew" runat="server" Rows="5" TextMode="MultiLine" 
                                                                  Width="200px"></asp:TextBox>
                                                          </td>
                                                      </tr>
                                                      <tr>
                                                          <td>
                                                              <asp:HiddenField ID="hfCollectionIDNew" runat="server" />
                                                          </td>
                                                          <td>
                                                              <table style="width: 60%;">
                                                                  <tr>
                                                                      <td>
                                                                          <asp:Button ID="btnSaveNewCollection" runat="server" 
                                                                              Text="Save New Collection" />
                                                                      </td>
                                                                      <td>
                                                                          &nbsp;</td>
                                                                  </tr>
                                                              </table>
                                                          </td>
                                                      </tr>
                                                  </table>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  &nbsp;</td>
                                          </tr>
                                      </table>
                                  </asp:Panel>
                                            </td>
        </tr>
        <tr>
            <td>
                                                                          <asp:Button ID="btnAddNewCollection" 
                                      runat="server" Text="Add New Collection" />
                                  <asp:ObjectDataSource 
                                                    ID="odsCollectionSchedule" runat="server" 
                                                    SelectMethod="GetSageCollectionsSchedule" 
                                                    TypeName="PollokCU.DataAccess.Layer.clsSagePay">
                                                    <SelectParameters>
                                                        <asp:QueryStringParameter DefaultValue="0" Name="ApplicationID" 
                                                            QueryStringField="ID" Type="Int32" />
                                                        <asp:QueryStringParameter DefaultValue="PDL" Name="ApplicationType" 
                                                            QueryStringField="AppType" Type="String" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </td>
        </tr>
        <tr>
            <td>
                                                <asp:GridView ID="gvCalcActivity" runat="server" 
                                                    AutoGenerateColumns="False" 
                          DataSourceID="odsCollectionSchedule" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="SequenceID" HeaderText="Sequence" 
                                                            SortExpression="SequenceID" >
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Amount" HeaderText="Amount" 
                                                            SortExpression="Amount" >
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ScheduleDateTime" 
                                                            HeaderText="Schedule Date" />
                                                        <asp:BoundField DataField="PaymentTakenDesc" HeaderText="Payment Taken" 
                                                            SortExpression="PaymentTakenDesc" >
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AttemptCount" HeaderText="Attempted Count" >
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="LastAttemptDateTime" HeaderText="Last Attempted" />
                                                        <asp:BoundField DataField="Note" HeaderText="Note" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlEditCollection" runat="server" 
                                                                    ImageUrl="~/images/icon_edit.png" 
                                                                    NavigateUrl='<%# "StaffEditPDLPaymentCollections.aspx?AppType="& Request.QueryString("AppType") &"&ID="& Eval("ApplicationID") &"&CollectionID=" & Eval("SagePayCollectionID") %>' 
                                                                    Text="Edit/View PDL "></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No collection schedule available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

