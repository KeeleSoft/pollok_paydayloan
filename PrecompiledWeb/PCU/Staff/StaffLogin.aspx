﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffLogin, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="130" valign="top">
            &nbsp;</td>
        <td width="23">&nbsp;</td>
        <td valign="top"><table width="466" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Welcome to Pollok Credit Union Staff Services </span></td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText"><br/>
                    <span class="Button"><span class="style39">Staff login<br /><br /></span><asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlLogin" runat="server">
                                            <table border="0" cellpadding="0" cellspacing="0" width="465">
                                                <tr bgcolor="#D2F1FC">
                                                    <td class="BodyText" height="30" colspan="2">
                                                        <asp:Label ID="lblMessage" runat="server" CssClass="MemChar" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#D2F1FC">
                                                    <td class="BodyText" height="40" width="173">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login Name:&nbsp;
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="txtLoginName" ErrorMessage="Login name required">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td height="30" width="292">
                                                        <asp:TextBox ID="txtLoginName" runat="server" Width="149px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#D2F1FC">
                                                    <td class="BodyText" height="40">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password:
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtPassword" ErrorMessage="Password required">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td height="30">
                                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="149px"></asp:TextBox>
                                                        </td>
                                                </tr>
                                                <tr bgcolor="#D2F1FC">
                                                    <td class="BodyText" height="30">
                                                        &nbsp;</td>
                                                    <td height="30">
                                                        <asp:Button ID="btnLogin" runat="server" Text="Submit" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </span>
                
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="218" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

