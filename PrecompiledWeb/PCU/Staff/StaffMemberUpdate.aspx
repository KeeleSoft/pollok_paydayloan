﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffMemberUpdate, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10" valign="top">
        &nbsp;
        </td>
        <td>&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130" valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" />
            </td>
        <td valign="top">
            &nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">
                &nbsp;&nbsp;Change Member Details </span></td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    CssClass="errormsg" />
            </td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                    
                <table style="width:100%;">
                    <tr>
                        <td width="40%">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Member ID:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                runat="server" ControlToValidate="txtMemberID" 
                                ErrorMessage="Member ID required">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMemberID" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            Mobile:</td>
                        <td>
                            <asp:TextBox ID="txtMobile" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:Button ID="btnChange" runat="server" Text="Update" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                    
            </td>
          </tr>
          <tr>
            <td height="15" valign="middle" class="BodyText">
                    <p>For security reasons, when you have finished using staff services always select LOG OUT.</p>
            </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

