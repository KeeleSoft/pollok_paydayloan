﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffPDLoans, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;WeeLoans </span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table class="BodyText" style="width:100%;">
                    <tr>
                        <td>
                            Last faster payment time:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litLastFPTime" runat="server"></asp:Literal>
                        </td>
                        <td>
                            Next faster payment time:</td>
                        <td style="font-weight: 700">
                            <asp:Literal ID="litNextFPTime" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
              </td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table 
                    style="width:100%;" class="BodyText">
                <tr>
                    <td>
                        <strong>Search...</strong></td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        First name</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Surname</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSurName" runat="server" Width="150px"></asp:TextBox>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Completed Level</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlLevel" runat="server" Width="154px">
                                                                            <asp:ListItem></asp:ListItem>
                                                                            <asp:ListItem>1</asp:ListItem>
                                                                            <asp:ListItem>2</asp:ListItem>
                                                                            <asp:ListItem>3</asp:ListItem>
                                                                            <asp:ListItem>4</asp:ListItem>
                                                                            <asp:ListItem>5</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Status</td>
                                                                    <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" DataSourceID="odsStatus" 
                                DataTextField="AppStatusDescription" DataValueField="ApplicationStatusID" 
                                Width="154px">
                            </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                    <tr>
                                                                    <td>
                                                                        Partner applications</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlPartner" runat="server" Width="154px">
                                                                            
                                                                            <asp:ListItem>All</asp:ListItem>
                                                                            <asp:ListItem>PCU</asp:ListItem>
                                                                            <asp:ListItem>BCD</asp:ListItem>
                                                                           
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Epic 360</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlEpic" runat="server" Width="154px">
                                                                            
                                                                            <asp:ListItem>All</asp:ListItem>
                                                                            <asp:ListItem>Epic 360</asp:ListItem>
                                                                            <asp:ListItem>Non Epic 360</asp:ListItem>
                                                                           
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                            <asp:ObjectDataSource ID="odsStatus" runat="server" 
                                SelectMethod="GetAllPDLApplicationStatuses" 
                                TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                            </asp:ObjectDataSource>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
          </tr>
          <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Duplicated" BackColor="#FF3300" 
                    Font-Bold="True" ForeColor="White" Width="60px"></asp:Label>&nbsp;<asp:Label 
                    ID="Label2" runat="server" Text="BACS" BackColor="#009933" Font-Bold="True" 
                    ForeColor="White" Width="35px"></asp:Label></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsPDLLoans" runat="server" 
                    SelectMethod="GetAllPDLApplications" 
                     TypeName="PollokCU.DataAccess.Layer.clsStaffPayDayLoan">
                    <SelectParameters>
                    <asp:Parameter Name="FirstName" DefaultValue="" Type="String" />
                    <asp:Parameter Name="SurName" DefaultValue="" Type="String" />
                        <asp:Parameter DefaultValue="0" Name="Level" Type="Int16" />
                        <asp:Parameter DefaultValue="0" Name="ApplicationStatus" Type="Int32" />
                        <asp:Parameter Name="PartnerType" DefaultValue="" Type="String" />
                        <asp:Parameter Name="Epic360" DefaultValue="ALL" Type="String" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsPDLLoans" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True" 
                    OnRowDataBound="gvBatches_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="MemberID" HeaderText="Member#" 
                            SortExpression="MemberID" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AppliedDate" HeaderText="Applied On" 
                            SortExpression="AppliedDate" />
                        <asp:TemplateField HeaderText="Name" SortExpression="FullName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FullName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StaffName" HeaderText="Staff" 
                            SortExpression="StaffName" />
                        <asp:BoundField DataField="AppStatusDescription" 
                            HeaderText="Status" 
                            SortExpression="AppStatusDescription" />
                        <asp:BoundField DataField="CompletedLevel" HeaderText="Level" 
                            SortExpression="CompletedLevel">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server" 
                                    ImageUrl="~/images/icon_edit.png" 
                                    NavigateUrl='<%# "StaffEditPDLoan.aspx?ID=" & Eval("ID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                                <asp:HiddenField ID="hfDuplicated" runat="server" 
                                    Value='<%# Eval("DuplicatedApplication") %>' />
                                <asp:HiddenField ID="hfSuccessfulBacs" runat="server" 
                                    Value='<%# Eval("SuccessfulBacs") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlPDFDownload" runat="server" 
                                    ImageUrl="~/images/icon_pdfsmall.png" 
                                    NavigateUrl='<%# "..\PDL\PDLApps\" & Eval("ID") & "_" & Eval("SurName") & "_" & Eval("DOBShortFormat") & ".pdf" %>' Target="_blank" 
                                    Text="View PDF"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

