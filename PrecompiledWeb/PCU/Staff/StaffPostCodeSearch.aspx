﻿<%@ page validaterequest="false" language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffPostCodeSearch, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">
            &nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Enable/Disable Experian Address Search</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
              </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td>
                            &nbsp;<table style="width:80%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Join CU Online</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlRegister" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveRegister" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Become a Member</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlMember" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveMember" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Loan</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlLoan" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveLoan" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Current Account</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlCurrent" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveCurrent" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Rev Loan</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlRevLoan" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveRevLoan" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;<strong>Enable Address Change</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlAddress" runat="server">
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                            <asp:Button ID="btnSaveAddress" runat="server" Text="Save" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
                </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

