﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffReportSelector, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Select Report </span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText" width="70%"><br/>
                                    
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR1" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=1" Target="ReportPage">PDL- Calculator activity</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR2" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=2" Target="ReportPage">Credit Score & Q Score</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR3" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=3" Target="ReportPage">PDL- Monthly Income</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR4" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=4" Target="ReportPage">PDL- Job Title</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR5" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=5" Target="ReportPage">Q1 to 5</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR6" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=6" Target="ReportPage">PDL-genda</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR7" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=7" Target="ReportPage">Number of loan applications received by LMCU</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR8" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=8" Target="ReportPage">Repayment period & payday loans applied</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR9" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=9" Target="ReportPage">PDL-Age</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR10" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=10" Target="ReportPage">PDL- No of dependents</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hlR11" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=11" Target="ReportPage">Summary by Marital Status</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HyperLink ID="HyperLink1" runat="server"  
                                    NavigateUrl="StaffViewCrystalReport.aspx?RID=12" Target="ReportPage">Summary by Employment Status</asp:HyperLink>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                </table>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

