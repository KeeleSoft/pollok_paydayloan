﻿<%@ page validaterequest="false" language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffSiteContent, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="15">&nbsp;</td>
        <td valign="top">
            &nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td>&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Site Messages Control Panel</span></td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText">
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg"></asp:Label>
              </td>
          </tr>
          <!-- TemplateEndEditable -->
          <tr>
            <td valign="middle" class="BodyText">
                <table style="width:100%;">
                    <tr>
                        <td>
                            &nbsp;<table style="width:100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        &nbsp;<strong>Login Page - Message</strong></td>
                                    <td>
                                        <asp:DropDownList ID="ddlShowMsg1" runat="server">
                                            <asp:ListItem Value="False">Hide</asp:ListItem>
                                            <asp:ListItem Value="True">Show</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtMessage1" runat="server" MaxLength="2000" Rows="8" 
                                TextMode="MultiLine" Width="410px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" />
                        </td>
                    </tr>
                </table>
                </td>
          </tr>
          </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

