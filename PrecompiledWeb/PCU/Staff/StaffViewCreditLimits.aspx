﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffViewCreditLimits, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Credit Limits </span></td>
          </tr>          
          <tr>
            <td height="25" valign="middle">
                <table 
                    style="width:100%;" class="BodyText">
                <tr>
                    <td>
                        <strong>Search...</strong></td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        First name</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        Surname</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSurName" runat="server" Width="150px"></asp:TextBox>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Product with no limits</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlProductStatus" runat="server" Width="154px">
                                                                            <asp:ListItem Value="0">Select all</asp:ListItem>
                                                                            <asp:ListItem Value="1">Blank Pay day loan</asp:ListItem>
                                                                            <asp:ListItem Value="2">Blank rev loan</asp:ListItem>
                                                                            <asp:ListItem Value="3">Blank OD loan</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Member #</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtMemberID" runat="server" Width="150px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
          </tr>
          <tr>
            <td>
                &nbsp;</td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsCreditLimits" runat="server" 
                    SelectMethod="GetCreditLimitMemberList" 
                     TypeName="PollokCU.DataAccess.Layer.clsCreditLimits">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="False" Name="blankRevLoan" Type="Boolean" />
                        <asp:Parameter DefaultValue="False" Name="blankPDLLoan" Type="Boolean" />
                        <asp:Parameter DefaultValue="False" Name="blankODLoan" Type="Boolean" />
                        <asp:Parameter Name="FirstName" DefaultValue="" Type="String" />
                        <asp:Parameter Name="SurName" DefaultValue="" Type="String" />
                        <asp:Parameter Name="MemberID" DefaultValue="0" Type="Int32" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvMemberList" runat="server" AutoGenerateColumns="False" 
                    PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True" 
                    OnRowDataBound="gvBatches_RowDataBound" EnableModelValidation="True">
                    <Columns>
                        <asp:BoundField DataField="MemberID" HeaderText="Member#" 
                            SortExpression="MemberID" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Member Name" SortExpression="MemberFullName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("MemberFullName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MemberFullName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="RevLoanAmountLimit" HeaderText="ALPS" 
                            SortExpression="RevLoanAmountLimit" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="PDLRepeatAmountLimit" 
                            HeaderText="PDL" 
                            SortExpression="PDLRepeatAmountLimit" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="ODLoanAmountLimit" DataFormatString="{0:N0}" 
                            HeaderText="OD" SortExpression="ODLoanAmountLimit" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server" 
                                    ImageUrl="~/images/icon_edit.png" 
                                    NavigateUrl='<%# "StaffEditCreditLimits.aspx?MemberID=" & Eval("MemberID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

