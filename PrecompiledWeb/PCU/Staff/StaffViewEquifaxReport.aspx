﻿<%@ page title="" language="VB" masterpagefile="~/Staff/MasterPageHeaderOnly.master" autoeventwireup="false" inherits="Staff_StaffViewEquifaxReport, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width:100%;" class="BodyText">
        <tr>
        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Equifax Report - 
            <asp:Literal ID="litName" runat="server"></asp:Literal></span>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
        </tr>
        <tr>
            <td class="BodyText"><strong>Authentication: </strong><asp:Literal ID="litAuthStatus" runat="server"></asp:Literal></td>
        </tr>
         <tr>
            <td class="BodyText">
                <asp:Panel ID="Panel1" runat="server">
                    Error Text:&nbsp; <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                    </asp:Panel>
                 </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlAuth" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table class="BodyText" style="width:100%;">
                                    <tr ID="AuthErrorTableRow" runat="server">
                                        <td style="width: 122px">
                                            &nbsp;Non Address Data:</td>
                                        <td style="width: 163px">
                                            
                                        </td>
                                        <td style="width: 150px">
                                            &nbsp;</td>
                                        <td style="width: 169px">
                                            &nbsp;</td>
                                        <td style="width: 159px">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px; height: 20px;">
                                            Rolling register check:</td>
                                        <td style="width: 163px; height: 20px;">
                                            <asp:Literal ID="litVRC101" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px; height: 20px;">
                                            Name at Address on Full Electroral Roll:</td>
                                        <td style="height: 20px; width: 169px">
                                            <asp:Literal ID="litESC941" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px; height: 20px;">
                                            Name at Address on Rolling Electroral Register:</td>
                                        <td style="height: 20px">
                                            <asp:Literal ID="litRSC8" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px">
                                            Live Insight Accounts:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litQSC001" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            Number of CCJs last 6 years :</td>
                                        <td style="width: 169px">
                                            <asp:Literal ID="litCSC903" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 159px">
                                            Date of Birth Check:</td>
                                        <td>
                                            <asp:Literal ID="litFSC12" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px">
                                            Home Telephone Number Check:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litTSC1" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            Insight - Date of Birth Check:</td>
                                        <td style="width: 169px">
                                            <asp:Literal ID="litFSC3" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            HM Treasury Sanctions Match :</td>
                                        <td>
                                            <asp:Literal ID="litNSC1" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px">
                                            Senior Political Figures Match:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litNSC2" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            Office of Foreign Asset Control Check:</td>
                                        <td style="width: 169px">
                                            <asp:Literal ID="litHSC1" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            Halo Deceased Lvl 10:</td>
                                        <td>
                                            <asp:Literal ID="litDSC1" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px; height: 36px;">
                                            Halo Deceased Lvl 9:</td>
                                        <td style="width: 163px; height: 36px;">
                                            <asp:Literal ID="litDSC3" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px; height: 36px;">
                                            Halo Deceased Lvl 8:</td>
                                        <td style="width: 169px; height: 36px;">
                                            <asp:Literal ID="litDSC4" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px; height: 36px;">
                                            Halo Deceased Lvl 7:</td>
                                        <td style="height: 36px">
                                            <asp:Literal ID="litDSC5" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 122px">
                                            CIFAS (members only):</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litASC8" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            Pass/Fail Variant:</td>
                                        <td>
                                            <asp:Literal ID="litVXC3" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            Option:</td>
                                        <td>
                                            <asp:Literal ID="litOptions" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 122px">
                                            <strong>Number of Alerts:</strong></td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litVNC3" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            <strong>Number of Proofs of Residency:</strong></td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litVRC18" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            <strong>Number of Proofs of Identity:</strong></td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litVIC3" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                            <td style="width: 122px">&nbsp;</td>
                            
                        </tr>
                                    <tr>
                            <td style="width: 122px">&nbsp;Address Match Data: </td>
                                        
                        </tr>
                                     <tr>
                            <td style="width: 122px">&nbsp;</td>
                            
                        </tr>
                                     <tr>
                                        <td style="width: 122px">
                                            Notice Of Correction:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litNotice" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            County:</td>
                                        <td style="width: 169px">
                                            <asp:Literal ID="litCounty" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            Number:</td>
                                        <td>
                                            <asp:Literal ID="litNumber" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 122px">
                                            Post Code:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litPostCode" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 150px">
                                            Post Town:</td>
                                        <td style="width: 169px">
                                            <asp:Literal ID="litPostTown" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 159px">
                                            Street:</td>
                                        <td>
                                            <asp:Literal ID="litStreet1" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 122px">
                                            Address ID:</td>
                                        <td style="width: 163px">
                                            <asp:Literal ID="litAddressID" runat="server"></asp:Literal>
                                        </td>
                                         </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            
                        </tr>
                       <tr>
                            <td><strong>Authentication Characters: </strong></td>
                         </tr>
                         <tr>
                            <td><table style="width:100%;" cellpadding="0" cellspacing="0">
                    
                    <tr>
                            <td style="width: 839px">
                                Y = Matched on number / matched to supplied date of birth</td>
                        <td style="width: 753px">
                                N = date not checked, applicant under 18</td>
                        <td style="width: 391px">
                                X = Other</td>
                        </tr>
                                 <tr>
                            <td style="width: 839px">
                                T = the MMDD part of the date is transposed</td>
                        <td style="width: 753px">
                                E = The DDYY part of the date matched</td>
                        <td style="width: 391px">
                                U = ex-directory</td>
                        </tr>
                                     <tr>
                            <td style="width: 839px">
                                B = The MMYY part of the date matched</td>
                        <td style="width: 753px">
                                D = The DDMM part of the date matched</td>
                        <td style="width: 391px">
                                M = Mobile or pager</td>
                        </tr>
                                      <tr>
                            <td style="width: 839px">
                                E = The DDYY part of the date matched</td>
                        <td style="width: 753px">
                                G = The MM part of the date only matched</td>
                        <td style="width: 391px">
                                C = No access to data</td>
                        </tr>
                                  <tr>
                            <td style="width: 839px">
                                s = No data held / does not match to supplied date of birth,<br> whole date</td>
                        <td style="width: 753px">
                                H = The YY part of the date only matched</td>
                        <td style="width: 391px">
                                A = No data held</td>
                        </tr>
                                </table>
                                </td>
                           
                        </tr>
                        <tr>
                            <td>
                              F = Unmatched, STD code does not match and local number has more than 2 digits different</td>
                         </tr>
                         <tr>
                            <td>
                              1 = Partial match, STD code matches and local number is the same length and 2 digits or less are different</td>
                         </tr>
                        <tr>
                            <td>
                              2 = Partial match, STD code matches and local number is a different length and 2 digits or less are different</td>
                         </tr>
                        <tr>
                            <td>
                              3 = Partial match, STD code does not match but local number is the same length and matches</td>
                         </tr>
                        <tr>
                            <td>
                              4 = Partial match, STD code does not match and local number is the same length but 2 digits or less are different</td>
                         </tr>
                        <tr>
                            <td>
                              5 = Partial match, STD code does not match and local number is a different length and 2 digits or less are different</td>
                         </tr>
                        <tr>
                            <td>
                              6 = Partial match, STD code matches and local number is the same length but more than 2 digits are different</td>
                         </tr>
                        <tr>
                            <td>
                              7 = Partial match, STD code matches and local number is a different length and more than 2 digits are different</td>
                         </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr> 
        
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="BodyText"><strong>Bank Wizard: </strong>
                <asp:Literal ID="litBankWizardStatus" runat="server"></asp:Literal></td>
        </tr>
         <tr>
            <td class="BodyText">
                <asp:Panel ID="Panel2" runat="server">
                    Error Text:&nbsp; <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                    </asp:Panel>
                 </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlBankCheck" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width: 134px">
                                            Sort Code Verification (best Account Number) - LIVE:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSC032" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 190px">
                                            Account Number Verification - LIVE:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSC034" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Sort Code Verification (best Account Number) - LIVE:</td>
                                        <td>
                                            <asp:Literal ID="litQSP032" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px">
                                            Account Number Verification - LIVE:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSP034" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 190px">
                                            Sort Code Verification (best Account Number) - LIVE:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSE032" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Account Number Verification - LIVE:</td>
                                        <td>
                                            <asp:Literal ID="litQSE034" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px">
                                            Sort Code Verification (best Account Number) - LIVE:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSN032" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 190px">
                                            Account Number Verification - LIVE:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSN034" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Sort Code Verification - SETTLED:</td>
                                        <td>
                                            <asp:Literal ID="litQSC031" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px; height: 52px">
                                            Sort Code Verification (best Account Number) - SETTLED:</td>
                                        <td style="width: 165px; height: 52px">
                                            <asp:Literal ID="litQSC033" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px; height: 52px">
                                            Account Number Verification - SETTLED:</td>
                                        <td style="width: 166px; height: 52px">
                                            <asp:Literal ID="litQSC035" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px; height: 52px">
                                            Sort Code Verification - LIVE:</td>
                                        <td style="height: 52px">
                                            <asp:Literal ID="litQSP030" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px">
                                            Sort Code Verification - SETTLED:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSP031" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px">
                                            Sort Code Verification (best Account Number) - SETTLED:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSP033" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Account Number Verification - SETTLED:</td>
                                        <td>
                                            <asp:Literal ID="litQSP035" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px">
                                            Sort Code Verification - LIVE:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSE030" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px">
                                            Sort Code Verification - SETTLED:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSE031" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Sort Code Verification (best Account Number) - SETTLED:</td>
                                        <td>
                                            <asp:Literal ID="litQSE033" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 134px">
                                            Account Number Verification - SETTLED:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSE035" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px">
                                            Sort Code Verification - LIVE:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSN030" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Sort Code Verification - SETTLED:</td>
                                        <td>
                                            <asp:Literal ID="litQSN031" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="width: 134px">
                                            Sort Code Verification (best Account Number) - SETTLED:</td>
                                        <td style="width: 165px">
                                            <asp:Literal ID="litQSN033" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px">
                                            Account Number Verification - SETTLED:</td>
                                        <td style="width: 166px">
                                            <asp:Literal ID="litQSN035" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            Sort Code Verification - LIVE:</td>
                                        <td>
                                            <asp:Literal ID="litQSC030" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 134px">
                                            <strong>Sort Code Check:</strong></td>
                                        <td style="width: 165px; font-weight: 700">
                                            <asp:Literal ID="litFBC1" runat="server"></asp:Literal>
                                        </td>
                                       <td style="width: 190px">
                                            <strong>Account Number Check :</strong></td>
                                        <td style="width: 166px; font-weight: 700">
                                            <asp:Literal ID="litFBC2" runat="server"></asp:Literal>
                                        </td>
                                        <td style="width: 167px">
                                            <strong>Combined Sort Code/Acc.Number Check:</strong></td>
                                        <td style="font-weight: 700">
                                            <asp:Literal ID="litFBC5" runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                            <td style="width: 839px">
                               <strong> Bank Check Basic :&nbsp; </strong></td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                Y = Passed Bank Account / Sort Code Validation</td>
                        <td style="width: 753px">
                                F = Failed Bank Account / Sort Code Validation</td>
                        <td style="width: 391px">
                                X = Other</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                0 = Sort code and account number valid together</td>
                        <td style="width: 753px">
                                1 = Sort code and account number invalid together</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                               &nbsp;</td>
                        <td style="width: 753px">
                                &nbsp;</td>
                        <td style="width: 391px">
                                &nbsp;</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                               <strong> Bank Check Advance :&nbsp; </strong></td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                N = Account number / Sortcode not supplied</td>
                        <td style="width: 753px">
                                X = Account number / Sortcode invalid, ie not useable format</td>
                        <td style="width: 391px">
                                T = Address not found</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                H = Account number / Sortcode validation not available</td>
                        <td style="width: 753px">
                                A = Sort code/Account number not validated: ie does not match</td>
                        <td style="width: 391px">
                                M = No data held at address</td>
                        </tr>
                    <tr>
                            <td style="width: 839px; height: 32px;">
                                1 = Account Number is valid all 8 digits / Sort Code is <br> valid all 6 digits</td>
                        <td style="width: 753px; height: 32px;">
                                2 = Account number valid to 7 digits / Sort code valid to 5 digits</td>
                        <td style="width: 391px; height: 32px;">
                                C = No qualifiying data at address</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                3 = Account Number is valid all 8 digits / Sort Code is <br> valid all 6 digits</td>
                        <td style="width: 753px">
                                &nbsp;</td>
                        <td style="width: 391px">
                                &nbsp;</td>
                        </tr>
                    </table>
                    </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr runat="server" id="BankCheckConditionsHeaderRow">
                            <td><strong>
                                Bank Wizard: Conditions</strong></td>
                        </tr>
                        <tr runat="server" id="BankCheckConditionsDataRow">
                            <td>
                                <asp:GridView ID="gvBankconditions" runat="server" AutoGenerateColumns="False" 
                                    EnableModelValidation="True" SkinID="NormalGrid" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ErrorCode" HeaderText="ErrorCode">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Sevirity" HeaderText="Sevirity">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Value" HeaderText="Value">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #3366FF">
                <strong style="color: #C0C0C0">Credit Search Report</strong></td>
        </tr>  
         <tr>
                        <td>
                </td>
                    </tr> 
         <tr>
            <td class="BodyText"><strong>CreditSearch: </strong><asp:Literal ID="LiteralCredit" runat="server"></asp:Literal></td>
        </tr>   
         <tr>
            <td class="BodyText">
                <asp:Panel ID="Panel3" runat="server">
                    Error Text:&nbsp; <asp:Literal ID="Literal6" runat="server"></asp:Literal>
                    </asp:Panel>
                 </td>
        </tr> 
         <tr>
                        <td>
                </td>
                    </tr>  
        <tr runat="server" id = "CreditReport">
            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr id="CD_Scoring_Row" runat="server">
            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="background-color: #C0C0C0">
                            <strong>SCORING</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        FTOLF04_Value:</td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="Scoring_E5S01" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        RNOLF04_Value:</td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="Scoring_E5S02" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        Total&nbsp; Score:</td>
                                    <td style="font-weight: 700; width: 102px;">
                                        <asp:Literal ID="Scoring_E5S041" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="descScoring_E5S042" runat="server" 
                                            Text="&nbsp;"></asp:Literal>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server" Text="Sort Code Verification (best Account Number) - LIVE:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSC032" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal3" runat="server" Text="Account Number Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td width: 117px;">
                                        <asp:Literal ID="QSC034" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal5" runat="server" Text="Sort Code Verification (best Account Number) - LIVE:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSP032" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal7" runat="server" Text="Account Number Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSP034" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal8" runat="server" Text="Sort Code Verification (best Account Number) - LIVE:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSE032" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal10" runat="server" Text="Account Number Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="QSE034" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal12" runat="server" Text="Sort Code Verification (best Account Number) - LIVE:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSN032" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal14" runat="server" Text="Account Number Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSN034" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal15" runat="server" Text="Sort Code Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSC031" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal16" runat="server" Text="Sort Code Verification (best Account Number) - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="QSC033" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal17" runat="server" Text="Account Number Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSC035" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal18" runat="server" Text="Sort Code Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSP030" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal19" runat="server" Text="Sort Code Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSP031" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal20" runat="server" Text="Sort Code Verification (best Account Number) - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="QSP033" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal21" runat="server" Text="Account Number Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSP035" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal22" runat="server" Text="Sort Code Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSE030" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal23" runat="server" Text="Sort Code Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSE031" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal24" runat="server" Text="Sort Code Verification (best Account Number) - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="QSE033" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal25" runat="server" Text="Account Number Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSE035" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal26" runat="server" Text="Sort Code Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSN030" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px">
                                        <asp:Literal ID="Literal27" runat="server" Text="Sort Code Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px">
                                        <asp:Literal ID="QSN031" runat="server"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal28" runat="server" Text="Sort Code Verification (best Account Number) - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px">
                                        <asp:Literal ID="QSN033" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px">
                                        <asp:Literal ID="Literal29" runat="server" Text="Account Number Verification - SETTLED:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px">
                                        <asp:Literal ID="QSN035" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px">
                                        <asp:Literal ID="Literal30" runat="server" Text="Sort Code Verification - LIVE:"></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="QSC030" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 40px; height: 28px;">
                                        <asp:Literal ID="Literal31" runat="server" Text="FraudScan Bank Group - Sort Code Check:"></asp:Literal>
                                    </td>
                                    <td style="width: 109px; height: 28px;">
                                        <asp:Literal ID="FBC1" runat="server"></asp:Literal>
                                    </td>
                                    <td style="height: 28px">
                                        <asp:Literal ID="Literal32" runat="server" Text="FraudScan Bank Group - Account Number Check:"></asp:Literal>
                                    </td>
                                    <td style="width: 117px; height: 28px;">
                                        <asp:Literal ID="FBC2" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 101px; height: 28px;">
                                        <asp:Literal ID="Literal33" runat="server" Text="FraudScan Bank Group - Combined Sort Code/Account Number Check:"></asp:Literal>
                                    </td>
                                    <td style="width: 102px; height: 28px;">
                                        <asp:Literal ID="FBC5" runat="server"></asp:Literal>
                                    </td>
                                    <td style="width: 114px; height: 28px;">
                                        <asp:Literal ID="Literal34" runat="server" Text="Option"></asp:Literal>
                                    </td>
                                    <td style="height: 28px">
                                        <asp:Literal ID="Options" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                     <tr>
                        <td>
                             <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                            <td style="width: 839px">
                               <strong> Credit Check Basic :&nbsp; </strong></td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                Y = Passed Bank Account / Sort Code Validation</td>
                        <td style="width: 779px">
                                F = Failed Bank Account / Sort Code Validation</td>
                        <td style="width: 391px">
                                X = Other</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                0 = Sort code and account number valid together</td>
                        <td style="width: 779px">
                                1 = Sort code and account number invalid together</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                               &nbsp;</td>
                        <td style="width: 779px">
                                &nbsp;</td>
                        <td style="width: 391px">
                                &nbsp;</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                               <strong> Credit Check Advance :&nbsp; </strong></td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                N = Account number / Sortcode not supplied</td>
                        <td style="width: 779px">
                                X = Account number / Sortcode invalid, ie not useable <br>format</td>
                        <td style="width: 391px">
                                T = Address not found</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                H = Account number / Sortcode validation not available</td>
                        <td style="width: 779px">
                                A = Sort code/Account number not validated: ie does not match</td>
                        <td style="width: 391px">
                                M = No data held at address</td>
                        </tr>
                    <tr>
                            <td style="width: 839px; height: 32px;">
                                1 = Account Number is valid all 8 digits / Sort Code is <br> valid all 6 digits</td>
                        <td style="width: 779px; height: 32px;">
                                2 = Account number valid to 7 digits / Sort code valid to 5 digits</td>
                        <td style="width: 391px; height: 32px;">
                                C = No qualifiying data at address</td>
                        </tr>
                    <tr>
                            <td style="width: 839px">
                                3 = Account Number is valid all 8 digits / Sort Code is <br> valid all 6 digits</td>
                        <td style="width: 779px">
                                &nbsp;</td>
                        <td style="width: 391px">
                                &nbsp;</td>
                        </tr>
                    </table></td>
                    </tr>
                    <!--tr id="Electro">
                        <td style="background-color: #C0C0C0">
                            <strong>ELECTORAL ROLL/ADDRESS CONFIRMATION</strong></td>
                    </tr-->
                    <tr>
                        <td>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr id="CD_PubInfo_Row" runat="server">
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width:100%;">
                                <tr>
                                    <td style="background-color: #C0C0C0">
                                        <strong>PUBLIC INFORMATION</strong></td>
                                </tr>
                                <tr runat="server" id="ElectroRollDataRow">
                            <td>
                                <asp:GridView ID="ElectroRoll" runat="server" AutoGenerateColumns="False" SkinID="NormalGrid" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="MiddleName" HeaderText="Middle Name">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SurName" HeaderText="Sur Name">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ForeName" HeaderText="Fore Name">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NameMatchStatus" HeaderText="Match Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Seniority" HeaderText="Seniority">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Start" HeaderText="Start">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="End" HeaderText="End">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <asp:ObjectDataSource ID="odsElectoral" runat="server" 
                    SelectMethod="GetCreditSearchElectroRollData" 
                    TypeName="PollokCU.DataAccess.Layer.clsEquifax">
                </asp:ObjectDataSource>
                            </td>
                        </tr>
                                 
        <tr>
            <td>
                &nbsp;</td>
        </tr>
                                </table>
                             </td>
        </tr>
          <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr id="CD_IMPAIRED_CH" runat="server">
            <td>
                <table style="width:100%;">
                    <tr>
                        <td style="background-color: #C0C0C0"><strong>MATCH ADDRESSES</strong></td>
                    </tr>
                     <tr runat="server" id="AddressMatchDataRow">
                            <td>
                                <asp:GridView ID="AddressMatch" runat="server" AutoGenerateColumns="False" SkinID="NormalGrid" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="AddressMatchStatus" HeaderText="Address Status">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="County" HeaderText="County">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Number" HeaderText="Number">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PostCode" HeaderText="PostCode">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PostTown" HeaderText="PostTown">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Street1" HeaderText="Street">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AddressID" HeaderText="Address ID">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    
               <tr>
                        <td>
                             <asp:ObjectDataSource ID="odsMatchAddress" runat="server" 
                    SelectMethod="GetCreditSearchMatchAddressData" 
                    TypeName="PollokCU.DataAccess.Layer.clsEquifax">
                </asp:ObjectDataSource></td>
                    </tr>
                </table>
            </td>
        </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                
           
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>



