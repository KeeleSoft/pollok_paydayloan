﻿<%@ page title="" language="VB" masterpagefile="~/Staff/MasterPageHeaderOnly.master" autoeventwireup="false" inherits="Staff_StaffViewWorldCollections, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 80%;" class="BodyText">
        <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;WorldPay Payment Collection Schedule</span></td>
          </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                </td>
        </tr>
        <tr>
            <td>
                                  <table style="width:100%;">
                                      <tr>
                                          <td>
                                              <asp:Button ID="btnNextToProcess" runat="server" 
                                                  Text="Next Payments To Process" />
                                          </td>
                                          <td>
                                              <asp:Button ID="btnAllSchedules" runat="server" Text="All Scheduled Payments" />
                                          </td>
                                          <td>
                                              <asp:Button ID="btnAllFailed" runat="server" Text="All Failed Payments" 
                                                  Width="150px" />
                                          </td>
                                          <td>
                                              <asp:Button ID="llSuccess" runat="server" Text="All Success Payments" />
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              &nbsp;</td>
                                          <td>
                                              &nbsp;</td>
                                          <td>
                                              <asp:TextBox ID="txtFailedDate" runat="server" Width="148px"></asp:TextBox><asp:CalendarExtender
                                                      ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" 
                                                      PopupPosition="Right" TargetControlID="txtFailedDate" 
                                                      TodaysDateFormat="dd/MM/yyyy">
                                                  </asp:CalendarExtender>
                                          </td>
                                          <td>
                                              &nbsp;</td>
                                      </tr>
                                  </table>
                                            </td>
        </tr>
        <tr>
            <td>
                                  &nbsp;</td>
        </tr>
        <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;<asp:Literal 
                    ID="ReportDesc" runat="server"></asp:Literal></span></td>
          </tr>
        <tr>
            <td>
                                                <asp:GridView ID="gvSageCollections" runat="server" 
                                                    AutoGenerateColumns="False" SkinID="NormalGrid" 
                                                    Width="100%">
                                                    <Columns>
                                                        <asp:BoundField DataField="SequenceID" HeaderText="Sequence" 
                                                            SortExpression="SequenceID" >
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FullName" HeaderText="Name" 
                                                            SortExpression="FullName">
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Amount" HeaderText="Amount" 
                                                            SortExpression="Amount" >
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ScheduleDateTime" 
                                                            HeaderText="Schedule Date" />
                                                        <asp:BoundField DataField="PaymentTakenDesc" HeaderText="Payment Taken" 
                                                            SortExpression="PaymentTakenDesc" >
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AttemptCount" HeaderText="Attempted Count" >
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MonthlyAttemptCount" HeaderText="Monthly Attempt" />
                                                        <asp:BoundField DataField="LastAttemptDateTime" HeaderText="Last Attempted" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hlEditCollection" runat="server" 
                                                                    target="_SageCollectEdit"
                                                                    ImageUrl="~/images/icon_edit.png" 
                                                                    NavigateUrl='<%# "StaffEditPDLPaymentCollections.aspx?AppType=" & Eval("ApplicationType") & "&ID="& Eval("ApplicationID") &"&CollectionID=" & Eval("SagePayCollectionID") %>' 
                                                                    Text="Edit/View PDL "></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        No collection schedule available
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>


