﻿<%@ page language="VB" masterpagefile="MasterPageHeaderOnly.master" autoeventwireup="false" inherits="StaffYSA, App_Web_kgsx03tj" stylesheettheme="Grey" %>

<%@ Register src="MenuStaff_1.ascx" tagname="MenuStaff_1" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" colspan="6" style="width: 140px">
            
          </td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td width="130">&nbsp;</td>
        <td width="10">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
      <tr>
        <td width="10" valign="top">
            &nbsp;</td>
        <td valign="top"><uc3:MenuStaff_1 ID="MenuStaff_1" runat="server" /></td>
        <td valign="top">&nbsp;</td>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
          <tr>
            <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5"><span class="style37 style80">&nbsp;&nbsp;Young Savers </span></td>
          </tr>
          <tr>
            <td height="25" valign="middle">&nbsp;</td>
          </tr>          
          <tr>
            <td height="25" valign="middle">
                                                                        <asp:Label ID="lblMessage" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                                                    </td>
          </tr>
          <tr>
            <td height="25" valign="middle">
                <table 
                    style="width:100%;" class="BodyText">
                <tr>
                    <td>
                        <strong>Filter...</strong></td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                <tr>
                    <td>
                        School</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlSchool" runat="server" Width="154px" 
                                                                            DataSourceID="odsSchools" DataTextField="SchoolName" DataValueField="Payrunno">
                                                                            <asp:ListItem></asp:ListItem>
                                                                            <asp:ListItem>1</asp:ListItem>
                                                                            <asp:ListItem>2</asp:ListItem>
                                                                            <asp:ListItem>3</asp:ListItem>
                                                                            <asp:ListItem>4</asp:ListItem>
                                                                            <asp:ListItem>5</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        Member No</td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtMemberNo" runat="server" Width="150px"></asp:TextBox>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnUpdateLogin" runat="server" Text="Update School Login" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                            <asp:ObjectDataSource ID="odsSchools" runat="server" 
                                SelectMethod="SelectSchools" 
                                TypeName="PollokCU.DataAccess.Layer.clsSchoolsSavings">
                            </asp:ObjectDataSource>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                            </td>
          </tr>
          <tr>
            <td valign="middle" class="BodyText"><br/>
                                    
                    <asp:ObjectDataSource ID="odsYSADeposits" runat="server" 
                    SelectMethod="SelectSchoolDepositsByStaff" 
                     TypeName="PollokCU.DataAccess.Layer.clsSchoolsSavings">
                    <SelectParameters>
                    <asp:Parameter Name="payRunNo" DefaultValue="0" Type="Int32" />
                    <asp:Parameter Name="memberID" DefaultValue="0" Type="Int32" />
                        <asp:Parameter DefaultValue="01/01/1999" Name="depositDate" 
                            Type="String" />
                    </SelectParameters>                   
                </asp:ObjectDataSource>
                <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="odsYSADeposits" PageSize="100" SkinID="NormalGrid" 
                    Width="100%" AllowSorting="True" AllowPaging="True" 
                    OnRowDataBound="gvBatches_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="TransactionDate" HeaderText="Date" 
                            SortExpression="TransactionDate" />
                        <asp:TemplateField HeaderText="School Name" SortExpression="SchoolName">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Bind("SchoolName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FullName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="MemberID" HeaderText="Member#" 
                            SortExpression="MemberID" >
                            <HeaderStyle BackColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MemberName" HeaderText="Member Name" 
                            SortExpression="MemberName" />
                        <asp:BoundField DataField="Amount" 
                            HeaderText="Amount" 
                            SortExpression="Amount" DataFormatString="{0:c}" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEditClaim" runat="server" 
                                    ImageUrl="~/images/icon_edit.png" 
                                    NavigateUrl='<%# "StaffEditYSA.aspx?ID=" & Eval("DepositID") %>' 
                                    Text="Edit/View PDL "></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                                    
                    </p>
                For security reasons, when you have finished using staff services always select LOG OUT.
                </td>
          </tr>
          <!-- TemplateEndEditable -->
        </table></td>
        <td width="23">&nbsp;</td>
        <td width="10" valign="top">
            &nbsp;</td>
      </tr>
    </table>
</asp:Content>

