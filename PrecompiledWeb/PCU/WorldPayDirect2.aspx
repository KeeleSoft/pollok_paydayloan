﻿<%@ page title="" language="VB" autoeventwireup="false" inherits="WorldPayDirect2, App_Web_hw0trknz" stylesheettheme="Grey" %>

<%@ Register src="MenuLogged2.ascx" tagname="MenuLogged" tagprefix="uc1" %>

<%@ Register src="MenuPreLogged2.ascx" tagname="MenuPreLogged" tagprefix="uc4" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Pollok Credit Union - Online Services</title>   
<link href="scu2.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/JavaScript">
<!--
    function MM_preloadImages() { //v3.0
        var d = document; if (d.images) {
            if (!d.MM_p) d.MM_p = new Array();
            var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
                if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
        }
    }
    //-->
</script>
    <style type="text/css">
        .auto-style1 {
            width: 64%;
        }
    </style>
</head>
<body>
<form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    </div>
    <table width="900" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="240">&nbsp;</td>
        <td width="177">&nbsp;</td>
        <td width="443" valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td><img src="Images/POLLOK.jpeg" width="200" height="100"></td>
        <td colspan="2"><div align="right" class="Button style15">
            <table width="400" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="400" height="60" valign="top"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td width="390"><div align="right" class="Button"><a href="http://www.creditunion.co.uk/About%20us.htm" class="Button">About us </a> | <a href="http://www.creditunion.co.uk/Contact.htm" class="Button">Contact</a> | <a href="http://www.creditunion.co.uk/Downloads.htm" class="Button">Downloads</a> | <a href="http://www.creditunion.co.uk/News.htm" class="Button">News | </a><a href="#" class="Button"></a><span class="style21"><a href="#" class="Button"><span class="style13"></span></a><span class="style13"><a href="http://www.creditunion.co.uk/Governance.htm" class="Button">Governance</a> | <a href="http://www.creditunion.co.uk/CSR.htm" class="Button">CSR</a> | <a href="http://www.creditunion.co.uk/Legal.htm" class="Button">Legal</a> </span></span></div></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td width="400">&nbsp;</td>
              </tr>
              <tr>
                <td width="400"><div align="right" class="style34 style35">
                    <div align="right" class="style58 style59 style85 style86 style71"> </div>
                </div></td>
              </tr>
            </table>
        </div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30" valign="bottom"><table width="879" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="21">&nbsp;</td>
        <td width="858"><br>
          .............................................................................................................................................................................................................................................................................................<br>
          <br>
          <strong>Online Services<br>
          </strong>.............................................................................................................................................................................................................................................................................................<strong><br>
          <br>
          </strong></td>
      </tr>
    </table></td>
  </tr>
  
  <tr>
    <td height="15">&nbsp;</td>
  </tr>
  
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top">
    <table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="130" valign="top">
                <uc1:MenuLogged ID="MenuLogged1" runat="server" />
                <uc4:MenuPreLogged ID="MenuPreLogged1" runat="server" />
            </td>
            <td width="23">
                &nbsp;</td>
            <td valign="top">
                <table width="466" border="0" cellspacing="0" cellpadding="0">
          <!-- TemplateBeginEditable name="Main body" -->          
                    <tr>
                        <td height="25" valign="middle" bgcolor="#0068B6" class="Button style5">
                            <span class="style37 style80">&nbsp;Make a payment to Pollok - Confirm</span></td>
                    </tr>
                    <tr>
                        <td height="25" valign="middle" class="Button style5">
                            Please confirm your details to proceed</td>
                    </tr>
                    <tr>
                        <td height="10" valign="middle" class="BodyText">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMessege" runat="server" CssClass="errormsg" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="BodyText" style="width:100%;">
                                            <tr>
                                                <td width="30%">
                                                    Member Number:</td>
                                                <td>
                                                    <asp:Literal ID="litMemberNumber" runat="server"></asp:Literal>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Reason for payment:</td>
                                                <td>
                                                    <asp:Literal ID="litReason" runat="server"></asp:Literal>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Amount to pay (GBP):</td>
                                                <td>
                                                    <asp:Literal ID="litAmount" runat="server"></asp:Literal>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Loan ID:</td>
                                                <td>
                                                    <asp:Literal ID="litLoanID" runat="server"></asp:Literal>
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>
                                                    &nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" Width="70px" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Confirm" 
                                                       
                                                        Width="70px" />
                                                </td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <br />
                            <asp:Image ID="Image1" runat="server" Height="30px" ImageUrl="~/Images/VISA.gif" Width="51px" />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/visa_electron.gif" Width="44px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image5" runat="server" Height="29px" ImageUrl="~/Images/amex-logo2.gif" Width="58px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Image ID="Image6" runat="server" Height="30px" ImageUrl="~/Images/JCB.gif" Width="59px" />
                            <br />

                            <br>Pollok Credit Union will never ask you for your memorable data or Password in an e-mail. Never disclose this information to anyone.<br />
                            <h3>Terms and conditions :</h3> 
                             <asp:HyperLink ID="hlTC" runat="server" 
                                                        NavigateUrl="~/PDL/Docs/TC_LCCU.pdf" Target="new">Please click here 
                                                    to view Terms and conditions.</asp:HyperLink> <br />  <br />
                            Refund policy: Refunds are by discrete management final decision. <br />
                            <h3>Contact details :</h3>
                            Pollok Credit Union <br />
                            Main line: 0141 881 8731 <br />
                            Fax: 0141 881 8731 <br />
                            Address: 140 Woodhead road, Glasgow, G53 7NN <br />
                            Email: info@pollokcu.com <br /> 
                            Web: <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.pcu.org.uk" Target="new">www.pcu.org.uk</asp:HyperLink> <br />
                            <br />
                            <br /> For security reasons, when you have finished using Internet Banking always select LOG OUT.
                        </td>
                    </tr>
          <!-- TemplateEndEditable -->
                </table>
            </td>
            <td width="23">
                &nbsp;</td>
            <td width="218" valign="top">
                &nbsp;</td>
        </tr>
    </table>

    </td>        
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="860" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="10" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="30"><div align="center">.............................................................................................................................................................................................................................................................................................</div></td>
      </tr>
      <tr>
        <td height="30" valign="top" class="BodyText1 style23 style44 style13 style66 style56">
            <table border="0" cellspacing="0" cellpadding="0" style="width: 97%">
            <tr>
              <td class="auto-style1"><span class="style84">Pollok Credit Union is authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential Regulation Authority Firm No: 213798. We are members of the Financial Services Compensation Scheme and The Financial Ombudsman Service. Registered Office, Silverburn, Unit W13, 763 Barrhead Road, Glasgow, G53 6QR. </span></td>
              
                
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>


